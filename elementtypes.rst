.. _elementtypes:

Element types
-------------

.. _structureelements:

STRUCTURE ELEMENTS
------------------

.. _beam3:

BEAM3
~~~~~

*Shapes:*

- :ref:`LINE2 <line2>` (2 nodes)
- :ref:`LINE3 <line3>` (3 nodes)
- :ref:`LINE4 <line4>` (4 nodes)
- :ref:`LINE5 <line5>` (5 nodes)
- :ref:`LINE6 <line6>` (6 nodes)

Number of parameters: 3
no parameter,,MAT
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
+--------------+-------------+

.. _beam3r:

BEAM3R
~~~~~~

*Shapes:*

- :ref:`HERM2LINE2 <herm2line2>` (2 nodes)
- :ref:`HERM2LINE3 <herm2line3>` (3 nodes)
- :ref:`HERM2LINE4 <herm2line4>` (4 nodes)
- :ref:`HERM2LINE5 <herm2line5>` (5 nodes)
- :ref:`LINE2 <line2>` (2 nodes)
- :ref:`LINE3 <line3>` (3 nodes)
- :ref:`LINE4 <line4>` (4 nodes)
- :ref:`LINE5 <line5>` (5 nodes)

Number of parameters: 5
no parameter,,MAT,TRIADS,FAD
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
| TRIADS         |  6 x number |
| FAD (opt.)     | None        |
+--------------+-------------+

.. _beam3eb:

BEAM3EB
~~~~~~~

*Shapes:*

- :ref:`LINE2 <line2>` (2 nodes)
- :ref:`LINE3 <line3>` (3 nodes)

Number of parameters: 3
no parameter,,MAT
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
+--------------+-------------+

.. _beam3k:

BEAM3K
~~~~~~

*Shapes:*

- :ref:`LINE2 <line2>` (2 nodes)
- :ref:`LINE3 <line3>` (3 nodes)
- :ref:`LINE4 <line4>` (4 nodes)

Number of parameters: 7
no parameter,,WK,ROTVEC,MAT,TRIADS,FAD
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| WK             |  1 x number |
| ROTVEC         |  1 x number |
| MAT            |  1 x number |
| TRIADS         |  6 x number |
| FAD (opt.)     | None        |
+--------------+-------------+

.. _bele2:

BELE2
~~~~~

*Shapes:*

- :ref:`LINE2 <line2>` (2 nodes)
- :ref:`LINE3 <line3>` (3 nodes)

Number of parameters: 1
no parameter
.. _discsh3:

DISCSH3
~~~~~~~

*Shapes:*

- :ref:`TRI3 <tri3>` (3 nodes)

Number of parameters: 4
no parameter,,MAT,THICK
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
| THICK          |  1 x number |
+--------------+-------------+

.. _rigidsphere:

RIGIDSPHERE
~~~~~~~~~~~

*Shapes:*

- :ref:`POINT1 <point1>` (1 nodes)

Number of parameters: 4
no parameter,,RADIUS,DENSITY
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| RADIUS         |  1 x number |
| DENSITY        |  1 x number |
+--------------+-------------+

.. _nstet4:

NSTET4
~~~~~~

*Shapes:*

- :ref:`TET4 <tet4>` (4 nodes)

Number of parameters: 4
no parameter,,MAT,KINEM
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
| KINEM          |  1 x string |
+--------------+-------------+

.. _nstet5:

NSTET5
~~~~~~

*Shapes:*

- :ref:`TET4 <tet4>` (4 nodes)

Number of parameters: 13
no parameter,,MAT,KINEM,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _shell8:

SHELL8
~~~~~~

*Shapes:*

- :ref:`QUAD4 <quad4>` (4 nodes)
- :ref:`QUAD8 <quad8>` (8 nodes)
- :ref:`QUAD9 <quad9>` (9 nodes)
- :ref:`TRI3 <tri3>` (3 nodes)
- :ref:`TRI6 <tri6>` (6 nodes)

Number of parameters: 10
no parameter,,MAT,THICK,GP,GP_TRI,FORCES,EAS,ANS,SDC
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
| THICK          |  1 x number |
| GP             |  3 x number |
| GP_TRI         |  1 x number |
| FORCES         |  1 x string |
| EAS            |  5 x string |
| ANS            |  1 x string |
| SDC            |  1 x number |
+--------------+-------------+

.. _shell8scatra:

SHELL8SCATRA
~~~~~~~~~~~~

*Shapes:*

- :ref:`QUAD4 <quad4>` (4 nodes)
- :ref:`QUAD8 <quad8>` (8 nodes)
- :ref:`QUAD9 <quad9>` (9 nodes)
- :ref:`TRI3 <tri3>` (3 nodes)
- :ref:`TRI6 <tri6>` (6 nodes)

Number of parameters: 11
no parameter,,MAT,THICK,GP,GP_TRI,FORCES,EAS,ANS,SDC,TYPE
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
| THICK          |  1 x number |
| GP             |  3 x number |
| GP_TRI         |  1 x number |
| FORCES         |  1 x string |
| EAS            |  5 x string |
| ANS            |  1 x string |
| SDC            |  1 x number |
| TYPE           |  1 x string |
+--------------+-------------+

.. _solidh18:

SOLIDH18
~~~~~~~~

*Shapes:*

- :ref:`HEX18 <hex18>` (18 nodes)

Number of parameters: 13
no parameter,,MAT,KINEM,AXI,CIR,FIBER1,FIBER2,FIBER3,HU,RAD,STRENGTH,lambda
**Parameters**

+-----------------+-------------+
| Parameter         |             |
+=================+=============+
|  (opt.)           | None        |
| MAT               |  1 x number |
| KINEM             |  1 x string |
| AXI (opt.)        |  3 x number |
| CIR (opt.)        |  3 x number |
| FIBER1 (opt.)     |  3 x number |
| FIBER2 (opt.)     |  3 x number |
| FIBER3 (opt.)     |  3 x number |
| HU (opt.)         |  1 x number |
| RAD (opt.)        |  3 x number |
| STRENGTH (opt.)   |  1 x number |
| lambda (opt.)     |  1 x number |
+-----------------+-------------+

.. _solidh20:

SOLIDH20
~~~~~~~~

*Shapes:*

- :ref:`HEX20 <hex20>` (20 nodes)

Number of parameters: 14
no parameter,,MAT,KINEM,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,STRENGTH,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| STRENGTH (opt.)     |  1 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidh27:

SOLIDH27
~~~~~~~~

*Shapes:*

- :ref:`HEX27 <hex27>` (27 nodes)

Number of parameters: 14
no parameter,,MAT,KINEM,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,STRENGTH,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| STRENGTH (opt.)     |  1 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidh27poro:

SOLIDH27PORO
~~~~~~~~~~~~

*Shapes:*

- :ref:`HEX27 <hex27>` (27 nodes)

Number of parameters: 14
no parameter,,MAT,KINEM,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,STRENGTH,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| STRENGTH (opt.)     |  1 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidh27plast:

SOLIDH27PLAST
~~~~~~~~~~~~~

*Shapes:*

- :ref:`HEX27 <hex27>` (27 nodes)

Number of parameters: 14
no parameter,,MAT,KINEM,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,STRENGTH,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| STRENGTH (opt.)     |  1 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidh27thermo:

SOLIDH27THERMO
~~~~~~~~~~~~~~

*Shapes:*

- :ref:`HEX27 <hex27>` (27 nodes)

Number of parameters: 14
no parameter,,MAT,KINEM,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,STRENGTH,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| STRENGTH (opt.)     |  1 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _sonurbs27thermo:

SONURBS27THERMO
~~~~~~~~~~~~~~~

*Shapes:*

- :ref:`NURBS27 <nurbs27>` (27 nodes)

Number of parameters: 4
no parameter,,MAT,GP
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
| GP             |  3 x number |
+--------------+-------------+

.. _solidh20thermo:

SOLIDH20THERMO
~~~~~~~~~~~~~~

*Shapes:*

- :ref:`HEX20 <hex20>` (20 nodes)

Number of parameters: 14
no parameter,,MAT,KINEM,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,STRENGTH,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| STRENGTH (opt.)     |  1 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _sonurbs27:

SONURBS27
~~~~~~~~~

*Shapes:*

- :ref:`NURBS27 <nurbs27>` (27 nodes)

Number of parameters: 4
no parameter,,MAT,GP
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
| GP             |  3 x number |
+--------------+-------------+

.. _solidh8:

SOLIDH8
~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)

Number of parameters: 15
no parameter,,MAT,KINEM,EAS,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,STRENGTH,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| EAS                 |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| STRENGTH (opt.)     |  1 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidh8p1j1:

SOLIDH8P1J1
~~~~~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)

Number of parameters: 4
no parameter,,MAT,KINEM
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
| KINEM          |  1 x string |
+--------------+-------------+

.. _solidh8fbar:

SOLIDH8FBAR
~~~~~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)

Number of parameters: 13
no parameter,,MAT,KINEM,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidh8fbarscatra:

SOLIDH8FBARSCATRA
~~~~~~~~~~~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)

Number of parameters: 14
no parameter,,MAT,KINEM,TYPE,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| TYPE                |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidh8fbarthermo:

SOLIDH8FBARTHERMO
~~~~~~~~~~~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)

Number of parameters: 13
no parameter,,MAT,KINEM,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidh8poro:

SOLIDH8PORO
~~~~~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)

Number of parameters: 15
no parameter,,MAT,KINEM,EAS,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,STRENGTH,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| EAS                 |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| STRENGTH (opt.)     |  1 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidh8poroscatra:

SOLIDH8POROSCATRA
~~~~~~~~~~~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)

Number of parameters: 16
no parameter,,MAT,KINEM,EAS,TYPE,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,STRENGTH,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| EAS                 |  1 x string |
| TYPE                |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| STRENGTH (opt.)     |  1 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidh8porop1:

SOLIDH8POROP1
~~~~~~~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)

Number of parameters: 15
no parameter,,MAT,KINEM,EAS,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,STRENGTH,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| EAS                 |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| STRENGTH (opt.)     |  1 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidh8porop1scatra:

SOLIDH8POROP1SCATRA
~~~~~~~~~~~~~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)

Number of parameters: 16
no parameter,,MAT,KINEM,EAS,TYPE,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,STRENGTH,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| EAS                 |  1 x string |
| TYPE                |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| STRENGTH (opt.)     |  1 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidh8thermo:

SOLIDH8THERMO
~~~~~~~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)

Number of parameters: 15
no parameter,,MAT,KINEM,EAS,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,STRENGTH,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| EAS                 |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| STRENGTH (opt.)     |  1 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidh8plast:

SOLIDH8PLAST
~~~~~~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)

Number of parameters: 17
no parameter,,MAT,KINEM,EAS,FBAR,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,NUMGP,RAD,STRENGTH,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| EAS                 |  1 x string |
| FBAR                |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| NUMGP (opt.)        |  1 x number |
| RAD (opt.)          |  3 x number |
| STRENGTH (opt.)     |  1 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidh8scatra:

SOLIDH8SCATRA
~~~~~~~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)

Number of parameters: 16
no parameter,,MAT,KINEM,EAS,TYPE,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,STRENGTH,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| EAS                 |  1 x string |
| TYPE                |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| STRENGTH (opt.)     |  1 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidsh18:

SOLIDSH18
~~~~~~~~~

*Shapes:*

- :ref:`HEX18 <hex18>` (18 nodes)

Number of parameters: 17
no parameter,,MAT,KINEM,TSL,MEL,CTL,VOL,AXI,CIR,FIBER1,FIBER2,FIBER3,HU,RAD,STRENGTH,lambda
**Parameters**

+-----------------+-------------+
| Parameter         |             |
+=================+=============+
|  (opt.)           | None        |
| MAT               |  1 x number |
| KINEM             |  1 x string |
| TSL               |  1 x string |
| MEL               |  1 x string |
| CTL               |  1 x string |
| VOL               |  1 x string |
| AXI (opt.)        |  3 x number |
| CIR (opt.)        |  3 x number |
| FIBER1 (opt.)     |  3 x number |
| FIBER2 (opt.)     |  3 x number |
| FIBER3 (opt.)     |  3 x number |
| HU (opt.)         |  1 x number |
| RAD (opt.)        |  3 x number |
| STRENGTH (opt.)   |  1 x number |
| lambda (opt.)     |  1 x number |
+-----------------+-------------+

.. _solidsh18plast:

SOLIDSH18PLAST
~~~~~~~~~~~~~~

*Shapes:*

- :ref:`HEX18 <hex18>` (18 nodes)

Number of parameters: 17
no parameter,,MAT,KINEM,TSL,MEL,CTL,VOL,AXI,CIR,FIBER1,FIBER2,FIBER3,HU,RAD,STRENGTH,lambda
**Parameters**

+-----------------+-------------+
| Parameter         |             |
+=================+=============+
|  (opt.)           | None        |
| MAT               |  1 x number |
| KINEM             |  1 x string |
| TSL               |  1 x string |
| MEL               |  1 x string |
| CTL               |  1 x string |
| VOL               |  1 x string |
| AXI (opt.)        |  3 x number |
| CIR (opt.)        |  3 x number |
| FIBER1 (opt.)     |  3 x number |
| FIBER2 (opt.)     |  3 x number |
| FIBER3 (opt.)     |  3 x number |
| HU (opt.)         |  1 x number |
| RAD (opt.)        |  3 x number |
| STRENGTH (opt.)   |  1 x number |
| lambda (opt.)     |  1 x number |
+-----------------+-------------+

.. _solidsh8:

SOLIDSH8
~~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)

Number of parameters: 17
no parameter,,MAT,KINEM,EAS,ANS,THICKDIR,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,STRENGTH,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| EAS                 |  1 x string |
| ANS                 |  1 x string |
| THICKDIR            |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| STRENGTH (opt.)     |  1 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidsh8plast:

SOLIDSH8PLAST
~~~~~~~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)

Number of parameters: 10
no parameter,,MAT,KINEM,EAS,ANS,THICKDIR,FIBER1,FIBER2,FIBER3
**Parameters**

+---------------+-------------+
| Parameter       |             |
+===============+=============+
|  (opt.)         | None        |
| MAT             |  1 x number |
| KINEM           |  1 x string |
| EAS             |  1 x string |
| ANS             |  1 x string |
| THICKDIR        |  1 x string |
| FIBER1 (opt.)   |  3 x number |
| FIBER2 (opt.)   |  3 x number |
| FIBER3 (opt.)   |  3 x number |
+---------------+-------------+

.. _solidsh8p8:

SOLIDSH8P8
~~~~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)

Number of parameters: 14
no parameter,,MAT,KINEM,STAB,ANS,LIN,THICKDIR,EAS,ISO,AXI,CIR,RAD,STRENGTH
**Parameters**

+-----------------+-------------+
| Parameter         |             |
+=================+=============+
|  (opt.)           | None        |
| MAT               |  1 x number |
| KINEM             |  1 x string |
| STAB              |  1 x string |
| ANS               |  1 x string |
| LIN               |  1 x string |
| THICKDIR          |  1 x string |
| EAS               |  1 x string |
| ISO               |  1 x string |
| AXI (opt.)        |  3 x number |
| CIR (opt.)        |  3 x number |
| RAD (opt.)        |  3 x number |
| STRENGTH (opt.)   |  1 x number |
+-----------------+-------------+

.. _solidshw6:

SOLIDSHW6
~~~~~~~~~

*Shapes:*

- :ref:`WEDGE6 <wedge6>` (6 nodes)

Number of parameters: 9
no parameter,,MAT,KINEM,EAS,AXI,CIR,OPTORDER,RAD
**Parameters**

+-----------------+-------------+
| Parameter         |             |
+=================+=============+
|  (opt.)           | None        |
| MAT               |  1 x number |
| KINEM             |  1 x string |
| EAS               |  1 x string |
| AXI (opt.)        |  3 x number |
| CIR (opt.)        |  3 x number |
| OPTORDER (opt.)   | None        |
| RAD (opt.)        |  3 x number |
+-----------------+-------------+

.. _solidt10:

SOLIDT10
~~~~~~~~

*Shapes:*

- :ref:`TET10 <tet10>` (10 nodes)

Number of parameters: 14
no parameter,,MAT,KINEM,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,STRENGTH,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| STRENGTH (opt.)     |  1 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidt4:

SOLIDT4
~~~~~~~

*Shapes:*

- :ref:`TET4 <tet4>` (4 nodes)

Number of parameters: 13
no parameter,,MAT,KINEM,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidt4plast:

SOLIDT4PLAST
~~~~~~~~~~~~

*Shapes:*

- :ref:`TET4 <tet4>` (4 nodes)
- :ref:`TET4 <tet4>` (4 nodes)

Number of parameters: 13
no parameter,,MAT,KINEM,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidt4poro:

SOLIDT4PORO
~~~~~~~~~~~

*Shapes:*

- :ref:`TET4 <tet4>` (4 nodes)

Number of parameters: 13
no parameter,,MAT,KINEM,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidt4thermo:

SOLIDT4THERMO
~~~~~~~~~~~~~

*Shapes:*

- :ref:`TET4 <tet4>` (4 nodes)

Number of parameters: 13
no parameter,,MAT,KINEM,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidt10thermo:

SOLIDT10THERMO
~~~~~~~~~~~~~~

*Shapes:*

- :ref:`TET10 <tet10>` (10 nodes)

Number of parameters: 14
no parameter,,MAT,KINEM,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,STRENGTH,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| STRENGTH (opt.)     |  1 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidt4scatra:

SOLIDT4SCATRA
~~~~~~~~~~~~~

*Shapes:*

- :ref:`TET4 <tet4>` (4 nodes)

Number of parameters: 14
no parameter,,MAT,KINEM,TYPE,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| TYPE                |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidt10scatra:

SOLIDT10SCATRA
~~~~~~~~~~~~~~

*Shapes:*

- :ref:`TET10 <tet10>` (10 nodes)

Number of parameters: 15
no parameter,,MAT,KINEM,TYPE,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,STRENGTH,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| TYPE                |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| STRENGTH (opt.)     |  1 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidw6:

SOLIDW6
~~~~~~~

*Shapes:*

- :ref:`WEDGE6 <wedge6>` (6 nodes)

Number of parameters: 13
no parameter,,MAT,KINEM,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidp5:

SOLIDP5
~~~~~~~

*Shapes:*

- :ref:`PYRAMID5 <pyramid5>` (5 nodes)

Number of parameters: 14
no parameter,,MAT,KINEM,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,STRENGTH,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| STRENGTH (opt.)     |  1 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidp5fbar:

SOLIDP5FBAR
~~~~~~~~~~~

*Shapes:*

- :ref:`PYRAMID5 <pyramid5>` (5 nodes)

Number of parameters: 13
no parameter,,MAT,KINEM,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _solidw6scatra:

SOLIDW6SCATRA
~~~~~~~~~~~~~

*Shapes:*

- :ref:`WEDGE6 <wedge6>` (6 nodes)

Number of parameters: 14
no parameter,,MAT,KINEM,TYPE,AXI,CIR,FIBER1,FIBER2,FIBER3,GROWTHTRIG,HU,RAD,lambda
**Parameters**

+-------------------+-------------+
| Parameter           |             |
+===================+=============+
|  (opt.)             | None        |
| MAT                 |  1 x number |
| KINEM               |  1 x string |
| TYPE                |  1 x string |
| AXI (opt.)          |  3 x number |
| CIR (opt.)          |  3 x number |
| FIBER1 (opt.)       |  3 x number |
| FIBER2 (opt.)       |  3 x number |
| FIBER3 (opt.)       |  3 x number |
| GROWTHTRIG (opt.)   |  1 x number |
| HU (opt.)           |  1 x number |
| RAD (opt.)          |  3 x number |
| lambda (opt.)       |  1 x number |
+-------------------+-------------+

.. _torsion3:

TORSION3
~~~~~~~~

*Shapes:*

- :ref:`LINE3 <line3>` (3 nodes)

Number of parameters: 4
no parameter,,MAT,BENDINGPOTENTIAL
**Parameters**

+------------------+-------------+
| Parameter          |             |
+==================+=============+
|                    | None        |
| MAT                |  1 x number |
| BENDINGPOTENTIAL   |  1 x string |
+------------------+-------------+

.. _truss3:

TRUSS3
~~~~~~

*Shapes:*

- :ref:`LINE2 <line2>` (2 nodes)

Number of parameters: 5
no parameter,,MAT,CROSS,KINEM
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
| CROSS          |  1 x number |
| KINEM          |  1 x string |
+--------------+-------------+

.. _wall:

WALL
~~~~

*Shapes:*

- :ref:`NURBS4 <nurbs4>` (4 nodes)
- :ref:`NURBS9 <nurbs9>` (9 nodes)
- :ref:`QUAD4 <quad4>` (4 nodes)
- :ref:`QUAD8 <quad8>` (8 nodes)
- :ref:`QUAD9 <quad9>` (9 nodes)
- :ref:`TRI3 <tri3>` (3 nodes)
- :ref:`TRI6 <tri6>` (6 nodes)

Number of parameters: 8
no parameter,,MAT,KINEM,EAS,THICK,STRESS_STRAIN,GP
**Parameters**

+---------------+-------------+
| Parameter       |             |
+===============+=============+
|                 | None        |
| MAT             |  1 x number |
| KINEM           |  1 x string |
| EAS             |  1 x string |
| THICK           |  1 x number |
| STRESS_STRAIN   |  1 x string |
| GP              |  2 x number |
+---------------+-------------+

.. _wallscatra:

WALLSCATRA
~~~~~~~~~~

*Shapes:*

- :ref:`NURBS4 <nurbs4>` (4 nodes)
- :ref:`NURBS9 <nurbs9>` (9 nodes)
- :ref:`QUAD4 <quad4>` (4 nodes)
- :ref:`QUAD8 <quad8>` (8 nodes)
- :ref:`QUAD9 <quad9>` (9 nodes)
- :ref:`TRI3 <tri3>` (3 nodes)
- :ref:`TRI6 <tri6>` (6 nodes)

Number of parameters: 9
no parameter,,MAT,KINEM,EAS,THICK,STRESS_STRAIN,GP,TYPE
**Parameters**

+---------------+-------------+
| Parameter       |             |
+===============+=============+
|                 | None        |
| MAT             |  1 x number |
| KINEM           |  1 x string |
| EAS             |  1 x string |
| THICK           |  1 x number |
| STRESS_STRAIN   |  1 x string |
| GP              |  2 x number |
| TYPE            |  1 x string |
+---------------+-------------+

.. _wallq4poro:

WALLQ4PORO
~~~~~~~~~~

*Shapes:*

- :ref:`QUAD4 <quad4>` (4 nodes)

Number of parameters: 8
no parameter,,MAT,KINEM,EAS,THICK,STRESS_STRAIN,GP
**Parameters**

+---------------+-------------+
| Parameter       |             |
+===============+=============+
|                 | None        |
| MAT             |  1 x number |
| KINEM           |  1 x string |
| EAS             |  1 x string |
| THICK           |  1 x number |
| STRESS_STRAIN   |  1 x string |
| GP              |  2 x number |
+---------------+-------------+

.. _wallq4poroscatra:

WALLQ4POROSCATRA
~~~~~~~~~~~~~~~~

*Shapes:*

- :ref:`QUAD4 <quad4>` (4 nodes)

Number of parameters: 9
no parameter,,MAT,KINEM,EAS,THICK,STRESS_STRAIN,GP,TYPE
**Parameters**

+---------------+-------------+
| Parameter       |             |
+===============+=============+
|                 | None        |
| MAT             |  1 x number |
| KINEM           |  1 x string |
| EAS             |  1 x string |
| THICK           |  1 x number |
| STRESS_STRAIN   |  1 x string |
| GP              |  2 x number |
| TYPE            |  1 x string |
+---------------+-------------+

.. _wallq4porop1:

WALLQ4POROP1
~~~~~~~~~~~~

*Shapes:*

- :ref:`QUAD4 <quad4>` (4 nodes)

Number of parameters: 8
no parameter,,MAT,KINEM,EAS,THICK,STRESS_STRAIN,GP
**Parameters**

+---------------+-------------+
| Parameter       |             |
+===============+=============+
|                 | None        |
| MAT             |  1 x number |
| KINEM           |  1 x string |
| EAS             |  1 x string |
| THICK           |  1 x number |
| STRESS_STRAIN   |  1 x string |
| GP              |  2 x number |
+---------------+-------------+

.. _wallq4porop1scatra:

WALLQ4POROP1SCATRA
~~~~~~~~~~~~~~~~~~

*Shapes:*

- :ref:`QUAD4 <quad4>` (4 nodes)

Number of parameters: 9
no parameter,,MAT,KINEM,EAS,THICK,STRESS_STRAIN,GP,TYPE
**Parameters**

+---------------+-------------+
| Parameter       |             |
+===============+=============+
|                 | None        |
| MAT             |  1 x number |
| KINEM           |  1 x string |
| EAS             |  1 x string |
| THICK           |  1 x number |
| STRESS_STRAIN   |  1 x string |
| GP              |  2 x number |
| TYPE            |  1 x string |
+---------------+-------------+

.. _wallq9poro:

WALLQ9PORO
~~~~~~~~~~

*Shapes:*

- :ref:`QUAD9 <quad9>` (9 nodes)

Number of parameters: 8
no parameter,,MAT,KINEM,EAS,THICK,STRESS_STRAIN,GP
**Parameters**

+---------------+-------------+
| Parameter       |             |
+===============+=============+
|                 | None        |
| MAT             |  1 x number |
| KINEM           |  1 x string |
| EAS             |  1 x string |
| THICK           |  1 x number |
| STRESS_STRAIN   |  1 x string |
| GP              |  2 x number |
+---------------+-------------+

.. _fluidelements:

FLUID ELEMENTS
--------------

.. _fluid:

FLUID
~~~~~

*Shapes:*

- :ref:`HEX20 <hex20>` (20 nodes)
- :ref:`HEX27 <hex27>` (27 nodes)
- :ref:`HEX8 <hex8>` (8 nodes)
- :ref:`NURBS27 <nurbs27>` (27 nodes)
- :ref:`NURBS4 <nurbs4>` (4 nodes)
- :ref:`NURBS8 <nurbs8>` (8 nodes)
- :ref:`NURBS9 <nurbs9>` (9 nodes)
- :ref:`PYRAMID5 <pyramid5>` (5 nodes)
- :ref:`QUAD4 <quad4>` (4 nodes)
- :ref:`QUAD8 <quad8>` (8 nodes)
- :ref:`QUAD9 <quad9>` (9 nodes)
- :ref:`TET10 <tet10>` (10 nodes)
- :ref:`TET4 <tet4>` (4 nodes)
- :ref:`TRI3 <tri3>` (3 nodes)
- :ref:`TRI6 <tri6>` (6 nodes)
- :ref:`WEDGE15 <wedge15>` (15 nodes)
- :ref:`WEDGE6 <wedge6>` (6 nodes)

Number of parameters: 4
no parameter,,MAT,NA
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
| NA             |  1 x string |
+--------------+-------------+

.. _fluidxw:

FLUIDXW
~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)
- :ref:`TET4 <tet4>` (4 nodes)

Number of parameters: 4
no parameter,,MAT,NA
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
| NA             |  1 x string |
+--------------+-------------+

.. _fluidhdg:

FLUIDHDG
~~~~~~~~

*Shapes:*

- :ref:`HEX20 <hex20>` (20 nodes)
- :ref:`HEX27 <hex27>` (27 nodes)
- :ref:`HEX8 <hex8>` (8 nodes)
- :ref:`NURBS27 <nurbs27>` (27 nodes)
- :ref:`NURBS4 <nurbs4>` (4 nodes)
- :ref:`NURBS8 <nurbs8>` (8 nodes)
- :ref:`NURBS9 <nurbs9>` (9 nodes)
- :ref:`PYRAMID5 <pyramid5>` (5 nodes)
- :ref:`QUAD4 <quad4>` (4 nodes)
- :ref:`QUAD8 <quad8>` (8 nodes)
- :ref:`QUAD9 <quad9>` (9 nodes)
- :ref:`TET10 <tet10>` (10 nodes)
- :ref:`TET4 <tet4>` (4 nodes)
- :ref:`TRI3 <tri3>` (3 nodes)
- :ref:`TRI6 <tri6>` (6 nodes)
- :ref:`WEDGE15 <wedge15>` (15 nodes)
- :ref:`WEDGE6 <wedge6>` (6 nodes)

Number of parameters: 6
no parameter,,MAT,NA,DEG,SPC
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
| NA             |  1 x string |
| DEG            |  1 x number |
| SPC (opt.)     |  1 x number |
+--------------+-------------+

.. _fluidhdgweakcomp:

FLUIDHDGWEAKCOMP
~~~~~~~~~~~~~~~~

*Shapes:*

- :ref:`HEX20 <hex20>` (20 nodes)
- :ref:`HEX27 <hex27>` (27 nodes)
- :ref:`HEX8 <hex8>` (8 nodes)
- :ref:`NURBS27 <nurbs27>` (27 nodes)
- :ref:`NURBS4 <nurbs4>` (4 nodes)
- :ref:`NURBS8 <nurbs8>` (8 nodes)
- :ref:`NURBS9 <nurbs9>` (9 nodes)
- :ref:`PYRAMID5 <pyramid5>` (5 nodes)
- :ref:`QUAD4 <quad4>` (4 nodes)
- :ref:`QUAD8 <quad8>` (8 nodes)
- :ref:`QUAD9 <quad9>` (9 nodes)
- :ref:`TET10 <tet10>` (10 nodes)
- :ref:`TET4 <tet4>` (4 nodes)
- :ref:`TRI3 <tri3>` (3 nodes)
- :ref:`TRI6 <tri6>` (6 nodes)
- :ref:`WEDGE15 <wedge15>` (15 nodes)
- :ref:`WEDGE6 <wedge6>` (6 nodes)

Number of parameters: 6
no parameter,,MAT,NA,DEG,SPC
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
| NA             |  1 x string |
| DEG            |  1 x number |
| SPC (opt.)     |  1 x number |
+--------------+-------------+

.. _fluidimmersed:

FLUIDIMMERSED
~~~~~~~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)

Number of parameters: 4
no parameter,,MAT,NA
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
| NA             |  1 x string |
+--------------+-------------+

.. _fluidporoimmersed:

FLUIDPOROIMMERSED
~~~~~~~~~~~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)

Number of parameters: 4
no parameter,,MAT,NA
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
| NA             |  1 x string |
+--------------+-------------+

.. _lubricationelements:

LUBRICATION ELEMENTS
--------------------

.. _lubrication:

LUBRICATION
~~~~~~~~~~~

*Shapes:*

- :ref:`LINE2 <line2>` (2 nodes)
- :ref:`LINE3 <line3>` (3 nodes)
- :ref:`QUAD4 <quad4>` (4 nodes)
- :ref:`QUAD8 <quad8>` (8 nodes)
- :ref:`QUAD9 <quad9>` (9 nodes)
- :ref:`TRI3 <tri3>` (3 nodes)
- :ref:`TRI6 <tri6>` (6 nodes)

Number of parameters: 3
no parameter,,MAT
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
+--------------+-------------+

.. _transportelements:

TRANSPORT ELEMENTS
------------------

.. _transp:

TRANSP
~~~~~~

*Shapes:*

- :ref:`HEX20 <hex20>` (20 nodes)
- :ref:`HEX27 <hex27>` (27 nodes)
- :ref:`HEX8 <hex8>` (8 nodes)
- :ref:`LINE2 <line2>` (2 nodes)
- :ref:`LINE3 <line3>` (3 nodes)
- :ref:`NURBS2 <nurbs2>` (2 nodes)
- :ref:`NURBS27 <nurbs27>` (27 nodes)
- :ref:`NURBS3 <nurbs3>` (3 nodes)
- :ref:`NURBS4 <nurbs4>` (4 nodes)
- :ref:`NURBS8 <nurbs8>` (8 nodes)
- :ref:`NURBS9 <nurbs9>` (9 nodes)
- :ref:`PYRAMID5 <pyramid5>` (5 nodes)
- :ref:`QUAD4 <quad4>` (4 nodes)
- :ref:`QUAD8 <quad8>` (8 nodes)
- :ref:`QUAD9 <quad9>` (9 nodes)
- :ref:`TET10 <tet10>` (10 nodes)
- :ref:`TET4 <tet4>` (4 nodes)
- :ref:`TRI3 <tri3>` (3 nodes)
- :ref:`TRI6 <tri6>` (6 nodes)
- :ref:`WEDGE15 <wedge15>` (15 nodes)
- :ref:`WEDGE6 <wedge6>` (6 nodes)

Number of parameters: 5
no parameter,,MAT,TYPE,FIBER1
**Parameters**

+---------------+-------------+
| Parameter       |             |
+===============+=============+
|  (opt.)         | None        |
| MAT             |  1 x number |
| TYPE            |  1 x string |
| FIBER1 (opt.)   |  3 x number |
+---------------+-------------+

.. _transport2elements:

TRANSPORT2 ELEMENTS
-------------------

.. _transp:

TRANSP
~~~~~~

*Shapes:*

- :ref:`HEX20 <hex20>` (20 nodes)
- :ref:`HEX27 <hex27>` (27 nodes)
- :ref:`HEX8 <hex8>` (8 nodes)
- :ref:`LINE2 <line2>` (2 nodes)
- :ref:`LINE3 <line3>` (3 nodes)
- :ref:`NURBS2 <nurbs2>` (2 nodes)
- :ref:`NURBS27 <nurbs27>` (27 nodes)
- :ref:`NURBS3 <nurbs3>` (3 nodes)
- :ref:`NURBS4 <nurbs4>` (4 nodes)
- :ref:`NURBS8 <nurbs8>` (8 nodes)
- :ref:`NURBS9 <nurbs9>` (9 nodes)
- :ref:`PYRAMID5 <pyramid5>` (5 nodes)
- :ref:`QUAD4 <quad4>` (4 nodes)
- :ref:`QUAD8 <quad8>` (8 nodes)
- :ref:`QUAD9 <quad9>` (9 nodes)
- :ref:`TET10 <tet10>` (10 nodes)
- :ref:`TET4 <tet4>` (4 nodes)
- :ref:`TRI3 <tri3>` (3 nodes)
- :ref:`TRI6 <tri6>` (6 nodes)
- :ref:`WEDGE15 <wedge15>` (15 nodes)
- :ref:`WEDGE6 <wedge6>` (6 nodes)

Number of parameters: 5
no parameter,,MAT,TYPE,FIBER1
**Parameters**

+---------------+-------------+
| Parameter       |             |
+===============+=============+
|  (opt.)         | None        |
| MAT             |  1 x number |
| TYPE            |  1 x string |
| FIBER1 (opt.)   |  3 x number |
+---------------+-------------+

.. _aleelements:

ALE ELEMENTS
------------

.. _ale2:

ALE2
~~~~

*Shapes:*

- :ref:`QUAD4 <quad4>` (4 nodes)
- :ref:`QUAD8 <quad8>` (8 nodes)
- :ref:`QUAD9 <quad9>` (9 nodes)
- :ref:`TRI3 <tri3>` (3 nodes)
- :ref:`TRI6 <tri6>` (6 nodes)

Number of parameters: 3
no parameter,,MAT
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
+--------------+-------------+

.. _ale3:

ALE3
~~~~

*Shapes:*

- :ref:`HEX20 <hex20>` (20 nodes)
- :ref:`HEX27 <hex27>` (27 nodes)
- :ref:`HEX8 <hex8>` (8 nodes)
- :ref:`PYRAMID5 <pyramid5>` (5 nodes)
- :ref:`TET10 <tet10>` (10 nodes)
- :ref:`TET4 <tet4>` (4 nodes)
- :ref:`WEDGE15 <wedge15>` (15 nodes)
- :ref:`WEDGE6 <wedge6>` (6 nodes)

Number of parameters: 3
no parameter,,MAT
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
+--------------+-------------+

.. _thermoelements:

THERMO ELEMENTS
---------------

.. _thermo:

THERMO
~~~~~~

*Shapes:*

- :ref:`HEX20 <hex20>` (20 nodes)
- :ref:`HEX27 <hex27>` (27 nodes)
- :ref:`HEX8 <hex8>` (8 nodes)
- :ref:`LINE2 <line2>` (2 nodes)
- :ref:`LINE3 <line3>` (3 nodes)
- :ref:`NURBS27 <nurbs27>` (27 nodes)
- :ref:`NURBS4 <nurbs4>` (4 nodes)
- :ref:`NURBS9 <nurbs9>` (9 nodes)
- :ref:`PYRAMID5 <pyramid5>` (5 nodes)
- :ref:`QUAD4 <quad4>` (4 nodes)
- :ref:`QUAD8 <quad8>` (8 nodes)
- :ref:`QUAD9 <quad9>` (9 nodes)
- :ref:`TET10 <tet10>` (10 nodes)
- :ref:`TET4 <tet4>` (4 nodes)
- :ref:`TRI3 <tri3>` (3 nodes)
- :ref:`TRI6 <tri6>` (6 nodes)
- :ref:`WEDGE15 <wedge15>` (15 nodes)
- :ref:`WEDGE6 <wedge6>` (6 nodes)

Number of parameters: 3
no parameter,,MAT
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
+--------------+-------------+

.. _arteryelements:

ARTERY ELEMENTS
---------------

.. _art:

ART
~~~

*Shapes:*

- :ref:`LINE2 <line2>` (2 nodes)

Number of parameters: 6
no parameter,,MAT,GP,TYPE,DIAM
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
| GP             |  1 x number |
| TYPE           |  1 x string |
| DIAM           |  1 x number |
+--------------+-------------+

.. _reduced d airwayselements:

REDUCED D AIRWAYS ELEMENTS
--------------------------

.. _red_airway:

RED_AIRWAY
~~~~~~~~~~

*Shapes:*

- :ref:`LINE2 <line2>` (2 nodes)

Number of parameters: 21
no parameter,,MAT,ElemSolvingType,TYPE,Resistance,PowerOfVelocityProfile,WallElasticity,PoissonsRatio,ViscousTs,ViscousPhaseShift,WallThickness,Area,Generation,AirwayColl,BranchLength,Open_Init,Pcrit_Close,Pcrit_Open,S_Close,S_Open
**Parameters**

+------------------------+-------------+
| Parameter                |             |
+========================+=============+
|                          | None        |
| MAT                      |  1 x number |
| ElemSolvingType          |  1 x string |
| TYPE                     |  1 x string |
| Resistance               |  1 x string |
| PowerOfVelocityProfile   |  1 x number |
| WallElasticity           |  1 x number |
| PoissonsRatio            |  1 x number |
| ViscousTs                |  1 x number |
| ViscousPhaseShift        |  1 x number |
| WallThickness            |  1 x number |
| Area                     |  1 x number |
| Generation               |  1 x number |
| AirwayColl (opt.)        |  1 x number |
| BranchLength (opt.)      |  1 x number |
| Open_Init (opt.)         |  1 x number |
| Pcrit_Close (opt.)       |  1 x number |
| Pcrit_Open (opt.)        |  1 x number |
| S_Close (opt.)           |  1 x number |
| S_Open (opt.)            |  1 x number |
+------------------------+-------------+

.. _red_acinus:

RED_ACINUS
~~~~~~~~~~

*Shapes:*

- :ref:`LINE2 <line2>` (2 nodes)

Number of parameters: 21
no parameter,,MAT,TYPE,AcinusVolume,AlveolarDuctVolume,Area,BETA,E1_0,E1_01,E1_02,E1_EXP,E1_EXP1,E1_EXP2,E1_LIN,E1_LIN1,E1_LIN2,KAPPA,TAU,TAU1,TAU2
**Parameters**

+--------------------+-------------+
| Parameter            |             |
+====================+=============+
|                      | None        |
| MAT                  |  1 x number |
| TYPE                 |  1 x string |
| AcinusVolume         |  1 x number |
| AlveolarDuctVolume   |  1 x number |
| Area (opt.)          |  1 x number |
| BETA (opt.)          |  1 x number |
| E1_0 (opt.)          |  1 x number |
| E1_01 (opt.)         |  1 x number |
| E1_02 (opt.)         |  1 x number |
| E1_EXP (opt.)        |  1 x number |
| E1_EXP1 (opt.)       |  1 x number |
| E1_EXP2 (opt.)       |  1 x number |
| E1_LIN (opt.)        |  1 x number |
| E1_LIN1 (opt.)       |  1 x number |
| E1_LIN2 (opt.)       |  1 x number |
| KAPPA (opt.)         |  1 x number |
| TAU (opt.)           |  1 x number |
| TAU1 (opt.)          |  1 x number |
| TAU2 (opt.)          |  1 x number |
+--------------------+-------------+

.. _red_acinar_inter_dep:

RED_ACINAR_INTER_DEP
~~~~~~~~~~~~~~~~~~~~

*Shapes:*

- :ref:`LINE2 <line2>` (2 nodes)

Number of parameters: 3
no parameter,,MAT
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
+--------------+-------------+

.. _acousticelements:

ACOUSTIC ELEMENTS
-----------------

.. _acoustic:

ACOUSTIC
~~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)
- :ref:`QUAD4 <quad4>` (4 nodes)
- :ref:`QUAD9 <quad9>` (9 nodes)
- :ref:`TET4 <tet4>` (4 nodes)
- :ref:`TRI3 <tri3>` (3 nodes)

Number of parameters: 5
no parameter,,MAT,DEG,SPC
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
| DEG            |  1 x number |
| SPC            |  1 x number |
+--------------+-------------+

.. _acousticsol:

ACOUSTICSOL
~~~~~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)
- :ref:`QUAD4 <quad4>` (4 nodes)
- :ref:`QUAD9 <quad9>` (9 nodes)
- :ref:`TET4 <tet4>` (4 nodes)
- :ref:`TRI3 <tri3>` (3 nodes)

Number of parameters: 5
no parameter,,MAT,DEG,SPC
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
| DEG            |  1 x number |
| SPC            |  1 x number |
+--------------+-------------+

.. _electromagneticelements:

ELECTROMAGNETIC ELEMENTS
------------------------

.. _electromagnetic:

ELECTROMAGNETIC
~~~~~~~~~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)
- :ref:`QUAD4 <quad4>` (4 nodes)
- :ref:`QUAD9 <quad9>` (9 nodes)
- :ref:`TET4 <tet4>` (4 nodes)
- :ref:`TRI3 <tri3>` (3 nodes)

Number of parameters: 5
no parameter,,MAT,DEG,SPC
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
| DEG            |  1 x number |
| SPC            |  1 x number |
+--------------+-------------+

.. _electromagneticdiff:

ELECTROMAGNETICDIFF
~~~~~~~~~~~~~~~~~~~

*Shapes:*

- :ref:`HEX8 <hex8>` (8 nodes)
- :ref:`QUAD4 <quad4>` (4 nodes)
- :ref:`QUAD9 <quad9>` (9 nodes)
- :ref:`TET4 <tet4>` (4 nodes)
- :ref:`TRI3 <tri3>` (3 nodes)

Number of parameters: 5
no parameter,,MAT,DEG,SPC
**Parameters**

+--------------+-------------+
| Parameter      |             |
+==============+=============+
|                | None        |
| MAT            |  1 x number |
| DEG            |  1 x number |
| SPC            |  1 x number |
+--------------+-------------+

