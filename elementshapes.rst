.. _elementshapes:

Element shapes
--------------

.. _line2:

LINE2
~~~~~

- Dimension: xD
- Nodes: 2
- Integration points: 0

.. _line3:

LINE3
~~~~~

- Dimension: xD
- Nodes: 3
- Integration points: 0

.. _line4:

LINE4
~~~~~

- Dimension: xD
- Nodes: 4
- Integration points: 0

.. _line5:

LINE5
~~~~~

- Dimension: xD
- Nodes: 5
- Integration points: 0

.. _line6:

LINE6
~~~~~

- Dimension: xD
- Nodes: 6
- Integration points: 0

.. _herm2line2:

HERM2LINE2
~~~~~~~~~~

- Dimension: xD
- Nodes: 2
- Integration points: 0

.. _herm2line3:

HERM2LINE3
~~~~~~~~~~

- Dimension: xD
- Nodes: 3
- Integration points: 0

.. _herm2line4:

HERM2LINE4
~~~~~~~~~~

- Dimension: xD
- Nodes: 4
- Integration points: 0

.. _herm2line5:

HERM2LINE5
~~~~~~~~~~

- Dimension: xD
- Nodes: 5
- Integration points: 0

.. _tri3:

TRI3
~~~~

- Dimension: xD
- Nodes: 3
- Integration points: 0

.. _point1:

POINT1
~~~~~~

- Dimension: xD
- Nodes: 1
- Integration points: 0

.. _tet4:

TET4
~~~~

- Dimension: xD
- Nodes: 4
- Integration points: 0

.. _quad4:

QUAD4
~~~~~

- Dimension: xD
- Nodes: 4
- Integration points: 0

.. _quad8:

QUAD8
~~~~~

- Dimension: xD
- Nodes: 8
- Integration points: 0

.. _quad9:

QUAD9
~~~~~

- Dimension: xD
- Nodes: 9
- Integration points: 0

.. _tri6:

TRI6
~~~~

- Dimension: xD
- Nodes: 6
- Integration points: 0

.. _hex18:

HEX18
~~~~~

- Dimension: xD
- Nodes: 18
- Integration points: 0

.. _hex20:

HEX20
~~~~~

- Dimension: xD
- Nodes: 20
- Integration points: 0

.. _hex27:

HEX27
~~~~~

- Dimension: xD
- Nodes: 27
- Integration points: 0

.. _nurbs27:

NURBS27
~~~~~~~

- Dimension: xD
- Nodes: 27
- Integration points: 0

.. _hex8:

HEX8
~~~~

- Dimension: xD
- Nodes: 8
- Integration points: 0

.. _wedge6:

WEDGE6
~~~~~~

- Dimension: xD
- Nodes: 6
- Integration points: 0

.. _tet10:

TET10
~~~~~

- Dimension: xD
- Nodes: 10
- Integration points: 0

.. _pyramid5:

PYRAMID5
~~~~~~~~

- Dimension: xD
- Nodes: 5
- Integration points: 0

.. _nurbs4:

NURBS4
~~~~~~

- Dimension: xD
- Nodes: 4
- Integration points: 0

.. _nurbs9:

NURBS9
~~~~~~

- Dimension: xD
- Nodes: 9
- Integration points: 0

.. _nurbs8:

NURBS8
~~~~~~

- Dimension: xD
- Nodes: 8
- Integration points: 0

.. _wedge15:

WEDGE15
~~~~~~~

- Dimension: xD
- Nodes: 15
- Integration points: 0

.. _nurbs2:

NURBS2
~~~~~~

- Dimension: xD
- Nodes: 2
- Integration points: 0

.. _nurbs3:

NURBS3
~~~~~~

- Dimension: xD
- Nodes: 3
- Integration points: 0

