Tests for different parameter presentations
===============================================

Version 1: Extra table for all parameters
-----------------------------------------

.. csv-table:: 
    :header-rows: 1
    :widths: 20,10,10,50

    "Parameter", "type", "default", "description"
    "``RESTART``", "int",  "0", "None"


.. csv-table:: 
    :header-rows: 1
    :widths: 20,10,10,50

    "Parameter", "type", "default", "description"
    "``RESTARTTIME``", "int, double", "-1", "Used defined restart time"

.. csv-table::
    :header-rows: 1
    :widths: 20,10,10,50

    "Parameter", "type", "default", "description"
    "``RANDSEED``", "int", "-1", "Set the random seed. If < 0 use current time."

.. csv-table::
    :header-rows: 1
    :widths: 20,10,10,50

    "Parameter", "type", "default", "description"
    "``SHAPEFCT``","string", "Polynomial", "Defines the function spaces for the spatial approximation"

**Possible values for this parameter:** 

    HDG, Nurbs, Polynomial

.. csv-table::
    :header-rows: 1
    :widths: 20,10,10,50

    "Parameter", "type", "default", "description"
    "``PROBLEMTYP``","string", "Fluid_Structure_Interaction","This is the main parameter |break| defining the general procedure to be used"

**Possible values for this parameter:**

    Ale |break| ArterialNetwork |break| Atherosclerosis_Fluid_Structure_Interaction |break| Biofilm_Fluid_Structure_Interaction |break| Cardiac_Monodomain |break| Elastohydrodynamic_Lubrication |break| Electrochemistry |break| Electromagnetics |break| Fluid |break| Fluid_Ale |break| Fluid_Beam_Interaction |break| Fluid_Freesurface |break| Fluid_Poro_Structure_Interaction_XFEM |break| Fluid_Porous_Structure_Interaction |break| Fluid_Porous_Structure_Scalar_Scalar_Interaction |break| Fluid_RedModels |break| Fluid_Structure_Interaction |break| Fluid_Structure_Interaction_Lung |break| Fluid_Structure_Interaction_RedModels |break| Fluid_Structure_Interaction_XFEM |break| Fluid_XFEM |break| Fluid_XFEM_LevelSet |break| Gas_Fluid_Structure_Interaction |break| Immersed_FSI |break| Inverse_Analysis |break| Level_Set |break| Low_Mach_Number_Flow |break| Lubrication |break| Multiphase_Poroelasticity |break| Multiphase_Poroelasticity_ScaTra |break| Multiphase_Porous_Flow |break| NP_Supporting_Procs |break| Particle |break| Particle_Structure_Interaction |break| Polymer_Network |break| Poroelastic_scalar_transport |break| Poroelasticity |break| RedAirways_Tissue |break| ReducedDimensionalAirWays |break| Scalar_Thermo_Interaction |break| Scalar_Transport |break| Structure |break| Structure_Ale |break| Structure_Scalar_Interaction |break| Structure_Scalar_Thermo_Interaction |break| Thermo |break| Thermo_Fluid_Structure_Interaction |break| Thermo_Structure_Interaction |break| Tutorial |break| Two_Phase_Flow

Version 2: One table for all parameters
----------------------------------------

.. csv-table::
    :header-rows: 1

    "Parameter", "type", "default", "description"
    "``RESTART``", "int",  "0", ""
    "``RESTARTTIME``", "int, double", "-1", "Used defined restart time"
    "``RANDSEED``", "int", "-1", "Set the random seed. If < 0 use current time."
    "``SHAPEFCT``","string", "Polynomial", "Defines the function spaces for the spatial approximation"
    "``PROBLEMTYP``","Ale |break| ArterialNetwork |break| Atherosclerosis_Fluid_Structure_Interaction |break| Biofilm_Fluid_Structure_Interaction |break| Cardiac_Monodomain |break| Elastohydrodynamic_Lubrication |break| Electrochemistry |break| Electromagnetics |break| Fluid |break| Fluid_Ale |break| Fluid_Beam_Interaction |break| Fluid_Freesurface |break| Fluid_Poro_Structure_Interaction_XFEM |break| Fluid_Porous_Structure_Interaction |break| Fluid_Porous_Structure_Scalar_Scalar_Interaction |break| Fluid_RedModels |break| Fluid_Structure_Interaction |break| Fluid_Structure_Interaction_Lung |break| Fluid_Structure_Interaction_RedModels |break| Fluid_Structure_Interaction_XFEM |break| Fluid_XFEM |break| Fluid_XFEM_LevelSet |break| Gas_Fluid_Structure_Interaction |break| Immersed_FSI |break| Inverse_Analysis |break| Level_Set |break| Low_Mach_Number_Flow |break| Lubrication |break| Multiphase_Poroelasticity |break| Multiphase_Poroelasticity_ScaTra |break| Multiphase_Porous_Flow |break| NP_Supporting_Procs |break| Particle |break| Particle_Structure_Interaction |break| Polymer_Network |break| Poroelastic_scalar_transport |break| Poroelasticity |break| RedAirways_Tissue |break| ReducedDimensionalAirWays |break| Scalar_Thermo_Interaction |break| Scalar_Transport |break| Structure |break| Structure_Ale |break| Structure_Scalar_Interaction |break| Structure_Scalar_Thermo_Interaction |break| Thermo |break| Thermo_Fluid_Structure_Interaction |break| Thermo_Structure_Interaction |break| Tutorial |break| Two_Phase_Flow |break| ", "Fluid_Structure_Interaction","This is the main parameter defining the general procedure to be used"


Version 3: No table
----------------------

``RESTART`` (int) - default: 0 |break| no description

``RESTARTTIME`` (int, double) - default: -1 |break| Used defined restart time

``RANDSEED`` (int) - default: -1 |break| Set the random seed. If < 0 use current time.

``PROBLEMTYP`` (string) |break| This is the main parameter defining the general procedure to be used

   **Possible values** (*default:* Fluid_Structure_Interaction):

   - Ale
   - ArterialNetwork
   - Atherosclerosis_Fluid_Structure_Interaction
   - Biofilm_Fluid_Structure_Interaction
   - Cardiac_Monodomain
   - Elastohydrodynamic_Lubrication
   - Electrochemistry
   - Electromagnetics
   - Fluid
   - Fluid_Ale
   - Fluid_Beam_Interaction
   - Fluid_Freesurface
   - Fluid_Poro_Structure_Interaction_XFEM
   - Fluid_Porous_Structure_Interaction
   - Fluid_Porous_Structure_Scalar_Scalar_Interaction
   - Fluid_RedModels
   - Fluid_Structure_Interaction
   - Fluid_Structure_Interaction_Lung
   - Fluid_Structure_Interaction_RedModels
   - Fluid_Structure_Interaction_XFEM
   - Fluid_XFEM
   - Fluid_XFEM_LevelSet
   - Gas_Fluid_Structure_Interaction
   - Immersed_FSI
   - Inverse_Analysis
   - Level_Set
   - Low_Mach_Number_Flow
   - Lubrication
   - Multiphase_Poroelasticity
   - Thermo_Structure_Interaction
   - Tutorial
   - Two_Phase_Flow

   

``SHAPEFCT`` (string) |break| Defines the function spaces for the spatial approximation

   **Possible values** (*default:* Polynomial):

   - HDG
   - Nurbs
   - Polynomial

Version 4: No table
----------------------

**RESTART** (int) - default: 0 |break| no description

**RESTARTTIME** (int, double) - default: -1 |break| Used defined restart time

**RANDSEED** (int) - default: -1 |break| Set the random seed. If < 0 use current time.

**PROBLEMTYP** (string) |break| This is the main parameter defining the general procedure to be used

   **Possible values** (*default:* Fluid_Structure_Interaction):

   - Ale
   - ArterialNetwork
   - Atherosclerosis_Fluid_Structure_Interaction
   - Biofilm_Fluid_Structure_Interaction
   - Cardiac_Monodomain
   - Elastohydrodynamic_Lubrication
   - Electrochemistry
   - Electromagnetics
   - Fluid
   - Fluid_Ale
   - Fluid_Beam_Interaction
   - Fluid_Freesurface
   - Fluid_Poro_Structure_Interaction_XFEM
   - Fluid_Porous_Structure_Interaction
   - Fluid_Porous_Structure_Scalar_Scalar_Interaction
   - Scalar_Thermo_Interaction
   - Scalar_Transport
   - Structure
   - Structure_Ale
   - Structure_Scalar_Interaction
   - Structure_Scalar_Thermo_Interaction
   - Thermo
   - Thermo_Fluid_Structure_Interaction
   - Thermo_Structure_Interaction
   - Tutorial
   - Two_Phase_Flow

   

**SHAPEFCT** (string) |break| Defines the function spaces for the spatial approximation

   **Possible values** (*default:* Polynomial):

   - HDG
   - Nurbs
   - Polynomial



DESIGN POINT NEUMANN CONDITIONS
-------------------------------

Point Neumann

showing only string options
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

   ------------------------------------DESIGN POINT NEUMANN CONDITIONS
   DPOINT  0
   E num - NUMDOF 0  ONOFF 0  VAL 0.0  FUNCT none   <type> <surface> 

.. list-table::
   :header-rows: 1

   * - Parameter
     - Default value
     - Possible values for strings
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, increaseHydro_z, |break| pseudo_orthopressure, orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bottom

showing numbers as number types
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

   ------------------------------------DESIGN POINT NEUMANN CONDITIONS
   DPOINT  0
   E <int> - NUMDOF <int>  ONOFF <int vec>  VAL <real vec> \
     FUNCT <int vec> <string:type> <string:surface> 

If the number is preceeded by a separator, one may take the separator as a description of it.
**Problem here:** one cannot see how long a vector is, or how the length is affected.

.. list-table::
   :header-rows: 1

   * - Parameter
     - Default value
     - Possible values for strings
   * - E
     - 0
     - 
   * - NUMDOF
     - 0
     - 
   * - ONOFF
     - 0
     - 
   * - VAL
     - 0.0
     - 
   * - FUNCT
     - none
     - 
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, increaseHydro_z, |break| pseudo_orthopressure, orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bottom

showing numbers as number type with variable name
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

   ------------------------------------DESIGN POINT NEUMANN CONDITIONS
   DPOINT  0
   E <int:num> - NUMDOF <int:numdof> ONOFF <int vec:onoff> VAL <real vec:values> \
     FUNCT <int vec:functions> <string:type> <string:surface>

Again, if the number is preceeded by a separator, one may take the separator as a description of it.
In other cases, the variable name gives a hint on its meaning.
**Problem again:** one cannot see how long a vector is, or how the length is affected.

.. list-table::
   :header-rows: 1

   * - Parameter
     - Default value
     - Possible values for strings
   * - num
     - 0
     - 
   * - numdof
     - 0
     - 
   * - onoff
     - [0 ..0]
     - 
   * - values
     - [0.0 .. 0.0]
     - 
   * - functions
     - [none .. none]
     - 
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, increaseHydro_z, |break| pseudo_orthopressure, orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bottom


DESIGN NODE 1D ARTERY WINDKESSEL CONDITIONS
-------------------------------------------

Artery windkessel condition

showing only string options
~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

   ------------------------DESIGN NODE 1D ARTERY WINDKESSEL CONDITIONS
   DPOINT  0
   E num - <intigrationType> <windkesselType> 0.0 0.0 0.0 0.0 0.0   \ 
      none none none none none  

**Problem here:** Nobody knows what the various "0.0" and "none" mean, since there is no separator preceeded.

**String options:**

.. list-table::
   :widths: 10, 10, 40
   :header-rows: 1

   * - Parameter
     - Default
     - Admissible values
   * - <intigrationType>
     - ExplicitWindkessel
     - ExplicitWindkessel, ImpedaceWindkessel, 
   * - <windkesselType>
     - RCR
     - R, RC, RCR, RCRL, none, 


showing string options and number types/names
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

   ------------------------DESIGN NODE 1D ARTERY WINDKESSEL CONDITIONS
   DPOINT  0
   E num - <intigrationType> <windkesselType> <real vec:val> <int vec:curve>

**Problem here:** 
    val and curve are vectors with initial length 5 (fixed?). 
    Again, numbers are not preceeded by a separator.

**String options:**

.. list-table::
   :widths: 10, 10, 40
   :header-rows: 1

   * - Parameter
     - Default
     - Admissible values
   * - <intigrationType>
     - ExplicitWindkessel
     - ExplicitWindkessel, ImpedaceWindkessel, 
   * - <windkesselType>
     - RCR
     - R, RC, RCR, RCRL, none, 


DESIGN NODE Reduced D AIRWAYS PRESCRIBED CONDITIONS
---------------------------------------------------

Reduced d airway prescribed boundary condition

showing only string options 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

   ----------------DESIGN NODE Reduced D AIRWAYS PRESCRIBED CONDITIONS
   DPOINT  0
   E num - <boundarycond> 0.0 none none none

**String options:**

.. list-table::
   :widths: 10, 10, 40
   :header-rows: 1

   * - Parameter
     - Default
     - Admissible values
   * - <boundarycond>
     - flow
     - flow, pressure, switchFlowPressure, VolumeDependentPleuralPressure, 


showing string options and number types/names
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Reduced d airway prescribed boundary condition

::

   ----------------DESIGN NODE Reduced D AIRWAYS PRESCRIBED CONDITIONS
   DPOINT  0
   E num - <boundarycond> 0.0  <reL vec:val> <int vec:curve> <int vec:funct>

Problem is here: The curve is an int vector with initial length 2, 
while val and funct are vectors of initial length 1.

**String options:**

.. list-table::
   :widths: 10, 10, 40
   :header-rows: 1

   * - Parameter
     - Default
     - Admissible values
   * - <boundarycond>
     - flow
     - flow, pressure, switchFlowPressure, VolumeDependentPleuralPressure, 


Problematic boundary conditions
--------------------------------

- DESIGN NODE 1D ARTERY WINDKESSEL CONDITIONS
- DESIGN NODE Reduced D AIRWAYS PRESCRIBED CONDITIONS
- DESIGN LINE Reduced D AIRWAYS PRESCRIBED EXTERNAL PRESSURE CONDITIONS
- DESIGN NODE Reduced D AIRWAYS PRESCRIBED SCATRA CONDITIONS
- DESIGN LINE Reduced D AIRWAYS INITIAL SCATRA CONDITIONS
- DESIGN NODE Reduced D AIRWAYS VENTILATOR CONDITIONS
- DESIGN LINE REDUCED D AIRWAYS VOL DEPENDENT PLEURAL PRESSURE CONDITIONS
