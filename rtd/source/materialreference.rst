..
   Created using baci version (git SHA1):
   eeea9d37d6cc009fe38bd81cbc4579befe0c2b2e

.. _materialsreference:

Material reference
==================

::

   ----------------------------------------------------------MATERIALS

.. _MAT_fluid:

MAT_fluid
---------

Newtonian fluid

::

      MAT <matID>  MAT_fluid DYNVISCOSITY 0 DENSITY 0 GAMMA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - DYNVISCOSITY
     - 
     - dynamic viscosity
   * - DENSITY
     - 
     - spatial mass density
   * - GAMMA
     - yes
     - surface tension coefficient

.. _MAT_fluid_murnaghantait:

MAT_fluid_murnaghantait
-----------------------

Weakly compressible fluid according to Murnaghan-Tait

::

      MAT <matID>  MAT_fluid_murnaghantait DYNVISCOSITY 0 REFDENSITY 0 \
       REFPRESSURE 0 REFBULKMODULUS 0 MATPARAMETER 0 GAMMA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - DYNVISCOSITY
     - 
     - dynamic viscosity
   * - REFDENSITY
     - 
     - reference spatial mass density
   * - REFPRESSURE
     - 
     - reference pressure
   * - REFBULKMODULUS
     - 
     - reference bulk modulus
   * - MATPARAMETER
     - 
     - material parameter according to Murnaghan-Tait
   * - GAMMA
     - yes
     - surface tension coefficient

.. _MAT_fluid_linear_density_viscosity:

MAT_fluid_linear_density_viscosity
----------------------------------

Linear law (pressure-dependent) for the density and the viscosity

::

      MAT <matID>  MAT_fluid_linear_density_viscosity REFDENSITY 0 \
       REFVISCOSITY 0 REFPRESSURE 0 COEFFDENSITY 0 COEFFVISCOSITY 0 \
       GAMMA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - REFDENSITY
     - 
     - reference density
   * - REFVISCOSITY
     - 
     - reference viscosity
   * - REFPRESSURE
     - 
     - reference pressure
   * - COEFFDENSITY
     - 
     - density-pressure coefficient
   * - COEFFVISCOSITY
     - 
     - viscosity-pressure coefficient
   * - GAMMA
     - yes
     - surface tension coefficient

.. _MAT_fluid_weakly_compressible:

MAT_fluid_weakly_compressible
-----------------------------

Weakly compressible fluid

::

      MAT <matID>  MAT_fluid_weakly_compressible VISCOSITY 0 REFDENSITY 0 \
       REFPRESSURE 0 COMPRCOEFF 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - VISCOSITY
     - 
     - viscosity
   * - REFDENSITY
     - 
     - reference density
   * - REFPRESSURE
     - 
     - reference pressure
   * - COMPRCOEFF
     - 
     - compressibility coefficient

.. _MAT_carreauyasuda:

MAT_carreauyasuda
-----------------

fluid with non-linear viscosity according to Carreau-Yasuda

::

      MAT <matID>  MAT_carreauyasuda NU_0 0 NU_INF 0 LAMBDA 0 APARAM 0 \
       BPARAM 0 DENSITY 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - NU_0
     - 
     - zero-shear viscosity
   * - NU_INF
     - 
     - infinite-shear viscosity
   * - LAMBDA
     - 
     - characteristic time
   * - APARAM
     - 
     - constant parameter
   * - BPARAM
     - 
     - constant parameter
   * - DENSITY
     - 
     - density

.. _MAT_modpowerlaw:

MAT_modpowerlaw
---------------

fluid with nonlinear viscosity according to a modified power law

::

      MAT <matID>  MAT_modpowerlaw MCONS 0 DELTA 0 AEXP 0 DENSITY 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - MCONS
     - 
     - consistency
   * - DELTA
     - 
     - safety factor
   * - AEXP
     - 
     - exponent
   * - DENSITY
     - 
     - density

.. _MAT_herschelbulkley:

MAT_herschelbulkley
-------------------

fluid with non-linear viscosity according to Herschel-Bulkley

::

      MAT <matID>  MAT_herschelbulkley TAU_0 0 KFAC 0 NEXP 0 MEXP 0 \
       LOLIMSHEARRATE 0 UPLIMSHEARRATE 0 DENSITY 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - TAU_0
     - 
     - yield stress
   * - KFAC
     - 
     - constant factor
   * - NEXP
     - 
     - exponent
   * - MEXP
     - 
     - exponent
   * - LOLIMSHEARRATE
     - 
     - lower limit of shear rate
   * - UPLIMSHEARRATE
     - 
     - upper limit of shear rate
   * - DENSITY
     - 
     - density

.. _MAT_yoghurt:

MAT_yoghurt
-----------

yoghurt-type fluid with nonlinear viscosity

::

      MAT <matID>  MAT_yoghurt SHC 0 DENSITY 0 THERMCOND 0 STRAINRATEEXP 0 \
       PREEXCON 0 ACTENERGY 0 GASCON 0 DELTA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - SHC
     - 
     - specific heat capacity at constant pressure |break| 
       (J/(kg*K))
   * - DENSITY
     - 
     - density
   * - THERMCOND
     - 
     - thermal conductivity (J/(m*K*s))
   * - STRAINRATEEXP
     - 
     - exponent of strain-rate term
   * - PREEXCON
     - 
     - pre-exponential constant (1/s)
   * - ACTENERGY
     - 
     - activation energy (J/kg)
   * - GASCON
     - 
     - specific gas constant R (J/(kg*K))
   * - DELTA
     - 
     - safety factor

.. _MAT_permeable:

MAT_permeable
-------------

permeability for flow in porous media

::

      MAT <matID>  MAT_permeable TYPE Darcy-Stokes DYNVISCOSITY 0 \
       DENSITY 0 PERMEABILITY 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - TYPE
     - 
     - Problem type: Darcy, Darcy-Stokes (default)
   * - DYNVISCOSITY
     - 
     - dynamic viscosity
   * - DENSITY
     - 
     - density
   * - PERMEABILITY
     - 
     - permeability of medium

.. _MAT_lubrication:

MAT_lubrication
---------------

lubrication material

::

      MAT <matID>  MAT_lubrication LUBRICATIONLAWID 0 DENSITY 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - LUBRICATIONLAWID
     - 
     - lubrication law id
   * - DENSITY
     - 
     - lubricant density

.. _MAT_lubrication_law_constant:

MAT_lubrication_law_constant
----------------------------

constant lubrication material law

::

      MAT <matID>  MAT_lubrication_law_constant VISCOSITY 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - VISCOSITY
     - 
     - lubricant viscosity

.. _MAT_lubrication_law_barus:

MAT_lubrication_law_barus
-------------------------

barus lubrication material law

::

      MAT <matID>  MAT_lubrication_law_barus ABSViscosity 0 PreVisCoeff 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - ABSViscosity
     - 
     - absolute lubricant viscosity
   * - PreVisCoeff
     - 
     - pressure viscosity coefficient

.. _MAT_lubrication_law_roeland:

MAT_lubrication_law_roeland
---------------------------

roeland lubrication material law

::

      MAT <matID>  MAT_lubrication_law_roeland ABSViscosity 0 PreVisCoeff 0 \
       RefVisc 0 RefPress 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - ABSViscosity
     - 
     - absolute lubricant viscosity
   * - PreVisCoeff
     - 
     - pressure viscosity coefficient
   * - RefVisc
     - 
     - reference viscosity
   * - RefPress
     - 
     - reference Pressure

.. _MAT_scatra:

MAT_scatra
----------

scalar transport material

::

      MAT <matID>  MAT_scatra DIFFUSIVITY 0 REACOEFF 0 SCNUM 0 DENSIFICATION 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - DIFFUSIVITY
     - 
     - kinematic diffusivity
   * - REACOEFF
     - yes
     - reaction coefficient
   * - SCNUM
     - yes
     - schmidt number
   * - DENSIFICATION
     - yes
     - densification coefficient

.. _MAT_scatra_reaction_poro:

MAT_scatra_reaction_poro
------------------------

scalar transport material

::

      MAT <matID>  MAT_scatra_reaction_poro NUMSCAL 0 STOICH  REACCOEFF 0 \
       REACSCALE 0 DISTRFUNCT 0 COUPLING no_coupling ROLE  REACSTART 

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - NUMSCAL
     - 
     - number of scalars for these elements
   * - STOICH
     - 
     - reaction stoichometrie list
   * - REACCOEFF
     - 
     - reaction coefficient
   * - REACSCALE
     - 
     - scaling for reaction coefficient
   * - DISTRFUNCT
     - yes
     - spatial distribution of reaction coefficient
   * - COUPLING
     - 
     - type of coupling: simple_multiplicative, |break| 
       power_multiplicative, constant, michaelis_menten, |break| 
       by_function, no_coupling (default)
   * - ROLE
     - 
     - role in michaelis-menten like reactions
   * - REACSTART
     - yes
     - starting point of reaction

.. _MAT_scatra_reaction:

MAT_scatra_reaction
-------------------

advanced reaction material

::

      MAT <matID>  MAT_scatra_reaction NUMSCAL 0 STOICH  REACCOEFF 0 \
       DISTRFUNCT 0 COUPLING no_coupling ROLE  REACSTART 

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - NUMSCAL
     - 
     - number of scalars for these elements
   * - STOICH
     - 
     - reaction stoichometrie list
   * - REACCOEFF
     - 
     - reaction coefficient
   * - DISTRFUNCT
     - yes
     - spatial distribution of reaction coefficient
   * - COUPLING
     - 
     - type of coupling: simple_multiplicative, |break| 
       power_multiplicative, constant, michaelis_menten, |break| 
       by_function, no_coupling (default)
   * - ROLE
     - 
     - role in michaelis-menten like reactions
   * - REACSTART
     - yes
     - starting point of reaction

.. _MAT_scatra_multiporo_fluid:

MAT_scatra_multiporo_fluid
--------------------------

advanced reaction material for multiphase porous flow (species in fluid)

::

      MAT <matID>  MAT_scatra_multiporo_fluid DIFFUSIVITY 0 PHASEID 0 \
       REACOEFF 0 SCNUM 0 DENSIFICATION 0 DELTA 0 MIN_SAT 1e-09

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - DIFFUSIVITY
     - 
     - kinematic diffusivity
   * - PHASEID
     - 
     - ID of fluid phase the scalar is associated with
   * - REACOEFF
     - yes
     - reaction coefficient
   * - SCNUM
     - yes
     - schmidt number
   * - DENSIFICATION
     - yes
     - densification coefficient
   * - DELTA
     - yes
     - delta
   * - MIN_SAT
     - yes
     - minimum saturation under which also corresponding |break| 
       mass fraction is equal to zero

.. _MAT_scatra_multiporo_volfrac:

MAT_scatra_multiporo_volfrac
----------------------------

advanced reaction material for multiphase porous flow (species in volfrac)

::

      MAT <matID>  MAT_scatra_multiporo_volfrac DIFFUSIVITY 0 PHASEID 0 \
       REACOEFF 0 SCNUM 0 DENSIFICATION 0 DELTA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - DIFFUSIVITY
     - 
     - kinematic diffusivity
   * - PHASEID
     - 
     - ID of fluid phase the scalar is associated with
   * - REACOEFF
     - yes
     - reaction coefficient
   * - SCNUM
     - yes
     - schmidt number
   * - DENSIFICATION
     - yes
     - densification coefficient
   * - DELTA
     - yes
     - delta

.. _MAT_scatra_multiporo_solid:

MAT_scatra_multiporo_solid
--------------------------

advanced reaction material for multiphase porous flow (species in solid)

::

      MAT <matID>  MAT_scatra_multiporo_solid DIFFUSIVITY 0 REACOEFF 0 \
       SCNUM 0 DENSIFICATION 0 DELTA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - DIFFUSIVITY
     - 
     - kinematic diffusivity
   * - REACOEFF
     - yes
     - reaction coefficient
   * - SCNUM
     - yes
     - schmidt number
   * - DENSIFICATION
     - yes
     - densification coefficient
   * - DELTA
     - yes
     - delta

.. _MAT_scatra_multiporo_temperature:

MAT_scatra_multiporo_temperature
--------------------------------

advanced reaction material for multiphase porous flow (temperature)

::

      MAT <matID>  MAT_scatra_multiporo_temperature NUMFLUIDPHASES_IN_MULTIPHASEPORESPACE 0 \
       CP_FLUID  NUMVOLFRAC 0 CP_VOLFRAC  CP_SOLID 0 KAPPA_FLUID  \
       KAPPA_VOLFRAC  KAPPA_SOLID 0 DIFFUSIVITY 1 REACOEFF 0 SCNUM 0 \
       DENSIFICATION 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - NUMFLUIDPHASES_IN_MULTIPHASEPORESPACE
     - 
     - number of fluid dofs
   * - CP_FLUID
     - 
     - heat capacity fluid phases
   * - NUMVOLFRAC
     - 
     - number of volfrac dofs
   * - CP_VOLFRAC
     - 
     - heat capacity volfrac
   * - CP_SOLID
     - 
     - heat capacity solid
   * - KAPPA_FLUID
     - 
     - thermal diffusivity fluid phases
   * - KAPPA_VOLFRAC
     - 
     - thermal diffusivity volfrac
   * - KAPPA_SOLID
     - 
     - heat capacity solid
   * - DIFFUSIVITY
     - yes
     - kinematic diffusivity
   * - REACOEFF
     - yes
     - reaction coefficient
   * - SCNUM
     - yes
     - schmidt number
   * - DENSIFICATION
     - yes
     - densification coefficient

.. _MAT_scatra_chemotaxis:

MAT_scatra_chemotaxis
---------------------

chemotaxis material

::

      MAT <matID>  MAT_scatra_chemotaxis NUMSCAL 0 PAIR  CHEMOCOEFF 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - NUMSCAL
     - 
     - number of chemotactic pairs for these elements
   * - PAIR
     - 
     - chemotaxis pairing
   * - CHEMOCOEFF
     - 
     - chemotaxis coefficient

.. _MAT_scatra_aniso:

MAT_scatra_aniso
----------------

anisotropic scalar transport material

::

      MAT <matID>  MAT_scatra_aniso DIFF1 0 DIFF2 0 DIFF3 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - DIFF1
     - 
     - kinematic diffusivity component 1
   * - DIFF2
     - 
     - kinematic diffusivity component 2
   * - DIFF3
     - 
     - kinematic diffusivity component 3

.. _MAT_scatra_multiscale:

MAT_scatra_multiscale
---------------------

scalar transport material for multi-scale approach

::

      MAT <matID>  MAT_scatra_multiscale MICROFILE filename.dat MICRODIS_NUM 0 \
       POROSITY 0 TORTUOSITY 0 A_s 0 DIFFUSIVITY 0 REACOEFF 0 SCNUM 0 \
       DENSIFICATION 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - MICROFILE
     - 
     - input file for micro scale
   * - MICRODIS_NUM
     - 
     - number of micro-scale discretization
   * - POROSITY
     - 
     - porosity
   * - TORTUOSITY
     - 
     - tortuosity
   * - A_s
     - 
     - specific micro-scale surface area
   * - DIFFUSIVITY
     - 
     - kinematic diffusivity
   * - REACOEFF
     - yes
     - reaction coefficient
   * - SCNUM
     - yes
     - Schmidt number
   * - DENSIFICATION
     - yes
     - densification coefficient

.. _MAT_Muscle_Weickenmeier:

MAT_Muscle_Weickenmeier
-----------------------

Weickenmeier muscle material

::

      MAT <matID>  MAT_Muscle_Weickenmeier ALPHA 0 BETA 0 GAMMA 0 \
       KAPPA 0 OMEGA0 0 ACTMUNUM 0 MUTYPESNUM 0 INTERSTIM  FRACACTMU  \
       FTWITCH  TTWITCH  LAMBDAMIN 0 LAMBDAOPT 0 DOTLAMBDAMIN 0 KE 0 \
       KC 0 DE 0 DC 0 ACTTIMESNUM 0 ACTTIMES  ACTINTERVALSNUM 0 ACTVALUES  \
       DENS 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - ALPHA
     - 
     - experimentally fitted material parameter
   * - BETA
     - 
     - experimentally fitted material parameter
   * - GAMMA
     - 
     - experimentally fitted material parameter
   * - KAPPA
     - 
     - material parameter for coupled volumetric |break| 
       contribution
   * - OMEGA0
     - 
     - weighting factor for isotropic tissue constituents
   * - ACTMUNUM
     - 
     - number of active motor units per undeformed muscle |break| 
       cross-sectional area
   * - MUTYPESNUM
     - 
     - number of motor unit types
   * - INTERSTIM
     - 
     - interstimulus interval
   * - FRACACTMU
     - 
     - fraction of motor unit type
   * - FTWITCH
     - 
     - twitch force of motor unit type
   * - TTWITCH
     - 
     - twitch contraction time of motor unit type
   * - LAMBDAMIN
     - 
     - minimal active fiber stretch
   * - LAMBDAOPT
     - 
     - optimal active fiber stretch related to active |break| 
       nominal stress maximum
   * - DOTLAMBDAMIN
     - 
     - minimal stretch rate
   * - KE
     - 
     - parameter controlling the curvature of the |break| 
       velocity dependent activation function in the |break| 
       eccentric case
   * - KC
     - 
     - parameter controlling the curvature of the |break| 
       velocity dependent activation function in the |break| 
       concentric case
   * - DE
     - 
     - parameter controlling the amplitude of the |break| 
       velocity dependent activation function in the |break| 
       eccentric case
   * - DC
     - 
     - parameter controlling the amplitude of the |break| 
       velocity dependent activation function in the |break| 
       concentric case
   * - ACTTIMESNUM
     - 
     - number of time boundaries to prescribe activation
   * - ACTTIMES
     - 
     - time boundaries between intervals
   * - ACTINTERVALSNUM
     - 
     - number of time intervals to prescribe activation
   * - ACTVALUES
     - 
     - scaling factor in intervals (1=full activation, |break| 
       0=no activation)
   * - DENS
     - 
     - density

.. _MAT_Muscle_Giantesio:

MAT_Muscle_Giantesio
--------------------

Giantesio active strain muscle material

::

      MAT <matID>  MAT_Muscle_Giantesio ALPHA 0 BETA 0 GAMMA 0 KAPPA 0 \
       OMEGA0 0 ACTMUNUM 0 MUTYPESNUM 0 INTERSTIM  FRACACTMU  FTWITCH  \
       TTWITCH  LAMBDAMIN 0 LAMBDAOPT 0 DOTLAMBDAMIN 0 KE 0 KC 0 \
       DE 0 DC 0 ACTTIMESNUM 0 ACTTIMES  ACTINTERVALSNUM 0 ACTVALUES  \
       DENS 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - ALPHA
     - 
     - experimentally fitted material parameter
   * - BETA
     - 
     - experimentally fitted material parameter
   * - GAMMA
     - 
     - experimentally fitted material parameter
   * - KAPPA
     - 
     - material parameter for coupled volumetric |break| 
       contribution
   * - OMEGA0
     - 
     - weighting factor for isotropic tissue constituents
   * - ACTMUNUM
     - 
     - number of active motor units per undeformed muscle |break| 
       cross-sectional area
   * - MUTYPESNUM
     - 
     - number of motor unit types
   * - INTERSTIM
     - 
     - interstimulus interval
   * - FRACACTMU
     - 
     - fraction of motor unit type
   * - FTWITCH
     - 
     - twitch force of motor unit type
   * - TTWITCH
     - 
     - twitch contraction time of motor unit type
   * - LAMBDAMIN
     - 
     - minimal active fiber stretch
   * - LAMBDAOPT
     - 
     - optimal active fiber stretch related to active |break| 
       nominal stress maximum
   * - DOTLAMBDAMIN
     - 
     - minimal stretch rate
   * - KE
     - 
     - parameter controlling the curvature of the |break| 
       velocity dependent activation function in the |break| 
       eccentric case
   * - KC
     - 
     - parameter controlling the curvature of the |break| 
       velocity dependent activation function in the |break| 
       concentric case
   * - DE
     - 
     - parameter controlling the amplitude of the |break| 
       velocity dependent activation function in the |break| 
       eccentric case
   * - DC
     - 
     - parameter controlling the amplitude of the |break| 
       velocity dependent activation function in the |break| 
       concentric case
   * - ACTTIMESNUM
     - 
     - number of time boundaries to prescribe activation
   * - ACTTIMES
     - 
     - time boundaries between intervals
   * - ACTINTERVALSNUM
     - 
     - number of time intervals to prescribe activation
   * - ACTVALUES
     - 
     - scaling factor in intervals (1=full activation, |break| 
       0=no activation)
   * - DENS
     - 
     - density

.. _MAT_myocard:

MAT_myocard
-----------

Myocard muscle material

::

      MAT <matID>  MAT_myocard DIFF1 0 DIFF2 0 DIFF3 0 PERTUBATION_DERIV 0 \
       MODEL MV TISSUE M TIME_SCALE 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - DIFF1
     - 
     - conductivity in fiber direction
   * - DIFF2
     - 
     - conductivity perpendicular to fiber direction
   * - DIFF3
     - 
     - conductivity perpendicular to fiber direction
   * - PERTUBATION_DERIV
     - 
     - pertubation for calculation of reaction |break| 
       coefficient derivative
   * - MODEL
     - 
     - Model type: MV (default), FHN, TNNP, SAN or INADA
   * - TISSUE
     - 
     - Tissue type: M (default), ENDO, EPI, AN, N or NH
   * - TIME_SCALE
     - 
     - Scale factor for time units of Model

.. _MAT_mixfrac:

MAT_mixfrac
-----------

material according to mixture-fraction approach

::

      MAT <matID>  MAT_mixfrac KINVISC 0 KINDIFF 0 EOSFACA 0 EOSFACB 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - KINVISC
     - 
     - kinematic viscosity
   * - KINDIFF
     - 
     - kinematic diffusivity
   * - EOSFACA
     - 
     - equation-of-state factor a
   * - EOSFACB
     - 
     - equation-of-state factor b

.. _MAT_sutherland:

MAT_sutherland
--------------

material according to Sutherland law

::

      MAT <matID>  MAT_sutherland REFVISC 0 REFTEMP 0 SUTHTEMP 0 \
       SHC 0 PRANUM 0 THERMPRESS 0 GASCON 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - REFVISC
     - 
     - reference dynamic viscosity (kg/(m*s))
   * - REFTEMP
     - 
     - reference temperature (K)
   * - SUTHTEMP
     - 
     - Sutherland temperature (K)
   * - SHC
     - 
     - specific heat capacity at constant pressure |break| 
       (J/(kg*K))
   * - PRANUM
     - 
     - Prandtl number
   * - THERMPRESS
     - 
     - (initial) thermodynamic pressure (J/m^3)
   * - GASCON
     - 
     - specific gas constant R (J/(kg*K))

.. _MAT_tempdepwater:

MAT_tempdepwater
----------------

material for temperature-dependent water

::

      MAT <matID>  MAT_tempdepwater CRITDENS 0 CRITTEMP 0 SHC 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - CRITDENS
     - 
     - critical density (kg/m^3)
   * - CRITTEMP
     - 
     - critical temperature (K)
   * - SHC
     - 
     - specific heat capacity at constant pressure |break| 
       (J/(kg*K))

.. _MAT_arrhenius_spec:

MAT_arrhenius_spec
------------------

Arrhenius-type chemical kinetics (species)

::

      MAT <matID>  MAT_arrhenius_spec REFVISC 0 REFTEMP 0 SUTHTEMP 0 \
       SCHNUM 0 PREEXCON 0 TEMPEXP 0 ACTEMP 0 GASCON 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - REFVISC
     - 
     - reference dynamic viscosity (kg/(m*s))
   * - REFTEMP
     - 
     - reference temperature (K)
   * - SUTHTEMP
     - 
     - Sutherland temperature (K)
   * - SCHNUM
     - 
     - Schmidt number
   * - PREEXCON
     - 
     - pre-exponential constant (1/s)
   * - TEMPEXP
     - 
     - exponent of temperature dependence
   * - ACTEMP
     - 
     - activation temperature (K)
   * - GASCON
     - 
     - specific gas constant R (J/(kg*K))

.. _MAT_arrhenius_temp:

MAT_arrhenius_temp
------------------

Arrhenius-type chemical kinetics (temperature)

::

      MAT <matID>  MAT_arrhenius_temp REFVISC 0 REFTEMP 0 SUTHTEMP 0 \
       SHC 0 PRANUM 0 REAHEAT 0 PREEXCON 0 TEMPEXP 0 ACTEMP 0 GASCON 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - REFVISC
     - 
     - reference dynamic viscosity (kg/(m*s))
   * - REFTEMP
     - 
     - reference temperature (K)
   * - SUTHTEMP
     - 
     - Sutherland temperature (K)
   * - SHC
     - 
     - specific heat capacity at constant pressure |break| 
       (J/(kg*K))
   * - PRANUM
     - 
     - Prandtl number
   * - REAHEAT
     - 
     - heat of reaction per unit mass (J/kg)
   * - PREEXCON
     - 
     - pre-exponential constant (1/s)
   * - TEMPEXP
     - 
     - exponent of temperature dependence
   * - ACTEMP
     - 
     - activation temperature (K)
   * - GASCON
     - 
     - specific gas constant R (J/(kg*K))

.. _MAT_arrhenius_pv:

MAT_arrhenius_pv
----------------

material with Arrhenius-type chemical kinetics (progress variable)

::

      MAT <matID>  MAT_arrhenius_pv REFVISC 0 REFTEMP 0 SUTHTEMP 0 \
       PRANUM 0 PREEXCON 0 TEMPEXP 0 ACTEMP 0 UNBSHC 0 BURSHC 0 UNBTEMP 0 \
       BURTEMP 0 UNBDENS 0 BURDENS 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - REFVISC
     - 
     - reference dynamic viscosity (kg/(m*s))
   * - REFTEMP
     - 
     - reference temperature (K)
   * - SUTHTEMP
     - 
     - Sutherland temperature (K)
   * - PRANUM
     - 
     - Prandtl number
   * - PREEXCON
     - 
     - pre-exponential constant (1/s)
   * - TEMPEXP
     - 
     - exponent of temperature dependence
   * - ACTEMP
     - 
     - activation temperature (K)
   * - UNBSHC
     - 
     - specific heat capacity of unburnt phase (J/(kg*K))
   * - BURSHC
     - 
     - specific heat capacity of burnt phase (J/(kg*K))
   * - UNBTEMP
     - 
     - temperature of unburnt phase (K)
   * - BURTEMP
     - 
     - temperature of burnt phase (K)
   * - UNBDENS
     - 
     - density of unburnt phase (kg/m^3)
   * - BURDENS
     - 
     - density of burnt phase (kg/m^3)

.. _MAT_ferech_pv:

MAT_ferech_pv
-------------

material with Ferziger-Echekki (1993) chemical kinetics (progress variable)

::

      MAT <matID>  MAT_ferech_pv REFVISC 0 REFTEMP 0 SUTHTEMP 0 PRANUM 0 \
       REACRATECON 0 PVCRIT 0 UNBSHC 0 BURSHC 0 UNBTEMP 0 BURTEMP 0 \
       UNBDENS 0 BURDENS 0 MOD 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - REFVISC
     - 
     - reference dynamic viscosity (kg/(m*s))
   * - REFTEMP
     - 
     - reference temperature (K)
   * - SUTHTEMP
     - 
     - Sutherland temperature (K)
   * - PRANUM
     - 
     - Prandtl number
   * - REACRATECON
     - 
     - reaction-rate constant (1/s)
   * - PVCRIT
     - 
     - critical value of progress variable
   * - UNBSHC
     - 
     - specific heat capacity of unburnt phase (J/(kg*K))
   * - BURSHC
     - 
     - specific heat capacity of burnt phase (J/(kg*K))
   * - UNBTEMP
     - 
     - temperature of unburnt phase (K)
   * - BURTEMP
     - 
     - temperature of burnt phase (K)
   * - UNBDENS
     - 
     - density of unburnt phase (kg/m^3)
   * - BURDENS
     - 
     - density of burnt phase (kg/m^3)
   * - MOD
     - 
     - modification factor (0.0=original, 1.0=modified)

.. _MAT_ion:

MAT_ion
-------

material parameters for ion species in electrolyte solution

::

      MAT <matID>  MAT_ion DIFFUSIVITY 0 VALENCE 0 DENSIFICATION 0 \
       ELIM_DIFFUSIVITY 0 ELIM_VALENCE 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - DIFFUSIVITY
     - 
     - kinematic diffusivity
   * - VALENCE
     - 
     - valence (= charge number)
   * - DENSIFICATION
     - yes
     - densification coefficient
   * - ELIM_DIFFUSIVITY
     - yes
     - kinematic diffusivity of elim. species
   * - ELIM_VALENCE
     - yes
     - valence of elim. species

.. _MAT_newman:

MAT_newman
----------

material parameters for ion species in electrolyte solution

::

      MAT <matID>  MAT_newman VALENCE 0 DIFF_COEF_CONC_DEP_FUNCT 0 \
       DIFF_COEF_TEMP_SCALE_FUNCT 0 TRANSNR 0 THERMFAC 0 COND_CONC_DEP_FUNCT 0 \
       COND_TEMP_SCALE_FUNCT 0 DIFF_PARA_NUM 0 DIFF_PARA  DIFF_COEF_TEMP_SCALE_FUNCT_PARA_NUM 0 \
       DIFF_COEF_TEMP_SCALE_FUNCT_PARA  TRANS_PARA_NUM 0 TRANS_PARA  \
       THERM_PARA_NUM 0 THERM_PARA  COND_PARA_NUM 0 COND_PARA  COND_TEMP_SCALE_FUNCT_PARA_NUM 0 \
       COND_TEMP_SCALE_FUNCT_PARA  END

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - VALENCE
     - 
     - valence (= charge number)
   * - DIFF_COEF_CONC_DEP_FUNCT
     - 
     - function number of function describing |break| 
       concentration dependence of diffusion coefficient
   * - DIFF_COEF_TEMP_SCALE_FUNCT
     - 
     - FUNCT number describing temperature scaling of |break| 
       diffusion coefficient
   * - TRANSNR
     - 
     - curve number for transference number
   * - THERMFAC
     - 
     - curve number for thermodynamic factor
   * - COND_CONC_DEP_FUNCT
     - 
     - function number of function describing |break| 
       concentration dependence of conductivity
   * - COND_TEMP_SCALE_FUNCT
     - 
     - FUNCT number describing temperature scaling of |break| 
       conductivity
   * - DIFF_PARA_NUM
     - yes
     - number of parameters for diffusion coefficient
   * - DIFF_PARA
     - yes
     - parameters for diffusion coefficient
   * - DIFF_COEF_TEMP_SCALE_FUNCT_PARA_NUM
     - yes
     - number of parameters for scaling function |break| 
       describing temperature dependence of diffusion |break| 
       coefficient
   * - DIFF_COEF_TEMP_SCALE_FUNCT_PARA
     - yes
     - parameters for function describing temperature |break| 
       dependence of diffusion coefficient
   * - TRANS_PARA_NUM
     - yes
     - number of parameters for transference number
   * - TRANS_PARA
     - yes
     - parameters for transference number
   * - THERM_PARA_NUM
     - yes
     - number of parameters for thermodynamic factor
   * - THERM_PARA
     - yes
     - parameters for thermodynamic factor
   * - COND_PARA_NUM
     - yes
     - number of parameters for conductivity
   * - COND_PARA
     - yes
     - parameters for conductivity
   * - COND_TEMP_SCALE_FUNCT_PARA_NUM
     - yes
     - number of parameters for temperature scaling of |break| 
       conductivity
   * - COND_TEMP_SCALE_FUNCT_PARA
     - yes
     - parameters for temperature scaling of conductivity
   * - END
     - 
     - indicating end of line

.. _MAT_newman_multiscale:

MAT_newman_multiscale
---------------------

material parameters for ion species in electrolyte solution for multi-scale approach

::

      MAT <matID>  MAT_newman_multiscale VALENCE 0 DIFF_COEF_CONC_DEP_FUNCT 0 \
       DIFF_COEF_TEMP_SCALE_FUNCT 0 TRANSNR 0 THERMFAC 0 COND_CONC_DEP_FUNCT 0 \
       COND_TEMP_SCALE_FUNCT 0 SIGMA 0 A_s 0 MICROFILE filename.dat \
       MICRODIS_NUM 0 DIFF_PARA_NUM 0 DIFF_PARA  DIFF_COEF_TEMP_SCALE_FUNCT_PARA_NUM 0 \
       DIFF_COEF_TEMP_SCALE_FUNCT_PARA  TRANS_PARA_NUM 0 TRANS_PARA  \
       THERM_PARA_NUM 0 THERM_PARA  COND_PARA_NUM 0 COND_PARA  COND_TEMP_SCALE_FUNCT_PARA_NUM 0 \
       COND_TEMP_SCALE_FUNCT_PARA  END

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - VALENCE
     - 
     - valence (= charge number)
   * - DIFF_COEF_CONC_DEP_FUNCT
     - 
     - function number of function describing |break| 
       concentration dependence of diffusion coefficient
   * - DIFF_COEF_TEMP_SCALE_FUNCT
     - 
     - FUNCT number describing temperature scaling of |break| 
       diffusion coefficient
   * - TRANSNR
     - 
     - curve number for transference number
   * - THERMFAC
     - 
     - curve number for thermodynamic factor
   * - COND_CONC_DEP_FUNCT
     - 
     - function number of function describing |break| 
       concentration dependence of conductivity
   * - COND_TEMP_SCALE_FUNCT
     - 
     - FUNCT number describing temperature scaling of |break| 
       conductivity
   * - SIGMA
     - 
     - electronic conductivity
   * - A_s
     - 
     - specific micro-scale surface area
   * - MICROFILE
     - 
     - input file for micro scale
   * - MICRODIS_NUM
     - 
     - number of micro-scale discretization
   * - DIFF_PARA_NUM
     - yes
     - number of parameters for diffusion coefficient
   * - DIFF_PARA
     - yes
     - parameters for diffusion coefficient
   * - DIFF_COEF_TEMP_SCALE_FUNCT_PARA_NUM
     - yes
     - number of parameters for scaling function |break| 
       describing temperature dependence of diffusion |break| 
       coefficient
   * - DIFF_COEF_TEMP_SCALE_FUNCT_PARA
     - yes
     - parameters for function describing temperature |break| 
       dependence of diffusion coefficient
   * - TRANS_PARA_NUM
     - yes
     - number of parameters for transference number
   * - TRANS_PARA
     - yes
     - parameters for transference number
   * - THERM_PARA_NUM
     - yes
     - number of parameters for thermodynamic factor
   * - THERM_PARA
     - yes
     - parameters for thermodynamic factor
   * - COND_PARA_NUM
     - yes
     - number of parameters for ionic conductivity
   * - COND_PARA
     - yes
     - parameters for ionic conductivity
   * - COND_TEMP_SCALE_FUNCT_PARA_NUM
     - yes
     - number of parameters for temperature scaling of |break| 
       conductivity
   * - COND_TEMP_SCALE_FUNCT_PARA
     - yes
     - parameters for temperature scaling of conductivity
   * - END
     - 
     - indicating end of line

.. _MAT_scl:

MAT_scl
-------

material parameters for space charge layers

::

      MAT <matID>  MAT_scl VALENCE 0 DIFF_COEF_CONC_DEP_FUNCT 0 DIFF_COEF_TEMP_SCALE_FUNCT 0 \
       TRANSNR 0 COND_CONC_DEP_FUNCT 0 COND_TEMP_SCALE_FUNCT 0 DIFF_PARA_NUM 0 \
       DIFF_PARA  DIFF_COEF_TEMP_SCALE_FUNCT_PARA_NUM 0 DIFF_COEF_TEMP_SCALE_FUNCT_PARA  \
       TRANS_PARA_NUM 0 TRANS_PARA  COND_PARA_NUM 0 COND_PARA  COND_TEMP_SCALE_FUNCT_PARA_NUM 0 \
       COND_TEMP_SCALE_FUNCT_PARA  MAX_CONC 1 EXTRAPOL_DIFF 0 LIM_CONC 1 \
       BULK_CONC 1 SUSCEPT 1 DELTA_NU 0 END

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - VALENCE
     - 
     - valence/charge number
   * - DIFF_COEF_CONC_DEP_FUNCT
     - 
     - function number of function describing |break| 
       concentration dependence of diffusion coefficient
   * - DIFF_COEF_TEMP_SCALE_FUNCT
     - 
     - function number describing temperature scaling of |break| 
       diffusion coefficient
   * - TRANSNR
     - 
     - curve number for transference number
   * - COND_CONC_DEP_FUNCT
     - 
     - function number of function describing |break| 
       concentration dependence of conductivity
   * - COND_TEMP_SCALE_FUNCT
     - 
     - function number describing temperature scaling of |break| 
       conductivity
   * - DIFF_PARA_NUM
     - yes
     - number of parameters for diffusion coefficient
   * - DIFF_PARA
     - yes
     - parameters for diffusion coefficient
   * - DIFF_COEF_TEMP_SCALE_FUNCT_PARA_NUM
     - yes
     - number of parameters for scaling function |break| 
       describing temperature dependence of diffusion |break| 
       coefficient
   * - DIFF_COEF_TEMP_SCALE_FUNCT_PARA
     - yes
     - parameters for function describing temperature |break| 
       dependence of diffusion coefficient
   * - TRANS_PARA_NUM
     - yes
     - number of parameters for transference number
   * - TRANS_PARA
     - yes
     - parameters for transference number
   * - COND_PARA_NUM
     - yes
     - number of parameters for conductivity
   * - COND_PARA
     - yes
     - parameters for conductivity
   * - COND_TEMP_SCALE_FUNCT_PARA_NUM
     - yes
     - number of parameters for temperature scaling of |break| 
       conductivity
   * - COND_TEMP_SCALE_FUNCT_PARA
     - yes
     - parameters for temperature scaling of conductivity
   * - MAX_CONC
     - 
     - maximum cation concentration
   * - EXTRAPOL_DIFF
     - 
     - strategy for extrapolation of diffusion |break| 
       coefficient below 0 and above MAX_CONC (-1: |break| 
       disabled, 0: constant)
   * - LIM_CONC
     - yes
     - limiting concentration for extrapolation
   * - BULK_CONC
     - 
     - bulk ion concentration
   * - SUSCEPT
     - 
     - susceptibility
   * - DELTA_NU
     - 
     - difference of partial molar volumes (vacancy & |break| 
       cation)
   * - END
     - 
     - indicating end of line

.. _MAT_electrode:

MAT_electrode
-------------

electrode material

::

      MAT <matID>  MAT_electrode DIFF_COEF_CONC_DEP_FUNCT 0 DIFF_COEF_TEMP_SCALE_FUNCT 0 \
       COND_CONC_DEP_FUNCT 0 COND_TEMP_SCALE_FUNCT 0 DIFF_PARA_NUM 0 \
       DIFF_PARA  DIFF_COEF_TEMP_SCALE_FUNCT_PARA_NUM 0 DIFF_COEF_TEMP_SCALE_FUNCT_PARA  \
       COND_PARA_NUM 0 COND_PARA  COND_TEMP_SCALE_FUNCT_PARA_NUM 0 \
       COND_TEMP_SCALE_FUNCT_PARA  C_MAX 0 CHI_MAX 0 OCP_MODEL none \
       X_MIN 2 X_MAX 2 OCP_PARA_NUM 0 OCP_PARA  OCP_CSV  END

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - DIFF_COEF_CONC_DEP_FUNCT
     - 
     - function number of function describing |break| 
       concentration dependence of diffusion coefficient
   * - DIFF_COEF_TEMP_SCALE_FUNCT
     - 
     - FUNCT number describing temperature scaling of |break| 
       diffusion coefficient
   * - COND_CONC_DEP_FUNCT
     - 
     - function number of function describing |break| 
       concentration dependence of conductivity
   * - COND_TEMP_SCALE_FUNCT
     - 
     - FUNCT number describing temperature scaling of |break| 
       conductivity
   * - DIFF_PARA_NUM
     - yes
     - number of parameters for diffusion coefficient
   * - DIFF_PARA
     - yes
     - parameters for diffusion coefficient
   * - DIFF_COEF_TEMP_SCALE_FUNCT_PARA_NUM
     - yes
     - number of parameters for scaling function |break| 
       describing temperature dependence of diffusion |break| 
       coefficient
   * - DIFF_COEF_TEMP_SCALE_FUNCT_PARA
     - yes
     - parameters for function describing temperature |break| 
       dependence of diffusion coefficient
   * - COND_PARA_NUM
     - yes
     - number of parameters for electronic conductivity
   * - COND_PARA
     - yes
     - parameters for electronic conductivity
   * - COND_TEMP_SCALE_FUNCT_PARA_NUM
     - yes
     - number of parameters for temperature scaling of |break| 
       conductivity
   * - COND_TEMP_SCALE_FUNCT_PARA
     - yes
     - parameters for temperature scaling of conductivity
   * - C_MAX
     - 
     - saturation value of intercalated Lithium |break| 
       concentration
   * - CHI_MAX
     - 
     - lithiation value corresponding to saturation value |break| 
       of intercalated Lithium concentration 'C_MAX'
   * - OCP_MODEL
     - 
     - model for half cell open circuit potential of |break| 
       electrode: Redlich-Kister, Taralov, Polynomial, |break| 
       csv
   * - X_MIN
     - 
     - lower bound of range of validity as a fraction of |break| 
       C_MAX for ocp calculation model
   * - X_MAX
     - 
     - upper bound of range of validity as a fraction of |break| 
       C_MAX for ocp calculation model
   * - OCP_PARA_NUM
     - yes
     - number of parameters underlying half cell open |break| 
       circuit potential model
   * - OCP_PARA
     - yes
     - parameters underlying half cell open circuit |break| 
       potential model
   * - OCP_CSV
     - yes
     - \*.csv file with data points for half cell open |break| 
       circuit potential
   * - END
     - 
     - indicating end of line

.. _MAT_matlist:

MAT_matlist
-----------

list/collection of materials, i.e. material IDs

::

      MAT <matID>  MAT_matlist LOCAL No NUMMAT 0 MATIDS  END

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - LOCAL
     - 
     - individual materials allocated per element or only |break| 
       at global scope
   * - NUMMAT
     - 
     - number of materials in list
   * - MATIDS
     - 
     - the list material IDs
   * - END
     - 
     - indicating end of line

.. _MAT_matlist_reactions:

MAT_matlist_reactions
---------------------

list/collection of materials, i.e. material IDs and list of reactions

::

      MAT <matID>  MAT_matlist_reactions LOCAL No NUMMAT 0 MATIDS  \
       NUMREAC 0 REACIDS  END

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - LOCAL
     - 
     - individual materials allocated per element or only |break| 
       at global scope
   * - NUMMAT
     - 
     - number of materials in list
   * - MATIDS
     - 
     - the list material IDs
   * - NUMREAC
     - 
     - number of reactions for these elements
   * - REACIDS
     - 
     - advanced reaction list
   * - END
     - 
     - indicating end of line

.. _MAT_matlist_chemotaxis:

MAT_matlist_chemotaxis
----------------------

list/collection of materials, i.e. material IDs and list of chemotactic pairs

::

      MAT <matID>  MAT_matlist_chemotaxis LOCAL No NUMMAT 0 MATIDS  \
       NUMPAIR 0 PAIRIDS  END

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - LOCAL
     - 
     - individual materials allocated per element or only |break| 
       at global scope
   * - NUMMAT
     - 
     - number of materials in list
   * - MATIDS
     - 
     - the list material IDs
   * - NUMPAIR
     - 
     - number of pairs for these elements
   * - PAIRIDS
     - 
     - chemotaxis pairs list
   * - END
     - 
     - indicating end of line

.. _MAT_matlist_chemo_reac:

MAT_matlist_chemo_reac
----------------------

list/collection of materials, i.e. material IDs and list of reactive/chemotactic pairs

::

      MAT <matID>  MAT_matlist_chemo_reac LOCAL No NUMMAT 0 MATIDS  \
       NUMPAIR 0 PAIRIDS  NUMREAC 0 REACIDS  END

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - LOCAL
     - 
     - individual materials allocated per element or only |break| 
       at global scope
   * - NUMMAT
     - 
     - number of materials in list
   * - MATIDS
     - 
     - the list material IDs
   * - NUMPAIR
     - 
     - number of pairs for these elements
   * - PAIRIDS
     - 
     - chemotaxis pairs list
   * - NUMREAC
     - 
     - number of reactions for these elements
   * - REACIDS
     - 
     - advanced reaction list
   * - END
     - 
     - indicating end of line

.. _MAT_elchmat:

MAT_elchmat
-----------

specific list/collection of species and phases for elch applications

::

      MAT <matID>  MAT_elchmat LOCAL No NUMDOF 0 NUMSCAL 0 NUMPHASE 0 \
       PHASEIDS  END

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - LOCAL
     - yes
     - individual materials allocated per element or only |break| 
       at global scope
   * - NUMDOF
     - 
     - number of dof's per node
   * - NUMSCAL
     - 
     - number of transported scalars per node
   * - NUMPHASE
     - 
     - number of phases in electrolyte
   * - PHASEIDS
     - 
     - the list phasel IDs
   * - END
     - 
     - indicating end of line

.. _MAT_elchphase:

MAT_elchphase
-------------

material parameters for ion species in electrolyte solution

::

      MAT <matID>  MAT_elchphase LOCAL No EPSILON 0 TORTUOSITY 0 \
       NUMMAT 0 MATIDS  END

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - LOCAL
     - yes
     - individual materials allocated per element or only |break| 
       at global scope
   * - EPSILON
     - 
     - phase porosity
   * - TORTUOSITY
     - 
     - inverse (!) of phase tortuosity
   * - NUMMAT
     - 
     - number of materials in electrolyte
   * - MATIDS
     - 
     - the list phasel IDs
   * - END
     - 
     - indicating end of line

.. _MAT_Struct_StVenantKirchhoff:

MAT_Struct_StVenantKirchhoff
----------------------------

St.Venant--Kirchhoff material

::

      MAT <matID>  MAT_Struct_StVenantKirchhoff YOUNG 0 NUE 0 DENS 0 \
       THEXPANS 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - YOUNG
     - 
     - Young's modulus
   * - NUE
     - 
     - Poisson's ratio
   * - DENS
     - 
     - mass density
   * - THEXPANS
     - yes
     - coefficient of linear thermal expansion

.. _MAT_Struct_ThrStVenantK:

MAT_Struct_ThrStVenantK
-----------------------

Thermo St.Venant--Kirchhoff material

::

      MAT <matID>  MAT_Struct_ThrStVenantK YOUNGNUM 0 YOUNG  NUE 0 \
       DENS 0 THEXPANS 0 CAPA 0 CONDUCT 0 INITTEMP 0 THERMOMAT -1

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - YOUNGNUM
     - 
     - number of Young's modulus in list (if 1 Young is |break| 
       const, if >1 Young is temperature) dependent
   * - YOUNG
     - 
     - Young's modulus
   * - NUE
     - 
     - Poisson's ratio
   * - DENS
     - 
     - mass density
   * - THEXPANS
     - 
     - constant coefficient of linear thermal expansion
   * - CAPA
     - 
     - capacity
   * - CONDUCT
     - 
     - conductivity
   * - INITTEMP
     - 
     - initial temperature
   * - THERMOMAT
     - yes
     - mat id of thermal material part

.. _MAT_Struct_ThrPlasticLinElast:

MAT_Struct_ThrPlasticLinElast
-----------------------------

Thermo-elastic St.Venant Kirchhoff / plastic von Mises material

::

      MAT <matID>  MAT_Struct_ThrPlasticLinElast YOUNG 0 NUE 0 DENS 0 \
       THEXPANS 0 INITTEMP 0 YIELD 0 ISOHARD 0 KINHARD 0 SAMPLENUM 0 \
       SIGMA_Y  EPSBAR_P  TOL 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - YOUNG
     - 
     - Young's modulus
   * - NUE
     - 
     - Poisson's ratio
   * - DENS
     - 
     - mass density
   * - THEXPANS
     - 
     - coefficient of linear thermal expansion
   * - INITTEMP
     - 
     - initial temperature
   * - YIELD
     - 
     - yield stress
   * - ISOHARD
     - 
     - isotropic hardening modulus
   * - KINHARD
     - 
     - kinematic hardening modulus
   * - SAMPLENUM
     - 
     - number of stress-strain pairs in list
   * - SIGMA_Y
     - 
     - yield stress
   * - EPSBAR_P
     - 
     - accumulated plastic strain corresponding to |break| 
       SIGMA_Y
   * - TOL
     - 
     - tolerance for local Newton iteration

.. _MAT_Struct_SuperElastSMA:

MAT_Struct_SuperElastSMA
------------------------

finite strain superelastic shape memory alloy

::

      MAT <matID>  MAT_Struct_SuperElastSMA DENS 0 YOUNG 0 NUE 0 \
       EPSILON_L 0 T_AS_s 0 T_AS_f 0 T_SA_s 0 T_SA_f 0 C_AS 0 C_SA 0 \
       SIGMA_AS_s 0 SIGMA_AS_f 0 SIGMA_SA_s 0 SIGMA_SA_f 0 ALPHA 0 \
       MODEL 0 BETA_AS 0 BETA_SA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - DENS
     - 
     - mass density
   * - YOUNG
     - 
     - Young's modulus
   * - NUE
     - 
     - Poisson's ratio
   * - EPSILON_L
     - 
     - parameter representing the maximum deformation |break| 
       obtainable only by detwinning of the |break| 
       multiple-variant martensite
   * - T_AS_s
     - 
     - Temperature at which the phase transformation from |break| 
       austenite to martensite starts
   * - T_AS_f
     - 
     - Temperature at which the phase transformation from |break| 
       austenite to martensite finishes
   * - T_SA_s
     - 
     - Temperature at which the phase transformation from |break| 
       martensite to autenite starts
   * - T_SA_f
     - 
     - Temperature at which the phase transformation from |break| 
       martensite to autenite finishes
   * - C_AS
     - 
     - Coefficient of the linear temperature dependence |break| 
       of T_AS
   * - C_SA
     - 
     - Coefficient of the linear temperature dependence |break| 
       of T_SA
   * - SIGMA_AS_s
     - 
     - stress at which the phase transformation from |break| 
       austenite to martensite begins
   * - SIGMA_AS_f
     - 
     - stress at which the phase transformation from |break| 
       austenite to martensite finishes
   * - SIGMA_SA_s
     - 
     - stress at which the phase transformation from |break| 
       martensite to austenite begins
   * - SIGMA_SA_f
     - 
     - stress at which the phase transformation from |break| 
       martensite to austenite finishes
   * - ALPHA
     - 
     - pressure dependency in the drucker-prager-type |break| 
       loading
   * - MODEL
     - 
     - Model used for the evolution of martensitic |break| 
       fraction (1=exponential; 2=linear)
   * - BETA_AS
     - yes
     - parameter, measuring the speed of the |break| 
       transformation from austenite to martensite
   * - BETA_SA
     - yes
     - parameter, measuring the speed of the |break| 
       transformation from martensite to austenite

.. _MAT_Struct_ThrPlasticHyperElast:

MAT_Struct_ThrPlasticHyperElast
-------------------------------

Thermo-hyperelastic / finite strain plastic von Mises material

::

      MAT <matID>  MAT_Struct_ThrPlasticHyperElast YOUNG 0 NUE 0 \
       DENS 0 CTE 0 INITTEMP 0 YIELD 0 ISOHARD 0 SATHARDENING 0 HARDEXPO 0 \
       YIELDSOFT 0 HARDSOFT 0 TOL 1e-08

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - YOUNG
     - 
     - Young's modulus
   * - NUE
     - 
     - Poisson's ratio
   * - DENS
     - 
     - mass density
   * - CTE
     - yes
     - coefficient of thermal expansion
   * - INITTEMP
     - yes
     - initial, reference temperature
   * - YIELD
     - 
     - initial yield stress
   * - ISOHARD
     - yes
     - isotropic hardening modulus
   * - SATHARDENING
     - yes
     - saturation hardening
   * - HARDEXPO
     - yes
     - hardening exponent
   * - YIELDSOFT
     - yes
     - yield stress softening
   * - HARDSOFT
     - yes
     - hardening softening
   * - TOL
     - yes
     - tolerance for local Newton iteration

.. _MAT_Struct_PlasticNlnLogNeoHooke:

MAT_Struct_PlasticNlnLogNeoHooke
--------------------------------

hyperelastic / finite strain plastic von Mises material

::

      MAT <matID>  MAT_Struct_PlasticNlnLogNeoHooke YOUNG 0 NUE 0 \
       DENS 0 YIELD 0 ISOHARD 0 SATHARDENING 0 HARDEXPO 0 VISC 0 \
       RATE_DEPENDENCY 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - YOUNG
     - 
     - Young's modulus
   * - NUE
     - 
     - Poisson's ratio
   * - DENS
     - 
     - mass density
   * - YIELD
     - 
     - yield stress
   * - ISOHARD
     - 
     - isotropic hardening modulus
   * - SATHARDENING
     - 
     - saturation hardening
   * - HARDEXPO
     - 
     - hardening exponent
   * - VISC
     - yes
     - VISCOSITY
   * - RATE_DEPENDENCY
     - yes
     - rate dependency

.. _MAT_Struct_PlasticLinElast:

MAT_Struct_PlasticLinElast
--------------------------

elastic St.Venant Kirchhoff / plastic von Mises material

::

      MAT <matID>  MAT_Struct_PlasticLinElast YOUNG 0 NUE 0 DENS 0 \
       YIELD 0 ISOHARD 0 KINHARD 0 TOL 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - YOUNG
     - 
     - Young's modulus
   * - NUE
     - 
     - Poisson's ratio
   * - DENS
     - 
     - mass density
   * - YIELD
     - 
     - yield stress
   * - ISOHARD
     - 
     - isotropic hardening modulus
   * - KINHARD
     - 
     - kinematic hardening modulus
   * - TOL
     - 
     - tolerance for local Newton iteration

.. _MAT_Struct_Viscoplastic_No_Yield_Surface:

MAT_Struct_Viscoplastic_No_Yield_Surface
----------------------------------------

Elastic visco-plastic finite strain material law without yield surface

::

      MAT <matID>  MAT_Struct_Viscoplastic_No_Yield_Surface YOUNG 0 \
       NUE 0 DENS 0 TEMPERATURE 0 PRE_EXP_FAC 0 ACTIVATION_ENERGY 0 \
       GAS_CONSTANT 0 STRAIN_RATE_SENS 0 INIT_FLOW_RES 0 FLOW_RES_PRE_FAC 0 \
       FLOW_RES_EXP 0 FLOW_RES_SAT_FAC 0 FLOW_RES_SAT_EXP 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - YOUNG
     - 
     - Young's modulus
   * - NUE
     - 
     - Poisson's ratio
   * - DENS
     - 
     - material mass density
   * - TEMPERATURE
     - 
     - temperature in Kelvin
   * - PRE_EXP_FAC
     - 
     - pre-exponential factor of plastic shear strain |break| 
       rate 'A'
   * - ACTIVATION_ENERGY
     - 
     - activation energy 'Q'
   * - GAS_CONSTANT
     - 
     - gas constant 'R'
   * - STRAIN_RATE_SENS
     - 
     - strain-rate-sensitivity 'm'
   * - INIT_FLOW_RES
     - 
     - initial isotropic flow resistance 'S^0'
   * - FLOW_RES_PRE_FAC
     - 
     - flow resistance factor 'H_0'
   * - FLOW_RES_EXP
     - 
     - flow resistance exponential value 'a'
   * - FLOW_RES_SAT_FAC
     - 
     - flow resistance saturation factor 'S_*'
   * - FLOW_RES_SAT_EXP
     - 
     - flow resistance saturation exponent 'b'

.. _MAT_Struct_Robinson:

MAT_Struct_Robinson
-------------------

Robinson's visco-plastic material

::

      MAT <matID>  MAT_Struct_Robinson KIND Arya_NarloyZ YOUNGNUM 0 \
       YOUNG  NUE 0 DENS 0 THEXPANS 0 INITTEMP 0 HRDN_FACT 0 HRDN_EXPO 0 \
       SHRTHRSHLDNUM 0 SHRTHRSHLD  RCVRY 0 ACTV_ERGY 0 ACTV_TMPR 0 \
       G0 0 M_EXPO 0 BETANUM 0 BETA  H_FACT 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - KIND
     - 
     - kind of Robinson material: Butler, Arya, |break| 
       Arya_NarloyZ (default), Arya_CrMoSteel
   * - YOUNGNUM
     - 
     - number of Young's modulus in list
   * - YOUNG
     - 
     - Young's modulus
   * - NUE
     - 
     - Poisson's ratio
   * - DENS
     - 
     - mass density
   * - THEXPANS
     - 
     - coefficient of linear thermal expansion
   * - INITTEMP
     - 
     - initial temperature
   * - HRDN_FACT
     - 
     - hardening factor 'A'
   * - HRDN_EXPO
     - 
     - hardening power 'n'
   * - SHRTHRSHLDNUM
     - 
     - number of shear stress threshold 'K^2'in list
   * - SHRTHRSHLD
     - 
     - Bingam-Prager shear stress threshold 'K^2'
   * - RCVRY
     - 
     - recovery factor 'R_0'
   * - ACTV_ERGY
     - 
     - activation energy 'Q_0'
   * - ACTV_TMPR
     - 
     - activation temperature 'T_0'
   * - G0
     - 
     - 'G_0'
   * - M_EXPO
     - 
     - 'm'
   * - BETANUM
     - 
     - number of 'beta' in list
   * - BETA
     - 
     - beta
   * - H_FACT
     - 
     - 'H'

.. _MAT_Struct_Damage:

MAT_Struct_Damage
-----------------

elasto-plastic von Mises material with ductile damage

::

      MAT <matID>  MAT_Struct_Damage YOUNG 0 NUE 0 DENS 0 SAMPLENUM 0 \
       SIGMA_Y  EPSBAR_P  DAMDEN 0 DAMEXP 0 DAMTHRESHOLD 0 KINHARD 0 \
       KINHARD_REC 0 SATHARDENING 0 HARDEXPO 0 TOL 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - YOUNG
     - 
     - Young's modulus
   * - NUE
     - 
     - Poisson's ratio
   * - DENS
     - 
     - mass density
   * - SAMPLENUM
     - 
     - number of stress-strain pairs in list
   * - SIGMA_Y
     - 
     - yield stress
   * - EPSBAR_P
     - 
     - accumulated plastic strain corresponding to |break| 
       SIGMA_Y
   * - DAMDEN
     - 
     - denominator of damage evoluation law
   * - DAMEXP
     - 
     - exponent of damage evoluation law
   * - DAMTHRESHOLD
     - 
     - damage threshold
   * - KINHARD
     - 
     - kinematic hardening modulus, stress-like variable
   * - KINHARD_REC
     - 
     - recovery factor, scalar-valued variable
   * - SATHARDENING
     - 
     - saturation hardening
   * - HARDEXPO
     - 
     - hardening exponent
   * - TOL
     - 
     - tolerance for local Newton iteration

.. _MAT_Struct_AAANeoHooke:

MAT_Struct_AAANeoHooke
----------------------

aneurysm wall material according to Raghavan and Vorp [2000]

::

      MAT <matID>  MAT_Struct_AAANeoHooke YOUNG 0 BETA 0 NUE 0 DENS 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - YOUNG
     - 
     - Young's modulus
   * - BETA
     - 
     - 2nd parameter
   * - NUE
     - 
     - Poisson's ratio
   * - DENS
     - 
     - mass density

.. _MAT_Struct_AAANeoHookeStopro:

MAT_Struct_AAANeoHookeStopro
----------------------------

aneurysm wall material according to Raghavan and Vorp [2000] with stochastic modelling of beta

::

      MAT <matID>  MAT_Struct_AAANeoHookeStopro YOUNG 0 BETA 0 NUE 0 \
       DENS 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - YOUNG
     - 
     - Young's modulus
   * - BETA
     - 
     - 2nd parameter
   * - NUE
     - 
     - Poisson's ratio
   * - DENS
     - 
     - mass density

.. _MAT_Struct_AAAGasser:

MAT_Struct_AAAGasser
--------------------

AAA thrombus material according to GASSER [2008]

::

      MAT <matID>  MAT_Struct_AAAGasser DENS 0 VOL OSM NUE 0 BETA 0 \
       CLUM 0 CMED 0 CABLUM 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - DENS
     - 
     - mass density
   * - VOL
     - 
     - Type of volumetric Strain Energy Density: OSM |break| 
       (default),SuBa,SiTa
   * - NUE
     - 
     - Poisson's ratio (0.49)
   * - BETA
     - 
     - empiric constant for OSM (-2.0)
   * - CLUM
     - 
     - luminal stiffness parameter (2.62e3)
   * - CMED
     - 
     - medial stiffness parameter (2.62e3)
   * - CABLUM
     - 
     - abluminal stiffness parameter (2.62e3)

.. _MAT_Raghavan_Damage:

MAT_Raghavan_Damage
-------------------

aneurysm wall material according to Raghavan and Vorp [2000] with damage

::

      MAT <matID>  MAT_Raghavan_Damage BULK 0 ALPHA 0 BETA 0 EQSTRMIN 0 \
       A 0 B 0 DENS 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - BULK
     - 
     - Bulk's modulus
   * - ALPHA
     - 
     - 1nd parameter,alpha
   * - BETA
     - 
     - 2nd parameter,beta
   * - EQSTRMIN
     - 
     - equivalent strain initial damage
   * - A
     - 
     - 1st parameter, a
   * - B
     - 
     - 2nd parameter, b
   * - DENS
     - 
     - mass density

.. _MAT_Struct_AAA_MixedEffects:

MAT_Struct_AAA_MixedEffects
---------------------------

aneurysm wall material according to Mixed Effects Model

::

      MAT <matID>  MAT_Struct_AAA_MixedEffects AGE 0 REFDIA 0 NUE 0 \
       DENS 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - AGE
     - 
     - age
   * - REFDIA
     - 
     - subrenal diameter
   * - NUE
     - 
     - Poisson's ratio
   * - DENS
     - 
     - mass density

.. _MAT_VISCONEOHOOKE:

MAT_VISCONEOHOOKE
-----------------

visco-elastic neo-Hookean material law

::

      MAT <matID>  MAT_VISCONEOHOOKE YOUNGS_SLOW 0 POISSON 0 DENS 0 \
       YOUNGS_FAST 0 RELAX 0 THETA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - YOUNGS_SLOW
     - 
     - ???
   * - POISSON
     - 
     - ???
   * - DENS
     - 
     - ???
   * - YOUNGS_FAST
     - 
     - ???
   * - RELAX
     - 
     - ???
   * - THETA
     - 
     - ???

.. _MAT_VISCOANISO:

MAT_VISCOANISO
--------------

visco-elastic anisotropic fibre material law

::

      MAT <matID>  MAT_VISCOANISO KAPPA 0 MUE 0 DENS 0 K1 0 K2 0 \
       GAMMA 0 BETA_ISO 0 BETA_ANISO 0 RELAX_ISO 0 RELAX_ANISO 0 \
       MINSTRETCH 0 ELETHICKDIR 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - KAPPA
     - 
     - dilatation modulus
   * - MUE
     - 
     - Shear Modulus
   * - DENS
     - 
     - Density
   * - K1
     - 
     - Parameter for linear fiber stiffness
   * - K2
     - 
     - Parameter for exponetial fiber stiffness
   * - GAMMA
     - 
     - angle between fibers
   * - BETA_ISO
     - 
     - ratio between elasticities in generalized Maxweel |break| 
       body
   * - BETA_ANISO
     - 
     - ratio between elasticities in generalized Maxweel |break| 
       body
   * - RELAX_ISO
     - 
     - isotropic relaxation time
   * - RELAX_ANISO
     - 
     - anisotropic relaxation time
   * - MINSTRETCH
     - 
     - minimal principal stretch fibers do respond to
   * - ELETHICKDIR
     - 
     - Element thickness direction applies also to fibers |break| 
       (only sosh)

.. _MAT_Struct_Multiscale:

MAT_Struct_Multiscale
---------------------

Structural micro-scale approach: material parameters are calculated from microscale simulation

::

      MAT <matID>  MAT_Struct_Multiscale MICROFILE filename.dat MICRODIS_NUM 0 \
       INITVOL 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - MICROFILE
     - 
     - inputfile for microstructure
   * - MICRODIS_NUM
     - 
     - Number of microscale discretization
   * - INITVOL
     - yes
     - Initial volume of RVE

.. _MAT_ElastHyper:

MAT_ElastHyper
--------------

list/collection of hyperelastic materials, i.e. material IDs

::

      MAT <matID>  MAT_ElastHyper NUMMAT 0 MATIDS  DENS 0 POLYCONVEX 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - NUMMAT
     - 
     - number of materials/potentials in list
   * - MATIDS
     - 
     - the list material/potential IDs
   * - DENS
     - 
     - material mass density
   * - POLYCONVEX
     - yes
     - 1.0 if polyconvexity of system is checked

.. _MAT_ViscoElastHyper:

MAT_ViscoElastHyper
-------------------

Viscohyperelastic material compatible with the collection of hyperelastic materials

::

      MAT <matID>  MAT_ViscoElastHyper NUMMAT 0 MATIDS  DENS 0 POLYCONVEX 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - NUMMAT
     - 
     - number of materials/potentials in list
   * - MATIDS
     - 
     - the list material/potential IDs
   * - DENS
     - 
     - material mass density
   * - POLYCONVEX
     - yes
     - 1.0 if polyconvexity of system is checked

.. _MAT_PlasticElastHyper:

MAT_PlasticElastHyper
---------------------

list/collection of hyperelastic materials, i.e. material IDs

::

      MAT <matID>  MAT_PlasticElastHyper NUMMAT 0 MATIDS  DENS 0 \
       INITYIELD 0 POLYCONVEX 0 ISOHARD 0 EXPISOHARD 0 INFYIELD 0 \
       KINHARD 0 VISC 0 RATE_DEPENDENCY 1 VISC_SOFT 0 PL_SPIN_CHI 0 \
       rY_11 0 rY_22 0 rY_33 0 rY_12 0 rY_23 0 rY_13 0 CTE 0 INITTEMP 0 \
       YIELDSOFT 0 HARDSOFT 0 TAYLOR_QUINNEY 1

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - NUMMAT
     - 
     - number of materials/potentials in list
   * - MATIDS
     - 
     - the list material/potential IDs
   * - DENS
     - 
     - material mass density
   * - INITYIELD
     - 
     - initial yield stress
   * - POLYCONVEX
     - yes
     - 1.0 if polyconvexity of system is checked
   * - ISOHARD
     - yes
     - linear isotropic hardening modulus
   * - EXPISOHARD
     - yes
     - nonlinear isotropic hardening exponent
   * - INFYIELD
     - yes
     - saturation yield stress for nonlinear isotropic |break| 
       hardening
   * - KINHARD
     - yes
     - linear kinematic hardening modulus
   * - VISC
     - yes
     - Visco-Plasticity parameter 'eta' in Perzyna model
   * - RATE_DEPENDENCY
     - yes
     - Visco-Plasticity parameter 'eta' in Perzyna model
   * - VISC_SOFT
     - yes
     - Visco-Plasticity temperature dependency (eta = |break| 
       eta_0 * (1-(T-T_0)*x)
   * - PL_SPIN_CHI
     - yes
     - Plastic spin coupling parameter chi (often called |break| 
       eta)
   * - rY_11
     - yes
     - relative yield stress in fiber1-direction |break| 
       (Y_11/Y_0)
   * - rY_22
     - yes
     - relative yield stress in fiber2-direction |break| 
       (Y_22/Y_0)
   * - rY_33
     - yes
     - relative yield stress in fiber3-direction |break| 
       (Y_33/Y_0)
   * - rY_12
     - yes
     - relative shear yield stress in 12-direction |break| 
       (Y_12/Y_0)
   * - rY_23
     - yes
     - relative shear yield stress in 23-direction |break| 
       (Y_23/Y_0)
   * - rY_13
     - yes
     - relative shear yield stress in 13-direction |break| 
       (Y_13/Y_0)
   * - CTE
     - yes
     - coefficient of thermal expansion
   * - INITTEMP
     - yes
     - initial, reference temperature
   * - YIELDSOFT
     - yes
     - yield stress softening
   * - HARDSOFT
     - yes
     - hardening softening
   * - TAYLOR_QUINNEY
     - yes
     - Taylor-Quinney factor for plastic heat conversion

.. _MAT_PlasticElastHyperVCU:

MAT_PlasticElastHyperVCU
------------------------

list/collection of hyperelastic materials, i.e. material IDs

::

      MAT <matID>  MAT_PlasticElastHyperVCU NUMMAT 0 MATIDS  DENS 0 \
       INITYIELD 0 ISOHARD 0 EXPISOHARD 0 INFYIELD 0 KINHARD 0 VISC 0 \
       RATE_DEPENDENCY 1 VISC_SOFT 0 PL_SPIN_CHI 0 rY_11 0 rY_22 0 \
       rY_33 0 rY_12 0 rY_23 0 rY_13 0 CTE 0 INITTEMP 0 YIELDSOFT 0 \
       HARDSOFT 0 TAYLOR_QUINNEY 1 POLYCONVEX 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - NUMMAT
     - 
     - number of materials/potentials in list
   * - MATIDS
     - 
     - the list material/potential IDs
   * - DENS
     - 
     - material mass density
   * - INITYIELD
     - 
     - initial yield stress
   * - ISOHARD
     - yes
     - linear isotropic hardening modulus
   * - EXPISOHARD
     - yes
     - nonlinear isotropic hardening exponent
   * - INFYIELD
     - yes
     - saturation yield stress for nonlinear isotropic |break| 
       hardening
   * - KINHARD
     - yes
     - linear kinematic hardening modulus
   * - VISC
     - yes
     - Visco-Plasticity parameter 'eta' in Perzyna model
   * - RATE_DEPENDENCY
     - yes
     - Visco-Plasticity parameter 'eta' in Perzyna model
   * - VISC_SOFT
     - yes
     - Visco-Plasticity temperature dependency (eta = |break| 
       eta_0 * (1-(T-T_0)*x)
   * - PL_SPIN_CHI
     - yes
     - Plastic spin coupling parameter chi (often called |break| 
       eta)
   * - rY_11
     - yes
     - relative yield stress in fiber1-direction |break| 
       (Y_11/Y_0)
   * - rY_22
     - yes
     - relative yield stress in fiber2-direction |break| 
       (Y_22/Y_0)
   * - rY_33
     - yes
     - relative yield stress in fiber3-direction |break| 
       (Y_33/Y_0)
   * - rY_12
     - yes
     - relative shear yield stress in 12-direction |break| 
       (Y_12/Y_0)
   * - rY_23
     - yes
     - relative shear yield stress in 23-direction |break| 
       (Y_23/Y_0)
   * - rY_13
     - yes
     - relative shear yield stress in 13-direction |break| 
       (Y_13/Y_0)
   * - CTE
     - yes
     - coefficient of thermal expansion
   * - INITTEMP
     - yes
     - initial, reference temperature
   * - YIELDSOFT
     - yes
     - yield stress softening
   * - HARDSOFT
     - yes
     - hardening softening
   * - TAYLOR_QUINNEY
     - yes
     - Taylor-Quinney factor for plastic heat conversion
   * - POLYCONVEX
     - yes
     - 1.0 if polyconvexity of system is checked

.. _ELAST_CoupLogNeoHooke:

ELAST_CoupLogNeoHooke
---------------------

logarithmic neo-Hooke material acc. to Bonet and Wood

::

      MAT <matID>  ELAST_CoupLogNeoHooke MODE YN C1 0 C2 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - MODE
     - 
     - parameter set: YN (Young's modulus and Poisson's |break| 
       ration; default) or Lame (mue and lambda)
   * - C1
     - 
     - E or mue
   * - C2
     - 
     - nue or lambda

.. _ELAST_CoupSVK:

ELAST_CoupSVK
-------------

Saint-Venant-Kirchhoff as elastic summand

::

      MAT <matID>  ELAST_CoupSVK YOUNG 0 NUE 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - YOUNG
     - 
     - Young's modulus
   * - NUE
     - 
     - Poisson's ratio

.. _ELAST_CoupSimoPister:

ELAST_CoupSimoPister
--------------------

Simo-Pister type material

::

      MAT <matID>  ELAST_CoupSimoPister MUE 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - MUE
     - 
     - material constant

.. _ELAST_CoupLogMixNeoHooke:

ELAST_CoupLogMixNeoHooke
------------------------

mixed logarithmic neo-Hooke material

::

      MAT <matID>  ELAST_CoupLogMixNeoHooke MODE YN C1 0 C2 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - MODE
     - 
     - parameter set: YN (Young's modulus and Poisson's |break| 
       ration; default) or Lame (mue and lambda)
   * - C1
     - 
     - E or mue
   * - C2
     - 
     - nue or lambda

.. _ELAST_CoupExpPol:

ELAST_CoupExpPol
----------------

compressible, isochoric exponential material law for soft tissue

::

      MAT <matID>  ELAST_CoupExpPol A 0 B 0 C 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - A
     - 
     - material constant
   * - B
     - 
     - material constant linear I_1
   * - C
     - 
     - material constant linear J

.. _ELAST_CoupNeoHooke:

ELAST_CoupNeoHooke
------------------

compressible neo-Hooke material acc. to Holzapfel

::

      MAT <matID>  ELAST_CoupNeoHooke YOUNG 0 NUE 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - YOUNG
     - yes
     - Young's modulus
   * - NUE
     - yes
     - Poisson's ratio

.. _ELAST_CoupMooneyRivlin:

ELAST_CoupMooneyRivlin
----------------------

Mooney - Rivlin material acc. to Holzapfel

::

      MAT <matID>  ELAST_CoupMooneyRivlin C1 0 C2 0 C3 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - C1
     - yes
     - material constant
   * - C2
     - yes
     - material constant
   * - C3
     - yes
     - material constant

.. _ELAST_CoupBlatzKo:

ELAST_CoupBlatzKo
-----------------

Blatz and Ko material acc. to Holzapfel

::

      MAT <matID>  ELAST_CoupBlatzKo MUE 0 NUE 0 F 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - MUE
     - 
     - Shear modulus
   * - NUE
     - 
     - Poisson's ratio
   * - F
     - 
     - interpolation parameter

.. _ELAST_IsoNeoHooke:

ELAST_IsoNeoHooke
-----------------

isochoric part of neo-Hooke material acc. to Holzapfel

::

      MAT <matID>  ELAST_IsoNeoHooke MUE 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - MUE
     - 
     - Shear modulus

.. _ELAST_IsoOgden:

ELAST_IsoOgden
--------------

isochoric part of the one-term Ogden material

::

      MAT <matID>  ELAST_IsoOgden MUE 0 ALPHA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - MUE
     - 
     - Shear modulus
   * - ALPHA
     - 
     - Nonlinearity parameter

.. _ELAST_IsoVolHUDependentNeoHooke:

ELAST_IsoVolHUDependentNeoHooke
-------------------------------

isochoric and volumetric part of HU dependent neo-Hooke material

::

      MAT <matID>  ELAST_IsoVolHUDependentNeoHooke ALPHA_MAX 0 CT_MIN 0 \
       CT_MAX 0 NUE 0 BETA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - ALPHA_MAX
     - 
     - 
   * - CT_MIN
     - 
     - 
   * - CT_MAX
     - 
     - 
   * - NUE
     - 
     - 
   * - BETA
     - 
     - 

.. _ELAST_IsoVolAAAGasser:

ELAST_IsoVolAAAGasser
---------------------

isochoric and volumetric part of AAAGasser material (thrombus)

::

      MAT <matID>  ELAST_IsoVolAAAGasser CLUM 0 CMED 0 CABLUM 0 NUE 0 \
       BETA 0 MULUM 0 MUMED 0 MUABLUM 0 SIGMALUM 0 SIGMAMED 0 SIGMAABLUM 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - CLUM
     - 
     - luminal stiffness parameter (2.62e3)
   * - CMED
     - 
     - medial stiffness parameter (2.62e3)
   * - CABLUM
     - 
     - abluminal stiffness parameter (2.62e3)
   * - NUE
     - 
     - 
   * - BETA
     - 
     - 
   * - MULUM
     - yes
     - mu for luminal pdf, irrelevant for deterministic |break| 
       analysis
   * - MUMED
     - yes
     - mu for medial pdf, irrelevant for deterministic |break| 
       analysis
   * - MUABLUM
     - yes
     - mu for abluminal pdf, irrelevant for deterministic |break| 
       analysis
   * - SIGMALUM
     - yes
     - std for luminal pdf, irrelevant for deterministic |break| 
       analysis
   * - SIGMAMED
     - yes
     - std for medial pdf, irrelevant for deterministic |break| 
       analysis
   * - SIGMAABLUM
     - yes
     - std for abluminal pdf, irrelevant for |break| 
       deterministic analysis

.. _ELAST_IsoYeoh:

ELAST_IsoYeoh
-------------

isochoric part of  Yeoh material acc. to Holzapfel

::

      MAT <matID>  ELAST_IsoYeoh C1 0 C2 0 C3 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - C1
     - 
     - Linear modulus
   * - C2
     - 
     - Quadratic modulus
   * - C3
     - 
     - Cubic modulus

.. _ELAST_Iso1Pow:

ELAST_Iso1Pow
-------------

isochoric part of general power material

::

      MAT <matID>  ELAST_Iso1Pow C 0 D 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - C
     - 
     - material parameter
   * - D
     - 
     - exponent

.. _ELAST_Iso2Pow:

ELAST_Iso2Pow
-------------

isochoric part of general power material

::

      MAT <matID>  ELAST_Iso2Pow C 0 D 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - C
     - 
     - material parameter
   * - D
     - 
     - exponent

.. _ELAST_Coup1Pow:

ELAST_Coup1Pow
--------------

part of general power material

::

      MAT <matID>  ELAST_Coup1Pow C 0 D 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - C
     - 
     - material parameter
   * - D
     - 
     - exponent

.. _ELAST_Coup2Pow:

ELAST_Coup2Pow
--------------

part of general power material

::

      MAT <matID>  ELAST_Coup2Pow C 0 D 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - C
     - 
     - material parameter
   * - D
     - 
     - exponent

.. _ELAST_Coup3Pow:

ELAST_Coup3Pow
--------------

part of general power material

::

      MAT <matID>  ELAST_Coup3Pow C 0 D 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - C
     - 
     - material parameter
   * - D
     - 
     - exponent

.. _ELAST_Coup13aPow:

ELAST_Coup13aPow
----------------

hyperelastic potential summand for multiplicative coupled invariants I1 and I3

::

      MAT <matID>  ELAST_Coup13aPow C 0 D 0 A 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - C
     - 
     - material parameter
   * - D
     - 
     - exponent of all
   * - A
     - 
     - negative exponent of I3

.. _ELAST_IsoExpoPow:

ELAST_IsoExpoPow
----------------

isochoric part of  exponential material acc. to Holzapfel

::

      MAT <matID>  ELAST_IsoExpoPow K1 0 K2 0 C 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - K1
     - 
     - material parameter
   * - K2
     - 
     - material parameter
   * - C
     - 
     - exponent

.. _ELAST_IsoMooneyRivlin:

ELAST_IsoMooneyRivlin
---------------------

isochoric part of  Mooney-Rivlin material acc. to Holzapfel

::

      MAT <matID>  ELAST_IsoMooneyRivlin C1 0 C2 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - C1
     - 
     - Linear modulus for first invariant
   * - C2
     - 
     - Linear modulus for second invariant

.. _ELAST_IsoMuscle_Blemker:

ELAST_IsoMuscle_Blemker
-----------------------

anisotropic Blemker muscle material

::

      MAT <matID>  ELAST_IsoMuscle_Blemker G1 0 G2 0 P1 0 P2 0 SIGMAMAX 0 \
       LAMBDAOFL 0 LAMBDASTAR 0 ALPHA 0 BETA 0 ACTSTARTTIME 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - G1
     - 
     - muscle along fiber shear modulus
   * - G2
     - 
     - muscle cross fiber shear modulus
   * - P1
     - 
     - linear material parameter for passive along-fiber |break| 
       response
   * - P2
     - 
     - exponential material parameter for passive |break| 
       along-fiber response
   * - SIGMAMAX
     - 
     - maximal active isometric stress
   * - LAMBDAOFL
     - 
     - optimal fiber stretch
   * - LAMBDASTAR
     - 
     - stretch at which the normalized passive fiber |break| 
       force becomes linear
   * - ALPHA
     - 
     - tetanised activation level,
   * - BETA
     - 
     - constant scaling tanh-type activation function
   * - ACTSTARTTIME
     - 
     - starting time of muscle activation

.. _ELAST_IsoTestMaterial:

ELAST_IsoTestMaterial
---------------------

test material to test elasthyper-toolbox

::

      MAT <matID>  ELAST_IsoTestMaterial C1 0 C2 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - C1
     - 
     - Modulus for first invariant
   * - C2
     - 
     - Modulus for second invariant

.. _ELAST_RemodelFiber:

ELAST_RemodelFiber
------------------

General fiber material for remodeling

::

      MAT <matID>  ELAST_RemodelFiber NUMMAT 0 MATIDS  TDECAY 0 GROWTHFAC 0 \
       COLMASSFRAC  DEPOSITIONSTRETCH 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - NUMMAT
     - 
     - number of materials/potentials in list
   * - MATIDS
     - 
     - the list material/potential IDs
   * - TDECAY
     - 
     - decay time of Poisson (degradation) process
   * - GROWTHFAC
     - yes
     - time constant for collagen growth
   * - COLMASSFRAC
     - yes
     - initial mass fraction of first collagen fiber |break| 
       family in constraint mixture
   * - DEPOSITIONSTRETCH
     - 
     - deposition stretch

.. _ELAST_VolSussmanBathe:

ELAST_VolSussmanBathe
---------------------

volumetric part of  SussmanBathe material

::

      MAT <matID>  ELAST_VolSussmanBathe KAPPA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - KAPPA
     - 
     - dilatation modulus

.. _ELAST_VolPenalty:

ELAST_VolPenalty
----------------

Penalty formulation for the volumetric part

::

      MAT <matID>  ELAST_VolPenalty EPSILON 0 GAMMA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - EPSILON
     - 
     - penalty parameter
   * - GAMMA
     - 
     - penalty parameter

.. _ELAST_VolOgden:

ELAST_VolOgden
--------------

Ogden formulation for the volumetric part

::

      MAT <matID>  ELAST_VolOgden KAPPA 0 BETA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - KAPPA
     - 
     - dilatation modulus
   * - BETA
     - 
     - empiric constant

.. _ELAST_VolPow:

ELAST_VolPow
------------

Power law formulation for the volumetric part

::

      MAT <matID>  ELAST_VolPow A 0 EXPON 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - A
     - 
     - prefactor of power law
   * - EXPON
     - 
     - exponent of power law

.. _ELAST_CoupAnisoExpoActive:

ELAST_CoupAnisoExpoActive
-------------------------

anisotropic active fiber

::

      MAT <matID>  ELAST_CoupAnisoExpoActive K1 0 K2 0 GAMMA 0 K1COMP 0 \
       K2COMP 0 STR_TENS_ID 0 INIT 1 ADAPT_ANGLE No S 0 LAMBDAMAX 0 \
       LAMBDA0 0 DENS 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - K1
     - 
     - linear constant
   * - K2
     - 
     - exponential constant
   * - GAMMA
     - 
     - angle
   * - K1COMP
     - 
     - linear constant
   * - K2COMP
     - 
     - exponential constant
   * - STR_TENS_ID
     - 
     - MAT ID for definition of Structural Tensor
   * - INIT
     - yes
     - initialization modus for fiber alignment
   * - ADAPT_ANGLE
     - yes
     - adapt angle during remodeling
   * - S
     - 
     - maximum contractile stress
   * - LAMBDAMAX
     - 
     - stretch at maximum active force generation
   * - LAMBDA0
     - 
     - stretch at zero active force generation
   * - DENS
     - 
     - total reference mass density of constrained |break| 
       mixture

.. _ELAST_CoupAnisoExpo:

ELAST_CoupAnisoExpo
-------------------

anisotropic part with one exp. fiber

::

      MAT <matID>  ELAST_CoupAnisoExpo K1 0 K2 0 GAMMA 0 K1COMP 0 \
       K2COMP 0 STR_TENS_ID 0 INIT 1 ADAPT_ANGLE No FIBER_ID 1

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - K1
     - 
     - linear constant
   * - K2
     - 
     - exponential constant
   * - GAMMA
     - 
     - angle
   * - K1COMP
     - 
     - linear constant
   * - K2COMP
     - 
     - exponential constant
   * - STR_TENS_ID
     - 
     - MAT ID for definition of Structural Tensor
   * - INIT
     - yes
     - initialization modus for fiber alignment
   * - ADAPT_ANGLE
     - yes
     - adapt angle during remodeling
   * - FIBER_ID
     - yes
     - Id of the fiber to be used (1 for first fiber, |break| 
       default)

.. _ELAST_CoupAnisoExpoShear:

ELAST_CoupAnisoExpoShear
------------------------

Exponential shear behavior between two fibers

::

      MAT <matID>  ELAST_CoupAnisoExpoShear K1 0 K2 0 GAMMA 0 K1COMP 0 \
       K2COMP 0 INIT 1 FIBER_IDS 0 0 

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - K1
     - 
     - linear constant
   * - K2
     - 
     - exponential constant
   * - GAMMA
     - 
     - angle
   * - K1COMP
     - 
     - linear constant
   * - K2COMP
     - 
     - exponential constant
   * - INIT
     - yes
     - initialization modus for fiber alignment
   * - FIBER_IDS
     - 
     - Ids of the two fibers to be used (1 for the first |break| 
       fiber, 2 for the second, default)

.. _ELAST_CoupAnisoPow:

ELAST_CoupAnisoPow
------------------

anisotropic part with one pow-like fiber

::

      MAT <matID>  ELAST_CoupAnisoPow K 0 D1 0 D2 0 ACTIVETHRES 1 \
       STR_TENS_ID 0 FIBER 1 GAMMA 0 INIT 1 ADAPT_ANGLE No

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - K
     - 
     - linear constant
   * - D1
     - 
     - exponential constant for fiber invariant
   * - D2
     - 
     - exponential constant for system
   * - ACTIVETHRES
     - yes
     - Deformation threshold for activating fibers. |break| 
       Default: 1.0 (off at compression); If 0.0 (always |break| 
       active)
   * - STR_TENS_ID
     - 
     - MAT ID for definition of Structural Tensor
   * - FIBER
     - yes
     - Number of the fiber family contained in the |break| 
       element
   * - GAMMA
     - yes
     - angle
   * - INIT
     - yes
     - initialization modus for fiber alignment
   * - ADAPT_ANGLE
     - yes
     - adapt angle during remodeling

.. _ELAST_CoupAnisoExpoTwoCoup:

ELAST_CoupAnisoExpoTwoCoup
--------------------------

anisotropic part with two exp. fibers

::

      MAT <matID>  ELAST_CoupAnisoExpoTwoCoup A4 0 B4 0 A6 0 B6 0 \
       A8 0 B8 0 GAMMA 0 STR_TENS_ID 0 INIT 1 FIB_COMP Yes ADAPT_ANGLE No

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - A4
     - 
     - linear anisotropic constant for fiber 1
   * - B4
     - 
     - exponential anisotropic constant for fiber 1
   * - A6
     - 
     - linear anisotropic constant for fiber 2
   * - B6
     - 
     - exponential anisotropic constant for fiber 2
   * - A8
     - 
     - linear anisotropic constant for fiber 1 relating |break| 
       fiber 2
   * - B8
     - 
     - exponential anisotropic constant for fiber 1 |break| 
       relating fiber 2
   * - GAMMA
     - 
     - angle
   * - STR_TENS_ID
     - 
     - MAT ID for definition of Structural Tensor
   * - INIT
     - yes
     - initialization modus for fiber alignment
   * - FIB_COMP
     - yes
     - fibers support compression: yes (true) or no |break| 
       (false)
   * - ADAPT_ANGLE
     - yes
     - adapt angle during remodeling

.. _ELAST_CoupAnisoNeoHooke:

ELAST_CoupAnisoNeoHooke
-----------------------

anisotropic part with one neo Hookean fiber

::

      MAT <matID>  ELAST_CoupAnisoNeoHooke C 0 GAMMA 0 STR_TENS_ID 0 \
       INIT 1 ADAPT_ANGLE No

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - C
     - 
     - linear constant
   * - GAMMA
     - 
     - angle
   * - STR_TENS_ID
     - 
     - MAT ID for definition of Structural Tensor
   * - INIT
     - yes
     - initialization modus for fiber alignment
   * - ADAPT_ANGLE
     - yes
     - adapt angle during remodeling

.. _ELAST_AnisoActiveStress_Evolution:

ELAST_AnisoActiveStress_Evolution
---------------------------------

anisotropic part with one fiber with coefficient given by a simplification of the activation-contraction law of Bestel-Clement-Sorine-2001

::

      MAT <matID>  ELAST_AnisoActiveStress_Evolution SIGMA 0 TAUC0 0 \
       MAX_ACTIVATION 0 MIN_ACTIVATION 0 SOURCE_ACTIVATION 0 ACTIVATION_THRES 0 \
       STRAIN_DEPENDENCY No LAMBDA_LOWER 1 LAMBDA_UPPER 1 GAMMA 0 \
       STR_TENS_ID 0 INIT 1 ADAPT_ANGLE No

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - SIGMA
     - 
     - Contractility (maximal stress)
   * - TAUC0
     - 
     - Initial value for the active stress
   * - MAX_ACTIVATION
     - 
     - Maximal value for the rescaled activation
   * - MIN_ACTIVATION
     - 
     - Minimal value for the rescaled activation
   * - SOURCE_ACTIVATION
     - 
     - Where the activation comes from: 0=scatra , >0 Id |break| 
       for FUNCT
   * - ACTIVATION_THRES
     - 
     - Threshold for activation (contraction starts when |break| 
       activation function is larger than this value, |break| 
       relaxes otherwise)
   * - STRAIN_DEPENDENCY
     - yes
     - model strain dependency of contractility |break| 
       (Frank-Starling law): no (false) or yes (true)
   * - LAMBDA_LOWER
     - yes
     - lower fiber stretch for Frank-Starling law
   * - LAMBDA_UPPER
     - yes
     - upper fiber stretch for Frank-Starling law
   * - GAMMA
     - yes
     - angle
   * - STR_TENS_ID
     - 
     - MAT ID for definition of Structural Tensor
   * - INIT
     - yes
     - initialization mode for fiber alignment
   * - ADAPT_ANGLE
     - yes
     - adapt angle during remodeling

.. _ELAST_CoupAnisoNeoHooke_VarProp:

ELAST_CoupAnisoNeoHooke_VarProp
-------------------------------

anisotropic part with one neo Hookean fiber with variable coefficient

::

      MAT <matID>  ELAST_CoupAnisoNeoHooke_VarProp C 0 SOURCE_ACTIVATION 0 \
       GAMMA 0 THETA 0 STR_TENS_ID 0 INIT 1 ADAPT_ANGLE No

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - C
     - 
     - linear constant
   * - SOURCE_ACTIVATION
     - 
     - Where the activation comes from: 0=scatra , >0 Id |break| 
       for FUNCT
   * - GAMMA
     - yes
     - azimuth angle
   * - THETA
     - yes
     - polar angle
   * - STR_TENS_ID
     - 
     - MAT ID for definition of Structural Tensor
   * - INIT
     - yes
     - initialization mode for fiber alignment
   * - ADAPT_ANGLE
     - yes
     - adapt angle during remodeling

.. _ELAST_IsoAnisoExpo:

ELAST_IsoAnisoExpo
------------------

anisotropic part with one exp. fiber

::

      MAT <matID>  ELAST_IsoAnisoExpo K1 0 K2 0 GAMMA 0 K1COMP 0 \
       K2COMP 0 STR_TENS_ID 0 INIT 1 ADAPT_ANGLE No

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - K1
     - 
     - linear constant
   * - K2
     - 
     - exponential constant
   * - GAMMA
     - 
     - angle
   * - K1COMP
     - 
     - linear constant
   * - K2COMP
     - 
     - exponential constant
   * - STR_TENS_ID
     - 
     - MAT ID for definition of Structural Tensor
   * - INIT
     - yes
     - initialization modus for fiber alignment
   * - ADAPT_ANGLE
     - yes
     - adapt angle during remodeling

.. _ELAST_StructuralTensor:

ELAST_StructuralTensor
----------------------

Parameter for structural tensor strategy in anisotropic materials

::

      MAT <matID>  ELAST_StructuralTensor STRATEGY Standard DISTR none \
       C1 1 C2 0 C3 0 C4 1e+16

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - STRATEGY
     - 
     - Strategy for evaluation of structural tensor: |break| 
       Standard (default), ByDistributionFunction, |break| 
       DispersedTransverselyIsotropic
   * - DISTR
     - yes
     - Type of distribution function around mean |break| 
       direction: none, Bingham, vonMisesFisher
   * - C1
     - yes
     - constant 1 for distribution function
   * - C2
     - yes
     - constant 2 for distribution function
   * - C3
     - yes
     - constant 3 for distribution function
   * - C4
     - yes
     - constant 4 for distribution function

.. _ELAST_CoupTransverselyIsotropic:

ELAST_CoupTransverselyIsotropic
-------------------------------

transversely part of a simple othotropic, transversely isotropic hyperelastic constitutive equation

::

      MAT <matID>  ELAST_CoupTransverselyIsotropic ALPHA 0 BETA 0 \
       GAMMA 0 ANGLE 0 STR_TENS_ID 0 FIBER 1 INIT 1

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - ALPHA
     - 
     - 1-st constant
   * - BETA
     - 
     - 2-nd constant
   * - GAMMA
     - 
     - 3-rd constant
   * - ANGLE
     - 
     - fiber angle
   * - STR_TENS_ID
     - 
     - MAT ID for definition of Structural Tensor
   * - FIBER
     - yes
     - exponential constant
   * - INIT
     - yes
     - initialization modus for fiber alignment

.. _ELAST_CoupVarga:

ELAST_CoupVarga
---------------

Varga material acc. to Holzapfel

::

      MAT <matID>  ELAST_CoupVarga MUE 0 BETA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - MUE
     - 
     - Shear modulus
   * - BETA
     - 
     - 'Anti-modulus'

.. _ELAST_IsoVarga:

ELAST_IsoVarga
--------------

Isochoric Varga material acc. to Holzapfel

::

      MAT <matID>  ELAST_IsoVarga MUE 0 BETA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - MUE
     - 
     - Shear modulus
   * - BETA
     - 
     - 'Anti-modulus'

.. _VISCO_CoupMyocard:

VISCO_CoupMyocard
-----------------

Isotropic viscous contribution of myocardial matrix

::

      MAT <matID>  VISCO_CoupMyocard N 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - N
     - 
     - material parameter

.. _VISCO_IsoRateDep:

VISCO_IsoRateDep
----------------

Isochoric rate dependent viscous material

::

      MAT <matID>  VISCO_IsoRateDep N 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - N
     - 
     - material parameter

.. _VISCO_GenMax:

VISCO_GenMax
------------

Viscous contribution according to SLS-Model

::

      MAT <matID>  VISCO_GenMax TAU 0 BETA 0 SOLVE OST

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - TAU
     - 
     - relaxation parameter
   * - BETA
     - 
     - emphasis of viscous to elastic part
   * - SOLVE
     - 
     - Solution of evolution equation via: OST (default) |break| 
       or CONVOL (convolution integral)

.. _VISCO_Fract:

VISCO_Fract
-----------

Viscous contribution according to FSLS-Model

::

      MAT <matID>  VISCO_Fract TAU 0 ALPHA 0 BETA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - TAU
     - 
     - relaxation parameter
   * - ALPHA
     - 
     - fractional order derivative
   * - BETA
     - 
     - emphasis of viscous to elastic part

.. _VISCO_PART:

VISCO_PART
----------

Viscous contribution of a viscoelastic Branch

::

      MAT <matID>  VISCO_PART TAU 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - TAU
     - 
     - dynamic viscosity divided by young's modulus of |break| 
       the branch

.. _VISCO_GeneralizedGenMax:

VISCO_GeneralizedGenMax
-----------------------

Viscoelastic Branches of generalized Maxwell

::

      MAT <matID>  VISCO_GeneralizedGenMax NUMBRANCH 0 MATIDS  SOLVE CONVOL

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - NUMBRANCH
     - 
     - number of viscoelastic branches
   * - MATIDS
     - 
     - the list material IDs
   * - SOLVE
     - 
     - Solution for evolution equation: OST (default) or |break| 
       CONVOL (convolution integral)

.. _VISCO_BRANCH:

VISCO_BRANCH
------------

Viscoelastic Branch (viscous and elastic contribution)

::

      MAT <matID>  VISCO_BRANCH NUMMAT 0 MATIDS 

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - NUMMAT
     - 
     - number of materials in the viscoelastic branch
   * - MATIDS
     - 
     - the list material IDs

.. _MAT_CNST_ART:

MAT_CNST_ART
------------

artery with constant properties

::

      MAT <matID>  MAT_CNST_ART VISCOSITY 0 DENS 0 YOUNG 0 NUE 0 \
       TH 0 PEXT1 0 PEXT2 0 VISCOSITYLAW CONSTANT BLOOD_VISC_SCALE_DIAM_TO_MICRONS 1 \
       VARYING_DIAMETERLAW CONSTANT VARYING_DIAMETER_FUNCTION -1 \
       COLLAPSE_THRESHOLD -1

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - VISCOSITY
     - 
     - viscosity (for CONSTANT viscosity law taken as |break| 
       blood viscosity, for BLOOD viscosity law taken as |break| 
       the viscosity of blood plasma)
   * - DENS
     - 
     - density of blood
   * - YOUNG
     - 
     - artery Youngs modulus of elasticity
   * - NUE
     - 
     - Poissons ratio of artery fiber
   * - TH
     - 
     - artery thickness
   * - PEXT1
     - 
     - artery fixed external pressure 1
   * - PEXT2
     - 
     - artery fixed external pressure 2
   * - VISCOSITYLAW
     - yes
     - type of viscosity law, CONSTANT (default) or BLOOD
   * - BLOOD_VISC_SCALE_DIAM_TO_MICRONS
     - yes
     - used to scale the diameter for blood viscosity law |break| 
       to microns if your problem is not given in |break| 
       microns, e.g., if you use mms, set this parameter |break| 
       to 1.0e3
   * - VARYING_DIAMETERLAW
     - yes
     - type of varying diameter law, CONSTANT (default) |break| 
       or BY_FUNCTION
   * - VARYING_DIAMETER_FUNCTION
     - yes
     - function for varying diameter law
   * - COLLAPSE_THRESHOLD
     - yes
     - Collapse threshold for diameter (below this |break| 
       diameter element is assumed to be collapsed with |break| 
       zero diameter and is not evaluated)

.. _THERM_FourierIso:

THERM_FourierIso
----------------

isotropic (linear) Fourier's law of heat conduction

::

      MAT <matID>  THERM_FourierIso CAPA 0 CONDUCT 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - CAPA
     - 
     - volumetric heat capacity
   * - CONDUCT
     - 
     - thermal conductivity

.. _MAT_soret:

MAT_soret
---------

material for heat transport due to Fourier-type thermal conduction and the Soret effect

::

      MAT <matID>  MAT_soret CAPA 0 CONDUCT 0 SORET 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - CAPA
     - 
     - volumetric heat capacity
   * - CONDUCT
     - 
     - thermal conductivity
   * - SORET
     - 
     - Soret coefficient

.. _MAT_GrowthVolumetric:

MAT_GrowthVolumetric
--------------------

volumetric growth

::

      MAT <matID>  MAT_GrowthVolumetric GROWTHLAW 0 IDMATELASTIC 0 \
       STARTTIME 0 ENDTIME 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - GROWTHLAW
     - 
     - number of growth law in input file
   * - IDMATELASTIC
     - 
     - number of elastic material in input file: MAT |break| 
       IDMATELASTIC ...
   * - STARTTIME
     - 
     - start growth after this time
   * - ENDTIME
     - 
     - end growth after this time

.. _MAT_Membrane_ElastHyper:

MAT_Membrane_ElastHyper
-----------------------

list/collection of hyperelastic materials for membranes, i.e. material IDs

::

      MAT <matID>  MAT_Membrane_ElastHyper NUMMAT 0 MATIDS  DENS 0 \
       POLYCONVEX 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - NUMMAT
     - 
     - number of materials/potentials in list
   * - MATIDS
     - 
     - the list material/potential IDs
   * - DENS
     - 
     - material mass density
   * - POLYCONVEX
     - yes
     - 1.0 if polyconvexity of system is checked

.. _MAT_Membrane_ActiveStrain:

MAT_Membrane_ActiveStrain
-------------------------

active strain membrane material

::

      MAT <matID>  MAT_Membrane_ActiveStrain MATIDPASSIVE 0 SCALIDVOLTAGE 0 \
       DENS 0 BETA1 0 BETA2 0 VOLTHRESH 0 ALPHA1 0 ALPHA2 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - MATIDPASSIVE
     - 
     - MATID for the passive material
   * - SCALIDVOLTAGE
     - 
     - ID of the scalar that represents the (SMC) voltage
   * - DENS
     - 
     - material mass density
   * - BETA1
     - 
     - Ca2+ dynamics
   * - BETA2
     - 
     - opening dynamics of the VDCC
   * - VOLTHRESH
     - 
     - voltage threshold for activation
   * - ALPHA1
     - 
     - intensity of contraction in fiber direction 1
   * - ALPHA2
     - 
     - intensity of contraction in fiber direction 2

.. _MAT_GrowthRemodel_ElastHyper:

MAT_GrowthRemodel_ElastHyper
----------------------------

growth and remodeling

::

      MAT <matID>  MAT_GrowthRemodel_ElastHyper NUMMATRF 0 NUMMATEL3D 0 \
       NUMMATEL2D 0 MATIDSRF  MATIDSEL3D  MATIDSEL2D  MATIDELPENALTY -1 \
       ELMASSFRAC 0 DENS 0 PRESTRETCHELASTINCIR 0 PRESTRETCHELASTINAX 0 \
       THICKNESS -1 MEANPRESSURE -1 RADIUS -1 DAMAGE 0 GROWTHTYPE 0 \
       LOCTIMEINT 0 MEMBRANE -1 CYLINDER -1

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - NUMMATRF
     - 
     - number of remodelfiber materials in list
   * - NUMMATEL3D
     - yes
     - number of 3d elastin matrix materials/potentials |break| 
       in list
   * - NUMMATEL2D
     - 
     - number of 2d elastin matrix materials/potentials |break| 
       in list
   * - MATIDSRF
     - 
     - the list remodelfiber material IDs
   * - MATIDSEL3D
     - yes
     - the list 3d elastin matrix material/potential IDs
   * - MATIDSEL2D
     - 
     - the list 2d elastin matrix material/potential IDs
   * - MATIDELPENALTY
     - yes
     - penalty material ID
   * - ELMASSFRAC
     - 
     - initial mass fraction of elastin matrix in |break| 
       constraint mixture
   * - DENS
     - 
     - material mass density
   * - PRESTRETCHELASTINCIR
     - 
     - circumferential prestretch of elastin matrix
   * - PRESTRETCHELASTINAX
     - 
     - axial prestretch of elastin matrix
   * - THICKNESS
     - yes
     - reference wall thickness of the idealized |break| 
       cylindrical aneurysm [m]
   * - MEANPRESSURE
     - yes
     - mean blood pressure [Pa]
   * - RADIUS
     - yes
     - inner radius of the idealized cylindrical aneurysm |break| 
       [m]
   * - DAMAGE
     - 
     - 1: elastin damage after prestressing,0: no elastin |break| 
       damage
   * - GROWTHTYPE
     - 
     - flag to decide what type of collagen growth is |break| 
       used: 1: anisotropic growth; 0: isotropic growth
   * - LOCTIMEINT
     - 
     - flag to decide what type of local time integration |break| 
       scheme is used: 1: Backward Euler Method; 0: |break| 
       Forward Euler Method
   * - MEMBRANE
     - yes
     - Flag whether Hex or Membrane elements are used ( |break| 
       Membrane: 1, Hex: Everything else )
   * - CYLINDER
     - yes
     - Flag that geometry is a cylinder. 1: aligned in |break| 
       x-direction; 2: y-direction; 3: z-direction

.. _MAT_MultiplicativeSplitDefgradElastHyper:

MAT_MultiplicativeSplitDefgradElastHyper
----------------------------------------

multiplicative split of deformation gradient

::

      MAT <matID>  MAT_MultiplicativeSplitDefgradElastHyper NUMMATEL 0 \
       MATIDSEL  NUMFACINEL 0 INELDEFGRADFACIDS  DENS 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - NUMMATEL
     - 
     - number of elastic materials/potentials in list
   * - MATIDSEL
     - 
     - the list of elastic material/potential IDs
   * - NUMFACINEL
     - 
     - number of factors of inelastic deformation |break| 
       gradient
   * - INELDEFGRADFACIDS
     - 
     - the list of inelastic deformation gradient factor |break| 
       IDs
   * - DENS
     - 
     - material mass density

.. _MAT_InelasticDefgradNoGrowth:

MAT_InelasticDefgradNoGrowth
----------------------------

no volume change, i.e. the inelastic deformation gradient is the identity tensor

::

      MAT <matID>  MAT_InelasticDefgradNoGrowth

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - no |break| 
       parameters
     - 
     - 

.. _MAT_InelasticDefgradLinScalarIso:

MAT_InelasticDefgradLinScalarIso
--------------------------------

scalar dependent isotropic growth law; volume change linearly dependent on scalar (in material configuration)

::

      MAT <matID>  MAT_InelasticDefgradLinScalarIso SCALAR1 0 SCALAR1_MolarGrowthFac 0 \
       SCALAR1_RefConc 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - SCALAR1
     - 
     - number of growth inducing scalar
   * - SCALAR1_MolarGrowthFac
     - 
     - isotropic molar growth factor due to scalar 1
   * - SCALAR1_RefConc
     - 
     - reference concentration of scalar 1 causing no |break| 
       strains

.. _MAT_InelasticDefgradLinScalarAniso:

MAT_InelasticDefgradLinScalarAniso
----------------------------------

scalar dependent anisotropic growth law; growth in direction as given in input-file; volume change linearly dependent on scalar (in material configuration)

::

      MAT <matID>  MAT_InelasticDefgradLinScalarAniso SCALAR1 0 SCALAR1_MolarGrowthFac 0 \
       SCALAR1_RefConc 0 NUMSPACEDIM 0 GrowthDirection 

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - SCALAR1
     - 
     - number of growth inducing scalar
   * - SCALAR1_MolarGrowthFac
     - 
     - anisotropic molar growth factor due to scalar 1
   * - SCALAR1_RefConc
     - 
     - reference concentration of scalar 1 causing no |break| 
       strains
   * - NUMSPACEDIM
     - 
     - Number of space dimension (only 3 valid)
   * - GrowthDirection
     - 
     - vector that defines the growth direction

.. _MAT_InelasticDefgradPolyIntercalFracIso:

MAT_InelasticDefgradPolyIntercalFracIso
---------------------------------------

scalar dependent isotropic growth law; volume change nonlinearly dependent on the intercalation fraction, that is calculated using the scalar concentration (in material configuration)

::

      MAT <matID>  MAT_InelasticDefgradPolyIntercalFracIso SCALAR1 0 \
       SCALAR1_RefConc 0 POLY_PARA_NUM 0 POLY_PARAMS  X_min 0 X_max 0 \
       MATID 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - SCALAR1
     - 
     - number of growth inducing scalar
   * - SCALAR1_RefConc
     - 
     - reference concentration of scalar 1 causing no |break| 
       strains
   * - POLY_PARA_NUM
     - 
     - number of polynomial coefficients
   * - POLY_PARAMS
     - 
     - coefficients of polynomial
   * - X_min
     - 
     - lower bound of validity of polynomial
   * - X_max
     - 
     - upper bound of validity of polynomial
   * - MATID
     - 
     - material ID of the corresponding scatra material

.. _MAT_InelasticDefgradPolyIntercalFracAniso:

MAT_InelasticDefgradPolyIntercalFracAniso
-----------------------------------------

scalar dependent anisotropic growth law; growth in direction as given in input-file; volume change nonlinearly dependent on the intercalation fraction, that is calculated using the scalar concentration (in material configuration)

::

      MAT <matID>  MAT_InelasticDefgradPolyIntercalFracAniso SCALAR1 0 \
       SCALAR1_RefConc 0 NUMSPACEDIM 0 GrowthDirection  POLY_PARA_NUM 0 \
       POLY_PARAMS  X_min 0 X_max 0 MATID 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - SCALAR1
     - 
     - number of growth inducing scalar
   * - SCALAR1_RefConc
     - 
     - reference concentration of scalar 1 causing no |break| 
       strains
   * - NUMSPACEDIM
     - 
     - Number of space dimension (only 3 valid)
   * - GrowthDirection
     - 
     - vector that defines the growth direction
   * - POLY_PARA_NUM
     - 
     - number of polynomial coefficients
   * - POLY_PARAMS
     - 
     - coefficients of polynomial
   * - X_min
     - 
     - lower bound of validity of polynomial
   * - X_max
     - 
     - upper bound of validity of polynomial
   * - MATID
     - 
     - material ID of the corresponding scatra material

.. _MAT_InelasticDefgradLinTempIso:

MAT_InelasticDefgradLinTempIso
------------------------------

Temperature dependent growth law. Volume change linearly dependent on temperature

::

      MAT <matID>  MAT_InelasticDefgradLinTempIso Temp_GrowthFac 0 \
       RefTemp 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - Temp_GrowthFac
     - 
     - isotropic growth factor due to temperature
   * - RefTemp
     - 
     - reference temperature causing no strains

.. _MAT_ScDepInterp:

MAT_ScDepInterp
---------------

integration point based and scalar dependent interpolation between to materials

::

      MAT <matID>  MAT_ScDepInterp IDMATZEROSC 0 IDMATUNITSC 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - IDMATZEROSC
     - 
     - material for lambda equal to zero
   * - IDMATUNITSC
     - 
     - material for lambda equal to one

.. _MAT_GrowthAnisoStrain:

MAT_GrowthAnisoStrain
---------------------

growth law depending on elastic stretch in fiber direction, growth in fiber direction

::

      MAT <matID>  MAT_GrowthAnisoStrain TAU 0 TAU_REV 0 THETA_MIN 0 \
       THETA_MAX 0 GAMMA 0 GAMMA_REV 0 LAMBDA_CRIT 0 TOL 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - TAU
     - 
     - growth time scale
   * - TAU_REV
     - 
     - reverse growth time scale
   * - THETA_MIN
     - 
     - lower limit for growth stretch
   * - THETA_MAX
     - 
     - upper limit for growth stretch
   * - GAMMA
     - 
     - growth non-linearity
   * - GAMMA_REV
     - 
     - reverse growth non-linearity
   * - LAMBDA_CRIT
     - 
     - critical fiber stretch
   * - TOL
     - 
     - tolerance for local Newton iteration

.. _MAT_GrowthAnisoStress:

MAT_GrowthAnisoStress
---------------------

growth law depending on elastic Mandel stress, growth perpendicular to fiber direction

::

      MAT <matID>  MAT_GrowthAnisoStress TAU 0 TAU_REV 0 THETA_MIN 0 \
       THETA_MAX 0 GAMMA 0 GAMMA_REV 0 P_CRIT 0 TOL 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - TAU
     - 
     - growth time scale
   * - TAU_REV
     - 
     - reverse growth time scale
   * - THETA_MIN
     - 
     - lower limit for growth stretch
   * - THETA_MAX
     - 
     - upper limit for growth stretch
   * - GAMMA
     - 
     - growth non-linearity
   * - GAMMA_REV
     - 
     - reverse growth non-linearity
   * - P_CRIT
     - 
     - critical pressure
   * - TOL
     - 
     - tolerance for local Newton iteration

.. _MAT_GrowthAnisoStrainConstTrig:

MAT_GrowthAnisoStrainConstTrig
------------------------------

growth law depending on prescribed constant elastic stretch in fiber direction, growth in fiber direction

::

      MAT <matID>  MAT_GrowthAnisoStrainConstTrig TAU 0 TAU_REV 0 \
       THETA_MIN 0 THETA_MAX 0 GAMMA 0 GAMMA_REV 0 LAMBDA_CRIT 0 \
       TOL 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - TAU
     - 
     - growth time scale
   * - TAU_REV
     - 
     - reverse growth time scale
   * - THETA_MIN
     - 
     - lower limit for growth stretch
   * - THETA_MAX
     - 
     - upper limit for growth stretch
   * - GAMMA
     - 
     - growth non-linearity
   * - GAMMA_REV
     - 
     - reverse growth non-linearity
   * - LAMBDA_CRIT
     - 
     - critical fiber stretch
   * - TOL
     - 
     - tolerance for local Newton iteration

.. _MAT_GrowthAnisoStressConstTrig:

MAT_GrowthAnisoStressConstTrig
------------------------------

growth law depending on prescribed constant elastic Mandel stress, growth perpendicular to fiber direction

::

      MAT <matID>  MAT_GrowthAnisoStressConstTrig TAU 0 TAU_REV 0 \
       THETA_MIN 0 THETA_MAX 0 GAMMA 0 GAMMA_REV 0 P_CRIT 0 TOL 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - TAU
     - 
     - growth time scale
   * - TAU_REV
     - 
     - reverse growth time scale
   * - THETA_MIN
     - 
     - lower limit for growth stretch
   * - THETA_MAX
     - 
     - upper limit for growth stretch
   * - GAMMA
     - 
     - growth non-linearity
   * - GAMMA_REV
     - 
     - reverse growth non-linearity
   * - P_CRIT
     - 
     - critical pressure
   * - TOL
     - 
     - tolerance for local Newton iteration

.. _MAT_GrowthIsoStress:

MAT_GrowthIsoStress
-------------------

stress-dependent growth law

::

      MAT <matID>  MAT_GrowthIsoStress THETAPLUS 0 KPLUS 0 MPLUS 0 \
       THETAMINUS 0 KMINUS 0 MMINUS 0 HOMMANDEL 0 TOL 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - THETAPLUS
     - 
     - maximal growth stretch
   * - KPLUS
     - 
     - growth law parameter kthetaplus
   * - MPLUS
     - 
     - growth law parameter mthetaplus
   * - THETAMINUS
     - 
     - minimal growth stretch
   * - KMINUS
     - 
     - growth law parameter kthetaminus
   * - MMINUS
     - 
     - growth law parameter mthetaminus
   * - HOMMANDEL
     - 
     - homeostatic value for mandelstress
   * - TOL
     - 
     - tolerance for local Newton iteration

.. _MAT_GrowthAC:

MAT_GrowthAC
------------

scalar depended volumetric growth

::

      MAT <matID>  MAT_GrowthAC SCALAR1 0 ALPHA 0 SCALAR2 1 BETA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - SCALAR1
     - 
     - number of first growth inducing scalar
   * - ALPHA
     - 
     - volume per first scalar's mass density
   * - SCALAR2
     - yes
     - number of second growth inducing scalar
   * - BETA
     - yes
     - volume per second scalar's mass density

.. _MAT_GrowthACRadial:

MAT_GrowthACRadial
------------------

scalar depended growth in radial direction

::

      MAT <matID>  MAT_GrowthACRadial SCALAR1 0 ALPHA 0 SCALAR2 1 \
       BETA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - SCALAR1
     - 
     - number of first growth inducing scalar
   * - ALPHA
     - 
     - volume per first scalar's mass density
   * - SCALAR2
     - yes
     - number of second growth inducing scalar
   * - BETA
     - yes
     - volume per second scalar's mass density

.. _MAT_GrowthACRadialRefConc:

MAT_GrowthACRadialRefConc
-------------------------

scalar depended growth in radial direction

::

      MAT <matID>  MAT_GrowthACRadialRefConc SCALAR1 0 ALPHA 0 SCALAR2 1 \
       BETA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - SCALAR1
     - 
     - number of first growth inducing scalar
   * - ALPHA
     - 
     - volume per first scalar's mass density
   * - SCALAR2
     - yes
     - number of second growth inducing scalar
   * - BETA
     - yes
     - volume per second scalar's mass density

.. _MAT_GrowthConst:

MAT_GrowthConst
---------------

constant growth law

::

      MAT <matID>  MAT_GrowthConst THETARATE 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - THETARATE
     - 
     - reference value for mandelstress

.. _MAT_ConstraintMixture:

MAT_ConstraintMixture
---------------------

growth and remodeling of arteries

::

      MAT <matID>  MAT_ConstraintMixture DENS 0 MUE 0 NUE 0 PHIE 0 \
       PREELA 0 K1 0 K2 0 NUMHOM 1 PRECOLL  DAMAGE 0 K1M 0 K2M 0 \
       PHIM 0 PREMUS 0 SMAX 0 KAPPA 0 LIFETIME 0 GROWTHFAC 0 HOMSTR  \
       SHEARGROWTHFAC 0 HOMRAD 0 STARTTIME 0 INTEGRATION Explicit \
       TOL 0 GROWTHFORCE Single ELASTINDEGRAD None MASSPROD Lin INITSTRETCH None \
       CURVE 0 DEGOPTION Lin MAXMASSPRODFAC 0 ELASTINFAC 0 STOREHISTORY No

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - DENS
     - 
     - Density
   * - MUE
     - 
     - Shear Modulus
   * - NUE
     - 
     - Poisson's ratio
   * - PHIE
     - 
     - mass fraction of elastin
   * - PREELA
     - 
     - prestretch of elastin
   * - K1
     - 
     - Parameter for linear collagen fiber stiffness
   * - K2
     - 
     - Parameter for exponential collagen fiber stiffness
   * - NUMHOM
     - 
     - Number of homeostatic parameters
   * - PRECOLL
     - 
     - prestretch of collagen fibers
   * - DAMAGE
     - 
     - damage stretch of collagen fibers
   * - K1M
     - 
     - Parameter for linear smooth muscle fiber stiffness
   * - K2M
     - 
     - Parameter for exponential smooth muscle fiber |break| 
       stiffness
   * - PHIM
     - 
     - mass fraction of smooth muscle
   * - PREMUS
     - 
     - prestretch of smooth muscle fibers
   * - SMAX
     - 
     - maximal active stress
   * - KAPPA
     - 
     - dilatation modulus
   * - LIFETIME
     - 
     - lifetime of collagen fibers
   * - GROWTHFAC
     - 
     - growth factor for stress
   * - HOMSTR
     - 
     - homeostatic target value of scalar stress measure
   * - SHEARGROWTHFAC
     - 
     - growth factor for shear
   * - HOMRAD
     - 
     - homeostatic target value of inner radius
   * - STARTTIME
     - 
     - at this time turnover of collagen starts
   * - INTEGRATION
     - 
     - time integration scheme: Explicit (default), or |break| 
       Implicit
   * - TOL
     - 
     - tolerance for local Newton iteration, only for |break| 
       implicit integration
   * - GROWTHFORCE
     - 
     - driving force of growth: Single (default), All, |break| 
       ElaCol
   * - ELASTINDEGRAD
     - 
     - how elastin is degraded: None (default), |break| 
       Rectangle, Time
   * - MASSPROD
     - 
     - how mass depends on driving force: Lin (default), |break| 
       CosCos
   * - INITSTRETCH
     - 
     - how to set stretches in the beginning (None, |break| 
       Homeo, UpdatePrestretch)
   * - CURVE
     - 
     - number of timecurve for increase of prestretch in |break| 
       time
   * - DEGOPTION
     - 
     - Type of degradation function: Lin (default), Cos, |break| 
       Exp, ExpVar
   * - MAXMASSPRODFAC
     - 
     - maximal factor of mass production
   * - ELASTINFAC
     - yes
     - factor for elastin content
   * - STOREHISTORY
     - yes
     - store all history variables, not recommended for |break| 
       forward simulations

.. _MAT_StructPoro:

MAT_StructPoro
--------------

wrapper for structure poroelastic material

::

      MAT <matID>  MAT_StructPoro MATID 0 POROLAWID 0 INITPOROSITY 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - MATID
     - 
     - ID of structure material
   * - POROLAWID
     - 
     - ID of porosity law
   * - INITPOROSITY
     - 
     - initial porosity of porous medium

.. _MAT_PoroLawLinear:

MAT_PoroLawLinear
-----------------

linear constitutive law for porosity

::

      MAT <matID>  MAT_PoroLawLinear BULKMODULUS 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - BULKMODULUS
     - 
     - bulk modulus of porous medium

.. _MAT_PoroLawConstant:

MAT_PoroLawConstant
-------------------

constant constitutive law for porosity

::

      MAT <matID>  MAT_PoroLawConstant

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - no |break| 
       parameters
     - 
     - 

.. _MAT_PoroLawNeoHooke:

MAT_PoroLawNeoHooke
-------------------

NeoHookean-like constitutive law for porosity

::

      MAT <matID>  MAT_PoroLawNeoHooke BULKMODULUS 0 PENALTYPARAMETER 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - BULKMODULUS
     - 
     - bulk modulus of porous medium
   * - PENALTYPARAMETER
     - 
     - penalty paramter of porous medium

.. _MAT_PoroLawIncompSkel:

MAT_PoroLawIncompSkel
---------------------

porosity law for incompressible skeleton phase

::

      MAT <matID>  MAT_PoroLawIncompSkel

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - no |break| 
       parameters
     - 
     - 

.. _MAT_PoroLawLinBiot:

MAT_PoroLawLinBiot
------------------

linear biot model for porosity law

::

      MAT <matID>  MAT_PoroLawLinBiot INVBIOTMODULUS 0 BIOTCEOFF 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - INVBIOTMODULUS
     - 
     - inverse Biot modulus of porous medium
   * - BIOTCEOFF
     - 
     - Biot coefficient of porous medium

.. _MAT_PoroLawDensityDependent:

MAT_PoroLawDensityDependent
---------------------------

porosity depending on the density

::

      MAT <matID>  MAT_PoroLawDensityDependent DENSITYLAWID 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - DENSITYLAWID
     - 
     - material ID of density law

.. _MAT_PoroDensityLawConstant:

MAT_PoroDensityLawConstant
--------------------------

density law for constant density in porous multiphase medium

::

      MAT <matID>  MAT_PoroDensityLawConstant

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - no |break| 
       parameters
     - 
     - 

.. _MAT_PoroDensityLawExp:

MAT_PoroDensityLawExp
---------------------

density law for pressure dependent exponential function

::

      MAT <matID>  MAT_PoroDensityLawExp BULKMODULUS 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - BULKMODULUS
     - 
     - bulk modulus of porous medium

.. _MAT_FluidPoroRelPermeabilityLawConstant:

MAT_FluidPoroRelPermeabilityLawConstant
---------------------------------------

permeability law for constant permeability in porous multiphase medium

::

      MAT <matID>  MAT_FluidPoroRelPermeabilityLawConstant VALUE 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - VALUE
     - 
     - constant value of permeability

.. _MAT_FluidPoroRelPermeabilityLawExp:

MAT_FluidPoroRelPermeabilityLawExp
----------------------------------

permeability law depending on saturation in porous multiphase medium

::

      MAT <matID>  MAT_FluidPoroRelPermeabilityLawExp EXP 0 MIN_SAT 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - EXP
     - 
     - exponent of the saturation of this phase
   * - MIN_SAT
     - 
     - minimum saturation which is used for calculation

.. _MAT_FluidPoroViscosityLawConstant:

MAT_FluidPoroViscosityLawConstant
---------------------------------

viscosity law for constant viscosity in porous multiphase medium

::

      MAT <matID>  MAT_FluidPoroViscosityLawConstant VALUE 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - VALUE
     - 
     - constant value of viscosity

.. _MAT_FluidPoroViscosityLawCellAdherence:

MAT_FluidPoroViscosityLawCellAdherence
--------------------------------------

visosity law depending on pressure gradient in porous multiphase medium

::

      MAT <matID>  MAT_FluidPoroViscosityLawCellAdherence VISC_0 0 \
       XI 0 PSI 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - VISC_0
     - 
     - Visc0 parameter for modelling cell adherence
   * - XI
     - 
     - xi parameter for modelling cell adherence
   * - PSI
     - 
     - psi parameter for modelling cell adherence

.. _MAT_StructPoroReaction:

MAT_StructPoroReaction
----------------------

wrapper for structure porelastic material with reaction

::

      MAT <matID>  MAT_StructPoroReaction MATID 0 POROLAWID 0 INITPOROSITY 0 \
       DOFIDREACSCALAR 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - MATID
     - 
     - ID of structure material
   * - POROLAWID
     - 
     - ID of porosity law
   * - INITPOROSITY
     - 
     - initial porosity of porous medium
   * - DOFIDREACSCALAR
     - 
     - Id of DOF within scalar transport problem, which |break| 
       controls the reaction

.. _MAT_StructPoroReactionECM:

MAT_StructPoroReactionECM
-------------------------

wrapper for structure porelastic material with reaction

::

      MAT <matID>  MAT_StructPoroReactionECM MATID 0 POROLAWID 0 \
       INITPOROSITY 0 DENSCOLLAGEN 0 DOFIDREACSCALAR 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - MATID
     - 
     - ID of structure material
   * - POROLAWID
     - 
     - ID of porosity law
   * - INITPOROSITY
     - 
     - initial porosity of porous medium
   * - DENSCOLLAGEN
     - 
     - density of collagen
   * - DOFIDREACSCALAR
     - 
     - Id of DOF within scalar transport problem, which |break| 
       controls the reaction

.. _MAT_FluidPoro:

MAT_FluidPoro
-------------

flow in deformable porous media

::

      MAT <matID>  MAT_FluidPoro DYNVISCOSITY 0 DENSITY 0 PERMEABILITY 0 \
       AXIALPERMEABILITY 0 ORTHOPERMEABILITY1 0 ORTHOPERMEABILITY2 0 \
       ORTHOPERMEABILITY3 0 TYPE Darcy PERMEABILITYFUNCTION Const

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - DYNVISCOSITY
     - 
     - dynamic viscosity
   * - DENSITY
     - 
     - density
   * - PERMEABILITY
     - yes
     - permeability of medium
   * - AXIALPERMEABILITY
     - yes
     - axial permeability for transverse isotropy
   * - ORTHOPERMEABILITY1
     - yes
     - first permeability for orthotropy
   * - ORTHOPERMEABILITY2
     - yes
     - second permeability for orthotropy
   * - ORTHOPERMEABILITY3
     - yes
     - third permeability for orthotropy
   * - TYPE
     - 
     - Problem type: Darcy (default) or Darcy-Brinkman
   * - PERMEABILITYFUNCTION
     - yes
     - Permeability function: Const(Default) or |break| 
       Kozeny_Carman

.. _MAT_FluidPoroMultiPhase:

MAT_FluidPoroMultiPhase
-----------------------

multi phase flow in deformable porous media

::

      MAT <matID>  MAT_FluidPoroMultiPhase LOCAL No PERMEABILITY 0 \
       NUMMAT 0 MATIDS  NUMFLUIDPHASES_IN_MULTIPHASEPORESPACE 0 END

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - LOCAL
     - 
     - individual materials allocated per element or only |break| 
       at global scope
   * - PERMEABILITY
     - 
     - permeability of medium
   * - NUMMAT
     - 
     - number of materials in list
   * - MATIDS
     - 
     - the list material IDs
   * - NUMFLUIDPHASES_IN_MULTIPHASEPORESPACE
     - 
     - number of fluid phases
   * - END
     - 
     - indicating end of line

.. _MAT_FluidPoroMultiPhaseReactions:

MAT_FluidPoroMultiPhaseReactions
--------------------------------

multi phase flow in deformable porous media and list of reactions

::

      MAT <matID>  MAT_FluidPoroMultiPhaseReactions LOCAL No PERMEABILITY 0 \
       NUMMAT 0 MATIDS  NUMFLUIDPHASES_IN_MULTIPHASEPORESPACE 0 NUMREAC 0 \
       REACIDS  END

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - LOCAL
     - 
     - individual materials allocated per element or only |break| 
       at global scope
   * - PERMEABILITY
     - 
     - permeability of medium
   * - NUMMAT
     - 
     - number of materials in list
   * - MATIDS
     - 
     - the list material IDs
   * - NUMFLUIDPHASES_IN_MULTIPHASEPORESPACE
     - 
     - number of fluid phases
   * - NUMREAC
     - 
     - number of reactions for these elements
   * - REACIDS
     - 
     - advanced reaction list
   * - END
     - 
     - indicating end of line

.. _MAT_FluidPoroSingleReaction:

MAT_FluidPoroSingleReaction
---------------------------

advanced reaction material

::

      MAT <matID>  MAT_FluidPoroSingleReaction NUMSCAL 0 TOTALNUMDOF 0 \
       NUMVOLFRAC 0 SCALE  COUPLING no_coupling FUNCTID 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - NUMSCAL
     - 
     - number of scalars coupled with this problem
   * - TOTALNUMDOF
     - 
     - total number of multiphase-dofs
   * - NUMVOLFRAC
     - 
     - number of volfracs
   * - SCALE
     - 
     - advanced reaction list
   * - COUPLING
     - 
     - type of coupling: scalar_by_function, no_coupling |break| 
       (default)
   * - FUNCTID
     - 
     - function ID defining the reaction

.. _MAT_FluidPoroSinglePhase:

MAT_FluidPoroSinglePhase
------------------------

one phase for multiphase flow in deformable porous media

::

      MAT <matID>  MAT_FluidPoroSinglePhase DENSITYLAWID 0 DENSITY 0 \
       RELPERMEABILITYLAWID 0 VISCOSITYLAWID 0 DOFTYPEID 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - DENSITYLAWID
     - 
     - ID of density law
   * - DENSITY
     - 
     - reference/initial density
   * - RELPERMEABILITYLAWID
     - 
     - ID of relative permeability law
   * - VISCOSITYLAWID
     - 
     - ID of viscosity law
   * - DOFTYPEID
     - 
     - ID of dof definition

.. _MAT_FluidPoroSingleVolFrac:

MAT_FluidPoroSingleVolFrac
--------------------------

one phase for multiphase flow in deformable porous media

::

      MAT <matID>  MAT_FluidPoroSingleVolFrac DENSITY 0 DIFFUSIVITY 0 \
       AddScalarDependentFlux No NUMSCAL 0 SCALARDIFFS  OMEGA_HALF 

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - DENSITY
     - 
     - reference/initial density
   * - DIFFUSIVITY
     - 
     - diffusivity of phase
   * - AddScalarDependentFlux
     - 
     - Is there additional scalar dependent flux (yes) or |break| 
       (no)
   * - NUMSCAL
     - yes
     - Number of scalars
   * - SCALARDIFFS
     - yes
     - Diffusivities for additional scalar-dependent flux
   * - OMEGA_HALF
     - yes
     - Constant for receptor kinetic law

.. _MAT_FluidPoroVolFracPressure:

MAT_FluidPoroVolFracPressure
----------------------------

one volume fraction pressure for multiphase flow in deformable porous media

::

      MAT <matID>  MAT_FluidPoroVolFracPressure PERMEABILITY 0 VISCOSITYLAWID 0 \
       MIN_VOLFRAC 0.001

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - PERMEABILITY
     - 
     - permeability of phase
   * - VISCOSITYLAWID
     - 
     - ID of viscosity law
   * - MIN_VOLFRAC
     - yes
     - Minimum volume fraction under which we assume that |break| 
       VolfracPressure is zero

.. _MAT_FluidPoroSinglePhaseDofDiffPressure:

MAT_FluidPoroSinglePhaseDofDiffPressure
---------------------------------------

one degrree of freedom for multiphase flow in deformable porous media

::

      MAT <matID>  MAT_FluidPoroSinglePhaseDofDiffPressure PHASELAWID 0 \
       NUMDOF 0 PRESCOEFF  END

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - PHASELAWID
     - 
     - ID of pressure-saturation law
   * - NUMDOF
     - 
     - number of DoFs
   * - PRESCOEFF
     - 
     - pressure IDs for differential pressure
   * - END
     - 
     - indicating end of line

.. _MAT_FluidPoroSinglePhaseDofPressure:

MAT_FluidPoroSinglePhaseDofPressure
-----------------------------------

one degrree of freedom for multiphase flow in deformable porous media

::

      MAT <matID>  MAT_FluidPoroSinglePhaseDofPressure PHASELAWID 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - PHASELAWID
     - 
     - ID of pressure-saturation law

.. _MAT_FluidPoroSinglePhaseDofSaturation:

MAT_FluidPoroSinglePhaseDofSaturation
-------------------------------------

one degrree of freedom for multiphase flow in deformable porous media

::

      MAT <matID>  MAT_FluidPoroSinglePhaseDofSaturation PHASELAWID 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - PHASELAWID
     - 
     - ID of pressure-saturation law

.. _MAT_PhaseLawLinear:

MAT_PhaseLawLinear
------------------

saturated fluid phase of porous medium

::

      MAT <matID>  MAT_PhaseLawLinear RELTENSION 0 SATURATION_0 0 \
       NUMDOF 0 PRESCOEFF  END

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - RELTENSION
     - 
     - relative interface tensions
   * - SATURATION_0
     - 
     - saturation at zero differential pressure
   * - NUMDOF
     - 
     - number of DoFs
   * - PRESCOEFF
     - 
     - Coefficients for pressure dependence
   * - END
     - 
     - indicating end of line

.. _MAT_PhaseLawTangent:

MAT_PhaseLawTangent
-------------------

tangent fluid phase of porous medium

::

      MAT <matID>  MAT_PhaseLawTangent RELTENSION 0 EXP 0 SATURATION_0 0 \
       NUMDOF 0 PRESCOEFF  END

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - RELTENSION
     - 
     - relative interface tensions
   * - EXP
     - 
     - exponent in pressure-saturation law
   * - SATURATION_0
     - 
     - saturation at zero differential pressure
   * - NUMDOF
     - 
     - number of DoFs
   * - PRESCOEFF
     - 
     - Coefficients for pressure dependence
   * - END
     - 
     - indicating end of line

.. _MAT_PhaseLawConstraint:

MAT_PhaseLawConstraint
----------------------

constraint fluid phase of porous medium

::

      MAT <matID>  MAT_PhaseLawConstraint

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - no |break| 
       parameters
     - 
     - 

.. _MAT_PhaseLawByFunction:

MAT_PhaseLawByFunction
----------------------

fluid phase of porous medium defined by functions

::

      MAT <matID>  MAT_PhaseLawByFunction FUNCTPRES 0 FUNCTSAT 0 \
       NUMDOF 0 PRESCOEFF  END

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - FUNCTPRES
     - 
     - ID of function for differential pressure
   * - FUNCTSAT
     - 
     - ID of function for saturation
   * - NUMDOF
     - 
     - number of DoFs
   * - PRESCOEFF
     - 
     - Coefficients for pressure dependence
   * - END
     - 
     - indicating end of line

.. _MAT_Struct_Spring:

MAT_Struct_Spring
-----------------

elastic spring

::

      MAT <matID>  MAT_Struct_Spring STIFFNESS 0 DENS 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - STIFFNESS
     - 
     - spring constant
   * - DENS
     - 
     - density

.. _MAT_BeamReissnerElastHyper:

MAT_BeamReissnerElastHyper
--------------------------

material parameters for a Simo-Reissner type beam element based on hyperelastic stored energy function

::

      MAT <matID>  MAT_BeamReissnerElastHyper YOUNG 0 SHEARMOD -1 \
       POISSONRATIO -1 DENS 0 CROSSAREA 0 SHEARCORR 0 MOMINPOL 0 \
       MOMIN2 0 MOMIN3 0 FAD No INTERACTIONRADIUS -1

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - YOUNG
     - 
     - Young's modulus
   * - SHEARMOD
     - yes
     - shear modulus
   * - POISSONRATIO
     - yes
     - Poisson's ratio
   * - DENS
     - 
     - mass density
   * - CROSSAREA
     - 
     - cross-section area
   * - SHEARCORR
     - 
     - shear correction factor
   * - MOMINPOL
     - 
     - polar/axial area moment of inertia
   * - MOMIN2
     - 
     - area moment of inertia w.r.t. first principal axis |break| 
       of inertia (i.e. second base vector)
   * - MOMIN3
     - 
     - area moment of inertia w.r.t. second principal |break| 
       axis of inertia (i.e. third base vector)
   * - FAD
     - yes
     - Does automatic differentiation have to be used
   * - INTERACTIONRADIUS
     - yes
     - radius of a circular cross-section which is |break| 
       EXCLUSIVELY used to evaluate interactions such as |break| 
       contact, potentials, ...

.. _MAT_BeamReissnerElastPlastic:

MAT_BeamReissnerElastPlastic
----------------------------

material parameters for a Simo-Reissner type beam element based on hyperelastic stored energy function

::

      MAT <matID>  MAT_BeamReissnerElastPlastic YOUNG 0 YIELDN -1 \
       YIELDM -1 ISOHARDN -1 ISOHARDM -1 TORSIONPLAST 0 SHEARMOD -1 \
       POISSONRATIO -1 DENS 0 CROSSAREA 0 SHEARCORR 0 MOMINPOL 0 \
       MOMIN2 0 MOMIN3 0 FAD No INTERACTIONRADIUS -1

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - YOUNG
     - 
     - Young's modulus
   * - YIELDN
     - yes
     - initial yield stress N
   * - YIELDM
     - yes
     - initial yield stress M
   * - ISOHARDN
     - yes
     - isotropic hardening modulus of forces
   * - ISOHARDM
     - yes
     - isotropic hardening modulus of moments
   * - TORSIONPLAST
     - yes
     - defines whether torsional moment contributes to |break| 
       plasticity
   * - SHEARMOD
     - yes
     - shear modulus
   * - POISSONRATIO
     - yes
     - Poisson's ratio
   * - DENS
     - 
     - mass density
   * - CROSSAREA
     - 
     - cross-section area
   * - SHEARCORR
     - 
     - shear correction factor
   * - MOMINPOL
     - 
     - polar/axial area moment of inertia
   * - MOMIN2
     - 
     - area moment of inertia w.r.t. first principal axis |break| 
       of inertia (i.e. second base vector)
   * - MOMIN3
     - 
     - area moment of inertia w.r.t. second principal |break| 
       axis of inertia (i.e. third base vector)
   * - FAD
     - yes
     - Does automatic differentiation have to be used
   * - INTERACTIONRADIUS
     - yes
     - radius of a circular cross-section which is |break| 
       EXCLUSIVELY used to evaluate interactions such as |break| 
       contact, potentials, ...

.. _MAT_BeamReissnerElastHyper_ByModes:

MAT_BeamReissnerElastHyper_ByModes
----------------------------------

material parameters for a Simo-Reissner type beam element based on hyperelastic stored energy function, specified for individual deformation modes

::

      MAT <matID>  MAT_BeamReissnerElastHyper_ByModes EA 0 GA2 0 \
       GA3 0 GI_T 0 EI2 0 EI3 0 RhoA 0 MASSMOMINPOL 0 MASSMOMIN2 0 \
       MASSMOMIN3 0 FAD No INTERACTIONRADIUS -1

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - EA
     - 
     - axial rigidity
   * - GA2
     - 
     - shear rigidity w.r.t first principal axis of |break| 
       inertia
   * - GA3
     - 
     - shear rigidity w.r.t second principal axis of |break| 
       inertia
   * - GI_T
     - 
     - torsional rigidity
   * - EI2
     - 
     - flexural/bending rigidity w.r.t. first principal |break| 
       axis of inertia
   * - EI3
     - 
     - flexural/bending rigidity w.r.t. second principal |break| 
       axis of inertia
   * - RhoA
     - 
     - translational inertia: mass density * |break| 
       cross-section area
   * - MASSMOMINPOL
     - 
     - polar mass moment of inertia, i.e. w.r.t. rotation |break| 
       around beam axis
   * - MASSMOMIN2
     - 
     - mass moment of inertia w.r.t. first principal axis |break| 
       of inertia
   * - MASSMOMIN3
     - 
     - mass moment of inertia w.r.t. second principal |break| 
       axis of inertia
   * - FAD
     - yes
     - Does automatic differentiation have to be used
   * - INTERACTIONRADIUS
     - yes
     - radius of a circular cross-section which is |break| 
       EXCLUSIVELY used to evaluate interactions such as |break| 
       contact, potentials, ...

.. _MAT_BeamKirchhoffElastHyper:

MAT_BeamKirchhoffElastHyper
---------------------------

material parameters for a Kirchhoff-Love type beam element based on hyperelastic stored energy function

::

      MAT <matID>  MAT_BeamKirchhoffElastHyper YOUNG 0 SHEARMOD -1 \
       POISSONRATIO -1 DENS 0 CROSSAREA 0 MOMINPOL 0 MOMIN2 0 MOMIN3 0 \
       FAD No INTERACTIONRADIUS -1

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - YOUNG
     - 
     - Young's modulus
   * - SHEARMOD
     - yes
     - shear modulus
   * - POISSONRATIO
     - yes
     - Poisson's ratio
   * - DENS
     - 
     - mass density
   * - CROSSAREA
     - 
     - cross-section area
   * - MOMINPOL
     - 
     - polar/axial area moment of inertia
   * - MOMIN2
     - 
     - area moment of inertia w.r.t. first principal axis |break| 
       of inertia (i.e. second base vector)
   * - MOMIN3
     - 
     - area moment of inertia w.r.t. second principal |break| 
       axis of inertia (i.e. third base vector)
   * - FAD
     - yes
     - Does automatic differentiation have to be used
   * - INTERACTIONRADIUS
     - yes
     - radius of a circular cross-section which is |break| 
       EXCLUSIVELY used to evaluate interactions such as |break| 
       contact, potentials, ...

.. _MAT_BeamKirchhoffElastHyper_ByModes:

MAT_BeamKirchhoffElastHyper_ByModes
-----------------------------------

material parameters for a Kirchhoff-Love type beam element based on hyperelastic stored energy function, specified for individual deformation modes

::

      MAT <matID>  MAT_BeamKirchhoffElastHyper_ByModes EA 0 GI_T 0 \
       EI2 0 EI3 0 RhoA 0 MASSMOMINPOL 0 MASSMOMIN2 0 MASSMOMIN3 0 \
       FAD No INTERACTIONRADIUS -1

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - EA
     - 
     - axial rigidity
   * - GI_T
     - 
     - torsional rigidity
   * - EI2
     - 
     - flexural/bending rigidity w.r.t. first principal |break| 
       axis of inertia
   * - EI3
     - 
     - flexural/bending rigidity w.r.t. second principal |break| 
       axis of inertia
   * - RhoA
     - 
     - translational inertia: mass density * |break| 
       cross-section area
   * - MASSMOMINPOL
     - 
     - polar mass moment of inertia, i.e. w.r.t. rotation |break| 
       around beam axis
   * - MASSMOMIN2
     - 
     - mass moment of inertia w.r.t. first principal axis |break| 
       of inertia
   * - MASSMOMIN3
     - 
     - mass moment of inertia w.r.t. second principal |break| 
       axis of inertia
   * - FAD
     - yes
     - Does automatic differentiation have to be used
   * - INTERACTIONRADIUS
     - yes
     - radius of a circular cross-section which is |break| 
       EXCLUSIVELY used to evaluate interactions such as |break| 
       contact, potentials, ...

.. _MAT_BeamKirchhoffTorsionFreeElastHyper:

MAT_BeamKirchhoffTorsionFreeElastHyper
--------------------------------------

material parameters for a torsion-free, isotropic Kirchhoff-Love type beam element based on hyperelastic stored energy function

::

      MAT <matID>  MAT_BeamKirchhoffTorsionFreeElastHyper YOUNG 0 \
       DENS 0 CROSSAREA 0 MOMIN 0 FAD No INTERACTIONRADIUS -1

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - YOUNG
     - 
     - Young's modulus
   * - DENS
     - 
     - mass density
   * - CROSSAREA
     - 
     - cross-section area
   * - MOMIN
     - 
     - area moment of inertia
   * - FAD
     - yes
     - Does automatic differentiation have to be used
   * - INTERACTIONRADIUS
     - yes
     - radius of a circular cross-section which is |break| 
       EXCLUSIVELY used to evaluate interactions such as |break| 
       contact, potentials, ...

.. _MAT_BeamKirchhoffTorsionFreeElastHyper_ByModes:

MAT_BeamKirchhoffTorsionFreeElastHyper_ByModes
----------------------------------------------

material parameters for a torsion-free, isotropic Kirchhoff-Love type beam element based on hyperelastic stored energy function, specified for individual deformation modes

::

      MAT <matID>  MAT_BeamKirchhoffTorsionFreeElastHyper_ByModes \
       EA 0 EI 0 RhoA 0 FAD No INTERACTIONRADIUS -1

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - EA
     - 
     - axial rigidity
   * - EI
     - 
     - flexural/bending rigidity
   * - RhoA
     - 
     - translational inertia: mass density * |break| 
       cross-section area
   * - FAD
     - yes
     - Does automatic differentiation have to be used
   * - INTERACTIONRADIUS
     - yes
     - radius of a circular cross-section which is |break| 
       EXCLUSIVELY used to evaluate interactions such as |break| 
       contact, potentials, ...

.. _MAT_Crosslinker:

MAT_Crosslinker
---------------

material for a linkage between beams

::

      MAT <matID>  MAT_Crosslinker MATNUM 0 JOINTTYPE beam3rline2rigid \
       LINKINGLENGTH 0 LINKINGLENGTHTOL 0 LINKINGANGLE 0 LINKINGANGLETOL 0 \
       K_ON 0 K_OFF 0 DELTABELLEQ 0 NOBONDDISTSPHERE 0 TYPE arbitrary

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - MATNUM
     - 
     - number of beam elasthyper material
   * - JOINTTYPE
     - 
     - type of joint: beam3rline2rigid (default), |break| 
       beam3rline2pin or truss
   * - LINKINGLENGTH
     - 
     - distance between the two binding domains of a |break| 
       linker
   * - LINKINGLENGTHTOL
     - 
     - tolerance for linker length in the sense: length |break| 
       +- tolerance
   * - LINKINGANGLE
     - 
     - preferred binding angle enclosed by two filaments' |break| 
       axes in radians
   * - LINKINGANGLETOL
     - 
     - tolerance for preferred binding angle in radians |break| 
       in the sense of: angle +- tolerance
   * - K_ON
     - 
     - chemical association-rate
   * - K_OFF
     - 
     - chemical dissociation-rate
   * - DELTABELLEQ
     - yes
     - deltaD in Bell's equation for force dependent off |break| 
       rate
   * - NOBONDDISTSPHERE
     - yes
     - distance to sphere elements in which no double |break| 
       bonded linker is allowed
   * - TYPE
     - yes
     - type of crosslinker: arbitrary (default), actin, |break| 
       collagen, integrin

.. _MAT_0D_MAXWELL_ACINUS:

MAT_0D_MAXWELL_ACINUS
---------------------

0D acinar material

::

      MAT <matID>  MAT_0D_MAXWELL_ACINUS Stiffness1 0 Stiffness2 0 \
       Viscosity1 0 Viscosity2 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - Stiffness1
     - 
     - first stiffness
   * - Stiffness2
     - 
     - second stiffness
   * - Viscosity1
     - 
     - first viscosity
   * - Viscosity2
     - 
     - second viscosity

.. _MAT_0D_MAXWELL_ACINUS_NEOHOOKEAN:

MAT_0D_MAXWELL_ACINUS_NEOHOOKEAN
--------------------------------

0D acinar material neohookean

::

      MAT <matID>  MAT_0D_MAXWELL_ACINUS_NEOHOOKEAN Stiffness1 0 \
       Stiffness2 0 Viscosity1 0 Viscosity2 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - Stiffness1
     - 
     - first stiffness
   * - Stiffness2
     - 
     - second stiffness
   * - Viscosity1
     - 
     - first viscosity
   * - Viscosity2
     - 
     - second viscosity

.. _MAT_0D_MAXWELL_ACINUS_EXPONENTIAL:

MAT_0D_MAXWELL_ACINUS_EXPONENTIAL
---------------------------------

0D acinar material exponential

::

      MAT <matID>  MAT_0D_MAXWELL_ACINUS_EXPONENTIAL Stiffness1 0 \
       Stiffness2 0 Viscosity1 0 Viscosity2 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - Stiffness1
     - 
     - first stiffness
   * - Stiffness2
     - 
     - second stiffness
   * - Viscosity1
     - 
     - first viscosity
   * - Viscosity2
     - 
     - second viscosity

.. _MAT_0D_MAXWELL_ACINUS_DOUBLEEXPONENTIAL:

MAT_0D_MAXWELL_ACINUS_DOUBLEEXPONENTIAL
---------------------------------------

0D acinar material doubleexponential

::

      MAT <matID>  MAT_0D_MAXWELL_ACINUS_DOUBLEEXPONENTIAL Stiffness1 0 \
       Stiffness2 0 Viscosity1 0 Viscosity2 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - Stiffness1
     - 
     - first stiffness
   * - Stiffness2
     - 
     - second stiffness
   * - Viscosity1
     - 
     - first viscosity
   * - Viscosity2
     - 
     - second viscosity

.. _MAT_0D_MAXWELL_ACINUS_OGDEN:

MAT_0D_MAXWELL_ACINUS_OGDEN
---------------------------

0D acinar material ogden

::

      MAT <matID>  MAT_0D_MAXWELL_ACINUS_OGDEN Stiffness1 0 Stiffness2 0 \
       Viscosity1 0 Viscosity2 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - Stiffness1
     - 
     - first stiffness
   * - Stiffness2
     - 
     - second stiffness
   * - Viscosity1
     - 
     - first viscosity
   * - Viscosity2
     - 
     - second viscosity

.. _MAT_0D_O2_HEMOGLOBIN_SATURATION:

MAT_0D_O2_HEMOGLOBIN_SATURATION
-------------------------------

0D O2 hemoglobin saturation material

::

      MAT <matID>  MAT_0D_O2_HEMOGLOBIN_SATURATION PerVolumeBlood 0 \
       O2SaturationPerVolBlood 0 PressureHalf 0 Power 0 NumberOfO2PerVO2 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - PerVolumeBlood
     - 
     - how much of blood satisfies this rule (usually |break| 
       100ml)
   * - O2SaturationPerVolBlood
     - 
     - O2 saturation per volume blood (In healthy blood |break| 
       21.36ml/100ml of blood)
   * - PressureHalf
     - 
     - PO2 of 50% saturated O2 (In healthy blood 26mmHg)
   * - Power
     - 
     - Power of the Sigmoidal saturation curve (2.5)
   * - NumberOfO2PerVO2
     - 
     - Number of O2 moles per unit volume of O2

.. _MAT_0D_O2_AIR_SATURATION:

MAT_0D_O2_AIR_SATURATION
------------------------

0D O2 air saturation material

::

      MAT <matID>  MAT_0D_O2_AIR_SATURATION AtmosphericPressure 0 \
       NumberOfO2PerVO2 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - AtmosphericPressure
     - 
     - The atmospheric pressure
   * - NumberOfO2PerVO2
     - 
     - Number of O2 moles per unit volume of O2

.. _MAT_ParticleSPHFluid:

MAT_ParticleSPHFluid
--------------------

particle material for SPH fluid

::

      MAT <matID>  MAT_ParticleSPHFluid INITRADIUS 0 INITDENSITY 0 \
       REFDENSFAC 0 EXPONENT 0 BACKGROUNDPRESSURE 0 BULK_MODULUS 0 \
       DYNAMIC_VISCOSITY 0 BULK_VISCOSITY 0 ARTIFICIAL_VISCOSITY 0 \
       INITTEMPERATURE 0 THERMALCAPACITY 0 THERMALCONDUCTIVITY 0 \
       THERMALABSORPTIVITY 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - INITRADIUS
     - 
     - initial radius
   * - INITDENSITY
     - 
     - initial density
   * - REFDENSFAC
     - 
     - reference density factor in equation of state
   * - EXPONENT
     - 
     - exponent in equation of state
   * - BACKGROUNDPRESSURE
     - 
     - background pressure for transport velocity |break| 
       formulation
   * - BULK_MODULUS
     - 
     - bulk modulus
   * - DYNAMIC_VISCOSITY
     - 
     - dynamic shear viscosity
   * - BULK_VISCOSITY
     - 
     - bulk viscosity
   * - ARTIFICIAL_VISCOSITY
     - 
     - artificial viscosity
   * - INITTEMPERATURE
     - yes
     - initial temperature
   * - THERMALCAPACITY
     - yes
     - thermal capacity
   * - THERMALCONDUCTIVITY
     - yes
     - thermal conductivity
   * - THERMALABSORPTIVITY
     - yes
     - thermal absorptivity

.. _MAT_ParticleSPHBoundary:

MAT_ParticleSPHBoundary
-----------------------

particle material for SPH boundary

::

      MAT <matID>  MAT_ParticleSPHBoundary INITRADIUS 0 INITDENSITY 0 \
       INITTEMPERATURE 0 THERMALCAPACITY 0 THERMALCONDUCTIVITY 0 \
       THERMALABSORPTIVITY 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - INITRADIUS
     - 
     - initial radius
   * - INITDENSITY
     - 
     - initial density
   * - INITTEMPERATURE
     - yes
     - initial temperature
   * - THERMALCAPACITY
     - yes
     - thermal capacity
   * - THERMALCONDUCTIVITY
     - yes
     - thermal conductivity
   * - THERMALABSORPTIVITY
     - yes
     - thermal absorptivity

.. _MAT_ParticleDEM:

MAT_ParticleDEM
---------------

particle material for DEM

::

      MAT <matID>  MAT_ParticleDEM INITRADIUS 0 INITDENSITY 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - INITRADIUS
     - 
     - initial radius of particle
   * - INITDENSITY
     - 
     - initial density of particle

.. _MAT_ParticleWallDEM:

MAT_ParticleWallDEM
-------------------

particle wall material for DEM

::

      MAT <matID>  MAT_ParticleWallDEM FRICT_COEFF_TANG -1 FRICT_COEFF_ROLL -1 \
       ADHESION_SURFACE_ENERGY -1

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - FRICT_COEFF_TANG
     - yes
     - friction coefficient for tangential contact
   * - FRICT_COEFF_ROLL
     - yes
     - friction coefficient for rolling contact
   * - ADHESION_SURFACE_ENERGY
     - yes
     - adhesion surface energy

.. _MAT_Electromagnetic:

MAT_Electromagnetic
-------------------

Electromagnetic material

::

      MAT <matID>  MAT_Electromagnetic CONDUCTIVITY 0 PERMITTIVITY 0 \
       PERMEABILITY 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - CONDUCTIVITY
     - 
     - electrical conductivity
   * - PERMITTIVITY
     - 
     - Permittivity
   * - PERMEABILITY
     - 
     - Permeability

.. _MAT_ACTIVEFIBER:

MAT_ACTIVEFIBER
---------------

active fiber formation for the modeling of living cells

::

      MAT <matID>  MAT_ACTIVEFIBER DENS 0 DECAY 0 IDMATPASSIVE 0 \
       KFOR 0 KBACK 0 KVAR 0 SIGMAX 0 EPSNULL 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - DENS
     - 
     - Density
   * - DECAY
     - 
     - decay constant of activation signal
   * - IDMATPASSIVE
     - 
     - number of passive material in input file: MAT |break| 
       IDMATPASSIVE ...
   * - KFOR
     - 
     - formation rate parameter kforwards
   * - KBACK
     - 
     - dissociation parameter kbackwards
   * - KVAR
     - 
     - fiber rate sensitivity
   * - SIGMAX
     - 
     - maximum tension exerted by stress fibres
   * - EPSNULL
     - 
     - reference strain rate of cross-bridge dynamics law

.. _MAT_Mixture:

MAT_Mixture
-----------

General mixture model

::

      MAT <matID>  MAT_Mixture NUMCONST 0 MATIDMIXTURERULE 0 MATIDSCONST 

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - NUMCONST
     - 
     - number of mixture constituents
   * - MATIDMIXTURERULE
     - 
     - material id of the mixturerule
   * - MATIDSCONST
     - 
     - list material IDs of the mixture constituents

.. _MIX_Constituent_ElastHyper:

MIX_Constituent_ElastHyper
--------------------------

ElastHyper toolbox

::

      MAT <matID>  MIX_Constituent_ElastHyper NUMMAT 0 MATIDS  PRESTRESS_STRATEGY 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - NUMMAT
     - 
     - number of summands
   * - MATIDS
     - 
     - list material IDs of the summands
   * - PRESTRESS_STRATEGY
     - yes
     - Material id of the prestress strategy (optional, |break| 
       by default no prestretch)

.. _MIX_Constituent_ElastHyper_Damage:

MIX_Constituent_ElastHyper_Damage
---------------------------------

ElastHyper toolbox with damage

::

      MAT <matID>  MIX_Constituent_ElastHyper_Damage NUMMAT 0 MATIDS  \
       PRESTRESS_STRATEGY 0 DAMAGE_FUNCT 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - NUMMAT
     - 
     - number of summands
   * - MATIDS
     - 
     - list material IDs of the membrane summands
   * - PRESTRESS_STRATEGY
     - yes
     - Material id of the prestress strategy (optional, |break| 
       by default no prestretch)
   * - DAMAGE_FUNCT
     - 
     - Reference to the function that is a gain for the |break| 
       increase/decrease of the reference mass density.

.. _MIX_Constituent_ElastHyper_ElastinMembrane:

MIX_Constituent_ElastHyper_ElastinMembrane
------------------------------------------

ElastHyper toolbox with damage and 2D membrane material

::

      MAT <matID>  MIX_Constituent_ElastHyper_ElastinMembrane NUMMAT 0 \
       MATIDS  MEMBRANENUMMAT 0 MEMBRANEMATIDS  PRESTRESS_STRATEGY 0 \
       DAMAGE_FUNCT 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - NUMMAT
     - 
     - number of summands
   * - MATIDS
     - 
     - list material IDs of the membrane summands
   * - MEMBRANENUMMAT
     - 
     - number of summands
   * - MEMBRANEMATIDS
     - 
     - list material IDs of the membrane summands
   * - PRESTRESS_STRATEGY
     - yes
     - Material id of the prestress strategy (optional, |break| 
       by default no prestretch)
   * - DAMAGE_FUNCT
     - 
     - Reference to the function that is a gain for the |break| 
       increase/decrease of the reference mass density.

.. _MIX_Constituent_SolidMaterial:

MIX_Constituent_SolidMaterial
-----------------------------

Solid material

::

      MAT <matID>  MIX_Constituent_SolidMaterial MATID 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - MATID
     - 
     - ID of the solid material

.. _MIX_GrowthStrategy_Isotropic:

MIX_GrowthStrategy_Isotropic
----------------------------

isotropic growth

::

      MAT <matID>  MIX_GrowthStrategy_Isotropic

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - no |break| 
       parameters
     - 
     - 

.. _MIX_GrowthStrategy_Anisotropic:

MIX_GrowthStrategy_Anisotropic
------------------------------

anisotropic growth

::

      MAT <matID>  MIX_GrowthStrategy_Anisotropic INIT 1 FIBER_ID 1

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - INIT
     - yes
     - initialization modus for growth direction |break| 
       alignment
   * - FIBER_ID
     - yes
     - Id of the fiber to point the growth direction (1 |break| 
       for first fiber, default)

.. _MIX_GrowthStrategy_Stiffness:

MIX_GrowthStrategy_Stiffness
----------------------------

Extension of all constituents simultaneously

::

      MAT <matID>  MIX_GrowthStrategy_Stiffness KAPPA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - KAPPA
     - 
     - Penalty parameter for the modified penalty term |break| 
       for incompressibility

.. _MIX_Prestress_Strategy_Constant:

MIX_Prestress_Strategy_Constant
-------------------------------

Simple predefined prestress

::

      MAT <matID>  MIX_Prestress_Strategy_Constant PRESTRETCH 0 0 0 0 0 0 0 0 0 

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - PRESTRETCH
     - 
     - Definition of the prestretch as a 9x1 vector

.. _MIX_Prestress_Strategy_Cylinder:

MIX_Prestress_Strategy_Cylinder
-------------------------------

Simple prestress strategy for a cylinder

::

      MAT <matID>  MIX_Prestress_Strategy_Cylinder INNER_RADIUS 0 \
       WALL_THICKNESS 0 AXIAL_PRESTRETCH 0 CIRCUMFERENTIAL_PRESTRETCH 0 \
       PRESSURE 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - INNER_RADIUS
     - 
     - Inner radius of the cylinder
   * - WALL_THICKNESS
     - 
     - Wall thickness of the cylinder
   * - AXIAL_PRESTRETCH
     - 
     - Prestretch in axial direction
   * - CIRCUMFERENTIAL_PRESTRETCH
     - 
     - Prestretch in circumferential direction
   * - PRESSURE
     - 
     - Pressure in the inner of the cylinder

.. _MIX_Prestress_Strategy_Iterative:

MIX_Prestress_Strategy_Iterative
--------------------------------

Simple iterative prestress strategy for any geometry. Needed to be used within the mixture framework.

::

      MAT <matID>  MIX_Prestress_Strategy_Iterative ISOCHORIC No

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - ISOCHORIC
     - yes
     - Flag whether prestretch tensor is isochoric

.. _MIX_Constituent_ExplicitRemodelFiber:

MIX_Constituent_ExplicitRemodelFiber
------------------------------------

A 1D constituent that remodels

::

      MAT <matID>  MIX_Constituent_ExplicitRemodelFiber FIBER_ID 1 \
       FIBER_MATERIAL_ID 0 GROWTH_ENABLED Yes DECAY_TIME 0 GROWTH_CONSTANT 0 \
       DEPOSITION_STRETCH 0 DEPOSITION_STRETCH_TIMEFUNCT 0 INELASTIC_GROWTH No \
       INIT 0 GAMMA 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - FIBER_ID
     - yes
     - Id of the fiber
   * - FIBER_MATERIAL_ID
     - 
     - Id of fiber material
   * - GROWTH_ENABLED
     - yes
     - Switch for the growth (default true)
   * - DECAY_TIME
     - 
     - Decay time of deposited tissue
   * - GROWTH_CONSTANT
     - 
     - Growth constant of the tissue
   * - DEPOSITION_STRETCH
     - 
     - Stretch at with the fiber is deposited
   * - DEPOSITION_STRETCH_TIMEFUNCT
     - yes
     - Id of the time function to scale the deposition |break| 
       stretch (Default: 0=None)
   * - INELASTIC_GROWTH
     - yes
     - Mixture rule has inelastic growth (default false)
   * - INIT
     - 
     - Initialization mode for fibers (1=element fibers, |break| 
       2=nodal fibers)
   * - GAMMA
     - yes
     - Angle of fiber alignment in degree (default = 0.0 |break| 
       degrees)

.. _MIX_Constituent_ImplicitRemodelFiber:

MIX_Constituent_ImplicitRemodelFiber
------------------------------------

A 1D constituent that remodels

::

      MAT <matID>  MIX_Constituent_ImplicitRemodelFiber FIBER_ID 0 \
       FIBER_MATERIAL_ID 0 GROWTH_ENABLED Yes DECAY_TIME 0 GROWTH_CONSTANT 0 \
       DEPOSITION_STRETCH 0 DEPOSITION_STRETCH_TIMEFUNCT 0 INIT 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - FIBER_ID
     - 
     - Id of the fiber
   * - FIBER_MATERIAL_ID
     - 
     - Id of fiber material
   * - GROWTH_ENABLED
     - yes
     - Switch for the growth (default true)
   * - DECAY_TIME
     - 
     - Decay time of deposited tissue
   * - GROWTH_CONSTANT
     - 
     - Growth constant of the tissue
   * - DEPOSITION_STRETCH
     - 
     - Stretch at with the fiber is deposited
   * - DEPOSITION_STRETCH_TIMEFUNCT
     - yes
     - Id of the time function to scale the deposition |break| 
       stretch (Default: 0=None)
   * - INIT
     - 
     - Initialization mode for fibers (1=element fibers, |break| 
       2=nodal fibers)

.. _MIX_Constituent_RemodelFiber_Material_Exponential:

MIX_Constituent_RemodelFiber_Material_Exponential
-------------------------------------------------

An exponential strain energy function for the remodel fiber

::

      MAT <matID>  MIX_Constituent_RemodelFiber_Material_Exponential \
       K1 0 K2 0 COMPRESSION No

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - K1
     - 
     - First parameter of exponential strain energy |break| 
       function
   * - K2
     - 
     - Second parameter of exponential strain energy |break| 
       function
   * - COMPRESSION
     - 
     - Bool, whether the fiber material also supports |break| 
       compressive forces.

.. _MIX_Constituent_RemodelFiber_Material_Exponential_Active:

MIX_Constituent_RemodelFiber_Material_Exponential_Active
--------------------------------------------------------

An exponential strain energy function for the remodel fiber with an active contribution

::

      MAT <matID>  MIX_Constituent_RemodelFiber_Material_Exponential_Active \
       K1 0 K2 0 COMPRESSION No SIGMA_MAX 0 LAMBDAMAX 0 LAMBDA0 0 \
       LAMBDAACT 1 DENS 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - K1
     - 
     - First parameter of exponential strain energy |break| 
       function
   * - K2
     - 
     - Second parameter of exponential strain energy |break| 
       function
   * - COMPRESSION
     - 
     - Bool, whether the fiber material also supports |break| 
       compressive forces.
   * - SIGMA_MAX
     - 
     - Maximum active Cauchy-stress
   * - LAMBDAMAX
     - 
     - Stretch at maximum active Cauchy-stress
   * - LAMBDA0
     - 
     - Stretch at zero active Cauchy-stress
   * - LAMBDAACT
     - yes
     - Current stretch
   * - DENS
     - 
     - Density of the whole mixture

.. _MIX_Rule_Simple:

MIX_Rule_Simple
---------------

Simple mixture rule

::

      MAT <matID>  MIX_Rule_Simple DENS 0 NUMCONST 0 MASSFRAC 

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - DENS
     - 
     - 
   * - NUMCONST
     - 
     - number of mixture constituents
   * - MASSFRAC
     - 
     - list mass fractions of the mixture constituents

.. _MIX_GrowthRemodelMixtureRule:

MIX_GrowthRemodelMixtureRule
----------------------------

Mixture rule for growth/remodel homogenized constrained mixture models

::

      MAT <matID>  MIX_GrowthRemodelMixtureRule GROWTH_STRATEGY 0 \
       DENS 0 NUMCONST 0 MASSFRAC 

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - GROWTH_STRATEGY
     - 
     - Material id of the growth strategy
   * - DENS
     - 
     - 
   * - NUMCONST
     - 
     - number of mixture constituents
   * - MASSFRAC
     - 
     - list mass fractions of the mixture constituents

.. _MAT_crystal_plasticity:

MAT_crystal_plasticity
----------------------

 Crystal plasticity 

::

      MAT <matID>  MAT_crystal_plasticity TOL 0 YOUNG 0 NUE 0 DENS 0 \
       LAT FCC CTOA 0 ABASE 0 NUMSLIPSYS 0 NUMSLIPSETS 0 SLIPSETMEMBERS  \
       SLIPRATEEXP  GAMMADOTSLIPREF  DISDENSINIT  DISGENCOEFF  DISDYNRECCOEFF  \
       TAUY0  MFPSLIP  SLIPHPCOEFF  SLIPBYTWIN  NUMTWINSYS 0 NUMTWINSETS 0 \
       TWINSETMEMBERS  TWINRATEEXP  GAMMADOTTWINREF  TAUT0  MFPTWIN  \
       TWINHPCOEFF  TWINBYSLIP  TWINBYTWIN 

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - TOL
     - 
     - tolerance for internal Newton iteration
   * - YOUNG
     - 
     - Young's modulus
   * - NUE
     - 
     - Poisson's ratio
   * - DENS
     - 
     - Mass density
   * - LAT
     - 
     - lattice type: FCC, BCC, HCP, D019 or L10
   * - CTOA
     - 
     - c to a ratio of crystal unit cell
   * - ABASE
     - 
     - base length a of the crystal unit cell
   * - NUMSLIPSYS
     - 
     - number of slip systems
   * - NUMSLIPSETS
     - 
     - number of slip system sets
   * - SLIPSETMEMBERS
     - 
     - vector of NUMSLIPSYS indices ranging from 1 to |break| 
       NUMSLIPSETS that indicate to which set each slip |break| 
       system belongs
   * - SLIPRATEEXP
     - 
     - vector containing NUMSLIPSETS entries for the rate |break| 
       sensitivity exponent
   * - GAMMADOTSLIPREF
     - 
     - vector containing NUMSLIPSETS entries for the |break| 
       reference slip shear rate
   * - DISDENSINIT
     - 
     - vector containing NUMSLIPSETS entries for the |break| 
       initial dislocation density
   * - DISGENCOEFF
     - 
     - vector containing NUMSLIPSETS entries for the |break| 
       dislocation generation coefficients
   * - DISDYNRECCOEFF
     - 
     - vector containing NUMSLIPSETS entries for the |break| 
       coefficients for dynamic dislocation removal
   * - TAUY0
     - 
     - vector containing NUMSLIPSETS entries for the |break| 
       lattice resistance to slip, e.g. the Peierls |break| 
       barrier
   * - MFPSLIP
     - 
     - vector containing NUMSLIPSETS microstructural |break| 
       parameters that are relevant for Hall-Petch |break| 
       strengthening, e.g., grain size
   * - SLIPHPCOEFF
     - 
     - vector containing NUMSLIPSETS entries for the |break| 
       Hall-Petch coefficients corresponding to the |break| 
       microstructural parameters given in MFPSLIP
   * - SLIPBYTWIN
     - yes
     - (optional) vector containing NUMSLIPSETS entries |break| 
       for the work hardening coefficients by twinning on |break| 
       non-coplanar systems
   * - NUMTWINSYS
     - yes
     - (optional) number of twinning systems
   * - NUMTWINSETS
     - yes
     - (optional) number of sets of twinning systems
   * - TWINSETMEMBERS
     - yes
     - (optional) vector of NUMTWINSYS indices ranging |break| 
       from 1 to NUMTWINSETS that indicate to which set |break| 
       each slip system belongs
   * - TWINRATEEXP
     - yes
     - (optional) vector containing NUMTWINSETS entries |break| 
       for the rate sensitivity exponent
   * - GAMMADOTTWINREF
     - yes
     - (optional) vector containing NUMTWINSETS entries |break| 
       for the reference slip shear rate
   * - TAUT0
     - yes
     - (optional) vector containing NUMTWINSETS entries |break| 
       for the lattice resistance to twinning, e.g. the |break| 
       Peierls barrier
   * - MFPTWIN
     - yes
     - (optional) vector containing NUMTWINSETS |break| 
       microstructural parameters that are relevant for |break| 
       Hall-Petch strengthening of twins, e.g., grain |break| 
       size
   * - TWINHPCOEFF
     - yes
     - (optional) vector containing NUMTWINSETS entries |break| 
       for the Hall-Petch coefficients corresponding to |break| 
       the microstructural parameters given in MFPTWIN
   * - TWINBYSLIP
     - yes
     - (optional) vector containing NUMTWINSETS entries |break| 
       for the work hardening coefficients by slip
   * - TWINBYTWIN
     - yes
     - (optional) vector containing NUMTWINSETS entries |break| 
       for the work hardening coefficients by twins on |break| 
       non-coplanar systems

.. _MAT_LinElast1D:

MAT_LinElast1D
--------------

linear elastic material in one direction

::

      MAT <matID>  MAT_LinElast1D YOUNG 0 DENS 0

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - YOUNG
     - 
     - Young's modulus
   * - DENS
     - 
     - mass density

.. _MAT_LinElast1DGrowth:

MAT_LinElast1DGrowth
--------------------

linear elastic material with growth in one direction

::

      MAT <matID>  MAT_LinElast1DGrowth YOUNG 0 DENS 0 C0 0 AOS_PROP_GROWTH No \
       POLY_PARA_NUM 0 POLY_PARAMS 

.. list-table::
   :header-rows: 1
   :widths: 10,10,50

   * - Parameter
     - optional
     - Description
   * - YOUNG
     - 
     - Young's modulus
   * - DENS
     - 
     - mass density
   * - C0
     - 
     - reference concentration
   * - AOS_PROP_GROWTH
     - 
     - growth proportional to amount of substance (AOS) |break| 
       if true or proportional to concentration if false
   * - POLY_PARA_NUM
     - 
     - number of polynomial coefficients
   * - POLY_PARAMS
     - 
     - coefficients of polynomial

