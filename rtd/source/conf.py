# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
# import sys
# sys.path.append(os.path.abspath('/home/scheider/.local/lib/python3.8/site-packages/sphinx/ext'))


# -- Project information -----------------------------------------------------

project = 'BACI'
copyright = '2022, The BACI developers'
author = 'The BACI developers'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['sphinx.ext.todo', 
              'sphinx.ext.ifconfig', 
              'sphinx.ext.intersphinx', 
              'myst_parser', 
              'nbsphinx',
              'sphinx.ext.mathjax']

todo_include_todos = True

def setup(app):
    app.add_config_value("institution", "hereon", "env")

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
source_suffix = ['.rst', '.md']
exclude_patterns = []

rst_prolog = """
.. |break| raw:: html

   <br />

.. |newline| replace:: <br />
"""
# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
# html_theme = 'classic'
html_theme = 'sphinx_rtd_theme'
# html_theme = 'alabaster'

html_theme_options = {
    'collapse_navigation': False,
    'sticky_navigation': False,
    'navigation_depth': 4,
    'titles_only': False
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
#
# for finding headers as internal link targets, we have to set the variable myst_heading_anchors
myst_heading_anchors = 4
#
# mathjax extension:
mathjax_path="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"
