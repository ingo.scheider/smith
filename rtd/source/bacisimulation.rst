.. _bacisimulation:

BACI Simulation
================



.. todo::

   Here we have to add some content ;-)

Restarting an analysis
-----------------------

For restarting an analysis one has to provide restart information in the first simulation by including the parameter
``RESTARTEVRY  <numincs>`` in the ``--STRUCTURAL DYNAMICS`` section, 
so that the information is written every *numincs* increment. 

In the second simulation, no additional parameters have be included. The information that it is a restart, is
given on the command line:

::

   <baci-command> <datfile> <outputfile> [restartfrom=<restart_filename>] restart=<inc>

Here, one has to provide the increment, at which the restart is started from the previous simulation.
If the parameter ``restartfrom`` is given, the initial configuration is read from this file, 
otherwise it is read from ``outfile``. In the latter case the filename of the new output is the same with an appended number, e.g., ``outfile-1``.
Note that the value for ``inc`` must be given in the 
file ``<outfile>.control`` in one of the step lines: ``step = <inc>``.

.. note::

   - The parameters RESTART and RESTARTTIME in the PROBLEMTYP section 
     are not needed anymore, and will probably vanish soon.
   - The parameter MAXTIME indicates the maximum time of all simulations, 
     it is NOT a step time, so be aware that MAXTIME in the subsequent simulation 
     must be larger than in the first one.
   - The parameter TIMEINIT can be 0 also in the subsequent simulation, it is not used, 
     since the initial time is defined by the restart.
   - If a restart simulation is a dynamic one, the initial simulation must be a dynamic one as well, 
     since otherwise no mass matrix information is given.

