
.. _SetupGuidetoBACI:

Setup Guide to BACI
===================

Notes on setting up and running BACI, the Finite-Element code at the
Chair of Computational Mechanics. For information about
solving specific types of problems using BACI see the problem
introduction guides. For information about the inner workings of the
code and recommendations for developers see the developing guide.

Getting the code
----------------

The BACI code lives in an gitlab repository on a central LNM file server. 
There are two ways to get the code.

- Students ask their custodian :) for a copy. Before that can happen
  the non-disclosure form needs to be filled.

- Coworkers get it via gitlab:
  Ask your friendly administrator for a gitlab account.

It is a very good idea to get to know gitlab before you do this.

git hints
~~~~~~~~~

.. note::

   Write some git related information

Directory structure
~~~~~~~~~~~~~~~~~~~

The ``BACI`` code comes with documentation, example input files and
support scripts. The important subdirectories are the following:


:``src``: contains the real ``BACI`` code in several subdirectories

:``Input``:   contains various valid and running ``*``\ ``.dat`` files, ``BACI``
    input files that are used for (automatic) testing.

:``buildconfig``:   contains configuration files (platform specifications) needed to
    setup a ``BACI`` Makefile

:``utilities``:  contains configuration scripts needed to setup a ``BACI`` Makefile,
    

:``tests``:   contains scripts used for automatic testing and the list of files
    that are to be run during tests

:``unittests``:  t.b.a.

:``doc``:   contains all the documentation



Setup and Run
-------------

``BACI`` is developed and used on Linux. Other Unixes work as well.
Windows versions might be created using cygwin or mingw, but this will
require some small modifications and is not covered here.

``BACI`` is a non-interactive shell application that reads an input
file and creates a bunch of files in return. To build and run it you
will need a basic understanding of Linux and the Linux shell. You will
also want to choose your favorite text editoror integrated development environment (IDE).
Further information on this topic see [Development environment]_.

You'll find more information about the ``BACI`` installation in the ``readme.md`` and the
``contribute.md`` files located in the BACI root directory.




Running examples
~~~~~~~~~~~~~~~~

In ``Input`` there are test examples; all necessary “packages” must have
been activated in the defines-file that was used to configure the
``BACI`` at hand. For example,

``./baci-release Input/f2_drivencavity20x20_drt.dat xxx``

runs the 2d fluid driven cavity example and writes the output to files
beginning with ``xxx``. 
You can also run the code in parallel with the mpirun
command like this:

::

   mpirun -np 1 ./baci-release Input/f2_drivencavity20x20_drt.dat xxx

Testing
-------

.. note::
   
   The testing section is obviously heavily outdated.

There is the script ``test_script`` that runs all the examples file from
the ``Input``-directory that are marked for testing. It knows four modes
depending on the command line parameters:


*   compile ``BACI`` to fit one input file exactly and run it

*   compile ``BACI`` for all input files and run them one after the other

*   compile ``BACI`` for all input files, run them and restart them
    from a certain step

*   compile ``BACI`` so this it is able to run any (or neary any) input
    file and run them all using this version

After each run some result values are compared to expected values. Only
if these values match the test was successful.

The ``test_release`` script can be used to abbreviate all this. It runs
``test all``, ``restart test`` and ``release test`` in a row:

::

   ./test_release config/muench.fc6.ser

``BACI`` needs to survive these tests on any supported platform at any
time. This is enforced by nightly tests.

A nightly test builds and runs all configured example files. The
BuildBot server runs on ``gauss``, the clients that do the actual
testing are distributed among the desktop machines.

To add your own input file to the nightly tests edit
``testing/list_of_files``.

Cluster
-------

.. note::

   If we want to keep this section, we have to write some information about work balancing systems
   in general, and how to set up a queuing batch script.

Running a parallel job on a cluster with PBS work balancing system
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There is a queue where you submit your job, that is the executable and
its input file. To do this you will have to write a short job file that
tells the queue where to find and how to run your code.

The following file prototype serves as the execution script for the
cluster. Copy and modify it to your needs.

::

   #!/bin/sh
   # Here are some comments the PBS system will search for.
   #...job name 
   #PBS -N jobname 
   #...nodes = number of nodes, ppn = processors per node (always 2)
   #PBS -l nodes=4:ppn=2
   # we start our job from inside the source
   # directory and we also want the results there.
   srcdir=$PBS_O_WORKDIR 
   cd $srcdir
   # number of processors
   PROCS=‘wc -l < $PBS_NODEFILE‘ 
   # parameters, to be changed!
   PREFIX=xxx 
   EXE=cca_par_ompi_cluster.fast
   INPUT=inputfile.dat
   RESTART=
   # add the InfiniBand library to the search path
   VAPILIB=/usr/local/ibgd/driver/infinihost/lib64 
   export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$VAPILIB
   MPIDIR=/cluster/openmpi-1.1.4
   $MPIDIR/bin/mpirun -np $PROCS -hostfile $PBS_NODEFILE $EXE \\
       $INPUT $PREFIX $RESTART | tee $srcdir/$PREFIX.log

This job file makes several assumptions:

-   You want to run a parallel job compiled using OpenMPI (the supported
    MPI version on the cluster.)

-   You are going to use 4 nodes with 2 processors each. If that is not
    what you want change the line

::

      #PBS -l nodes=4:ppn=2

Note: You will always end up with two processes on each node unless
you provide your own nodefile. Most of the time two processes per
node are exactly what you want.

You want the output files to go to the same directory where your
``BACI`` executable resides. If you build ``BACI`` with the
``BINIO`` flag set and switched binary output on in your input file,
this directory must be accessible from all nodes. Most probably you
will want to start the job from somewhere inside your home directory.

Note that you can use the scratch space for output. This requires a
slight change in the job file above. Additionally the binary output
does not yet support writing processor local files.

The ``PREFIX`` and ``INPUT`` variables are most likely to need a change.

- If you want to restart a calculation you have to set the ``PREFIX``
     variable accordingly, and
- specify ``RESTART=restart``. 
- Additionally the restart step must be set
     inside the input file.

To submit the job use ``qsub job.sh``, with the above file saved as
``job.sh``. Each job in the queue gets an unique number.

For a chart on the commands and command line parameters for the various cluster commands, 
see the comarison `here <https://slurm.schedmd.com/rosetta.pdf>`_.



Development environment
-----------------------

The main development tools for ``BACI`` are ``gcc``, ``g77``, ``gdb``,
``bash`` and the related tools. Most people, however, prefere some kind
of environment that plugs these tools together.

This section does only provide some hints what is there. These tools are
not necessarilly installed on your machine. But they can be using
``yum`` or ``apt-get``.

Checkout student version of BACI (version controlled: git)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| Incorporating the work of students into the current version of BACI or
  updating the students version of the code can be painful. Using git
  version control alleviates the pain to some extend and offers a more
  convenient way to provide students with BACI updates or to merge their
  work with the current revision. However, because svn and git do not
  really go great together, it takes some effort to set it up. First you
  will have to create an additional instances of BACI on your Machine.
  It is as usual checked out using svn into e.g /Baci_stud. Afterwards
  you can initialize a new git repository in /Baci_stud. From this git
  instance of BACI you can now push a version into the student’s home
  directory which is from now on referred to as /Baci_bare. The students
  can now pull a version of Baci_bare onto their local drive i.e
  /scratch to work with/compile BACI. Also they can commit changes to
  their /Baci_bare instance, from which you can pull it back up onto
  your machine. Allright, here’s what you need to do:

**In your home directory**

#. | check out a new svn version of BACI

#. | initialize a new git repository in the folder Baci_stud/
   | ``git init``

#. | add all files in the folder Baci_stud to the repository:
   | ``git add .``

#. | commit the changes:
   | ``git commit -a -m "commit message"``

#. | generate a new branch
   | ``git branch <student_name>``

#. | change to new branch:
   | ``git checkout <student_name>``

**In student’s home directory**

#. | clone the repository Baci_stud:
   | ``git clone -bare -b <student_name>``
   | ``ssh://<myname>@<serveraddress>/Baci_stud Baci_bare``

#. | delete master branch (if there is one):
   | ``git branch``\ :math:`\rightarrow`\  list all branches in the
     current clone
   | ``git branch -d master``

**In the student’s scratch**

#. generate some folder structure:

   -  ``mkdir -p /scratch/<student_name>/workspace``

   -  ``cd /scratch/<student_name>``

   -  Note: do not change into the workspace

#. | clone baci_bare to this folder (it will be your future workspace):
   | ``git clone -b <student_name> ~/Baci_bare baci``

#. start eclipse, select the proper workspace
   (``/scratch/<student_name>/workspace``) and create an empty C++
   project

#. Import ``/scratch/<student_name>/baci`` into the new project, which
   will create a copy in the workspace

#. | delete the original baci:
   | ``rm -rf /scratch/<student_name>/baci``

#. After the student made some changes to the code he/she can commit
   his/her work to the Baci_bare in his/her home directory. This way
   they can access their code from another machine next time and the
   code is backed up by the lrz

**Workflow from a student’s point of view**

#. Student makes changes in his/her baci

#. | In case the student creates new files, git commit -a is not enough.
     The new files have to be added first:
   | ``git add src/newfile1 src/newfile2``

#. | Student commits the changes to his/her local git repository in
     ``/scratch/<student_name>/workspace/baci``
   | ``git commit -a -m "commit message"``

#. | Transfer changes to baci_bare in student’s home so that the
     supervisor can harvest the changes:
   | ``git push``

#. After further changes go back to step 2.

**Update baci version of the student**

In student’s home directory (scratch/student_name/workspace/baci)
                                                                 

#. | check status of working copy:
   | ``git status``

#. | if there are any changes:
   | ``git add <new file> <new folder>`` (or add everything with
     ``git add .``)
   | ``git commit -a -m "message"``

In your home directory (Baci_stud)
                                  

First, we pull the students version to your home directory:

#. | switch to student branch:
   | ``git checkout <student_name>``

#. | get the BACI version of your student:
   | ``git pull ssh://<student_name>@redxx.stud/~/Baci_bare/``
   | In order to update the student’s code you have to update the svn
     and the git instances of BACI on your machine:

#. | make sure, that you are on branch master:
   | ``git checkout master``

#. | update BACI in Baci_stud:
   | ``svn update –force``

#. | check status of master:
   | ``git status``

#. | if there are any untracked files or folders, add and commit them:
   | ``git add <file> <folder>``
   | ``git commit -a -m "commit message"``
   | The master branch has now been updated to the latest version of
     BACI. Next, merge the student and the master branch:

#. | switch to branch student:
   | ``git checkout <student_name>``

#. | merge with master:
   | ``git merge master``

#. | resolve conflicts:
   | There will probably be some confilcts. They will be listed under
     "both modified", when looking at the current status:
   | ``git status``
   | Resolve the conflicts with, e.g., kdiff3:
   | ``git mergetool -t kdiff3`` (maybe you must install kdiff3)
   | After resolving, add and commit the modified files and folders:
   | ``git add <file>``
   | ``git commit -a -m "commit message"``
   | Now, the merged version needs to be pushed back to your student’s
     home:

#. | make sure, you are on branch student:
   | ``git checkout <student_name>``

#. | push the new version to your student’s home:
   | ``git push ssh://<student_name>@redxx.stud/~/Baci_bare/``

.. _in-students-home-directory-scratchstudent_nameworkspacebaci-1:

In student’s home directory (scratch/student_name/workspace/baci)
                                                                 

The last step is to update the local version of your student

#. | just to check:
   | ``git status``
   | There should be no conflicts/untracked files...

#. | update the local version of BACI:
   | ``git pull``

**Useful commands**

-  | ``git status``
   | overview over the actual status of your repository: untracked files
     (can be added to the repository) and tracked files with
     modifications (can be commited)

-  | ``git branch``
   | list all branches in the current clone

-  | ``git diff $*``\ :math:`\mid`\ ``kompare -``
   | compare the files in kompare

-  | ``git diff branch_name $*``\ :math:`\mid`\ ``kompare -``
   | compare current branch to branch_name in kompare

-  Before you can update your version in your home directory, you need
   to commit your modifications on the branch student_name (scratch).
   Commands are executed in /scratch/student_name/workspace/Baci:

   -  ``git commit -a``,

   -  | ``git commit ’file’``
      | After commiting the modifications the command ’git diff’ does
        not show anymore any differences in the files. Now, the git
        version in the branch student_name on scratch matches the
        version on your computer

   -  | ``git add .``, ``git add ’file’``
      | all files or a particular file is added to the version control

-  At different computers the student has always access to the latest
   baci version stored in his home directory (Baci_bare). Therefore,
   Baci_bare need to be updated regularly.

   -  | git pull cannot be executed from baci_bare:
      | ``git push origin`` (executed student’s scratch)

   -  | getting latest version from Baci_bare:
      | ``git pull origin`` (executed student’s scratch)

-  | ``gitk``
   | graphical representation of the history

Eclipse version: Galileo
~~~~~~~~~~~~~~~~~~~~~~~~

#. execute ``eclipse`` (see paragraph: In students home directory) to
   start eclipse

#. browse to your ``workspace``: ``/scratch/student_name/workspace`` and
   press Ok

#. If you are not already there, choose "Workbench"

#. ``File -> New -> C++ Project...``

#. project name need to be identical to the folder ``baci`` generated
   with git

   -  ``Project name: baci``

   -  choose ``Executable``\ :math:`\rightarrow`\ ``empty project``

   -  press ``Finish``

   -  your folder structure should show up in the tab
      ``Project Explorer``

#. Change to the shell and configure BACI as usual:

   -  ``mkdir /scratch/<student_name>/workspace/baci_debug``

   -  ``cd /scratch/<student_name>/workspace/baci_debug``

   -  ``../baci/do-configure –debug``

   -  it will take some seconds

#. Now go back to eclipse

#. If you are not already there, choose "Workbench".

#. Right-Click on BACI in the Project Explorer and Choose ``Properties``

#. ``C/C++ Build`` :math:`\rightarrow` tab ``Builder Settings``:

   -  De-Select ``Generate Makefiles automatically``

   -  | ``Build directory``: browse via ``File system`` to your folder
      | /scratch/student_name/workspace/baci_debug

#. ``C/C++ Build`` :math:`\rightarrow` tab ``Behaviour``:

   -  Activate ``Use parallel build`` on 2 processors

   -  remove "all" from ``Build on resource save (Auto build)``

   -  remove "all" from ``Build (Incremental build)``

#. Open subitem of ``C/C++ Build`` named ``Discovery Options``

   -  ``Discover Profile Options``: browse to your previously created
      Makefile

   -  press ``Load`` to load all defines flag from your Makefile
      (baci_debug/Makefile)

#. Open subitem of ``C/C++ General`` named ``Path and Symbols``

   -  tab ``Includes``, add: Directory :math:`\rightarrow` Max and
      select ``Add to all configurations``, ``Add to all language``
      (``Is a workspace path`` need to be deselected)

   -  Under ``Symbols``, add: Name :math:`\rightarrow` Moritz and select
      ``Add to all configurations``, ``Add to all language``

#. Restart eclipse!

#. compile project by ``Project`` :math:`\rightarrow` ``Build all``

#. configure tab size:

   -  window :math:`\rightarrow` *preferences*

   -  *C/C++ General* :math:`\rightarrow` *Code Style*

   -  tick *enable project specific settings*

   -  chose *myprofile* (until now there is no institute style file) or

   -  generate a new profile including

      -  Tab policy: spaces only

      -  indendation size: 2 white spaces

      -  tab size: 2 white spaces


Text editors 
~~~~~~~~~~~~~

That is a matter of taste, really. Only those with taste choose the
right one: ``xemacs``. However, those of us with taste and style choose
``emacs``.

.. _xemacs:

**xemacs**

The right one.

A text editor that has shortcuts for everything ever imagined. Very
huge. Comes with extensive documentation. Contains the elisp programming
language for customization and extention. Has been around for ages. Is a
standard on its own. It does not stick to any standard you are likely to
know. Love it or hate it.

It takes some effort to become fluent with xemacs. You have to remember
some seemingly random keyboard shortcuts. The benefits are unimagined
text editing power. Some clever customizations are available that make
editing with xemacs even more fun. (Derived from the xemacs
customizations used by the KDE people.)

.. note::

   An Emacs mode “baci.el” with highlighting (sic) for BACI input DAT files
   can be found at ``src/emacsgoodies/baci.el``.

   Installation is explained at “baci.el” itself.

A very good reason why you should give xemacs a closer look is the
``xref`` utility.

*xref*

is a cross referencing and refactoring tool to be used in connection
with (x)emacs. With this tool you can jump to the definition of any
symbol in your code. It is amazing...

``xref`` needs to be installed
before it can be used. To do so call the ``xrefsetup`` script. It will
modify your xemacs configuration file ``~/.xemacs/init.el`` and offer a
simple tutorial. Do it.

The ``xref`` configuration lives in the ``~/.xrefrc`` file. For all
projects you want to work on (e.g. all copies of ``BACI`` you happen
to keep) you will need a section in that file. A ``~/.xrefrc`` file with
just one section that covers the ``BACI`` copy located in ``~/BACI``
looks like this:

.. container:: list

   [/home/username/BACI]

     //  input files and directories (processed recursively)

     /home/username/BACI/src/

     //  directory where tag files are stored

     -refs /home/username/Xrefs/BACI

     //  split tag files using first letter

     -refalphahash

     //  include directories

     -I /lnm/lib/fc6/ser/include

     // resolve symbols using definition place

     -exactpositionresolve

     //  setting for Emacs compile and run

     -set compilefile 'mpicc -g -ansi -Wall %s'

     -set compiledir 'mpicc -g -ansi -Wall \*.c'

     -set compileproject ''

          cd /home/<username>/fem/BACI

          make


     -set run1 'ca_par_linux.debg'

     -set run2 'ca_seq_linux.debg'

     -set run5 '' // an empty run; C-F8 will only compile

     //  set default to run1   -set run ${run1}

     //  HTML configuration

     -htmlroot=/home/kuettler/HTML

     -htmlgxlist -htmllxlist -htmldirectx -htmllinenums

     -htmltab=8 -htmllinenumcolor=000000

     //  pre-processor macros and passes specification

     -DAZTEC_PACKAGE

     -DD_MLSTRUCT

     -DBINIO

     -DDEBUG

     -DD_ALE

     -DD_AXISHELL

     -DD_BEAM3

     -DD_BRICK1

     -DD_CONTACT

     -DD_FLUID

     -DD_FLUID_PM

     -DD_FLUID2

     -DFLUID2_ML

     -DD_FLUID2TU

     -DFLUID3_ML

     -DD_FLUID2_PRO

     -DD_FLUID3

     -DD_FLUID3_F

     -DD_FSI

     -DD_INTERF

     -DD_MAT

     -DD_OPTIM

     -DD_SHELL8

     -DD_SHELL9

     -DD_WALL1

     -DD_WALLGE

     -DLINUX_MUENCH

     -DPERF

     -DRESULTTEST

     -DS8CONTACT

     -DUMFPACK

     -DSOLVE_DIRICH

     -DSOLVE_DIRICH2

     -pass1

       -DPARALLEL

       -DSPOOLES_PACKAGE

     -pass2

       -DNOTPARALLEL 

Not all of that is important. In particular all the settings related to
compile, run and html are of no importance here. You might try to find
an use for that if you like. Three parts, however, are highly relevant:

-  the license string in the first line of the file

-  the directories that state where your project lives, where the
   project sources are, where to put the internal database and where to
   find additional include files

-  the list of preprocessor macros that are defined “on the command
   line.”

After you setup a project like shown above start your xemacs and choose
“Create Xref Tags” from the “Xrefactory” menu. This is going to take a
while. After that you can jump around in ``BACI`` just like you did in
the small ``xref`` tutorial. Cool! And it helps a lot!

**post_drt_monitor**


This filter ``post_drt_monitor`` is used to extract the behaviour of a
single node by writing a text file ``xxx.mon`` which contains a table
like for example

::

   # structure problem, writing nodal data of node 1
   # control information: nodal coordinates   x = 1    y = 0    z = 0
   #
   # step   time     d_x      d_y      v_x      v_y
      1    0.05  0.00615565   0.246226   9.84904
      2    0.1  0.0244717   0.486418   -0.24137
      3    0.15  0.0544966   0.714575   9.36765

This file can be readily digested by many plotting applications:
*GNUplot* is perfectly suitable.

The filter is applied by executing for instance

::

   ./post_drt_monitor --field="structure" --node=1 --file="xxx"

You can get all options by asking for ``–help``, i.e.

::

   ./post_drt_monitor --help

The filter is build by running a

::

   make post_drt_monitor

**emacs**

A close miss. Everything said in faviour of :ref:`xemacs <xemacs>` applies here as well,
except ... it misses an ``x``.

**kdevelop**

The developement environment made by the KDE people. Is meant to be a
modern IDE. There are still problems with the way we use define flags in
``BACI``. The buildin parser fails to see many files, so function
based navigation is rather limited.

**nedit, kwrite, kate**

Plain editors. Most often syntax highlighting is supported, so at least
you get colour. All the developement has to be done using the Linux
shell.

File comparators
~~~~~~~~~~~~~~~~

A file comparator allows to show neatly the differences of two files.
These are handy to compare two (or more) text files such as input files,
output files or source code.

* kompare: The standard KDE tool for the job.

* Eclipse: has a built in comparison functionality


* Kdiff3: compares two or three input files or directories

* Xxdiff: Sometimes it is helpful –indeed very helpful– to compare two text files
  and see their differences. One way to get these differences displayed
  conveniently is Xxdiff (``xxdiff`` on the command line).

  Xxdiff is a free clone of the Unix freeware Xdiff
  (<http://reality.sgiweb.org/rudy/xdiff/>); due to its heritage it looks a
  bit siliconish.

Debugging frontends
~~~~~~~~~~~~~~~~~~~

There is only one debugger: ``gdb``. But it has a very plain user
interface. Most people use a frontend application that internally talks
to ``gdb``.

Development environments that are more than text editors (xemacs,
eclipse, kdevelop) contain a debugging ability. There are, however,
standalone debugging tools. The point of these frontends is to show the
code along with the current position, the contents of variables and so
on.

**ddd**

Debugger frontend with some graphical ability. Based on motiv, therefore
it is sometimes cumbersome to use.

**kdbg**

A KDE frontend to gdb. Lacks the ability to input commands at the
``gdb`` command line. Suitable for simple debugging jobs.

**gdbtui**

Simple ``gdb`` enhancement. Text based. Nothing more that ``gdb`` with a
source code view.

**ddt**

To debug in parallel on the cluster use ``ddt``. It is installed in
``/cluster/allinea/ddt`` and supports up to 8 processes. There is one
debugging session allowed at a time.

Before you can use ``ddt`` you have to set it up so that it knows you
are running your job via OpenMPI and a PBS queue. Several steps are
required:

.. container:: itemize

   add the line

      ``export DDTMPIRUN=/cluster/openmpi-1.1.4/bin/mpirun``

   to your ``~/.bashrc``. This allows ddt to run proccesses in parallel
   on the frontend. (You are not supposed to do that regularly.)

   create a file ``~/.ddt/config.ddt`` with the following content

   .. container:: list

      DDT Config File

      [editor] 

      tab size = 4

      [fonts] 

      family = fixed 

      size = 14

      [groups] 

      filename =

      [startup] 

      default number of processes = 2 

      default debugger = gdb 

      stop at exit = no 

      stop at abort = yes

      [mpi] 

      type = OpenMPI

      [queue] 

      use queue = yes 

      template file = /home/username/pbs.qtf 

      submit = /cluster/torque/bin/qsub 

      cancel = /cluster/torque/bin/qdel JOB_ID_TAG 

      job regexp = [0-9]+ 

      display = /cluster/torque/bin/qstat 

      use num_nodes = yes 

      procs_per_node = 2

      [arguments] 

      mpi0 =

      [attach] 

      node list file = 

      hide forked children = yes

      [attach list]

      [memory debugging] 

      enabled = yes 

      setting = none 

      interval = -1 

      preload = dmalloc 

   This file can be generated using the ``ddt`` gui as well, however it
   is far more convenient to create beforehand. This configuration file
   tells ``ddt`` how to use the queue. The only line you have to change
   in the above file is the one defining the ``template file``.

   The ``template file`` is used to create a PBS job file. There are
   some tags that ``ddt`` replaces with its values before it submits the
   file into the queue. A valid template file looks like this:

   ::

      #!/bin/bash

      # DDT will generate a submission script from this by replacing these tags: 

      #        TAG NAME       |      DESCRIPTION              |      EXAMPLE 

      # --------------------------------------------------------------------------- 

      # PROGRAM_TAG           | target path and filename      | /users/ned/a.out 

      # PROGRAM_ARGUMENTS_TAG | arguments to target program   | -myarg myval 

      # NUM_PROCS_TAG         | total number of processes     | 16 

      # NUM_NODES_TAG         | number of compute nodes       | 8 

      # PROCS_PER_NODE_TAG    | processes per node            | 2 

      # 

      # Note that NUM_NODES_TAG and PROCS_PER_NODE_TAG are only valid if DDT is 

      # set to ’use NUM_NODES’ in the queue options. If not, they will be replaced 

      # with the number of processes and 1 respectively.

      #PBS -l walltime=24:00:00,nodes=NUM_NODES_TAG:ppn=PROCS_PER_NODE_TAG 

      #P BS -q debug 

      #PBS -V 

      #PBS -o PROGRAM_TAG-ddt.output 

      #PBS -e PROGRAM_TAG-ddt.error

      srcdir=$PBS_O_WORKDIR cd $srcdir

      VAPILIB=/usr/local/ibgd/driver/infinihost/lib64

      export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$VAPILIB

      MPIDIR=/cluster/openmpi-1.1.4

      $MPIDIR/bin/mpirun -hostfile $PBS_NODEFILE -np NUM_PROCS_TAG \\

          /cluster/allinea/ddt/bin/ddt-debugger PROGRAM_ARGUMENTS_TAG | tee debug_output

Once you have the configuration in place you can call ``ddt`` via

.. container:: list

   /cluster/allinea/ddt/bin/ddt cca_par_ompi_cluster.debg inputfile.dat xxx

Note: ``ddt`` has a memory debugging ability that consumes a large
amount of memory. If you want to debug large examples you will have to
turn memory debugging off.

Debugging hints
~~~~~~~~~~~~~~~

What to do when ``BACI`` fails.

**debug version of ``BACI``**

To be able to debug ``BACI`` you have to build it in debug mode. To do
this you have to uncomment the ``DEBUG`` flag in your
``config/defines.my`` file and call ``configure`` again. Do not forget
to ``make clean`` before you compile again!

Note: The ``DEBUG`` flag slows things down. You are not supposed to run
long jobs with the debug version of ``BACI``.

Note: If all you want is add the ``DEBUG`` flag to your configuration,
there is no need to actually edit your ``config/defines.my`` file.
Instead you can set the environment variable ``DEBUG`` to ``yes`` and
reconfigure:

.. container:: list

   DEBUG=yes make reconfig && make clean && make

``make reconfig`` is an abbreviation for the ``configure`` call you used
to configure in the first place. In particular both configuration files
will be read again, so if these files have changed it will do something
different.

**build in a different directory**

A ``BACI`` reconfigure overwrites the ``Makefile``. This might not be
what you want. Instead you might run ``configure`` from a directory
different from the ``BACI`` source directory. This way you will get a
Makefile in the directory you run configure from. For example, starting
from the ``BACI`` source directory you might:

.. container:: list

   mkdir quicktest

   cd quicktest

   ../configure ../config/muench.fc6.ser ../config/defines.my

   make

This will create a ``Makefile`` and from that a whole new ``BACI``
inside the ``quicktest`` directory. The ``Makefile`` in the source
directory as well all the object files from your original build will
remain untouched. All new files will be inside the ``quicktest``
directory. So you can remove the whole thing by

.. container:: list

   cd ..

   rm -r quicktest

Note: Sometimes you want to create an executable fast for just one run,
without worring about source dependencies. So you can skip the
dependency generation by setting the environment variable ``NODEPS`` to
``yes`` like this:

.. container:: list

   NODEPS=yes ../configure ../config/muench.fc6.ser ../config/defines.my

**core files**

To get information out of crashing ``BACI`` processes you want to
switch on ``core`` files. To do this add the line

.. container:: list

   ulimit -c unlimited

to your ``~/.bashrc`` and start a new shell. This will create a file
``core`` or ``core.$$`` in the current directory each time ``BACI``
crashes, where ``$$`` stands for the process id. Afterwards you can have
a look into ``BACI`` at just the moment the program failed:

.. container:: list

   gdb cca_fc6_ser.debg core

Debugger frontends can be used as well.

You might also want to include the ``DSERROR_DUMP`` flag in your
``config/defines.my`` file to have ``BACI`` crash each time a runtime
error occurs (and ``dserror`` gets called.)

Note: Writing a ``core`` file in Linux is pretty fast. However, if your
process took a large amount of memory and you run it on a nfs mounted
directory, it might take a few seconds to dump the ``core`` file.

**valgrind**

If it gets all confused and you have lost track of your pointers it
might be time to reach for something strong. Enter ``valgrind``.

``valgrind`` is a memory checker (and more!) It simulates your code,
keeps track of the memory usage and reports anything strange like using
unallocated memory, uninitialized variables, freeing the same pointer
twice or leaking memory. ``valgrind`` can save lifes!

To use it simple write valgrind before the ``BACI`` executable:

.. container:: list

   valgrind ./cca_fc6_ser.debg Input/f2_drivencavity20x20.dat xxx

The messages from ``valgrind`` might seem cryptic. But do not ignore
them. Decipher!

There are two issues with ``valgrind``: It is slow. And memory
demanding. So you will want to use it on small examples.

Graphical Developement Environment Eclipse & CDT
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

...includes most of the seperate tools from above.

**Checkout BACI**

-  ``File -> New -> Project...``

-  ``SVN -> Checkout Projects from SVN``

-  ``Create a new repository location``

-  Url: ``<something_appropriate>``

-  Select BACI (to be renamed to BACI)

-  Click Finish

-  Select Wizard: ``C++ -> C++ Project``

-  Give Projectname: baci

-  Click Finish and then OK

-  Change to console and configure BACI as usual

-  Now go back into eclipse

-  If you are not already there, choose "Workbench".

-  Right-Click on BACI in the Project Explorer and Choose ``Properties``

-  On ``C/C++ General`` in "Paths and Symbols": In the "Includes" as
   well as in the "Symbols" part click "Add": Write some dummy name in
   the new window and click at "Add to all configurations" and "Add to
   all languages"

-  On ``C/C++ Build`` De-Select "Generate Makefiles automatically"

-  In the text field "Build directory", remove the current text and add
   the absolute path of your build directory

-  Open tab "Behaviour" and remove "all" from "Build(Incremental build)"
   and press "OK"

-  Close Eclipse

-  Change to console and reconfigure BACI (the "DEFINES" are loaded now
   into BACI)

-  build BACI using "make"

-  Restart eclipse!

-  Happy development...

In case there are many red bug symbols at almost every function call or
call of external libraries (“Function/Symbol could not be resolved.”)
just rebuild the index (right click on your project :math:`\rightarrow`
Index :math:`\rightarrow` Rebuild). Close and open the file. The red bug
symbols should be gone now.

**Usage**
~~~~~~~~~~


Debugging in eclipse
~~~~~~~~~~~~~~~~~~~~

-  Add debug configuration (small bug in the bar): double click on
   ``C/C++ local application``

-  Configure debug configuration: (identical to command
   ``./baci-debug (Inputfile) (Outputfile)`` in the shell):

   -  Under ``Main``

   -  choose your project

   -  choose your application (baci_debug/baci-debug)

   -  Under ``Arguments``

   -  add ``$ {file_prompt} XXX`` to box

   -  choose your working direction (all data are saved in this folder)

BACI is now checked out and ready to use.

