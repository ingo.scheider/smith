..
   Created using baci version (git SHA1):
   eeea9d37d6cc009fe38bd81cbc4579befe0c2b2e

.. _headerparameters:

Header parameters
=================

.. _SECdiscretisation:

DISCRETISATION
--------------

no description yet

::

   -----------------------------------------------------DISCRETISATION

.. _discretisation_numfluiddis:

**NUMFLUIDDIS** | *default:* 1 |break| Number of meshes in fluid field

.. _discretisation_numstrucdis:

**NUMSTRUCDIS** | *default:* 1 |break| Number of meshes in structural field

.. _discretisation_numaledis:

**NUMALEDIS** | *default:* 1 |break| Number of meshes in ale field

.. _discretisation_numartnetdis:

**NUMARTNETDIS** | *default:* 1 |break| Number of meshes in arterial network field

.. _discretisation_numthermdis:

**NUMTHERMDIS** | *default:* 1 |break| Number of meshes in thermal field

.. _discretisation_numairwaysdis:

**NUMAIRWAYSDIS** | *default:* 1 |break| Number of meshes in reduced dimensional airways network field

.. _SECproblemsize:

PROBLEM SIZE
------------

no description yet

::

   -------------------------------------------------------PROBLEM SIZE

.. _problemsize_dim:

**DIM** | *default:* 3 |break| 2d or 3d problem

.. _problemsize_elements:

**ELEMENTS** | *default:* 0 |break| Total number of elements

.. _problemsize_nodes:

**NODES** | *default:* 0 |break| Total number of nodes

.. _problemsize_npatches:

**NPATCHES** | *default:* 0 |break| number of nurbs patches

.. _problemsize_materials:

**MATERIALS** | *default:* 0 |break| number of materials

.. _problemsize_numdf:

**NUMDF** | *default:* 3 |break| maximum number of degrees of freedom

.. _SECproblemtyp:

PROBLEM TYP
-----------

no description yet

::

   --------------------------------------------------------PROBLEM TYP

.. _problemtyp_problemtyp:

**PROBLEMTYP** | *default:* Fluid_Structure_Interaction |break| no description yet

   **Possible values:**

   - Ale
   - ArterialNetwork
   - Atherosclerosis_Fluid_Structure_Interaction
   - Biofilm_Fluid_Structure_Interaction
   - Cardiac_Monodomain
   - Elastohydrodynamic_Lubrication
   - Electrochemistry
   - Electromagnetics
   - Fluid
   - Fluid_Ale
   - Fluid_Beam_Interaction
   - Fluid_Freesurface
   - Fluid_Poro_Structure_Interaction_XFEM
   - Fluid_Porous_Structure_Interaction
   - Fluid_Porous_Structure_Scalar_Scalar_Interaction
   - Fluid_RedModels
   - Fluid_Structure_Interaction
   - Fluid_Structure_Interaction_Lung
   - Fluid_Structure_Interaction_RedModels
   - Fluid_Structure_Interaction_XFEM
   - Fluid_XFEM
   - Fluid_XFEM_LevelSet
   - Gas_Fluid_Structure_Interaction
   - Immersed_FSI
   - Inverse_Analysis
   - Level_Set
   - Low_Mach_Number_Flow
   - Lubrication
   - Multiphase_Poroelasticity
   - Multiphase_Poroelasticity_ScaTra
   - Multiphase_Porous_Flow
   - NP_Supporting_Procs
   - Particle
   - Particle_Structure_Interaction
   - Polymer_Network
   - Poroelastic_scalar_transport
   - Poroelasticity
   - RedAirways_Tissue
   - ReducedDimensionalAirWays
   - Scalar_Thermo_Interaction
   - Scalar_Transport
   - Structure
   - Structure_Ale
   - Structure_Scalar_Interaction
   - Structure_Scalar_Thermo_Interaction
   - Thermo
   - Thermo_Fluid_Structure_Interaction
   - Thermo_Structure_Interaction
   - Tutorial
   - Two_Phase_Flow

.. _problemtyp_shapefct:

**SHAPEFCT** | *default:* Polynomial |break| Defines the function spaces for the spatial approximation

   **Possible values:**

   - HDG
   - Nurbs
   - Polynomial

.. _problemtyp_restart:

**RESTART** | *default:* 0 |break| no description yet

.. _problemtyp_restarttime:

**RESTARTTIME** | *default:* -1 |break| Used defined restart time

.. _problemtyp_randseed:

**RANDSEED** | *default:* -1 |break| Set the random seed. If < 0 use current time.

.. _SECdesigndescription:

DESIGN DESCRIPTION
------------------

number of nodal clouds

::

   -------------------------------------------------DESIGN DESCRIPTION

.. _designdescription_ndpoint:

**NDPOINT** | *default:* 0 |break| number of points

.. _designdescription_ndline:

**NDLINE** | *default:* 0 |break| number of line clouds

.. _designdescription_ndsurf:

**NDSURF** | *default:* 0 |break| number of surface clouds

.. _designdescription_ndvol:

**NDVOL** | *default:* 0 |break| number of volume clouds

.. _SECstructuraldynamic:

STRUCTURAL DYNAMIC
------------------

no description yet

::

   -------------------------------------------------STRUCTURAL DYNAMIC

.. _structuraldynamic_int_strategy:

**INT_STRATEGY** | *default:* Old |break| global type of the used integration strategy

   **Possible values:**

   - Old
   - Standard
   - LOCA

.. _structuraldynamic_dynamictyp:

**DYNAMICTYP** | *default:* GenAlpha |break| type of the specific dynamic time integration scheme

   **Possible values:**

   - Statics
   - GenAlpha
   - GenAlphaLieGroup
   - OneStepTheta
   - GEMM
   - ExplicitEuler
   - CentrDiff
   - AdamsBashforth2
   - EulerMaruyama
   - EulerImpStoch

.. _structuraldynamic_prestress:

**PRESTRESS** | *default:* none |break| prestressing takes values none mulf id

   **Possible values:**

   - none
   - None
   - NONE
   - mulf
   - Mulf
   - MULF
   - id
   - Id
   - ID
   - Material_Iterative
   - MATERIAL_ITERATIVE
   - material_iterative

.. _structuraldynamic_prestresstime:

**PRESTRESSTIME** | *default:* 0 |break| time to switch from pre to post stressing

.. _structuraldynamic_prestresstoldisp:

**PRESTRESSTOLDISP** | *default:* 1e-09 |break| tolerance in the displacement norm during prestressing

.. _structuraldynamic_prestressminloadsteps:

**PRESTRESSMINLOADSTEPS** | *default:* 0 |break| Minimum number of load steps during prestressing

.. _structuraldynamic_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| save displacements and contact forces every RESULTSEVRY steps

.. _structuraldynamic_resevryergy:

**RESEVRYERGY** | *default:* 0 |break| write system energies every requested step

.. _structuraldynamic_restartevry:

**RESTARTEVRY** | *default:* 1 |break| write restart possibility every RESTARTEVRY steps

.. _structuraldynamic_timestep:

**TIMESTEP** | *default:* 0.05 |break| time step size

.. _structuraldynamic_numstep:

**NUMSTEP** | *default:* 200 |break| maximum number of steps

.. _structuraldynamic_timeinit:

**TIMEINIT** | *default:* 0 |break| initial time

.. _structuraldynamic_maxtime:

**MAXTIME** | *default:* 5 |break| maximum time

.. _structuraldynamic_damping:

**DAMPING** | *default:* No |break| type of damping: (1) Rayleigh damping matrix and use it from M_DAMP x M + K_DAMP x K, (2) Material based and calculated in elements

   **Possible values:**

   - no
   - No
   - NO
   - yes
   - Yes
   - YES
   - Rayleigh
   - Material
   - BrownianMotion

.. _structuraldynamic_m_damp:

**M_DAMP** | *default:* -1 |break| no description yet

.. _structuraldynamic_k_damp:

**K_DAMP** | *default:* -1 |break| no description yet

.. _structuraldynamic_toldisp:

**TOLDISP** | *default:* 1e-10 |break| tolerance in the displacement norm for the newton iteration

.. _structuraldynamic_norm_disp:

**NORM_DISP** | *default:* Abs |break| type of norm for displacement convergence check

   **Possible values:**

   - Abs
   - Rel
   - Mix

.. _structuraldynamic_tolres:

**TOLRES** | *default:* 1e-08 |break| tolerance in the residual norm for the newton iteration

.. _structuraldynamic_norm_resf:

**NORM_RESF** | *default:* Abs |break| type of norm for residual convergence check

   **Possible values:**

   - Abs
   - Rel
   - Mix

.. _structuraldynamic_tolpre:

**TOLPRE** | *default:* 1e-08 |break| tolerance in pressure norm for the newton iteration

.. _structuraldynamic_norm_pres:

**NORM_PRES** | *default:* Abs |break| type of norm for pressure convergence check

   **Possible values:**

   - Abs

.. _structuraldynamic_tolinco:

**TOLINCO** | *default:* 1e-08 |break| tolerance in the incompressible residual norm for the newton iteration

.. _structuraldynamic_norm_inco:

**NORM_INCO** | *default:* Abs |break| type of norm for incompressible residual convergence check

   **Possible values:**

   - Abs

.. _structuraldynamic_normcombi_disppres:

**NORMCOMBI_DISPPRES** | *default:* And |break| binary operator to combine pressure and displacement values

   **Possible values:**

   - And
   - Or

.. _structuraldynamic_normcombi_resfinco:

**NORMCOMBI_RESFINCO** | *default:* And |break| binary operator to combine force and incompressible residual

   **Possible values:**

   - And
   - Or

.. _structuraldynamic_normcombi_resfdisp:

**NORMCOMBI_RESFDISP** | *default:* And |break| binary operator to combine displacement and residual force values

   **Possible values:**

   - And
   - Or

.. _structuraldynamic_stc_scaling:

**STC_SCALING** | *default:* no |break| Scaled director conditioning for thin shell structures

   **Possible values:**

   - no
   - No
   - NO
   - Symmetric
   - Right

.. _structuraldynamic_stc_layer:

**STC_LAYER** | *default:* 1 |break| number of STC layers for multilayer case

.. _structuraldynamic_ptcdt:

**PTCDT** | *default:* 0.1 |break| pseudo time step for pseudo transient continuation (PTC) stabilized Newton procedure

.. _structuraldynamic_tolconstr:

**TOLCONSTR** | *default:* 1e-08 |break| tolerance in the constr error norm for the newton iteration

.. _structuraldynamic_tolconstrincr:

**TOLCONSTRINCR** | *default:* 1e-08 |break| tolerance in the constr lm incr norm for the newton iteration

.. _structuraldynamic_maxiter:

**MAXITER** | *default:* 50 |break| maximum number of iterations allowed for Newton-Raphson iteration before failure

.. _structuraldynamic_miniter:

**MINITER** | *default:* 0 |break| minimum number of iterations to be done within Newton-Raphson loop

.. _structuraldynamic_iternorm:

**ITERNORM** | *default:* L2 |break| type of norm to be applied to residuals

   **Possible values:**

   - L1
   - L2
   - Rms
   - Inf

.. _structuraldynamic_divercont:

**DIVERCONT** | *default:* stop |break| What to do with time integration when Newton-Raphson iteration failed

   **Possible values:**

   - stop
   - continue
   - repeat_step
   - halve_step
   - adapt_step
   - rand_adapt_step
   - rand_adapt_step_ele_err
   - repeat_simulation
   - adapt_penaltycontact
   - adapt_3D0Dptc_ele_err

.. _structuraldynamic_maxdivconrefinementlevel:

**MAXDIVCONREFINEMENTLEVEL** | *default:* 10 |break| number of times timestep is halved in case nonlinear solver diverges

.. _structuraldynamic_nlnsol:

**NLNSOL** | *default:* fullnewton |break| Nonlinear solution technique

   **Possible values:**

   - vague
   - fullnewton
   - modnewton
   - lsnewton
   - ptc
   - newtonlinuzawa
   - augmentedlagrange
   - NoxNewtonLineSearch
   - noxgeneral
   - noxnln

.. _structuraldynamic_lsmaxiter:

**LSMAXITER** | *default:* 30 |break| maximum number of line search steps

.. _structuraldynamic_alpha_ls:

**ALPHA_LS** | *default:* 0.5 |break| step reduction factor alpha in (Newton) line search scheme

.. _structuraldynamic_sigma_ls:

**SIGMA_LS** | *default:* 0.0001 |break| sufficient descent factor in (Newton) line search scheme

.. _structuraldynamic_materialtangent:

**MATERIALTANGENT** | *default:* analytical |break| way of evaluating the constitutive matrix

   **Possible values:**

   - analytical
   - finitedifferences

.. _structuraldynamic_loadlin:

**LOADLIN** | *default:* No |break| Use linearization of external follower load in Newton

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _structuraldynamic_masslin:

**MASSLIN** | *default:* No |break| Application of nonlinear inertia terms

   **Possible values:**

   - No
   - no
   - Standard
   - standard
   - Rotations
   - rotations

.. _structuraldynamic_neglectinertia:

**NEGLECTINERTIA** | *default:* No |break| Neglect inertia

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _structuraldynamic_predict:

**PREDICT** | *default:* ConstDis |break| Type of predictor

   **Possible values:**

   - Vague
   - ConstDis
   - ConstVel
   - ConstAcc
   - ConstDisVelAcc
   - TangDis
   - TangDisConstFext
   - ConstDisPres
   - ConstDisVelAccPres

.. _structuraldynamic_uzawaparam:

**UZAWAPARAM** | *default:* 1 |break| Parameter for Uzawa algorithm dealing with lagrange multipliers

.. _structuraldynamic_uzawatol:

**UZAWATOL** | *default:* 1e-08 |break| Tolerance for iterative solve with Uzawa algorithm

.. _structuraldynamic_uzawamaxiter:

**UZAWAMAXITER** | *default:* 50 |break| maximum number of iterations allowed for uzawa algorithm before failure going to next newton step

.. _structuraldynamic_uzawaalgo:

**UZAWAALGO** | *default:* direct |break| no description yet

   **Possible values:**

   - uzawa
   - simple
   - direct

.. _structuraldynamic_adaptconv:

**ADAPTCONV** | *default:* No |break| Switch on adaptive control of linear solver tolerance for nonlinear solution

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _structuraldynamic_adaptconv_better:

**ADAPTCONV_BETTER** | *default:* 0.1 |break| The linear solver shall be this much better than the current nonlinear residual in the nonlinear convergence limit

.. _structuraldynamic_lumpmass:

**LUMPMASS** | *default:* No |break| Lump the mass matrix for explicit time integration

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _structuraldynamic_modifiedexpleuler:

**MODIFIEDEXPLEULER** | *default:* Yes |break| Use the modified explicit Euler time integration scheme

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _structuraldynamic_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| number of linear solver used for structural problems

.. _structuraldynamic_geometry:

**GEOMETRY** | *default:* full |break| How the geometry is specified

   **Possible values:**

   - full
   - box
   - file

.. _structuraldynamic_midtime_energy_type:

**MIDTIME_ENERGY_TYPE** | *default:* vague |break| Specify the mid-averaging type for the structural energy contributions

   **Possible values:**

   - vague
   - imrLike
   - trLike

.. _structuraldynamic_initialdisp:

**INITIALDISP** | *default:* zero_displacement |break| Initial displacement for structure problem

   **Possible values:**

   - zero_displacement
   - displacement_by_function

.. _structuraldynamic_startfuncno:

**STARTFUNCNO** | *default:* -1 |break| Function for Initial displacement

.. _structuraldynamic_calcerror:

**CALCERROR** | *default:* no |break| Flag to (de)activate error calculations

   **Possible values:**

   - no
   - byfunct

.. _structuraldynamic_calcerrorfuncno:

**CALCERRORFUNCNO** | *default:* -1 |break| Function for Error Calculation

.. _SECstructuraldynamic_timeadaptivity:

STRUCTURAL DYNAMIC/TIMEADAPTIVITY
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ----------------------------------STRUCTURAL DYNAMIC/TIMEADAPTIVITY

.. _structuraldynamic_timeadaptivity_kind:

**KIND** | *default:* None |break| Method for time step size adaptivity

   **Possible values:**

   - None
   - ZienkiewiczXie
   - AdamsBashforth2
   - ExplicitEuler
   - CentralDifference

.. _structuraldynamic_timeadaptivity_outsysperiod:

**OUTSYSPERIOD** | *default:* 0 |break| Write system vectors (displacements, velocities, etc) every given period of time

.. _structuraldynamic_timeadaptivity_outstrperiod:

**OUTSTRPERIOD** | *default:* 0 |break| Write stress/strain every given period of time

.. _structuraldynamic_timeadaptivity_outeneperiod:

**OUTENEPERIOD** | *default:* 0 |break| Write energy every given period of time

.. _structuraldynamic_timeadaptivity_outrestperiod:

**OUTRESTPERIOD** | *default:* 0 |break| Write restart data every given period of time

.. _structuraldynamic_timeadaptivity_outsizeevery:

**OUTSIZEEVERY** | *default:* 0 |break| Write step size every given time step

.. _structuraldynamic_timeadaptivity_stepsizemax:

**STEPSIZEMAX** | *default:* 0 |break| Limit maximally permitted time step size (>0)

.. _structuraldynamic_timeadaptivity_stepsizemin:

**STEPSIZEMIN** | *default:* 0 |break| Limit minimally allowed time step size (>0)

.. _structuraldynamic_timeadaptivity_sizeratiomax:

**SIZERATIOMAX** | *default:* 0 |break| Limit maximally permitted change of time step size compared to previous size, important for multi-step schemes (>0)

.. _structuraldynamic_timeadaptivity_sizeratiomin:

**SIZERATIOMIN** | *default:* 0 |break| Limit minimally permitted change of time step size compared to previous size, important for multi-step schemes (>0)

.. _structuraldynamic_timeadaptivity_sizeratioscale:

**SIZERATIOSCALE** | *default:* 0.9 |break| This is a safety factor to scale theoretical optimal step size, should be lower than 1 and must be larger than 0

.. _structuraldynamic_timeadaptivity_locerrnorm:

**LOCERRNORM** | *default:* Vague |break| Vector norm to treat error vector with

   **Possible values:**

   - Vague
   - L1
   - L2
   - Rms
   - Inf

.. _structuraldynamic_timeadaptivity_locerrtol:

**LOCERRTOL** | *default:* 0 |break| Target local error tolerance (>0)

.. _structuraldynamic_timeadaptivity_adaptstepmax:

**ADAPTSTEPMAX** | *default:* 0 |break| Limit maximally allowed step size reduction attempts (>0)

.. _SECstructuraldynamic_genalpha:

STRUCTURAL DYNAMIC/GENALPHA
~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ----------------------------------------STRUCTURAL DYNAMIC/GENALPHA

.. _structuraldynamic_genalpha_genavg:

**GENAVG** | *default:* TrLike |break| mid-average type of internal forces

   **Possible values:**

   - Vague
   - ImrLike
   - TrLike

.. _structuraldynamic_genalpha_beta:

**BETA** | *default:* -1 |break| Generalised-alpha factor in (0,1/2]

.. _structuraldynamic_genalpha_gamma:

**GAMMA** | *default:* -1 |break| Generalised-alpha factor in (0,1]

.. _structuraldynamic_genalpha_alpha_m:

**ALPHA_M** | *default:* -1 |break| Generalised-alpha factor in [0,1)

.. _structuraldynamic_genalpha_alpha_f:

**ALPHA_F** | *default:* -1 |break| Generalised-alpha factor in [0,1)

.. _structuraldynamic_genalpha_rho_inf:

**RHO_INF** | *default:* 1 |break| Spectral radius for generalised-alpha time integration, valid range is [0,1]

.. _SECstructuraldynamic_onesteptheta:

STRUCTURAL DYNAMIC/ONESTEPTHETA
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ------------------------------------STRUCTURAL DYNAMIC/ONESTEPTHETA

.. _structuraldynamic_onesteptheta_theta:

**THETA** | *default:* 0.5 |break| One-step-theta factor in (0,1]

.. _SECstructuraldynamic_gemm:

STRUCTURAL DYNAMIC/GEMM
~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   --------------------------------------------STRUCTURAL DYNAMIC/GEMM

.. _structuraldynamic_gemm_beta:

**BETA** | *default:* 0.25 |break| Generalised-alpha factor in (0,0.5]

.. _structuraldynamic_gemm_gamma:

**GAMMA** | *default:* 0.5 |break| Generalised-alpha factor in (0,1]

.. _structuraldynamic_gemm_alpha_m:

**ALPHA_M** | *default:* 0.5 |break| Generalised-alpha factor in [0,1)

.. _structuraldynamic_gemm_alpha_f:

**ALPHA_F** | *default:* 0.5 |break| Generalised-alpha factor in [0,1)

.. _structuraldynamic_gemm_xi:

**XI** | *default:* 0 |break| generalisation factor in [0,1)

.. _SECio:

IO
--

no description yet

::

   -----------------------------------------------------------------IO

.. _io_output_gmsh:

**OUTPUT_GMSH** | *default:* No |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_output_rot:

**OUTPUT_ROT** | *default:* No |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_output_spring:

**OUTPUT_SPRING** | *default:* No |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_output_bin:

**OUTPUT_BIN** | *default:* yes |break| Do you want to have binary output?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_output_every_iter:

**OUTPUT_EVERY_ITER** | *default:* no |break| Do you desire structural displ. output every Newton iteration

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_oei_file_counter:

**OEI_FILE_COUNTER** | *default:* 0 |break| Add an output name affix by introducing a additional number

.. _io_struct_ele:

**STRUCT_ELE** | *default:* Yes |break| Output of element properties

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_struct_disp:

**STRUCT_DISP** | *default:* Yes |break| Output of displacements

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_struct_vel_acc:

**STRUCT_VEL_ACC** | *default:* No |break| Output of velocity and acceleration

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_struct_se:

**STRUCT_SE** | *default:* No |break| Output of strain energy

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_struct_current_volume:

**STRUCT_CURRENT_VOLUME** | *default:* No |break| Output of current element volume as scalar value for each structural element

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_struct_stress:

**STRUCT_STRESS** | *default:* No |break| Output of stress

   **Possible values:**

   - No
   - no
   - NO
   - Yes
   - yes
   - YES
   - Cauchy
   - cauchy
   - 2PK
   - 2pk

.. _io_struct_coupling_stress:

**STRUCT_COUPLING_STRESS** | *default:* No |break| no description yet

   **Possible values:**

   - No
   - no
   - NO
   - Yes
   - yes
   - YES
   - Cauchy
   - cauchy
   - 2PK
   - 2pk

.. _io_struct_strain:

**STRUCT_STRAIN** | *default:* No |break| Output of strains

   **Possible values:**

   - No
   - no
   - NO
   - Yes
   - yes
   - YES
   - EA
   - ea
   - GL
   - gl
   - LOG
   - log

.. _io_struct_plastic_strain:

**STRUCT_PLASTIC_STRAIN** | *default:* No |break| no description yet

   **Possible values:**

   - No
   - no
   - NO
   - Yes
   - yes
   - YES
   - EA
   - ea
   - GL
   - gl

.. _io_struct_optional_quantity:

**STRUCT_OPTIONAL_QUANTITY** | *default:* No |break| Output of an optional quantity

   **Possible values:**

   - No
   - no
   - NO
   - membranethickness

.. _io_struct_surfactant:

**STRUCT_SURFACTANT** | *default:* No |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_struct_jacobian_matlab:

**STRUCT_JACOBIAN_MATLAB** | *default:* No |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_struct_condition_number:

**STRUCT_CONDITION_NUMBER** | *default:* none |break| Compute the condition number of the structural system matrix and write it to a text file.

   **Possible values:**

   - gmres_estimate
   - max_min_ev_ratio
   - one-norm
   - inf-norm
   - none

.. _io_fluid_sol:

**FLUID_SOL** | *default:* Yes |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_fluid_stress:

**FLUID_STRESS** | *default:* No |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_fluid_wall_shear_stress:

**FLUID_WALL_SHEAR_STRESS** | *default:* No |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_fluid_eledata_evry_step:

**FLUID_ELEDATA_EVRY_STEP** | *default:* No |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_fluid_nodedata_first_step:

**FLUID_NODEDATA_FIRST_STEP** | *default:* No |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_fluid_vis:

**FLUID_VIS** | *default:* No |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_therm_temperature:

**THERM_TEMPERATURE** | *default:* No |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_therm_heatflux:

**THERM_HEATFLUX** | *default:* None |break| no description yet

   **Possible values:**

   - None
   - No
   - NO
   - no
   - Current
   - Initial

.. _io_therm_tempgrad:

**THERM_TEMPGRAD** | *default:* None |break| no description yet

   **Possible values:**

   - None
   - No
   - NO
   - no
   - Current
   - Initial

.. _io_filesteps:

**FILESTEPS** | *default:* 1000 |break| Amount of timesteps written to a single result file

.. _io_stdoutevry:

**STDOUTEVRY** | *default:* 1 |break| Print to screen every n step

.. _io_write_to_screen:

**WRITE_TO_SCREEN** | *default:* Yes |break| Write screen output

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_write_to_file:

**WRITE_TO_FILE** | *default:* No |break| Write the output into a file

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_write_initial_state:

**WRITE_INITIAL_STATE** | *default:* yes |break| Do you want to write output for initial state ?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_write_final_state:

**WRITE_FINAL_STATE** | *default:* no |break| Enforce to write output/restart data at the final state regardless of the other output/restart intervals

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_prefix_group_id:

**PREFIX_GROUP_ID** | *default:* No |break| Put a <GroupID>: in front of every line

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_limit_outp_to_proc:

**LIMIT_OUTP_TO_PROC** | *default:* -1 |break| Only the specified procs will write output

.. _io_verbosity:

**VERBOSITY** | *default:* verbose |break| no description yet

   **Possible values:**

   - minimal
   - Minimal
   - standard
   - Standard
   - verbose
   - Verbose
   - debug
   - Debug

.. _io_restartwalltimeinterval:

**RESTARTWALLTIMEINTERVAL** | *default:* -1 |break| Enforce restart after this walltime interval (in seconds), smaller zero to disable

.. _io_restartevry:

**RESTARTEVRY** | *default:* -1 |break| write restart every RESTARTEVRY steps

.. _SECio_everyiteration:

IO/EVERY ITERATION
~~~~~~~~~~~~~~~~~~

no description yet

::

   -------------------------------------------------IO/EVERY ITERATION

.. _io_everyiteration_output_every_iter:

**OUTPUT_EVERY_ITER** | *default:* No |break| Do you wish output every Newton iteration?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_everyiteration_run_number:

**RUN_NUMBER** | *default:* -1 |break| Create a new folder for different runs of the same simulation. If equal -1, no folder is created.

.. _io_everyiteration_step_np_number:

**STEP_NP_NUMBER** | *default:* -1 |break| Give the number of the step (i.e. step_{n+1}) for which you want to write the debug output. If a negative step number is provided, all steps willbe written.

.. _io_everyiteration_write_owner_each_newton_iter:

**WRITE_OWNER_EACH_NEWTON_ITER** | *default:* No |break| If yes, the ownership of elements and nodes are written each Newton step, instead of only onceper time/load step.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECio_monitorstructuredbc:

IO/MONITOR STRUCTURE DBC
~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   -------------------------------------------IO/MONITOR STRUCTURE DBC

.. _io_monitorstructuredbc_interval_steps:

**INTERVAL_STEPS** | *default:* -1 |break| write reaction force output every INTERVAL_STEPS steps

.. _io_monitorstructuredbc_precision_file:

**PRECISION_FILE** | *default:* 16 |break| precision for written file

.. _io_monitorstructuredbc_precision_screen:

**PRECISION_SCREEN** | *default:* 5 |break| precision for written screen output

.. _io_monitorstructuredbc_file_type:

**FILE_TYPE** | *default:* csv |break| type of written output file

   **Possible values:**

   - csv
   - CSV
   - Csv
   - data
   - Data
   - DATA

.. _io_monitorstructuredbc_write_header:

**WRITE_HEADER** | *default:* No |break| write information about monitored boundary condition to output file

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECio_runtimevtkoutput:

IO/RUNTIME VTK OUTPUT
~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ----------------------------------------------IO/RUNTIME VTK OUTPUT

.. _io_runtimevtkoutput_interval_steps:

**INTERVAL_STEPS** | *default:* -1 |break| write VTK output at runtime every INTERVAL_STEPS steps

.. _io_runtimevtkoutput_output_data_format:

**OUTPUT_DATA_FORMAT** | *default:* binary |break| data format for written numeric data

   **Possible values:**

   - binary
   - Binary
   - ascii
   - ASCII

.. _io_runtimevtkoutput_every_iteration:

**EVERY_ITERATION** | *default:* No |break| write output in every iteration of the nonlinear solver

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECio_runtimevtkoutput_fluid:

IO/RUNTIME VTK OUTPUT/FLUID
~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ----------------------------------------IO/RUNTIME VTK OUTPUT/FLUID

.. _io_runtimevtkoutput_fluid_output_fluid:

**OUTPUT_FLUID** | *default:* No |break| write fluid output

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_fluid_velocity:

**VELOCITY** | *default:* No |break| write velocity output

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_fluid_pressure:

**PRESSURE** | *default:* No |break| write pressure output

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_fluid_acceleration:

**ACCELERATION** | *default:* No |break| write acceleration output

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_fluid_displacement:

**DISPLACEMENT** | *default:* No |break| write displacement output

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_fluid_gridvelocity:

**GRIDVELOCITY** | *default:* No |break| write grid velocity output

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_fluid_element_owner:

**ELEMENT_OWNER** | *default:* No |break| write element owner

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_fluid_element_gid:

**ELEMENT_GID** | *default:* No |break| write baci internal element GIDs

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_fluid_node_gid:

**NODE_GID** | *default:* No |break| write baci internal node GIDs

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECio_runtimevtkoutput_structure:

IO/RUNTIME VTK OUTPUT/STRUCTURE
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ------------------------------------IO/RUNTIME VTK OUTPUT/STRUCTURE

.. _io_runtimevtkoutput_structure_output_structure:

**OUTPUT_STRUCTURE** | *default:* No |break| write structure output

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_structure_displacement:

**DISPLACEMENT** | *default:* No |break| write displacement output

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_structure_velocity:

**VELOCITY** | *default:* No |break| write velocity output

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_structure_element_owner:

**ELEMENT_OWNER** | *default:* No |break| write element owner

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_structure_element_gid:

**ELEMENT_GID** | *default:* No |break| write baci internal element GIDs

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_structure_element_ghosting:

**ELEMENT_GHOSTING** | *default:* No |break| write which processors ghost the elements

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_structure_node_gid:

**NODE_GID** | *default:* No |break| write baci internal node GIDs

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_structure_stress_strain:

**STRESS_STRAIN** | *default:* No |break| Write element stress and / or strain  data. The type of stress / strain has to be selected in the --IO input section

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_structure_gauss_point_data_output_type:

**GAUSS_POINT_DATA_OUTPUT_TYPE** | *default:* none |break| Where to write gauss point data. (none, projected to nodes, projected to element center, raw at gauss points)

   **Possible values:**

   - none
   - nodes
   - element_center
   - gauss_points

.. _SECio_runtimevtkoutput_beams:

IO/RUNTIME VTK OUTPUT/BEAMS
~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ----------------------------------------IO/RUNTIME VTK OUTPUT/BEAMS

.. _io_runtimevtkoutput_beams_output_beams:

**OUTPUT_BEAMS** | *default:* No |break| write special output for beam elements

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_beams_displacement:

**DISPLACEMENT** | *default:* No |break| write displacement output

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_beams_use_absolute_positions:

**USE_ABSOLUTE_POSITIONS** | *default:* Yes |break| use absolute positions or initial positions for vtu geometry (i.e. point coordinates)

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_beams_internal_energy_element:

**INTERNAL_ENERGY_ELEMENT** | *default:* No |break| write internal (elastic) energy for each element

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_beams_kinetic_energy_element:

**KINETIC_ENERGY_ELEMENT** | *default:* No |break| write kinetic energy for each element

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_beams_triad_visualizationpoint:

**TRIAD_VISUALIZATIONPOINT** | *default:* No |break| write triads at every visualization point

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_beams_strains_gausspoint:

**STRAINS_GAUSSPOINT** | *default:* No |break| write material cross-section strains at the Gauss points

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_beams_strains_continuous:

**STRAINS_CONTINUOUS** | *default:* No |break| write material cross-section strains at the visualization points

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_beams_material_forces_gausspoint:

**MATERIAL_FORCES_GAUSSPOINT** | *default:* No |break| write material cross-section stresses at the Gauss points

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_beams_material_forces_continuous:

**MATERIAL_FORCES_CONTINUOUS** | *default:* No |break| write material cross-section stresses at the visualization points

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_beams_spatial_forces_gausspoint:

**SPATIAL_FORCES_GAUSSPOINT** | *default:* No |break| write material cross-section stresses at the Gauss points

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_beams_beamfilamentcondition:

**BEAMFILAMENTCONDITION** | *default:* No |break| write element filament numbers

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_beams_orientation_parameter:

**ORIENTATION_PARAMETER** | *default:* No |break| write element filament numbers

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_beams_rve_crosssection_forces:

**RVE_CROSSSECTION_FORCES** | *default:* No |break|  get sum of all internal forces of  

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_beams_ref_length:

**REF_LENGTH** | *default:* No |break| write reference length of all beams

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_beams_element_gid:

**ELEMENT_GID** | *default:* No |break| write the BACI internal element GIDs

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_beams_element_ghosting:

**ELEMENT_GHOSTING** | *default:* No |break| write which processors ghost the elements

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtkoutput_beams_number_subsegments:

**NUMBER_SUBSEGMENTS** | *default:* 5 |break| Number of subsegments along a single beam element for visualization

.. _SECio_runtimevtpoutputstructure:

IO/RUNTIME VTP OUTPUT STRUCTURE
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ------------------------------------IO/RUNTIME VTP OUTPUT STRUCTURE

.. _io_runtimevtpoutputstructure_interval_steps:

**INTERVAL_STEPS** | *default:* -1 |break| write VTP output at runtime every INTERVAL_STEPS steps

.. _io_runtimevtpoutputstructure_output_data_format:

**OUTPUT_DATA_FORMAT** | *default:* binary |break| data format for written numeric data

   **Possible values:**

   - binary
   - Binary
   - ascii
   - ASCII

.. _io_runtimevtpoutputstructure_every_iteration:

**EVERY_ITERATION** | *default:* No |break| write output in every iteration of the nonlinear solver

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtpoutputstructure_owner:

**OWNER** | *default:* No |break| write owner of every point

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtpoutputstructure_orientationandlength:

**ORIENTATIONANDLENGTH** | *default:* No |break| write orientation at every point

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtpoutputstructure_numberofbonds:

**NUMBEROFBONDS** | *default:* No |break| write number of bonds of every point

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _io_runtimevtpoutputstructure_linkingforce:

**LINKINGFORCE** | *default:* No |break| write force acting in linker

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECstatinverseanalysis:

STAT INVERSE ANALYSIS
---------------------

no description yet

::

   ----------------------------------------------STAT INVERSE ANALYSIS

.. _statinverseanalysis_stat_inv_analysis:

**STAT_INV_ANALYSIS** | *default:* none |break| types of statistical inverse analysis and on/off switch

   **Possible values:**

   - none
   - MonteCarloSMC
   - MonteCarloMH
   - LBFGS
   - BruteForce
   - ParticlePrediction

.. _statinverseanalysis_lbfgsinitscal:

**LBFGSINITSCAL** | *default:* yes |break| want initial scaling for the LBFGS?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _statinverseanalysis_fprestart:

**FPRESTART** | *default:* 0 |break| forward problem restart

.. _statinverseanalysis_fpoutputfilename:

**FPOUTPUTFILENAME** | *default:* none |break| controlfilename (without .control) which to use as forward problem output and restartfrom

.. _statinverseanalysis_restartevry:

**RESTARTEVRY** | *default:* 1 |break| write restart information every x-th step

.. _statinverseanalysis_parametrization:

**PARAMETRIZATION** | *default:* none |break| how to parametrize the parameter field

   **Possible values:**

   - none
   - patchwise
   - tvsvd
   - elementwise
   - uniform

.. _statinverseanalysis_num_reduct_levels:

**NUM_REDUCT_LEVELS** | *default:* 4 |break| number of levels for the basis reduction (patch-levels, or eigenvectors)

.. _statinverseanalysis_tvsvd_anasazi_nev:

**TVSVD_ANASAZI_NEV** | *default:* 10 |break| anasazi's number of eigenvectors for the eigensolver in INVANA::MatParManagerTVSVD

.. _statinverseanalysis_tvsvd_anasazi_nblocks:

**TVSVD_ANASAZI_NBLOCKS** | *default:* 4 |break| anasazi's number of blocks for the eigensolver in INVANA::MatParManagerTVSVD

.. _statinverseanalysis_tvsvd_anasazi_bsize:

**TVSVD_ANASAZI_BSIZE** | *default:* 10 |break| anasazi's blocksize for the eigensolver in INVANA::MatParManagerTVSVD

.. _statinverseanalysis_graphweights:

**GRAPHWEIGHTS** | *default:* area |break| weights for the elementwise graph creation

   **Possible values:**

   - area
   - unity

.. _statinverseanalysis_regularization:

**REGULARIZATION** | *default:* none |break| want regularization? ('tikhonov', 'totalvariation', 'none')

   **Possible values:**

   - none
   - tikhonov
   - totalvariation

.. _statinverseanalysis_objectivefunct:

**OBJECTIVEFUNCT** | *default:* none |break| choose type of objective function ('displacements', 'surfcurr')

   **Possible values:**

   - none
   - displacements
   - surfcurr

.. _statinverseanalysis_objectivefunctscal:

**OBJECTIVEFUNCTSCAL** | *default:* No |break| want scaling of objective function?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _statinverseanalysis_monitorfile:

**MONITORFILE** | *default:* none.monitor |break| filename of file containing measured displacements

.. _statinverseanalysis_targetdiscretization:

**TARGETDISCRETIZATION** | *default:* none.dat |break| datfile containing target discretization

.. _statinverseanalysis_paramlist:

**PARAMLIST** | *default:* none |break| list of std::string of parameters to be optimized, order as in INV_LIST e.g. 1 YOUNG BETA

.. _statinverseanalysis_maxiter:

**MAXITER** | *default:* 100 |break| max iterations for inverse analysis

.. _statinverseanalysis_itertopc:

**ITERTOPC** | *default:* 10 |break| iterations before parameter continuation in the forward problem

.. _statinverseanalysis_prestress:

**PRESTRESS** | *default:* none |break| prestressing takes values none mulf id

   **Possible values:**

   - none
   - None
   - NONE
   - mulf
   - Mulf
   - MULF
   - id
   - Id
   - ID

.. _statinverseanalysis_stepsize:

**STEPSIZE** | *default:* 1 |break| stepsize for the gradient descent scheme

.. _statinverseanalysis_bflinspace:

**BFLINSPACE** | *default:* 0.0 1.0 100.0 |break| linear space (a,b,n) of n linearly spaced samples in the interval [a,b] 

.. _statinverseanalysis_convtol:

**CONVTOL** | *default:* 1e-06 |break| stop optimizaiton iterations for convergence criterion below this value

.. _statinverseanalysis_reg_weight:

**REG_WEIGHT** | *default:* 1 |break| weight of the regularization functional

.. _statinverseanalysis_tvd_eps:

**TVD_EPS** | *default:* 0.01 |break| differentiation epsilon for total variation

.. _statinverseanalysis_sizestorage:

**SIZESTORAGE** | *default:* 20 |break| number of vectors to keep in storage; defaults to 20 (lbfgs usage only)

.. _statinverseanalysis_num_particles:

**NUM_PARTICLES** | *default:* 1 |break| number of particles for the sequential monte carlo.

.. _statinverseanalysis_metaparams:

**METAPARAMS** | *default:* none |break| choose type of metaparametrization (none/quad/exp/arctan)

   **Possible values:**

   - none
   - quad
   - exp
   - arctan

.. _statinverseanalysis_kernelscale:

**KERNELSCALE** | *default:* -1 |break| scale of the kernel function

.. _statinverseanalysis_measvarestim:

**MEASVARESTIM** | *default:* 1 |break| variance of the measurement noise

.. _statinverseanalysis_synthnoise:

**SYNTHNOISE** | *default:* No |break| want noise on your synthetic measurements?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _statinverseanalysis_synthnoiseseed:

**SYNTHNOISESEED** | *default:* 1 |break| seed to be used for synthetic noise generation

.. _statinverseanalysis_mc_init_scale:

**MC_INIT_SCALE** | *default:* 0.1 |break| inital scaling factor for the markov kernel in the SMC

.. _statinverseanalysis_smc_kernel_iter:

**SMC_KERNEL_ITER** | *default:* 1 |break| number of kernel applications in smc rejuvenation

.. _statinverseanalysis_map_prior_scale:

**MAP_PRIOR_SCALE** | *default:* 1 |break| scaling for the prior covariance in the LogLikePrior

.. _statinverseanalysis_map_restartfile:

**MAP_RESTARTFILE** | *default:* none |break| control file to read the maximum a posterior approximation (as initial guess) from

.. _statinverseanalysis_map_restart:

**MAP_RESTART** | *default:* 0 |break| step to read the maximum a posterior approximation (as initial guess) from

.. _statinverseanalysis_map_reduct_restartfile:

**MAP_REDUCT_RESTARTFILE** | *default:* none |break| control file to read the maximum a posterior approximation (to create a reduced basis) from

.. _statinverseanalysis_map_reduct_restart:

**MAP_REDUCT_RESTART** | *default:* 0 |break| step to read the maximum a posterior approximation (to create a reduced basis) from

.. _statinverseanalysis_smc_ess_reduction:

**SMC_ESS_REDUCTION** | *default:* 0.05 |break| targeted effective sample size reduction per step

.. _statinverseanalysis_mh_accadapt_iter:

**MH_ACCADAPT_ITER** | *default:* 0 |break| iterations used to adapt the acceptance ratio

.. _statinverseanalysis_mh_accadapt_evry:

**MH_ACCADAPT_EVRY** | *default:* 0 |break| adapt the acceptance ratio every x iterations

.. _statinverseanalysis_mh_thin:

**MH_THIN** | *default:* 0 |break| use only every thin-th sample for the statistic

.. _statinverseanalysis_mh_burnin:

**MH_BURNIN** | *default:* 0 |break| use samples in the statistic only after burnin

.. _statinverseanalysis_init_type:

**INIT_TYPE** | *default:* dat |break| how to initialize the optimization

   **Possible values:**

   - dat
   - map

.. _SECmortarcoupling:

MORTAR COUPLING
---------------

no description yet

::

   ----------------------------------------------------MORTAR COUPLING

.. _mortarcoupling_lm_shapefcn:

**LM_SHAPEFCN** | *default:* Dual |break| Type of employed set of shape functions

   **Possible values:**

   - Dual
   - dual
   - Standard
   - standard
   - std
   - PetrovGalerkin
   - petrovgalerkin
   - pg

.. _mortarcoupling_search_algorithm:

**SEARCH_ALGORITHM** | *default:* Binarytree |break| Type of contact search

   **Possible values:**

   - BruteForce
   - bruteforce
   - BruteForceEleBased
   - bruteforceelebased
   - BinaryTree
   - Binarytree
   - binarytree

.. _mortarcoupling_binarytree_updatetype:

**BINARYTREE_UPDATETYPE** | *default:* BottomUp |break| Type of binary tree update, which is either a bottom up or a top down approach.

   **Possible values:**

   - BottomUp
   - TopDown

.. _mortarcoupling_search_param:

**SEARCH_PARAM** | *default:* 0.3 |break| Radius / Bounding volume inflation for contact search

.. _mortarcoupling_search_use_aux_pos:

**SEARCH_USE_AUX_POS** | *default:* Yes |break| If chosen auxiliary position is used for computing dops

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _mortarcoupling_lm_quad:

**LM_QUAD** | *default:* undefined |break| Type of LM interpolation for quadratic FE

   **Possible values:**

   - undefined
   - quad
   - quadratic
   - pwlin
   - piecewiselinear
   - lin
   - linear
   - const

.. _mortarcoupling_crosspoints:

**CROSSPOINTS** | *default:* No |break| If chosen, multipliers are removed from crosspoints / edge nodes

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _mortarcoupling_lm_dual_consistent:

**LM_DUAL_CONSISTENT** | *default:* boundary |break| For which elements should the dual basis be calculated on EXACTLY the same GPs as the contact terms

   **Possible values:**

   - none
   - boundary
   - all

.. _mortarcoupling_mesh_relocation:

**MESH_RELOCATION** | *default:* Initial |break| Type of mesh relocation

   **Possible values:**

   - Initial
   - initial
   - Every_Timestep
   - every_timestep
   - No
   - no

.. _mortarcoupling_algorithm:

**ALGORITHM** | *default:* Mortar |break| Type of meshtying/contact algorithm

   **Possible values:**

   - mortar
   - Mortar
   - nts
   - NTS
   - gpts
   - GPTS
   - lts
   - LTS
   - ltl
   - LTL
   - stl
   - STL

.. _mortarcoupling_inttype:

**INTTYPE** | *default:* Segments |break| Type of numerical integration scheme

   **Possible values:**

   - Segments
   - segments
   - Elements
   - elements
   - Elements_BS
   - elements_BS

.. _mortarcoupling_numgp_per_dim:

**NUMGP_PER_DIM** | *default:* 0 |break| Number of employed integration points per dimension

.. _mortarcoupling_triangulation:

**TRIANGULATION** | *default:* Delaunay |break| Type of triangulation for segment-based integration

   **Possible values:**

   - Delaunay
   - delaunay
   - Center
   - center

.. _mortarcoupling_restart_with_meshtying:

**RESTART_WITH_MESHTYING** | *default:* No |break| Must be chosen if a non-meshtying simulation is to be restarted with meshtying

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _mortarcoupling_output_interfaces:

**OUTPUT_INTERFACES** | *default:* No |break| Write output for each mortar interface separately.
This is an additional feature, purely to enhance visualization. Currently, this is limited to solid meshtying and contact w/o friction.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECmortarcoupling_parallelredistribution:

MORTAR COUPLING/PARALLEL REDISTRIBUTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Parameters to control parallel redistribution of mortar interfaces

::

   ----------------------------MORTAR COUPLING/PARALLEL REDISTRIBUTION

.. _mortarcoupling_parallelredistribution_exploit_proximity:

**EXPLOIT_PROXIMITY** | *default:* Yes |break| Exploit information on geometric proximity to split slave interface into close and non-close parts and redistribute them independently. [Contact only]

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _mortarcoupling_parallelredistribution_ghosting_strategy:

**GHOSTING_STRATEGY** | *default:* redundant_master |break| Type of interface ghosting and ghosting extension algorithm

   **Possible values:**

   - redundant_all
   - redundant_master
   - round_robin
   - binning

.. _mortarcoupling_parallelredistribution_imbalance_tol:

**IMBALANCE_TOL** | *default:* 1.1 |break| Max. relative imbalance of subdomain size after redistribution

.. _mortarcoupling_parallelredistribution_max_balance_eval_time:

**MAX_BALANCE_EVAL_TIME** | *default:* 2 |break| Max-to-min ratio of contact evalation time per processor to triggger parallel redistribution

.. _mortarcoupling_parallelredistribution_max_balance_slave_eles:

**MAX_BALANCE_SLAVE_ELES** | *default:* 0.5 |break| Max-to-min ratio of mortar slave elements per processor to triggger parallel redistribution

.. _mortarcoupling_parallelredistribution_min_eleproc:

**MIN_ELEPROC** | *default:* 0 |break| Minimum no. of elements per processor for parallel redistribution

.. _mortarcoupling_parallelredistribution_parallel_redist:

**PARALLEL_REDIST** | *default:* Static |break| Type of redistribution algorithm

   **Possible values:**

   - None
   - none
   - No
   - no
   - Static
   - static
   - Dynamic
   - dynamic

.. _mortarcoupling_parallelredistribution_print_distribution:

**PRINT_DISTRIBUTION** | *default:* Yes |break| Print details of the parallel distribution, i.e. number of nodes/elements for each rank.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECcontactdynamic:

CONTACT DYNAMIC
---------------

no description yet

::

   ----------------------------------------------------CONTACT DYNAMIC

.. _contactdynamic_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| number of linear solver used for meshtying and contact

.. _contactdynamic_restart_with_contact:

**RESTART_WITH_CONTACT** | *default:* No |break| Must be chosen if a non-contact simulation is to be restarted with contact

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _contactdynamic_adhesion:

**ADHESION** | *default:* None |break| Type of adhesion law

   **Possible values:**

   - None
   - none
   - bounded
   - b

.. _contactdynamic_friction:

**FRICTION** | *default:* None |break| Type of friction law

   **Possible values:**

   - None
   - Stick
   - Tresca
   - Coulomb

.. _contactdynamic_frless_first:

**FRLESS_FIRST** | *default:* No |break| If chosen the first time step of a newly in contact slave node is regarded as frictionless

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _contactdynamic_gp_slip_incr:

**GP_SLIP_INCR** | *default:* No |break| If chosen the slip increment is computed gp-wise which results to a non-objective quantity, but this would be consistent to wear and tsi calculations.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _contactdynamic_strategy:

**STRATEGY** | *default:* LagrangianMultipliers |break| Type of employed solving strategy

   **Possible values:**

   - LagrangianMultipliers
   - lagrange
   - Lagrange
   - penalty
   - Penalty
   - Uzawa
   - Augmented
   - SteepestAscent
   - SteepestAscent_SP
   - StdLagrange
   - Combo
   - Nitsche
   - Ehl
   - MultiScale

.. _contactdynamic_system:

**SYSTEM** | *default:* Condensed |break| Type of linear system setup / solution

   **Possible values:**

   - Condensed
   - condensed
   - cond
   - Condensedlagmult
   - condensedlagmult
   - condlm
   - SaddlePoint
   - Saddlepoint
   - saddlepoint
   - sp
   - none

.. _contactdynamic_penaltyparam:

**PENALTYPARAM** | *default:* 0 |break| Penalty parameter for penalty / Uzawa augmented solution strategy

.. _contactdynamic_penaltyparamtan:

**PENALTYPARAMTAN** | *default:* 0 |break| Tangential penalty parameter for penalty / Uzawa augmented solution strategy

.. _contactdynamic_uzawamaxsteps:

**UZAWAMAXSTEPS** | *default:* 10 |break| Maximum no. of Uzawa steps for Uzawa solution strategy

.. _contactdynamic_uzawaconstrtol:

**UZAWACONSTRTOL** | *default:* 1e-08 |break| Tolerance of constraint norm for Uzawa solution strategy

.. _contactdynamic_semi_smooth_newton:

**SEMI_SMOOTH_NEWTON** | *default:* Yes |break| If chosen semi-smooth Newton concept is applied

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _contactdynamic_semi_smooth_cn:

**SEMI_SMOOTH_CN** | *default:* 1 |break| Weighting factor cn for semi-smooth PDASS

.. _contactdynamic_semi_smooth_ct:

**SEMI_SMOOTH_CT** | *default:* 1 |break| Weighting factor ct for semi-smooth PDASS

.. _contactdynamic_contactforce_endtime:

**CONTACTFORCE_ENDTIME** | *default:* No |break| If chosen, the contact force is not evaluated at the generalized midpoint, but at the end of the time step

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _contactdynamic_velocity_update:

**VELOCITY_UPDATE** | *default:* No |break| If chosen, velocity update method is applied

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _contactdynamic_emoutput:

**EMOUTPUT** | *default:* None |break| Type of energy and momentum output

   **Possible values:**

   - None
   - none
   - No
   - no
   - Screen
   - screen
   - File
   - file
   - Both
   - both

.. _contactdynamic_error_norms:

**ERROR_NORMS** | *default:* None |break| Choice of analytical solution for error norm computation

   **Possible values:**

   - None
   - none
   - No
   - no
   - Zero
   - zero
   - Bending
   - bending
   - Sphere
   - sphere
   - Thick
   - thick
   - Plate
   - plate

.. _contactdynamic_initcontactbygap:

**INITCONTACTBYGAP** | *default:* No |break| Initialize init contact by weighted gap vector

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _contactdynamic_initcontactgapvalue:

**INITCONTACTGAPVALUE** | *default:* 0 |break| Value for initialization of init contact set with gap vector

.. _contactdynamic_normcombi_resfcontconstr:

**NORMCOMBI_RESFCONTCONSTR** | *default:* And |break| binary operator to combine contact constraints and residual force values

   **Possible values:**

   - And
   - Or

.. _contactdynamic_normcombi_displagr:

**NORMCOMBI_DISPLAGR** | *default:* And |break| binary operator to combine displacement increments and Lagrange multiplier increment values

   **Possible values:**

   - And
   - Or

.. _contactdynamic_tolcontconstr:

**TOLCONTCONSTR** | *default:* 1e-06 |break| tolerance in the contact constraint norm for the newton iteration (saddlepoint formulation only)

.. _contactdynamic_tollagr:

**TOLLAGR** | *default:* 1e-06 |break| tolerance in the LM norm for the newton iteration (saddlepoint formulation only)

.. _contactdynamic_constraint_directions:

**CONSTRAINT_DIRECTIONS** | *default:* ntt |break| formulation of constraints in normal/tangential or xyz-direction

   **Possible values:**

   - ntt
   - xyz

.. _contactdynamic_contact_regularization:

**CONTACT_REGULARIZATION** | *default:* no |break| use regularized contact

   **Possible values:**

   - no
   - tanh

.. _contactdynamic_nonsmooth_geometries:

**NONSMOOTH_GEOMETRIES** | *default:* No |break| If chosen the contact algorithm combines mortar and nts formulations. This is needed if contact between entities of different geometric dimension (such as contact between surfaces and lines, or lines and nodes) can occur

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _contactdynamic_nonsmooth_contact_surface:

**NONSMOOTH_CONTACT_SURFACE** | *default:* No |break| This flag is used to alter the criterion for the evaluation of the so-called qualified vectors in the case of a self contact scenario. This is needed as the standard criterion is only valid for smooth surfaces and thus has to be altered, if the surface that is defined to be a self contact surface is non-smooth!

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _contactdynamic_hybrid_angle_min:

**HYBRID_ANGLE_MIN** | *default:* -1 |break| Non-smooth contact: angle between cpp normal and element normal: begin transition (Mortar)

.. _contactdynamic_hybrid_angle_max:

**HYBRID_ANGLE_MAX** | *default:* -1 |break| Non-smooth contact: angle between cpp normal and element normal: end transition (NTS)

.. _contactdynamic_cpp_normals:

**CPP_NORMALS** | *default:* No |break| If chosen the nodal normal field is created as averaged CPP normal field.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _contactdynamic_timing_details:

**TIMING_DETAILS** | *default:* No |break| Enable and print detailed contact timings to screen.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _contactdynamic_nitsche_theta:

**NITSCHE_THETA** | *default:* 0 |break| +1: symmetric, 0: non-symmetric, -1: skew-symmetric

.. _contactdynamic_nitsche_theta_2:

**NITSCHE_THETA_2** | *default:* 1 |break| +1: Chouly-type, 0: Burman penalty-free (only with theta=-1)

.. _contactdynamic_nitsche_weighting:

**NITSCHE_WEIGHTING** | *default:* harmonic |break| how to weight consistency terms in Nitsche contact formulation

   **Possible values:**

   - slave
   - master
   - harmonic

.. _contactdynamic_nitsche_penalty_adaptive:

**NITSCHE_PENALTY_ADAPTIVE** | *default:* yes |break| adapt penalty parameter after each converged time step

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _contactdynamic_regularized_normal_contact:

**REGULARIZED_NORMAL_CONTACT** | *default:* No |break| add a regularized normal contact formulation

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _contactdynamic_regularization_thickness:

**REGULARIZATION_THICKNESS** | *default:* -1 |break| maximum contact penetration

.. _contactdynamic_regularization_stiffness:

**REGULARIZATION_STIFFNESS** | *default:* -1 |break| initial contact stiffness (i.e. initial "penalty parameter")

.. _SECcontactdynamic_augmented:

CONTACT DYNAMIC/AUGMENTED
~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ------------------------------------------CONTACT DYNAMIC/AUGMENTED

.. _contactdynamic_augmented_print_linear_conservation:

**PRINT_LINEAR_CONSERVATION** | *default:* No |break| Do and print the linear momentum conservation check.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _contactdynamic_augmented_print_angular_conservation:

**PRINT_ANGULAR_CONSERVATION** | *default:* No |break| Do and print the angular momentum conservation check.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _contactdynamic_augmented_variational_approach:

**VARIATIONAL_APPROACH** | *default:* incomplete |break| Type of employed variational approach

   **Possible values:**

   - complete
   - incomplete

.. _contactdynamic_augmented_add_inactive_force_contributions:

**ADD_INACTIVE_FORCE_CONTRIBUTIONS** | *default:* No |break| Add the contribution from the inactive Lagrange multipliers to theforce balance.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _contactdynamic_augmented_assemble_strategy:

**ASSEMBLE_STRATEGY** | *default:* node_based |break| Type of employed assemble strategy

   **Possible values:**

   - node_based

.. _contactdynamic_augmented_parallel_redist_interval:

**PARALLEL_REDIST_INTERVAL** | *default:* -1 |break| Specifies the Newton iteration interval in which the parallel redistribution is controlled. An interval value equal to or smaller than zero disables the dynamic redistribution control mechanism during the Newton iteration.

.. _SECcontactdynamic_augmented_steepestascent:

CONTACT DYNAMIC/AUGMENTED/STEEPESTASCENT
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ---------------------------CONTACT DYNAMIC/AUGMENTED/STEEPESTASCENT

.. _contactdynamic_augmented_steepestascent_correction_parameter:

**CORRECTION_PARAMETER** | *default:* 0 |break| Some penalty update methods use a user-specified correction parameter, which accelerates the convergence. This parameter can be specified here.

.. _contactdynamic_augmented_steepestascent_decrease_correction_parameter:

**DECREASE_CORRECTION_PARAMETER** | *default:* 1 |break| Some penalty update methods use a user-specified decrease correction parameter, which can be used to initiate a decrease of the regularization parameter in cumbersome situations.

.. _contactdynamic_augmented_steepestascent_penalty_update:

**PENALTY_UPDATE** | *default:* Vague |break| Which kind of penalty update should be used?

   **Possible values:**

   - Vague
   - SufficientLinearReduction
   - SufficientAngle
   - None

.. _SECcontactdynamic_augmented_combo:

CONTACT DYNAMIC/AUGMENTED/COMBO
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ------------------------------------CONTACT DYNAMIC/AUGMENTED/COMBO

.. _contactdynamic_augmented_combo_strategy_0:

**STRATEGY_0** | *default:* Vague |break| Type of first solving strategy

   **Possible values:**

   - Vague
   - Augmented
   - StdLagrange
   - SteepestAscent
   - SteepestAscent_SP

.. _contactdynamic_augmented_combo_strategy_1:

**STRATEGY_1** | *default:* Vague |break| Type of second solving strategy

   **Possible values:**

   - Vague
   - Augmented
   - StdLagrange
   - SteepestAscent
   - SteepestAscent_SP

.. _contactdynamic_augmented_combo_linear_solver_strategy_0:

**LINEAR_SOLVER_STRATEGY_0** | *default:* -1 |break| Linear solver for STRATEGY_0 of the COMBO strategy.

.. _contactdynamic_augmented_combo_linear_solver_strategy_1:

**LINEAR_SOLVER_STRATEGY_1** | *default:* -1 |break| Linear solver for STRATEGY_1 of the COMBO strategy.

.. _contactdynamic_augmented_combo_switching_strategy:

**SWITCHING_STRATEGY** | *default:* PreAsymptotic |break| Type of switching strategy to switch between the different solving strategies

   **Possible values:**

   - PreAsymptotic

.. _contactdynamic_augmented_combo_print2screen:

**PRINT2SCREEN** | *default:* Yes |break| Activate the screen output of the COMBO strategy and the different switching strategies.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECcontactdynamic_augmented_lagrange_multiplier_function:

CONTACT DYNAMIC/AUGMENTED/LAGRANGE_MULTIPLIER_FUNCTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   -------------CONTACT DYNAMIC/AUGMENTED/LAGRANGE_MULTIPLIER_FUNCTION

.. _contactdynamic_augmented_lagrange_multiplier_function_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| Linear Solver number for the least squares problem.

.. _SECcontactdynamic_augmented_plot:

CONTACT DYNAMIC/AUGMENTED/PLOT
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   -------------------------------------CONTACT DYNAMIC/AUGMENTED/PLOT

.. _contactdynamic_augmented_plot_resolution_x:

**RESOLUTION_X** | *default:* 10 |break| Plot resolution in x/displacement direction

.. _contactdynamic_augmented_plot_resolution_y:

**RESOLUTION_Y** | *default:* 10 |break| Plot resolution in y/Lagrange multiplier direction

.. _contactdynamic_augmented_plot_step:

**STEP** | *default:* -1 |break| Plot this step.

.. _contactdynamic_augmented_plot_iter:

**ITER** | *default:* -1 |break| Plot this iteration of the specified step.

.. _contactdynamic_augmented_plot_output_precision:

**OUTPUT_PRECISION** | *default:* 16 |break| Precision for scientific numbers.

.. _contactdynamic_augmented_plot_x_type:

**X_TYPE** | *default:* vague |break| Support type for the x-direction.

   **Possible values:**

   - vague
   - step_length
   - characteristic_element_length
   - position_angle
   - position_distance

.. _contactdynamic_augmented_plot_min_x:

**MIN_X** | *default:* 1 |break| no description yet

.. _contactdynamic_augmented_plot_max_x:

**MAX_X** | *default:* 1 |break| no description yet

.. _contactdynamic_augmented_plot_first_ref_point:

**FIRST_REF_POINT** | *default:* 0.0 0.0 0.0 |break| coordinates of the first reference point

.. _contactdynamic_augmented_plot_second_ref_point:

**SECOND_REF_POINT** | *default:* 0.0 0.0 0.0 |break| coordinates of the second reference point

.. _contactdynamic_augmented_plot_y_type:

**Y_TYPE** | *default:* vague |break| Support type for the y-direction. Only relevant for multi-dimensional plots.

   **Possible values:**

   - vague
   - step_length
   - characteristic_element_length
   - position_angle

.. _contactdynamic_augmented_plot_min_y:

**MIN_Y** | *default:* 1 |break| no description yet

.. _contactdynamic_augmented_plot_max_y:

**MAX_Y** | *default:* 1 |break| no description yet

.. _contactdynamic_augmented_plot_func_name:

**FUNC_NAME** | *default:* vague |break| Plot type.

   **Possible values:**

   - vague
   - Lagrangian
   - Infeasibility
   - Energy
   - Energy_Gradient
   - Weighted_Gap
   - Weighted_Gap_Gradient
   - Weighted_Gap_Modified_Gradient
   - Weighted_Gap_Gradient_Error
   - Weighted_Gap_Gradient_Nodal_Jacobian_Error
   - Weighted_Gap_Gradient_Nodal_Ma_Proj_Error

.. _contactdynamic_augmented_plot_file_format:

**FILE_FORMAT** | *default:* matlab |break| Format of the written text file.

   **Possible values:**

   - matlab
   - pgfplot

.. _contactdynamic_augmented_plot_file_open_mode:

**FILE_OPEN_MODE** | *default:* truncate |break| File opening mode.

   **Possible values:**

   - truncate
   - append

.. _contactdynamic_augmented_plot_wgap_node_gid:

**WGAP_NODE_GID** | *default:* -1 |break| Weighted gap of the slave node with this global ID will be considered, if FUNC_NAME == "Weighted_GAP".

.. _contactdynamic_augmented_plot_mode:

**MODE** | *default:* off |break| Plot mode.

   **Possible values:**

   - off
   - write_single_iteration_of_step
   - write_each_iteration_of_step
   - write_last_iteration_of_step

.. _contactdynamic_augmented_plot_type:

**TYPE** | *default:* line |break| Plot type.

   **Possible values:**

   - vague
   - scalar
   - line
   - surface
   - vector_field_2d

.. _contactdynamic_augmented_plot_direction:

**DIRECTION** | *default:* vague |break| Choose the direction for the plot.

   **Possible values:**

   - vague
   - current
   - read_from_file
   - zero

.. _contactdynamic_augmented_plot_direction_file:

**DIRECTION_FILE** | *default:* none |break| Filename of the text file containing the plot direction.

.. _contactdynamic_augmented_plot_direction_split:

**DIRECTION_SPLIT** | *default:* vague |break| Choose how to split the search direction for a multi-dimensional plot, e.g. a surface plot.

   **Possible values:**

   - vague
   - displ_lm
   - slave_master_displ

.. _contactdynamic_augmented_plot_reference_type:

**REFERENCE_TYPE** | *default:* vague |break| Reference state for the plot.

   **Possible values:**

   - vague
   - current_solution
   - previous_solution

.. _SECvolmortarcoupling:

VOLMORTAR COUPLING
------------------

no description yet

::

   -------------------------------------------------VOLMORTAR COUPLING

.. _volmortarcoupling_inttype:

**INTTYPE** | *default:* Elements |break| Type of numerical integration scheme

   **Possible values:**

   - Elements
   - elements
   - Segments
   - segments

.. _volmortarcoupling_couplingtype:

**COUPLINGTYPE** | *default:* Volmortar |break| Type of coupling

   **Possible values:**

   - Volmortar
   - volmortar
   - consistentinterpolation
   - consint

.. _volmortarcoupling_shapefcn:

**SHAPEFCN** | *default:* Dual |break| Type of employed set of shape functions

   **Possible values:**

   - Dual
   - dual
   - Standard
   - standard
   - std

.. _volmortarcoupling_cuttype:

**CUTTYPE** | *default:* dd |break| Type of cut procedure/ integration point calculation

   **Possible values:**

   - dd
   - directdivergence
   - DirectDivergence
   - tessellation
   - t
   - Tessellation

.. _volmortarcoupling_dualquad:

**DUALQUAD** | *default:* nomod |break| Type of dual shape function for weighting function for quadr. problems

   **Possible values:**

   - nm
   - nomod
   - lm
   - lin_mod
   - qm
   - quad_mod

.. _volmortarcoupling_mesh_init:

**MESH_INIT** | *default:* No |break| If chosen, mesh initialization procedure is performed

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _volmortarcoupling_keep_extendedghosting:

**KEEP_EXTENDEDGHOSTING** | *default:* Yes |break| If chosen, extended ghosting is kept for simulation

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECwear:

WEAR
----

no description yet

::

   ---------------------------------------------------------------WEAR

.. _wear_wearlaw:

**WEARLAW** | *default:* None |break| Type of wear law

   **Possible values:**

   - None
   - none
   - Archard
   - archard

.. _wear_matchinggrid:

**MATCHINGGRID** | *default:* Yes |break| is matching grid

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _wear_wearcoeff_conf:

**WEARCOEFF_CONF** | *default:* material |break| configuration in which wcoeff is defined

   **Possible values:**

   - material
   - mat
   - spatial
   - sp

.. _wear_wear_shape_evo:

**WEAR_SHAPE_EVO** | *default:* material |break| configuration for shape evolution step

   **Possible values:**

   - material
   - mat
   - spatial
   - sp

.. _wear_wear_shapefcn:

**WEAR_SHAPEFCN** | *default:* std |break| Type of employed set of shape functions for wear

   **Possible values:**

   - Dual
   - dual
   - Standard
   - standard
   - std

.. _wear_wearcoeff:

**WEARCOEFF** | *default:* 0 |break| Wear coefficient for slave surface

.. _wear_wearcoeff_master:

**WEARCOEFF_MASTER** | *default:* 0 |break| Wear coefficient for master surface

.. _wear_wear_timeratio:

**WEAR_TIMERATIO** | *default:* 1 |break| Time step ratio between wear and spatial time scale

.. _wear_ssslip:

**SSSLIP** | *default:* 1 |break| Fixed slip for steady state wear

.. _wear_sswear:

**SSWEAR** | *default:* No |break| flag for steady state wear

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _wear_volmass_output:

**VOLMASS_OUTPUT** | *default:* No |break| flag for output of mass/volume in ref,mat and cur. conf.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _wear_wear_side:

**WEAR_SIDE** | *default:* slave |break| Definition of wear side

   **Possible values:**

   - s
   - slave
   - Slave
   - both
   - slave_master
   - sm

.. _wear_weartype:

**WEARTYPE** | *default:* internal_state |break| Definition of wear algorithm

   **Possible values:**

   - intstate
   - is
   - internal_state
   - primvar
   - pv
   - primary_variable

.. _wear_weartimint:

**WEARTIMINT** | *default:* explicit |break| Definition of wear time integration

   **Possible values:**

   - explicit
   - e
   - expl
   - implicit
   - i
   - impl

.. _wear_wear_coupalgo:

**WEAR_COUPALGO** | *default:* stagg |break| Definition of wear (ALE) coupling algorithm

   **Possible values:**

   - stagg
   - s
   - iterstagg
   - is
   - monolithic
   - mono

.. _wear_wear_timescale:

**WEAR_TIMESCALE** | *default:* equal |break| Definition wear time scale compares to std. time scale

   **Possible values:**

   - equal
   - e
   - different
   - d

.. _SECbeamcontact:

BEAM CONTACT
------------

no description yet

::

   -------------------------------------------------------BEAM CONTACT

.. _beamcontact_beams_strategy:

**BEAMS_STRATEGY** | *default:* None |break| Type of employed solving strategy

   **Possible values:**

   - None
   - none
   - Penalty
   - penalty
   - Gmshonly
   - gmshonly

.. _beamcontact_modelevaluator:

**MODELEVALUATOR** | *default:* old |break| Type of model evaluator

   **Possible values:**

   - Old
   - old
   - Standard
   - standard

.. _beamcontact_beams_newgap:

**BEAMS_NEWGAP** | *default:* No |break| choose between original or enhanced gapfunction

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beamcontact_beams_segcon:

**BEAMS_SEGCON** | *default:* No |break| choose between beam contact with and without subsegment generation

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beamcontact_beams_debug:

**BEAMS_DEBUG** | *default:* No |break| This flag can be used for testing purposes. When it is switched on, some sanity checks are not performed!

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beamcontact_beams_inactivestiff:

**BEAMS_INACTIVESTIFF** | *default:* No |break| Always apply contact stiffness in first Newton step for pairs which have active in last time step

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beamcontact_beams_btsolmt:

**BEAMS_BTSOLMT** | *default:* No |break| decide, if also meshtying between beams and solids is possible

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beamcontact_beams_btsol:

**BEAMS_BTSOL** | *default:* No |break| decide, if also the contact between beams and solids is possible

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beamcontact_beams_btsph:

**BEAMS_BTSPH** | *default:* No |break| decide, if also the contact between beams and spheres is possible

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beamcontact_beams_endpointpenalty:

**BEAMS_ENDPOINTPENALTY** | *default:* No |break| Additional consideration of endpoint-line and endpoint-endpoint contacts

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beamcontact_beams_smoothing:

**BEAMS_SMOOTHING** | *default:* None |break| Application of smoothed tangent field

   **Possible values:**

   - None
   - none
   - Cpp
   - cpp

.. _beamcontact_beams_damping:

**BEAMS_DAMPING** | *default:* No |break| Application of a contact damping force

   **Possible values:**

   - No
   - no
   - Yes
   - yes

.. _beamcontact_beams_btbpenaltyparam:

**BEAMS_BTBPENALTYPARAM** | *default:* 0 |break| Penalty parameter for beam-to-beam point contact

.. _beamcontact_beams_btblinepenaltyparam:

**BEAMS_BTBLINEPENALTYPARAM** | *default:* -1 |break| Penalty parameter per unit length for beam-to-beam line contact

.. _beamcontact_beams_btsmtpenaltyparam:

**BEAMS_BTSMTPENALTYPARAM** | *default:* 0 |break| Penalty parameter for beam-to-solid meshtying

.. _beamcontact_beams_btspenaltyparam:

**BEAMS_BTSPENALTYPARAM** | *default:* 0 |break| Penalty parameter for beam-to-solid contact

.. _beamcontact_beams_btsph_penaltyparam:

**BEAMS_BTSPH_PENALTYPARAM** | *default:* 0 |break| Penalty parameter for beam-to-rigidsphere penalty

.. _beamcontact_beams_dampingparam:

**BEAMS_DAMPINGPARAM** | *default:* -1000 |break| Damping parameter for contact damping force

.. _beamcontact_beams_dampregparam1:

**BEAMS_DAMPREGPARAM1** | *default:* -1000 |break| First (at gap1, with gap1>gap2) regularization parameter for contact damping force

.. _beamcontact_beams_dampregparam2:

**BEAMS_DAMPREGPARAM2** | *default:* -1000 |break| Second (at gap2, with gap1>gap2) regularization parameter for contact damping force

.. _beamcontact_beams_maxdisiscalefac:

**BEAMS_MAXDISISCALEFAC** | *default:* -1 |break| Scale factor in order to limit maximal iterative displacement increment (resiudal displacement)

.. _beamcontact_beams_maxdeltadisscalefac:

**BEAMS_MAXDELTADISSCALEFAC** | *default:* 1 |break| Scale factor in order to limit maximal displacement per time step

.. _beamcontact_beams_perpshiftangle1:

**BEAMS_PERPSHIFTANGLE1** | *default:* -1 |break| Lower shift angle (in degrees) for penalty scaling of large-angle-contact

.. _beamcontact_beams_perpshiftangle2:

**BEAMS_PERPSHIFTANGLE2** | *default:* -1 |break| Upper shift angle (in degrees) for penalty scaling of large-angle-contact

.. _beamcontact_beams_parshiftangle1:

**BEAMS_PARSHIFTANGLE1** | *default:* -1 |break| Lower shift angle (in degrees) for penalty scaling of small-angle-contact

.. _beamcontact_beams_parshiftangle2:

**BEAMS_PARSHIFTANGLE2** | *default:* -1 |break| Upper shift angle (in degrees) for penalty scaling of small-angle-contact

.. _beamcontact_beams_segangle:

**BEAMS_SEGANGLE** | *default:* -1 |break| Maximal angle deviation allowed for contact search segmentation

.. _beamcontact_beams_numintegrationinterval:

**BEAMS_NUMINTEGRATIONINTERVAL** | *default:* 1 |break| Number of integration intervals per element

.. _beamcontact_beams_penaltylaw:

**BEAMS_PENALTYLAW** | *default:* LinPen |break| Applied Penalty Law

   **Possible values:**

   - LinPen
   - QuadPen
   - LinNegQuadPen
   - LinPosQuadPen
   - LinPosCubPen
   - LinPosDoubleQuadPen
   - LinPosExpPen

.. _beamcontact_beams_penregparam_g0:

**BEAMS_PENREGPARAM_G0** | *default:* -1 |break| First penalty regularization parameter G0 >=0: For gap<G0 contact is active!

.. _beamcontact_beams_penregparam_f0:

**BEAMS_PENREGPARAM_F0** | *default:* -1 |break| Second penalty regularization parameter F0 >=0: F0 represents the force at the transition point between regularized and linear force law!

.. _beamcontact_beams_penregparam_c0:

**BEAMS_PENREGPARAM_C0** | *default:* -1 |break| Third penalty regularization parameter C0 >=0: C0 has different physical meanings for the different penalty laws!

.. _beamcontact_beams_gapshiftparam:

**BEAMS_GAPSHIFTPARAM** | *default:* 0 |break| Parameter to shift penalty law!

.. _beamcontact_beams_basicstiffgap:

**BEAMS_BASICSTIFFGAP** | *default:* -1 |break| For gaps > -BEAMS_BASICSTIFFGAP, only the basic part of the contact linearization is applied!

.. _beamcontact_beams_octree:

**BEAMS_OCTREE** | *default:* None |break| octree and bounding box type for octree search routine

   **Possible values:**

   - None
   - none
   - octree_axisaligned
   - octree_cylorient
   - octree_spherical

.. _beamcontact_beams_additext:

**BEAMS_ADDITEXT** | *default:* Yes |break| Switch between No==multiplicative extrusion factor and Yes==additive extrusion factor

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beamcontact_beams_extval:

**BEAMS_EXTVAL** | *default:* -1.0 |break| extrusion value(s) of the bounding box, Depending on BEAMS_ADDITIVEEXTFAC is either additive or multiplicative. Give one or two values.

.. _beamcontact_beams_treedepth:

**BEAMS_TREEDEPTH** | *default:* 6 |break| max, tree depth of the octree

.. _beamcontact_beams_boxesinoct:

**BEAMS_BOXESINOCT** | *default:* 8 |break| max number of bounding boxes in any leaf octant

.. _SECbeamcontact_runtimevtkoutput:

BEAM CONTACT/RUNTIME VTK OUTPUT
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ------------------------------------BEAM CONTACT/RUNTIME VTK OUTPUT

.. _beamcontact_runtimevtkoutput_vtk_output_beam_contact:

**VTK_OUTPUT_BEAM_CONTACT** | *default:* No |break| write vtp output for beam contact

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beamcontact_runtimevtkoutput_interval_steps:

**INTERVAL_STEPS** | *default:* -1 |break| write VTK output at runtime every INTERVAL_STEPS steps

.. _beamcontact_runtimevtkoutput_output_data_format:

**OUTPUT_DATA_FORMAT** | *default:* binary |break| data format for written numeric data

   **Possible values:**

   - binary
   - Binary
   - ascii
   - ASCII

.. _beamcontact_runtimevtkoutput_every_iteration:

**EVERY_ITERATION** | *default:* No |break| write output in every iteration of the nonlinear solver

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beamcontact_runtimevtkoutput_contact_forces:

**CONTACT_FORCES** | *default:* No |break| write vtp output for contact forces

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beamcontact_runtimevtkoutput_gaps:

**GAPS** | *default:* No |break| write vtp output for gap, i.e. penetration

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECbeampotential:

BEAM POTENTIAL
--------------

no description yet

::

   -----------------------------------------------------BEAM POTENTIAL

.. _beampotential_pot_law_exponent:

**POT_LAW_EXPONENT** | *default:* 1.0 |break| negative(!) exponent(s)  :math:`m_i` of potential law :math:`\Phi(r) = \sum_i (k_i * r^{-m_i}).`

.. _beampotential_pot_law_prefactor:

**POT_LAW_PREFACTOR** | *default:* 0.0 |break| prefactor(s) :math:`k_i` of potential law :math:`\Phi(r) = \sum_i (k_i * r^{-m_i})`.

.. _beampotential_beampotential_type:

**BEAMPOTENTIAL_TYPE** | *default:* Surface |break| Type of potential interaction: surface (default) or volume potential

   **Possible values:**

   - Surface
   - surface
   - Volume
   - volume

.. _beampotential_strategy:

**STRATEGY** | *default:* DoubleLengthSpecific_LargeSepApprox |break| strategy to evaluate interaction potential: double/single length specific, small/large separation approximation, ...

   **Possible values:**

   - DoubleLengthSpecific_LargeSepApprox
   - DoubleLengthSpecific_SmallSepApprox
   - SingleLengthSpecific_SmallSepApprox
   - SingleLengthSpecific_SmallSepApprox_Simple

.. _beampotential_cutoff_radius:

**CUTOFF_RADIUS** | *default:* -1 |break| Neglect all potential contributions at separation largerthan this cutoff radius

.. _beampotential_regularization_type:

**REGULARIZATION_TYPE** | *default:* none |break| Type of regularization applied to the force law

   **Possible values:**

   - linear_extrapolation
   - constant_extrapolation
   - None
   - none

.. _beampotential_regularization_separation:

**REGULARIZATION_SEPARATION** | *default:* -1 |break| Use regularization of force law at separations smaller than this separation

.. _beampotential_num_integration_segments:

**NUM_INTEGRATION_SEGMENTS** | *default:* 1 |break| Number of integration segments used per beam element

.. _beampotential_num_gausspoints:

**NUM_GAUSSPOINTS** | *default:* 10 |break| Number of Gauss points used per integration segment

.. _beampotential_automatic_differentiation:

**AUTOMATIC_DIFFERENTIATION** | *default:* No |break| apply automatic differentiation via FAD?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beampotential_choice_master_slave:

**CHOICE_MASTER_SLAVE** | *default:* smaller_eleGID_is_slave |break| According to which rule shall the role of master and slave be assigned to beam elements?

   **Possible values:**

   - smaller_eleGID_is_slave
   - higher_eleGID_is_slave

.. _beampotential_beampot_btsol:

**BEAMPOT_BTSOL** | *default:* No |break| decide, whether potential-based interaction between beams and solids is considered

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beampotential_beampot_btsph:

**BEAMPOT_BTSPH** | *default:* No |break| decide, whether potential-based interaction between beams and spheres is considered

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beampotential_beampot_octree:

**BEAMPOT_OCTREE** | *default:* None |break| octree and bounding box type for octree search routine

   **Possible values:**

   - None
   - none
   - octree_axisaligned
   - octree_cylorient
   - octree_spherical

.. _beampotential_beampot_treedepth:

**BEAMPOT_TREEDEPTH** | *default:* 6 |break| max, tree depth of the octree

.. _beampotential_beampot_boxesinoct:

**BEAMPOT_BOXESINOCT** | *default:* 8 |break| max number of bounding boxes in any leaf octant

.. _SECbeampotential_runtimevtkoutput:

BEAM POTENTIAL/RUNTIME VTK OUTPUT
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ----------------------------------BEAM POTENTIAL/RUNTIME VTK OUTPUT

.. _beampotential_runtimevtkoutput_vtk_output_beam_potential:

**VTK_OUTPUT_BEAM_POTENTIAL** | *default:* No |break| write vtk output for potential-based beam interactions

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beampotential_runtimevtkoutput_interval_steps:

**INTERVAL_STEPS** | *default:* -1 |break| write VTK output at runtime every INTERVAL_STEPS steps

.. _beampotential_runtimevtkoutput_output_data_format:

**OUTPUT_DATA_FORMAT** | *default:* binary |break| data format for written numeric data

   **Possible values:**

   - binary
   - Binary
   - ascii
   - ASCII

.. _beampotential_runtimevtkoutput_every_iteration:

**EVERY_ITERATION** | *default:* No |break| write output in every iteration of the nonlinear solver

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beampotential_runtimevtkoutput_forces:

**FORCES** | *default:* No |break| write vtp output for forces

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beampotential_runtimevtkoutput_moments:

**MOMENTS** | *default:* No |break| write vtp output for moments

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beampotential_runtimevtkoutput_write_force_moment_per_elementpair:

**WRITE_FORCE_MOMENT_PER_ELEMENTPAIR** | *default:* No |break| write vtp output for forces/moments separately for each element pair

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECbeaminteraction:

BEAM INTERACTION
----------------

no description yet

::

   ---------------------------------------------------BEAM INTERACTION

.. _beaminteraction_repartitionstrategy:

**REPARTITIONSTRATEGY** | *default:* Adaptive |break| Type of employed repartitioning strategy

   **Possible values:**

   - Adaptive
   - adaptive
   - Everydt
   - everydt

.. _beaminteraction_search_strategy:

**SEARCH_STRATEGY** | *default:* bruteforce_with_binning |break| Type of search strategy used for finding coupling pairs

   **Possible values:**

   - bruteforce_with_binning
   - bounding_volume_hierarchy

.. _SECbeaminteraction_crosslinking:

BEAM INTERACTION/CROSSLINKING
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   --------------------------------------BEAM INTERACTION/CROSSLINKING

.. _beaminteraction_crosslinking_crosslinker:

**CROSSLINKER** | *default:* No |break| Crosslinker in problem

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beaminteraction_crosslinking_init_linker_boundingbox:

**INIT_LINKER_BOUNDINGBOX** | *default:* 1e12 1e12 1e12 1e12 1e12 1e12 |break| Linker are initially set randomly within this bounding box

.. _beaminteraction_crosslinking_timestep:

**TIMESTEP** | *default:* -1 |break| time step for stochastic events concerning crosslinking (e.g. diffusion, p_link, p_unlink) 

.. _beaminteraction_crosslinking_viscosity:

**VISCOSITY** | *default:* 0 |break| viscosity

.. _beaminteraction_crosslinking_kt:

**KT** | *default:* 0 |break| thermal energy

.. _beaminteraction_crosslinking_maxnuminitcrosslinkerpertype:

**MAXNUMINITCROSSLINKERPERTYPE** | *default:* 0 |break| number of initial crosslinker of certain type (additional to NUMCROSSLINKERPERTYPE) 

.. _beaminteraction_crosslinking_numcrosslinkerpertype:

**NUMCROSSLINKERPERTYPE** | *default:* 0 |break| number of crosslinker of certain type 

.. _beaminteraction_crosslinking_matcrosslinkerpertype:

**MATCROSSLINKERPERTYPE** | *default:* -1 |break| material number characterizing crosslinker type 

.. _beaminteraction_crosslinking_maxnumbondsperfilamentbspot:

**MAXNUMBONDSPERFILAMENTBSPOT** | *default:* 1 |break| maximal number of bonds per filament binding spot

.. _beaminteraction_crosslinking_filamentbspotintervalglobal:

**FILAMENTBSPOTINTERVALGLOBAL** | *default:* -1.0 |break| distance between two binding spots on all filaments

.. _beaminteraction_crosslinking_filamentbspotintervallocal:

**FILAMENTBSPOTINTERVALLOCAL** | *default:* -1.0 |break| distance between two binding spots on current filament

.. _beaminteraction_crosslinking_filamentbspotrangeglobal:

**FILAMENTBSPOTRANGEGLOBAL** | *default:* -1.0 -1.0 |break| Lower and upper arc parameter bound for binding spots on a filament

.. _beaminteraction_crosslinking_filamentbspotrangelocal:

**FILAMENTBSPOTRANGELOCAL** | *default:* 0.0 1.0 |break| Lower and upper arc parameter bound for binding spots on a filament

.. _SECbeaminteraction_spherebeamlink:

BEAM INTERACTION/SPHERE BEAM LINK
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ----------------------------------BEAM INTERACTION/SPHERE BEAM LINK

.. _beaminteraction_spherebeamlink_spherebeamlinking:

**SPHEREBEAMLINKING** | *default:* No |break| Integrins in problem

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beaminteraction_spherebeamlink_contractionrate:

**CONTRACTIONRATE** | *default:* 0 |break| contraction rate of cell (integrin linker) in [microm/s]

.. _beaminteraction_spherebeamlink_timestep:

**TIMESTEP** | *default:* -1 |break| time step for stochastic events concerning sphere beam linking (e.g. catch-slip-bond behavior) 

.. _beaminteraction_spherebeamlink_maxnumlinkerpertype:

**MAXNUMLINKERPERTYPE** | *default:* 0 |break| number of crosslinker of certain type 

.. _beaminteraction_spherebeamlink_matlinkerpertype:

**MATLINKERPERTYPE** | *default:* -1 |break| material number characterizing crosslinker type 

.. _beaminteraction_spherebeamlink_filamentbspotintervalglobal:

**FILAMENTBSPOTINTERVALGLOBAL** | *default:* -1.0 |break| distance between two binding spots on all filaments

.. _beaminteraction_spherebeamlink_filamentbspotintervallocal:

**FILAMENTBSPOTINTERVALLOCAL** | *default:* -1.0 |break| distance between two binding spots on current filament

.. _beaminteraction_spherebeamlink_filamentbspotrangeglobal:

**FILAMENTBSPOTRANGEGLOBAL** | *default:* -1.0 -1.0 |break| Lower and upper arc parameter bound for binding spots on a filament

.. _beaminteraction_spherebeamlink_filamentbspotrangelocal:

**FILAMENTBSPOTRANGELOCAL** | *default:* 0.0 1.0 |break| Lower and upper arc parameter bound for binding spots on a filament

.. _SECbeaminteraction_beamtobeamcontact:

BEAM INTERACTION/BEAM TO BEAM CONTACT
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ------------------------------BEAM INTERACTION/BEAM TO BEAM CONTACT

.. _beaminteraction_beamtobeamcontact_strategy:

**STRATEGY** | *default:* None |break| Type of employed solving strategy

   **Possible values:**

   - None
   - none
   - Penalty
   - penalty

.. _SECbeaminteraction_beamtospherecontact:

BEAM INTERACTION/BEAM TO SPHERE CONTACT
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ----------------------------BEAM INTERACTION/BEAM TO SPHERE CONTACT

.. _beaminteraction_beamtospherecontact_strategy:

**STRATEGY** | *default:* None |break| Type of employed solving strategy

   **Possible values:**

   - None
   - none
   - Penalty
   - penalty

.. _SECbeaminteraction_beamtosolidvolumemeshtying:

BEAM INTERACTION/BEAM TO SOLID VOLUME MESHTYING
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   --------------------BEAM INTERACTION/BEAM TO SOLID VOLUME MESHTYING

.. _beaminteraction_beamtosolidvolumemeshtying_contact_discretization:

**CONTACT_DISCRETIZATION** | *default:* none |break| Type of employed contact discretization

   **Possible values:**

   - none
   - gauss_point_to_segment
   - mortar
   - gauss_point_cross_section

.. _beaminteraction_beamtosolidvolumemeshtying_constraint_strategy:

**CONSTRAINT_STRATEGY** | *default:* none |break| Type of employed constraint enforcement strategy

   **Possible values:**

   - none
   - penalty

.. _beaminteraction_beamtosolidvolumemeshtying_mortar_shape_function:

**MORTAR_SHAPE_FUNCTION** | *default:* none |break| Shape function for the mortar Lagrange-multipliers

   **Possible values:**

   - none
   - line2
   - line3
   - line4

.. _beaminteraction_beamtosolidvolumemeshtying_penalty_parameter:

**PENALTY_PARAMETER** | *default:* 0 |break| Penalty parameter for beam-to-solid volume meshtying

.. _beaminteraction_beamtosolidvolumemeshtying_geometry_pair_strategy:

**GEOMETRY_PAIR_STRATEGY** | *default:* segmentation |break| Type of employed segmentation strategy

   **Possible values:**

   - none
   - segmentation
   - gauss_point_projection_without_boundary_segmentation
   - gauss_point_projection_boundary_segmentation
   - gauss_point_projection_cross_section

.. _beaminteraction_beamtosolidvolumemeshtying_geometry_pair_search_points:

**GEOMETRY_PAIR_SEARCH_POINTS** | *default:* 6 |break| Number of search points for segmentation

.. _beaminteraction_beamtosolidvolumemeshtying_gauss_points:

**GAUSS_POINTS** | *default:* 6 |break| Number of Gauss Points for the integral evaluations

.. _beaminteraction_beamtosolidvolumemeshtying_integration_points_circumference:

**INTEGRATION_POINTS_CIRCUMFERENCE** | *default:* 6 |break| Number of Integration points along the circumferencial direction of the beam. This is parameter is only used in beam to cylinder meshtying. No gauss integration is used along the circumferencial direction, equally spaced integration points are used.

.. _beaminteraction_beamtosolidvolumemeshtying_couple_restart_state:

**COUPLE_RESTART_STATE** | *default:* No |break| Enable / disable the coupling of the restart configuration.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beaminteraction_beamtosolidvolumemeshtying_rotation_coupling:

**ROTATION_COUPLING** | *default:* none |break| Type of rotational coupling

   **Possible values:**

   - none
   - deformation_gradient_3d_general_in_cross_section_plane
   - polar_decomposition_2d
   - deformation_gradient_y_2d
   - deformation_gradient_z_2d
   - deformation_gradient_average_2d
   - fix_triad_2d
   - deformation_gradient_3d_local_1
   - deformation_gradient_3d_local_2
   - deformation_gradient_3d_local_3
   - deformation_gradient_3d_general
   - deformation_gradient_3d_base_1

.. _beaminteraction_beamtosolidvolumemeshtying_rotation_coupling_mortar_shape_function:

**ROTATION_COUPLING_MORTAR_SHAPE_FUNCTION** | *default:* none |break| Shape function for the mortar Lagrange-multipliers

   **Possible values:**

   - none
   - line2
   - line3
   - line4

.. _beaminteraction_beamtosolidvolumemeshtying_rotation_coupling_penalty_parameter:

**ROTATION_COUPLING_PENALTY_PARAMETER** | *default:* 0 |break| Penalty parameter for rotational coupling in beam-to-solid volume mesh tying

.. _SECbeaminteraction_beamtosolidvolumemeshtying_runtimevtkoutput:

BEAM INTERACTION/BEAM TO SOLID VOLUME MESHTYING/RUNTIME VTK OUTPUT
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   --BEAM INTERACTION/BEAM TO SOLID VOLUME MESHTYING/RUNTIME VTK OUTPUT

.. _beaminteraction_beamtosolidvolumemeshtying_runtimevtkoutput_write_output:

**WRITE_OUTPUT** | *default:* No |break| Enable / disable beam-to-solid volume mesh tying output.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beaminteraction_beamtosolidvolumemeshtying_runtimevtkoutput_nodal_forces:

**NODAL_FORCES** | *default:* No |break| Enable / disable output of the resulting nodal forces due to beam to solid interaction.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beaminteraction_beamtosolidvolumemeshtying_runtimevtkoutput_mortar_lambda_discret:

**MORTAR_LAMBDA_DISCRET** | *default:* No |break| Enable / disable output of the discrete Lagrange multipliers at the node of the Lagrange multiplier shape functions.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beaminteraction_beamtosolidvolumemeshtying_runtimevtkoutput_mortar_lambda_continuous:

**MORTAR_LAMBDA_CONTINUOUS** | *default:* No |break| Enable / disable output of the continuous Lagrange multipliers function along the beam.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beaminteraction_beamtosolidvolumemeshtying_runtimevtkoutput_mortar_lambda_continuous_segments:

**MORTAR_LAMBDA_CONTINUOUS_SEGMENTS** | *default:* 5 |break| Number of segments for continuous mortar output

.. _beaminteraction_beamtosolidvolumemeshtying_runtimevtkoutput_segmentation:

**SEGMENTATION** | *default:* No |break| Enable / disable output of segmentation points.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beaminteraction_beamtosolidvolumemeshtying_runtimevtkoutput_integration_points:

**INTEGRATION_POINTS** | *default:* No |break| Enable / disable output of used integration points. If the contact method has 'forces' at the integration point, they will also be output.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beaminteraction_beamtosolidvolumemeshtying_runtimevtkoutput_unique_ids:

**UNIQUE_IDS** | *default:* No |break| Enable / disable output of unique IDs (mainly for testing of created VTK files).

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECbeaminteraction_beamtosolidsurfacemeshtying:

BEAM INTERACTION/BEAM TO SOLID SURFACE MESHTYING
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   -------------------BEAM INTERACTION/BEAM TO SOLID SURFACE MESHTYING

.. _beaminteraction_beamtosolidsurfacemeshtying_contact_discretization:

**CONTACT_DISCRETIZATION** | *default:* none |break| Type of employed contact discretization

   **Possible values:**

   - none
   - gauss_point_to_segment
   - mortar

.. _beaminteraction_beamtosolidsurfacemeshtying_constraint_strategy:

**CONSTRAINT_STRATEGY** | *default:* none |break| Type of employed constraint enforcement strategy

   **Possible values:**

   - none
   - penalty

.. _beaminteraction_beamtosolidsurfacemeshtying_coupling_type:

**COUPLING_TYPE** | *default:* none |break| How the coupling constraints are formulated/

   **Possible values:**

   - none
   - reference_configuration_forced_to_zero
   - reference_configuration_forced_to_zero_fad
   - displacement
   - displacement_fad
   - consistent_fad

.. _beaminteraction_beamtosolidsurfacemeshtying_mortar_shape_function:

**MORTAR_SHAPE_FUNCTION** | *default:* none |break| Shape function for the mortar Lagrange-multipliers

   **Possible values:**

   - none
   - line2
   - line3
   - line4

.. _beaminteraction_beamtosolidsurfacemeshtying_penalty_parameter:

**PENALTY_PARAMETER** | *default:* 0 |break| Penalty parameter for beam-to-solid surface meshtying

.. _beaminteraction_beamtosolidsurfacemeshtying_rotational_coupling:

**ROTATIONAL_COUPLING** | *default:* No |break| Enable / disable rotational coupling

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beaminteraction_beamtosolidsurfacemeshtying_rotational_coupling_penalty_parameter:

**ROTATIONAL_COUPLING_PENALTY_PARAMETER** | *default:* 0 |break| Penalty parameter for beam-to-solid surface rotational meshtying

.. _beaminteraction_beamtosolidsurfacemeshtying_rotational_coupling_surface_triad:

**ROTATIONAL_COUPLING_SURFACE_TRIAD** | *default:* none |break| Construction method for surface triad

   **Possible values:**

   - none
   - surface_cross_section_director
   - averaged

.. _beaminteraction_beamtosolidsurfacemeshtying_geometry_pair_strategy:

**GEOMETRY_PAIR_STRATEGY** | *default:* segmentation |break| Type of employed segmentation strategy

   **Possible values:**

   - none
   - segmentation
   - gauss_point_projection_without_boundary_segmentation
   - gauss_point_projection_boundary_segmentation
   - gauss_point_projection_cross_section

.. _beaminteraction_beamtosolidsurfacemeshtying_geometry_pair_search_points:

**GEOMETRY_PAIR_SEARCH_POINTS** | *default:* 6 |break| Number of search points for segmentation

.. _beaminteraction_beamtosolidsurfacemeshtying_gauss_points:

**GAUSS_POINTS** | *default:* 6 |break| Number of Gauss Points for the integral evaluations

.. _beaminteraction_beamtosolidsurfacemeshtying_integration_points_circumference:

**INTEGRATION_POINTS_CIRCUMFERENCE** | *default:* 6 |break| Number of Integration points along the circumferencial direction of the beam. This is parameter is only used in beam to cylinder meshtying. No gauss integration is used along the circumferencial direction, equally spaced integration points are used.

.. _beaminteraction_beamtosolidsurfacemeshtying_geometry_pair_surface_normals:

**GEOMETRY_PAIR_SURFACE_NORMALS** | *default:* standard |break| How the surface normals are evaluated

   **Possible values:**

   - standard
   - extended_volume

.. _SECbeaminteraction_beamtosolidsurfacecontact:

BEAM INTERACTION/BEAM TO SOLID SURFACE CONTACT
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ---------------------BEAM INTERACTION/BEAM TO SOLID SURFACE CONTACT

.. _beaminteraction_beamtosolidsurfacecontact_contact_discretization:

**CONTACT_DISCRETIZATION** | *default:* none |break| Type of employed contact discretization

   **Possible values:**

   - none
   - gauss_point_to_segment

.. _beaminteraction_beamtosolidsurfacecontact_constraint_strategy:

**CONSTRAINT_STRATEGY** | *default:* none |break| Type of employed constraint enforcement strategy

   **Possible values:**

   - none
   - penalty

.. _beaminteraction_beamtosolidsurfacecontact_penalty_parameter:

**PENALTY_PARAMETER** | *default:* 0 |break| Penalty parameter for beam-to-solid surface contact

.. _beaminteraction_beamtosolidsurfacecontact_contact_type:

**CONTACT_TYPE** | *default:* none |break| How the contact constraints are formulated

   **Possible values:**

   - none
   - gap_variation
   - potential

.. _beaminteraction_beamtosolidsurfacecontact_penalty_law:

**PENALTY_LAW** | *default:* none |break| Type of penalty law

   **Possible values:**

   - none
   - linear
   - linear_quadratic

.. _beaminteraction_beamtosolidsurfacecontact_penalty_parameter_g0:

**PENALTY_PARAMETER_G0** | *default:* 0 |break| First penalty regularization parameter G0 >=0: For gap<G0 contact is active

.. _beaminteraction_beamtosolidsurfacecontact_geometry_pair_strategy:

**GEOMETRY_PAIR_STRATEGY** | *default:* segmentation |break| Type of employed segmentation strategy

   **Possible values:**

   - none
   - segmentation
   - gauss_point_projection_without_boundary_segmentation
   - gauss_point_projection_boundary_segmentation
   - gauss_point_projection_cross_section

.. _beaminteraction_beamtosolidsurfacecontact_geometry_pair_search_points:

**GEOMETRY_PAIR_SEARCH_POINTS** | *default:* 6 |break| Number of search points for segmentation

.. _beaminteraction_beamtosolidsurfacecontact_gauss_points:

**GAUSS_POINTS** | *default:* 6 |break| Number of Gauss Points for the integral evaluations

.. _beaminteraction_beamtosolidsurfacecontact_integration_points_circumference:

**INTEGRATION_POINTS_CIRCUMFERENCE** | *default:* 6 |break| Number of Integration points along the circumferencial direction of the beam. This is parameter is only used in beam to cylinder meshtying. No gauss integration is used along the circumferencial direction, equally spaced integration points are used.

.. _beaminteraction_beamtosolidsurfacecontact_geometry_pair_surface_normals:

**GEOMETRY_PAIR_SURFACE_NORMALS** | *default:* standard |break| How the surface normals are evaluated

   **Possible values:**

   - standard
   - extended_volume

.. _beaminteraction_beamtosolidsurfacecontact_mortar_shape_function:

**MORTAR_SHAPE_FUNCTION** | *default:* none |break| Shape function for the mortar Lagrange-multipliers

   **Possible values:**

   - none

.. _SECbeaminteraction_beamtosolidsurface:

BEAM INTERACTION/BEAM TO SOLID SURFACE
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   -----------------------------BEAM INTERACTION/BEAM TO SOLID SURFACE

.. _SECbeaminteraction_beamtosolidsurface_runtimevtkoutput:

BEAM INTERACTION/BEAM TO SOLID SURFACE/RUNTIME VTK OUTPUT
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ----------BEAM INTERACTION/BEAM TO SOLID SURFACE/RUNTIME VTK OUTPUT

.. _beaminteraction_beamtosolidsurface_runtimevtkoutput_write_output:

**WRITE_OUTPUT** | *default:* No |break| Enable / disable beam-to-solid volume mesh tying output.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beaminteraction_beamtosolidsurface_runtimevtkoutput_nodal_forces:

**NODAL_FORCES** | *default:* No |break| Enable / disable output of the resulting nodal forces due to beam to solid interaction.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beaminteraction_beamtosolidsurface_runtimevtkoutput_averaged_normals:

**AVERAGED_NORMALS** | *default:* No |break| Enable / disable output of averaged nodal normals on the surface.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beaminteraction_beamtosolidsurface_runtimevtkoutput_mortar_lambda_discret:

**MORTAR_LAMBDA_DISCRET** | *default:* No |break| Enable / disable output of the discrete Lagrange multipliers at the node of the Lagrange multiplier shape functions.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beaminteraction_beamtosolidsurface_runtimevtkoutput_mortar_lambda_continuous:

**MORTAR_LAMBDA_CONTINUOUS** | *default:* No |break| Enable / disable output of the continuous Lagrange multipliers function along the beam.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beaminteraction_beamtosolidsurface_runtimevtkoutput_mortar_lambda_continuous_segments:

**MORTAR_LAMBDA_CONTINUOUS_SEGMENTS** | *default:* 5 |break| Number of segments for continuous mortar output

.. _beaminteraction_beamtosolidsurface_runtimevtkoutput_segmentation:

**SEGMENTATION** | *default:* No |break| Enable / disable output of segmentation points.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beaminteraction_beamtosolidsurface_runtimevtkoutput_integration_points:

**INTEGRATION_POINTS** | *default:* No |break| Enable / disable output of used integration points. If the contact method has 'forces' at the integration point, they will also be output.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _beaminteraction_beamtosolidsurface_runtimevtkoutput_unique_ids:

**UNIQUE_IDS** | *default:* No |break| Enable / disable output of unique IDs (mainly for testing of created VTK files).

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECbrowniandynamics:

BROWNIAN DYNAMICS
-----------------

no description yet

::

   --------------------------------------------------BROWNIAN DYNAMICS

.. _browniandynamics_browndynprob:

**BROWNDYNPROB** | *default:* No |break| switch Brownian dynamics on/off

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _browniandynamics_viscosity:

**VISCOSITY** | *default:* 0 |break| viscosity

.. _browniandynamics_kt:

**KT** | *default:* 0 |break| thermal energy

.. _browniandynamics_maxrandforce:

**MAXRANDFORCE** | *default:* -1 |break| Any random force beyond MAXRANDFORCE*(standard dev.) will be omitted and redrawn. -1.0 means no bounds.'

.. _browniandynamics_timestep:

**TIMESTEP** | *default:* -1 |break| Within this time interval the random numbers remain constant. -1.0 

.. _browniandynamics_beams_damping_coeff_specified_via:

**BEAMS_DAMPING_COEFF_SPECIFIED_VIA** | *default:* cylinder_geometry_approx |break| In which way are damping coefficient values for beams specified?

   **Possible values:**

   - cylinder_geometry_approx
   - Cylinder_geometry_approx
   - input_file
   - Input_file

.. _browniandynamics_beams_damping_coeff_per_unitlength:

**BEAMS_DAMPING_COEFF_PER_UNITLENGTH** | *default:* 0.0 0.0 0.0 |break| values for beam damping coefficients (per unit length and NOT yet multiplied by fluid viscosity): translational perpendicular/parallel to beam axis, rotational around axis

.. _SECloca:

LOCA
----

no description yet

::

   ---------------------------------------------------------------LOCA

.. _SECloca_stepper:

LOCA/Stepper
~~~~~~~~~~~~

Sub-list used by LOCA::Stepper to set parameters relevant for continuation run.

::

   -------------------------------------------------------LOCA/Stepper

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _loca_stepper_continuationmethod:

**Continuation Method** | *default:* Arc Length |break| Type of continuation to use.

   **Possible values:**

   - Natural
   - Arc Length

.. _loca_stepper_continuationparameter:

**Continuation Parameter** | *default:* Unknown |break| Name of continuation parameter.

   **Possible values:**

   - Unknown

.. _loca_stepper_initialvalue:

**Initial Value** | *default:* -1e+12 |break| (must be supplied) Initial value of continuation parameter.

.. _loca_stepper_maxvalue:

**Max Value** | *default:* -1e+12 |break| (must be supplied) Maximum value of continuation parameter.

.. _loca_stepper_minvalue:

**Min Value** | *default:* -1e+12 |break| (must be supplied) Minimum value of continuation parameter.

.. _loca_stepper_maxsteps:

**Max Steps** | *default:* 100 |break| Maximum number of continuation steps (including failed step)

.. _loca_stepper_maxnonlineariterations:

**Max Nonlinear Iterations** | *default:* 15 |break| Maximum number of nonlinear iterations per continuation step

.. _loca_stepper_skipparameterderivative:

**Skip Parameter Derivative** | *default:* Yes |break| For natural continuation skip the df/dp computation which is usually unnecessary.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _loca_stepper_enablearclengthscaling:

**Enable Arc Length Scaling** | *default:* Yes |break| Enable arc-length scaling to equilibrate solution and parameter components to arc-length equations (see LOCA::MultiContinuation::ArcLengthGroup).

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _loca_stepper_goalarclengthparametercontribution:

**Goal Arc Length Parameter Contribution** | *default:* 0.5 |break| Goal for parameter contribution to arc-length equation.

.. _loca_stepper_maxarclengthparametercontribution:

**Max Arc Length Parameter Contribution** | *default:* 0.8 |break| Max for parameter contribution to arc-length equation, triggering rescaling.

.. _loca_stepper_initialscalefactor:

**Initial Scale Factor** | *default:* 1 |break| Initial scale factor for parameter term of arc-length equation.

.. _loca_stepper_minscalefactor:

**Min Scale Factor** | *default:* 0.001 |break| Minimum scale factor for scaling parameter term of arc-length equation

.. _loca_stepper_enabletangentfactorstepsizescaling:

**Enable Tangent Factor Step Size Scaling** | *default:* No |break| Enable step size scaling by cosine between two consecutive tangent vectors :math:`v_0` and :math:`v_1` to continuation curve :math:`|v_0 * v_1|^{\alpha}` where :math:`\alpha` is the tangent factor exponent.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _loca_stepper_mintangentfactor:

**Min Tangent Factor** | *default:* 0.1 |break| Minimum cosine between two consecutive tangent vectors, below which the continuation step is failed.

.. _loca_stepper_tangentfactorexponent:

**Tangent Factor Exponent** | *default:* 1 |break| Exponent on the cosine between two consecutive tangent vectors, which then modifies the step size.

.. _loca_stepper_borderedsolvermethod:

**Bordered Solver Method** | *default:* Householder |break| Method for solving bordered system of equations

   **Possible values:**

   - Bordering
   - Nested
   - Householder
   - Augmented

.. _loca_stepper_computeeigenvalues:

**Compute Eigenvalues** | *default:* No |break| Flag for requesting eigenvalue calculation after each continuation step.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECloca_stepper_eigensolver:

LOCA/Stepper/Eigensolver
~~~~~~~~~~~~~~~~~~~~~~~~

Sub-list used by LOCA::Eigensolver::Factory to determine eigensolver strategies.

::

   -------------------------------------------LOCA/Stepper/Eigensolver

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _loca_stepper_eigensolver_method:

**Method** | *default:* Default |break| no description yet

   **Possible values:**

   - Default
   - Anasazi

.. _loca_stepper_eigensolver_operator:

**Operator** | *default:* Jacobian Inverse |break| "Jacobian Inverse" - Eigenvalues of :math:`J^{-1}` | "Shift-Invert" - Eigenvalues of :math:`(J-\sigma M)^{-1}M` | "Cayley" - Eigenvalues of :math:`(J-\sigma M)^{-1}(J - \mu M)` 

   **Possible values:**

   - Jacobian Inverse
   - Shift-Invert
   - Cayley

.. _loca_stepper_eigensolver_shift:

**Shift** | *default:* 0 |break| :math:`\sigma` of "Shift-Invert".

.. _loca_stepper_eigensolver_cayleypole:

**Cayley Pole** | *default:* 0 |break| :math:`\sigma` of "Cayley".

.. _loca_stepper_eigensolver_cayleyzero:

**Cayley Zero** | *default:* 0 |break| :math:`\mu` of "Cayley".

.. _loca_stepper_eigensolver_blocksize:

**Block Size** | *default:* 1 |break| Block Size

.. _loca_stepper_eigensolver_numblocks:

**Num Blocks** | *default:* 30 |break| Maximum number of blocks (equals the maximum length of the Arnoldi factorization.

.. _loca_stepper_eigensolver_numeigenvalues:

**Num Eigenvalues** | *default:* 4 |break| Number of requested eigenvalues.

.. _loca_stepper_eigensolver_convergencetolerance:

**Convergence Tolerance** | *default:* 1e-07 |break| Tolerance for the converged eigenvalues.

.. _loca_stepper_eigensolver_stepsize:

**Step Size** | *default:* 1 |break| Checks convergence every so many steps.

.. _loca_stepper_eigensolver_maximumrestarts:

**Maximum Restarts** | *default:* 1 |break| Number of restarts allowed.

.. _loca_stepper_eigensolver_symmetric:

**Symmetric** | *default:* No |break| Is the operator symmetric?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _loca_stepper_eigensolver_normalizeeigenvectorswithmassmatrix:

**Normalize Eigenvectors with Mass Matrix** | *default:* No |break| Option to normalize :math:`v M v = 1` instead of :math:`v v =1`.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _loca_stepper_eigensolver_sortingorder:

**Sorting Order** | *default:* LM |break| "LM" - Largest magnitude | "LR" - Largest real component | "LI" - Largest Imaginary component | "SM" - Smallest magnitude | "SR" - Smallest real component | "SI" - Smallest imaginary component | "CA" - Largest real part of inverse Cayley transformation

   **Possible values:**

   - LM
   - Largest Magnitude
   - LR
   - Largest Real Component
   - LI
   - Largest Imaginary Component
   - SM
   - Smallest Magnitude
   - SR
   - Smallest Real Component
   - SI
   - Smallest Imaginary Component
   - CA
   - Cayley

.. _SECloca_bifurcation:

LOCA/Bifurcation
~~~~~~~~~~~~~~~~

Sub-list used by LOCA::Bifurcation::Factory to determine what type of bifurcation calculation, if any to use.

::

   ---------------------------------------------------LOCA/Bifurcation

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _loca_bifurcation_type:

**Type** | *default:* None |break| Bifurcation method to use.

   **Possible values:**

   - None
   - Turning Point
   - Pitchfork
   - Hopf
   - User Defined

.. _loca_bifurcation_formulation:

**Formulation** | *default:* Moore-Spence |break| Name of the bifurcation formulation.

   **Possible values:**

   - Moore-Spence
   - Minimally Augmented

.. _loca_bifurcation_bifurcationparameter:

**Bifurcation Parameter** | *default:* Unknown |break| (Must be supplied if "Type" is not "None") Name of bifurcation parameter.

   **Possible values:**

   - Unknown

.. _loca_bifurcation_perturbinitialsolution:

**Perturb Initial Solution** | *default:* No |break| Flag indicating whether to apply an initial perturbation to the initial guess for the solution vector before starting bifurcation algorithm.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _loca_bifurcation_relativeperturbationsize:

**Relative Perturbation Size** | *default:* 0.001 |break| Size of relative perturbation of initial guess for solution vector.

.. _loca_bifurcation_solvermethod:

**Solver Method** | *default:* Salinger Bordering |break| Solver method

   **Possible values:**

   - Salinger Bordering
   - Phipps Bordering

.. _SECloca_predictor:

LOCA/Predictor
~~~~~~~~~~~~~~

Sub-list used by LOCA::MultiPredictor::Factory to determine what type of predictor to use for each continuation step.

::

   -----------------------------------------------------LOCA/Predictor

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _loca_predictor_method:

**Method** | *default:* Secant |break| Predictor method to use for computing the initial guess for each continuation step.

   **Possible values:**

   - Constant
   - Secant
   - Tangent
   - Random

.. _loca_predictor_epsilon:

**Epsilon** | *default:* 0.001 |break| Relative size of perturbation for random predictor.

.. _SECloca_predictor_firststeppredictor:

LOCA/Predictor/First Step Predictor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sub-Sub-list used by the secant predictor to determine which predictor to use for the first continuation step.

::

   --------------------------------LOCA/Predictor/First Step Predictor

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _loca_predictor_firststeppredictor_method:

**Method** | *default:* Secant |break| Predictor method to use for computing the initial guess for each continuation step.

   **Possible values:**

   - Constant
   - Secant
   - Tangent
   - Random

.. _loca_predictor_firststeppredictor_epsilon:

**Epsilon** | *default:* 0.001 |break| Relative size of perturbation for random predictor.

.. _SECloca_predictor_laststeppredictor:

LOCA/Predictor/Last Step Predictor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sub-Sub-list used for last step of arc-length continuation to hit target (max or min) value exactly (usually "Constant" or "Random").

::

   ---------------------------------LOCA/Predictor/Last Step Predictor

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _loca_predictor_laststeppredictor_method:

**Method** | *default:* Secant |break| Predictor method to use for computing the initial guess for each continuation step.

   **Possible values:**

   - Constant
   - Secant
   - Tangent
   - Random

.. _loca_predictor_laststeppredictor_epsilon:

**Epsilon** | *default:* 0.001 |break| Relative size of perturbation for random predictor.

.. _SECloca_stepsize:

LOCA/Step Size
~~~~~~~~~~~~~~

Sub-list used by LOCA::StepSize::Factory to determine step size control strategies.

::

   -----------------------------------------------------LOCA/Step Size

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _loca_stepsize_method:

**Method** | *default:* Adaptive |break| Step size control strategy to use. "Constant" - Use a constant step size in general, reducing the step size after a failure and increasing step size back up to original value after subsequent successes (see LOCA::StepSize::Constant) | "Adaptive" - Use an adaptive step size control strategy that adjusts step size according to the number of Newton iterations per step (see LOCA::StepSize::Adaptive)

   **Possible values:**

   - Constant
   - Adaptive

.. _loca_stepsize_initialstepsize:

**Initial Step Size** | *default:* 1 |break| Initial parameter step size.

.. _loca_stepsize_minstepsize:

**Min Step Size** | *default:* 1e-12 |break| Minimum parameter step size.

.. _loca_stepsize_maxstepsize:

**Max Step Size** | *default:* 1e+12 |break| Maximum parameter step size.

.. _loca_stepsize_failedstepreductionfactor:

**Failed Step Reduction Factor** | *default:* 0.5 |break| Factor by which step size is reduced after a failed step.

.. _loca_stepsize_successfulstepincreasefactor:

**Successful Step Increase Factor** | *default:* 1.26 |break| Factor by which step size is increased after a successful step when the step size is smaller than the initial step size (Constant step size method only).

.. _loca_stepsize_aggressiveness:

**Aggressiveness** | *default:* 0.5 |break| Aggressiveness factor in adaptive step size adjustment.

.. _SECloca_constraints:

LOCA/Constraints
~~~~~~~~~~~~~~~~

Sub-list used to provide additional constraint equations.

::

   ---------------------------------------------------LOCA/Constraints

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _loca_constraints_borderedsolvermethod:

**Bordered Solver Method** | *default:* Householder |break| Method for solving bordered system of equations

   **Possible values:**

   - Bordering
   - Nested
   - Householder
   - Augmented

.. _SECsemi-smoothplasticity:

SEMI-SMOOTH PLASTICITY
----------------------

no description yet

::

   ---------------------------------------------SEMI-SMOOTH PLASTICITY

.. _semi-smoothplasticity_semi_smooth_cpl:

**SEMI_SMOOTH_CPL** | *default:* 1 |break| Weighting factor cpl for semi-smooth PDASS

.. _semi-smoothplasticity_stabilization_s:

**STABILIZATION_S** | *default:* 1 |break| Stabilization factor s for semi-smooth PDASS

.. _semi-smoothplasticity_normcombi_resfplastconstr:

**NORMCOMBI_RESFPLASTCONSTR** | *default:* And |break| binary operator to combine plasticity constraints and residual force values

   **Possible values:**

   - And
   - Or

.. _semi-smoothplasticity_normcombi_dispplastincr:

**NORMCOMBI_DISPPLASTINCR** | *default:* And |break| binary operator to combine displacement increments and plastic flow (Delta Lp) increment values

   **Possible values:**

   - And
   - Or

.. _semi-smoothplasticity_tolplastconstr:

**TOLPLASTCONSTR** | *default:* 1e-08 |break| tolerance in the plastic constraint norm for the newton iteration

.. _semi-smoothplasticity_toldeltalp:

**TOLDELTALP** | *default:* 1e-08 |break| tolerance in the plastic flow (Delta Lp) norm for the Newton iteration

.. _semi-smoothplasticity_normcombi_easres:

**NORMCOMBI_EASRES** | *default:* And |break| binary operator to combine EAS-residual and residual force values

   **Possible values:**

   - And
   - Or

.. _semi-smoothplasticity_normcombi_easincr:

**NORMCOMBI_EASINCR** | *default:* And |break| binary operator to combine displacement increments and EAS increment values

   **Possible values:**

   - And
   - Or

.. _semi-smoothplasticity_toleasres:

**TOLEASRES** | *default:* 1e-08 |break| tolerance in the EAS residual norm for the newton iteration

.. _semi-smoothplasticity_toleasincr:

**TOLEASINCR** | *default:* 1e-08 |break| tolerance in the EAS increment norm for the Newton iteration

.. _semi-smoothplasticity_dissipation_mode:

**DISSIPATION_MODE** | *default:* pl_multiplier |break| method to calculate the plastic dissipation

   **Possible values:**

   - pl_multiplier
   - pl_flow
   - Taylor_Quinney

.. _SECthermaldynamic:

THERMAL DYNAMIC
---------------

no description yet

::

   ----------------------------------------------------THERMAL DYNAMIC

.. _thermaldynamic_dynamictyp:

**DYNAMICTYP** | *default:* OneStepTheta |break| type of time integration control

   **Possible values:**

   - Statics
   - OneStepTheta
   - GenAlpha
   - ExplicitEuler

.. _thermaldynamic_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| save temperature and other global quantities every RESULTSEVRY steps

.. _thermaldynamic_resevryergy:

**RESEVRYERGY** | *default:* 0 |break| write system energies every requested step

.. _thermaldynamic_restartevry:

**RESTARTEVRY** | *default:* 1 |break| write restart possibility every RESTARTEVRY steps

.. _thermaldynamic_initialfield:

**INITIALFIELD** | *default:* zero_field |break| Initial Field for thermal problem

   **Possible values:**

   - zero_field
   - field_by_function
   - field_by_condition

.. _thermaldynamic_initfuncno:

**INITFUNCNO** | *default:* -1 |break| function number for thermal initial field

.. _thermaldynamic_timestep:

**TIMESTEP** | *default:* 0.05 |break| time step size

.. _thermaldynamic_numstep:

**NUMSTEP** | *default:* 200 |break| maximum number of steps

.. _thermaldynamic_maxtime:

**MAXTIME** | *default:* 5 |break| maximum time

.. _thermaldynamic_toltemp:

**TOLTEMP** | *default:* 1e-10 |break| tolerance in the temperature norm of the Newton iteration

.. _thermaldynamic_norm_temp:

**NORM_TEMP** | *default:* Abs |break| type of norm for temperature convergence check

   **Possible values:**

   - Abs
   - Rel
   - Mix

.. _thermaldynamic_tolres:

**TOLRES** | *default:* 1e-08 |break| tolerance in the residual norm for the Newton iteration

.. _thermaldynamic_norm_resf:

**NORM_RESF** | *default:* Abs |break| type of norm for residual convergence check

   **Possible values:**

   - Abs
   - Rel
   - Mix

.. _thermaldynamic_normcombi_resftemp:

**NORMCOMBI_RESFTEMP** | *default:* And |break| binary operator to combine temperature and residual force values

   **Possible values:**

   - And
   - Or

.. _thermaldynamic_maxiter:

**MAXITER** | *default:* 50 |break| maximum number of iterations allowed for Newton-Raphson iteration before failure

.. _thermaldynamic_miniter:

**MINITER** | *default:* 0 |break| minimum number of iterations to be done within Newton-Raphson loop

.. _thermaldynamic_iternorm:

**ITERNORM** | *default:* L2 |break| type of norm to be applied to residuals

   **Possible values:**

   - L1
   - L2
   - Rms
   - Inf

.. _thermaldynamic_divercont:

**DIVERCONT** | *default:* stop |break| What to do with time integration when Newton-Raphson iteration failed

   **Possible values:**

   - stop
   - continue
   - halve_step
   - repeat_step
   - repeat_simulation

.. _thermaldynamic_maxdivconrefinementlevel:

**MAXDIVCONREFINEMENTLEVEL** | *default:* 10 |break| number of times timestep is halved in case nonlinear solver diverges

.. _thermaldynamic_nlnsol:

**NLNSOL** | *default:* fullnewton |break| Nonlinear solution technique

   **Possible values:**

   - vague
   - fullnewton

.. _thermaldynamic_predict:

**PREDICT** | *default:* ConstTemp |break| Predictor of iterative solution techniques

   **Possible values:**

   - Vague
   - ConstTemp
   - ConstTempRate
   - TangTemp

.. _thermaldynamic_adaptconv:

**ADAPTCONV** | *default:* No |break| Switch on adaptive control of linear solver tolerance for nonlinear solution

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _thermaldynamic_adaptconv_better:

**ADAPTCONV_BETTER** | *default:* 0.1 |break| The linear solver shall be this much better than the current nonlinear residual in the nonlinear convergence limit

.. _thermaldynamic_lumpcapa:

**LUMPCAPA** | *default:* No |break| Lump the capacity matrix for explicit time integration

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _thermaldynamic_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| number of linear solver used for thermal problems

.. _thermaldynamic_geometry:

**GEOMETRY** | *default:* full |break| How the geometry is specified

   **Possible values:**

   - full
   - box
   - file

.. _thermaldynamic_calcerror:

**CALCERROR** | *default:* No |break| compute error compared to analytical solution

   **Possible values:**

   - No
   - byfunct

.. _thermaldynamic_calcerrorfuncno:

**CALCERRORFUNCNO** | *default:* -1 |break| Function for Error Calculation

.. _SECthermaldynamic_genalpha:

THERMAL DYNAMIC/GENALPHA
~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   -------------------------------------------THERMAL DYNAMIC/GENALPHA

.. _thermaldynamic_genalpha_genavg:

**GENAVG** | *default:* TrLike |break| mid-average type of internal forces

   **Possible values:**

   - Vague
   - ImrLike
   - TrLike

.. _thermaldynamic_genalpha_gamma:

**GAMMA** | *default:* 0.5 |break| Generalised-alpha factor in (0,1]

.. _thermaldynamic_genalpha_alpha_m:

**ALPHA_M** | *default:* 0.5 |break| Generalised-alpha factor in [0.5,1)

.. _thermaldynamic_genalpha_alpha_f:

**ALPHA_F** | *default:* 0.5 |break| Generalised-alpha factor in [0.5,1)

.. _thermaldynamic_genalpha_rho_inf:

**RHO_INF** | *default:* -1 |break| Generalised-alpha factor in [0,1]

.. _SECthermaldynamic_onesteptheta:

THERMAL DYNAMIC/ONESTEPTHETA
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ---------------------------------------THERMAL DYNAMIC/ONESTEPTHETA

.. _thermaldynamic_onesteptheta_theta:

**THETA** | *default:* 0.5 |break| One-step-theta factor in (0,1]

.. _SECtsidynamic:

TSI DYNAMIC
-----------

Thermo Structure Interaction
Dynamic section for TSI solver with various coupling methods

::

   --------------------------------------------------------TSI DYNAMIC

.. _tsidynamic_coupalgo:

**COUPALGO** | *default:* tsi_monolithic |break| Coupling strategies for TSI solvers

   **Possible values:**

   - tsi_oneway
   - tsi_sequstagg
   - tsi_iterstagg
   - tsi_iterstagg_aitken
   - tsi_iterstagg_aitkenirons
   - tsi_iterstagg_fixedrelax
   - tsi_monolithic

.. _tsidynamic_matchinggrid:

**MATCHINGGRID** | *default:* Yes |break| is matching grid

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _tsidynamic_tfsi_coupalgo:

**TFSI_COUPALGO** | *default:* tfsi |break| Coupling strategies for BACI-INCA coupling (TFSI)

   **Possible values:**

   - tfsi
   - fsi
   - conj_heat_transfer
   - no_inca_fsi

.. _tsidynamic_restartevry:

**RESTARTEVRY** | *default:* 1 |break| write restart possibility every RESTARTEVRY steps

.. _tsidynamic_numstep:

**NUMSTEP** | *default:* 200 |break| maximum number of Timesteps

.. _tsidynamic_maxtime:

**MAXTIME** | *default:* 1000 |break| total simulation time

.. _tsidynamic_timestep:

**TIMESTEP** | *default:* 0.05 |break| time step size dt

.. _tsidynamic_itemax:

**ITEMAX** | *default:* 10 |break| maximum number of iterations over fields

.. _tsidynamic_itemin:

**ITEMIN** | *default:* 1 |break| minimal number of iterations over fields

.. _tsidynamic_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| increment for writing solution

.. _tsidynamic_norm_inc:

**NORM_INC** | *default:* Abs |break| type of norm for convergence check of primary variables in TSI

   **Possible values:**

   - Abs
   - Rel
   - Mix

.. _SECtsidynamic_monolithic:

TSI DYNAMIC/MONOLITHIC
~~~~~~~~~~~~~~~~~~~~~~

Monolithic Thermo Structure Interaction
Dynamic section for monolithic TSI

::

   ---------------------------------------------TSI DYNAMIC/MONOLITHIC

.. _tsidynamic_monolithic_convtol:

**CONVTOL** | *default:* 1e-06 |break| tolerance for convergence check of TSI

.. _tsidynamic_monolithic_tolinc:

**TOLINC** | *default:* 1e-06 |break| tolerance for convergence check of TSI-increment in monolithic TSI

.. _tsidynamic_monolithic_norm_resf:

**NORM_RESF** | *default:* Abs |break| type of norm for residual convergence check

   **Possible values:**

   - Abs
   - Rel
   - Mix

.. _tsidynamic_monolithic_normcombi_resfinc:

**NORMCOMBI_RESFINC** | *default:* Coupl_And_Singl |break| binary operator to combine primary variables and residual force values

   **Possible values:**

   - And
   - Or
   - Coupl_Or_Singl
   - Coupl_And_Singl
   - And_Singl
   - Or_Singl

.. _tsidynamic_monolithic_iternorm:

**ITERNORM** | *default:* Rms |break| type of norm to be applied to residuals

   **Possible values:**

   - L1
   - L1_Scaled
   - L2
   - Rms
   - Inf

.. _tsidynamic_monolithic_nlnsol:

**NLNSOL** | *default:* fullnewton |break| Nonlinear solution technique

   **Possible values:**

   - fullnewton
   - ptc

.. _tsidynamic_monolithic_ptcdt:

**PTCDT** | *default:* 0.1 |break| pseudo time step for pseudo-transient continuation (PTC) stabilised Newton procedure

.. _tsidynamic_monolithic_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| number of linear solver used for monolithic TSI problems

.. _tsidynamic_monolithic_adaptconv:

**ADAPTCONV** | *default:* No |break| Switch on adaptive control of linear solver tolerance for nonlinear solution

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _tsidynamic_monolithic_adaptconv_better:

**ADAPTCONV_BETTER** | *default:* 0.1 |break| The linear solver shall be this much better than the current nonlinear residual in the nonlinear convergence limit

.. _tsidynamic_monolithic_infnormscaling:

**INFNORMSCALING** | *default:* yes |break| Scale blocks of matrix with row infnorm?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _tsidynamic_monolithic_merge_tsi_block_matrix:

**MERGE_TSI_BLOCK_MATRIX** | *default:* No |break| Merge TSI block matrix

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _tsidynamic_monolithic_calc_necking_tsi_values:

**CALC_NECKING_TSI_VALUES** | *default:* No |break| Calculate nodal values for evaluation and validation of necking

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _tsidynamic_monolithic_tsi_line_search:

**TSI_LINE_SEARCH** | *default:* none |break| line-search strategy

   **Possible values:**

   - none
   - structure
   - thermo
   - and
   - or

.. _SECtsidynamic_partitioned:

TSI DYNAMIC/PARTITIONED
~~~~~~~~~~~~~~~~~~~~~~~

Partitioned Thermo Structure Interaction
Dynamic section for partitioned TSI

::

   --------------------------------------------TSI DYNAMIC/PARTITIONED

.. _tsidynamic_partitioned_coupvariable:

**COUPVARIABLE** | *default:* Displacement |break| Coupling variable

   **Possible values:**

   - Displacement
   - Temperature

.. _tsidynamic_partitioned_maxomega:

**MAXOMEGA** | *default:* 0 |break| largest omega allowed for Aitken relaxation (0.0 means no constraint)

.. _tsidynamic_partitioned_fixedomega:

**FIXEDOMEGA** | *default:* 1 |break| fixed relaxation parameter

.. _tsidynamic_partitioned_convtol:

**CONVTOL** | *default:* 1e-06 |break| tolerance for convergence check of outer iteraiton within partitioned TSI

.. _SECtsicontact:

TSI CONTACT
-----------

no description yet

::

   --------------------------------------------------------TSI CONTACT

.. _tsicontact_heattransslave:

**HEATTRANSSLAVE** | *default:* 0 |break| Heat transfer parameter for slave side in thermal contact

.. _tsicontact_heattransmaster:

**HEATTRANSMASTER** | *default:* 0 |break| Heat transfer parameter for master side in thermal contact

.. _tsicontact_temp_damage:

**TEMP_DAMAGE** | *default:* 1e+12 |break| damage temperatue at contact interface: friction coefficient zero there

.. _tsicontact_temp_ref:

**TEMP_REF** | *default:* 0 |break| reference temperatue at contact interface: friction coefficient equals the given value

.. _tsicontact_nitsche_theta_tsi:

**NITSCHE_THETA_TSI** | *default:* 0 |break| +1: symmetric, 0: non-symmetric, -1: skew-symmetric

.. _tsicontact_nitsche_weighting_tsi:

**NITSCHE_WEIGHTING_TSI** | *default:* harmonic |break| how to weight consistency terms in Nitsche contact formulation

   **Possible values:**

   - slave
   - master
   - harmonic
   - physical

.. _tsicontact_nitsche_penalty_adaptive_tsi:

**NITSCHE_PENALTY_ADAPTIVE_TSI** | *default:* yes |break| adapt penalty parameter after each converged time step

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _tsicontact_penaltyparam_thermo:

**PENALTYPARAM_THERMO** | *default:* 0 |break| Penalty parameter for Nitsche solution strategy

.. _tsicontact_nitsche_method_tsi:

**NITSCHE_METHOD_TSI** | *default:* nitsche |break| how to treat thermal interface problem: strong substitution or Nitsche for general interface conditions

   **Possible values:**

   - nitsche
   - substitution

.. _SECfluiddynamic:

FLUID DYNAMIC
-------------

no description yet

::

   ------------------------------------------------------FLUID DYNAMIC

.. _fluiddynamic_physical_type:

**PHYSICAL_TYPE** | *default:* Incompressible |break| Physical Type

   **Possible values:**

   - Incompressible
   - Weakly_compressible
   - Weakly_compressible_stokes
   - Weakly_compressible_dens_mom
   - Weakly_compressible_stokes_dens_mom
   - Artificial_compressibility
   - Varying_density
   - Loma
   - Temp_dep_water
   - Boussinesq
   - Stokes
   - Oseen

.. _fluiddynamic_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| number of linear solver used for fluid dynamics

.. _fluiddynamic_simpler_solver:

**SIMPLER_SOLVER** | *default:* -1 |break| number of linear solver used for fluid dynamics (ONLY NECESSARY FOR BlockGaussSeidel solver block within fluid mehstying case any more!!!!)

.. _fluiddynamic_wss_type:

**WSS_TYPE** | *default:* Standard |break| which type of stresses and wss

   **Possible values:**

   - Standard
   - Aggregation
   - Mean

.. _fluiddynamic_wss_ml_agr_solver:

**WSS_ML_AGR_SOLVER** | *default:* -1 |break| Set ML-solver number for smoothing of residual-based calculated wallshearstress via plain aggregation.

.. _fluiddynamic_timeintegr:

**TIMEINTEGR** | *default:* One_Step_Theta |break| Time Integration Scheme

   **Possible values:**

   - Stationary
   - Np_Gen_Alpha
   - Af_Gen_Alpha
   - One_Step_Theta
   - BDF2

.. _fluiddynamic_ost_cont_press:

**OST_CONT_PRESS** | *default:* Cont_normal_Press_normal |break| One step theta option for time discretization of continuity eq. and pressure

   **Possible values:**

   - Cont_normal_Press_normal
   - Cont_impl_Press_normal
   - Cont_impl_Press_impl

.. _fluiddynamic_geometry:

**GEOMETRY** | *default:* full |break| How the geometry is specified

   **Possible values:**

   - full
   - box
   - file

.. _fluiddynamic_nonliniter:

**NONLINITER** | *default:* fixed_point_like |break| Nonlinear iteration scheme

   **Possible values:**

   - fixed_point_like
   - Newton

.. _fluiddynamic_predictor:

**PREDICTOR** | *default:* steady_state |break| Predictor for first guess in nonlinear iteration

   **Possible values:**

   - steady_state
   - zero_acceleration
   - constant_acceleration
   - constant_increment
   - explicit_second_order_midpoint
   - TangVel

.. _fluiddynamic_convcheck:

**CONVCHECK** | *default:* L_2_norm |break| norm for convergence check

   **Possible values:**

   - L_2_norm

.. _fluiddynamic_inconsistent_residual:

**INCONSISTENT_RESIDUAL** | *default:* No |break| do not evaluate residual after solution has converged (->faster)

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_initialfield:

**INITIALFIELD** | *default:* zero_field |break| Initial field for fluid problem

   **Possible values:**

   - zero_field
   - field_by_function
   - disturbed_field_from_function
   - FLAME_VORTEX_INTERACTION
   - BELTRAMI-FLOW
   - KIM-MOIN-FLOW
   - hit_comte_bellot_corrsin_initial_field
   - forced_hit_simple_algebraic_spectrum
   - forced_hit_numeric_spectrum
   - forced_hit_passive
   - channel_weakly_compressible

.. _fluiddynamic_oseenfieldfuncno:

**OSEENFIELDFUNCNO** | *default:* -1 |break| function number of Oseen advective field

.. _fluiddynamic_liftdrag:

**LIFTDRAG** | *default:* No |break| Calculate lift and drag forces along specified boundary

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_convform:

**CONVFORM** | *default:* convective |break| form of convective term

   **Possible values:**

   - convective
   - conservative

.. _fluiddynamic_nonlinearbc:

**NONLINEARBC** | *default:* no |break| Flag to activate check for potential nonlinear boundary conditions

   **Possible values:**

   - no
   - yes

.. _fluiddynamic_meshtying:

**MESHTYING** | *default:* no |break| Flag to (de)activate mesh tying algorithm

   **Possible values:**

   - no
   - Condensed_Smat
   - Condensed_Bmat
   - Condensed_Bmat_merged

.. _fluiddynamic_gridvel:

**GRIDVEL** | *default:* BE |break| scheme for determination of gridvelocity from displacements

   **Possible values:**

   - BE
   - BDF2
   - OST

.. _fluiddynamic_alldofcoupled:

**ALLDOFCOUPLED** | *default:* Yes |break| all dof (incl. pressure) are coupled

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_calcerror:

**CALCERROR** | *default:* no |break| Flag to (de)activate error calculations

   **Possible values:**

   - no
   - beltrami_flow
   - channel2D
   - gravitation
   - shear_flow
   - byfunct
   - beltrami_stat_stokes
   - beltrami_stat_navier_stokes
   - beltrami_instat_stokes
   - beltrami_instat_navier_stokes
   - kimmoin_stat_stokes
   - kimmoin_stat_navier_stokes
   - kimmoin_instat_stokes
   - kimmoin_instat_navier_stokes
   - fsi_fluid_pusher
   - channel_weakly_compressible

.. _fluiddynamic_calcerrorfuncno:

**CALCERRORFUNCNO** | *default:* -1 |break| Function for Error Calculation

.. _fluiddynamic_corrtermfuncno:

**CORRTERMFUNCNO** | *default:* -1 |break| Function for calculation of the correction term for the weakly compressible problem

.. _fluiddynamic_bodyforcefuncno:

**BODYFORCEFUNCNO** | *default:* -1 |break| Function for calculation of the body force for the weakly compressible problem

.. _fluiddynamic_stab_den_ref:

**STAB_DEN_REF** | *default:* 0 |break| Reference stabilization parameter for the density for the HDG weakly compressible formulation

.. _fluiddynamic_stab_mom_ref:

**STAB_MOM_REF** | *default:* 0 |break| Reference stabilization parameter for the momentum for the HDG weakly compressible formulation

.. _fluiddynamic_varviscfuncno:

**VARVISCFUNCNO** | *default:* -1 |break| Function for calculation of a variable viscosity for the weakly compressible problem

.. _fluiddynamic_pressavgbc:

**PRESSAVGBC** | *default:* no |break| Flag to (de)activate imposition of boundary condition for the considered element average pressure

   **Possible values:**

   - no
   - yes

.. _fluiddynamic_refmach:

**REFMACH** | *default:* 1 |break| Reference Mach number

.. _fluiddynamic_simpler:

**SIMPLER** | *default:* no |break| Switch on SIMPLE family of solvers, only works with block preconditioners like CheapSIMPLE!

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_adaptconv:

**ADAPTCONV** | *default:* yes |break| Switch on adaptive control of linear solver tolerance for nonlinear solution

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_adaptconv_better:

**ADAPTCONV_BETTER** | *default:* 0.1 |break| The linear solver shall be this much better than the current nonlinear residual in the nonlinear convergence limit

.. _fluiddynamic_infnormscaling:

**INFNORMSCALING** | *default:* no |break| Scale blocks of matrix with row infnorm?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_gmsh_output:

**GMSH_OUTPUT** | *default:* No |break| write output to gmsh files

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_compute_divu:

**COMPUTE_DIVU** | *default:* No |break| Compute divergence of velocity field at the element center

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_compute_ekin:

**COMPUTE_EKIN** | *default:* No |break| Compute kinetic energy at the end of each time step and write it to file.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_new_ost:

**NEW_OST** | *default:* No |break| Solve the Navier-Stokes equation with the new One Step Theta algorithm

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| Increment for writing solution

.. _fluiddynamic_restartevry:

**RESTARTEVRY** | *default:* 20 |break| Increment for writing restart

.. _fluiddynamic_numstep:

**NUMSTEP** | *default:* 1 |break| Total number of Timesteps

.. _fluiddynamic_steadystep:

**STEADYSTEP** | *default:* -1 |break| steady state check every step

.. _fluiddynamic_numstasteps:

**NUMSTASTEPS** | *default:* 0 |break| Number of Steps for Starting Scheme

.. _fluiddynamic_startfuncno:

**STARTFUNCNO** | *default:* -1 |break| Function for Initial Starting Field

.. _fluiddynamic_itemax:

**ITEMAX** | *default:* 10 |break| max. number of nonlin. iterations

.. _fluiddynamic_initstatitemax:

**INITSTATITEMAX** | *default:* 5 |break| max number of nonlinear iterations for initial stationary solution

.. _fluiddynamic_timestep:

**TIMESTEP** | *default:* 0.01 |break| Time increment dt

.. _fluiddynamic_maxtime:

**MAXTIME** | *default:* 1000 |break| Total simulation time

.. _fluiddynamic_alpha_m:

**ALPHA_M** | *default:* 1 |break| Time integration factor

.. _fluiddynamic_alpha_f:

**ALPHA_F** | *default:* 1 |break| Time integration factor

.. _fluiddynamic_gamma:

**GAMMA** | *default:* 1 |break| Time integration factor

.. _fluiddynamic_theta:

**THETA** | *default:* 0.66 |break| Time integration factor

.. _fluiddynamic_start_theta:

**START_THETA** | *default:* 1 |break| Time integration factor for starting scheme

.. _fluiddynamic_strong_redd_3d_coupling_type:

**STRONG_REDD_3D_COUPLING_TYPE** | *default:* no |break| Flag to (de)activate potential Strong 3D redD coupling

   **Possible values:**

   - no
   - yes

.. _fluiddynamic_velgrad_proj_solver:

**VELGRAD_PROJ_SOLVER** | *default:* -1 |break| Number of linear solver used for L2 projection

.. _fluiddynamic_velgrad_proj_method:

**VELGRAD_PROJ_METHOD** | *default:* none |break| Flag to (de)activate gradient reconstruction.

   **Possible values:**

   - none
   - superconvergent_patch_recovery
   - L2_projection

.. _fluiddynamic_off_proc_assembly:

**OFF_PROC_ASSEMBLY** | *default:* No |break| Do not evaluate ghosted elements but communicate them --> faster if element call is expensive

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECfluiddynamic_nonlinearsolvertolerances:

FLUID DYNAMIC/NONLINEAR SOLVER TOLERANCES
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   --------------------------FLUID DYNAMIC/NONLINEAR SOLVER TOLERANCES

.. _fluiddynamic_nonlinearsolvertolerances_tol_vel_res:

**TOL_VEL_RES** | *default:* 1e-06 |break| Tolerance for convergence check of velocity residual

.. _fluiddynamic_nonlinearsolvertolerances_tol_vel_inc:

**TOL_VEL_INC** | *default:* 1e-06 |break| Tolerance for convergence check of velocity increment

.. _fluiddynamic_nonlinearsolvertolerances_tol_pres_res:

**TOL_PRES_RES** | *default:* 1e-06 |break| Tolerance for convergence check of pressure residual

.. _fluiddynamic_nonlinearsolvertolerances_tol_pres_inc:

**TOL_PRES_INC** | *default:* 1e-06 |break| Tolerance for convergence check of pressure increment

.. _SECfluiddynamic_residual-basedstabilization:

FLUID DYNAMIC/RESIDUAL-BASED STABILIZATION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   -------------------------FLUID DYNAMIC/RESIDUAL-BASED STABILIZATION

.. _fluiddynamic_residual-basedstabilization_stabtype:

**STABTYPE** | *default:* residual_based |break| Apply (un)stabilized fluid formulation

   **Possible values:**

   - no_stabilization
   - residual_based
   - edge_based
   - pressure_projection

.. _fluiddynamic_residual-basedstabilization_inconsistent:

**INCONSISTENT** | *default:* No |break| residual based without second derivatives (i.e. only consistent for tau->0, but faster)

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_residual-basedstabilization_reconstruct_sec_der:

**Reconstruct_Sec_Der** | *default:* No |break| residual computed with a reconstruction of the second derivatives via projection or superconvergent patch recovery

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_residual-basedstabilization_tds:

**TDS** | *default:* quasistatic |break| Flag to allow time dependency of subscales for residual-based stabilization.

   **Possible values:**

   - quasistatic
   - time_dependent

.. _fluiddynamic_residual-basedstabilization_transient:

**TRANSIENT** | *default:* no_transient |break| Specify how to treat the transient term.

   **Possible values:**

   - no_transient
   - yes_transient
   - transient_complete

.. _fluiddynamic_residual-basedstabilization_pspg:

**PSPG** | *default:* Yes |break| Flag to (de)activate PSPG stabilization.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_residual-basedstabilization_supg:

**SUPG** | *default:* Yes |break| Flag to (de)activate SUPG stabilization.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_residual-basedstabilization_grad_div:

**GRAD_DIV** | *default:* Yes |break| Flag to (de)activate grad-div term.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_residual-basedstabilization_vstab:

**VSTAB** | *default:* no_vstab |break| Flag to (de)activate viscous term in residual-based stabilization.

   **Possible values:**

   - no_vstab
   - vstab_gls
   - vstab_gls_rhs
   - vstab_usfem
   - vstab_usfem_rhs

.. _fluiddynamic_residual-basedstabilization_rstab:

**RSTAB** | *default:* no_rstab |break| Flag to (de)activate reactive term in residual-based stabilization.

   **Possible values:**

   - no_rstab
   - rstab_gls
   - rstab_usfem

.. _fluiddynamic_residual-basedstabilization_cross-stress:

**CROSS-STRESS** | *default:* no_cross |break| Flag to (de)activate cross-stress term -> residual-based VMM.

   **Possible values:**

   - no_cross
   - yes_cross
   - cross_rhs

.. _fluiddynamic_residual-basedstabilization_reynolds-stress:

**REYNOLDS-STRESS** | *default:* no_reynolds |break| Flag to (de)activate Reynolds-stress term -> residual-based VMM.

   **Possible values:**

   - no_reynolds
   - yes_reynolds
   - reynolds_rhs

.. _fluiddynamic_residual-basedstabilization_definition_tau:

**DEFINITION_TAU** | *default:* Franca_Barrenechea_Valentin_Frey_Wall |break| Definition of tau_M and Tau_C

   **Possible values:**

   - Taylor_Hughes_Zarins
   - Taylor_Hughes_Zarins_wo_dt
   - Taylor_Hughes_Zarins_Whiting_Jansen
   - Taylor_Hughes_Zarins_Whiting_Jansen_wo_dt
   - Taylor_Hughes_Zarins_scaled
   - Taylor_Hughes_Zarins_scaled_wo_dt
   - Franca_Barrenechea_Valentin_Frey_Wall
   - Franca_Barrenechea_Valentin_Frey_Wall_wo_dt
   - Shakib_Hughes_Codina
   - Shakib_Hughes_Codina_wo_dt
   - Codina
   - Codina_wo_dt
   - Codina_convscaled
   - Franca_Madureira_Valentin_Badia_Codina
   - Franca_Madureira_Valentin_Badia_Codina_wo_dt
   - Hughes_Franca_Balestra_wo_dt

.. _fluiddynamic_residual-basedstabilization_charelelength_u:

**CHARELELENGTH_U** | *default:* streamlength |break| Characteristic element length for tau_Mu

   **Possible values:**

   - streamlength
   - volume_equivalent_diameter
   - root_of_volume

.. _fluiddynamic_residual-basedstabilization_charelelength_pc:

**CHARELELENGTH_PC** | *default:* volume_equivalent_diameter |break| Characteristic element length for tau_Mp/tau_C

   **Possible values:**

   - streamlength
   - volume_equivalent_diameter
   - root_of_volume

.. _fluiddynamic_residual-basedstabilization_evaluation_tau:

**EVALUATION_TAU** | *default:* element_center |break| Location where tau is evaluated

   **Possible values:**

   - element_center
   - integration_point

.. _fluiddynamic_residual-basedstabilization_evaluation_mat:

**EVALUATION_MAT** | *default:* element_center |break| Location where material law is evaluated

   **Possible values:**

   - element_center
   - integration_point

.. _fluiddynamic_residual-basedstabilization_loma_conti_supg:

**LOMA_CONTI_SUPG** | *default:* No |break| Flag to (de)activate SUPG stabilization in loma continuity equation.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_residual-basedstabilization_loma_conti_cross_stress:

**LOMA_CONTI_CROSS_STRESS** | *default:* no_cross |break| Flag to (de)activate cross-stress term loma continuity equation-> residual-based VMM.

   **Possible values:**

   - no_cross
   - yes_cross
   - cross_rhs

.. _fluiddynamic_residual-basedstabilization_loma_conti_reynolds_stress:

**LOMA_CONTI_REYNOLDS_STRESS** | *default:* no_reynolds |break| Flag to (de)activate Reynolds-stress term loma continuity equation-> residual-based VMM.

   **Possible values:**

   - no_reynolds
   - yes_reynolds
   - reynolds_rhs

.. _SECfluiddynamic_edge-basedstabilization:

FLUID DYNAMIC/EDGE-BASED STABILIZATION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   -----------------------------FLUID DYNAMIC/EDGE-BASED STABILIZATION

.. _fluiddynamic_edge-basedstabilization_eos_pres:

**EOS_PRES** | *default:* none |break| Flag to (de)activate pressure edge-based stabilization.

   **Possible values:**

   - none
   - std_eos
   - xfem_gp

.. _fluiddynamic_edge-basedstabilization_eos_conv_stream:

**EOS_CONV_STREAM** | *default:* none |break| Flag to (de)activate convective streamline edge-based stabilization.

   **Possible values:**

   - none
   - std_eos
   - xfem_gp

.. _fluiddynamic_edge-basedstabilization_eos_conv_cross:

**EOS_CONV_CROSS** | *default:* none |break| Flag to (de)activate convective crosswind edge-based stabilization.

   **Possible values:**

   - none
   - std_eos
   - xfem_gp

.. _fluiddynamic_edge-basedstabilization_eos_div:

**EOS_DIV** | *default:* none |break| Flag to (de)activate divergence edge-based stabilization.

   **Possible values:**

   - none
   - vel_jump_std_eos
   - vel_jump_xfem_gp
   - div_jump_std_eos
   - div_jump_xfem_gp

.. _fluiddynamic_edge-basedstabilization_pres_krylov_2dz:

**PRES_KRYLOV_2Dz** | *default:* No |break| residual based without second derivatives (i.e. only consistent for tau->0, but faster)

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_edge-basedstabilization_eos_definition_tau:

**EOS_DEFINITION_TAU** | *default:* Burman_Hansbo_DAngelo_Zunino |break| Definition of stabilization parameter for edge-based stabilization

   **Possible values:**

   - Burman_Fernandez_Hansbo
   - Burman_Fernandez_Hansbo_wo_dt
   - Braack_Burman_John_Lube
   - Braack_Burman_John_Lube_wo_divjump
   - Franca_Barrenechea_Valentin_Wall
   - Burman_Fernandez
   - Burman_Hansbo_DAngelo_Zunino
   - Burman_Hansbo_DAngelo_Zunino_wo_dt
   - Schott_Massing_Burman_DAngelo_Zunino
   - Schott_Massing_Burman_DAngelo_Zunino_wo_dt
   - Burman
   - Taylor_Hughes_Zarins_Whiting_Jansen_Codina_scaling
   - tau_not_defined

.. _fluiddynamic_edge-basedstabilization_eos_h_definition:

**EOS_H_DEFINITION** | *default:* EOS_he_max_diameter_to_opp_surf |break| Definition of element length for edge-based stabilization

   **Possible values:**

   - EOS_he_max_diameter_to_opp_surf
   - EOS_he_max_dist_to_opp_surf
   - EOS_he_surf_with_max_diameter
   - EOS_hk_max_diameter
   - EOS_he_surf_diameter
   - EOS_he_vol_eq_diameter

.. _SECfluiddynamic_porous-flowstabilization:

FLUID DYNAMIC/POROUS-FLOW STABILIZATION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ----------------------------FLUID DYNAMIC/POROUS-FLOW STABILIZATION

.. _fluiddynamic_porous-flowstabilization_stab_biot:

**STAB_BIOT** | *default:* No |break| Flag to (de)activate BIOT stabilization.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_porous-flowstabilization_stab_biot_scaling:

**STAB_BIOT_SCALING** | *default:* 1 |break| Scaling factor for stabilization parameter for biot stabilization of porous flow.

.. _fluiddynamic_porous-flowstabilization_stabtype:

**STABTYPE** | *default:* residual_based |break| Apply (un)stabilized fluid formulation

   **Possible values:**

   - no_stabilization
   - residual_based
   - edge_based

.. _fluiddynamic_porous-flowstabilization_inconsistent:

**INCONSISTENT** | *default:* No |break| residual based without second derivatives (i.e. only consistent for tau->0, but faster)

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_porous-flowstabilization_reconstruct_sec_der:

**Reconstruct_Sec_Der** | *default:* No |break| residual computed with a reconstruction of the second derivatives via projection or superconvergent patch recovery

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_porous-flowstabilization_tds:

**TDS** | *default:* quasistatic |break| Flag to allow time dependency of subscales for residual-based stabilization.

   **Possible values:**

   - quasistatic
   - time_dependent

.. _fluiddynamic_porous-flowstabilization_transient:

**TRANSIENT** | *default:* no_transient |break| Specify how to treat the transient term.

   **Possible values:**

   - no_transient
   - yes_transient
   - transient_complete

.. _fluiddynamic_porous-flowstabilization_pspg:

**PSPG** | *default:* Yes |break| Flag to (de)activate PSPG stabilization.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_porous-flowstabilization_supg:

**SUPG** | *default:* Yes |break| Flag to (de)activate SUPG stabilization.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_porous-flowstabilization_grad_div:

**GRAD_DIV** | *default:* Yes |break| Flag to (de)activate grad-div term.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_porous-flowstabilization_vstab:

**VSTAB** | *default:* no_vstab |break| Flag to (de)activate viscous term in residual-based stabilization.

   **Possible values:**

   - no_vstab
   - vstab_gls
   - vstab_gls_rhs
   - vstab_usfem
   - vstab_usfem_rhs

.. _fluiddynamic_porous-flowstabilization_rstab:

**RSTAB** | *default:* no_rstab |break| Flag to (de)activate reactive term in residual-based stabilization.

   **Possible values:**

   - no_rstab
   - rstab_gls
   - rstab_usfem

.. _fluiddynamic_porous-flowstabilization_cross-stress:

**CROSS-STRESS** | *default:* no_cross |break| Flag to (de)activate cross-stress term -> residual-based VMM.

   **Possible values:**

   - no_cross
   - yes_cross
   - cross_rhs

.. _fluiddynamic_porous-flowstabilization_reynolds-stress:

**REYNOLDS-STRESS** | *default:* no_reynolds |break| Flag to (de)activate Reynolds-stress term -> residual-based VMM.

   **Possible values:**

   - no_reynolds
   - yes_reynolds
   - reynolds_rhs

.. _fluiddynamic_porous-flowstabilization_definition_tau:

**DEFINITION_TAU** | *default:* Franca_Barrenechea_Valentin_Frey_Wall |break| Definition of tau_M and Tau_C

   **Possible values:**

   - Taylor_Hughes_Zarins
   - Taylor_Hughes_Zarins_wo_dt
   - Taylor_Hughes_Zarins_Whiting_Jansen
   - Taylor_Hughes_Zarins_Whiting_Jansen_wo_dt
   - Taylor_Hughes_Zarins_scaled
   - Taylor_Hughes_Zarins_scaled_wo_dt
   - Franca_Barrenechea_Valentin_Frey_Wall
   - Franca_Barrenechea_Valentin_Frey_Wall_wo_dt
   - Shakib_Hughes_Codina
   - Shakib_Hughes_Codina_wo_dt
   - Codina
   - Codina_wo_dt
   - Franca_Madureira_Valentin_Badia_Codina
   - Franca_Madureira_Valentin_Badia_Codina_wo_dt

.. _fluiddynamic_porous-flowstabilization_charelelength_u:

**CHARELELENGTH_U** | *default:* streamlength |break| Characteristic element length for tau_Mu

   **Possible values:**

   - streamlength
   - volume_equivalent_diameter
   - root_of_volume

.. _fluiddynamic_porous-flowstabilization_charelelength_pc:

**CHARELELENGTH_PC** | *default:* volume_equivalent_diameter |break| Characteristic element length for tau_Mp/tau_C

   **Possible values:**

   - streamlength
   - volume_equivalent_diameter
   - root_of_volume

.. _fluiddynamic_porous-flowstabilization_evaluation_tau:

**EVALUATION_TAU** | *default:* element_center |break| Location where tau is evaluated

   **Possible values:**

   - element_center
   - integration_point

.. _fluiddynamic_porous-flowstabilization_evaluation_mat:

**EVALUATION_MAT** | *default:* element_center |break| Location where material law is evaluated

   **Possible values:**

   - element_center
   - integration_point

.. _fluiddynamic_porous-flowstabilization_loma_conti_supg:

**LOMA_CONTI_SUPG** | *default:* No |break| Flag to (de)activate SUPG stabilization in loma continuity equation.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_porous-flowstabilization_loma_conti_cross_stress:

**LOMA_CONTI_CROSS_STRESS** | *default:* no_cross |break| Flag to (de)activate cross-stress term loma continuity equation-> residual-based VMM.

   **Possible values:**

   - no_cross
   - yes_cross
   - cross_rhs

.. _fluiddynamic_porous-flowstabilization_loma_conti_reynolds_stress:

**LOMA_CONTI_REYNOLDS_STRESS** | *default:* no_reynolds |break| Flag to (de)activate Reynolds-stress term loma continuity equation-> residual-based VMM.

   **Possible values:**

   - no_reynolds
   - yes_reynolds
   - reynolds_rhs

.. _SECfluiddynamic_turbulencemodel:

FLUID DYNAMIC/TURBULENCE MODEL
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   -------------------------------------FLUID DYNAMIC/TURBULENCE MODEL

.. _fluiddynamic_turbulencemodel_turbulence_approach:

**TURBULENCE_APPROACH** | *default:* DNS_OR_RESVMM_LES |break| There are several options to deal with turbulent flows.

   **Possible values:**

   - DNS_OR_RESVMM_LES
   - CLASSICAL_LES

.. _fluiddynamic_turbulencemodel_physical_model:

**PHYSICAL_MODEL** | *default:* no_model |break| Classical LES approaches require an additional model for
the turbulent viscosity.

   **Possible values:**

   - no_model
   - Smagorinsky
   - Smagorinsky_with_van_Driest_damping
   - Dynamic_Smagorinsky
   - Multifractal_Subgrid_Scales
   - Vreman
   - Dynamic_Vreman

.. _fluiddynamic_turbulencemodel_fssugrvisc:

**FSSUGRVISC** | *default:* No |break| fine-scale subgrid viscosity

   **Possible values:**

   - No
   - Smagorinsky_all
   - Smagorinsky_small

.. _fluiddynamic_turbulencemodel_sampling_start:

**SAMPLING_START** | *default:* 10000000 |break| Time step after when sampling shall be started

.. _fluiddynamic_turbulencemodel_sampling_stop:

**SAMPLING_STOP** | *default:* 1 |break| Time step when sampling shall be stopped

.. _fluiddynamic_turbulencemodel_dumping_period:

**DUMPING_PERIOD** | *default:* 1 |break| Period of time steps after which statistical data shall be dumped

.. _fluiddynamic_turbulencemodel_subgrid_dissipation:

**SUBGRID_DISSIPATION** | *default:* No |break| Flag to (de)activate estimation of subgrid-scale dissipation (only for seclected flows).

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_turbulencemodel_outmean:

**OUTMEAN** | *default:* No |break| Flag to (de)activate averaged paraview output

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_turbulencemodel_turbmodel_ls:

**TURBMODEL_LS** | *default:* Yes |break| Flag to (de)activate turbulence model in level-set equation

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_turbulencemodel_canonical_flow:

**CANONICAL_FLOW** | *default:* no |break| Sampling is different for different canonical flows 
--- so specify what kind of flow you've got

   **Possible values:**

   - no
   - time_averaging
   - channel_flow_of_height_2
   - lid_driven_cavity
   - backward_facing_step
   - square_cylinder
   - square_cylinder_nurbs
   - rotating_circular_cylinder_nurbs
   - rotating_circular_cylinder_nurbs_scatra
   - loma_channel_flow_of_height_2
   - loma_lid_driven_cavity
   - loma_backward_facing_step
   - combust_oracles
   - bubbly_channel_flow
   - scatra_channel_flow_of_height_2
   - decaying_homogeneous_isotropic_turbulence
   - forced_homogeneous_isotropic_turbulence
   - scatra_forced_homogeneous_isotropic_turbulence
   - taylor_green_vortex
   - periodic_hill
   - blood_fda_flow
   - backward_facing_step2

.. _fluiddynamic_turbulencemodel_homdir:

**HOMDIR** | *default:* not_specified |break| Specify the homogenous direction(s) of a flow

   **Possible values:**

   - not_specified
   - x
   - y
   - z
   - xy
   - xz
   - yz
   - xyz

.. _fluiddynamic_turbulencemodel_chan_ampl_init_dist:

**CHAN_AMPL_INIT_DIST** | *default:* 0.1 |break| Max. amplitude of the random disturbance in percent of the initial value in mean flow direction.

.. _fluiddynamic_turbulencemodel_forcing_type:

**FORCING_TYPE** | *default:* linear_compensation_from_intermediate_spectrum |break| forcing strategy

   **Possible values:**

   - linear_compensation_from_intermediate_spectrum
   - fixed_power_input

.. _fluiddynamic_turbulencemodel_cha_numsubdivisions:

**CHA_NUMSUBDIVISIONS** | *default:* 5 |break| Number of homogenious sampling planes in element

.. _fluiddynamic_turbulencemodel_forcing_time_steps:

**FORCING_TIME_STEPS** | *default:* 0 |break| Number of time steps during which forcing is applied. Decaying homogeneous isotropic turbulence only.

.. _fluiddynamic_turbulencemodel_threshold_wavenumber:

**THRESHOLD_WAVENUMBER** | *default:* 0 |break| Forcing is only applied to wave numbers lower or equal than the given threshold wave number.

.. _fluiddynamic_turbulencemodel_power_input:

**POWER_INPUT** | *default:* 0 |break| power of forcing

.. _fluiddynamic_turbulencemodel_scalar_forcing:

**SCALAR_FORCING** | *default:* no |break| Define forcing for scalar field.

   **Possible values:**

   - no
   - isotropic
   - mean_scalar_gradient

.. _fluiddynamic_turbulencemodel_mean_scalar_gradient:

**MEAN_SCALAR_GRADIENT** | *default:* 0 |break| Value of imposed mean-scalar gradient to force scalar field.

.. _fluiddynamic_turbulencemodel_exclude_xfem:

**EXCLUDE_XFEM** | *default:* No |break| Flag to (de)activate XFEM dofs in calculation of fine-scale velocity.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECfluiddynamic_subgridviscosity:

FLUID DYNAMIC/SUBGRID VISCOSITY
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ------------------------------------FLUID DYNAMIC/SUBGRID VISCOSITY

.. _fluiddynamic_subgridviscosity_c_smagorinsky:

**C_SMAGORINSKY** | *default:* 0 |break| Constant for the Smagorinsky model. Something between 0.1 to 0.24. Vreman constant if the constant vreman model is applied (something between 0.07 and 0.01).

.. _fluiddynamic_subgridviscosity_c_yoshizawa:

**C_YOSHIZAWA** | *default:* -1 |break| Constant for the compressible Smagorinsky model: isotropic part of subgrid-stress tensor. About 0.09 or 0.0066. Ci will not be squared!

.. _fluiddynamic_subgridviscosity_c_smagorinsky_averaged:

**C_SMAGORINSKY_AVERAGED** | *default:* No |break| Flag to (de)activate averaged Smagorinksy constant

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_subgridviscosity_c_include_ci:

**C_INCLUDE_CI** | *default:* No |break| Flag to (de)inclusion of Yoshizawa model

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_subgridviscosity_channel_l_tau:

**CHANNEL_L_TAU** | *default:* 0 |break| Used for normalisation of the wall normal distance in the Van 
Driest Damping function. May be taken from the output of 
the apply_mesh_stretching.pl preprocessing script.

.. _fluiddynamic_subgridviscosity_c_turbprandtl:

**C_TURBPRANDTL** | *default:* 1 |break| (Constant) turbulent Prandtl number for the Smagorinsky model in scalar transport.

.. _fluiddynamic_subgridviscosity_filter_width:

**FILTER_WIDTH** | *default:* CubeRootVol |break| The Vreman model requires a filter width.

   **Possible values:**

   - CubeRootVol
   - Direction_dependent
   - Minimum_length

.. _SECfluiddynamic_wallmodel:

FLUID DYNAMIC/WALL MODEL
~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   -------------------------------------------FLUID DYNAMIC/WALL MODEL

.. _fluiddynamic_wallmodel_x_wall:

**X_WALL** | *default:* No |break| Flag to switch on the xwall model

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_wallmodel_tauw_type:

**Tauw_Type** | *default:* constant |break| Methods for calculating/updating the wall shear stress necessary for Spalding's law.

   **Possible values:**

   - constant
   - between_steps

.. _fluiddynamic_wallmodel_tauw_calc_type:

**Tauw_Calc_Type** | *default:* residual |break| Methods for calculating the wall shear stress necessary for Spalding's law.

   **Possible values:**

   - residual
   - gradient
   - gradient_to_residual

.. _fluiddynamic_wallmodel_switch_step:

**Switch_Step** | *default:* -1 |break| Switch from gradient to residual based tauw.

.. _fluiddynamic_wallmodel_projection:

**Projection** | *default:* No |break| Flag to switch projection of the enriched dofs after updating tauw, alternatively with or without continuity constraint.

   **Possible values:**

   - No
   - onlyl2projection
   - l2projectionwithcontinuityconstraint

.. _fluiddynamic_wallmodel_c_tauw:

**C_Tauw** | *default:* 1 |break| Constant wall shear stress for Spalding's law, if applicable

.. _fluiddynamic_wallmodel_min_tauw:

**Min_Tauw** | *default:* 2e-09 |break| Minimum wall shear stress preventing system to become singular

.. _fluiddynamic_wallmodel_inc_tauw:

**Inc_Tauw** | *default:* 1 |break| Increment of Tauw of full step, between 0.0 and 1.0

.. _fluiddynamic_wallmodel_blending_type:

**Blending_Type** | *default:* none |break| Methods for blending the enrichment space.

   **Possible values:**

   - none
   - ramp_function

.. _fluiddynamic_wallmodel_gp_wall_normal:

**GP_Wall_Normal** | *default:* 3 |break| Gauss points in wall normal direction

.. _fluiddynamic_wallmodel_gp_wall_normal_off_wall:

**GP_Wall_Normal_Off_Wall** | *default:* 3 |break| Gauss points in wall normal direction, off-wall elements

.. _fluiddynamic_wallmodel_gp_wall_parallel:

**GP_Wall_Parallel** | *default:* 3 |break| Gauss points in wall parallel direction

.. _fluiddynamic_wallmodel_treat_tauw_on_dirichlet_inflow:

**Treat_Tauw_on_Dirichlet_Inflow** | *default:* No |break| Flag to treat residual on Dirichlet inflow nodes for calculation of wall shear stress

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_wallmodel_projection_solver:

**PROJECTION_SOLVER** | *default:* -1 |break| Set solver number for l2-projection.

.. _SECfluiddynamic_multifractalsubgridscales:

FLUID DYNAMIC/MULTIFRACTAL SUBGRID SCALES
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   --------------------------FLUID DYNAMIC/MULTIFRACTAL SUBGRID SCALES

.. _fluiddynamic_multifractalsubgridscales_csgs:

**CSGS** | *default:* 0 |break| Modelparameter of multifractal subgrid-scales.

.. _fluiddynamic_multifractalsubgridscales_scale_separation:

**SCALE_SEPARATION** | *default:* no_scale_sep |break| Specify the filter type for scale separation in LES

   **Possible values:**

   - no_scale_sep
   - box_filter
   - algebraic_multigrid_operator

.. _fluiddynamic_multifractalsubgridscales_ml_solver:

**ML_SOLVER** | *default:* -1 |break| Set solver number for scale separation via level set transfer operators from plain aggregation.

.. _fluiddynamic_multifractalsubgridscales_calc_n:

**CALC_N** | *default:* No |break| Flag to (de)activate calculation of N from the Reynolds number.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_multifractalsubgridscales_n:

**N** | *default:* 1 |break| Set grid to viscous scale ratio.

.. _fluiddynamic_multifractalsubgridscales_ref_length:

**REF_LENGTH** | *default:* cube_edge |break| Specify the reference length for Re-dependent N.

   **Possible values:**

   - cube_edge
   - sphere_diameter
   - streamlength
   - gradient_based
   - metric_tensor

.. _fluiddynamic_multifractalsubgridscales_ref_velocity:

**REF_VELOCITY** | *default:* strainrate |break| Specify the reference velocity for Re-dependent N.

   **Possible values:**

   - strainrate
   - resolved
   - fine_scale

.. _fluiddynamic_multifractalsubgridscales_c_nu:

**C_NU** | *default:* 1 |break| Proportionality constant between Re and ratio viscous scale to element length.

.. _fluiddynamic_multifractalsubgridscales_near_wall_limit:

**NEAR_WALL_LIMIT** | *default:* No |break| Flag to (de)activate near-wall limit.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_multifractalsubgridscales_evaluation_b:

**EVALUATION_B** | *default:* element_center |break| Location where B is evaluated

   **Possible values:**

   - element_center
   - integration_point

.. _fluiddynamic_multifractalsubgridscales_beta:

**BETA** | *default:* 0 |break| Cross- and Reynolds-stress terms only on right-hand-side.

.. _fluiddynamic_multifractalsubgridscales_convform:

**CONVFORM** | *default:* convective |break| form of convective term

   **Possible values:**

   - convective
   - conservative

.. _fluiddynamic_multifractalsubgridscales_csgs_phi:

**CSGS_PHI** | *default:* 0 |break| Modelparameter of multifractal subgrid-scales for scalar transport.

.. _fluiddynamic_multifractalsubgridscales_adapt_csgs_phi:

**ADAPT_CSGS_PHI** | *default:* No |break| Flag to (de)activate adaption of CsgsD to CsgsB.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_multifractalsubgridscales_near_wall_limit_csgs_phi:

**NEAR_WALL_LIMIT_CSGS_PHI** | *default:* No |break| Flag to (de)activate near-wall limit for scalar field.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_multifractalsubgridscales_consistent_fluid_residual:

**CONSISTENT_FLUID_RESIDUAL** | *default:* No |break| Flag to (de)activate the consistency term for residual-based stabilization.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_multifractalsubgridscales_c_diff:

**C_DIFF** | *default:* 1 |break| Proportionality constant between Re*Pr and ratio dissipative scale to element length. Usually equal cnu.

.. _fluiddynamic_multifractalsubgridscales_set_fine_scale_vel:

**SET_FINE_SCALE_VEL** | *default:* No |break| Flag to set fine-scale velocity for parallel nightly tests.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_multifractalsubgridscales_loma_conti:

**LOMA_CONTI** | *default:* No |break| Flag to (de)activate cross- and Reynolds-stress terms in loma continuity equation.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECfluiddynamic_turbulentinflow:

FLUID DYNAMIC/TURBULENT INFLOW
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   -------------------------------------FLUID DYNAMIC/TURBULENT INFLOW

.. _fluiddynamic_turbulentinflow_turbulentinflow:

**TURBULENTINFLOW** | *default:* No |break| Flag to (de)activate potential separate turbulent inflow section

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluiddynamic_turbulentinflow_initialinflowfield:

**INITIALINFLOWFIELD** | *default:* zero_field |break| Initial field for inflow section

   **Possible values:**

   - zero_field
   - field_by_function
   - disturbed_field_from_function

.. _fluiddynamic_turbulentinflow_inflowfunc:

**INFLOWFUNC** | *default:* -1 |break| Function number for initial flow field in inflow section

.. _fluiddynamic_turbulentinflow_inflow_init_dist:

**INFLOW_INIT_DIST** | *default:* 0.1 |break| Max. amplitude of the random disturbance in percent of the initial value in mean flow direction.

.. _fluiddynamic_turbulentinflow_numinflowstep:

**NUMINFLOWSTEP** | *default:* 1 |break| Total number of time steps for development of turbulent flow

.. _fluiddynamic_turbulentinflow_canonical_inflow:

**CANONICAL_INFLOW** | *default:* no |break| Sampling is different for different canonical flows 
--- so specify what kind of flow you've got

   **Possible values:**

   - no
   - time_averaging
   - channel_flow_of_height_2
   - loma_channel_flow_of_height_2
   - scatra_channel_flow_of_height_2

.. _fluiddynamic_turbulentinflow_inflow_cha_side:

**INFLOW_CHA_SIDE** | *default:* 0 |break| Most right side of inflow channel. Necessary to define sampling domain.

.. _fluiddynamic_turbulentinflow_inflow_homdir:

**INFLOW_HOMDIR** | *default:* not_specified |break| Specify the homogenous direction(s) of a flow

   **Possible values:**

   - not_specified
   - x
   - y
   - z
   - xy
   - xz
   - yz

.. _fluiddynamic_turbulentinflow_inflow_sampling_start:

**INFLOW_SAMPLING_START** | *default:* 10000000 |break| Time step after when sampling shall be started

.. _fluiddynamic_turbulentinflow_inflow_sampling_stop:

**INFLOW_SAMPLING_STOP** | *default:* 1 |break| Time step when sampling shall be stopped

.. _fluiddynamic_turbulentinflow_inflow_dumping_period:

**INFLOW_DUMPING_PERIOD** | *default:* 1 |break| Period of time steps after which statistical data shall be dumped

.. _SECfluiddynamic_timeadaptivity:

FLUID DYNAMIC/TIMEADAPTIVITY
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ---------------------------------------FLUID DYNAMIC/TIMEADAPTIVITY

.. _fluiddynamic_timeadaptivity_adaptive_time_step_estimator:

**ADAPTIVE_TIME_STEP_ESTIMATOR** | *default:* none |break| Method used to determine adaptive time step size.

   **Possible values:**

   - none
   - cfl_number
   - only_print_cfl_number

.. _fluiddynamic_timeadaptivity_cfl_number:

**CFL_NUMBER** | *default:* -1 |break| CFL number for adaptive time step

.. _fluiddynamic_timeadaptivity_freeze_adaptive_dt_at:

**FREEZE_ADAPTIVE_DT_AT** | *default:* 1000000 |break| keep time step constant after this step, otherwise turbulence statistics sampling is not consistent

.. _fluiddynamic_timeadaptivity_adaptive_dt_inc:

**ADAPTIVE_DT_INC** | *default:* 0.8 |break| Increment of whole step for adaptive dt via CFL

.. _SECtwophaseflow:

TWO PHASE FLOW
--------------

no description yet

::

   -----------------------------------------------------TWO PHASE FLOW

.. _twophaseflow_numstep:

**NUMSTEP** | *default:* 10 |break| Number of Time Steps

.. _twophaseflow_timestep:

**TIMESTEP** | *default:* 0.01 |break| Time increment dt

.. _twophaseflow_maxtime:

**MAXTIME** | *default:* 0 |break| Total simulation time

.. _twophaseflow_convtol:

**CONVTOL** | *default:* 1e-06 |break| Tolerance for convergence check

.. _twophaseflow_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| Increment for writing solution

.. _twophaseflow_restartevry:

**RESTARTEVRY** | *default:* 1 |break| Increment for writing restart

.. _twophaseflow_itemax:

**ITEMAX** | *default:* 1 |break| Maximum number of iterations in levelset-fluid loop

.. _twophaseflow_write_center_of_mass:

**WRITE_CENTER_OF_MASS** | *default:* No |break| Write center of mass to file

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _twophaseflow_restart_scatra_input:

**RESTART_SCATRA_INPUT** | *default:* No |break| Use ScaTra field from .dat-file instead

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECtwophaseflow_smeared:

TWO PHASE FLOW/SMEARED
~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ---------------------------------------------TWO PHASE FLOW/SMEARED

.. _twophaseflow_smeared_interface_thickness:

**INTERFACE_THICKNESS** | *default:* 0 |break| Thickness of interface for multiphase flow

.. _twophaseflow_smeared_enhanced_gaussrule:

**ENHANCED_GAUSSRULE** | *default:* No |break| Set higher order gaussrule within the interface layer.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECtwophaseflow_surfacetension:

TWO PHASE FLOW/SURFACE TENSION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   -------------------------------------TWO PHASE FLOW/SURFACE TENSION

.. _twophaseflow_surfacetension_surftensapprox:

**SURFTENSAPPROX** | *default:* surface_tension_approx_none |break| Type of surface tension approximation

   **Possible values:**

   - surface_tension_approx_none
   - surface_tension_approx_fixed_curvature
   - surface_tension_approx_divgrad_normal
   - surface_tension_approx_nodal_curvature
   - surface_tension_approx_laplacebeltrami

.. _twophaseflow_surfacetension_smoothgradphi:

**SMOOTHGRADPHI** | *default:* smooth_grad_phi_l2_projection |break| Type of smoothing for grad(phi)

   **Possible values:**

   - smooth_grad_phi_meanvalue
   - smooth_grad_phi_l2_projection
   - smooth_grad_phi_superconvergent_patch_recovery_3D
   - smooth_grad_phi_superconvergent_patch_recovery_2Dz

.. _twophaseflow_surfacetension_l2_projection_second_derivatives:

**L2_PROJECTION_SECOND_DERIVATIVES** | *default:* No |break| L2 Projection Second Derivatives of Level Set

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _twophaseflow_surfacetension_scale_smoothed_gradients:

**SCALE_SMOOTHED_GRADIENTS** | *default:* No |break| Scale the smoothed normal to unit length

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _twophaseflow_surfacetension_nodal_curvature:

**NODAL_CURVATURE** | *default:* l2_projected |break| Type of calculation of nodal curvature value

   **Possible values:**

   - l2_projected
   - averaged

.. _twophaseflow_surfacetension_laplace_beltrami:

**LAPLACE_BELTRAMI** | *default:* matrix_mixed_smoothed |break| Type of calculation of Laplace-Beltrami projection matrix

   **Possible values:**

   - matrix_non_smoothed
   - matrix_smoothed
   - matrix_mixed_smoothed

.. _twophaseflow_surfacetension_smoothing_parameter:

**SMOOTHING_PARAMETER** | *default:* 0 |break| Diffusion Coefficient for Smoothing

.. _SEClomacontrol:

LOMA CONTROL
------------

control parameters for low-Mach-number flow problems


::

   -------------------------------------------------------LOMA CONTROL

.. _lomacontrol_monolithic:

**MONOLITHIC** | *default:* no |break| monolithic solver

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _lomacontrol_numstep:

**NUMSTEP** | *default:* 24 |break| Total number of time steps

.. _lomacontrol_timestep:

**TIMESTEP** | *default:* 0.1 |break| Time increment dt

.. _lomacontrol_maxtime:

**MAXTIME** | *default:* 1000 |break| Total simulation time

.. _lomacontrol_itemax:

**ITEMAX** | *default:* 10 |break| Maximum number of outer iterations

.. _lomacontrol_itemax_before_sampling:

**ITEMAX_BEFORE_SAMPLING** | *default:* 1 |break| Maximum number of outer iterations before sampling (for turbulent flows only)

.. _lomacontrol_convtol:

**CONVTOL** | *default:* 1e-06 |break| Tolerance for convergence check

.. _lomacontrol_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| Increment for writing solution

.. _lomacontrol_restartevry:

**RESTARTEVRY** | *default:* 1 |break| Increment for writing restart

.. _lomacontrol_consthermpress:

**CONSTHERMPRESS** | *default:* Yes |break| treatment of thermodynamic pressure in time

   **Possible values:**

   - No_energy
   - No_mass
   - Yes

.. _lomacontrol_sgs_material_update:

**SGS_MATERIAL_UPDATE** | *default:* no |break| update material by adding subgrid-scale scalar field

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _lomacontrol_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| number of linear solver used for LOMA problem

.. _SECcutgeneral:

CUT GENERAL
-----------

no description yet

::

   --------------------------------------------------------CUT GENERAL

.. _cutgeneral_kernel_intersection_floattype:

**KERNEL_INTERSECTION_FLOATTYPE** | *default:* double |break| The floattype of the cut surface-edge intersection

   **Possible values:**

   - cln
   - double

.. _cutgeneral_kernel_distance_floattype:

**KERNEL_DISTANCE_FLOATTYPE** | *default:* double |break| The floattype of the cut distance computation

   **Possible values:**

   - cln
   - double

.. _cutgeneral_general_positon_distance_floattype:

**GENERAL_POSITON_DISTANCE_FLOATTYPE** | *default:* none |break| A general floattype for GEO::CUT::Position for Embedded Elements (ComputeDistance)

   **Possible values:**

   - none
   - cln
   - double

.. _cutgeneral_general_positon_position_floattype:

**GENERAL_POSITON_POSITION_FLOATTYPE** | *default:* none |break| A general floattype for GEO::CUT::Position Elements (ComputePosition)

   **Possible values:**

   - none
   - cln
   - double

.. _cutgeneral_direct_divergence_refplane:

**DIRECT_DIVERGENCE_REFPLANE** | *default:* all |break| Specifiy which Referenceplanes are used in DirectDivergence

   **Possible values:**

   - all
   - diagonal_side
   - facet
   - diagonal
   - side
   - none

.. _cutgeneral_split_cutsides:

**SPLIT_CUTSIDES** | *default:* Yes |break| Split Quad4 CutSides into Tri3-Subtriangles?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _cutgeneral_do_selfcut:

**DO_SELFCUT** | *default:* Yes |break| Do the SelfCut?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _cutgeneral_selfcut_do_meshcorrection:

**SELFCUT_DO_MESHCORRECTION** | *default:* Yes |break| Do meshcorrection in the SelfCut?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _cutgeneral_selfcut_meshcorrection_multiplicator:

**SELFCUT_MESHCORRECTION_MULTIPLICATOR** | *default:* 30 |break| ISLANDS with maximal size of the bounding box of h*multiplacator will be removed in the meshcorrection

.. _cutgeneral_boundarycell_cubaturdegree:

**BOUNDARYCELL_CUBATURDEGREE** | *default:* 20 |break| Cubaturedegree utilized for the numerical integration on the CUT BoundaryCells.

.. _SECxfemgeneral:

XFEM GENERAL
------------

no description yet

::

   -------------------------------------------------------XFEM GENERAL

.. _xfemgeneral_gmsh_debug_out:

**GMSH_DEBUG_OUT** | *default:* No |break| Do you want to write extended Gmsh output for each timestep?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _xfemgeneral_gmsh_debug_out_screen:

**GMSH_DEBUG_OUT_SCREEN** | *default:* No |break| Do you want to be informed, if Gmsh output is written?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _xfemgeneral_gmsh_sol_out:

**GMSH_SOL_OUT** | *default:* No |break| Do you want to write extended Gmsh output for each timestep?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _xfemgeneral_gmsh_timint_out:

**GMSH_TIMINT_OUT** | *default:* No |break| Do you want to write extended Gmsh output for each timestep?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _xfemgeneral_gmsh_eos_out:

**GMSH_EOS_OUT** | *default:* No |break| Do you want to write extended Gmsh output for each timestep?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _xfemgeneral_gmsh_discret_out:

**GMSH_DISCRET_OUT** | *default:* No |break| Do you want to write extended Gmsh output for each timestep?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _xfemgeneral_gmsh_cut_out:

**GMSH_CUT_OUT** | *default:* No |break| Do you want to write extended Gmsh output for each timestep?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _xfemgeneral_max_num_dofsets:

**MAX_NUM_DOFSETS** | *default:* 3 |break| Maximum number of volumecells in the XFEM element

.. _xfemgeneral_nodal_dofset_strategy:

**NODAL_DOFSET_STRATEGY** | *default:* full |break| Strategy used for the nodal dofset management per node

   **Possible values:**

   - OneDofset_PerNodeAndPosition
   - ConnectGhostDofsets_PerNodeAndPosition
   - full

.. _xfemgeneral_volume_gauss_points_by:

**VOLUME_GAUSS_POINTS_BY** | *default:* Tessellation |break| Method for finding Gauss Points for the cut volumes

   **Possible values:**

   - Tessellation
   - MomentFitting
   - DirectDivergence

.. _xfemgeneral_boundary_gauss_points_by:

**BOUNDARY_GAUSS_POINTS_BY** | *default:* Tessellation |break| Method for finding Gauss Points for the boundary cells

   **Possible values:**

   - Tessellation
   - MomentFitting
   - DirectDivergence

.. _SECxfluiddynamic:

XFLUID DYNAMIC
--------------

no description yet

::

   -----------------------------------------------------XFLUID DYNAMIC

.. _SECxfluiddynamic_general:

XFLUID DYNAMIC/GENERAL
~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ---------------------------------------------XFLUID DYNAMIC/GENERAL

.. _xfluiddynamic_general_xfluidfluid:

**XFLUIDFLUID** | *default:* no |break| Use an embedded fluid patch.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _xfluiddynamic_general_relaxing_ale_every:

**RELAXING_ALE_EVERY** | *default:* 1 |break| Relaxing Ale after how many monolithic steps

.. _xfluiddynamic_general_relaxing_ale:

**RELAXING_ALE** | *default:* yes |break| switch on/off for relaxing Ale in monolithic fluid-fluid-fsi

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _xfluiddynamic_general_xfluidfluid_searchradius:

**XFLUIDFLUID_SEARCHRADIUS** | *default:* 1 |break| Radius of the search tree

.. _xfluiddynamic_general_monolithic_xffsi_approach:

**MONOLITHIC_XFFSI_APPROACH** | *default:* xffsi_fixedALE_partitioned |break| The monolithic approach for xfluidfluid-fsi

   **Possible values:**

   - xffsi_full_newton
   - xffsi_fixedALE_interpolation
   - xffsi_fixedALE_partitioned

.. _xfluiddynamic_general_xfluidfluid_timeint:

**XFLUIDFLUID_TIMEINT** | *default:* Xff_TimeInt_FullProj |break| The xfluidfluid-timeintegration approach

   **Possible values:**

   - Xff_TimeInt_FullProj
   - Xff_TimeInt_ProjIfMoved
   - Xff_TimeInt_KeepGhostValues
   - Xff_TimeInt_IncompProj

.. _xfluiddynamic_general_xfluid_timeint:

**XFLUID_TIMEINT** | *default:* STD=COPY/SL_and_GHOST=COPY/GP |break| The xfluid time integration approach

   **Possible values:**

   - STD=COPY_and_GHOST=COPY/GP
   - STD=COPY/SL_and_GHOST=COPY/GP
   - STD=SL(boundary-zone)_and_GHOST=GP
   - STD=COPY/PROJ_and_GHOST=COPY/PROJ/GP

.. _xfluiddynamic_general_ale_xfluid:

**ALE_XFluid** | *default:* no |break| XFluid is Ale Fluid?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _xfluiddynamic_general_interface_terms_previous_state:

**INTERFACE_TERMS_PREVIOUS_STATE** | *default:* PreviousState_only_consistency |break| how to treat interface terms from previous time step (new OST)

   **Possible values:**

   - PreviousState_only_consistency
   - PreviousState_full

.. _xfluiddynamic_general_xfluid_timeint_check_interfacetips:

**XFLUID_TIMEINT_CHECK_INTERFACETIPS** | *default:* Yes |break| Xfluid TimeIntegration Special Check if node has changed the side!

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _xfluiddynamic_general_xfluid_timeint_check_slidingonsurface:

**XFLUID_TIMEINT_CHECK_SLIDINGONSURFACE** | *default:* Yes |break| Xfluid TimeIntegration Special Check if node is sliding on surface!

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECxfluiddynamic_stabilization:

XFLUID DYNAMIC/STABILIZATION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ---------------------------------------XFLUID DYNAMIC/STABILIZATION

.. _xfluiddynamic_stabilization_coupling_method:

**COUPLING_METHOD** | *default:* Nitsche |break| method how to enforce embedded boundary/coupling conditions at the interface

   **Possible values:**

   - Hybrid_LM_Cauchy_stress
   - Hybrid_LM_viscous_stress
   - Nitsche

.. _xfluiddynamic_stabilization_hybrid_lm_l2_proj:

**HYBRID_LM_L2_PROJ** | *default:* part_ele_proj |break| perform the L2 projection between stress fields on whole element or on fluid part?

   **Possible values:**

   - full_ele_proj
   - part_ele_proj

.. _xfluiddynamic_stabilization_visc_adjoint_symmetry:

**VISC_ADJOINT_SYMMETRY** | *default:* yes |break| viscous and adjoint viscous interface terms with matching sign?

   **Possible values:**

   - yes
   - no
   - sym
   - skew
   - none

.. _xfluiddynamic_stabilization_nit_stab_fac:

**NIT_STAB_FAC** | *default:* 35 |break|  ( stabilization parameter for Nitsche's penalty term

.. _xfluiddynamic_stabilization_nit_stab_fac_tang:

**NIT_STAB_FAC_TANG** | *default:* 35 |break|  ( stabilization parameter for Nitsche's penalty tangential term

.. _xfluiddynamic_stabilization_visc_stab_trace_estimate:

**VISC_STAB_TRACE_ESTIMATE** | *default:* CT_div_by_hk |break| how to estimate the scaling from the trace inequality in Nitsche's method

   **Possible values:**

   - CT_div_by_hk
   - eigenvalue

.. _xfluiddynamic_stabilization_update_eigenvalue_trace_estimate:

**UPDATE_EIGENVALUE_TRACE_ESTIMATE** | *default:* every_iter |break| how often should the local eigenvalue problem be updated

   **Possible values:**

   - every_iter
   - every_timestep
   - once

.. _xfluiddynamic_stabilization_visc_stab_hk:

**VISC_STAB_HK** | *default:* ele_vol_div_by_max_ele_surf |break| how to define the characteristic element length in cut elements

   **Possible values:**

   - vol_equivalent
   - cut_vol_div_by_cut_surf
   - ele_vol_div_by_cut_surf
   - ele_vol_div_by_ele_surf
   - ele_vol_div_by_max_ele_surf

.. _xfluiddynamic_stabilization_conv_stab_scaling:

**CONV_STAB_SCALING** | *default:* none |break| scaling factor for viscous interface stabilization (Nitsche, MSH)

   **Possible values:**

   - inflow
   - abs_inflow
   - none

.. _xfluiddynamic_stabilization_xff_conv_stab_scaling:

**XFF_CONV_STAB_SCALING** | *default:* none |break| scaling factor for convective interface stabilization of fluid-fluid Coupling

   **Possible values:**

   - inflow
   - averaged
   - none

.. _xfluiddynamic_stabilization_mass_conservation_combo:

**MASS_CONSERVATION_COMBO** | *default:* max |break| choose the maximum from viscous and convective contributions or just sum both up

   **Possible values:**

   - max
   - sum

.. _xfluiddynamic_stabilization_mass_conservation_scaling:

**MASS_CONSERVATION_SCALING** | *default:* only_visc |break| apply additional scaling of penalty term to enforce mass conservation for convection-dominated flow

   **Possible values:**

   - full
   - only_visc

.. _xfluiddynamic_stabilization_ghost_penalty_stab:

**GHOST_PENALTY_STAB** | *default:* no |break| switch on/off ghost penalty interface stabilization

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _xfluiddynamic_stabilization_ghost_penalty_transient_stab:

**GHOST_PENALTY_TRANSIENT_STAB** | *default:* no |break| switch on/off ghost penalty transient interface stabilization

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _xfluiddynamic_stabilization_ghost_penalty_2nd_stab:

**GHOST_PENALTY_2nd_STAB** | *default:* no |break| switch on/off ghost penalty interface stabilization for 2nd order derivatives

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _xfluiddynamic_stabilization_ghost_penalty_2nd_stab_normal:

**GHOST_PENALTY_2nd_STAB_NORMAL** | *default:* no |break| switch between ghost penalty interface stabilization for 2nd order derivatives in normal or all spatial directions

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _xfluiddynamic_stabilization_ghost_penalty_fac:

**GHOST_PENALTY_FAC** | *default:* 0.1 |break| define stabilization parameter ghost penalty interface stabilization

.. _xfluiddynamic_stabilization_ghost_penalty_transient_fac:

**GHOST_PENALTY_TRANSIENT_FAC** | *default:* 0.001 |break| define stabilization parameter ghost penalty transient interface stabilization

.. _xfluiddynamic_stabilization_ghost_penalty_2nd_fac:

**GHOST_PENALTY_2nd_FAC** | *default:* 0.05 |break| define stabilization parameter ghost penalty 2nd order viscous interface stabilization

.. _xfluiddynamic_stabilization_ghost_penalty_pressure_2nd_fac:

**GHOST_PENALTY_PRESSURE_2nd_FAC** | *default:* 0.05 |break| define stabilization parameter ghost penalty 2nd order pressure interface stabilization

.. _xfluiddynamic_stabilization_xff_eos_pres_emb_layer:

**XFF_EOS_PRES_EMB_LAYER** | *default:* no |break| switch on/off edge-based pressure stabilization on interface-contributing elements of the embedded fluid

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _xfluiddynamic_stabilization_is_pseudo_2d:

**IS_PSEUDO_2D** | *default:* no |break| modify viscous interface stabilization due to the vanishing polynomial in third dimension when using strong Dirichlet conditions to block polynomials in one spatial dimension

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _xfluiddynamic_stabilization_ghost_penalty_add_inner_faces:

**GHOST_PENALTY_ADD_INNER_FACES** | *default:* no |break| Apply ghost penalty stabilization also for inner faces if this is possible due to the dofsets

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECxfluiddynamic_xfpsimonolithic:

XFLUID DYNAMIC/XFPSI MONOLITHIC
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ------------------------------------XFLUID DYNAMIC/XFPSI MONOLITHIC

.. _xfluiddynamic_xfpsimonolithic_itemin:

**ITEMIN** | *default:* 1 |break| How many iterations are performed minimal

.. _xfluiddynamic_xfpsimonolithic_itemax_outer:

**ITEMAX_OUTER** | *default:* 5 |break| How many outer iterations are performed maximal

.. _xfluiddynamic_xfpsimonolithic_nd_newton_damping:

**ND_NEWTON_DAMPING** | *default:* no |break| Activate Newton damping based on residual and increment

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _xfluiddynamic_xfpsimonolithic_nd_max_disp_iterinc:

**ND_MAX_DISP_ITERINC** | *default:* -1 |break| Maximal displacement increment to apply full newton --> otherwise damp newton

.. _xfluiddynamic_xfpsimonolithic_nd_max_vel_iterinc:

**ND_MAX_VEL_ITERINC** | *default:* -1 |break| Maximal fluid velocity increment to apply full newton --> otherwise damp newton

.. _xfluiddynamic_xfpsimonolithic_nd_max_pres_iterinc:

**ND_MAX_PRES_ITERINC** | *default:* -1 |break| Maximal fluid pressure increment to apply full newton --> otherwise damp newton

.. _xfluiddynamic_xfpsimonolithic_nd_max_pvel_iterinc:

**ND_MAX_PVEL_ITERINC** | *default:* -1 |break| Maximal porofluid velocity increment to apply full newton --> otherwise damp newton

.. _xfluiddynamic_xfpsimonolithic_nd_max_ppres_iterinc:

**ND_MAX_PPRES_ITERINC** | *default:* -1 |break| Maximal porofluid pressure increment to apply full newton --> otherwise damp newton

.. _xfluiddynamic_xfpsimonolithic_cut_evaluate_mintol:

**CUT_EVALUATE_MINTOL** | *default:* 0 |break| Minimal value of the maximal strucutral displacement for which the CUT is evaluate in this iteration!

.. _xfluiddynamic_xfpsimonolithic_cut_evaluate_miniter:

**CUT_EVALUATE_MINITER** | *default:* 0 |break| Minimal number of nonlinear iterations, before the CUT is potentially not evaluated

.. _xfluiddynamic_xfpsimonolithic_extrapolate_to_zero:

**EXTRAPOLATE_TO_ZERO** | *default:* no |break| the extrapolation of the fluid stress in the contact zone is relaxed to zero after a certain distance

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _xfluiddynamic_xfpsimonolithic_porocontactfpsi_hfraction:

**POROCONTACTFPSI_HFRACTION** | *default:* 1 |break| factor of element size, when transition between FPSI and PSCI is started!

.. _xfluiddynamic_xfpsimonolithic_porocontactfpsi_fullpcfraction:

**POROCONTACTFPSI_FULLPCFRACTION** | *default:* 0 |break| ration of gap/(POROCONTACTFPSI_HFRACTION*h) when full PSCI is started!

.. _xfluiddynamic_xfpsimonolithic_use_poro_pressure:

**USE_PORO_PRESSURE** | *default:* yes |break| the extrapolation of the fluid stress in the contact zone is relaxed to zero after a certtain distance

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SEClubricationdynamic:

LUBRICATION DYNAMIC
-------------------

control parameters for Lubrication problems


::

   ------------------------------------------------LUBRICATION DYNAMIC

.. _lubricationdynamic_maxtime:

**MAXTIME** | *default:* 1000 |break| Total simulation time

.. _lubricationdynamic_numstep:

**NUMSTEP** | *default:* 20 |break| Total number of time steps

.. _lubricationdynamic_timestep:

**TIMESTEP** | *default:* 0.1 |break| Time increment dt

.. _lubricationdynamic_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| Increment for writing solution

.. _lubricationdynamic_restartevry:

**RESTARTEVRY** | *default:* 1 |break| Increment for writing restart

.. _lubricationdynamic_calcerror:

**CALCERROR** | *default:* No |break| compute error compared to analytical solution

   **Possible values:**

   - No
   - error_by_function

.. _lubricationdynamic_calcerrorno:

**CALCERRORNO** | *default:* -1 |break| function number for lubrication error computation

.. _lubricationdynamic_velocityfield:

**VELOCITYFIELD** | *default:* zero |break| type of velocity field used for lubrication problems

   **Possible values:**

   - zero
   - function
   - EHL

.. _lubricationdynamic_velfuncno:

**VELFUNCNO** | *default:* -1 |break| function number for lubrication velocity field

.. _lubricationdynamic_heightfeild:

**HEIGHTFEILD** | *default:* zero |break| type of height field used for lubrication problems

   **Possible values:**

   - zero
   - function
   - EHL

.. _lubricationdynamic_hfuncno:

**HFUNCNO** | *default:* -1 |break| function number for lubrication height field

.. _lubricationdynamic_outmean:

**OUTMEAN** | *default:* No |break| Output of mean values for scalars and density

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _lubricationdynamic_output_gmsh:

**OUTPUT_GMSH** | *default:* No |break| Do you want to write Gmsh postprocessing files?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _lubricationdynamic_matlab_state_output:

**MATLAB_STATE_OUTPUT** | *default:* No |break| Do you want to write the state solution to Matlab file?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _lubricationdynamic_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| number of linear solver used for the Lubrication problem

.. _lubricationdynamic_itemax:

**ITEMAX** | *default:* 10 |break| max. number of nonlin. iterations

.. _lubricationdynamic_abstolres:

**ABSTOLRES** | *default:* 1e-14 |break| Absolute tolerance for deciding if residual of nonlinear problem is already zero

.. _lubricationdynamic_convtol:

**CONVTOL** | *default:* 1e-13 |break| Tolerance for convergence check

.. _lubricationdynamic_adaptconv:

**ADAPTCONV** | *default:* yes |break| Switch on adaptive control of linear solver tolerance for nonlinear solution

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _lubricationdynamic_adaptconv_better:

**ADAPTCONV_BETTER** | *default:* 0.1 |break| The linear solver shall be this much better than the current nonlinear residual in the nonlinear convergence limit

.. _lubricationdynamic_norm_pre:

**NORM_PRE** | *default:* Abs |break| type of norm for temperature convergence check

   **Possible values:**

   - Abs
   - Rel
   - Mix

.. _lubricationdynamic_norm_resf:

**NORM_RESF** | *default:* Abs |break| type of norm for residual convergence check

   **Possible values:**

   - Abs
   - Rel
   - Mix

.. _lubricationdynamic_iternorm:

**ITERNORM** | *default:* L2 |break| type of norm to be applied to residuals

   **Possible values:**

   - L1
   - L2
   - Rms
   - Inf

.. _lubricationdynamic_tolpre:

**TOLPRE** | *default:* 1e-06 |break| tolerance in the temperature norm of the Newton iteration

.. _lubricationdynamic_tolres:

**TOLRES** | *default:* 1e-06 |break| tolerance in the residual norm for the Newton iteration

.. _lubricationdynamic_penalty_cavitation:

**PENALTY_CAVITATION** | *default:* 0 |break| penalty parameter for regularized cavitation

.. _lubricationdynamic_gap_offset:

**GAP_OFFSET** | *default:* 0 |break| Additional offset to the fluid gap

.. _lubricationdynamic_roughness_std_deviation:

**ROUGHNESS_STD_DEVIATION** | *default:* 0 |break| standard deviation of surface roughness

.. _lubricationdynamic_modified_reynolds_equ:

**MODIFIED_REYNOLDS_EQU** | *default:* No |break| the lubrication problem will use the modified reynolds equ. in order to consider surface roughness

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _lubricationdynamic_add_squeeze_term:

**ADD_SQUEEZE_TERM** | *default:* No |break| the squeeze term will also be considered in the Reynolds Equation

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _lubricationdynamic_pure_lub:

**PURE_LUB** | *default:* No |break| the problem is pure lubrication

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECscalartransportdynamic:

SCALAR TRANSPORT DYNAMIC
------------------------

control parameters for scalar transport problems


::

   -------------------------------------------SCALAR TRANSPORT DYNAMIC

.. _scalartransportdynamic_solvertype:

**SOLVERTYPE** | *default:* linear_full |break| type of scalar transport solver

   **Possible values:**

   - linear_full
   - linear_incremental
   - nonlinear
   - nonlinear_multiscale_macrotomicro
   - nonlinear_multiscale_macrotomicro_aitken
   - nonlinear_multiscale_macrotomicro_aitken_dofsplit
   - nonlinear_multiscale_microtomacro

.. _scalartransportdynamic_timeintegr:

**TIMEINTEGR** | *default:* One_Step_Theta |break| Time Integration Scheme

   **Possible values:**

   - Stationary
   - One_Step_Theta
   - BDF2
   - Gen_Alpha

.. _scalartransportdynamic_maxtime:

**MAXTIME** | *default:* 1000 |break| Total simulation time

.. _scalartransportdynamic_numstep:

**NUMSTEP** | *default:* 20 |break| Total number of time steps

.. _scalartransportdynamic_timestep:

**TIMESTEP** | *default:* 0.1 |break| Time increment dt

.. _scalartransportdynamic_theta:

**THETA** | *default:* 0.5 |break| One-step-theta time integration factor

.. _scalartransportdynamic_alpha_m:

**ALPHA_M** | *default:* 0.5 |break| Generalized-alpha time integration factor

.. _scalartransportdynamic_alpha_f:

**ALPHA_F** | *default:* 0.5 |break| Generalized-alpha time integration factor

.. _scalartransportdynamic_gamma:

**GAMMA** | *default:* 0.5 |break| Generalized-alpha time integration factor

.. _scalartransportdynamic_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| Increment for writing solution

.. _scalartransportdynamic_restartevry:

**RESTARTEVRY** | *default:* 1 |break| Increment for writing restart

.. _scalartransportdynamic_matid:

**MATID** | *default:* -1 |break| Material ID for automatic mesh generation

.. _scalartransportdynamic_velocityfield:

**VELOCITYFIELD** | *default:* zero |break| type of velocity field used for scalar transport problems

   **Possible values:**

   - zero
   - function
   - Navier_Stokes

.. _scalartransportdynamic_velfuncno:

**VELFUNCNO** | *default:* -1 |break| function number for scalar transport velocity field

.. _scalartransportdynamic_initialfield:

**INITIALFIELD** | *default:* zero_field |break| Initial Field for scalar transport problem

   **Possible values:**

   - zero_field
   - field_by_function
   - field_by_condition
   - disturbed_field_by_function
   - 1D_DISCONTPV
   - FLAME_VORTEX_INTERACTION
   - RAYTAYMIXFRAC
   - L_shaped_domain
   - facing_flame_fronts
   - oracles_flame
   - high_forced_hit
   - low_forced_hit
   - algebraic_field_dependence

.. _scalartransportdynamic_initfuncno:

**INITFUNCNO** | *default:* -1 |break| function number for scalar transport initial field

.. _scalartransportdynamic_sphericalcoords:

**SPHERICALCOORDS** | *default:* No |break| use of spherical coordinates

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_calcerror:

**CALCERROR** | *default:* No |break| compute error compared to analytical solution

   **Possible values:**

   - No
   - Kwok_Wu
   - ConcentricCylinders
   - Electroneutrality
   - error_by_function
   - error_by_condition
   - SphereDiffusion
   - AnalyticSeries

.. _scalartransportdynamic_calcerrorno:

**CALCERRORNO** | *default:* -1 |break| function number for scalar transport error computation

.. _scalartransportdynamic_calcflux_domain:

**CALCFLUX_DOMAIN** | *default:* No |break| output of diffusive/total flux vectors inside domain

   **Possible values:**

   - No
   - total
   - diffusive

.. _scalartransportdynamic_calcflux_domain_lumped:

**CALCFLUX_DOMAIN_LUMPED** | *default:* Yes |break| perform approximate domain flux calculation involving matrix lumping

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_calcflux_boundary:

**CALCFLUX_BOUNDARY** | *default:* No |break| output of convective/diffusive/total flux vectors on boundary

   **Possible values:**

   - No
   - total
   - diffusive
   - convective

.. _scalartransportdynamic_calcflux_boundary_lumped:

**CALCFLUX_BOUNDARY_LUMPED** | *default:* Yes |break| perform approximate boundary flux calculation involving matrix lumping

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_writeflux_ids:

**WRITEFLUX_IDS** | *default:* -1 |break| Write diffusive/total flux vector fields for these scalar fields only (starting with 1)

.. _scalartransportdynamic_outputscalars:

**OUTPUTSCALARS** | *default:* none |break| Output of total and mean values for transported scalars

   **Possible values:**

   - none
   - entire_domain
   - by_condition
   - entire_domain_and_by_condition

.. _scalartransportdynamic_outputscalarsmeangrad:

**OUTPUTSCALARSMEANGRAD** | *default:* No |break| Output of mean gradient of scalars

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_outintegrreac:

**OUTINTEGRREAC** | *default:* No |break| Output of integral reaction values

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_output_gmsh:

**OUTPUT_GMSH** | *default:* No |break| Do you want to write Gmsh postprocessing files?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_matlab_state_output:

**MATLAB_STATE_OUTPUT** | *default:* No |break| Do you want to write the state solution to Matlab file?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_convform:

**CONVFORM** | *default:* convective |break| form of convective term

   **Possible values:**

   - convective
   - conservative

.. _scalartransportdynamic_neumanninflow:

**NEUMANNINFLOW** | *default:* no |break| Flag to (de)activate potential Neumann inflow term(s)

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_conv_heat_trans:

**CONV_HEAT_TRANS** | *default:* no |break| Flag to (de)activate potential convective heat transfer boundary conditions

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_skipinitder:

**SKIPINITDER** | *default:* no |break| Flag to skip computation of initial time derivative

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_fssugrdiff:

**FSSUGRDIFF** | *default:* No |break| fine-scale subgrid diffusivity

   **Possible values:**

   - No
   - artificial
   - Smagorinsky_all
   - Smagorinsky_small

.. _scalartransportdynamic_electromagneticdiffusion:

**ELECTROMAGNETICDIFFUSION** | *default:* No |break| flag to activate electromagnetic diffusion problems

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_emdsource:

**EMDSOURCE** | *default:* -1 |break| Current density source

.. _scalartransportdynamic_meshtying:

**MESHTYING** | *default:* no |break| Flag to (de)activate mesh tying algorithm

   **Possible values:**

   - no
   - Condensed_Smat
   - Condensed_Bmat
   - Condensed_Bmat_merged

.. _scalartransportdynamic_fieldcoupling:

**FIELDCOUPLING** | *default:* matching |break| Type of coupling strategy between fields

   **Possible values:**

   - matching
   - volmortar

.. _scalartransportdynamic_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| number of linear solver used for scalar transport/elch...

.. _scalartransportdynamic_l2_proj_linear_solver:

**L2_PROJ_LINEAR_SOLVER** | *default:* -1 |break| number of linear solver used for l2-projection sub-problems

.. _scalartransportdynamic_equilibration:

**EQUILIBRATION** | *default:* none |break| flag for equilibration of global system of equations

   **Possible values:**

   - none
   - rows_full
   - rows_maindiag
   - columns_full
   - columns_maindiag
   - rowsandcolumns_full
   - rowsandcolumns_maindiag

.. _scalartransportdynamic_matrixtype:

**MATRIXTYPE** | *default:* sparse |break| type of global system matrix in global system of equations

   **Possible values:**

   - sparse
   - block_condition
   - block_condition_dof

.. _scalartransportdynamic_natural_convection:

**NATURAL_CONVECTION** | *default:* No |break| Include natural convection effects

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_fdcheck:

**FDCHECK** | *default:* none |break| flag for finite difference check: none, local, or global

   **Possible values:**

   - none
   - global
   - global_extended
   - local

.. _scalartransportdynamic_fdcheckeps:

**FDCHECKEPS** | *default:* 1e-06 |break| dof perturbation magnitude for finite difference check (1.e-6 seems to work very well, whereas smaller values don't)

.. _scalartransportdynamic_fdchecktol:

**FDCHECKTOL** | *default:* 1e-06 |break| relative tolerance for finite difference check

.. _scalartransportdynamic_computeintegrals:

**COMPUTEINTEGRALS** | *default:* none |break| flag for optional computation of domain integrals

   **Possible values:**

   - none
   - initial
   - repeated

.. _scalartransportdynamic_padaptivity:

**PADAPTIVITY** | *default:* no |break| Flag to (de)activate p-adativity

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_padapterrortol:

**PADAPTERRORTOL** | *default:* 1e-06 |break| The error tolerance to calculate the variation of the elemental degree

.. _scalartransportdynamic_padapterrorbase:

**PADAPTERRORBASE** | *default:* 1.66 |break| The error tolerance base to calculate the variation of the elemental degree

.. _scalartransportdynamic_padaptdegreemax:

**PADAPTDEGREEMAX** | *default:* 4 |break| The max. degree of the shape functions

.. _scalartransportdynamic_semiimplicit:

**SEMIIMPLICIT** | *default:* no |break| Flag to (de)activate semi-implicit calculation of the reaction term

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_outputlinsolverstats:

**OUTPUTLINSOLVERSTATS** | *default:* No |break| flag for output of performance statistics associated with linear solver into csv file

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_outputnonlinsolverstats:

**OUTPUTNONLINSOLVERSTATS** | *default:* No |break| flag for output of performance statistics associated with nonlinear solver into csv file

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_nullspace_pointbased:

**NULLSPACE_POINTBASED** | *default:* No |break| flag for point-based null space calculation

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_adaptive_timestepping:

**ADAPTIVE_TIMESTEPPING** | *default:* No |break| flag for adaptive time stepping

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECscalartransportdynamic_nonlinear:

SCALAR TRANSPORT DYNAMIC/NONLINEAR
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

control parameters for solving nonlinear SCATRA problems


::

   ---------------------------------SCALAR TRANSPORT DYNAMIC/NONLINEAR

.. _scalartransportdynamic_nonlinear_itemax:

**ITEMAX** | *default:* 10 |break| max. number of nonlin. iterations

.. _scalartransportdynamic_nonlinear_convtol:

**CONVTOL** | *default:* 1e-06 |break| Tolerance for convergence check

.. _scalartransportdynamic_nonlinear_itemax_outer:

**ITEMAX_OUTER** | *default:* 10 |break| Maximum number of outer iterations in partitioned coupling schemes (natural convection, multi-scale simulations etc.)

.. _scalartransportdynamic_nonlinear_convtol_outer:

**CONVTOL_OUTER** | *default:* 1e-06 |break| Convergence check tolerance for outer loop in partitioned coupling schemes (natural convection, multi-scale simulations etc.)

.. _scalartransportdynamic_nonlinear_explpredict:

**EXPLPREDICT** | *default:* no |break| do an explicit predictor step before starting nonlinear iteration

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_nonlinear_abstolres:

**ABSTOLRES** | *default:* 1e-14 |break| Absolute tolerance for deciding if residual of nonlinear problem is already zero

.. _scalartransportdynamic_nonlinear_adaptconv:

**ADAPTCONV** | *default:* yes |break| Switch on adaptive control of linear solver tolerance for nonlinear solution

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_nonlinear_adaptconv_better:

**ADAPTCONV_BETTER** | *default:* 0.1 |break| The linear solver shall be this much better than the current nonlinear residual in the nonlinear convergence limit

.. _SECscalartransportdynamic_stabilization:

SCALAR TRANSPORT DYNAMIC/STABILIZATION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

control parameters for the stabilization of scalar transport problems

::

   -----------------------------SCALAR TRANSPORT DYNAMIC/STABILIZATION

.. _scalartransportdynamic_stabilization_stabtype:

**STABTYPE** | *default:* SUPG |break| type of stabilization (if any)

   **Possible values:**

   - no_stabilization
   - SUPG
   - GLS
   - USFEM
   - centered
   - upwind

.. _scalartransportdynamic_stabilization_sugrvel:

**SUGRVEL** | *default:* no |break| potential incorporation of subgrid-scale velocity

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_stabilization_assugrdiff:

**ASSUGRDIFF** | *default:* no |break| potential incorporation of all-scale subgrid diffusivity (a.k.a. discontinuity-capturing) term

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_stabilization_definition_tau:

**DEFINITION_TAU** | *default:* Franca_Valentin |break| Definition of tau

   **Possible values:**

   - Taylor_Hughes_Zarins
   - Taylor_Hughes_Zarins_wo_dt
   - Franca_Valentin
   - Franca_Valentin_wo_dt
   - Shakib_Hughes_Codina
   - Shakib_Hughes_Codina_wo_dt
   - Codina
   - Codina_wo_dt
   - Franca_Madureira_Valentin
   - Franca_Madureira_Valentin_wo_dt
   - Exact_1D
   - Zero
   - Numerical_Value

.. _scalartransportdynamic_stabilization_charelelength:

**CHARELELENGTH** | *default:* streamlength |break| Characteristic element length for tau

   **Possible values:**

   - streamlength
   - volume_equivalent_diameter
   - root_of_volume

.. _scalartransportdynamic_stabilization_definition_assgd:

**DEFINITION_ASSGD** | *default:* artificial_linear |break| Definition of (all-scale) subgrid diffusivity

   **Possible values:**

   - artificial_linear
   - artificial_linear_reinit
   - Hughes_etal_86_nonlinear
   - Tezduyar_Park_86_nonlinear
   - Tezduyar_Park_86_nonlinear_wo_phizero
   - doCarmo_Galeao_91_nonlinear
   - Almeida_Silva_97_nonlinear
   - YZbeta_nonlinear
   - Codina_nonlinear

.. _scalartransportdynamic_stabilization_evaluation_tau:

**EVALUATION_TAU** | *default:* element_center |break| Location where tau is evaluated

   **Possible values:**

   - element_center
   - integration_point

.. _scalartransportdynamic_stabilization_evaluation_mat:

**EVALUATION_MAT** | *default:* element_center |break| Location where material law is evaluated

   **Possible values:**

   - element_center
   - integration_point

.. _scalartransportdynamic_stabilization_consistency:

**CONSISTENCY** | *default:* no |break| improvement of consistency for stabilization

   **Possible values:**

   - no
   - L2_projection_lumped

.. _scalartransportdynamic_stabilization_tau_value:

**TAU_VALUE** | *default:* 0 |break| Numerical value for tau for stabilization

.. _SECscalartransportdynamic_arterycoupling:

SCALAR TRANSPORT DYNAMIC/ARTERY COUPLING
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Parameters for artery mesh tying

::

   ---------------------------SCALAR TRANSPORT DYNAMIC/ARTERY COUPLING

.. _scalartransportdynamic_arterycoupling_artery_coupling_method:

**ARTERY_COUPLING_METHOD** | *default:* None |break| Coupling method for artery coupling.

   **Possible values:**

   - None
   - Nodal
   - GPTS
   - MP
   - NTP

.. _scalartransportdynamic_arterycoupling_penalty:

**PENALTY** | *default:* 1000 |break| Penalty parameter for line-based coupling

.. _scalartransportdynamic_arterycoupling_coupleddofs_artscatra:

**COUPLEDDOFS_ARTSCATRA** | *default:* -1.0 |break| coupled artery dofs for mesh tying

.. _scalartransportdynamic_arterycoupling_coupleddofs_scatra:

**COUPLEDDOFS_SCATRA** | *default:* -1.0 |break| coupled porofluid dofs for mesh tying

.. _scalartransportdynamic_arterycoupling_reacfunct_art:

**REACFUNCT_ART** | *default:* -1 |break| functions for coupling (arteryscatra part)

.. _scalartransportdynamic_arterycoupling_scalereac_art:

**SCALEREAC_ART** | *default:* 0 |break| scale for coupling (arteryscatra part)

.. _scalartransportdynamic_arterycoupling_reacfunct_cont:

**REACFUNCT_CONT** | *default:* -1 |break| functions for coupling (scatra part)

.. _scalartransportdynamic_arterycoupling_scalereac_cont:

**SCALEREAC_CONT** | *default:* 0 |break| scale for coupling (scatra part)

.. _SECscalartransportdynamic_s2icoupling:

SCALAR TRANSPORT DYNAMIC/S2I COUPLING
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

control parameters for scatra-scatra interface coupling

::

   ------------------------------SCALAR TRANSPORT DYNAMIC/S2I COUPLING

.. _scalartransportdynamic_s2icoupling_couplingtype:

**COUPLINGTYPE** | *default:* Undefined |break| type of mortar meshtying

   **Possible values:**

   - Undefined
   - MatchingNodes
   - StandardMortar
   - SaddlePointMortar_Petrov
   - SaddlePointMortar_Bubnov
   - CondensedMortar_Petrov
   - CondensedMortar_Bubnov
   - StandardNodeToSegment

.. _scalartransportdynamic_s2icoupling_lmside:

**LMSIDE** | *default:* slave |break| flag for interface side underlying Lagrange multiplier definition

   **Possible values:**

   - slave
   - master

.. _scalartransportdynamic_s2icoupling_slaveonly:

**SLAVEONLY** | *default:* No |break| flag for evaluation of interface linearizations and residuals on slave side only

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _scalartransportdynamic_s2icoupling_ntsprojtol:

**NTSPROJTOL** | *default:* 0 |break| node-to-segment projection tolerance

.. _scalartransportdynamic_s2icoupling_intlayergrowth_evaluation:

**INTLAYERGROWTH_EVALUATION** | *default:* none |break| flag for evaluation of scatra-scatra interface coupling involving interface layer growth

   **Possible values:**

   - none
   - monolithic
   - semi-implicit

.. _scalartransportdynamic_s2icoupling_intlayergrowth_convtol:

**INTLAYERGROWTH_CONVTOL** | *default:* 1e-12 |break| local Newton-Raphson convergence tolerance for scatra-scatra interface coupling involving interface layer growth

.. _scalartransportdynamic_s2icoupling_intlayergrowth_itemax:

**INTLAYERGROWTH_ITEMAX** | *default:* 5 |break| maximum number of local Newton-Raphson iterations for scatra-scatra interface coupling involving interface layer growth

.. _scalartransportdynamic_s2icoupling_intlayergrowth_linear_solver:

**INTLAYERGROWTH_LINEAR_SOLVER** | *default:* -1 |break| ID of linear solver for monolithic scatra-scatra interface coupling involving interface layer growth

.. _scalartransportdynamic_s2icoupling_intlayergrowth_timestep:

**INTLAYERGROWTH_TIMESTEP** | *default:* -1 |break| modified time step size for scatra-scatra interface coupling involving interface layer growth

.. _scalartransportdynamic_s2icoupling_meshtying_conditions_independent_setup:

**MESHTYING_CONDITIONS_INDEPENDENT_SETUP** | *default:* No |break| mesh tying for different conditions should be setup independently

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SEClevel-setcontrol:

LEVEL-SET CONTROL
-----------------

control parameters for level-set problems


::

   --------------------------------------------------LEVEL-SET CONTROL

.. _level-setcontrol_numstep:

**NUMSTEP** | *default:* 24 |break| Total number of time steps

.. _level-setcontrol_timestep:

**TIMESTEP** | *default:* 0.1 |break| Time increment dt

.. _level-setcontrol_maxtime:

**MAXTIME** | *default:* 1000 |break| Total simulation time

.. _level-setcontrol_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| Increment for writing solution

.. _level-setcontrol_restartevry:

**RESTARTEVRY** | *default:* 1 |break| Increment for writing restart

.. _level-setcontrol_calcerror:

**CALCERROR** | *default:* No |break| compute error compared to analytical solution

   **Possible values:**

   - No
   - InitialField

.. _level-setcontrol_extract_interface_vel:

**EXTRACT_INTERFACE_VEL** | *default:* No |break| replace computed velocity at nodes of given distance of interface by approximated interface velocity

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _level-setcontrol_num_convel_layers:

**NUM_CONVEL_LAYERS** | *default:* -1 |break| number of layers around the interface which keep their computed convective velocity

.. _SEClevel-setcontrol_reinitialization:

LEVEL-SET CONTROL/REINITIALIZATION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ---------------------------------LEVEL-SET CONTROL/REINITIALIZATION

.. _level-setcontrol_reinitialization_reinitialization:

**REINITIALIZATION** | *default:* None |break| Type of reinitialization strategy for level set function

   **Possible values:**

   - None
   - Signed_Distance_Function
   - Sussman
   - EllipticEq

.. _level-setcontrol_reinitialization_reinit_initial:

**REINIT_INITIAL** | *default:* No |break| Has level set field to be reinitialized before first time step?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _level-setcontrol_reinitialization_reinitinterval:

**REINITINTERVAL** | *default:* 1 |break| reinitialization interval

.. _level-setcontrol_reinitialization_reinitband:

**REINITBAND** | *default:* No |break| reinitialization only within a band around the interface, or entire domain?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _level-setcontrol_reinitialization_reinitbandwidth:

**REINITBANDWIDTH** | *default:* 1 |break| level-set value defining band width for reinitialization

.. _level-setcontrol_reinitialization_numstepsreinit:

**NUMSTEPSREINIT** | *default:* 1 |break| (maximal) number of pseudo-time steps

.. _level-setcontrol_reinitialization_linearizationreinit:

**LINEARIZATIONREINIT** | *default:* newton |break| linearization scheme for nonlinear convective term of reinitialization equation

   **Possible values:**

   - newton
   - fixed_point

.. _level-setcontrol_reinitialization_timestepreinit:

**TIMESTEPREINIT** | *default:* 1 |break| pseudo-time step length (usually a * characteristic element length of discretization with a>0)

.. _level-setcontrol_reinitialization_thetareinit:

**THETAREINIT** | *default:* 1 |break| theta for time discretization of reinitialization equation

.. _level-setcontrol_reinitialization_stabtypereinit:

**STABTYPEREINIT** | *default:* SUPG |break| type of stabilization (if any)

   **Possible values:**

   - no_stabilization
   - SUPG
   - GLS
   - USFEM

.. _level-setcontrol_reinitialization_definition_tau_reinit:

**DEFINITION_TAU_REINIT** | *default:* Taylor_Hughes_Zarins |break| Definition of tau

   **Possible values:**

   - Taylor_Hughes_Zarins
   - Taylor_Hughes_Zarins_wo_dt
   - Franca_Valentin
   - Franca_Valentin_wo_dt
   - Shakib_Hughes_Codina
   - Shakib_Hughes_Codina_wo_dt
   - Codina
   - Codina_wo_dt
   - Exact_1D
   - Zero

.. _level-setcontrol_reinitialization_artdiffreinit:

**ARTDIFFREINIT** | *default:* no |break| potential incorporation of all-scale subgrid diffusivity (a.k.a. discontinuity-capturing) term

   **Possible values:**

   - no
   - isotropic
   - crosswind

.. _level-setcontrol_reinitialization_definition_artdiffreinit:

**DEFINITION_ARTDIFFREINIT** | *default:* artificial_linear |break| Definition of (all-scale) subgrid diffusivity

   **Possible values:**

   - artificial_linear
   - artificial_linear_reinit
   - Hughes_etal_86_nonlinear
   - Tezduyar_Park_86_nonlinear
   - Tezduyar_Park_86_nonlinear_wo_phizero
   - doCarmo_Galeao_91_nonlinear
   - Almeida_Silva_97_nonlinear
   - YZbeta_nonlinear
   - Codina_nonlinear

.. _level-setcontrol_reinitialization_smoothed_sign_type:

**SMOOTHED_SIGN_TYPE** | *default:* SussmanSmerekaOsher1994 |break| sign function for reinitialization equation

   **Possible values:**

   - NonSmoothed
   - SussmanFatemi1999
   - SussmanSmerekaOsher1994
   - PengEtAl1999

.. _level-setcontrol_reinitialization_charelelengthreinit:

**CHARELELENGTHREINIT** | *default:* root_of_volume |break| characteristic element length for sign function

   **Possible values:**

   - root_of_volume
   - streamlength

.. _level-setcontrol_reinitialization_interface_thickness:

**INTERFACE_THICKNESS** | *default:* 1 |break| factor for interface thickness (multiplied by element length)

.. _level-setcontrol_reinitialization_velreinit:

**VELREINIT** | *default:* integration_point_based |break| evaluate velocity at integration point or compute node-based velocity

   **Possible values:**

   - integration_point_based
   - node_based

.. _level-setcontrol_reinitialization_corrector_step:

**CORRECTOR_STEP** | *default:* yes |break| correction of interface position via volume constraint according to Sussman & Fatemi

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _level-setcontrol_reinitialization_convtol_reinit:

**CONVTOL_REINIT** | *default:* -1 |break| tolerance for convergence check according to Sussman et al. 1994 (turned off negative)

.. _level-setcontrol_reinitialization_reinitvolcorrection:

**REINITVOLCORRECTION** | *default:* No |break| volume correction after reinitialization

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _level-setcontrol_reinitialization_penalty_para:

**PENALTY_PARA** | *default:* -1 |break| penalty parameter for elliptic reinitialization

.. _level-setcontrol_reinitialization_dimension:

**DIMENSION** | *default:* 3D |break| number of space dimensions for handling of quasi-2D problems with 3D elements

   **Possible values:**

   - 3D
   - 2Dx
   - 2Dy
   - 2Dz

.. _level-setcontrol_reinitialization_projection:

**PROJECTION** | *default:* yes |break| use L2-projection for grad phi and related quantities

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _level-setcontrol_reinitialization_projection_diff:

**PROJECTION_DIFF** | *default:* 0 |break| use diffusive term for L2-projection

.. _level-setcontrol_reinitialization_lumping:

**LUMPING** | *default:* no |break| use lumped mass matrix for L2-projection

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _level-setcontrol_reinitialization_diff_func:

**DIFF_FUNC** | *default:* hyperbolic |break| function for diffusivity

   **Possible values:**

   - hyperbolic
   - hyperbolic_smoothed_positive
   - hyperbolic_clipped_05
   - hyperbolic_clipped_1

.. _SECelchcontrol:

ELCH CONTROL
------------

control parameters for electrochemistry problems


::

   -------------------------------------------------------ELCH CONTROL

.. _elchcontrol_movboundaryitemax:

**MOVBOUNDARYITEMAX** | *default:* 10 |break| Maximum number of outer iterations in electrode shape change computations

.. _elchcontrol_movboundaryconvtol:

**MOVBOUNDARYCONVTOL** | *default:* 1e-06 |break| Convergence check tolerance for outer loop in electrode shape change computations

.. _elchcontrol_temperature:

**TEMPERATURE** | *default:* 298 |break| Constant temperature (Kelvin)

.. _elchcontrol_temperature_from_funct:

**TEMPERATURE_FROM_FUNCT** | *default:* -1 |break| Homogeneous temperature within electrochemistry field that can be time dependent according to function definition

.. _elchcontrol_faraday_constant:

**FARADAY_CONSTANT** | *default:* 96485.3 |break| Faraday constant (in unit system as chosen in input file)

.. _elchcontrol_gas_constant:

**GAS_CONSTANT** | *default:* 8.31447 |break| (universal) gas constant (in unit system as chosen in input file)

.. _elchcontrol_movingboundary:

**MOVINGBOUNDARY** | *default:* No |break| ELCH algorithm for deforming meshes

   **Possible values:**

   - No
   - pseudo-transient
   - fully-transient

.. _elchcontrol_molarvolume:

**MOLARVOLUME** | *default:* 0 |break| Molar volume for electrode shape change computations

.. _elchcontrol_movboundarytheta:

**MOVBOUNDARYTHETA** | *default:* 0 |break| One-step-theta factor in electrode shape change computations

.. _elchcontrol_galvanostatic:

**GALVANOSTATIC** | *default:* No |break| flag for galvanostatic mode

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _elchcontrol_gstat_approx_elect_resist:

**GSTAT_APPROX_ELECT_RESIST** | *default:* relation_pot_cur |break| relation of potential and current flow

   **Possible values:**

   - relation_pot_cur
   - effective_length_with_initial_cond
   - effective_length_with_integrated_cond

.. _elchcontrol_gstatcondid_cathode:

**GSTATCONDID_CATHODE** | *default:* 0 |break| condition id of electrode kinetics for cathode

.. _elchcontrol_gstatcondid_anode:

**GSTATCONDID_ANODE** | *default:* 1 |break| condition id of electrode kinetics for anode

.. _elchcontrol_gstatconvtol:

**GSTATCONVTOL** | *default:* 1e-05 |break| Convergence check tolerance for galvanostatic mode

.. _elchcontrol_gstatcurtol:

**GSTATCURTOL** | *default:* 1e-15 |break| Current Tolerance

.. _elchcontrol_gstatfunctno:

**GSTATFUNCTNO** | *default:* -1 |break| function number defining the imposed current curve

.. _elchcontrol_gstatitemax:

**GSTATITEMAX** | *default:* 10 |break| maximum number of iterations for galvanostatic mode

.. _elchcontrol_gstat_length_currentpath:

**GSTAT_LENGTH_CURRENTPATH** | *default:* 0 |break| average length of the current path

.. _elchcontrol_equpot:

**EQUPOT** | *default:* Undefined |break| type of closing equation for electric potential

   **Possible values:**

   - Undefined
   - ENC
   - ENC_PDE
   - ENC_PDE_ELIM
   - Poisson
   - Laplace
   - divi

.. _elchcontrol_blockprecond:

**BLOCKPRECOND** | *default:* NO |break| Switch to block-preconditioned family of solvers, only works with block preconditioners like CheapSIMPLE!

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _elchcontrol_diffcond_formulation:

**DIFFCOND_FORMULATION** | *default:* No |break| Activation of diffusion-conduction formulation

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _elchcontrol_initpotcalc:

**INITPOTCALC** | *default:* No |break| Automatically calculate initial field for electric potential

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _elchcontrol_onlypotential:

**ONLYPOTENTIAL** | *default:* no |break| Coupling of general ion transport equation with Laplace equation

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _elchcontrol_couple_boundary_fluxes:

**COUPLE_BOUNDARY_FLUXES** | *default:* Yes |break| Coupling of lithium-ion flux density and electric current density at Dirichlet and Neumann boundaries

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _elchcontrol_cycling_timestep:

**CYCLING_TIMESTEP** | *default:* -1 |break| modified time step size for CCCV cell cycling

.. _SECelchcontrol_diffcond:

ELCH CONTROL/DIFFCOND
~~~~~~~~~~~~~~~~~~~~~

control parameters for electrochemical diffusion conduction problems


::

   ----------------------------------------------ELCH CONTROL/DIFFCOND

.. _elchcontrol_diffcond_current_solution_var:

**CURRENT_SOLUTION_VAR** | *default:* No |break| Current as a solution variable

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _elchcontrol_diffcond_mat_diffcond_diffbased:

**MAT_DIFFCOND_DIFFBASED** | *default:* Yes |break| Coupling terms of chemical diffusion for current equation are based on t and kappa

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _elchcontrol_diffcond_mat_newman_const_a:

**MAT_NEWMAN_CONST_A** | *default:* 2 |break| Constant A for the Newman model(term for the concentration overpotential)

.. _elchcontrol_diffcond_mat_newman_const_b:

**MAT_NEWMAN_CONST_B** | *default:* -2 |break| Constant B for the Newman model(term for the concentration overpotential)

.. _elchcontrol_diffcond_mat_newman_const_c:

**MAT_NEWMAN_CONST_C** | *default:* -1 |break| Constant C for the Newman model(term for the concentration overpotential)

.. _elchcontrol_diffcond_permittivity_vacuum:

**PERMITTIVITY_VACUUM** | *default:* 8.85419e-12 |break| Vacuum permittivity

.. _SECelchcontrol_scl:

ELCH CONTROL/SCL
~~~~~~~~~~~~~~~~

control parameters for coupled probelms with space-charge layer formation


::

   ---------------------------------------------------ELCH CONTROL/SCL

.. _elchcontrol_scl_add_micro_macro_coupling:

**ADD_MICRO_MACRO_COUPLING** | *default:* No |break| flag for micro macro coupling with scls

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _elchcontrol_scl_coupling_output:

**COUPLING_OUTPUT** | *default:* No |break| write coupled node gids and node coordinates to csv file

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _elchcontrol_scl_initpotcalc:

**INITPOTCALC** | *default:* No |break| calculate initial potential field?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _elchcontrol_scl_solver:

**SOLVER** | *default:* -1 |break| solver for coupled SCL problem

.. _elchcontrol_scl_matrixtype:

**MATRIXTYPE** | *default:* undefined |break| type of global system matrix in global system of equations

   **Possible values:**

   - undefined
   - block
   - sparse

.. _elchcontrol_scl_adapt_time_step:

**ADAPT_TIME_STEP** | *default:* -1 |break| time step when time step size should be updated to 'ADAPTED_TIME_STEP_SIZE'.

.. _elchcontrol_scl_adapted_time_step_size:

**ADAPTED_TIME_STEP_SIZE** | *default:* -1 |break| new time step size.

.. _elchcontrol_scl_initialfield:

**INITIALFIELD** | *default:* zero_field |break| Initial Field for scalar transport problem

   **Possible values:**

   - zero_field
   - field_by_function
   - field_by_condition

.. _elchcontrol_scl_initfuncno:

**INITFUNCNO** | *default:* -1 |break| function number for scalar transport initial field

.. _SECcardiacmonodomaincontrol:

CARDIAC MONODOMAIN CONTROL
--------------------------

control parameters for cardiac electrophysiology problems


::

   -----------------------------------------CARDIAC MONODOMAIN CONTROL

.. _cardiacmonodomaincontrol_writemaxintstate:

**WRITEMAXINTSTATE** | *default:* 0 |break| number of maximal internal state variables to be postprocessed

.. _cardiacmonodomaincontrol_writemaxioniccurrents:

**WRITEMAXIONICCURRENTS** | *default:* 0 |break| number of maximal ionic currents to be postprocessed

.. _cardiacmonodomaincontrol_actthres:

**ACTTHRES** | *default:* 1 |break| threshold for the potential for computing and postprocessing activation time 

.. _SECstidynamic:

STI DYNAMIC
-----------

general control parameters for scatra-thermo interaction problems

::

   --------------------------------------------------------STI DYNAMIC

.. _stidynamic_scatratiminttype:

**SCATRATIMINTTYPE** | *default:* Standard |break| scalar transport time integration type is needed to instantiate correct scalar transport time integration scheme for scatra-thermo interaction problems

   **Possible values:**

   - Standard
   - Elch

.. _stidynamic_couplingtype:

**COUPLINGTYPE** | *default:* Undefined |break| type of coupling between scatra and thermo fields

   **Possible values:**

   - Undefined
   - Monolithic
   - OneWay_ScatraToThermo
   - OneWay_ThermoToScatra
   - TwoWay_ScatraToThermo
   - TwoWay_ScatraToThermo_Aitken
   - TwoWay_ScatraToThermo_Aitken_Dofsplit
   - TwoWay_ThermoToScatra
   - TwoWay_ThermoToScatra_Aitken

.. _stidynamic_thermo_initialfield:

**THERMO_INITIALFIELD** | *default:* zero_field |break| initial temperature field for scatra-thermo interaction problems

   **Possible values:**

   - zero_field
   - field_by_function
   - field_by_condition

.. _stidynamic_thermo_initfuncno:

**THERMO_INITFUNCNO** | *default:* -1 |break| function number for initial temperature field for scatra-thermo interaction problems

.. _stidynamic_thermo_linear_solver:

**THERMO_LINEAR_SOLVER** | *default:* -1 |break| ID of linear solver for temperature field

.. _stidynamic_thermo_condensation:

**THERMO_CONDENSATION** | *default:* No |break| flag for double condensation of linear equations associated with temperature field

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECstidynamic_monolithic:

STI DYNAMIC/MONOLITHIC
~~~~~~~~~~~~~~~~~~~~~~

control parameters for monolithic scatra-thermo interaction problems

::

   ---------------------------------------------STI DYNAMIC/MONOLITHIC

.. _stidynamic_monolithic_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| ID of linear solver for global system of equations

.. _stidynamic_monolithic_matrixtype:

**MATRIXTYPE** | *default:* block |break| type of global system matrix in global system of equations

   **Possible values:**

   - block
   - sparse

.. _SECstidynamic_partitioned:

STI DYNAMIC/PARTITIONED
~~~~~~~~~~~~~~~~~~~~~~~

control parameters for partitioned scatra-thermo interaction problems

::

   --------------------------------------------STI DYNAMIC/PARTITIONED

.. _stidynamic_partitioned_omega:

**OMEGA** | *default:* 1 |break| relaxation parameter

.. _stidynamic_partitioned_omegamax:

**OMEGAMAX** | *default:* 0 |break| maximum value of Aitken relaxation parameter (0.0 = no constraint)

.. _SECfs3idynamic:

FS3I DYNAMIC
------------

control parameters for FS3I problems


::

   -------------------------------------------------------FS3I DYNAMIC

.. _fs3idynamic_timestep:

**TIMESTEP** | *default:* 0.1 |break| Time increment dt

.. _fs3idynamic_numstep:

**NUMSTEP** | *default:* 20 |break| Total number of time steps

.. _fs3idynamic_maxtime:

**MAXTIME** | *default:* 1000 |break| Total simulation time

.. _fs3idynamic_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| Increment for writing solution

.. _fs3idynamic_restartevry:

**RESTARTEVRY** | *default:* 1 |break| Increment for writing restart

.. _fs3idynamic_scatra_solvertype:

**SCATRA_SOLVERTYPE** | *default:* nonlinear |break| type of scalar transport solver

   **Possible values:**

   - linear
   - nonlinear

.. _fs3idynamic_inf_perm:

**INF_PERM** | *default:* yes |break| Flag for infinite permeability

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fs3idynamic_consthermpress:

**CONSTHERMPRESS** | *default:* Yes |break| treatment of thermodynamic pressure in time

   **Possible values:**

   - No_energy
   - No_mass
   - Yes

.. _fs3idynamic_coupled_linear_solver:

**COUPLED_LINEAR_SOLVER** | *default:* -1 |break| number of linear solver used for fs3i problem

.. _fs3idynamic_linear_solver1:

**LINEAR_SOLVER1** | *default:* -1 |break| number of linear solver used for fluid problem

.. _fs3idynamic_linear_solver2:

**LINEAR_SOLVER2** | *default:* -1 |break| number of linear solver used for structural problem

.. _fs3idynamic_structscal_convform:

**STRUCTSCAL_CONVFORM** | *default:* conservative |break| form of convective term of structure scalar

   **Possible values:**

   - convective
   - conservative

.. _fs3idynamic_structscal_initialfield:

**STRUCTSCAL_INITIALFIELD** | *default:* zero_field |break| Initial Field for structure scalar transport problem

   **Possible values:**

   - zero_field
   - field_by_function

.. _fs3idynamic_structscal_initfuncno:

**STRUCTSCAL_INITFUNCNO** | *default:* -1 |break| function number for structure scalar transport initial field

.. _fs3idynamic_structscal_fieldcoupling:

**STRUCTSCAL_FIELDCOUPLING** | *default:* volume_matching |break| Type of coupling strategy between structure and structure-scalar field

   **Possible values:**

   - volume_matching
   - volume_nonmatching

.. _fs3idynamic_fluidscal_fieldcoupling:

**FLUIDSCAL_FIELDCOUPLING** | *default:* volume_matching |break| Type of coupling strategy between fluid and fluid-scalar field

   **Possible values:**

   - volume_matching
   - volume_nonmatching

.. _fs3idynamic_fluidscal_scatratype:

**FLUIDSCAL_SCATRATYPE** | *default:* ConvectionDiffusion |break| Type of scalar transport problem

   **Possible values:**

   - Undefined
   - ConvectionDiffusion
   - Loma
   - Advanced_Reaction
   - Chemotaxis
   - Chemo_Reac

.. _fs3idynamic_restart_from_part_fsi:

**RESTART_FROM_PART_FSI** | *default:* No |break| restart from partitioned fsi problem (e.g. from prestress calculations) instead of fs3i

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECfs3idynamic_partitioned:

FS3I DYNAMIC/PARTITIONED
~~~~~~~~~~~~~~~~~~~~~~~~

partioned fluid-structure-scalar-scalar interaction control section

::

   -------------------------------------------FS3I DYNAMIC/PARTITIONED

.. _fs3idynamic_partitioned_coupalgo:

**COUPALGO** | *default:* fs3i_IterStagg |break| Coupling strategies for FS3I solvers

   **Possible values:**

   - fs3i_SequStagg
   - fs3i_IterStagg

.. _fs3idynamic_partitioned_convtol:

**CONVTOL** | *default:* 1e-06 |break| tolerance for convergence check of outer iteration within partitioned FS3I

.. _fs3idynamic_partitioned_itemax:

**ITEMAX** | *default:* 10 |break| Maximum number of outer iterations

.. _SECfs3idynamic_structurescalarstabilization:

FS3I DYNAMIC/STRUCTURE SCALAR STABILIZATION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

parameters for stabilization of the structure-scalar field

::

   ------------------------FS3I DYNAMIC/STRUCTURE SCALAR STABILIZATION

.. _fs3idynamic_structurescalarstabilization_stabtype:

**STABTYPE** | *default:* SUPG |break| type of stabilization (if any)

   **Possible values:**

   - no_stabilization
   - SUPG
   - GLS
   - USFEM
   - centered
   - upwind

.. _fs3idynamic_structurescalarstabilization_sugrvel:

**SUGRVEL** | *default:* no |break| potential incorporation of subgrid-scale velocity

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fs3idynamic_structurescalarstabilization_assugrdiff:

**ASSUGRDIFF** | *default:* no |break| potential incorporation of all-scale subgrid diffusivity (a.k.a. discontinuity-capturing) term

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fs3idynamic_structurescalarstabilization_definition_tau:

**DEFINITION_TAU** | *default:* Franca_Valentin |break| Definition of tau

   **Possible values:**

   - Taylor_Hughes_Zarins
   - Taylor_Hughes_Zarins_wo_dt
   - Franca_Valentin
   - Franca_Valentin_wo_dt
   - Shakib_Hughes_Codina
   - Shakib_Hughes_Codina_wo_dt
   - Codina
   - Codina_wo_dt
   - Franca_Madureira_Valentin
   - Franca_Madureira_Valentin_wo_dt
   - Exact_1D
   - Zero
   - Numerical_Value

.. _fs3idynamic_structurescalarstabilization_charelelength:

**CHARELELENGTH** | *default:* streamlength |break| Characteristic element length for tau

   **Possible values:**

   - streamlength
   - volume_equivalent_diameter
   - root_of_volume

.. _fs3idynamic_structurescalarstabilization_definition_assgd:

**DEFINITION_ASSGD** | *default:* artificial_linear |break| Definition of (all-scale) subgrid diffusivity

   **Possible values:**

   - artificial_linear
   - artificial_linear_reinit
   - Hughes_etal_86_nonlinear
   - Tezduyar_Park_86_nonlinear
   - Tezduyar_Park_86_nonlinear_wo_phizero
   - doCarmo_Galeao_91_nonlinear
   - Almeida_Silva_97_nonlinear
   - YZbeta_nonlinear
   - Codina_nonlinear

.. _fs3idynamic_structurescalarstabilization_evaluation_tau:

**EVALUATION_TAU** | *default:* element_center |break| Location where tau is evaluated

   **Possible values:**

   - element_center
   - integration_point

.. _fs3idynamic_structurescalarstabilization_evaluation_mat:

**EVALUATION_MAT** | *default:* element_center |break| Location where material law is evaluated

   **Possible values:**

   - element_center
   - integration_point

.. _fs3idynamic_structurescalarstabilization_consistency:

**CONSISTENCY** | *default:* no |break| improvement of consistency for stabilization

   **Possible values:**

   - no
   - L2_projection_lumped

.. _fs3idynamic_structurescalarstabilization_tau_value:

**TAU_VALUE** | *default:* 0 |break| Numerical value for tau for stabilization

.. _SECfs3idynamic_ac:

FS3I DYNAMIC/AC
~~~~~~~~~~~~~~~

Atherosclerosis fluid-structure-scalar-scalar interaction control section

::

   ----------------------------------------------------FS3I DYNAMIC/AC

.. _fs3idynamic_ac_fsi_steps_per_scatra_step:

**FSI_STEPS_PER_SCATRA_STEP** | *default:* 1 |break| FSI time steps per SSI time step

.. _fs3idynamic_ac_periodicity:

**PERIODICITY** | *default:* -1 |break| Periodicity of the FSI problem

.. _fs3idynamic_ac_windkessel_rel_tol:

**WINDKESSEL_REL_TOL** | *default:* -1 |break| Tolerance for the fluid windkessel to decide if the FSI problem is periodic

.. _fs3idynamic_ac_fluid_scatra_rel_tol:

**FLUID_SCATRA_REL_TOL** | *default:* -1 |break| Tolerance for the fluid scatra field to decide if it is periodic

.. _fs3idynamic_ac_wss_rel_tol:

**WSS_REL_TOL** | *default:* -1 |break| Tolerance for the wall shear stresses to decide if the FSI problem is periodic

.. _fs3idynamic_ac_growth_updates:

**GROWTH_UPDATES** | *default:* 1 |break| Amount of growth updates in the large time scale loop

.. _fs3idynamic_ac_fsi_update_tol:

**FSI_UPDATE_TOL** | *default:* -1 |break| Tolerance for the structure scatra field to decide if a FSI update is necessary

.. _fs3idynamic_ac_large_timescale_timestep:

**LARGE_TIMESCALE_TIMESTEP** | *default:* -1 |break| time step of the large time scale

.. _SECporoelasticitydynamic:

POROELASTICITY DYNAMIC
----------------------

Poroelasticity

::

   ---------------------------------------------POROELASTICITY DYNAMIC

.. _poroelasticitydynamic_coupalgo:

**COUPALGO** | *default:* poro_monolithic |break| Coupling strategies for poroelasticity solvers

   **Possible values:**

   - poro_partitioned
   - poro_monolithic
   - poro_monolithicstructuresplit
   - poro_monolithicfluidsplit
   - poro_monolithicnopenetrationsplit
   - poro_monolithicmeshtying

.. _poroelasticitydynamic_physical_type:

**PHYSICAL_TYPE** | *default:* Poro |break| Physical Type of Porofluid

   **Possible values:**

   - Poro
   - Poro_P1

.. _poroelasticitydynamic_transient_terms:

**TRANSIENT_TERMS** | *default:* all |break| which equation includes transient terms

   **Possible values:**

   - none
   - momentum
   - continuity
   - all

.. _poroelasticitydynamic_restartevry:

**RESTARTEVRY** | *default:* 1 |break| write restart possibility every RESTARTEVRY steps

.. _poroelasticitydynamic_numstep:

**NUMSTEP** | *default:* 200 |break| maximum number of Timesteps

.. _poroelasticitydynamic_maxtime:

**MAXTIME** | *default:* 1000 |break| total simulation time

.. _poroelasticitydynamic_timestep:

**TIMESTEP** | *default:* 0.05 |break| time step size dt

.. _poroelasticitydynamic_itemax:

**ITEMAX** | *default:* 10 |break| maximum number of iterations over fields

.. _poroelasticitydynamic_itemin:

**ITEMIN** | *default:* 1 |break| minimal number of iterations over fields

.. _poroelasticitydynamic_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| increment for writing solution

.. _poroelasticitydynamic_tolres_global:

**TOLRES_GLOBAL** | *default:* 1e-08 |break| tolerance in the residual norm for the Newton iteration

.. _poroelasticitydynamic_tolinc_global:

**TOLINC_GLOBAL** | *default:* 1e-08 |break| tolerance in the increment norm for the Newton iteration

.. _poroelasticitydynamic_tolres_disp:

**TOLRES_DISP** | *default:* 1e-08 |break| tolerance in the residual norm for the Newton iteration

.. _poroelasticitydynamic_tolinc_disp:

**TOLINC_DISP** | *default:* 1e-08 |break| tolerance in the increment norm for the Newton iteration

.. _poroelasticitydynamic_tolres_poro:

**TOLRES_PORO** | *default:* 1e-08 |break| tolerance in the residual norm for the Newton iteration

.. _poroelasticitydynamic_tolinc_poro:

**TOLINC_PORO** | *default:* 1e-08 |break| tolerance in the increment norm for the Newton iteration

.. _poroelasticitydynamic_tolres_vel:

**TOLRES_VEL** | *default:* 1e-08 |break| tolerance in the residual norm for the Newton iteration

.. _poroelasticitydynamic_tolinc_vel:

**TOLINC_VEL** | *default:* 1e-08 |break| tolerance in the increment norm for the Newton iteration

.. _poroelasticitydynamic_tolres_pres:

**TOLRES_PRES** | *default:* 1e-08 |break| tolerance in the residual norm for the Newton iteration

.. _poroelasticitydynamic_tolinc_pres:

**TOLINC_PRES** | *default:* 1e-08 |break| tolerance in the increment norm for the Newton iteration

.. _poroelasticitydynamic_tolres_ncoup:

**TOLRES_NCOUP** | *default:* 1e-08 |break| tolerance in the residual norm for the Newton iteration

.. _poroelasticitydynamic_norm_inc:

**NORM_INC** | *default:* AbsSingleFields |break| type of norm for primary variables convergence check

   **Possible values:**

   - AbsGlobal
   - AbsSingleFields

.. _poroelasticitydynamic_norm_resf:

**NORM_RESF** | *default:* AbsSingleFields |break| type of norm for residual convergence check

   **Possible values:**

   - AbsGlobal
   - AbsSingleFields

.. _poroelasticitydynamic_normcombi_resfinc:

**NORMCOMBI_RESFINC** | *default:* And |break| binary operator to combine primary variables and residual force values

   **Possible values:**

   - And
   - Or

.. _poroelasticitydynamic_vectornorm_resf:

**VECTORNORM_RESF** | *default:* L2 |break| type of norm to be applied to residuals

   **Possible values:**

   - L1
   - L1_Scaled
   - L2
   - Rms
   - Inf

.. _poroelasticitydynamic_vectornorm_inc:

**VECTORNORM_INC** | *default:* L2 |break| type of norm to be applied to residuals

   **Possible values:**

   - L1
   - L1_Scaled
   - L2
   - Rms
   - Inf

.. _poroelasticitydynamic_secondorder:

**SECONDORDER** | *default:* Yes |break| Second order coupling at the interface.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _poroelasticitydynamic_contipartint:

**CONTIPARTINT** | *default:* No |break| Partial integration of porosity gradient in continuity equation

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _poroelasticitydynamic_contactnopen:

**CONTACTNOPEN** | *default:* No |break| No-Penetration Condition on active contact surface in case of poro contact problem!

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _poroelasticitydynamic_matchinggrid:

**MATCHINGGRID** | *default:* Yes |break| is matching grid

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _poroelasticitydynamic_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| number of linear solver used for poroelasticity problems

.. _poroelasticitydynamic_equilibration:

**EQUILIBRATION** | *default:* none |break| flag for equilibration of global system of equations

   **Possible values:**

   - none
   - rows_full
   - rows_maindiag
   - columns_full
   - columns_maindiag
   - rowsandcolumns_full
   - rowsandcolumns_maindiag

.. _SECporoscatracontrol:

POROSCATRA CONTROL
------------------

Control paramters for scatra porous media coupling

::

   -------------------------------------------------POROSCATRA CONTROL

.. _poroscatracontrol_restartevry:

**RESTARTEVRY** | *default:* 1 |break| write restart possibility every RESTARTEVRY steps

.. _poroscatracontrol_numstep:

**NUMSTEP** | *default:* 200 |break| maximum number of Timesteps

.. _poroscatracontrol_maxtime:

**MAXTIME** | *default:* 1000 |break| total simulation time

.. _poroscatracontrol_timestep:

**TIMESTEP** | *default:* 0.05 |break| time step size dt

.. _poroscatracontrol_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| increment for writing solution

.. _poroscatracontrol_itemax:

**ITEMAX** | *default:* 10 |break| maximum number of iterations over fields

.. _poroscatracontrol_itemin:

**ITEMIN** | *default:* 1 |break| minimal number of iterations over fields

.. _poroscatracontrol_tolres_global:

**TOLRES_GLOBAL** | *default:* 1e-08 |break| tolerance in the residual norm for the Newton iteration

.. _poroscatracontrol_tolinc_global:

**TOLINC_GLOBAL** | *default:* 1e-08 |break| tolerance in the increment norm for the Newton iteration

.. _poroscatracontrol_tolres_disp:

**TOLRES_DISP** | *default:* 1e-08 |break| tolerance in the residual norm for the Newton iteration

.. _poroscatracontrol_tolinc_disp:

**TOLINC_DISP** | *default:* 1e-08 |break| tolerance in the increment norm for the Newton iteration

.. _poroscatracontrol_tolres_vel:

**TOLRES_VEL** | *default:* 1e-08 |break| tolerance in the residual norm for the Newton iteration

.. _poroscatracontrol_tolinc_vel:

**TOLINC_VEL** | *default:* 1e-08 |break| tolerance in the increment norm for the Newton iteration

.. _poroscatracontrol_tolres_pres:

**TOLRES_PRES** | *default:* 1e-08 |break| tolerance in the residual norm for the Newton iteration

.. _poroscatracontrol_tolinc_pres:

**TOLINC_PRES** | *default:* 1e-08 |break| tolerance in the increment norm for the Newton iteration

.. _poroscatracontrol_tolres_scalar:

**TOLRES_SCALAR** | *default:* 1e-08 |break| tolerance in the residual norm for the Newton iteration

.. _poroscatracontrol_tolinc_scalar:

**TOLINC_SCALAR** | *default:* 1e-08 |break| tolerance in the increment norm for the Newton iteration

.. _poroscatracontrol_norm_inc:

**NORM_INC** | *default:* AbsSingleFields |break| type of norm for primary variables convergence check

   **Possible values:**

   - AbsGlobal
   - AbsSingleFields

.. _poroscatracontrol_norm_resf:

**NORM_RESF** | *default:* AbsSingleFields |break| type of norm for residual convergence check

   **Possible values:**

   - AbsGlobal
   - AbsSingleFields

.. _poroscatracontrol_normcombi_resfinc:

**NORMCOMBI_RESFINC** | *default:* And |break| binary operator to combine primary variables and residual force values

   **Possible values:**

   - And
   - Or

.. _poroscatracontrol_vectornorm_resf:

**VECTORNORM_RESF** | *default:* L2 |break| type of norm to be applied to residuals

   **Possible values:**

   - L1
   - L1_Scaled
   - L2
   - Rms
   - Inf

.. _poroscatracontrol_vectornorm_inc:

**VECTORNORM_INC** | *default:* L2 |break| type of norm to be applied to residuals

   **Possible values:**

   - L1
   - L1_Scaled
   - L2
   - Rms
   - Inf

.. _poroscatracontrol_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| number of linear solver used for monolithic poroscatra problems

.. _poroscatracontrol_coupalgo:

**COUPALGO** | *default:* solid_to_scatra |break| Coupling strategies for poroscatra solvers

   **Possible values:**

   - monolithic
   - scatra_to_solid
   - solid_to_scatra
   - two_way

.. _poroscatracontrol_matchinggrid:

**MATCHINGGRID** | *default:* Yes |break| is matching grid

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECporomultiphasedynamic:

POROMULTIPHASE DYNAMIC
----------------------

Control paramters for multiphase porous medium

::

   ---------------------------------------------POROMULTIPHASE DYNAMIC

.. _poromultiphasedynamic_restartevry:

**RESTARTEVRY** | *default:* 1 |break| write restart possibility every RESTARTEVRY steps

.. _poromultiphasedynamic_numstep:

**NUMSTEP** | *default:* 200 |break| maximum number of Timesteps

.. _poromultiphasedynamic_maxtime:

**MAXTIME** | *default:* 1000 |break| total simulation time

.. _poromultiphasedynamic_timestep:

**TIMESTEP** | *default:* -1 |break| time step size dt

.. _poromultiphasedynamic_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| increment for writing solution

.. _poromultiphasedynamic_itemax:

**ITEMAX** | *default:* 10 |break| maximum number of iterations over fields

.. _poromultiphasedynamic_solve_structure:

**SOLVE_STRUCTURE** | *default:* yes |break| Flag to skip computation of structural field

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _poromultiphasedynamic_coupalgo:

**COUPALGO** | *default:* twoway_partitioned |break| Coupling strategies for poro multiphase solvers

   **Possible values:**

   - twoway_partitioned
   - twoway_monolithic

.. _poromultiphasedynamic_artery_coupling:

**ARTERY_COUPLING** | *default:* No |break| Coupling with 1D blood vessels.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECporomultiphasedynamic_monolithic:

POROMULTIPHASE DYNAMIC/MONOLITHIC
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Parameters for monolithic Poro-Multiphase-Scatra Interaction

::

   ----------------------------------POROMULTIPHASE DYNAMIC/MONOLITHIC

.. _poromultiphasedynamic_monolithic_tolres_global:

**TOLRES_GLOBAL** | *default:* 1e-08 |break| tolerance in the residual norm for the Newton iteration

.. _poromultiphasedynamic_monolithic_tolinc_global:

**TOLINC_GLOBAL** | *default:* 1e-08 |break| tolerance in the increment norm for the Newton iteration

.. _poromultiphasedynamic_monolithic_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| number of linear solver used for poroelasticity problems

.. _poromultiphasedynamic_monolithic_fdcheck:

**FDCHECK** | *default:* none |break| flag for finite difference check: none or global

   **Possible values:**

   - none
   - global

.. _poromultiphasedynamic_monolithic_vectornorm_resf:

**VECTORNORM_RESF** | *default:* L2 |break| type of norm to be applied to residuals

   **Possible values:**

   - L1
   - L1_Scaled
   - L2
   - Rms
   - Inf

.. _poromultiphasedynamic_monolithic_vectornorm_inc:

**VECTORNORM_INC** | *default:* L2 |break| type of norm to be applied to residuals

   **Possible values:**

   - L1
   - L1_Scaled
   - L2
   - Rms
   - Inf

.. _poromultiphasedynamic_monolithic_equilibration:

**EQUILIBRATION** | *default:* none |break| flag for equilibration of global system of equations

   **Possible values:**

   - none
   - rows_full
   - rows_maindiag
   - columns_full
   - columns_maindiag
   - rowsandcolumns_full
   - rowsandcolumns_maindiag

.. _poromultiphasedynamic_monolithic_adaptconv:

**ADAPTCONV** | *default:* yes |break| Switch on adaptive control of linear solver tolerance for nonlinear solution

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _poromultiphasedynamic_monolithic_adaptconv_better:

**ADAPTCONV_BETTER** | *default:* 0.001 |break| The linear solver shall be this much better than the current nonlinear residual in the nonlinear convergence limit

.. _SECporomultiphasedynamic_partitioned:

POROMULTIPHASE DYNAMIC/PARTITIONED
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Parameters for partitioned Poro-Multiphase-Scatra Interaction

::

   ---------------------------------POROMULTIPHASE DYNAMIC/PARTITIONED

.. _poromultiphasedynamic_partitioned_convtol:

**CONVTOL** | *default:* 1e-06 |break| tolerance for convergence check of outer iteration

.. _poromultiphasedynamic_partitioned_relaxation:

**RELAXATION** | *default:* none |break| flag for relaxation of partitioned scheme

   **Possible values:**

   - none
   - Constant
   - Aitken

.. _poromultiphasedynamic_partitioned_startomega:

**STARTOMEGA** | *default:* 1 |break| fixed relaxation parameter

.. _poromultiphasedynamic_partitioned_minomega:

**MINOMEGA** | *default:* 0.1 |break| smallest omega allowed for Aitken relaxation

.. _poromultiphasedynamic_partitioned_maxomega:

**MAXOMEGA** | *default:* 10 |break| largest omega allowed for Aitken relaxation

.. _SECporomultiphasescatradynamic:

POROMULTIPHASESCATRA DYNAMIC
----------------------------

Control paramters for scatra porous multiphase media coupling

::

   ---------------------------------------POROMULTIPHASESCATRA DYNAMIC

.. _poromultiphasescatradynamic_restartevry:

**RESTARTEVRY** | *default:* 1 |break| write restart possibility every RESTARTEVRY steps

.. _poromultiphasescatradynamic_numstep:

**NUMSTEP** | *default:* 200 |break| maximum number of Timesteps

.. _poromultiphasescatradynamic_maxtime:

**MAXTIME** | *default:* 1000 |break| total simulation time

.. _poromultiphasescatradynamic_timestep:

**TIMESTEP** | *default:* 0.05 |break| time step size dt

.. _poromultiphasescatradynamic_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| increment for writing solution

.. _poromultiphasescatradynamic_itemax:

**ITEMAX** | *default:* 10 |break| maximum number of iterations over fields

.. _poromultiphasescatradynamic_itemin:

**ITEMIN** | *default:* 1 |break| minimal number of iterations over fields

.. _poromultiphasescatradynamic_coupalgo:

**COUPALGO** | *default:* twoway_partitioned_nested |break| Coupling strategies for poroscatra solvers

   **Possible values:**

   - twoway_partitioned_nested
   - twoway_partitioned_sequential
   - twoway_monolithic

.. _poromultiphasescatradynamic_artery_coupling:

**ARTERY_COUPLING** | *default:* No |break| Coupling with 1D blood vessels.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _poromultiphasescatradynamic_divercont:

**DIVERCONT** | *default:* stop |break| What to do with time integration when Poromultiphase-Scatra iteration failed

   **Possible values:**

   - stop
   - continue

.. _SECporomultiphasescatradynamic_monolithic:

POROMULTIPHASESCATRA DYNAMIC/MONOLITHIC
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Parameters for monolithic Poro-Multiphase-Scatra Interaction

::

   ----------------------------POROMULTIPHASESCATRA DYNAMIC/MONOLITHIC

.. _poromultiphasescatradynamic_monolithic_vectornorm_resf:

**VECTORNORM_RESF** | *default:* L2 |break| type of norm to be applied to residuals

   **Possible values:**

   - L1
   - L1_Scaled
   - L2
   - Rms
   - Inf

.. _poromultiphasescatradynamic_monolithic_vectornorm_inc:

**VECTORNORM_INC** | *default:* L2 |break| type of norm to be applied to residuals

   **Possible values:**

   - L1
   - L1_Scaled
   - L2
   - Rms
   - Inf

.. _poromultiphasescatradynamic_monolithic_adaptconv:

**ADAPTCONV** | *default:* yes |break| Switch on adaptive control of linear solver tolerance for nonlinear solution

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _poromultiphasescatradynamic_monolithic_adaptconv_better:

**ADAPTCONV_BETTER** | *default:* 0.001 |break| The linear solver shall be this much better than the current nonlinear residual in the nonlinear convergence limit

.. _poromultiphasescatradynamic_monolithic_tolres_global:

**TOLRES_GLOBAL** | *default:* 1e-08 |break| tolerance in the residual norm for the Newton iteration

.. _poromultiphasescatradynamic_monolithic_tolinc_global:

**TOLINC_GLOBAL** | *default:* 1e-08 |break| tolerance in the increment norm for the Newton iteration

.. _poromultiphasescatradynamic_monolithic_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| number of linear solver used for monolithic poroscatra problems

.. _poromultiphasescatradynamic_monolithic_fdcheck:

**FDCHECK** | *default:* none |break| flag for finite difference check: none or global

   **Possible values:**

   - none
   - global

.. _poromultiphasescatradynamic_monolithic_equilibration:

**EQUILIBRATION** | *default:* none |break| flag for equilibration of global system of equations

   **Possible values:**

   - none
   - rows_full
   - rows_maindiag
   - columns_full
   - columns_maindiag
   - rowsandcolumns_full
   - rowsandcolumns_maindiag

.. _SECporomultiphasescatradynamic_partitioned:

POROMULTIPHASESCATRA DYNAMIC/PARTITIONED
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Parameters for partitioned Poro-Multiphase-Scatra Interaction

::

   ---------------------------POROMULTIPHASESCATRA DYNAMIC/PARTITIONED

.. _poromultiphasescatradynamic_partitioned_convtol:

**CONVTOL** | *default:* 1e-06 |break| tolerance for convergence check of outer iteration

.. _SECporofluidmultiphasedynamic:

POROFLUIDMULTIPHASE DYNAMIC
---------------------------

control parameters for porofluidmultiphase problems


::

   ----------------------------------------POROFLUIDMULTIPHASE DYNAMIC

.. _porofluidmultiphasedynamic_maxtime:

**MAXTIME** | *default:* 1000 |break| Total simulation time

.. _porofluidmultiphasedynamic_numstep:

**NUMSTEP** | *default:* 20 |break| Total number of time steps

.. _porofluidmultiphasedynamic_timestep:

**TIMESTEP** | *default:* 0.1 |break| Time increment dt

.. _porofluidmultiphasedynamic_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| Increment for writing solution

.. _porofluidmultiphasedynamic_restartevry:

**RESTARTEVRY** | *default:* 1 |break| Increment for writing restart

.. _porofluidmultiphasedynamic_theta:

**THETA** | *default:* 0.5 |break| One-step-theta time integration factor

.. _porofluidmultiphasedynamic_timeintegr:

**TIMEINTEGR** | *default:* One_Step_Theta |break| Time Integration Scheme

   **Possible values:**

   - One_Step_Theta

.. _porofluidmultiphasedynamic_calcerror:

**CALCERROR** | *default:* No |break| compute error compared to analytical solution

   **Possible values:**

   - No
   - error_by_function

.. _porofluidmultiphasedynamic_calcerrorno:

**CALCERRORNO** | *default:* -1 |break| function number for porofluidmultiphase error computation

.. _porofluidmultiphasedynamic_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| number of linear solver used for the porofluidmultiphase problem

.. _porofluidmultiphasedynamic_itemax:

**ITEMAX** | *default:* 10 |break| max. number of nonlin. iterations

.. _porofluidmultiphasedynamic_abstolres:

**ABSTOLRES** | *default:* 1e-14 |break| Absolute tolerance for deciding if residual of nonlinear problem is already zero

.. _porofluidmultiphasedynamic_adaptconv:

**ADAPTCONV** | *default:* yes |break| Switch on adaptive control of linear solver tolerance for nonlinear solution

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _porofluidmultiphasedynamic_adaptconv_better:

**ADAPTCONV_BETTER** | *default:* 0.1 |break| The linear solver shall be this much better than the current nonlinear residual in the nonlinear convergence limit

.. _porofluidmultiphasedynamic_fdcheck:

**FDCHECK** | *default:* none |break| flag for finite difference check: none, local, or global

   **Possible values:**

   - none
   - global

.. _porofluidmultiphasedynamic_fdcheckeps:

**FDCHECKEPS** | *default:* 1e-06 |break| dof perturbation magnitude for finite difference check (1.e-6 seems to work very well, whereas smaller values don't)

.. _porofluidmultiphasedynamic_fdchecktol:

**FDCHECKTOL** | *default:* 1e-06 |break| relative tolerance for finite difference check

.. _porofluidmultiphasedynamic_skipinitder:

**SKIPINITDER** | *default:* yes |break| Flag to skip computation of initial time derivative

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _porofluidmultiphasedynamic_output_satandpress:

**OUTPUT_SATANDPRESS** | *default:* yes |break| Flag if output of saturations and pressures should be calculated

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _porofluidmultiphasedynamic_output_solidpress:

**OUTPUT_SOLIDPRESS** | *default:* yes |break| Flag if output of solid pressure should be calculated

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _porofluidmultiphasedynamic_output_porosity:

**OUTPUT_POROSITY** | *default:* yes |break| Flag if output of porosity should be calculated

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _porofluidmultiphasedynamic_stab_biot:

**STAB_BIOT** | *default:* No |break| Flag to (de)activate BIOT stabilization.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _porofluidmultiphasedynamic_stab_biot_scaling:

**STAB_BIOT_SCALING** | *default:* 1 |break| Scaling factor for stabilization parameter for biot stabilization of porous flow.

.. _porofluidmultiphasedynamic_vectornorm_resf:

**VECTORNORM_RESF** | *default:* L2 |break| type of norm to be applied to residuals

   **Possible values:**

   - L1
   - L1_Scaled
   - L2
   - Rms
   - Inf

.. _porofluidmultiphasedynamic_vectornorm_inc:

**VECTORNORM_INC** | *default:* L2 |break| type of norm to be applied to residuals

   **Possible values:**

   - L1
   - L1_Scaled
   - L2
   - Rms
   - Inf

.. _porofluidmultiphasedynamic_tolres:

**TOLRES** | *default:* 1e-06 |break| tolerance in the residual norm for the Newton iteration

.. _porofluidmultiphasedynamic_tolinc:

**TOLINC** | *default:* 1e-06 |break| tolerance in the increment norm for the Newton iteration

.. _porofluidmultiphasedynamic_initialfield:

**INITIALFIELD** | *default:* zero_field |break| Initial Field for transport problem

   **Possible values:**

   - zero_field
   - field_by_function
   - field_by_condition

.. _porofluidmultiphasedynamic_initfuncno:

**INITFUNCNO** | *default:* -1 |break| function number for scalar transport initial field

.. _porofluidmultiphasedynamic_divercont:

**DIVERCONT** | *default:* stop |break| What to do with time integration when Newton-Raphson iteration failed

   **Possible values:**

   - stop
   - continue

.. _porofluidmultiphasedynamic_flux_proj_solver:

**FLUX_PROJ_SOLVER** | *default:* -1 |break| Number of linear solver used for L2 projection

.. _porofluidmultiphasedynamic_flux_proj_method:

**FLUX_PROJ_METHOD** | *default:* none |break| Flag to (de)activate flux reconstruction.

   **Possible values:**

   - none
   - L2_projection

.. _porofluidmultiphasedynamic_domainint_funct:

**DOMAININT_FUNCT** | *default:* -1.0 |break| functions used for domain integrals

.. _porofluidmultiphasedynamic_artery_coupling:

**ARTERY_COUPLING** | *default:* No |break| Coupling with 1D blood vessels.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _porofluidmultiphasedynamic_starting_dbc_time_end:

**STARTING_DBC_TIME_END** | *default:* -1 |break| End time for the starting Dirichlet BC.

.. _porofluidmultiphasedynamic_starting_dbc_onoff:

**STARTING_DBC_ONOFF** | *default:* 0 |break| Switching the starting Dirichlet BC on or off.

.. _porofluidmultiphasedynamic_starting_dbc_funct:

**STARTING_DBC_FUNCT** | *default:* 0 |break| Function prescribing the starting Dirichlet BC.

.. _SECporofluidmultiphasedynamic_arterycoupling:

POROFLUIDMULTIPHASE DYNAMIC/ARTERY COUPLING
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Parameters for artery mesh tying

::

   ------------------------POROFLUIDMULTIPHASE DYNAMIC/ARTERY COUPLING

.. _porofluidmultiphasedynamic_arterycoupling_maxnumsegperartele:

**MAXNUMSEGPERARTELE** | *default:* 5 |break| maximum number of segments per artery element for 1D-3D artery coupling

.. _porofluidmultiphasedynamic_arterycoupling_penalty:

**PENALTY** | *default:* 1000 |break| Penalty parameter for line-based coupling

.. _porofluidmultiphasedynamic_arterycoupling_artery_coupling_method:

**ARTERY_COUPLING_METHOD** | *default:* None |break| Coupling method for artery coupling.

   **Possible values:**

   - None
   - Nodal
   - GPTS
   - MP
   - NTP

.. _porofluidmultiphasedynamic_arterycoupling_coupleddofs_art:

**COUPLEDDOFS_ART** | *default:* -1.0 |break| coupled artery dofs for mesh tying

.. _porofluidmultiphasedynamic_arterycoupling_coupleddofs_poro:

**COUPLEDDOFS_PORO** | *default:* -1.0 |break| coupled porofluid dofs for mesh tying

.. _porofluidmultiphasedynamic_arterycoupling_reacfunct_art:

**REACFUNCT_ART** | *default:* -1 |break| functions for coupling (artery part)

.. _porofluidmultiphasedynamic_arterycoupling_scalereac_art:

**SCALEREAC_ART** | *default:* 0 |break| scale for coupling (artery part)

.. _porofluidmultiphasedynamic_arterycoupling_reacfunct_cont:

**REACFUNCT_CONT** | *default:* -1 |break| functions for coupling (porofluid part)

.. _porofluidmultiphasedynamic_arterycoupling_scalereac_cont:

**SCALEREAC_CONT** | *default:* 0 |break| scale for coupling (porofluid part)

.. _porofluidmultiphasedynamic_arterycoupling_evaluate_in_ref_config:

**EVALUATE_IN_REF_CONFIG** | *default:* yes |break| Flag if artery elements are evaluated in reference or current configuration

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _porofluidmultiphasedynamic_arterycoupling_lateral_surface_coupling:

**LATERAL_SURFACE_COUPLING** | *default:* no |break| Flag if 1D-3D coupling should be evaluated on lateral (cylinder) surface of embedded artery elements

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _porofluidmultiphasedynamic_arterycoupling_numpatch_axi:

**NUMPATCH_AXI** | *default:* 1 |break| Number of integration patches per 1D element in axial direction for lateral surface coupling

.. _porofluidmultiphasedynamic_arterycoupling_numpatch_rad:

**NUMPATCH_RAD** | *default:* 1 |break| Number of integration patches per 1D element in radial direction for lateral surface coupling

.. _porofluidmultiphasedynamic_arterycoupling_output_bloodvesselvolfrac:

**OUTPUT_BLOODVESSELVOLFRAC** | *default:* no |break| Flag if output of blood vessel volume fraction should be calculated

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _porofluidmultiphasedynamic_arterycoupling_print_out_summary_pairs:

**PRINT_OUT_SUMMARY_PAIRS** | *default:* no |break| Flag if summary of coupling-pairs should be printed

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _porofluidmultiphasedynamic_arterycoupling_delete_free_hanging_eles:

**DELETE_FREE_HANGING_ELES** | *default:* no |break| Flag if free-hanging elements (after blood vessel collapse) should be deleted

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _porofluidmultiphasedynamic_arterycoupling_delete_small_free_hanging_comps:

**DELETE_SMALL_FREE_HANGING_COMPS** | *default:* -1 |break| Small connected components whose size is smaller than this fraction of the overall network size are additionally deleted (a valid choice of this parameter should lie between 0 and 1)

.. _SECelastohydrodynamic:

ELASTO HYDRO DYNAMIC
--------------------

Elastohydrodynamic paramters for elastohydrodynamic lubrication (lubrication structure interaction)

::

   -----------------------------------------------ELASTO HYDRO DYNAMIC

.. _elastohydrodynamic_restartevrytime:

**RESTARTEVRYTIME** | *default:* 0 |break| write restart possibility every RESTARTEVRY steps

.. _elastohydrodynamic_restartevry:

**RESTARTEVRY** | *default:* 1 |break| write restart possibility every RESTARTEVRY steps

.. _elastohydrodynamic_numstep:

**NUMSTEP** | *default:* 200 |break| maximum number of Timesteps

.. _elastohydrodynamic_maxtime:

**MAXTIME** | *default:* 1000 |break| total simulation time

.. _elastohydrodynamic_timestep:

**TIMESTEP** | *default:* -1 |break| time step size dt

.. _elastohydrodynamic_difftimestepsize:

**DIFFTIMESTEPSIZE** | *default:* No |break| use different step size for lubrication and solid

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _elastohydrodynamic_resultsevrytime:

**RESULTSEVRYTIME** | *default:* 0 |break| increment for writing solution

.. _elastohydrodynamic_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| increment for writing solution

.. _elastohydrodynamic_itemax:

**ITEMAX** | *default:* 10 |break| maximum number of iterations over fields

.. _elastohydrodynamic_itemin:

**ITEMIN** | *default:* 1 |break| minimal number of iterations over fields

.. _elastohydrodynamic_fieldcoupling:

**FIELDCOUPLING** | *default:* none |break| Type of coupling strategy between fields

   **Possible values:**

   - none
   - matching

.. _elastohydrodynamic_coupalgo:

**COUPALGO** | *default:* ehl_Monolithic |break| Coupling strategies for EHL solvers

   **Possible values:**

   - ehl_IterStagg
   - ehl_Monolithic

.. _elastohydrodynamic_unproj_zero_dbc:

**UNPROJ_ZERO_DBC** | *default:* No |break| set unprojectable nodes to zero pressure via Dirichlet condition

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _elastohydrodynamic_dry_contact_model:

**DRY_CONTACT_MODEL** | *default:* No |break| set unprojectable nodes to zero pressure via Dirichlet condition

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECelastohydrodynamic_monolithic:

ELASTO HYDRO DYNAMIC/MONOLITHIC
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Monolithic Thermo Structure Interaction
Dynamic section for monolithic EHL

::

   ------------------------------------ELASTO HYDRO DYNAMIC/MONOLITHIC

.. _elastohydrodynamic_monolithic_convtol:

**CONVTOL** | *default:* 1e-06 |break| tolerance for convergence check of EHL

.. _elastohydrodynamic_monolithic_tolinc:

**TOLINC** | *default:* 1e-06 |break| tolerance for convergence check of EHL-increment in monolithic EHL

.. _elastohydrodynamic_monolithic_norm_resf:

**NORM_RESF** | *default:* Abs |break| type of norm for residual convergence check

   **Possible values:**

   - Abs
   - Rel
   - Mix

.. _elastohydrodynamic_monolithic_norm_inc:

**NORM_INC** | *default:* Abs |break| type of norm for convergence check of primary variables in EHL

   **Possible values:**

   - Abs
   - Rel
   - Mix

.. _elastohydrodynamic_monolithic_normcombi_resfinc:

**NORMCOMBI_RESFINC** | *default:* Coupl_And_Singl |break| binary operator to combine primary variables and residual force values

   **Possible values:**

   - And
   - Or
   - Coupl_Or_Singl
   - Coupl_And_Singl
   - And_Singl
   - Or_Singl

.. _elastohydrodynamic_monolithic_iternorm:

**ITERNORM** | *default:* Rms |break| type of norm to be applied to residuals

   **Possible values:**

   - L1
   - L1_Scaled
   - L2
   - Rms
   - Inf

.. _elastohydrodynamic_monolithic_nlnsol:

**NLNSOL** | *default:* fullnewton |break| Nonlinear solution technique

   **Possible values:**

   - fullnewton

.. _elastohydrodynamic_monolithic_ptcdt:

**PTCDT** | *default:* 0.1 |break| pseudo time step for pseudo-transient continuation (PTC) stabilised Newton procedure

.. _elastohydrodynamic_monolithic_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| number of linear solver used for monolithic EHL problems

.. _elastohydrodynamic_monolithic_adaptconv:

**ADAPTCONV** | *default:* No |break| Switch on adaptive control of linear solver tolerance for nonlinear solution

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _elastohydrodynamic_monolithic_adaptconv_better:

**ADAPTCONV_BETTER** | *default:* 0.1 |break| The linear solver shall be this much better than the current nonlinear residual in the nonlinear convergence limit

.. _elastohydrodynamic_monolithic_infnormscaling:

**INFNORMSCALING** | *default:* yes |break| Scale blocks of matrix with row infnorm?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _elastohydrodynamic_monolithic_merge_ehl_block_matrix:

**MERGE_EHL_BLOCK_MATRIX** | *default:* No |break| Merge EHL block matrix

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECelastohydrodynamic_partitioned:

ELASTO HYDRO DYNAMIC/PARTITIONED
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Partitioned Structure Scalar Interaction
Control section for partitioned EHL

::

   -----------------------------------ELASTO HYDRO DYNAMIC/PARTITIONED

.. _elastohydrodynamic_partitioned_maxomega:

**MAXOMEGA** | *default:* 10 |break| largest omega allowed for Aitken relaxation

.. _elastohydrodynamic_partitioned_minomega:

**MINOMEGA** | *default:* 0.1 |break| smallest omega allowed for Aitken relaxation

.. _elastohydrodynamic_partitioned_startomega:

**STARTOMEGA** | *default:* 1 |break| fixed relaxation parameter

.. _elastohydrodynamic_partitioned_convtol:

**CONVTOL** | *default:* 1e-06 |break| tolerance for convergence check of outer iteration within partitioned EHL

.. _SECssicontrol:

SSI CONTROL
-----------

Control paramters for scatra structure interaction

::

   --------------------------------------------------------SSI CONTROL

.. _ssicontrol_restartevrytime:

**RESTARTEVRYTIME** | *default:* 0 |break| write restart possibility every RESTARTEVRY steps

.. _ssicontrol_restartevry:

**RESTARTEVRY** | *default:* 1 |break| write restart possibility every RESTARTEVRY steps

.. _ssicontrol_numstep:

**NUMSTEP** | *default:* 200 |break| maximum number of Timesteps

.. _ssicontrol_maxtime:

**MAXTIME** | *default:* 1000 |break| total simulation time

.. _ssicontrol_timestep:

**TIMESTEP** | *default:* -1 |break| time step size dt

.. _ssicontrol_difftimestepsize:

**DIFFTIMESTEPSIZE** | *default:* No |break| use different step size for scatra and solid

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _ssicontrol_resultsevrytime:

**RESULTSEVRYTIME** | *default:* 0 |break| increment for writing solution

.. _ssicontrol_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| increment for writing solution

.. _ssicontrol_itemax:

**ITEMAX** | *default:* 10 |break| maximum number of iterations over fields

.. _ssicontrol_scatra_from_restart_file:

**SCATRA_FROM_RESTART_FILE** | *default:* No |break| read scatra result from restart files (use option 'restartfromfile' during execution of baci)

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _ssicontrol_scatra_filename:

**SCATRA_FILENAME** | *default:* nil |break| Control-file name for reading scatra results in SSI

.. _ssicontrol_fieldcoupling:

**FIELDCOUPLING** | *default:* volume_matching |break| Type of coupling strategy between fields

   **Possible values:**

   - volume_matching
   - volume_nonmatching
   - boundary_nonmatching
   - volumeboundary_matching

.. _ssicontrol_coupalgo:

**COUPALGO** | *default:* ssi_IterStagg |break| Coupling strategies for SSI solvers

   **Possible values:**

   - ssi_OneWay_ScatraToSolid
   - ssi_OneWay_SolidToScatra
   - ssi_IterStagg
   - ssi_IterStaggFixedRel_ScatraToSolid
   - ssi_IterStaggFixedRel_SolidToScatra
   - ssi_IterStaggAitken_ScatraToSolid
   - ssi_IterStaggAitken_SolidToScatra
   - ssi_Monolithic

.. _ssicontrol_scatratiminttype:

**SCATRATIMINTTYPE** | *default:* Standard |break| scalar transport time integration type is needed to instantiate correct scalar transport time integration scheme for ssi problems

   **Possible values:**

   - Standard
   - Cardiac_Monodomain
   - Elch

.. _ssicontrol_restart_from_structure:

**RESTART_FROM_STRUCTURE** | *default:* no |break| restart from structure problem (e.g. from prestress calculations) instead of ssi

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _ssicontrol_adaptive_timestepping:

**ADAPTIVE_TIMESTEPPING** | *default:* no |break| flag for adaptive time stepping

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _ssicontrol_redistribute_solid:

**REDISTRIBUTE_SOLID** | *default:* No |break| redistribution by binning of solid mechanics discretization

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECssicontrol_partitioned:

SSI CONTROL/PARTITIONED
~~~~~~~~~~~~~~~~~~~~~~~

Partitioned Structure Scalar Interaction
Control section for partitioned SSI

::

   --------------------------------------------SSI CONTROL/PARTITIONED

.. _ssicontrol_partitioned_maxomega:

**MAXOMEGA** | *default:* 10 |break| largest omega allowed for Aitken relaxation

.. _ssicontrol_partitioned_minomega:

**MINOMEGA** | *default:* 0.1 |break| smallest omega allowed for Aitken relaxation

.. _ssicontrol_partitioned_startomega:

**STARTOMEGA** | *default:* 1 |break| fixed relaxation parameter

.. _ssicontrol_partitioned_convtol:

**CONVTOL** | *default:* 1e-06 |break| tolerance for convergence check of outer iteration within partitioned SSI

.. _SECssicontrol_monolithic:

SSI CONTROL/MONOLITHIC
~~~~~~~~~~~~~~~~~~~~~~

Monolithic Structure Scalar Interaction
Control section for monolithic SSI

::

   ---------------------------------------------SSI CONTROL/MONOLITHIC

.. _ssicontrol_monolithic_abstolres:

**ABSTOLRES** | *default:* 1e-14 |break| absolute tolerance for deciding if global residual of nonlinear problem is already zero

.. _ssicontrol_monolithic_convtol:

**CONVTOL** | *default:* 1e-06 |break| tolerance for convergence check of Newton-Raphson iteration within monolithic SSI

.. _ssicontrol_monolithic_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| ID of linear solver for global system of equations

.. _ssicontrol_monolithic_matrixtype:

**MATRIXTYPE** | *default:* undefined |break| type of global system matrix in global system of equations

   **Possible values:**

   - undefined
   - block
   - sparse

.. _ssicontrol_monolithic_equilibration:

**EQUILIBRATION** | *default:* none |break| flag for equilibration of global system of equations

   **Possible values:**

   - none
   - rows_full
   - rows_maindiag
   - columns_full
   - columns_maindiag
   - rowsandcolumns_full
   - rowsandcolumns_maindiag
   - local

.. _ssicontrol_monolithic_equilibration_structure:

**EQUILIBRATION_STRUCTURE** | *default:* none |break| flag for equilibration of structural equations

   **Possible values:**

   - none
   - rows_maindiag
   - columns_maindiag
   - rowsandcolumns_maindiag
   - symmetry

.. _ssicontrol_monolithic_equilibration_scatra:

**EQUILIBRATION_SCATRA** | *default:* none |break| flag for equilibration of scatra equations

   **Possible values:**

   - none
   - rows_maindiag
   - columns_maindiag
   - rowsandcolumns_maindiag
   - symmetry

.. _ssicontrol_monolithic_print_mat_rhs_map_matlab:

**PRINT_MAT_RHS_MAP_MATLAB** | *default:* no |break| print system matrix, rhs vector, and full map to matlab readable file after solution of time step

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECssicontrol_manifold:

SSI CONTROL/MANIFOLD
~~~~~~~~~~~~~~~~~~~~

Monolithic Structure Scalar Interaction with additional scalar transport on manifold

::

   -----------------------------------------------SSI CONTROL/MANIFOLD

.. _ssicontrol_manifold_add_manifold:

**ADD_MANIFOLD** | *default:* no |break| activate additional manifold?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _ssicontrol_manifold_meshtying_manifold:

**MESHTYING_MANIFOLD** | *default:* no |break| activate meshtying betweeen all manifold fields in case they intersect?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _ssicontrol_manifold_initialfield:

**INITIALFIELD** | *default:* zero_field |break| Initial field for scalar transport on manifold

   **Possible values:**

   - zero_field
   - field_by_function
   - field_by_condition

.. _ssicontrol_manifold_initfuncno:

**INITFUNCNO** | *default:* -1 |break| function number for scalar transport on manifold initial field

.. _ssicontrol_manifold_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| linear solver for scalar transport on manifold

.. _ssicontrol_manifold_output_inflow:

**OUTPUT_INFLOW** | *default:* no |break| write output of inflow of scatra manifold - scatra coupling into scatra manifold to csv file

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECssicontrol_elch:

SSI CONTROL/ELCH
~~~~~~~~~~~~~~~~

Monolithic Structure Scalar Interaction with Elch as SCATRATIMINTTYPE

::

   ---------------------------------------------------SSI CONTROL/ELCH

.. _ssicontrol_elch_initpotcalc:

**INITPOTCALC** | *default:* No |break| Automatically calculate initial field for electric potential

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECssticontrol:

SSTI CONTROL
------------

Control paramters for scatra structure thermo interaction

::

   -------------------------------------------------------SSTI CONTROL

.. _ssticontrol_restartevrytime:

**RESTARTEVRYTIME** | *default:* 0 |break| write restart possibility every RESTARTEVRY steps

.. _ssticontrol_restartevry:

**RESTARTEVRY** | *default:* 1 |break| write restart possibility every RESTARTEVRY steps

.. _ssticontrol_numstep:

**NUMSTEP** | *default:* 200 |break| maximum number of Timesteps

.. _ssticontrol_maxtime:

**MAXTIME** | *default:* 1000 |break| total simulation time

.. _ssticontrol_timestep:

**TIMESTEP** | *default:* -1 |break| time step size dt

.. _ssticontrol_resultsevrytime:

**RESULTSEVRYTIME** | *default:* 0 |break| increment for writing solution

.. _ssticontrol_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| increment for writing solution

.. _ssticontrol_itemax:

**ITEMAX** | *default:* 10 |break| maximum number of iterations over fields

.. _ssticontrol_scatra_from_restart_file:

**SCATRA_FROM_RESTART_FILE** | *default:* No |break| read scatra result from restart files (use option 'restartfromfile' during execution of baci)

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _ssticontrol_scatra_filename:

**SCATRA_FILENAME** | *default:* nil |break| Control-file name for reading scatra results in SSTI

.. _ssticontrol_coupalgo:

**COUPALGO** | *default:* ssti_Monolithic |break| Coupling strategies for SSTI solvers

   **Possible values:**

   - ssti_Monolithic

.. _ssticontrol_scatratiminttype:

**SCATRATIMINTTYPE** | *default:* Elch |break| scalar transport time integration type is needed to instantiate correct scalar transport time integration scheme for ssi problems

   **Possible values:**

   - Elch

.. _ssticontrol_adaptive_timestepping:

**ADAPTIVE_TIMESTEPPING** | *default:* no |break| flag for adaptive time stepping

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECssticontrol_monolithic:

SSTI CONTROL/MONOLITHIC
~~~~~~~~~~~~~~~~~~~~~~~

Monolithic Structure Scalar Interaction
Control section for monolithic SSI

::

   --------------------------------------------SSTI CONTROL/MONOLITHIC

.. _ssticontrol_monolithic_abstolres:

**ABSTOLRES** | *default:* 1e-14 |break| absolute tolerance for deciding if global residual of nonlinear problem is already zero

.. _ssticontrol_monolithic_convtol:

**CONVTOL** | *default:* 1e-06 |break| tolerance for convergence check of Newton-Raphson iteration within monolithic SSI

.. _ssticontrol_monolithic_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| ID of linear solver for global system of equations

.. _ssticontrol_monolithic_matrixtype:

**MATRIXTYPE** | *default:* undefined |break| type of global system matrix in global system of equations

   **Possible values:**

   - undefined
   - block
   - sparse

.. _ssticontrol_monolithic_equilibration:

**EQUILIBRATION** | *default:* none |break| flag for equilibration of global system of equations

   **Possible values:**

   - none
   - rows_full
   - rows_maindiag
   - rowsandcolumns_full
   - rowsandcolumns_maindiag
   - local

.. _ssticontrol_monolithic_equilibration_structure:

**EQUILIBRATION_STRUCTURE** | *default:* none |break| flag for equilibration of structural equations

   **Possible values:**

   - none
   - rows_maindiag
   - columns_maindiag
   - rowsandcolumns_maindiag
   - symmetry

.. _ssticontrol_monolithic_equilibration_scatra:

**EQUILIBRATION_SCATRA** | *default:* none |break| flag for equilibration of scatra equations

   **Possible values:**

   - none
   - rows_maindiag
   - columns_maindiag
   - rowsandcolumns_maindiag
   - symmetry

.. _ssticontrol_monolithic_equilibration_thermo:

**EQUILIBRATION_THERMO** | *default:* none |break| flag for equilibration of scatra equations

   **Possible values:**

   - none
   - rows_maindiag
   - columns_maindiag
   - rowsandcolumns_maindiag
   - symmetry

.. _ssticontrol_monolithic_equilibration_init_scatra:

**EQUILIBRATION_INIT_SCATRA** | *default:* no |break| use equilibration method of ScaTra to equilibrate initial calculation of potential

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECssticontrol_thermo:

SSTI CONTROL/THERMO
~~~~~~~~~~~~~~~~~~~

Parameters for thermo subproblem

::

   ------------------------------------------------SSTI CONTROL/THERMO

.. _ssticontrol_thermo_initthermofunct:

**INITTHERMOFUNCT** | *default:* -1 |break| initial function for thermo field

.. _ssticontrol_thermo_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| linear solver for thermo field

.. _ssticontrol_thermo_initialfield:

**INITIALFIELD** | *default:* field_by_function |break| defines, how to set the initial field

   **Possible values:**

   - field_by_function
   - field_by_condition

.. _SECaledynamic:

ALE DYNAMIC
-----------

no description yet

::

   --------------------------------------------------------ALE DYNAMIC

.. _aledynamic_timestep:

**TIMESTEP** | *default:* 0.1 |break| time step size

.. _aledynamic_numstep:

**NUMSTEP** | *default:* 41 |break| max number of time steps

.. _aledynamic_maxtime:

**MAXTIME** | *default:* 4 |break| max simulation time

.. _aledynamic_ale_type:

**ALE_TYPE** | *default:* solid |break| ale mesh movement algorithm

   **Possible values:**

   - solid
   - solid_linear
   - laplace_material
   - laplace_spatial
   - springs_material
   - springs_spatial

.. _aledynamic_assessmeshquality:

**ASSESSMESHQUALITY** | *default:* no |break| Evaluate element quality measure according to [Oddy et al. 1988]

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _aledynamic_updatematrix:

**UPDATEMATRIX** | *default:* no |break| Update stiffness matrix in every time step (only for linear/material strategies)

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _aledynamic_maxiter:

**MAXITER** | *default:* 1 |break| Maximum number of newton iterations.

.. _aledynamic_tolres:

**TOLRES** | *default:* 1e-06 |break| Absolute tolerance for length scaled L2 residual norm 

.. _aledynamic_toldisp:

**TOLDISP** | *default:* 1e-06 |break| Absolute tolerance for length scaled L2 increment norm 

.. _aledynamic_num_initstep:

**NUM_INITSTEP** | *default:* 0 |break| no description yet

.. _aledynamic_restartevry:

**RESTARTEVRY** | *default:* 1 |break| write restart data every RESTARTEVRY steps

.. _aledynamic_resultsevry:

**RESULTSEVRY** | *default:* 0 |break| write results every RESULTSTEVRY steps

.. _aledynamic_divercont:

**DIVERCONT** | *default:* continue |break| What to do if nonlinear solver does not converge?

   **Possible values:**

   - stop
   - continue

.. _aledynamic_meshtying:

**MESHTYING** | *default:* no |break| Flag to (de)activate mesh tying and mesh sliding algorithm

   **Possible values:**

   - no
   - meshtying
   - meshsliding

.. _aledynamic_initialdisp:

**INITIALDISP** | *default:* zero_displacement |break| Initial displacement for structure problem

   **Possible values:**

   - zero_displacement
   - displacement_by_function

.. _aledynamic_startfuncno:

**STARTFUNCNO** | *default:* -1 |break| Function for Initial displacement

.. _aledynamic_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| number of linear solver used for ale problems...

.. _SECfsidynamic:

FSI DYNAMIC
-----------

Fluid Structure Interaction
FSI solver with various coupling methods

::

   --------------------------------------------------------FSI DYNAMIC

.. _fsidynamic_coupalgo:

**COUPALGO** | *default:* iter_stagg_AITKEN_rel_param |break| Iteration Scheme over the fields

   **Possible values:**

   - basic_sequ_stagg
   - iter_stagg_fixed_rel_param
   - iter_stagg_AITKEN_rel_param
   - iter_stagg_steep_desc
   - iter_stagg_NLCG
   - iter_stagg_MFNK_FD
   - iter_stagg_MFNK_FSI
   - iter_stagg_MPE
   - iter_stagg_RRE
   - iter_monolithicfluidsplit
   - iter_monolithicstructuresplit
   - iter_lung_monolithicstructuresplit
   - iter_lung_monolithicfluidsplit
   - iter_xfem_monolithic
   - pseudo_structure
   - iter_constr_monolithicfluidsplit
   - iter_constr_monolithicstructuresplit
   - iter_mortar_monolithicstructuresplit
   - iter_mortar_monolithicfluidsplit
   - iter_fluidfluid_monolithicstructuresplit
   - iter_fluidfluid_monolithicfluidsplit
   - iter_fluidfluid_monolithicstructuresplit_nonox
   - iter_fluidfluid_monolithicfluidsplit_nonox
   - iter_sliding_monolithicfluidsplit
   - iter_sliding_monolithicstructuresplit

.. _fsidynamic_debugoutput:

**DEBUGOUTPUT** | *default:* No |break| Output of unconverged interface values during FSI iteration.
There will be a new control file for each time step.
This might be helpful to understand the coupling iteration.

   **Possible values:**

   - No
   - Yes
   - no
   - yes
   - NO
   - YES
   - Interface
   - Preconditioner
   - All

.. _fsidynamic_matchgrid_fluidale:

**MATCHGRID_FLUIDALE** | *default:* Yes |break| is matching grid (fluid-ale)

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fsidynamic_matchgrid_structale:

**MATCHGRID_STRUCTALE** | *default:* Yes |break| is matching grid (structure-ale)

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fsidynamic_matchall:

**MATCHALL** | *default:* Yes |break| is matching grid (fluid-ale) and is full fluid-ale (without euler part)

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fsidynamic_maxtime:

**MAXTIME** | *default:* 1000 |break| Total simulation time

.. _fsidynamic_numstep:

**NUMSTEP** | *default:* 200 |break| Total number of Timesteps

.. _fsidynamic_restartevry:

**RESTARTEVRY** | *default:* 1 |break| Increment for writing restart

.. _fsidynamic_restart_from_part_fsi:

**RESTART_FROM_PART_FSI** | *default:* No |break| restart from partitioned fsi (e.g. from prestress calculations) instead of monolithic fsi

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fsidynamic_secondorder:

**SECONDORDER** | *default:* No |break| Second order displacement-velocity conversion at the interface.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fsidynamic_slidealeproj:

**SLIDEALEPROJ** | *default:* None |break| Projection method to use for sliding FSI.

   **Possible values:**

   - None
   - Curr
   - Ref
   - RotZ
   - RotZSphere

.. _fsidynamic_timestep:

**TIMESTEP** | *default:* 0.1 |break| Time increment dt

.. _fsidynamic_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| Increment for writing solution

.. _fsidynamic_verbosity:

**VERBOSITY** | *default:* full |break| Verbosity of the FSI problem.

   **Possible values:**

   - full
   - medium
   - low
   - subproblem

.. _SECfsidynamic_timeadaptivity:

FSI DYNAMIC/TIMEADAPTIVITY
~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   -----------------------------------------FSI DYNAMIC/TIMEADAPTIVITY

.. _fsidynamic_timeadaptivity_adaptstepmax:

**ADAPTSTEPMAX** | *default:* 5 |break| Maximum number of repetitions of one time step for adapting/reducing the time step size (>0)

.. _fsidynamic_timeadaptivity_auxintegratorfluid:

**AUXINTEGRATORFLUID** | *default:* AB2 |break| Method for error estimation in the fluid field

   **Possible values:**

   - None
   - ExplicitEuler
   - AB2

.. _fsidynamic_timeadaptivity_averagingdt:

**AVERAGINGDT** | *default:* 0.3 0.7 |break| Averaging of time step sizes in case of increasing time step size.
Parameters are ordered from most recent weight to the most historic one.
Number of parameters determines the number of previous time steps that are involved
in the averaging procedure.

.. _fsidynamic_timeadaptivity_divercont:

**DIVERCONT** | *default:* stop |break| What to do if nonlinear solver does not converge?

   **Possible values:**

   - stop
   - continue
   - halve_step
   - revert_dt

.. _fsidynamic_timeadaptivity_dtmax:

**DTMAX** | *default:* 0.1 |break| Limit maximally permitted time step size (>0)

.. _fsidynamic_timeadaptivity_dtmin:

**DTMIN** | *default:* 0.0001 |break| Limit minimally allowed time step size (>0)

.. _fsidynamic_timeadaptivity_locerrtolfluid:

**LOCERRTOLFLUID** | *default:* 0.001 |break| Tolerance for the norm of local velocity error

.. _fsidynamic_timeadaptivity_numincreasesteps:

**NUMINCREASESTEPS** | *default:* 0 |break| Number of consecutive steps that want to increase time step size before
actually increasing it. Set 0 to deactivate this feature.

.. _fsidynamic_timeadaptivity_safetyfactor:

**SAFETYFACTOR** | *default:* 0.9 |break| This is a safety factor to scale theoretical optimal step size, 
should be lower than 1 and must be larger than 0

.. _fsidynamic_timeadaptivity_sizeratiomax:

**SIZERATIOMAX** | *default:* 2 |break| Limit maximally permitted change of
time step size compared to previous size (>0).

.. _fsidynamic_timeadaptivity_sizeratiomin:

**SIZERATIOMIN** | *default:* 0.5 |break| Limit minimally permitted change of
time step size compared to previous size (>0).

.. _fsidynamic_timeadaptivity_timeadapton:

**TIMEADAPTON** | *default:* No |break| Activate or deactivate time step size adaptivity

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECfsidynamic_monolithicsolver:

FSI DYNAMIC/MONOLITHIC SOLVER
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   --------------------------------------FSI DYNAMIC/MONOLITHIC SOLVER

.. _fsidynamic_monolithicsolver_adaptivedist:

**ADAPTIVEDIST** | *default:* 0 |break| Required distance for adaptive convergence check in Newton-type FSI.
This is the improvement we want to achieve in the linear extrapolation of the
adaptive convergence check. Set to zero to avoid the adaptive check altogether.

.. _fsidynamic_monolithicsolver_basetol:

**BASETOL** | *default:* 0.001 |break| Basic tolerance for adaptive convergence check in monolithic FSI.
This tolerance will be used for the linear solve of the FSI block system.
The linear convergence test will always use the relative residual norm (AZ_r0).
Not to be confused with the Newton tolerance (CONVTOL) that applies
to the nonlinear convergence test using a absolute residual norm.

.. _fsidynamic_monolithicsolver_convtol:

**CONVTOL** | *default:* 1e-06 |break| Nonlinear tolerance for lung/constraint/fluid-fluid FSI

.. _fsidynamic_monolithicsolver_energyfile:

**ENERGYFILE** | *default:* No |break| Write artificial interface energy due to temporal discretization to file

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fsidynamic_monolithicsolver_fsiamganalyze:

**FSIAMGANALYZE** | *default:* No |break| run analysis on fsiamg multigrid scheme

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fsidynamic_monolithicsolver_hybrid_as_type:

**HYBRID_AS_TYPE** | *default:* ILU |break| Type of inverse in additive part of hybrid preconditioner.

   **Possible values:**

   - ILU
   - Amesos_LU

.. _fsidynamic_monolithicsolver_hybrid_fill_level:

**HYBRID_FILL_LEVEL** | *default:* 0 |break| Level of fill of the hybrid ILU preconditioner.

.. _fsidynamic_monolithicsolver_hybridfull:

**HYBRIDFULL** | *default:* yes |break| Apply Additive Schwarz Preconditioner on all procs (yes)
or on interface procs only (no)

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fsidynamic_monolithicsolver_hybrid_imbalance_tol:

**HYBRID_IMBALANCE_TOL** | *default:* 1.1 |break| Relative imbalance of sudomain size for repartitioning in hybrid preconditioner

.. _fsidynamic_monolithicsolver_infnormscaling:

**INFNORMSCALING** | *default:* Yes |break| Scale Blocks with row infnorm?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fsidynamic_monolithicsolver_innerprec:

**INNERPREC** | *default:* PreconditionedKrylov |break| Inner preconditioner used in a hybrid Schwarz setting.

   **Possible values:**

   - PreconditionedKrylov
   - FSIAMG

.. _fsidynamic_monolithicsolver_itemax:

**ITEMAX** | *default:* 100 |break| Maximum allowed number of nonlinear iterations

.. _fsidynamic_monolithicsolver_krylov_itemax:

**KRYLOV_ITEMAX** | *default:* 1000 |break| Max Iterations for linear solver.

.. _fsidynamic_monolithicsolver_krylov_size:

**KRYLOV_SIZE** | *default:* 50 |break| Size of Krylov Subspace.

.. _fsidynamic_monolithicsolver_linearblocksolver:

**LINEARBLOCKSOLVER** | *default:* PreconditionedKrylov |break| Linear block preconditioner for block system in monolithic FSI.

   **Possible values:**

   - PreconditionedKrylov
   - FSIAMG
   - AMGnxn
   - HybridSchwarz
   - LinalgSolver

.. _fsidynamic_monolithicsolver_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| Number of SOLVER block describing the linear solver and preconditioner

.. _fsidynamic_monolithicsolver_norm_inc:

**NORM_INC** | *default:* Rel |break| type of norm for primary variables convergence check

   **Possible values:**

   - Abs
   - Rel
   - Mix

.. _fsidynamic_monolithicsolver_norm_resf:

**NORM_RESF** | *default:* Rel |break| type of norm for residual convergence check

   **Possible values:**

   - Abs
   - Rel
   - Mix

.. _fsidynamic_monolithicsolver_normcombi_resfinc:

**NORMCOMBI_RESFINC** | *default:* And |break| binary operator to combine primary variables and residual force values

   **Possible values:**

   - And

.. _fsidynamic_monolithicsolver_precondreuse:

**PRECONDREUSE** | *default:* 0 |break| Number of iterations in one time step reusing the preconditioner before rebuilding it

.. _fsidynamic_monolithicsolver_rebuildpreceverystep:

**REBUILDPRECEVERYSTEP** | *default:* Yes |break| Enforce rebuilding the preconditioner at the beginning of every time step

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fsidynamic_monolithicsolver_redistribute:

**REDISTRIBUTE** | *default:* off |break| Redistribute domain decomposition.

   **Possible values:**

   - off
   - structure
   - fluid
   - both
   - monolithic

.. _fsidynamic_monolithicsolver_redist_weight1:

**REDIST_WEIGHT1** | *default:* 1 |break| Weight for redistribution, general domain.

.. _fsidynamic_monolithicsolver_redist_weight2:

**REDIST_WEIGHT2** | *default:* 50000 |break| Weight for redistribution, interface domain.

.. _fsidynamic_monolithicsolver_redist_secondweight1:

**REDIST_SECONDWEIGHT1** | *default:* -1 |break| Weight for 2nd redistribution, general domain.

.. _fsidynamic_monolithicsolver_redist_secondweight2:

**REDIST_SECONDWEIGHT2** | *default:* -1 |break| Weight for 2nd redistribution, interface domain.

.. _fsidynamic_monolithicsolver_shapederivatives:

**SHAPEDERIVATIVES** | *default:* No |break| Include linearization with respect to mesh movement in Navier Stokes equation.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fsidynamic_monolithicsolver_symmetricprecond:

**SYMMETRICPRECOND** | *default:* No |break| Symmetric block GS preconditioner or ordinary GS

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fsidynamic_monolithicsolver_alepcomega:

**ALEPCOMEGA** | *default:* 1.0 1.0 1.0 1.0 |break| Relaxation factor for Richardson iteration on ale block in MFSI block preconditioner
FSIAMG: each number belongs to a level
PreconditiondKrylov: only first number is used for finest level

.. _fsidynamic_monolithicsolver_alepciter:

**ALEPCITER** | *default:* 1 1 1 1 |break| Number of Richardson iterations on ale block in MFSI block preconditioner
FSIAMG: each number belongs to a level
PreconditiondKrylov: only first number is used for finest level

.. _fsidynamic_monolithicsolver_fluidpcomega:

**FLUIDPCOMEGA** | *default:* 1.0 1.0 1.0 1.0 |break| Relaxation factor for Richardson iteration on fluid block in MFSI block preconditioner
FSIAMG: each number belongs to a level
PreconditiondKrylov: only first number is used for finest level

.. _fsidynamic_monolithicsolver_fluidpciter:

**FLUIDPCITER** | *default:* 1 1 1 1 |break| Number of Richardson iterations on fluid block in MFSI block preconditioner
FSIAMG: each number belongs to a level
PreconditiondKrylov: only first number is used for finest level

.. _fsidynamic_monolithicsolver_structpcomega:

**STRUCTPCOMEGA** | *default:* 1.0 1.0 1.0 1.0 |break| Relaxation factor for Richardson iteration on structural block in MFSI block preconditioner
FSIAMG: each number belongs to a level
PreconditiondKrylov: only first number is used for finest level

.. _fsidynamic_monolithicsolver_structpciter:

**STRUCTPCITER** | *default:* 1 1 1 1 |break| Number of Richardson iterations on structural block in MFSI block preconditioner
FSIAMG: each number belongs to a level
PreconditiondKrylov: only first number is used for finest level

.. _fsidynamic_monolithicsolver_pcomega:

**PCOMEGA** | *default:* 1.0 1.0 1.0 |break| Relaxation factor for Richardson iteration on whole MFSI block preconditioner
FSIAMG: each number belongs to a level
PreconditiondKrylov: only first number is used for finest level

.. _fsidynamic_monolithicsolver_pciter:

**PCITER** | *default:* 1 1 1 |break| Number of Richardson iterations on whole MFSI block preconditioner
FSIAMG: each number belongs to a level
PreconditiondKrylov: only first number is used for finest level

.. _fsidynamic_monolithicsolver_blocksmoother:

**BLOCKSMOOTHER** | *default:* BGS BGS BGS |break| Type of block smoother, can be BGS or Schur

.. _fsidynamic_monolithicsolver_schuromega:

**SCHUROMEGA** | *default:* 0.001 0.01 0.1 |break| Damping factor for Schur complement construction

.. _fsidynamic_monolithicsolver_tol_dis_res_l2:

**TOL_DIS_RES_L2** | *default:* 1e-06 |break| Absolute tolerance for structure displacement residual in L2-norm

.. _fsidynamic_monolithicsolver_tol_dis_res_inf:

**TOL_DIS_RES_INF** | *default:* 1e-06 |break| Absolute tolerance for structure displacement residual in Inf-norm

.. _fsidynamic_monolithicsolver_tol_dis_inc_l2:

**TOL_DIS_INC_L2** | *default:* 1e-06 |break| Absolute tolerance for structure displacement increment in L2-norm

.. _fsidynamic_monolithicsolver_tol_dis_inc_inf:

**TOL_DIS_INC_INF** | *default:* 1e-06 |break| Absolute tolerance for structure displacement increment in Inf-norm

.. _fsidynamic_monolithicsolver_tol_fsi_res_l2:

**TOL_FSI_RES_L2** | *default:* 1e-06 |break| Absolute tolerance for interface residual in L2-norm

.. _fsidynamic_monolithicsolver_tol_fsi_res_inf:

**TOL_FSI_RES_INF** | *default:* 1e-06 |break| Absolute tolerance for interface residual in Inf-norm

.. _fsidynamic_monolithicsolver_tol_fsi_inc_l2:

**TOL_FSI_INC_L2** | *default:* 1e-06 |break| Absolute tolerance for interface increment in L2-norm

.. _fsidynamic_monolithicsolver_tol_fsi_inc_inf:

**TOL_FSI_INC_INF** | *default:* 1e-06 |break| Absolute tolerance for interface increment in Inf-norm

.. _fsidynamic_monolithicsolver_tol_pre_res_l2:

**TOL_PRE_RES_L2** | *default:* 1e-06 |break| Absolute tolerance for fluid pressure residual in L2-norm

.. _fsidynamic_monolithicsolver_tol_pre_res_inf:

**TOL_PRE_RES_INF** | *default:* 1e-06 |break| Absolute tolerance for fluid pressure residual in Inf-norm

.. _fsidynamic_monolithicsolver_tol_pre_inc_l2:

**TOL_PRE_INC_L2** | *default:* 1e-06 |break| Absolute tolerance for fluid pressure increment in L2-norm

.. _fsidynamic_monolithicsolver_tol_pre_inc_inf:

**TOL_PRE_INC_INF** | *default:* 1e-06 |break| Absolute tolerance for fluid pressure increment in Inf-norm

.. _fsidynamic_monolithicsolver_tol_vel_res_l2:

**TOL_VEL_RES_L2** | *default:* 1e-06 |break| Absolute tolerance for fluid velocity residual in L2-norm

.. _fsidynamic_monolithicsolver_tol_vel_res_inf:

**TOL_VEL_RES_INF** | *default:* 1e-06 |break| Absolute tolerance for fluid velocity residual in Inf-norm

.. _fsidynamic_monolithicsolver_tol_vel_inc_l2:

**TOL_VEL_INC_L2** | *default:* 1e-06 |break| Absolute tolerance for fluid velocity increment in L2-norm

.. _fsidynamic_monolithicsolver_tol_vel_inc_inf:

**TOL_VEL_INC_INF** | *default:* 1e-06 |break| Absolute tolerance for fluid velocity increment in Inf-norm

.. _SECfsidynamic_partitionedsolver:

FSI DYNAMIC/PARTITIONED SOLVER
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   -------------------------------------FSI DYNAMIC/PARTITIONED SOLVER

.. _fsidynamic_partitionedsolver_basetol:

**BASETOL** | *default:* 0.001 |break| Basic tolerance for adaptive convergence check in monolithic FSI.
This tolerance will be used for the linear solve of the FSI block system.
The linear convergence test will always use the relative residual norm (AZ_r0).
Not to be confused with the Newton tolerance (CONVTOL) that applies
to the nonlinear convergence test using a absolute residual norm.

.. _fsidynamic_partitionedsolver_convtol:

**CONVTOL** | *default:* 1e-06 |break| Tolerance for iteration over fields in case of partitioned scheme

.. _fsidynamic_partitionedsolver_coupmethod:

**COUPMETHOD** | *default:* conforming |break| Coupling Method Mortar (mtr) or conforming nodes at interface

   **Possible values:**

   - MTR
   - Mtr
   - mtr
   - conforming
   - immersed

.. _fsidynamic_partitionedsolver_coupvariable:

**COUPVARIABLE** | *default:* Displacement |break| Coupling variable at the interface

   **Possible values:**

   - Displacement
   - Force
   - Velocity

.. _fsidynamic_partitionedsolver_divprojection:

**DIVPROJECTION** | *default:* no |break| Project velocity into divergence-free subspace for partitioned fsi

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fsidynamic_partitionedsolver_itemax:

**ITEMAX** | *default:* 100 |break| Maximum number of iterations over fields

.. _fsidynamic_partitionedsolver_maxomega:

**MAXOMEGA** | *default:* 0 |break| largest omega allowed for Aitken relaxation (0.0 means no constraint)

.. _fsidynamic_partitionedsolver_minomega:

**MINOMEGA** | *default:* -1 |break| smallest omega allowed for Aitken relaxation (default is -1.0)

.. _fsidynamic_partitionedsolver_partitioned:

**PARTITIONED** | *default:* DirichletNeumann |break| Coupling strategies for partitioned FSI solvers.

   **Possible values:**

   - DirichletNeumann
   - DirichletNeumannSlideALE
   - DirichletNeumannVolCoupl

.. _fsidynamic_partitionedsolver_predictor:

**PREDICTOR** | *default:* d(n) |break| Predictor for interface displacements

   **Possible values:**

   - d(n)
   - d(n)+dt*(1.5*v(n)-0.5*v(n-1))
   - d(n)+dt*v(n)
   - d(n)+dt*v(n)+0.5*dt^2*a(n)

.. _fsidynamic_partitionedsolver_relax:

**RELAX** | *default:* 1 |break| fixed relaxation parameter for partitioned FSI solvers

.. _SECfsidynamic_constraint:

FSI DYNAMIC/CONSTRAINT
~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ---------------------------------------------FSI DYNAMIC/CONSTRAINT

.. _fsidynamic_constraint_preconditioner:

**PRECONDITIONER** | *default:* Simple |break| preconditioner to use

   **Possible values:**

   - Simple
   - Simplec

.. _fsidynamic_constraint_simpleiter:

**SIMPLEITER** | *default:* 2 |break| Number of iterations for simple pc

.. _fsidynamic_constraint_alpha:

**ALPHA** | *default:* 0.8 |break| alpha parameter for simple pc

.. _SECarterialdynamic:

ARTERIAL DYNAMIC
----------------

no description yet

::

   ---------------------------------------------------ARTERIAL DYNAMIC

.. _arterialdynamic_dynamictyp:

**DYNAMICTYP** | *default:* ExpTaylorGalerkin |break| Explicit Taylor Galerkin Scheme

   **Possible values:**

   - ExpTaylorGalerkin
   - Stationary

.. _arterialdynamic_timestep:

**TIMESTEP** | *default:* 0.01 |break| Time increment dt

.. _arterialdynamic_numstep:

**NUMSTEP** | *default:* 0 |break| Number of Time Steps

.. _arterialdynamic_maxtime:

**MAXTIME** | *default:* 1000 |break| total simulation time

.. _arterialdynamic_restartevry:

**RESTARTEVRY** | *default:* 1 |break| Increment for writing restart

.. _arterialdynamic_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| Increment for writing solution

.. _arterialdynamic_solvescatra:

**SOLVESCATRA** | *default:* no |break| Flag to (de)activate solving scalar transport in blood

   **Possible values:**

   - no
   - yes

.. _arterialdynamic_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| number of linear solver used for arterial dynamics

.. _arterialdynamic_initfuncno:

**INITFUNCNO** | *default:* -1 |break| function number for artery initial field

.. _arterialdynamic_initialfield:

**INITIALFIELD** | *default:* zero_field |break| Initial Field for artery problem

   **Possible values:**

   - zero_field
   - field_by_function
   - field_by_condition

.. _SECcoupledreduced-dairwaysandtissuedynamic:

COUPLED REDUCED-D AIRWAYS AND TISSUE DYNAMIC
--------------------------------------------

no description yet

::

   -----------------------COUPLED REDUCED-D AIRWAYS AND TISSUE DYNAMIC

.. _coupledreduced-dairwaysandtissuedynamic_convtol_p:

**CONVTOL_P** | *default:* 1e-06 |break| Coupled red_airway and tissue iteration convergence for pressure

.. _coupledreduced-dairwaysandtissuedynamic_convtol_q:

**CONVTOL_Q** | *default:* 1e-06 |break| Coupled red_airway and tissue iteration convergence for flux

.. _coupledreduced-dairwaysandtissuedynamic_maxiter:

**MAXITER** | *default:* 5 |break| Maximum coupling iterations

.. _coupledreduced-dairwaysandtissuedynamic_relaxtype:

**RELAXTYPE** | *default:* norelaxation |break| Dynamic Relaxation Type

   **Possible values:**

   - norelaxation
   - fixedrelaxation
   - Aitken
   - SD

.. _coupledreduced-dairwaysandtissuedynamic_timestep:

**TIMESTEP** | *default:* 0.01 |break| Time increment dt

.. _coupledreduced-dairwaysandtissuedynamic_numstep:

**NUMSTEP** | *default:* 1 |break| Number of Time Steps

.. _coupledreduced-dairwaysandtissuedynamic_maxtime:

**MAXTIME** | *default:* 4 |break| no description yet

.. _coupledreduced-dairwaysandtissuedynamic_normal:

**NORMAL** | *default:* 1 |break| no description yet

.. _SECbiofilmcontrol:

BIOFILM CONTROL
---------------

control parameters for biofilm problems


::

   ----------------------------------------------------BIOFILM CONTROL

.. _biofilmcontrol_biofilmgrowth:

**BIOFILMGROWTH** | *default:* No |break| Scatra algorithm for biofilm growth

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _biofilmcontrol_avgrowth:

**AVGROWTH** | *default:* No |break| The calculation of growth parameters is based on averaged values

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _biofilmcontrol_fluxcoef:

**FLUXCOEF** | *default:* 0 |break| Coefficient for growth due to scalar flux

.. _biofilmcontrol_normforceposcoef:

**NORMFORCEPOSCOEF** | *default:* 0 |break| Coefficient for erosion due to traction normal surface forces

.. _biofilmcontrol_normforcenegcoef:

**NORMFORCENEGCOEF** | *default:* 0 |break| Coefficient for erosion due to compression normal surface forces

.. _biofilmcontrol_tangoneforcecoef:

**TANGONEFORCECOEF** | *default:* 0 |break| Coefficient for erosion due to the first tangential surface force

.. _biofilmcontrol_tangtwoforcecoef:

**TANGTWOFORCECOEF** | *default:* 0 |break| Coefficient for erosion due to the second tangential surface force

.. _biofilmcontrol_biotimestep:

**BIOTIMESTEP** | *default:* 0.05 |break| Time step size for biofilm growth

.. _biofilmcontrol_bionumstep:

**BIONUMSTEP** | *default:* 0 |break| Maximum number of steps for biofilm growth

.. _biofilmcontrol_output_gmsh:

**OUTPUT_GMSH** | *default:* No |break| Do you want to write Gmsh postprocessing files?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECpatientspecific:

PATIENT SPECIFIC
----------------

no description yet

::

   ---------------------------------------------------PATIENT SPECIFIC

.. _patientspecific_patspec:

**PATSPEC** | *default:* No |break| Triggers application of patient specific tools in discretization construction

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _patientspecific_remodel:

**REMODEL** | *default:* No |break| Turn remodeling on/off

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _patientspecific_calcinnerradius:

**CALCINNERRADIUS** | *default:* No |break| Compute inner radius for pure structural wall shear stress

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _patientspecific_linearcenterline:

**LINEARCENTERLINE** | *default:* No |break| Is the centerline linear? Only important when CALCINNERRADIUS is turned on

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _patientspecific_centerlinedirection:

**CENTERLINEDIRECTION** | *default:* -1 |break| direction of linear centerline

.. _patientspecific_centerlinepoint:

**CENTERLINEPOINT** | *default:* -1 |break| point on linear centerline

.. _patientspecific_maxhulumen:

**MAXHULUMEN** | *default:* 0 |break| max HU value within the blood lumen

.. _patientspecific_centerlinefile:

**CENTERLINEFILE** | *default:* name.txt |break| filename of file containing centerline points

.. _patientspecific_calcstrength:

**CALCSTRENGTH** | *default:* No |break| Calculate strength on/off

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _patientspecific_aaa_subrendia:

**AAA_SUBRENDIA** | *default:* 22.01 |break| subrenal diameter of the AAA

.. _patientspecific_familyhist:

**FAMILYHIST** | *default:* No |break| Does the patient have AAA family history

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _patientspecific_male_patient:

**MALE_PATIENT** | *default:* Yes |break| Is the patient a male?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _patientspecific_calc_accurate_max_ilt_thick:

**CALC_ACCURATE_MAX_ILT_THICK** | *default:* no |break| Method with which the Max ILT thickness is calculated

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECreduceddimensionalairwaysdynamic:

REDUCED DIMENSIONAL AIRWAYS DYNAMIC
-----------------------------------

no description yet

::

   --------------------------------REDUCED DIMENSIONAL AIRWAYS DYNAMIC

.. _reduceddimensionalairwaysdynamic_dynamictyp:

**DYNAMICTYP** | *default:* OneStepTheta |break| OneStepTheta Scheme

   **Possible values:**

   - OneStepTheta

.. _reduceddimensionalairwaysdynamic_solvertype:

**SOLVERTYPE** | *default:* Linear |break| Solver type

   **Possible values:**

   - Linear
   - Nonlinear

.. _reduceddimensionalairwaysdynamic_timestep:

**TIMESTEP** | *default:* 0.01 |break| Time increment dt

.. _reduceddimensionalairwaysdynamic_numstep:

**NUMSTEP** | *default:* 0 |break| Number of Time Steps

.. _reduceddimensionalairwaysdynamic_restartevry:

**RESTARTEVRY** | *default:* 1 |break| Increment for writing restart

.. _reduceddimensionalairwaysdynamic_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| Increment for writing solution

.. _reduceddimensionalairwaysdynamic_theta:

**THETA** | *default:* 1 |break| One-step-theta time integration factor

.. _reduceddimensionalairwaysdynamic_maxiterations:

**MAXITERATIONS** | *default:* 1 |break| maximum iteration steps

.. _reduceddimensionalairwaysdynamic_tolerance:

**TOLERANCE** | *default:* 1e-06 |break| tolerance

.. _reduceddimensionalairwaysdynamic_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| number of linear solver used for reduced dim arterial dynamics

.. _reduceddimensionalairwaysdynamic_solvescatra:

**SOLVESCATRA** | *default:* no |break| Flag to (de)activate solving scalar transport in blood

   **Possible values:**

   - no
   - yes

.. _reduceddimensionalairwaysdynamic_compawacinter:

**COMPAWACINTER** | *default:* no |break| Flag to (de)activate computation of airway-acinus interdependency 

   **Possible values:**

   - no
   - yes

.. _reduceddimensionalairwaysdynamic_calcv0prestress:

**CALCV0PRESTRESS** | *default:* no |break| Flag to (de)activate initial acini volume adjustment with pre-stress condition 

   **Possible values:**

   - no
   - yes

.. _reduceddimensionalairwaysdynamic_transpulmpress:

**TRANSPULMPRESS** | *default:* 800 |break| Transpulmonary pressure needed for recalculation of acini volumes

.. _SECcardiovascular0d-structurecoupling:

CARDIOVASCULAR 0D-STRUCTURE COUPLING
------------------------------------

no description yet

::

   -------------------------------CARDIOVASCULAR 0D-STRUCTURE COUPLING

.. _cardiovascular0d-structurecoupling_tol_cardvasc0d_res:

**TOL_CARDVASC0D_RES** | *default:* 1e-08 |break| tolerance in the cardiovascular0d error norm for the Newton iteration

.. _cardiovascular0d-structurecoupling_tol_cardvasc0d_dofincr:

**TOL_CARDVASC0D_DOFINCR** | *default:* 1e-08 |break| tolerance in the cardiovascular0d dof increment error norm for the Newton iteration

.. _cardiovascular0d-structurecoupling_timint_theta:

**TIMINT_THETA** | *default:* 0.5 |break| theta for one-step-theta time-integration scheme of Cardiovascular0D

.. _cardiovascular0d-structurecoupling_restart_with_cardvasc0d:

**RESTART_WITH_CARDVASC0D** | *default:* No |break| Must be chosen if a non-cardiovascular0d simulation is to be restarted as cardiovascular0d-structural coupled problem.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _cardiovascular0d-structurecoupling_enhanced_output:

**ENHANCED_OUTPUT** | *default:* No |break| Set to yes for enhanced output (like e.g. derivative information)

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _cardiovascular0d-structurecoupling_linear_coupled_solver:

**LINEAR_COUPLED_SOLVER** | *default:* -1 |break| number of linear solver used for cardiovascular 0D-structural problems

.. _cardiovascular0d-structurecoupling_solalgorithm:

**SOLALGORITHM** | *default:* direct |break| no description yet

   **Possible values:**

   - simple
   - direct
   - AMGnxn

.. _cardiovascular0d-structurecoupling_t_period:

**T_PERIOD** | *default:* -1 |break| periodic time

.. _cardiovascular0d-structurecoupling_eps_periodic:

**EPS_PERIODIC** | *default:* 1e-16 |break| tolerance for periodic state

.. _cardiovascular0d-structurecoupling_ptc_3d0d:

**PTC_3D0D** | *default:* No |break| Set to yes for doing PTC 2x2 block system.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _cardiovascular0d-structurecoupling_k_ptc:

**K_PTC** | *default:* 0 |break| PTC parameter: 0 means normal Newton, ->infty means steepest desc

.. _SECcardiovascular0d-structurecoupling_sys-pulcirculationparameters:

CARDIOVASCULAR 0D-STRUCTURE COUPLING/SYS-PUL CIRCULATION PARAMETERS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   --CARDIOVASCULAR 0D-STRUCTURE COUPLING/SYS-PUL CIRCULATION PARAMETERS

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_arvalve_max_l:

**R_arvalve_max_l** | *default:* 0 |break| maximal left arterial (semilunar) valve resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_arvalve_min_l:

**R_arvalve_min_l** | *default:* 0 |break| minimal left arterial (semilunar) valve resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_atvalve_max_l:

**R_atvalve_max_l** | *default:* 0 |break| maximal left atrial (atrioventricular) valve resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_atvalve_min_l:

**R_atvalve_min_l** | *default:* 0 |break| minimal left atrial (atrioventricular) valve resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_arvalve_max_r:

**R_arvalve_max_r** | *default:* 0 |break| maximal right arterial (semilunar) valve resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_arvalve_min_r:

**R_arvalve_min_r** | *default:* 0 |break| minimal right arterial (semilunar) valve resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_atvalve_max_r:

**R_atvalve_max_r** | *default:* 0 |break| maximal right atrial (atrioventricular) valve resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_atvalve_min_r:

**R_atvalve_min_r** | *default:* 0 |break| minimal right atrial (atrioventricular) valve resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_atrium_model:

**ATRIUM_MODEL** | *default:* 0D |break| no description yet

   **Possible values:**

   - 0D
   - 3D
   - prescribed

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_atrium_act_curve_l:

**Atrium_act_curve_l** | *default:* -1 |break| left atrial activation curve (ONLY for ATRIUM_MODEL '0D'!)

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_atrium_act_curve_r:

**Atrium_act_curve_r** | *default:* -1 |break| right atrial activation curve (ONLY for ATRIUM_MODEL '0D'!)

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_atrium_prescr_e_curve_l:

**Atrium_prescr_E_curve_l** | *default:* -1 |break| left atrial elastance prescription curve (ONLY for ATRIUM_MODEL 'prescribed'!)

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_atrium_prescr_e_curve_r:

**Atrium_prescr_E_curve_r** | *default:* -1 |break| right atrial elastance prescription curve (ONLY for ATRIUM_MODEL 'prescribed'!)

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_e_at_max_l:

**E_at_max_l** | *default:* 0 |break| 0D maximum left atrial elastance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_e_at_min_l:

**E_at_min_l** | *default:* 0 |break| 0D baseline left atrial elastance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_e_at_max_r:

**E_at_max_r** | *default:* 0 |break| 0D maximum right atrial elastance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_e_at_min_r:

**E_at_min_r** | *default:* 0 |break| 0D baseline right atrial elastance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_ventricle_model:

**VENTRICLE_MODEL** | *default:* 3D |break| no description yet

   **Possible values:**

   - 3D
   - 0D
   - prescribed

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_ventricle_act_curve_l:

**Ventricle_act_curve_l** | *default:* -1 |break| left ventricular activation curve (ONLY for VENTRICLE_MODEL '0D'!)

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_ventricle_act_curve_r:

**Ventricle_act_curve_r** | *default:* -1 |break| right ventricular activation curve (ONLY for VENTRICLE_MODEL '0D'!)

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_ventricle_prescr_e_curve_l:

**Ventricle_prescr_E_curve_l** | *default:* -1 |break| left ventricular elastance prescription curve (ONLY for VENTRICLE_MODEL 'prescribed'!)

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_ventricle_prescr_e_curve_r:

**Ventricle_prescr_E_curve_r** | *default:* -1 |break| right ventricular elastance prescription curve (ONLY for VENTRICLE_MODEL 'prescribed'!)

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_e_v_max_l:

**E_v_max_l** | *default:* 0 |break| 0D maximum left ventricular elastance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_e_v_min_l:

**E_v_min_l** | *default:* 0 |break| 0D baseline left ventricular elastance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_e_v_max_r:

**E_v_max_r** | *default:* 0 |break| 0D maximum right ventricular elastance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_e_v_min_r:

**E_v_min_r** | *default:* 0 |break| 0D baseline right ventricular elastance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_c_ar_sys:

**C_ar_sys** | *default:* 0 |break| systemic arterial compliance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_ar_sys:

**R_ar_sys** | *default:* 0 |break| systemic arterial resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_l_ar_sys:

**L_ar_sys** | *default:* 0 |break| systemic arterial inertance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_z_ar_sys:

**Z_ar_sys** | *default:* 0 |break| systemic arterial impedance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_c_ar_pul:

**C_ar_pul** | *default:* 0 |break| pulmonary arterial compliance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_ar_pul:

**R_ar_pul** | *default:* 0 |break| pulmonary arterial resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_l_ar_pul:

**L_ar_pul** | *default:* 0 |break| pulmonary arterial inertance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_z_ar_pul:

**Z_ar_pul** | *default:* 0 |break| pulmonary arterial impedance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_c_ven_sys:

**C_ven_sys** | *default:* 0 |break| systemic venous compliance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_ven_sys:

**R_ven_sys** | *default:* 0 |break| systemic venous resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_l_ven_sys:

**L_ven_sys** | *default:* 0 |break| systemic venous inertance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_c_ven_pul:

**C_ven_pul** | *default:* 0 |break| pulmonary venous compliance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_ven_pul:

**R_ven_pul** | *default:* 0 |break| pulmonary venous resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_l_ven_pul:

**L_ven_pul** | *default:* 0 |break| pulmonary venous inertance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_q_vin_l_0:

**q_vin_l_0** | *default:* 0 |break| initial left ventricular in-flux

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_p_at_l_0:

**p_at_l_0** | *default:* 0 |break| initial left atrial pressure

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_q_vout_l_0:

**q_vout_l_0** | *default:* 0 |break| initial left ventricular out-flux

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_p_v_l_0:

**p_v_l_0** | *default:* 0 |break| initial left ventricular pressure

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_p_ar_sys_0:

**p_ar_sys_0** | *default:* 0 |break| initial systemic arterial pressure

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_q_ar_sys_0:

**q_ar_sys_0** | *default:* 0 |break| initial systemic arterial flux

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_p_ven_sys_0:

**p_ven_sys_0** | *default:* 0 |break| initial systemic venous pressure

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_q_ven_sys_0:

**q_ven_sys_0** | *default:* 0 |break| initial systemic venous flux

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_q_vin_r_0:

**q_vin_r_0** | *default:* 0 |break| initial right ventricular in-flux

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_p_at_r_0:

**p_at_r_0** | *default:* 0 |break| initial right atrial pressure

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_q_vout_r_0:

**q_vout_r_0** | *default:* 0 |break| initial right ventricular out-flux

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_p_v_r_0:

**p_v_r_0** | *default:* 0 |break| initial right ventricular pressure

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_p_ar_pul_0:

**p_ar_pul_0** | *default:* 0 |break| initial pulmonary arterial pressure

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_q_ar_pul_0:

**q_ar_pul_0** | *default:* 0 |break| initial pulmonary arterial flux

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_p_ven_pul_0:

**p_ven_pul_0** | *default:* 0 |break| initial pulmonary venous pressure

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_q_ven_pul_0:

**q_ven_pul_0** | *default:* 0 |break| initial pulmonary venous flux

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_v_at_l_u:

**V_at_l_u** | *default:* 0 |break| unstressed volume of left 0D atrium

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_v_v_l_u:

**V_v_l_u** | *default:* 0 |break| unstressed volume of left 0D ventricle

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_v_ar_sys_u:

**V_ar_sys_u** | *default:* 0 |break| unstressed volume of systemic arteries and capillaries

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_v_ven_sys_u:

**V_ven_sys_u** | *default:* 100000 |break| unstressed volume of systemic veins

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_v_at_r_u:

**V_at_r_u** | *default:* 0 |break| unstressed volume of right 0D atrium

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_v_v_r_u:

**V_v_r_u** | *default:* 0 |break| unstressed volume of right 0D ventricle

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_v_ar_pul_u:

**V_ar_pul_u** | *default:* 0 |break| unstressed volume of pulmonary arteries and capillaries

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_v_ven_pul_u:

**V_ven_pul_u** | *default:* 120000 |break| unstressed volume of pulmonary veins

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_c_arspl_sys:

**C_arspl_sys** | *default:* 0 |break| systemic arterial splanchnic compliance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_arspl_sys:

**R_arspl_sys** | *default:* 0 |break| systemic arterial splanchnic resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_c_arespl_sys:

**C_arespl_sys** | *default:* 0 |break| systemic arterial extra-splanchnic compliance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_arespl_sys:

**R_arespl_sys** | *default:* 0 |break| systemic arterial extra-splanchnic resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_c_armsc_sys:

**C_armsc_sys** | *default:* 0 |break| systemic arterial muscular compliance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_armsc_sys:

**R_armsc_sys** | *default:* 0 |break| systemic arterial muscular resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_c_arcer_sys:

**C_arcer_sys** | *default:* 0 |break| systemic arterial cerebral compliance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_arcer_sys:

**R_arcer_sys** | *default:* 0 |break| systemic arterial cerebral resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_c_arcor_sys:

**C_arcor_sys** | *default:* 0 |break| systemic arterial coronary compliance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_arcor_sys:

**R_arcor_sys** | *default:* 0 |break| systemic arterial coronary resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_c_venspl_sys:

**C_venspl_sys** | *default:* 0 |break| systemic venous splanchnic compliance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_venspl_sys:

**R_venspl_sys** | *default:* 0 |break| systemic venous splanchnic resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_c_venespl_sys:

**C_venespl_sys** | *default:* 0 |break| systemic venous extra-splanchnic compliance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_venespl_sys:

**R_venespl_sys** | *default:* 0 |break| systemic venous extra-splanchnic resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_c_venmsc_sys:

**C_venmsc_sys** | *default:* 0 |break| systemic venous muscular compliance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_venmsc_sys:

**R_venmsc_sys** | *default:* 0 |break| systemic venous muscular resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_c_vencer_sys:

**C_vencer_sys** | *default:* 0 |break| systemic venous cerebral compliance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_vencer_sys:

**R_vencer_sys** | *default:* 0 |break| systemic venous cerebral resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_c_vencor_sys:

**C_vencor_sys** | *default:* 0 |break| systemic venous coronary compliance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_vencor_sys:

**R_vencor_sys** | *default:* 0 |break| systemic venous coronary resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_c_cap_pul:

**C_cap_pul** | *default:* 0 |break| pulmonary capillary compliance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_r_cap_pul:

**R_cap_pul** | *default:* 0 |break| pulmonary capillary resistance

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_p_arperi_sys_0:

**p_arperi_sys_0** | *default:* 0 |break| initial systemic peripheral arterial pressure

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_q_arspl_sys_0:

**q_arspl_sys_0** | *default:* 0 |break| initial systemic arterial splanchnic flux

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_q_arespl_sys_0:

**q_arespl_sys_0** | *default:* 0 |break| initial systemic arterial extra-splanchnic flux

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_q_armsc_sys_0:

**q_armsc_sys_0** | *default:* 0 |break| initial systemic arterial muscular flux

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_q_arcer_sys_0:

**q_arcer_sys_0** | *default:* 0 |break| initial systemic arterial cerebral flux

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_q_arcor_sys_0:

**q_arcor_sys_0** | *default:* 0 |break| initial systemic arterial coronary flux

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_p_venspl_sys_0:

**p_venspl_sys_0** | *default:* 0 |break| initial systemic venous splanchnic pressure

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_q_venspl_sys_0:

**q_venspl_sys_0** | *default:* 0 |break| initial systemic venous splanchnic flux

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_p_venespl_sys_0:

**p_venespl_sys_0** | *default:* 0 |break| initial systemic venous extra-splanchnic pressure

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_q_venespl_sys_0:

**q_venespl_sys_0** | *default:* 0 |break| initial systemic venous extra-splanchnic flux

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_p_venmsc_sys_0:

**p_venmsc_sys_0** | *default:* 0 |break| initial systemic venous muscular pressure

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_q_venmsc_sys_0:

**q_venmsc_sys_0** | *default:* 0 |break| initial systemic venous muscular flux

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_p_vencer_sys_0:

**p_vencer_sys_0** | *default:* 0 |break| initial systemic venous cerebral pressure

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_q_vencer_sys_0:

**q_vencer_sys_0** | *default:* 0 |break| initial systemic venous cerebral flux

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_p_vencor_sys_0:

**p_vencor_sys_0** | *default:* 0 |break| initial systemic venous coronary pressure

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_q_vencor_sys_0:

**q_vencor_sys_0** | *default:* 0 |break| initial systemic venous coronary flux

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_p_cap_pul_0:

**p_cap_pul_0** | *default:* 0 |break| initial pulmonary capillary pressure

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_q_cap_pul_0:

**q_cap_pul_0** | *default:* 0 |break| initial pulmonary capillary flux

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_v_arspl_sys_u:

**V_arspl_sys_u** | *default:* 274400 |break| unstressed volume of systemic splanchnic arteries

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_v_arespl_sys_u:

**V_arespl_sys_u** | *default:* 134640 |break| unstressed volume of systemic extra-splanchnic arteries

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_v_armsc_sys_u:

**V_armsc_sys_u** | *default:* 105800 |break| unstressed volume of systemic muscular arteries

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_v_arcer_sys_u:

**V_arcer_sys_u** | *default:* 72130 |break| unstressed volume of systemic cerebral arteries

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_v_arcor_sys_u:

**V_arcor_sys_u** | *default:* 24000 |break| unstressed volume of systemic coronary arteries

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_v_venspl_sys_u:

**V_venspl_sys_u** | *default:* 1.121e+06 |break| unstressed volume of systemic splanchnic veins

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_v_venespl_sys_u:

**V_venespl_sys_u** | *default:* 550000 |break| unstressed volume of systemic extra-splanchnic veins

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_v_venmsc_sys_u:

**V_venmsc_sys_u** | *default:* 432140 |break| unstressed volume of systemic muscular veins

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_v_vencer_sys_u:

**V_vencer_sys_u** | *default:* 294640 |break| unstressed volume of systemic cerebral veins

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_v_vencor_sys_u:

**V_vencor_sys_u** | *default:* 98210 |break| unstressed volume of systemic coronary veins

.. _cardiovascular0d-structurecoupling_sys-pulcirculationparameters_v_cap_pul_u:

**V_cap_pul_u** | *default:* 123000 |break| unstressed volume of pulmonary capillaries

.. _SECcardiovascular0d-structurecoupling_respiratoryparameters:

CARDIOVASCULAR 0D-STRUCTURE COUPLING/RESPIRATORY PARAMETERS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   --------CARDIOVASCULAR 0D-STRUCTURE COUPLING/RESPIRATORY PARAMETERS

.. _cardiovascular0d-structurecoupling_respiratoryparameters_respiratory_model:

**RESPIRATORY_MODEL** | *default:* None |break| no description yet

   **Possible values:**

   - None
   - Standard

.. _cardiovascular0d-structurecoupling_respiratoryparameters_l_alv:

**L_alv** | *default:* 0 |break| alveolar inertance

.. _cardiovascular0d-structurecoupling_respiratoryparameters_r_alv:

**R_alv** | *default:* 0 |break| alveolar resistance

.. _cardiovascular0d-structurecoupling_respiratoryparameters_e_alv:

**E_alv** | *default:* 0 |break| alveolar elastance

.. _cardiovascular0d-structurecoupling_respiratoryparameters_v_lung_tidal:

**V_lung_tidal** | *default:* 400000 |break| tidal volume (the total volume of inspired air, in a single breath)

.. _cardiovascular0d-structurecoupling_respiratoryparameters_v_lung_dead:

**V_lung_dead** | *default:* 150000 |break| dead space volume

.. _cardiovascular0d-structurecoupling_respiratoryparameters_v_lung_u:

**V_lung_u** | *default:* 0 |break| unstressed lung volume (volume of the lung when it is fully collapsed outside the body)

.. _cardiovascular0d-structurecoupling_respiratoryparameters_u_t_curve:

**U_t_curve** | *default:* -1 |break| time-varying, prescribed pleural pressure curve driven by diaphragm

.. _cardiovascular0d-structurecoupling_respiratoryparameters_u_m:

**U_m** | *default:* 0 |break| in-breath pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_fco2_ext:

**fCO2_ext** | *default:* 0.03 |break| atmospheric CO2 gas fraction

.. _cardiovascular0d-structurecoupling_respiratoryparameters_fo2_ext:

**fO2_ext** | *default:* 0.21 |break| atmospheric O2 gas fraction

.. _cardiovascular0d-structurecoupling_respiratoryparameters_kappa_co2:

**kappa_CO2** | *default:* 0 |break| diffusion coefficient for CO2 across the hemato-alveolar membrane, in molar value / (time * pressure)

.. _cardiovascular0d-structurecoupling_respiratoryparameters_kappa_o2:

**kappa_O2** | *default:* 0 |break| diffusion coefficient for O2 across the hemato-alveolar membrane, in molar value / (time * pressure)

.. _cardiovascular0d-structurecoupling_respiratoryparameters_v_m_gas:

**V_m_gas** | *default:* 22400 |break| molar volume of an ideal gas

.. _cardiovascular0d-structurecoupling_respiratoryparameters_p_vap_water_37:

**p_vap_water_37** | *default:* 6.27949 |break| vapor pressure of water at 37  degrees celsius

.. _cardiovascular0d-structurecoupling_respiratoryparameters_alpha_co2:

**alpha_CO2** | *default:* 0 |break| CO2 solubility constant, in molar value / (volume * pressure)

.. _cardiovascular0d-structurecoupling_respiratoryparameters_alpha_o2:

**alpha_O2** | *default:* 0 |break| O2 solubility constant, in molar value / (volume * pressure)

.. _cardiovascular0d-structurecoupling_respiratoryparameters_c_hb:

**c_Hb** | *default:* 0 |break| hemoglobin concentration of the blood, in molar value / volume

.. _cardiovascular0d-structurecoupling_respiratoryparameters_m_co2_arspl:

**M_CO2_arspl** | *default:* 0 |break| splanchnic metabolic rate of CO2 production

.. _cardiovascular0d-structurecoupling_respiratoryparameters_m_o2_arspl:

**M_O2_arspl** | *default:* 0 |break| splanchnic metabolic rate of O2 consumption

.. _cardiovascular0d-structurecoupling_respiratoryparameters_m_co2_arespl:

**M_CO2_arespl** | *default:* 0 |break| extra-splanchnic metabolic rate of CO2 production

.. _cardiovascular0d-structurecoupling_respiratoryparameters_m_o2_arespl:

**M_O2_arespl** | *default:* 0 |break| extra-splanchnic metabolic rate of O2 consumption

.. _cardiovascular0d-structurecoupling_respiratoryparameters_m_co2_armsc:

**M_CO2_armsc** | *default:* 0 |break| muscular metabolic rate of CO2 production

.. _cardiovascular0d-structurecoupling_respiratoryparameters_m_o2_armsc:

**M_O2_armsc** | *default:* 0 |break| muscular metabolic rate of O2 consumption

.. _cardiovascular0d-structurecoupling_respiratoryparameters_m_co2_arcer:

**M_CO2_arcer** | *default:* 0 |break| cerebral metabolic rate of CO2 production

.. _cardiovascular0d-structurecoupling_respiratoryparameters_m_o2_arcer:

**M_O2_arcer** | *default:* 0 |break| cerebral metabolic rate of O2 consumption

.. _cardiovascular0d-structurecoupling_respiratoryparameters_m_co2_arcor:

**M_CO2_arcor** | *default:* 0 |break| coronary metabolic rate of CO2 production

.. _cardiovascular0d-structurecoupling_respiratoryparameters_m_o2_arcor:

**M_O2_arcor** | *default:* 0 |break| coronary metabolic rate of O2 consumption

.. _cardiovascular0d-structurecoupling_respiratoryparameters_v_tissspl:

**V_tissspl** | *default:* 1 |break| splanchnic tissue volume

.. _cardiovascular0d-structurecoupling_respiratoryparameters_v_tissespl:

**V_tissespl** | *default:* 1 |break| extra-splanchnic tissue volume

.. _cardiovascular0d-structurecoupling_respiratoryparameters_v_tissmsc:

**V_tissmsc** | *default:* 1 |break| muscular tissue volume

.. _cardiovascular0d-structurecoupling_respiratoryparameters_v_tisscer:

**V_tisscer** | *default:* 1 |break| cerebral tissue volume

.. _cardiovascular0d-structurecoupling_respiratoryparameters_v_tisscor:

**V_tisscor** | *default:* 1 |break| coronary tissue volume

.. _cardiovascular0d-structurecoupling_respiratoryparameters_v_alv_0:

**V_alv_0** | *default:* -1 |break| initial alveolar volume

.. _cardiovascular0d-structurecoupling_respiratoryparameters_q_alv_0:

**q_alv_0** | *default:* 0 |break| initial alveolar flux

.. _cardiovascular0d-structurecoupling_respiratoryparameters_p_alv_0:

**p_alv_0** | *default:* -1 |break| initial alveolar pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_fco2_alv_0:

**fCO2_alv_0** | *default:* 0.05263 |break| initial alveolar CO2 fraction

.. _cardiovascular0d-structurecoupling_respiratoryparameters_fo2_alv_0:

**fO2_alv_0** | *default:* 0.1368 |break| initial alveolar O2 fraction

.. _cardiovascular0d-structurecoupling_respiratoryparameters_q_arspl_sys_in_0:

**q_arspl_sys_in_0** | *default:* 0 |break| initial arterial splanchnic in-flux

.. _cardiovascular0d-structurecoupling_respiratoryparameters_q_arsspl_sys_in_0:

**q_arsspl_sys_in_0** | *default:* 0 |break| initial arterial extra-splanchnic in-flux

.. _cardiovascular0d-structurecoupling_respiratoryparameters_q_armsc_sys_in_0:

**q_armsc_sys_in_0** | *default:* 0 |break| initial arterial muscular in-flux

.. _cardiovascular0d-structurecoupling_respiratoryparameters_q_arcer_sys_in_0:

**q_arcer_sys_in_0** | *default:* 0 |break| initial arterial cerebral in-flux

.. _cardiovascular0d-structurecoupling_respiratoryparameters_q_arcor_sys_in_0:

**q_arcor_sys_in_0** | *default:* 0 |break| initial arterial coronary in-flux

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppco2_at_r_0:

**ppCO2_at_r_0** | *default:* 1 |break| initial right atrial CO2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppo2_at_r_0:

**ppO2_at_r_0** | *default:* 1 |break| initial right atrial O2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppco2_v_r_0:

**ppCO2_v_r_0** | *default:* 1 |break| initial right ventricular CO2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppo2_v_r_0:

**ppO2_v_r_0** | *default:* 1 |break| initial right ventricular O2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppco2_ar_pul_0:

**ppCO2_ar_pul_0** | *default:* 1 |break| initial pulmonary arterial CO2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppo2_ar_pul_0:

**ppO2_ar_pul_0** | *default:* 1 |break| initial pulmonary arterial O2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppco2_cap_pul_0:

**ppCO2_cap_pul_0** | *default:* 1 |break| initial pulmonary capillary CO2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppo2_cap_pul_0:

**ppO2_cap_pul_0** | *default:* 1 |break| initial pulmonary capillary O2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppco2_ven_pul_0:

**ppCO2_ven_pul_0** | *default:* 1 |break| initial pulmonary venous CO2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppo2_ven_pul_0:

**ppO2_ven_pul_0** | *default:* 1 |break| initial pulmonary venous O2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppco2_at_l_0:

**ppCO2_at_l_0** | *default:* 1 |break| initial left atrial CO2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppo2_at_l_0:

**ppO2_at_l_0** | *default:* 1 |break| initial left atrial O2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppco2_v_l_0:

**ppCO2_v_l_0** | *default:* 1 |break| initial left ventricular CO2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppo2_v_l_0:

**ppO2_v_l_0** | *default:* 1 |break| initial left ventricular O2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppco2_ar_sys_0:

**ppCO2_ar_sys_0** | *default:* 1 |break| initial systemic arterial CO2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppo2_ar_sys_0:

**ppO2_ar_sys_0** | *default:* 1 |break| initial systemic arterial O2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppco2_arspl_sys_0:

**ppCO2_arspl_sys_0** | *default:* 1 |break| initial systemic arterial splanchnic CO2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppo2_arspl_sys_0:

**ppO2_arspl_sys_0** | *default:* 1 |break| initial systemic arterial splanchnic O2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppco2_arespl_sys_0:

**ppCO2_arespl_sys_0** | *default:* 1 |break| initial systemic arterial extra-splanchnic CO2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppo2_arespl_sys_0:

**ppO2_arespl_sys_0** | *default:* 1 |break| initial systemic arterial extra-splanchnic O2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppco2_armsc_sys_0:

**ppCO2_armsc_sys_0** | *default:* 1 |break| initial systemic arterial muscular CO2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppo2_armsc_sys_0:

**ppO2_armsc_sys_0** | *default:* 1 |break| initial systemic arterial muscular O2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppco2_arcer_sys_0:

**ppCO2_arcer_sys_0** | *default:* 1 |break| initial systemic arterial cerebral CO2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppo2_arcer_sys_0:

**ppO2_arcer_sys_0** | *default:* 1 |break| initial systemic arterial cerebral O2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppco2_arcor_sys_0:

**ppCO2_arcor_sys_0** | *default:* 1 |break| initial systemic arterial coronary CO2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppo2_arcor_sys_0:

**ppO2_arcor_sys_0** | *default:* 1 |break| initial systemic arterial coronary O2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppco2_venspl_sys_0:

**ppCO2_venspl_sys_0** | *default:* 1 |break| initial systemic venous splanchnic CO2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppo2_venspl_sys_0:

**ppO2_venspl_sys_0** | *default:* 1 |break| initial systemic venous splanchnic O2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppco2_venespl_sys_0:

**ppCO2_venespl_sys_0** | *default:* 1 |break| initial systemic venous extra-splanchnic CO2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppo2_venespl_sys_0:

**ppO2_venespl_sys_0** | *default:* 1 |break| initial systemic venous extra-splanchnic O2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppco2_venmsc_sys_0:

**ppCO2_venmsc_sys_0** | *default:* 1 |break| initial systemic venous muscular CO2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppo2_venmsc_sys_0:

**ppO2_venmsc_sys_0** | *default:* 1 |break| initial systemic venous muscular O2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppco2_vencer_sys_0:

**ppCO2_vencer_sys_0** | *default:* 1 |break| initial systemic venous cerebral CO2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppo2_vencer_sys_0:

**ppO2_vencer_sys_0** | *default:* 1 |break| initial systemic venous cerebral O2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppco2_vencor_sys_0:

**ppCO2_vencor_sys_0** | *default:* 1 |break| initial systemic venous coronary CO2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppo2_vencor_sys_0:

**ppO2_vencor_sys_0** | *default:* 1 |break| initial systemic venous coronary O2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppco2_ven_sys_0:

**ppCO2_ven_sys_0** | *default:* 1 |break| initial systemic venous CO2 partial pressure

.. _cardiovascular0d-structurecoupling_respiratoryparameters_ppo2_ven_sys_0:

**ppO2_ven_sys_0** | *default:* 1 |break| initial systemic venous O2 partial pressure

.. _SECimmersedmethod:

IMMERSED METHOD
---------------

General parameters for any immersed problem

::

   ----------------------------------------------------IMMERSED METHOD

.. _immersedmethod_coupalgo:

**COUPALGO** | *default:* partitioned |break| Coupling strategies for immersed method.

   **Possible values:**

   - partitioned
   - monolithic

.. _immersedmethod_scheme:

**SCHEME** | *default:* dirichletneumann |break| Coupling schemes for partitioned immersed method.

   **Possible values:**

   - neumannneumann
   - dirichletneumann

.. _immersedmethod_divercont:

**DIVERCONT** | *default:* stop |break| What to do after maxiter is reached.

   **Possible values:**

   - stop
   - continue

.. _immersedmethod_output_evry_nlniter:

**OUTPUT_EVRY_NLNITER** | *default:* no |break| write output after every solution step of the nonlin. part. iter. scheme

   **Possible values:**

   - yes
   - no

.. _immersedmethod_correct_boundary_velocities:

**CORRECT_BOUNDARY_VELOCITIES** | *default:* no |break| correct velocities in fluid elements cut by surface of immersed structure

   **Possible values:**

   - yes
   - no

.. _immersedmethod_deform_background_mesh:

**DEFORM_BACKGROUND_MESH** | *default:* no |break| switch between immersed with fixed or deformable background mesh

   **Possible values:**

   - yes
   - no

.. _immersedmethod_timestats:

**TIMESTATS** | *default:* everyiter |break| summarize time monitor every nln iteration

   **Possible values:**

   - everyiter
   - endofsim

.. _immersedmethod_fld_srchradius_fac:

**FLD_SRCHRADIUS_FAC** | *default:* 1 |break| fac times fluid ele. diag. length

.. _immersedmethod_strct_srchradius_fac:

**STRCT_SRCHRADIUS_FAC** | *default:* 0.5 |break| fac times structure bounding box diagonal

.. _immersedmethod_num_gp_fluid_bound:

**NUM_GP_FLUID_BOUND** | *default:* 8 |break| number of gp in fluid elements cut by surface of immersed structure (higher number yields better mass conservation)

.. _SECimmersedmethod_partitionedsolver:

IMMERSED METHOD/PARTITIONED SOLVER
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ---------------------------------IMMERSED METHOD/PARTITIONED SOLVER

.. _immersedmethod_partitionedsolver_coupalgo:

**COUPALGO** | *default:* iter_stagg_fixed_rel_param |break| Iteration Scheme over the fields

   **Possible values:**

   - basic_sequ_stagg
   - iter_stagg_fixed_rel_param
   - iter_stagg_AITKEN_rel_param

.. _immersedmethod_partitionedsolver_predictor:

**PREDICTOR** | *default:* d(n) |break| Predictor for interface displacements

   **Possible values:**

   - d(n)
   - d(n)+dt*(1.5*v(n)-0.5*v(n-1))
   - d(n)+dt*v(n)
   - d(n)+dt*v(n)+0.5*dt^2*a(n)

.. _immersedmethod_partitionedsolver_coupvariable:

**COUPVARIABLE** | *default:* Displacement |break| Coupling variable at the fsi interface

   **Possible values:**

   - Displacement
   - Force

.. _immersedmethod_partitionedsolver_convtol:

**CONVTOL** | *default:* 1e-06 |break| Tolerance for iteration over fields in case of partitioned scheme

.. _immersedmethod_partitionedsolver_relax:

**RELAX** | *default:* 1 |break| fixed relaxation parameter for partitioned FSI solvers

.. _immersedmethod_partitionedsolver_maxomega:

**MAXOMEGA** | *default:* 0 |break| largest omega allowed for Aitken relaxation (0.0 means no constraint)

.. _immersedmethod_partitionedsolver_itemax:

**ITEMAX** | *default:* 100 |break| Maximum number of iterations over fields

.. _SECfpsidynamic:

FPSI DYNAMIC
------------

Fluid Porous Structure Interaction
FPSI solver with various coupling methods

::

   -------------------------------------------------------FPSI DYNAMIC

.. _fpsidynamic_coupalgo:

**COUPALGO** | *default:* fpsi_monolithic_plain |break| Iteration Scheme over the fields

   **Possible values:**

   - fpsi_monolithic_plain

.. _fpsidynamic_shapederivatives:

**SHAPEDERIVATIVES** | *default:* No |break| Include linearization with respect to mesh movement in Navier Stokes equation.
Supported in monolithic FPSI for now.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fpsidynamic_useshapederivatives:

**USESHAPEDERIVATIVES** | *default:* No |break| Add linearization with respect to mesh movement in Navier Stokes equation to stiffness matrix.
Supported in monolithic FPSI for now.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fpsidynamic_partitioned:

**PARTITIONED** | *default:* RobinNeumann |break| Coupling strategies for partitioned FPSI solvers.

   **Possible values:**

   - RobinNeumann
   - monolithic
   - nocoupling

.. _fpsidynamic_secondorder:

**SECONDORDER** | *default:* No |break| Second order coupling at the interface.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fpsidynamic_restol:

**RESTOL** | *default:* 1e-8 1e-8 1e-8 1e-8 1e-8 1e-8 |break| Tolerances for single fields in the residual norm for the Newton iteration. 
For NORM_RESF != Abs_sys_split only the first value is used for all fields. 
Order of fields: porofluidvelocity, porofluidpressure, porostructure, fluidvelocity, fluidpressure, ale

.. _fpsidynamic_inctol:

**INCTOL** | *default:* 1e-8 1e-8 1e-8 1e-8 1e-8 1e-8 |break| Tolerance in the increment norm for the Newton iteration. 
For NORM_INC != \*_split only the first value is used for all fields. 
Order of fields: porofluidvelocity, porofluidpressure, porostructure, fluidvelocity, fluidpressure, ale

.. _fpsidynamic_norm_inc:

**NORM_INC** | *default:* Abs |break| Type of norm for primary variables convergence check.  
Abs: absolute values, Abs_sys_split: absolute values with correction of systemsize for every field seperate, Rel_sys: relative values with correction of systemsize.

   **Possible values:**

   - Abs
   - Abs_sys_split
   - Rel_sys

.. _fpsidynamic_norm_resf:

**NORM_RESF** | *default:* Abs |break| Type of norm for primary variables convergence check. 
Abs: absolute values, Abs_sys_split: absolute values with correction of systemsize for every field seperate, Rel_sys: relative values with correction of systemsize.

   **Possible values:**

   - Abs
   - Abs_sys_split
   - Rel_sys

.. _fpsidynamic_normcombi_resfinc:

**NORMCOMBI_RESFINC** | *default:* And |break| binary operator to combine primary variables and residual force values

   **Possible values:**

   - And
   - Or

.. _fpsidynamic_linesearch:

**LineSearch** | *default:* No |break| adapt increment in case of non-monotonic residual convergence or residual oscillations

   **Possible values:**

   - Yes
   - No

.. _fpsidynamic_fdcheck:

**FDCheck** | *default:* No |break| perform FPSIFDCheck() finite difference check

   **Possible values:**

   - Yes
   - No

.. _fpsidynamic_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| number of linear solver used for FPSI problems

.. _fpsidynamic_itemax:

**ITEMAX** | *default:* 100 |break| Maximum number of iterations over fields

.. _fpsidynamic_itemin:

**ITEMIN** | *default:* 1 |break| minimal number of iterations over fields

.. _fpsidynamic_numstep:

**NUMSTEP** | *default:* 200 |break| Total number of Timesteps

.. _fpsidynamic_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| Increment for writing solution

.. _fpsidynamic_restartevry:

**RESTARTEVRY** | *default:* 1 |break| Increment for writing restart

.. _fpsidynamic_fdcheck_row:

**FDCheck_row** | *default:* 0 |break| print row value during FDCheck

.. _fpsidynamic_fdcheck_column:

**FDCheck_column** | *default:* 0 |break| print column value during FDCheck

.. _fpsidynamic_timestep:

**TIMESTEP** | *default:* 0.1 |break| Time increment dt

.. _fpsidynamic_maxtime:

**MAXTIME** | *default:* 1000 |break| Total simulation time

.. _fpsidynamic_convtol:

**CONVTOL** | *default:* 1e-06 |break| Tolerance for iteration over fields

.. _fpsidynamic_alphabj:

**ALPHABJ** | *default:* 1 |break| Beavers-Joseph-Coefficient for Slip-Boundary-Condition at Fluid-Porous-Interface (0.1-4)

.. _SECfluidbeaminteraction:

FLUID BEAM INTERACTION
----------------------

no description yet

::

   ---------------------------------------------FLUID BEAM INTERACTION

.. _fluidbeaminteraction_coupling:

**COUPLING** | *default:* two-way |break| Type of FBI coupling

   **Possible values:**

   - two-way
   - fluid
   - solid

.. _fluidbeaminteraction_startstep:

**STARTSTEP** | *default:* 0 |break| Time Step at which to begin the fluid beam coupling. Usually this will be the first step.

.. _fluidbeaminteraction_presort_strategy:

**PRESORT_STRATEGY** | *default:* bruteforce |break| Presort strategy for the beam elements

   **Possible values:**

   - bruteforce
   - binning

.. _SECfluidbeaminteraction_beamtofluidmeshtying:

FLUID BEAM INTERACTION/BEAM TO FLUID MESHTYING
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ---------------------FLUID BEAM INTERACTION/BEAM TO FLUID MESHTYING

.. _fluidbeaminteraction_beamtofluidmeshtying_meshtying_discretization:

**MESHTYING_DISCRETIZATION** | *default:* none |break| Type of employed meshtying discretization

   **Possible values:**

   - none
   - gauss_point_to_segment
   - mortar

.. _fluidbeaminteraction_beamtofluidmeshtying_constraint_strategy:

**CONSTRAINT_STRATEGY** | *default:* none |break| Type of employed constraint enforcement strategy

   **Possible values:**

   - none
   - penalty

.. _fluidbeaminteraction_beamtofluidmeshtying_penalty_parameter:

**PENALTY_PARAMETER** | *default:* 0 |break| Penalty parameter for beam-to-Fluid volume meshtying

.. _fluidbeaminteraction_beamtofluidmeshtying_search_radius:

**SEARCH_RADIUS** | *default:* 1000 |break| Absolute Search radius for beam-to-fluid volume meshtying. Choose carefully to not blow up memory demand but to still find all interaction pairs!

.. _fluidbeaminteraction_beamtofluidmeshtying_mortar_shape_function:

**MORTAR_SHAPE_FUNCTION** | *default:* none |break| Shape function for the mortar Lagrange-multipliers

   **Possible values:**

   - none
   - line2
   - line3
   - line4

.. _fluidbeaminteraction_beamtofluidmeshtying_geometry_pair_strategy:

**GEOMETRY_PAIR_STRATEGY** | *default:* segmentation |break| Type of employed segmentation strategy

   **Possible values:**

   - none
   - segmentation
   - gauss_point_projection_without_boundary_segmentation
   - gauss_point_projection_boundary_segmentation
   - gauss_point_projection_cross_section

.. _fluidbeaminteraction_beamtofluidmeshtying_geometry_pair_search_points:

**GEOMETRY_PAIR_SEARCH_POINTS** | *default:* 6 |break| Number of search points for segmentation

.. _fluidbeaminteraction_beamtofluidmeshtying_gauss_points:

**GAUSS_POINTS** | *default:* 6 |break| Number of Gauss Points for the integral evaluations

.. _fluidbeaminteraction_beamtofluidmeshtying_integration_points_circumference:

**INTEGRATION_POINTS_CIRCUMFERENCE** | *default:* 6 |break| Number of Integration points along the circumferencial direction of the beam. This is parameter is only used in beam to cylinder meshtying. No gauss integration is used along the circumferencial direction, equally spaced integration points are used.

.. _SECfluidbeaminteraction_beamtofluidmeshtying_runtimevtkoutput:

FLUID BEAM INTERACTION/BEAM TO FLUID MESHTYING/RUNTIME VTK OUTPUT
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   --FLUID BEAM INTERACTION/BEAM TO FLUID MESHTYING/RUNTIME VTK OUTPUT

.. _fluidbeaminteraction_beamtofluidmeshtying_runtimevtkoutput_write_output:

**WRITE_OUTPUT** | *default:* No |break| Enable / disable beam-to-fluid mesh tying output.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluidbeaminteraction_beamtofluidmeshtying_runtimevtkoutput_nodal_forces:

**NODAL_FORCES** | *default:* No |break| Enable / disable output of the resulting nodal forces due to beam to Fluid interaction.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluidbeaminteraction_beamtofluidmeshtying_runtimevtkoutput_segmentation:

**SEGMENTATION** | *default:* No |break| Enable / disable output of segmentation points.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluidbeaminteraction_beamtofluidmeshtying_runtimevtkoutput_integration_points:

**INTEGRATION_POINTS** | *default:* No |break| Enable / disable output of used integration points. If the meshtying method has 'forces' at the integration point, they will also be output.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluidbeaminteraction_beamtofluidmeshtying_runtimevtkoutput_constraint_violation:

**CONSTRAINT_VIOLATION** | *default:* No |break| Enable / disable output of the constraint violation into a output_name.penalty csv file.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluidbeaminteraction_beamtofluidmeshtying_runtimevtkoutput_mortar_lambda_discret:

**MORTAR_LAMBDA_DISCRET** | *default:* No |break| Enable / disable output of the discrete Lagrange multipliers at the node of the Lagrange multiplier shape functions.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluidbeaminteraction_beamtofluidmeshtying_runtimevtkoutput_mortar_lambda_continuous:

**MORTAR_LAMBDA_CONTINUOUS** | *default:* No |break| Enable / disable output of the continuous Lagrange multipliers function along the beam.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _fluidbeaminteraction_beamtofluidmeshtying_runtimevtkoutput_mortar_lambda_continuous_segments:

**MORTAR_LAMBDA_CONTINUOUS_SEGMENTS** | *default:* 5 |break| Number of segments for continuous mortar output

.. _SECparticledynamic:

PARTICLE DYNAMIC
----------------

control parameters for particle simulations


::

   ---------------------------------------------------PARTICLE DYNAMIC

.. _particledynamic_dynamictyp:

**DYNAMICTYP** | *default:* VelocityVerlet |break| type of particle time integration

   **Possible values:**

   - SemiImplicitEuler
   - VelocityVerlet

.. _particledynamic_interaction:

**INTERACTION** | *default:* None |break| type of particle interaction

   **Possible values:**

   - None
   - SPH
   - DEM

.. _particledynamic_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| write particle runtime output every RESULTSEVRY steps

.. _particledynamic_restartevry:

**RESTARTEVRY** | *default:* 1 |break| write restart possibility every RESTARTEVRY steps

.. _particledynamic_output_data_format:

**OUTPUT_DATA_FORMAT** | *default:* Binary |break| data format for written numeric data

   **Possible values:**

   - Binary
   - ASCII

.. _particledynamic_write_ghosted_particles:

**WRITE_GHOSTED_PARTICLES** | *default:* no |break| write ghosted particles (debug feature)

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _particledynamic_timestep:

**TIMESTEP** | *default:* 0.01 |break| time step size

.. _particledynamic_numstep:

**NUMSTEP** | *default:* 100 |break| maximum number of steps

.. _particledynamic_maxtime:

**MAXTIME** | *default:* 1 |break| maximum time

.. _particledynamic_gravity_acceleration:

**GRAVITY_ACCELERATION** | *default:* 0.0 0.0 0.0 |break| acceleration due to gravity

.. _particledynamic_gravity_ramp_funct:

**GRAVITY_RAMP_FUNCT** | *default:* -1 |break| number of function governing gravity ramp

.. _particledynamic_viscous_damping:

**VISCOUS_DAMPING** | *default:* -1 |break| apply viscous damping force to determine static equilibrium solutions

.. _particledynamic_transfer_every:

**TRANSFER_EVERY** | *default:* no |break| transfer particles to new bins every time step

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _particledynamic_phase_to_dynloadbalfac:

**PHASE_TO_DYNLOADBALFAC** | *default:* none |break| considered particle phases with dynamic load balance weighting factor

.. _particledynamic_phase_to_material_id:

**PHASE_TO_MATERIAL_ID** | *default:* none |break| relate particle phase to material id

.. _particledynamic_initial_position_amplitude:

**INITIAL_POSITION_AMPLITUDE** | *default:* 0.0 0.0 0.0 |break| amplitude of noise added to initial position for each spatial direction

.. _particledynamic_particle_wall_source:

**PARTICLE_WALL_SOURCE** | *default:* NoParticleWall |break| type of particle wall source

   **Possible values:**

   - NoParticleWall
   - DiscretCondition
   - BoundingBox

.. _particledynamic_particle_wall_mat:

**PARTICLE_WALL_MAT** | *default:* -1 |break| material id for particle wall from bounding box source

.. _particledynamic_particle_wall_moving:

**PARTICLE_WALL_MOVING** | *default:* no |break| consider a moving particle wall

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _particledynamic_particle_wall_loaded:

**PARTICLE_WALL_LOADED** | *default:* no |break| consider loading on particle wall

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _particledynamic_rigid_body_motion:

**RIGID_BODY_MOTION** | *default:* no |break| consider rigid body motion

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _particledynamic_rigid_body_phasechange_radius:

**RIGID_BODY_PHASECHANGE_RADIUS** | *default:* -1 |break| search radius for neighboring rigid bodies in case of phase change

.. _SECparticledynamic_initialandboundaryconditions:

PARTICLE DYNAMIC/INITIAL AND BOUNDARY CONDITIONS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

control parameters for initial/boundary conditions in particle simulations


::

   -------------------PARTICLE DYNAMIC/INITIAL AND BOUNDARY CONDITIONS

.. _particledynamic_initialandboundaryconditions_initial_temp_field:

**INITIAL_TEMP_FIELD** | *default:* none |break| initial temperature field of particle phase given by function

.. _particledynamic_initialandboundaryconditions_initial_velocity_field:

**INITIAL_VELOCITY_FIELD** | *default:* none |break| initial velocity field of particle phase given by function

.. _particledynamic_initialandboundaryconditions_initial_acceleration_field:

**INITIAL_ACCELERATION_FIELD** | *default:* none |break| initial acceleration field of particle phase given by function

.. _particledynamic_initialandboundaryconditions_dirichlet_boundary_condition:

**DIRICHLET_BOUNDARY_CONDITION** | *default:* none |break| dirichlet boundary condition of particle phase given by function

.. _particledynamic_initialandboundaryconditions_temperature_boundary_condition:

**TEMPERATURE_BOUNDARY_CONDITION** | *default:* none |break| temperature boundary condition of particle phase given by function

.. _SECparticledynamic_sph:

PARTICLE DYNAMIC/SPH
~~~~~~~~~~~~~~~~~~~~

control parameters for smoothed particle hydrodynamics (SPH) simulations


::

   -----------------------------------------------PARTICLE DYNAMIC/SPH

.. _particledynamic_sph_write_particle_wall_interaction:

**WRITE_PARTICLE_WALL_INTERACTION** | *default:* no |break| write particle-wall interaction output

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _particledynamic_sph_kernel:

**KERNEL** | *default:* CubicSpline |break| type of smoothed particle hydrodynamics kernel

   **Possible values:**

   - CubicSpline
   - QuinticSpline

.. _particledynamic_sph_kernel_space_dim:

**KERNEL_SPACE_DIM** | *default:* Kernel3D |break| kernel space dimension number

   **Possible values:**

   - Kernel1D
   - Kernel2D
   - Kernel3D

.. _particledynamic_sph_initialparticlespacing:

**INITIALPARTICLESPACING** | *default:* 0 |break| initial spacing of particles

.. _particledynamic_sph_equationofstate:

**EQUATIONOFSTATE** | *default:* GenTait |break| type of smoothed particle hydrodynamics equation of state

   **Possible values:**

   - GenTait
   - IdealGas

.. _particledynamic_sph_momentumformulation:

**MOMENTUMFORMULATION** | *default:* AdamiMomentumFormulation |break| type of smoothed particle hydrodynamics momentum formulation

   **Possible values:**

   - AdamiMomentumFormulation
   - MonaghanMomentumFormulation

.. _particledynamic_sph_densityevaluation:

**DENSITYEVALUATION** | *default:* DensitySummation |break| type of density evaluation scheme

   **Possible values:**

   - DensitySummation
   - DensityIntegration
   - DensityPredictCorrect

.. _particledynamic_sph_densitycorrection:

**DENSITYCORRECTION** | *default:* NoCorrection |break| type of density correction scheme

   **Possible values:**

   - NoCorrection
   - InteriorCorrection
   - NormalizedCorrection
   - RandlesCorrection

.. _particledynamic_sph_boundaryparticleformulation:

**BOUNDARYPARTICLEFORMULATION** | *default:* NoBoundaryFormulation |break| type of boundary particle formulation

   **Possible values:**

   - NoBoundaryFormulation
   - AdamiBoundaryFormulation

.. _particledynamic_sph_boundaryparticleinteraction:

**BOUNDARYPARTICLEINTERACTION** | *default:* NoSlipBoundaryParticle |break| type of boundary particle interaction

   **Possible values:**

   - NoSlipBoundaryParticle
   - FreeSlipBoundaryParticle

.. _particledynamic_sph_wallformulation:

**WALLFORMULATION** | *default:* NoWallFormulation |break| type of wall formulation

   **Possible values:**

   - NoWallFormulation
   - VirtualParticleWallFormulation

.. _particledynamic_sph_transportvelocityformulation:

**TRANSPORTVELOCITYFORMULATION** | *default:* NoTransportVelocity |break| type of transport velocity formulation

   **Possible values:**

   - NoTransportVelocity
   - StandardTransportVelocity
   - GeneralizedTransportVelocity

.. _particledynamic_sph_temperatureevaluation:

**TEMPERATUREEVALUATION** | *default:* NoTemperatureEvaluation |break| type of temperature evaluation scheme

   **Possible values:**

   - NoTemperatureEvaluation
   - TemperatureIntegration

.. _particledynamic_sph_temperaturegradient:

**TEMPERATUREGRADIENT** | *default:* no |break| evaluate temperature gradient

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _particledynamic_sph_heatsourcetype:

**HEATSOURCETYPE** | *default:* NoHeatSource |break| type of heat source

   **Possible values:**

   - NoHeatSource
   - VolumeHeatSource
   - SurfaceHeatSource

.. _particledynamic_sph_heatsource_funct:

**HEATSOURCE_FUNCT** | *default:* -1 |break| number of function governing heat source

.. _particledynamic_sph_heatsource_direction:

**HEATSOURCE_DIRECTION** | *default:* 0.0 0.0 0.0 |break| direction of surface heat source

.. _particledynamic_sph_vapor_heatloss:

**VAPOR_HEATLOSS** | *default:* no |break| evaluate evaporation induced heat loss

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _particledynamic_sph_vapor_heatloss_latentheat:

**VAPOR_HEATLOSS_LATENTHEAT** | *default:* 0 |break| latent heat in heat loss formula

.. _particledynamic_sph_vapor_heatloss_enthalpy_reftemp:

**VAPOR_HEATLOSS_ENTHALPY_REFTEMP** | *default:* 0 |break| enthalpy reference temperature in heat loss formula

.. _particledynamic_sph_vapor_heatloss_pfac:

**VAPOR_HEATLOSS_PFAC** | *default:* 0 |break| pressure factor in heat loss formula

.. _particledynamic_sph_vapor_heatloss_tfac:

**VAPOR_HEATLOSS_TFAC** | *default:* 0 |break| temperature factor in heat loss formula

.. _particledynamic_sph_vapor_recoil:

**VAPOR_RECOIL** | *default:* no |break| evaluate evaporation induced recoil pressure

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _particledynamic_sph_vapor_recoil_boilingtemperature:

**VAPOR_RECOIL_BOILINGTEMPERATURE** | *default:* 0 |break| boiling temperature in recoil pressure formula

.. _particledynamic_sph_vapor_recoil_pfac:

**VAPOR_RECOIL_PFAC** | *default:* 0 |break| pressure factor in recoil pressure formula

.. _particledynamic_sph_vapor_recoil_tfac:

**VAPOR_RECOIL_TFAC** | *default:* 0 |break| temperature factor in recoil pressure formula

.. _particledynamic_sph_surfacetensionformulation:

**SURFACETENSIONFORMULATION** | *default:* NoSurfaceTension |break| type of surface tension formulation

   **Possible values:**

   - NoSurfaceTension
   - ContinuumSurfaceForce

.. _particledynamic_sph_surfacetension_ramp_funct:

**SURFACETENSION_RAMP_FUNCT** | *default:* -1 |break| number of function governing surface tension ramp

.. _particledynamic_sph_surfacetensioncoefficient:

**SURFACETENSIONCOEFFICIENT** | *default:* -1 |break| constant part of surface tension coefficient

.. _particledynamic_sph_surfacetensionminimum:

**SURFACETENSIONMINIMUM** | *default:* 0 |break| minimum surface tension coefficient in case of temperature dependence

.. _particledynamic_sph_surfacetensiontempfac:

**SURFACETENSIONTEMPFAC** | *default:* 0 |break| factor of dependence of surface tension coefficient on temperature

.. _particledynamic_sph_surfacetensionreftemp:

**SURFACETENSIONREFTEMP** | *default:* 0 |break| reference temperature for surface tension coefficient

.. _particledynamic_sph_staticcontactangle:

**STATICCONTACTANGLE** | *default:* 0 |break| static contact angle in degree with wetting effects

.. _particledynamic_sph_triplepointnormal_corr_cf_low:

**TRIPLEPOINTNORMAL_CORR_CF_LOW** | *default:* 0 |break| triple point normal correction wall color field low

.. _particledynamic_sph_triplepointnormal_corr_cf_up:

**TRIPLEPOINTNORMAL_CORR_CF_UP** | *default:* 0 |break| triple point normal correction wall color field up

.. _particledynamic_sph_interface_viscosity:

**INTERFACE_VISCOSITY** | *default:* no |break| evaluate interface viscosity

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _particledynamic_sph_interface_viscosity_liquidgas:

**INTERFACE_VISCOSITY_LIQUIDGAS** | *default:* 0 |break| artificial viscosity on liquid-gas interface

.. _particledynamic_sph_interface_viscosity_solidliquid:

**INTERFACE_VISCOSITY_SOLIDLIQUID** | *default:* 0 |break| artificial viscosity on solid-liquid interface

.. _particledynamic_sph_barrier_force:

**BARRIER_FORCE** | *default:* no |break| evaluate barrier force

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _particledynamic_sph_barrier_force_distance:

**BARRIER_FORCE_DISTANCE** | *default:* 0 |break| barrier force distance

.. _particledynamic_sph_barrier_force_tempscale:

**BARRIER_FORCE_TEMPSCALE** | *default:* 0 |break| barrier force temperature scaling

.. _particledynamic_sph_barrier_force_stiff_heavy:

**BARRIER_FORCE_STIFF_HEAVY** | *default:* -1 |break| barrier force stiffness of heavy phase

.. _particledynamic_sph_barrier_force_damp_heavy:

**BARRIER_FORCE_DAMP_HEAVY** | *default:* 0 |break| barrier force damping parameter of heavy phase

.. _particledynamic_sph_barrier_force_stiff_gas:

**BARRIER_FORCE_STIFF_GAS** | *default:* -1 |break| barrier force stiffness of gas phase

.. _particledynamic_sph_barrier_force_damp_gas:

**BARRIER_FORCE_DAMP_GAS** | *default:* 0 |break| barrier force damping parameter of gas phase

.. _particledynamic_sph_trans_ref_temperature:

**TRANS_REF_TEMPERATURE** | *default:* 0 |break| transition reference temperature

.. _particledynamic_sph_trans_dt_surfacetension:

**TRANS_DT_SURFACETENSION** | *default:* 0 |break| transition temperature difference for surface tension evaluation

.. _particledynamic_sph_trans_dt_marangoni:

**TRANS_DT_MARANGONI** | *default:* 0 |break| transition temperature difference for marangoni evaluation

.. _particledynamic_sph_trans_dt_curvature:

**TRANS_DT_CURVATURE** | *default:* 0 |break| transition temperature difference for curvature evaluation

.. _particledynamic_sph_trans_dt_wetting:

**TRANS_DT_WETTING** | *default:* 0 |break| transition temperature difference for wetting evaluation

.. _particledynamic_sph_trans_dt_intvisc:

**TRANS_DT_INTVISC** | *default:* 0 |break| transition temperature difference for interface viscosity evaluation

.. _particledynamic_sph_trans_dt_barrier:

**TRANS_DT_BARRIER** | *default:* 0 |break| transition temperature difference for barrier force evaluation

.. _particledynamic_sph_dirichletboundarytype:

**DIRICHLETBOUNDARYTYPE** | *default:* NoDirichletOpenBoundary |break| type of dirichlet open boundary

   **Possible values:**

   - NoDirichletOpenBoundary
   - DirichletNormalToPlane

.. _particledynamic_sph_dirichlet_funct:

**DIRICHLET_FUNCT** | *default:* -1 |break| number of function governing velocity condition on dirichlet open boundary

.. _particledynamic_sph_dirichlet_outward_normal:

**DIRICHLET_OUTWARD_NORMAL** | *default:* 0.0 0.0 0.0 |break| direction of outward normal on dirichlet open boundary

.. _particledynamic_sph_dirichlet_plane_point:

**DIRICHLET_PLANE_POINT** | *default:* 0.0 0.0 0.0 |break| point on dirichlet open boundary plane

.. _particledynamic_sph_neumannboundarytype:

**NEUMANNBOUNDARYTYPE** | *default:* NoNeumannOpenBoundary |break| type of neumann open boundary

   **Possible values:**

   - NoNeumannOpenBoundary
   - NeumannNormalToPlane

.. _particledynamic_sph_neumann_funct:

**NEUMANN_FUNCT** | *default:* -1 |break| number of function governing pressure condition on neumann open boundary

.. _particledynamic_sph_neumann_outward_normal:

**NEUMANN_OUTWARD_NORMAL** | *default:* 0.0 0.0 0.0 |break| direction of outward normal on neumann open boundary

.. _particledynamic_sph_neumann_plane_point:

**NEUMANN_PLANE_POINT** | *default:* 0.0 0.0 0.0 |break| point on neumann open boundary plane

.. _particledynamic_sph_phasechangetype:

**PHASECHANGETYPE** | *default:* NoPhaseChange |break| type of phase change

   **Possible values:**

   - NoPhaseChange
   - OneWayScalarBelowToAbovePhaseChange
   - OneWayScalarAboveToBelowPhaseChange
   - TwoWayScalarPhaseChange

.. _particledynamic_sph_phasechangedefinition:

**PHASECHANGEDEFINITION** | *default:* none |break| phase change definition

.. _particledynamic_sph_rigidparticlecontacttype:

**RIGIDPARTICLECONTACTTYPE** | *default:* NoRigidParticleContact |break| type of rigid particle contact

   **Possible values:**

   - NoRigidParticleContact
   - ElasticRigidParticleContact

.. _particledynamic_sph_rigidparticlecontactstiff:

**RIGIDPARTICLECONTACTSTIFF** | *default:* -1 |break| rigid particle contact stiffness

.. _particledynamic_sph_rigidparticlecontactdamp:

**RIGIDPARTICLECONTACTDAMP** | *default:* 0 |break| rigid particle contact damping parameter

.. _SECparticledynamic_dem:

PARTICLE DYNAMIC/DEM
~~~~~~~~~~~~~~~~~~~~

control parameters for discrete element method (DEM) simulations


::

   -----------------------------------------------PARTICLE DYNAMIC/DEM

.. _particledynamic_dem_write_particle_energy:

**WRITE_PARTICLE_ENERGY** | *default:* no |break| write particle energy output

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _particledynamic_dem_write_particle_wall_interaction:

**WRITE_PARTICLE_WALL_INTERACTION** | *default:* no |break| write particle-wall interaction output

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _particledynamic_dem_normalcontactlaw:

**NORMALCONTACTLAW** | *default:* NormalLinearSpring |break| normal contact law for particles

   **Possible values:**

   - NormalLinearSpring
   - NormalLinearSpringDamp
   - NormalHertz
   - NormalLeeHerrmann
   - NormalKuwabaraKono
   - NormalTsuji

.. _particledynamic_dem_tangentialcontactlaw:

**TANGENTIALCONTACTLAW** | *default:* NoTangentialContact |break| tangential contact law for particles

   **Possible values:**

   - NoTangentialContact
   - TangentialLinSpringDamp

.. _particledynamic_dem_rollingcontactlaw:

**ROLLINGCONTACTLAW** | *default:* NoRollingContact |break| rolling contact law for particles

   **Possible values:**

   - NoRollingContact
   - RollingViscous
   - RollingCoulomb

.. _particledynamic_dem_adhesionlaw:

**ADHESIONLAW** | *default:* NoAdhesion |break| type of adhesion law for particles

   **Possible values:**

   - NoAdhesion
   - AdhesionVdWDMT
   - AdhesionRegDMT

.. _particledynamic_dem_adhesion_surface_energy_distribution:

**ADHESION_SURFACE_ENERGY_DISTRIBUTION** | *default:* ConstantSurfaceEnergy |break| type of (random) surface energy distribution

   **Possible values:**

   - ConstantSurfaceEnergy
   - NormalSurfaceEnergyDistribution
   - LogNormalSurfaceEnergyDistribution

.. _particledynamic_dem_min_radius:

**MIN_RADIUS** | *default:* 0 |break| minimum allowed particle radius

.. _particledynamic_dem_max_radius:

**MAX_RADIUS** | *default:* 0 |break| maximum allowed particle radius

.. _particledynamic_dem_max_velocity:

**MAX_VELOCITY** | *default:* -1 |break| maximum expected particle velocity

.. _particledynamic_dem_initial_radius:

**INITIAL_RADIUS** | *default:* RadiusFromParticleMaterial |break| type of initial particle radius assignment

   **Possible values:**

   - RadiusFromParticleMaterial
   - RadiusFromParticleInput
   - NormalRadiusDistribution
   - LogNormalRadiusDistribution

.. _particledynamic_dem_radiusdistribution_sigma:

**RADIUSDISTRIBUTION_SIGMA** | *default:* -1 |break| sigma of random particle radius distribution

.. _particledynamic_dem_rel_penetration:

**REL_PENETRATION** | *default:* -1 |break| maximum allowed relative penetration

.. _particledynamic_dem_normal_stiff:

**NORMAL_STIFF** | *default:* -1 |break| normal contact stiffness

.. _particledynamic_dem_normal_damp:

**NORMAL_DAMP** | *default:* -1 |break| normal contact damping parameter

.. _particledynamic_dem_coeff_restitution:

**COEFF_RESTITUTION** | *default:* -1 |break| coefficient of restitution

.. _particledynamic_dem_damp_reg_fac:

**DAMP_REG_FAC** | *default:* -1 |break| linearly regularized damping normal force in the interval :math:`|g| < (DAMP_REG_FAC * r_{\min})`

.. _particledynamic_dem_tension_cutoff:

**TENSION_CUTOFF** | *default:* yes |break| evaluate tension cutoff of normal contact force

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _particledynamic_dem_poisson_ratio:

**POISSON_RATIO** | *default:* -1 |break| poisson ratio

.. _particledynamic_dem_young_modulus:

**YOUNG_MODULUS** | *default:* -1 |break| young's modulus

.. _particledynamic_dem_frict_coeff_tang:

**FRICT_COEFF_TANG** | *default:* -1 |break| friction coefficient for tangential contact

.. _particledynamic_dem_frict_coeff_roll:

**FRICT_COEFF_ROLL** | *default:* -1 |break| friction coefficient for rolling contact

.. _particledynamic_dem_adhesion_distance:

**ADHESION_DISTANCE** | *default:* -1 |break| adhesion distance between interacting surfaces

.. _particledynamic_dem_adhesion_max_contact_pressure:

**ADHESION_MAX_CONTACT_PRESSURE** | *default:* 0 |break| adhesion maximum contact pressure

.. _particledynamic_dem_adhesion_max_contact_force:

**ADHESION_MAX_CONTACT_FORCE** | *default:* 0 |break| adhesion maximum contact force

.. _particledynamic_dem_adhesion_use_max_contact_force:

**ADHESION_USE_MAX_CONTACT_FORCE** | *default:* no |break| use maximum contact force instead of maximum contact pressure

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _particledynamic_dem_adhesion_vdw_curve_shift:

**ADHESION_VDW_CURVE_SHIFT** | *default:* no |break| shifts van-der-Waals-curve to g = 0

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _particledynamic_dem_adhesion_hamaker:

**ADHESION_HAMAKER** | *default:* -1 |break| hamaker constant of van-der-Waals interaction

.. _particledynamic_dem_adhesion_surface_energy:

**ADHESION_SURFACE_ENERGY** | *default:* -1 |break| adhesion surface energy for the calculation of the pull-out force

.. _particledynamic_dem_adhesion_surface_energy_distribution_var:

**ADHESION_SURFACE_ENERGY_DISTRIBUTION_VAR** | *default:* -1 |break| variance of adhesion surface energy distribution

.. _particledynamic_dem_adhesion_surface_energy_distribution_cutoff_factor:

**ADHESION_SURFACE_ENERGY_DISTRIBUTION_CUTOFF_FACTOR** | *default:* -1 |break| adhesion surface energy distribution limited by multiple of variance

.. _particledynamic_dem_adhesion_surface_energy_factor:

**ADHESION_SURFACE_ENERGY_FACTOR** | *default:* 1 |break| factor to calculate minimum adhesion surface energy

.. _SECmor:

MOR
---

no description yet

::

   ----------------------------------------------------------------MOR

.. _mor_pod_matrix:

**POD_MATRIX** | *default:* none |break| filename of file containing projection matrix

.. _SECelectromagneticdynamic:

ELECTROMAGNETIC DYNAMIC
-----------------------

control parameters for electromagnetic problems


::

   --------------------------------------------ELECTROMAGNETIC DYNAMIC

.. _electromagneticdynamic_timestep:

**TIMESTEP** | *default:* 0.01 |break| Time-step length dt

.. _electromagneticdynamic_tau:

**TAU** | *default:* 1 |break| Stabilization parameter

.. _electromagneticdynamic_numstep:

**NUMSTEP** | *default:* 100 |break| Number of time steps

.. _electromagneticdynamic_maxtime:

**MAXTIME** | *default:* 1 |break| Total simulation time

.. _electromagneticdynamic_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| Increment for writing solution

.. _electromagneticdynamic_restartevry:

**RESTARTEVRY** | *default:* 1 |break| Increment for writing restart

.. _electromagneticdynamic_linear_solver:

**LINEAR_SOLVER** | *default:* -1 |break| Number of linear solver used for electromagnetic problem

.. _electromagneticdynamic_startfuncno:

**STARTFUNCNO** | *default:* -1 |break| Function for initial field

.. _electromagneticdynamic_sourcefuncno:

**SOURCEFUNCNO** | *default:* -1 |break| Function for source term in volume

.. _electromagneticdynamic_timeint:

**TIMEINT** | *default:* One_Step_Theta |break| Type of time integration scheme

   **Possible values:**

   - One_Step_Theta
   - BDF1
   - BDF2
   - BDF4
   - GenAlpha
   - Explicit_Euler
   - Runge_Kutta
   - Crank_Nicolson

.. _electromagneticdynamic_initialfield:

**INITIALFIELD** | *default:* zero_field |break| Initial field for ele problem

   **Possible values:**

   - zero_field
   - field_by_function
   - field_by_steady_state
   - field_by_steady_state_hdg

.. _electromagneticdynamic_calcerr:

**CALCERR** | *default:* No |break| Calc the error wrt ERRORFUNCNO?

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _electromagneticdynamic_postprocess:

**POSTPROCESS** | *default:* No |break| Postprocess solution? (very slow)

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _electromagneticdynamic_errorfuncno:

**ERRORFUNCNO** | *default:* -1 |break| Function for error calculation

.. _electromagneticdynamic_equilibration:

**EQUILIBRATION** | *default:* none |break| flag for equilibration of global system of equations

   **Possible values:**

   - none
   - rows_full
   - rows_maindiag
   - columns_full
   - columns_maindiag
   - rowsandcolumns_full
   - rowsandcolumns_maindiag

.. _SECsearchtree:

SEARCH TREE
-----------

no description yet

::

   --------------------------------------------------------SEARCH TREE

.. _searchtree_tree_type:

**TREE_TYPE** | *default:* notree |break| set tree type

   **Possible values:**

   - notree
   - octree3d
   - quadtree3d
   - quadtree2d

.. _SECbinningstrategy:

BINNING STRATEGY
----------------

no description yet

::

   ---------------------------------------------------BINNING STRATEGY

.. _binningstrategy_bin_size_lower_bound:

**BIN_SIZE_LOWER_BOUND** | *default:* -1 |break| Lower bound for bin size. Exact bin size is computed via (Domain edge length)/BIN_SIZE_LOWER_BOUND. This also determines the number of bins in each spatial direction

.. _binningstrategy_bin_per_dir:

**BIN_PER_DIR** | *default:* -1 -1 -1 |break| Number of bins per direction (x, y, z) in particle simulations. Either Define this value or BIN_SIZE_LOWER_BOUND

.. _binningstrategy_periodiconoff:

**PERIODICONOFF** | *default:* 0 0 0 |break| Turn on/off periodic boundary conditions in each spatial direction

.. _binningstrategy_domainboundingbox:

**DOMAINBOUNDINGBOX** | *default:* 1e12 1e12 1e12 1e12 1e12 1e12 |break| Bounding box for computational domain using binning strategy. Specify diagonal corner points

.. _binningstrategy_writebins:

**WRITEBINS** | *default:* none |break| Write none, row or column bins for visualization

   **Possible values:**

   - none
   - rows
   - cols

.. _SECboundingvolumestrategy:

BOUNDINGVOLUME STRATEGY
-----------------------

no description yet

::

   --------------------------------------------BOUNDINGVOLUME STRATEGY

.. _boundingvolumestrategy_beam_radius_extension_factor:

**BEAM_RADIUS_EXTENSION_FACTOR** | *default:* 2 |break| Beams radius is multiplied with the factor and then the bounding volume only depending on the beam centerline is extended in all directions (+ and -) by that value.

.. _boundingvolumestrategy_sphere_radius_extension_factor:

**SPHERE_RADIUS_EXTENSION_FACTOR** | *default:* 2 |break| Bounding volume of the sphere is the sphere center extended by this factor times the sphere radius in all directions (+ and -).

.. _SECpasidynamic:

PASI DYNAMIC
------------

general control parameters for particle structure interaction problems

::

   -------------------------------------------------------PASI DYNAMIC

.. _pasidynamic_resultsevry:

**RESULTSEVRY** | *default:* 1 |break| Increment for writing solution

.. _pasidynamic_restartevry:

**RESTARTEVRY** | *default:* 1 |break| Increment for writing restart

.. _pasidynamic_timestep:

**TIMESTEP** | *default:* 0.01 |break| Time increment dt

.. _pasidynamic_numstep:

**NUMSTEP** | *default:* 100 |break| Total number of Timesteps

.. _pasidynamic_maxtime:

**MAXTIME** | *default:* 1 |break| Total simulation time

.. _pasidynamic_coupling:

**COUPLING** | *default:* partitioned_onewaycoup |break| partitioned coupling strategies for particle structure interaction

   **Possible values:**

   - partitioned_onewaycoup
   - partitioned_twowaycoup
   - partitioned_twowaycoup_disprelax
   - partitioned_twowaycoup_disprelaxaitken

.. _pasidynamic_itemax:

**ITEMAX** | *default:* 10 |break| maximum number of partitioned iterations over fields

.. _pasidynamic_convtolscaleddisp:

**CONVTOLSCALEDDISP** | *default:* -1 |break| tolerance of dof and dt scaled interface displacement increments in partitioned iterations

.. _pasidynamic_convtolrelativedisp:

**CONVTOLRELATIVEDISP** | *default:* -1 |break| tolerance of relative interface displacement increments in partitioned iterations

.. _pasidynamic_convtolscaledforce:

**CONVTOLSCALEDFORCE** | *default:* -1 |break| tolerance of dof and dt scaled interface force increments in partitioned iterations

.. _pasidynamic_convtolrelativeforce:

**CONVTOLRELATIVEFORCE** | *default:* -1 |break| tolerance of relative interface force increments in partitioned iterations

.. _pasidynamic_ignore_conv_check:

**IGNORE_CONV_CHECK** | *default:* no |break| ignore convergence check and proceed simulation

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _pasidynamic_startomega:

**STARTOMEGA** | *default:* 1 |break| fixed relaxation parameter

.. _pasidynamic_maxomega:

**MAXOMEGA** | *default:* 10 |break| largest omega allowed for Aitken relaxation

.. _pasidynamic_minomega:

**MINOMEGA** | *default:* 0.1 |break| smallest omega allowed for Aitken relaxation

.. _SECmeshpartitioning:

MESH PARTITIONING
-----------------

no description yet

::

   --------------------------------------------------MESH PARTITIONING

.. _meshpartitioning_method:

**METHOD** | *default:* hypergraph |break| Type of rebalance/partition algorithm to be used for decomposing the entire mesh into subdomains for parallel computing.

   **Possible values:**

   - none
   - hypergraph
   - recursive_coordinate_bisection

.. _meshpartitioning_imbalance_tol:

**IMBALANCE_TOL** | *default:* 1.1 |break| Tolerance for relative imbalance of subdomain sizes for graph partitioning of unstructured meshes read from input files.

.. _SECsolver1:

SOLVER 1
--------

solver parameters for solver block 1

::

   -----------------------------------------------------------SOLVER 1

.. _solver1_solver:

**SOLVER** | *default:* undefined |break| The solver to attack the system of linear equations arising of FE approach with.

   **Possible values:**

   - UMFPACK
   - Superlu
   - Belos
   - undefined

.. _solver1_azsolve:

**AZSOLVE** | *default:* GMRES |break| Type of linear solver algorithm to use.

   **Possible values:**

   - CG
   - GMRES
   - BiCGSTAB

.. _solver1_azprec:

**AZPREC** | *default:* ILU |break| Type of internal preconditioner to use.
Note! this preconditioner will only be used if the input operator
supports the Epetra_RowMatrix interface and the client does not pass
in an external preconditioner!

   **Possible values:**

   - ILU
   - ICC
   - ML
   - MLFLUID
   - MLFLUID2
   - MueLu
   - MueLu_fluid
   - MueLu_tsi
   - MueLu_contactSP
   - MueLu_BeamSolid
   - MueLu_fsi
   - AMGnxn
   - BGS2x2
   - CheapSIMPLE

.. _solver1_ifpackoverlap:

**IFPACKOVERLAP** | *default:* 0 |break| The amount of overlap used for the ifpack "ilu" preconditioner.

.. _solver1_ifpackgfill:

**IFPACKGFILL** | *default:* 0 |break| The amount of fill allowed for an internal "ilu" preconditioner.

.. _solver1_ifpackcombine:

**IFPACKCOMBINE** | *default:* Add |break| Combine mode for Ifpack Additive Schwarz

   **Possible values:**

   - Add
   - Insert
   - Zero

.. _solver1_aziter:

**AZITER** | *default:* 1000 |break| The maximum number of iterations the underlying iterative solver is allowed to perform

.. _solver1_aztol:

**AZTOL** | *default:* 1e-08 |break| The level the residual norms must reach to decide about successful convergence

.. _solver1_azconv:

**AZCONV** | *default:* AZ_r0 |break| The implicit residual norm scaling type to use for terminating the iterative solver.

   **Possible values:**

   - AZ_r0
   - AZ_noscaled

.. _solver1_azoutput:

**AZOUTPUT** | *default:* 0 |break| The number of iterations between each output of the solver's progress is written to screen

.. _solver1_azreuse:

**AZREUSE** | *default:* 0 |break| The number specifying how often to recompute some preconditioners

.. _solver1_azsub:

**AZSUB** | *default:* 50 |break| The maximum size of the Krylov subspace used with "GMRES" before
a restart is performed.

.. _solver1_azgraph:

**AZGRAPH** | *default:* 0 |break| unused

.. _solver1_azbdiag:

**AZBDIAG** | *default:* 0 |break| unused

.. _solver1_azomega:

**AZOMEGA** | *default:* 0 |break| unused

.. _solver1_ml_print:

**ML_PRINT** | *default:* 0 |break| ML print-out level (0-10)

.. _solver1_ml_maxcoarsesize:

**ML_MAXCOARSESIZE** | *default:* 5000 |break| ML stop coarsening when coarse ndof smaller then this

.. _solver1_ml_maxlevel:

**ML_MAXLEVEL** | *default:* 5 |break| ML max number of levels

.. _solver1_ml_agg_size:

**ML_AGG_SIZE** | *default:* 27 |break| objective size of an aggregate with METIS/VBMETIS, 2D: 9, 3D: 27

.. _solver1_ml_dampfine:

**ML_DAMPFINE** | *default:* 1 |break| damping fine grid

.. _solver1_ml_dampmed:

**ML_DAMPMED** | *default:* 1 |break| damping med grids

.. _solver1_ml_dampcoarse:

**ML_DAMPCOARSE** | *default:* 1 |break| damping coarse grid

.. _solver1_ml_prolong_smo:

**ML_PROLONG_SMO** | *default:* 0 |break| damping factor for prolongator smoother (usually 1.33 or 0.0)

.. _solver1_ml_prolong_thres:

**ML_PROLONG_THRES** | *default:* 0 |break| threshold for prolongator smoother/aggregation

.. _solver1_ml_smotimes:

**ML_SMOTIMES** | *default:* 1 1 1 1 1 |break| no. smoothing steps or polynomial order on each level (at least ML_MAXLEVEL numbers)

.. _solver1_ml_coarsen:

**ML_COARSEN** | *default:* UC |break| no description yet

   **Possible values:**

   - UC
   - METIS
   - VBMETIS
   - MIS

.. _solver1_ml_rebalance:

**ML_REBALANCE** | *default:* Yes |break| Performe ML-internal rebalancing of coarse level operators.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _solver1_ml_smootherfine:

**ML_SMOOTHERFINE** | *default:* ILU |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver1_ml_smoothermed:

**ML_SMOOTHERMED** | *default:* ILU |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver1_ml_smoothercoarse:

**ML_SMOOTHERCOARSE** | *default:* Umfpack |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver1_sub_solver1:

**SUB_SOLVER1** | *default:* -1 |break| sub solver/smoother block number (SIMPLE/C: used for prediction of primary variable on all levels, BS: used for fine and intermedium BraessSarazin (BS) level smoother)

.. _solver1_sub_solver2:

**SUB_SOLVER2** | *default:* -1 |break| sub solver/smoother block number (SIMPLE/C: used for SchurComplement eq. on all levels, BS: used for coarse BraessSarazin (BS) level smoother)

.. _solver1_muelu_xml_file:

**MUELU_XML_FILE** | *default:* none |break| xml file defining any MueLu preconditioner

.. _solver1_muelu_xml_enforce:

**MUELU_XML_ENFORCE** | *default:* Yes |break| option defining xml file usage

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _solver1_bgs2x2_fliporder:

**BGS2X2_FLIPORDER** | *default:* block0_block1_order |break| BGS2x2 flip order parameter

   **Possible values:**

   - block0_block1_order
   - block1_block0_order

.. _solver1_bgs2x2_global_damping:

**BGS2X2_GLOBAL_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner

.. _solver1_bgs2x2_block1_damping:

**BGS2X2_BLOCK1_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner block1

.. _solver1_bgs2x2_block2_damping:

**BGS2X2_BLOCK2_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner block2

.. _solver1_azscal:

**AZSCAL** | *default:* none |break| scaling of the linear system to improve properties

   **Possible values:**

   - none
   - sym
   - infnorm

.. _solver1_permute_system:

**PERMUTE_SYSTEM** | *default:* none |break| allow linear solver to permute linear system to improve properties

   **Possible values:**

   - none
   - algebraic
   - local

.. _solver1_non_diagdominance_ratio:

**NON_DIAGDOMINANCE_RATIO** | *default:* 1 |break| matrix rows with diagEntry/maxEntry<nonDiagDominanceRatio are marked to be significantly non-diagonal dominant (default: 1.0 = mark all non-diagonal dominant rows)

.. _solver1_name:

**NAME** | *default:* No_name |break| User specified name for solver block

.. _solver1_simple_damping:

**SIMPLE_DAMPING** | *default:* 1 |break| damping parameter for SIMPLE preconditioner

.. _solver1_amgnxn_type:

**AMGNXN_TYPE** | *default:* AMG(BGS) |break| Name of the pre-built preconditioner to be used. If set to"XML" the preconditioner is defined using a xml file

.. _solver1_amgnxn_xml_file:

**AMGNXN_XML_FILE** | *default:* none |break| xml file defining the AMGnxn preconditioner

.. _SECsolver2:

SOLVER 2
--------

solver parameters for solver block 2

::

   -----------------------------------------------------------SOLVER 2

.. _solver2_solver:

**SOLVER** | *default:* undefined |break| The solver to attack the system of linear equations arising of FE approach with.

   **Possible values:**

   - UMFPACK
   - Superlu
   - Belos
   - undefined

.. _solver2_azsolve:

**AZSOLVE** | *default:* GMRES |break| Type of linear solver algorithm to use.

   **Possible values:**

   - CG
   - GMRES
   - BiCGSTAB

.. _solver2_azprec:

**AZPREC** | *default:* ILU |break| Type of internal preconditioner to use.
Note! this preconditioner will only be used if the input operator
supports the Epetra_RowMatrix interface and the client does not pass
in an external preconditioner!

   **Possible values:**

   - ILU
   - ICC
   - ML
   - MLFLUID
   - MLFLUID2
   - MueLu
   - MueLu_fluid
   - MueLu_tsi
   - MueLu_contactSP
   - MueLu_BeamSolid
   - MueLu_fsi
   - AMGnxn
   - BGS2x2
   - CheapSIMPLE

.. _solver2_ifpackoverlap:

**IFPACKOVERLAP** | *default:* 0 |break| The amount of overlap used for the ifpack "ilu" preconditioner.

.. _solver2_ifpackgfill:

**IFPACKGFILL** | *default:* 0 |break| The amount of fill allowed for an internal "ilu" preconditioner.

.. _solver2_ifpackcombine:

**IFPACKCOMBINE** | *default:* Add |break| Combine mode for Ifpack Additive Schwarz

   **Possible values:**

   - Add
   - Insert
   - Zero

.. _solver2_aziter:

**AZITER** | *default:* 1000 |break| The maximum number of iterations the underlying iterative solver is allowed to perform

.. _solver2_aztol:

**AZTOL** | *default:* 1e-08 |break| The level the residual norms must reach to decide about successful convergence

.. _solver2_azconv:

**AZCONV** | *default:* AZ_r0 |break| The implicit residual norm scaling type to use for terminating the iterative solver.

   **Possible values:**

   - AZ_r0
   - AZ_noscaled

.. _solver2_azoutput:

**AZOUTPUT** | *default:* 0 |break| The number of iterations between each output of the solver's progress is written to screen

.. _solver2_azreuse:

**AZREUSE** | *default:* 0 |break| The number specifying how often to recompute some preconditioners

.. _solver2_azsub:

**AZSUB** | *default:* 50 |break| The maximum size of the Krylov subspace used with "GMRES" before
a restart is performed.

.. _solver2_azgraph:

**AZGRAPH** | *default:* 0 |break| unused

.. _solver2_azbdiag:

**AZBDIAG** | *default:* 0 |break| unused

.. _solver2_azomega:

**AZOMEGA** | *default:* 0 |break| unused

.. _solver2_ml_print:

**ML_PRINT** | *default:* 0 |break| ML print-out level (0-10)

.. _solver2_ml_maxcoarsesize:

**ML_MAXCOARSESIZE** | *default:* 5000 |break| ML stop coarsening when coarse ndof smaller then this

.. _solver2_ml_maxlevel:

**ML_MAXLEVEL** | *default:* 5 |break| ML max number of levels

.. _solver2_ml_agg_size:

**ML_AGG_SIZE** | *default:* 27 |break| objective size of an aggregate with METIS/VBMETIS, 2D: 9, 3D: 27

.. _solver2_ml_dampfine:

**ML_DAMPFINE** | *default:* 1 |break| damping fine grid

.. _solver2_ml_dampmed:

**ML_DAMPMED** | *default:* 1 |break| damping med grids

.. _solver2_ml_dampcoarse:

**ML_DAMPCOARSE** | *default:* 1 |break| damping coarse grid

.. _solver2_ml_prolong_smo:

**ML_PROLONG_SMO** | *default:* 0 |break| damping factor for prolongator smoother (usually 1.33 or 0.0)

.. _solver2_ml_prolong_thres:

**ML_PROLONG_THRES** | *default:* 0 |break| threshold for prolongator smoother/aggregation

.. _solver2_ml_smotimes:

**ML_SMOTIMES** | *default:* 1 1 1 1 1 |break| no. smoothing steps or polynomial order on each level (at least ML_MAXLEVEL numbers)

.. _solver2_ml_coarsen:

**ML_COARSEN** | *default:* UC |break| no description yet

   **Possible values:**

   - UC
   - METIS
   - VBMETIS
   - MIS

.. _solver2_ml_rebalance:

**ML_REBALANCE** | *default:* Yes |break| Performe ML-internal rebalancing of coarse level operators.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _solver2_ml_smootherfine:

**ML_SMOOTHERFINE** | *default:* ILU |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver2_ml_smoothermed:

**ML_SMOOTHERMED** | *default:* ILU |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver2_ml_smoothercoarse:

**ML_SMOOTHERCOARSE** | *default:* Umfpack |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver2_sub_solver1:

**SUB_SOLVER1** | *default:* -1 |break| sub solver/smoother block number (SIMPLE/C: used for prediction of primary variable on all levels, BS: used for fine and intermedium BraessSarazin (BS) level smoother)

.. _solver2_sub_solver2:

**SUB_SOLVER2** | *default:* -1 |break| sub solver/smoother block number (SIMPLE/C: used for SchurComplement eq. on all levels, BS: used for coarse BraessSarazin (BS) level smoother)

.. _solver2_muelu_xml_file:

**MUELU_XML_FILE** | *default:* none |break| xml file defining any MueLu preconditioner

.. _solver2_muelu_xml_enforce:

**MUELU_XML_ENFORCE** | *default:* Yes |break| option defining xml file usage

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _solver2_bgs2x2_fliporder:

**BGS2X2_FLIPORDER** | *default:* block0_block1_order |break| BGS2x2 flip order parameter

   **Possible values:**

   - block0_block1_order
   - block1_block0_order

.. _solver2_bgs2x2_global_damping:

**BGS2X2_GLOBAL_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner

.. _solver2_bgs2x2_block1_damping:

**BGS2X2_BLOCK1_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner block1

.. _solver2_bgs2x2_block2_damping:

**BGS2X2_BLOCK2_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner block2

.. _solver2_azscal:

**AZSCAL** | *default:* none |break| scaling of the linear system to improve properties

   **Possible values:**

   - none
   - sym
   - infnorm

.. _solver2_permute_system:

**PERMUTE_SYSTEM** | *default:* none |break| allow linear solver to permute linear system to improve properties

   **Possible values:**

   - none
   - algebraic
   - local

.. _solver2_non_diagdominance_ratio:

**NON_DIAGDOMINANCE_RATIO** | *default:* 1 |break| matrix rows with diagEntry/maxEntry<nonDiagDominanceRatio are marked to be significantly non-diagonal dominant (default: 1.0 = mark all non-diagonal dominant rows)

.. _solver2_name:

**NAME** | *default:* No_name |break| User specified name for solver block

.. _solver2_simple_damping:

**SIMPLE_DAMPING** | *default:* 1 |break| damping parameter for SIMPLE preconditioner

.. _solver2_amgnxn_type:

**AMGNXN_TYPE** | *default:* AMG(BGS) |break| Name of the pre-built preconditioner to be used. If set to"XML" the preconditioner is defined using a xml file

.. _solver2_amgnxn_xml_file:

**AMGNXN_XML_FILE** | *default:* none |break| xml file defining the AMGnxn preconditioner

.. _SECsolver3:

SOLVER 3
--------

solver parameters for solver block 3

::

   -----------------------------------------------------------SOLVER 3

.. _solver3_solver:

**SOLVER** | *default:* undefined |break| The solver to attack the system of linear equations arising of FE approach with.

   **Possible values:**

   - UMFPACK
   - Superlu
   - Belos
   - undefined

.. _solver3_azsolve:

**AZSOLVE** | *default:* GMRES |break| Type of linear solver algorithm to use.

   **Possible values:**

   - CG
   - GMRES
   - BiCGSTAB

.. _solver3_azprec:

**AZPREC** | *default:* ILU |break| Type of internal preconditioner to use.
Note! this preconditioner will only be used if the input operator
supports the Epetra_RowMatrix interface and the client does not pass
in an external preconditioner!

   **Possible values:**

   - ILU
   - ICC
   - ML
   - MLFLUID
   - MLFLUID2
   - MueLu
   - MueLu_fluid
   - MueLu_tsi
   - MueLu_contactSP
   - MueLu_BeamSolid
   - MueLu_fsi
   - AMGnxn
   - BGS2x2
   - CheapSIMPLE

.. _solver3_ifpackoverlap:

**IFPACKOVERLAP** | *default:* 0 |break| The amount of overlap used for the ifpack "ilu" preconditioner.

.. _solver3_ifpackgfill:

**IFPACKGFILL** | *default:* 0 |break| The amount of fill allowed for an internal "ilu" preconditioner.

.. _solver3_ifpackcombine:

**IFPACKCOMBINE** | *default:* Add |break| Combine mode for Ifpack Additive Schwarz

   **Possible values:**

   - Add
   - Insert
   - Zero

.. _solver3_aziter:

**AZITER** | *default:* 1000 |break| The maximum number of iterations the underlying iterative solver is allowed to perform

.. _solver3_aztol:

**AZTOL** | *default:* 1e-08 |break| The level the residual norms must reach to decide about successful convergence

.. _solver3_azconv:

**AZCONV** | *default:* AZ_r0 |break| The implicit residual norm scaling type to use for terminating the iterative solver.

   **Possible values:**

   - AZ_r0
   - AZ_noscaled

.. _solver3_azoutput:

**AZOUTPUT** | *default:* 0 |break| The number of iterations between each output of the solver's progress is written to screen

.. _solver3_azreuse:

**AZREUSE** | *default:* 0 |break| The number specifying how often to recompute some preconditioners

.. _solver3_azsub:

**AZSUB** | *default:* 50 |break| The maximum size of the Krylov subspace used with "GMRES" before
a restart is performed.

.. _solver3_azgraph:

**AZGRAPH** | *default:* 0 |break| unused

.. _solver3_azbdiag:

**AZBDIAG** | *default:* 0 |break| unused

.. _solver3_azomega:

**AZOMEGA** | *default:* 0 |break| unused

.. _solver3_ml_print:

**ML_PRINT** | *default:* 0 |break| ML print-out level (0-10)

.. _solver3_ml_maxcoarsesize:

**ML_MAXCOARSESIZE** | *default:* 5000 |break| ML stop coarsening when coarse ndof smaller then this

.. _solver3_ml_maxlevel:

**ML_MAXLEVEL** | *default:* 5 |break| ML max number of levels

.. _solver3_ml_agg_size:

**ML_AGG_SIZE** | *default:* 27 |break| objective size of an aggregate with METIS/VBMETIS, 2D: 9, 3D: 27

.. _solver3_ml_dampfine:

**ML_DAMPFINE** | *default:* 1 |break| damping fine grid

.. _solver3_ml_dampmed:

**ML_DAMPMED** | *default:* 1 |break| damping med grids

.. _solver3_ml_dampcoarse:

**ML_DAMPCOARSE** | *default:* 1 |break| damping coarse grid

.. _solver3_ml_prolong_smo:

**ML_PROLONG_SMO** | *default:* 0 |break| damping factor for prolongator smoother (usually 1.33 or 0.0)

.. _solver3_ml_prolong_thres:

**ML_PROLONG_THRES** | *default:* 0 |break| threshold for prolongator smoother/aggregation

.. _solver3_ml_smotimes:

**ML_SMOTIMES** | *default:* 1 1 1 1 1 |break| no. smoothing steps or polynomial order on each level (at least ML_MAXLEVEL numbers)

.. _solver3_ml_coarsen:

**ML_COARSEN** | *default:* UC |break| no description yet

   **Possible values:**

   - UC
   - METIS
   - VBMETIS
   - MIS

.. _solver3_ml_rebalance:

**ML_REBALANCE** | *default:* Yes |break| Performe ML-internal rebalancing of coarse level operators.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _solver3_ml_smootherfine:

**ML_SMOOTHERFINE** | *default:* ILU |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver3_ml_smoothermed:

**ML_SMOOTHERMED** | *default:* ILU |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver3_ml_smoothercoarse:

**ML_SMOOTHERCOARSE** | *default:* Umfpack |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver3_sub_solver1:

**SUB_SOLVER1** | *default:* -1 |break| sub solver/smoother block number (SIMPLE/C: used for prediction of primary variable on all levels, BS: used for fine and intermedium BraessSarazin (BS) level smoother)

.. _solver3_sub_solver2:

**SUB_SOLVER2** | *default:* -1 |break| sub solver/smoother block number (SIMPLE/C: used for SchurComplement eq. on all levels, BS: used for coarse BraessSarazin (BS) level smoother)

.. _solver3_muelu_xml_file:

**MUELU_XML_FILE** | *default:* none |break| xml file defining any MueLu preconditioner

.. _solver3_muelu_xml_enforce:

**MUELU_XML_ENFORCE** | *default:* Yes |break| option defining xml file usage

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _solver3_bgs2x2_fliporder:

**BGS2X2_FLIPORDER** | *default:* block0_block1_order |break| BGS2x2 flip order parameter

   **Possible values:**

   - block0_block1_order
   - block1_block0_order

.. _solver3_bgs2x2_global_damping:

**BGS2X2_GLOBAL_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner

.. _solver3_bgs2x2_block1_damping:

**BGS2X2_BLOCK1_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner block1

.. _solver3_bgs2x2_block2_damping:

**BGS2X2_BLOCK2_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner block2

.. _solver3_azscal:

**AZSCAL** | *default:* none |break| scaling of the linear system to improve properties

   **Possible values:**

   - none
   - sym
   - infnorm

.. _solver3_permute_system:

**PERMUTE_SYSTEM** | *default:* none |break| allow linear solver to permute linear system to improve properties

   **Possible values:**

   - none
   - algebraic
   - local

.. _solver3_non_diagdominance_ratio:

**NON_DIAGDOMINANCE_RATIO** | *default:* 1 |break| matrix rows with diagEntry/maxEntry<nonDiagDominanceRatio are marked to be significantly non-diagonal dominant (default: 1.0 = mark all non-diagonal dominant rows)

.. _solver3_name:

**NAME** | *default:* No_name |break| User specified name for solver block

.. _solver3_simple_damping:

**SIMPLE_DAMPING** | *default:* 1 |break| damping parameter for SIMPLE preconditioner

.. _solver3_amgnxn_type:

**AMGNXN_TYPE** | *default:* AMG(BGS) |break| Name of the pre-built preconditioner to be used. If set to"XML" the preconditioner is defined using a xml file

.. _solver3_amgnxn_xml_file:

**AMGNXN_XML_FILE** | *default:* none |break| xml file defining the AMGnxn preconditioner

.. _SECsolver4:

SOLVER 4
--------

solver parameters for solver block 4

::

   -----------------------------------------------------------SOLVER 4

.. _solver4_solver:

**SOLVER** | *default:* undefined |break| The solver to attack the system of linear equations arising of FE approach with.

   **Possible values:**

   - UMFPACK
   - Superlu
   - Belos
   - undefined

.. _solver4_azsolve:

**AZSOLVE** | *default:* GMRES |break| Type of linear solver algorithm to use.

   **Possible values:**

   - CG
   - GMRES
   - BiCGSTAB

.. _solver4_azprec:

**AZPREC** | *default:* ILU |break| Type of internal preconditioner to use.
Note! this preconditioner will only be used if the input operator
supports the Epetra_RowMatrix interface and the client does not pass
in an external preconditioner!

   **Possible values:**

   - ILU
   - ICC
   - ML
   - MLFLUID
   - MLFLUID2
   - MueLu
   - MueLu_fluid
   - MueLu_tsi
   - MueLu_contactSP
   - MueLu_BeamSolid
   - MueLu_fsi
   - AMGnxn
   - BGS2x2
   - CheapSIMPLE

.. _solver4_ifpackoverlap:

**IFPACKOVERLAP** | *default:* 0 |break| The amount of overlap used for the ifpack "ilu" preconditioner.

.. _solver4_ifpackgfill:

**IFPACKGFILL** | *default:* 0 |break| The amount of fill allowed for an internal "ilu" preconditioner.

.. _solver4_ifpackcombine:

**IFPACKCOMBINE** | *default:* Add |break| Combine mode for Ifpack Additive Schwarz

   **Possible values:**

   - Add
   - Insert
   - Zero

.. _solver4_aziter:

**AZITER** | *default:* 1000 |break| The maximum number of iterations the underlying iterative solver is allowed to perform

.. _solver4_aztol:

**AZTOL** | *default:* 1e-08 |break| The level the residual norms must reach to decide about successful convergence

.. _solver4_azconv:

**AZCONV** | *default:* AZ_r0 |break| The implicit residual norm scaling type to use for terminating the iterative solver.

   **Possible values:**

   - AZ_r0
   - AZ_noscaled

.. _solver4_azoutput:

**AZOUTPUT** | *default:* 0 |break| The number of iterations between each output of the solver's progress is written to screen

.. _solver4_azreuse:

**AZREUSE** | *default:* 0 |break| The number specifying how often to recompute some preconditioners

.. _solver4_azsub:

**AZSUB** | *default:* 50 |break| The maximum size of the Krylov subspace used with "GMRES" before
a restart is performed.

.. _solver4_azgraph:

**AZGRAPH** | *default:* 0 |break| unused

.. _solver4_azbdiag:

**AZBDIAG** | *default:* 0 |break| unused

.. _solver4_azomega:

**AZOMEGA** | *default:* 0 |break| unused

.. _solver4_ml_print:

**ML_PRINT** | *default:* 0 |break| ML print-out level (0-10)

.. _solver4_ml_maxcoarsesize:

**ML_MAXCOARSESIZE** | *default:* 5000 |break| ML stop coarsening when coarse ndof smaller then this

.. _solver4_ml_maxlevel:

**ML_MAXLEVEL** | *default:* 5 |break| ML max number of levels

.. _solver4_ml_agg_size:

**ML_AGG_SIZE** | *default:* 27 |break| objective size of an aggregate with METIS/VBMETIS, 2D: 9, 3D: 27

.. _solver4_ml_dampfine:

**ML_DAMPFINE** | *default:* 1 |break| damping fine grid

.. _solver4_ml_dampmed:

**ML_DAMPMED** | *default:* 1 |break| damping med grids

.. _solver4_ml_dampcoarse:

**ML_DAMPCOARSE** | *default:* 1 |break| damping coarse grid

.. _solver4_ml_prolong_smo:

**ML_PROLONG_SMO** | *default:* 0 |break| damping factor for prolongator smoother (usually 1.33 or 0.0)

.. _solver4_ml_prolong_thres:

**ML_PROLONG_THRES** | *default:* 0 |break| threshold for prolongator smoother/aggregation

.. _solver4_ml_smotimes:

**ML_SMOTIMES** | *default:* 1 1 1 1 1 |break| no. smoothing steps or polynomial order on each level (at least ML_MAXLEVEL numbers)

.. _solver4_ml_coarsen:

**ML_COARSEN** | *default:* UC |break| no description yet

   **Possible values:**

   - UC
   - METIS
   - VBMETIS
   - MIS

.. _solver4_ml_rebalance:

**ML_REBALANCE** | *default:* Yes |break| Performe ML-internal rebalancing of coarse level operators.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _solver4_ml_smootherfine:

**ML_SMOOTHERFINE** | *default:* ILU |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver4_ml_smoothermed:

**ML_SMOOTHERMED** | *default:* ILU |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver4_ml_smoothercoarse:

**ML_SMOOTHERCOARSE** | *default:* Umfpack |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver4_sub_solver1:

**SUB_SOLVER1** | *default:* -1 |break| sub solver/smoother block number (SIMPLE/C: used for prediction of primary variable on all levels, BS: used for fine and intermedium BraessSarazin (BS) level smoother)

.. _solver4_sub_solver2:

**SUB_SOLVER2** | *default:* -1 |break| sub solver/smoother block number (SIMPLE/C: used for SchurComplement eq. on all levels, BS: used for coarse BraessSarazin (BS) level smoother)

.. _solver4_muelu_xml_file:

**MUELU_XML_FILE** | *default:* none |break| xml file defining any MueLu preconditioner

.. _solver4_muelu_xml_enforce:

**MUELU_XML_ENFORCE** | *default:* Yes |break| option defining xml file usage

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _solver4_bgs2x2_fliporder:

**BGS2X2_FLIPORDER** | *default:* block0_block1_order |break| BGS2x2 flip order parameter

   **Possible values:**

   - block0_block1_order
   - block1_block0_order

.. _solver4_bgs2x2_global_damping:

**BGS2X2_GLOBAL_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner

.. _solver4_bgs2x2_block1_damping:

**BGS2X2_BLOCK1_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner block1

.. _solver4_bgs2x2_block2_damping:

**BGS2X2_BLOCK2_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner block2

.. _solver4_azscal:

**AZSCAL** | *default:* none |break| scaling of the linear system to improve properties

   **Possible values:**

   - none
   - sym
   - infnorm

.. _solver4_permute_system:

**PERMUTE_SYSTEM** | *default:* none |break| allow linear solver to permute linear system to improve properties

   **Possible values:**

   - none
   - algebraic
   - local

.. _solver4_non_diagdominance_ratio:

**NON_DIAGDOMINANCE_RATIO** | *default:* 1 |break| matrix rows with diagEntry/maxEntry<nonDiagDominanceRatio are marked to be significantly non-diagonal dominant (default: 1.0 = mark all non-diagonal dominant rows)

.. _solver4_name:

**NAME** | *default:* No_name |break| User specified name for solver block

.. _solver4_simple_damping:

**SIMPLE_DAMPING** | *default:* 1 |break| damping parameter for SIMPLE preconditioner

.. _solver4_amgnxn_type:

**AMGNXN_TYPE** | *default:* AMG(BGS) |break| Name of the pre-built preconditioner to be used. If set to"XML" the preconditioner is defined using a xml file

.. _solver4_amgnxn_xml_file:

**AMGNXN_XML_FILE** | *default:* none |break| xml file defining the AMGnxn preconditioner

.. _SECsolver5:

SOLVER 5
--------

solver parameters for solver block 5

::

   -----------------------------------------------------------SOLVER 5

.. _solver5_solver:

**SOLVER** | *default:* undefined |break| The solver to attack the system of linear equations arising of FE approach with.

   **Possible values:**

   - UMFPACK
   - Superlu
   - Belos
   - undefined

.. _solver5_azsolve:

**AZSOLVE** | *default:* GMRES |break| Type of linear solver algorithm to use.

   **Possible values:**

   - CG
   - GMRES
   - BiCGSTAB

.. _solver5_azprec:

**AZPREC** | *default:* ILU |break| Type of internal preconditioner to use.
Note! this preconditioner will only be used if the input operator
supports the Epetra_RowMatrix interface and the client does not pass
in an external preconditioner!

   **Possible values:**

   - ILU
   - ICC
   - ML
   - MLFLUID
   - MLFLUID2
   - MueLu
   - MueLu_fluid
   - MueLu_tsi
   - MueLu_contactSP
   - MueLu_BeamSolid
   - MueLu_fsi
   - AMGnxn
   - BGS2x2
   - CheapSIMPLE

.. _solver5_ifpackoverlap:

**IFPACKOVERLAP** | *default:* 0 |break| The amount of overlap used for the ifpack "ilu" preconditioner.

.. _solver5_ifpackgfill:

**IFPACKGFILL** | *default:* 0 |break| The amount of fill allowed for an internal "ilu" preconditioner.

.. _solver5_ifpackcombine:

**IFPACKCOMBINE** | *default:* Add |break| Combine mode for Ifpack Additive Schwarz

   **Possible values:**

   - Add
   - Insert
   - Zero

.. _solver5_aziter:

**AZITER** | *default:* 1000 |break| The maximum number of iterations the underlying iterative solver is allowed to perform

.. _solver5_aztol:

**AZTOL** | *default:* 1e-08 |break| The level the residual norms must reach to decide about successful convergence

.. _solver5_azconv:

**AZCONV** | *default:* AZ_r0 |break| The implicit residual norm scaling type to use for terminating the iterative solver.

   **Possible values:**

   - AZ_r0
   - AZ_noscaled

.. _solver5_azoutput:

**AZOUTPUT** | *default:* 0 |break| The number of iterations between each output of the solver's progress is written to screen

.. _solver5_azreuse:

**AZREUSE** | *default:* 0 |break| The number specifying how often to recompute some preconditioners

.. _solver5_azsub:

**AZSUB** | *default:* 50 |break| The maximum size of the Krylov subspace used with "GMRES" before
a restart is performed.

.. _solver5_azgraph:

**AZGRAPH** | *default:* 0 |break| unused

.. _solver5_azbdiag:

**AZBDIAG** | *default:* 0 |break| unused

.. _solver5_azomega:

**AZOMEGA** | *default:* 0 |break| unused

.. _solver5_ml_print:

**ML_PRINT** | *default:* 0 |break| ML print-out level (0-10)

.. _solver5_ml_maxcoarsesize:

**ML_MAXCOARSESIZE** | *default:* 5000 |break| ML stop coarsening when coarse ndof smaller then this

.. _solver5_ml_maxlevel:

**ML_MAXLEVEL** | *default:* 5 |break| ML max number of levels

.. _solver5_ml_agg_size:

**ML_AGG_SIZE** | *default:* 27 |break| objective size of an aggregate with METIS/VBMETIS, 2D: 9, 3D: 27

.. _solver5_ml_dampfine:

**ML_DAMPFINE** | *default:* 1 |break| damping fine grid

.. _solver5_ml_dampmed:

**ML_DAMPMED** | *default:* 1 |break| damping med grids

.. _solver5_ml_dampcoarse:

**ML_DAMPCOARSE** | *default:* 1 |break| damping coarse grid

.. _solver5_ml_prolong_smo:

**ML_PROLONG_SMO** | *default:* 0 |break| damping factor for prolongator smoother (usually 1.33 or 0.0)

.. _solver5_ml_prolong_thres:

**ML_PROLONG_THRES** | *default:* 0 |break| threshold for prolongator smoother/aggregation

.. _solver5_ml_smotimes:

**ML_SMOTIMES** | *default:* 1 1 1 1 1 |break| no. smoothing steps or polynomial order on each level (at least ML_MAXLEVEL numbers)

.. _solver5_ml_coarsen:

**ML_COARSEN** | *default:* UC |break| no description yet

   **Possible values:**

   - UC
   - METIS
   - VBMETIS
   - MIS

.. _solver5_ml_rebalance:

**ML_REBALANCE** | *default:* Yes |break| Performe ML-internal rebalancing of coarse level operators.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _solver5_ml_smootherfine:

**ML_SMOOTHERFINE** | *default:* ILU |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver5_ml_smoothermed:

**ML_SMOOTHERMED** | *default:* ILU |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver5_ml_smoothercoarse:

**ML_SMOOTHERCOARSE** | *default:* Umfpack |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver5_sub_solver1:

**SUB_SOLVER1** | *default:* -1 |break| sub solver/smoother block number (SIMPLE/C: used for prediction of primary variable on all levels, BS: used for fine and intermedium BraessSarazin (BS) level smoother)

.. _solver5_sub_solver2:

**SUB_SOLVER2** | *default:* -1 |break| sub solver/smoother block number (SIMPLE/C: used for SchurComplement eq. on all levels, BS: used for coarse BraessSarazin (BS) level smoother)

.. _solver5_muelu_xml_file:

**MUELU_XML_FILE** | *default:* none |break| xml file defining any MueLu preconditioner

.. _solver5_muelu_xml_enforce:

**MUELU_XML_ENFORCE** | *default:* Yes |break| option defining xml file usage

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _solver5_bgs2x2_fliporder:

**BGS2X2_FLIPORDER** | *default:* block0_block1_order |break| BGS2x2 flip order parameter

   **Possible values:**

   - block0_block1_order
   - block1_block0_order

.. _solver5_bgs2x2_global_damping:

**BGS2X2_GLOBAL_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner

.. _solver5_bgs2x2_block1_damping:

**BGS2X2_BLOCK1_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner block1

.. _solver5_bgs2x2_block2_damping:

**BGS2X2_BLOCK2_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner block2

.. _solver5_azscal:

**AZSCAL** | *default:* none |break| scaling of the linear system to improve properties

   **Possible values:**

   - none
   - sym
   - infnorm

.. _solver5_permute_system:

**PERMUTE_SYSTEM** | *default:* none |break| allow linear solver to permute linear system to improve properties

   **Possible values:**

   - none
   - algebraic
   - local

.. _solver5_non_diagdominance_ratio:

**NON_DIAGDOMINANCE_RATIO** | *default:* 1 |break| matrix rows with diagEntry/maxEntry<nonDiagDominanceRatio are marked to be significantly non-diagonal dominant (default: 1.0 = mark all non-diagonal dominant rows)

.. _solver5_name:

**NAME** | *default:* No_name |break| User specified name for solver block

.. _solver5_simple_damping:

**SIMPLE_DAMPING** | *default:* 1 |break| damping parameter for SIMPLE preconditioner

.. _solver5_amgnxn_type:

**AMGNXN_TYPE** | *default:* AMG(BGS) |break| Name of the pre-built preconditioner to be used. If set to"XML" the preconditioner is defined using a xml file

.. _solver5_amgnxn_xml_file:

**AMGNXN_XML_FILE** | *default:* none |break| xml file defining the AMGnxn preconditioner

.. _SECsolver6:

SOLVER 6
--------

solver parameters for solver block 6

::

   -----------------------------------------------------------SOLVER 6

.. _solver6_solver:

**SOLVER** | *default:* undefined |break| The solver to attack the system of linear equations arising of FE approach with.

   **Possible values:**

   - UMFPACK
   - Superlu
   - Belos
   - undefined

.. _solver6_azsolve:

**AZSOLVE** | *default:* GMRES |break| Type of linear solver algorithm to use.

   **Possible values:**

   - CG
   - GMRES
   - BiCGSTAB

.. _solver6_azprec:

**AZPREC** | *default:* ILU |break| Type of internal preconditioner to use.
Note! this preconditioner will only be used if the input operator
supports the Epetra_RowMatrix interface and the client does not pass
in an external preconditioner!

   **Possible values:**

   - ILU
   - ICC
   - ML
   - MLFLUID
   - MLFLUID2
   - MueLu
   - MueLu_fluid
   - MueLu_tsi
   - MueLu_contactSP
   - MueLu_BeamSolid
   - MueLu_fsi
   - AMGnxn
   - BGS2x2
   - CheapSIMPLE

.. _solver6_ifpackoverlap:

**IFPACKOVERLAP** | *default:* 0 |break| The amount of overlap used for the ifpack "ilu" preconditioner.

.. _solver6_ifpackgfill:

**IFPACKGFILL** | *default:* 0 |break| The amount of fill allowed for an internal "ilu" preconditioner.

.. _solver6_ifpackcombine:

**IFPACKCOMBINE** | *default:* Add |break| Combine mode for Ifpack Additive Schwarz

   **Possible values:**

   - Add
   - Insert
   - Zero

.. _solver6_aziter:

**AZITER** | *default:* 1000 |break| The maximum number of iterations the underlying iterative solver is allowed to perform

.. _solver6_aztol:

**AZTOL** | *default:* 1e-08 |break| The level the residual norms must reach to decide about successful convergence

.. _solver6_azconv:

**AZCONV** | *default:* AZ_r0 |break| The implicit residual norm scaling type to use for terminating the iterative solver.

   **Possible values:**

   - AZ_r0
   - AZ_noscaled

.. _solver6_azoutput:

**AZOUTPUT** | *default:* 0 |break| The number of iterations between each output of the solver's progress is written to screen

.. _solver6_azreuse:

**AZREUSE** | *default:* 0 |break| The number specifying how often to recompute some preconditioners

.. _solver6_azsub:

**AZSUB** | *default:* 50 |break| The maximum size of the Krylov subspace used with "GMRES" before
a restart is performed.

.. _solver6_azgraph:

**AZGRAPH** | *default:* 0 |break| unused

.. _solver6_azbdiag:

**AZBDIAG** | *default:* 0 |break| unused

.. _solver6_azomega:

**AZOMEGA** | *default:* 0 |break| unused

.. _solver6_ml_print:

**ML_PRINT** | *default:* 0 |break| ML print-out level (0-10)

.. _solver6_ml_maxcoarsesize:

**ML_MAXCOARSESIZE** | *default:* 5000 |break| ML stop coarsening when coarse ndof smaller then this

.. _solver6_ml_maxlevel:

**ML_MAXLEVEL** | *default:* 5 |break| ML max number of levels

.. _solver6_ml_agg_size:

**ML_AGG_SIZE** | *default:* 27 |break| objective size of an aggregate with METIS/VBMETIS, 2D: 9, 3D: 27

.. _solver6_ml_dampfine:

**ML_DAMPFINE** | *default:* 1 |break| damping fine grid

.. _solver6_ml_dampmed:

**ML_DAMPMED** | *default:* 1 |break| damping med grids

.. _solver6_ml_dampcoarse:

**ML_DAMPCOARSE** | *default:* 1 |break| damping coarse grid

.. _solver6_ml_prolong_smo:

**ML_PROLONG_SMO** | *default:* 0 |break| damping factor for prolongator smoother (usually 1.33 or 0.0)

.. _solver6_ml_prolong_thres:

**ML_PROLONG_THRES** | *default:* 0 |break| threshold for prolongator smoother/aggregation

.. _solver6_ml_smotimes:

**ML_SMOTIMES** | *default:* 1 1 1 1 1 |break| no. smoothing steps or polynomial order on each level (at least ML_MAXLEVEL numbers)

.. _solver6_ml_coarsen:

**ML_COARSEN** | *default:* UC |break| no description yet

   **Possible values:**

   - UC
   - METIS
   - VBMETIS
   - MIS

.. _solver6_ml_rebalance:

**ML_REBALANCE** | *default:* Yes |break| Performe ML-internal rebalancing of coarse level operators.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _solver6_ml_smootherfine:

**ML_SMOOTHERFINE** | *default:* ILU |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver6_ml_smoothermed:

**ML_SMOOTHERMED** | *default:* ILU |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver6_ml_smoothercoarse:

**ML_SMOOTHERCOARSE** | *default:* Umfpack |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver6_sub_solver1:

**SUB_SOLVER1** | *default:* -1 |break| sub solver/smoother block number (SIMPLE/C: used for prediction of primary variable on all levels, BS: used for fine and intermedium BraessSarazin (BS) level smoother)

.. _solver6_sub_solver2:

**SUB_SOLVER2** | *default:* -1 |break| sub solver/smoother block number (SIMPLE/C: used for SchurComplement eq. on all levels, BS: used for coarse BraessSarazin (BS) level smoother)

.. _solver6_muelu_xml_file:

**MUELU_XML_FILE** | *default:* none |break| xml file defining any MueLu preconditioner

.. _solver6_muelu_xml_enforce:

**MUELU_XML_ENFORCE** | *default:* Yes |break| option defining xml file usage

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _solver6_bgs2x2_fliporder:

**BGS2X2_FLIPORDER** | *default:* block0_block1_order |break| BGS2x2 flip order parameter

   **Possible values:**

   - block0_block1_order
   - block1_block0_order

.. _solver6_bgs2x2_global_damping:

**BGS2X2_GLOBAL_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner

.. _solver6_bgs2x2_block1_damping:

**BGS2X2_BLOCK1_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner block1

.. _solver6_bgs2x2_block2_damping:

**BGS2X2_BLOCK2_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner block2

.. _solver6_azscal:

**AZSCAL** | *default:* none |break| scaling of the linear system to improve properties

   **Possible values:**

   - none
   - sym
   - infnorm

.. _solver6_permute_system:

**PERMUTE_SYSTEM** | *default:* none |break| allow linear solver to permute linear system to improve properties

   **Possible values:**

   - none
   - algebraic
   - local

.. _solver6_non_diagdominance_ratio:

**NON_DIAGDOMINANCE_RATIO** | *default:* 1 |break| matrix rows with diagEntry/maxEntry<nonDiagDominanceRatio are marked to be significantly non-diagonal dominant (default: 1.0 = mark all non-diagonal dominant rows)

.. _solver6_name:

**NAME** | *default:* No_name |break| User specified name for solver block

.. _solver6_simple_damping:

**SIMPLE_DAMPING** | *default:* 1 |break| damping parameter for SIMPLE preconditioner

.. _solver6_amgnxn_type:

**AMGNXN_TYPE** | *default:* AMG(BGS) |break| Name of the pre-built preconditioner to be used. If set to"XML" the preconditioner is defined using a xml file

.. _solver6_amgnxn_xml_file:

**AMGNXN_XML_FILE** | *default:* none |break| xml file defining the AMGnxn preconditioner

.. _SECsolver7:

SOLVER 7
--------

solver parameters for solver block 7

::

   -----------------------------------------------------------SOLVER 7

.. _solver7_solver:

**SOLVER** | *default:* undefined |break| The solver to attack the system of linear equations arising of FE approach with.

   **Possible values:**

   - UMFPACK
   - Superlu
   - Belos
   - undefined

.. _solver7_azsolve:

**AZSOLVE** | *default:* GMRES |break| Type of linear solver algorithm to use.

   **Possible values:**

   - CG
   - GMRES
   - BiCGSTAB

.. _solver7_azprec:

**AZPREC** | *default:* ILU |break| Type of internal preconditioner to use.
Note! this preconditioner will only be used if the input operator
supports the Epetra_RowMatrix interface and the client does not pass
in an external preconditioner!

   **Possible values:**

   - ILU
   - ICC
   - ML
   - MLFLUID
   - MLFLUID2
   - MueLu
   - MueLu_fluid
   - MueLu_tsi
   - MueLu_contactSP
   - MueLu_BeamSolid
   - MueLu_fsi
   - AMGnxn
   - BGS2x2
   - CheapSIMPLE

.. _solver7_ifpackoverlap:

**IFPACKOVERLAP** | *default:* 0 |break| The amount of overlap used for the ifpack "ilu" preconditioner.

.. _solver7_ifpackgfill:

**IFPACKGFILL** | *default:* 0 |break| The amount of fill allowed for an internal "ilu" preconditioner.

.. _solver7_ifpackcombine:

**IFPACKCOMBINE** | *default:* Add |break| Combine mode for Ifpack Additive Schwarz

   **Possible values:**

   - Add
   - Insert
   - Zero

.. _solver7_aziter:

**AZITER** | *default:* 1000 |break| The maximum number of iterations the underlying iterative solver is allowed to perform

.. _solver7_aztol:

**AZTOL** | *default:* 1e-08 |break| The level the residual norms must reach to decide about successful convergence

.. _solver7_azconv:

**AZCONV** | *default:* AZ_r0 |break| The implicit residual norm scaling type to use for terminating the iterative solver.

   **Possible values:**

   - AZ_r0
   - AZ_noscaled

.. _solver7_azoutput:

**AZOUTPUT** | *default:* 0 |break| The number of iterations between each output of the solver's progress is written to screen

.. _solver7_azreuse:

**AZREUSE** | *default:* 0 |break| The number specifying how often to recompute some preconditioners

.. _solver7_azsub:

**AZSUB** | *default:* 50 |break| The maximum size of the Krylov subspace used with "GMRES" before
a restart is performed.

.. _solver7_azgraph:

**AZGRAPH** | *default:* 0 |break| unused

.. _solver7_azbdiag:

**AZBDIAG** | *default:* 0 |break| unused

.. _solver7_azomega:

**AZOMEGA** | *default:* 0 |break| unused

.. _solver7_ml_print:

**ML_PRINT** | *default:* 0 |break| ML print-out level (0-10)

.. _solver7_ml_maxcoarsesize:

**ML_MAXCOARSESIZE** | *default:* 5000 |break| ML stop coarsening when coarse ndof smaller then this

.. _solver7_ml_maxlevel:

**ML_MAXLEVEL** | *default:* 5 |break| ML max number of levels

.. _solver7_ml_agg_size:

**ML_AGG_SIZE** | *default:* 27 |break| objective size of an aggregate with METIS/VBMETIS, 2D: 9, 3D: 27

.. _solver7_ml_dampfine:

**ML_DAMPFINE** | *default:* 1 |break| damping fine grid

.. _solver7_ml_dampmed:

**ML_DAMPMED** | *default:* 1 |break| damping med grids

.. _solver7_ml_dampcoarse:

**ML_DAMPCOARSE** | *default:* 1 |break| damping coarse grid

.. _solver7_ml_prolong_smo:

**ML_PROLONG_SMO** | *default:* 0 |break| damping factor for prolongator smoother (usually 1.33 or 0.0)

.. _solver7_ml_prolong_thres:

**ML_PROLONG_THRES** | *default:* 0 |break| threshold for prolongator smoother/aggregation

.. _solver7_ml_smotimes:

**ML_SMOTIMES** | *default:* 1 1 1 1 1 |break| no. smoothing steps or polynomial order on each level (at least ML_MAXLEVEL numbers)

.. _solver7_ml_coarsen:

**ML_COARSEN** | *default:* UC |break| no description yet

   **Possible values:**

   - UC
   - METIS
   - VBMETIS
   - MIS

.. _solver7_ml_rebalance:

**ML_REBALANCE** | *default:* Yes |break| Performe ML-internal rebalancing of coarse level operators.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _solver7_ml_smootherfine:

**ML_SMOOTHERFINE** | *default:* ILU |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver7_ml_smoothermed:

**ML_SMOOTHERMED** | *default:* ILU |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver7_ml_smoothercoarse:

**ML_SMOOTHERCOARSE** | *default:* Umfpack |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver7_sub_solver1:

**SUB_SOLVER1** | *default:* -1 |break| sub solver/smoother block number (SIMPLE/C: used for prediction of primary variable on all levels, BS: used for fine and intermedium BraessSarazin (BS) level smoother)

.. _solver7_sub_solver2:

**SUB_SOLVER2** | *default:* -1 |break| sub solver/smoother block number (SIMPLE/C: used for SchurComplement eq. on all levels, BS: used for coarse BraessSarazin (BS) level smoother)

.. _solver7_muelu_xml_file:

**MUELU_XML_FILE** | *default:* none |break| xml file defining any MueLu preconditioner

.. _solver7_muelu_xml_enforce:

**MUELU_XML_ENFORCE** | *default:* Yes |break| option defining xml file usage

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _solver7_bgs2x2_fliporder:

**BGS2X2_FLIPORDER** | *default:* block0_block1_order |break| BGS2x2 flip order parameter

   **Possible values:**

   - block0_block1_order
   - block1_block0_order

.. _solver7_bgs2x2_global_damping:

**BGS2X2_GLOBAL_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner

.. _solver7_bgs2x2_block1_damping:

**BGS2X2_BLOCK1_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner block1

.. _solver7_bgs2x2_block2_damping:

**BGS2X2_BLOCK2_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner block2

.. _solver7_azscal:

**AZSCAL** | *default:* none |break| scaling of the linear system to improve properties

   **Possible values:**

   - none
   - sym
   - infnorm

.. _solver7_permute_system:

**PERMUTE_SYSTEM** | *default:* none |break| allow linear solver to permute linear system to improve properties

   **Possible values:**

   - none
   - algebraic
   - local

.. _solver7_non_diagdominance_ratio:

**NON_DIAGDOMINANCE_RATIO** | *default:* 1 |break| matrix rows with diagEntry/maxEntry<nonDiagDominanceRatio are marked to be significantly non-diagonal dominant (default: 1.0 = mark all non-diagonal dominant rows)

.. _solver7_name:

**NAME** | *default:* No_name |break| User specified name for solver block

.. _solver7_simple_damping:

**SIMPLE_DAMPING** | *default:* 1 |break| damping parameter for SIMPLE preconditioner

.. _solver7_amgnxn_type:

**AMGNXN_TYPE** | *default:* AMG(BGS) |break| Name of the pre-built preconditioner to be used. If set to"XML" the preconditioner is defined using a xml file

.. _solver7_amgnxn_xml_file:

**AMGNXN_XML_FILE** | *default:* none |break| xml file defining the AMGnxn preconditioner

.. _SECsolver8:

SOLVER 8
--------

solver parameters for solver block 8

::

   -----------------------------------------------------------SOLVER 8

.. _solver8_solver:

**SOLVER** | *default:* undefined |break| The solver to attack the system of linear equations arising of FE approach with.

   **Possible values:**

   - UMFPACK
   - Superlu
   - Belos
   - undefined

.. _solver8_azsolve:

**AZSOLVE** | *default:* GMRES |break| Type of linear solver algorithm to use.

   **Possible values:**

   - CG
   - GMRES
   - BiCGSTAB

.. _solver8_azprec:

**AZPREC** | *default:* ILU |break| Type of internal preconditioner to use.
Note! this preconditioner will only be used if the input operator
supports the Epetra_RowMatrix interface and the client does not pass
in an external preconditioner!

   **Possible values:**

   - ILU
   - ICC
   - ML
   - MLFLUID
   - MLFLUID2
   - MueLu
   - MueLu_fluid
   - MueLu_tsi
   - MueLu_contactSP
   - MueLu_BeamSolid
   - MueLu_fsi
   - AMGnxn
   - BGS2x2
   - CheapSIMPLE

.. _solver8_ifpackoverlap:

**IFPACKOVERLAP** | *default:* 0 |break| The amount of overlap used for the ifpack "ilu" preconditioner.

.. _solver8_ifpackgfill:

**IFPACKGFILL** | *default:* 0 |break| The amount of fill allowed for an internal "ilu" preconditioner.

.. _solver8_ifpackcombine:

**IFPACKCOMBINE** | *default:* Add |break| Combine mode for Ifpack Additive Schwarz

   **Possible values:**

   - Add
   - Insert
   - Zero

.. _solver8_aziter:

**AZITER** | *default:* 1000 |break| The maximum number of iterations the underlying iterative solver is allowed to perform

.. _solver8_aztol:

**AZTOL** | *default:* 1e-08 |break| The level the residual norms must reach to decide about successful convergence

.. _solver8_azconv:

**AZCONV** | *default:* AZ_r0 |break| The implicit residual norm scaling type to use for terminating the iterative solver.

   **Possible values:**

   - AZ_r0
   - AZ_noscaled

.. _solver8_azoutput:

**AZOUTPUT** | *default:* 0 |break| The number of iterations between each output of the solver's progress is written to screen

.. _solver8_azreuse:

**AZREUSE** | *default:* 0 |break| The number specifying how often to recompute some preconditioners

.. _solver8_azsub:

**AZSUB** | *default:* 50 |break| The maximum size of the Krylov subspace used with "GMRES" before
a restart is performed.

.. _solver8_azgraph:

**AZGRAPH** | *default:* 0 |break| unused

.. _solver8_azbdiag:

**AZBDIAG** | *default:* 0 |break| unused

.. _solver8_azomega:

**AZOMEGA** | *default:* 0 |break| unused

.. _solver8_ml_print:

**ML_PRINT** | *default:* 0 |break| ML print-out level (0-10)

.. _solver8_ml_maxcoarsesize:

**ML_MAXCOARSESIZE** | *default:* 5000 |break| ML stop coarsening when coarse ndof smaller then this

.. _solver8_ml_maxlevel:

**ML_MAXLEVEL** | *default:* 5 |break| ML max number of levels

.. _solver8_ml_agg_size:

**ML_AGG_SIZE** | *default:* 27 |break| objective size of an aggregate with METIS/VBMETIS, 2D: 9, 3D: 27

.. _solver8_ml_dampfine:

**ML_DAMPFINE** | *default:* 1 |break| damping fine grid

.. _solver8_ml_dampmed:

**ML_DAMPMED** | *default:* 1 |break| damping med grids

.. _solver8_ml_dampcoarse:

**ML_DAMPCOARSE** | *default:* 1 |break| damping coarse grid

.. _solver8_ml_prolong_smo:

**ML_PROLONG_SMO** | *default:* 0 |break| damping factor for prolongator smoother (usually 1.33 or 0.0)

.. _solver8_ml_prolong_thres:

**ML_PROLONG_THRES** | *default:* 0 |break| threshold for prolongator smoother/aggregation

.. _solver8_ml_smotimes:

**ML_SMOTIMES** | *default:* 1 1 1 1 1 |break| no. smoothing steps or polynomial order on each level (at least ML_MAXLEVEL numbers)

.. _solver8_ml_coarsen:

**ML_COARSEN** | *default:* UC |break| no description yet

   **Possible values:**

   - UC
   - METIS
   - VBMETIS
   - MIS

.. _solver8_ml_rebalance:

**ML_REBALANCE** | *default:* Yes |break| Performe ML-internal rebalancing of coarse level operators.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _solver8_ml_smootherfine:

**ML_SMOOTHERFINE** | *default:* ILU |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver8_ml_smoothermed:

**ML_SMOOTHERMED** | *default:* ILU |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver8_ml_smoothercoarse:

**ML_SMOOTHERCOARSE** | *default:* Umfpack |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver8_sub_solver1:

**SUB_SOLVER1** | *default:* -1 |break| sub solver/smoother block number (SIMPLE/C: used for prediction of primary variable on all levels, BS: used for fine and intermedium BraessSarazin (BS) level smoother)

.. _solver8_sub_solver2:

**SUB_SOLVER2** | *default:* -1 |break| sub solver/smoother block number (SIMPLE/C: used for SchurComplement eq. on all levels, BS: used for coarse BraessSarazin (BS) level smoother)

.. _solver8_muelu_xml_file:

**MUELU_XML_FILE** | *default:* none |break| xml file defining any MueLu preconditioner

.. _solver8_muelu_xml_enforce:

**MUELU_XML_ENFORCE** | *default:* Yes |break| option defining xml file usage

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _solver8_bgs2x2_fliporder:

**BGS2X2_FLIPORDER** | *default:* block0_block1_order |break| BGS2x2 flip order parameter

   **Possible values:**

   - block0_block1_order
   - block1_block0_order

.. _solver8_bgs2x2_global_damping:

**BGS2X2_GLOBAL_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner

.. _solver8_bgs2x2_block1_damping:

**BGS2X2_BLOCK1_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner block1

.. _solver8_bgs2x2_block2_damping:

**BGS2X2_BLOCK2_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner block2

.. _solver8_azscal:

**AZSCAL** | *default:* none |break| scaling of the linear system to improve properties

   **Possible values:**

   - none
   - sym
   - infnorm

.. _solver8_permute_system:

**PERMUTE_SYSTEM** | *default:* none |break| allow linear solver to permute linear system to improve properties

   **Possible values:**

   - none
   - algebraic
   - local

.. _solver8_non_diagdominance_ratio:

**NON_DIAGDOMINANCE_RATIO** | *default:* 1 |break| matrix rows with diagEntry/maxEntry<nonDiagDominanceRatio are marked to be significantly non-diagonal dominant (default: 1.0 = mark all non-diagonal dominant rows)

.. _solver8_name:

**NAME** | *default:* No_name |break| User specified name for solver block

.. _solver8_simple_damping:

**SIMPLE_DAMPING** | *default:* 1 |break| damping parameter for SIMPLE preconditioner

.. _solver8_amgnxn_type:

**AMGNXN_TYPE** | *default:* AMG(BGS) |break| Name of the pre-built preconditioner to be used. If set to"XML" the preconditioner is defined using a xml file

.. _solver8_amgnxn_xml_file:

**AMGNXN_XML_FILE** | *default:* none |break| xml file defining the AMGnxn preconditioner

.. _SECsolver9:

SOLVER 9
--------

solver parameters for solver block 9

::

   -----------------------------------------------------------SOLVER 9

.. _solver9_solver:

**SOLVER** | *default:* undefined |break| The solver to attack the system of linear equations arising of FE approach with.

   **Possible values:**

   - UMFPACK
   - Superlu
   - Belos
   - undefined

.. _solver9_azsolve:

**AZSOLVE** | *default:* GMRES |break| Type of linear solver algorithm to use.

   **Possible values:**

   - CG
   - GMRES
   - BiCGSTAB

.. _solver9_azprec:

**AZPREC** | *default:* ILU |break| Type of internal preconditioner to use.
Note! this preconditioner will only be used if the input operator
supports the Epetra_RowMatrix interface and the client does not pass
in an external preconditioner!

   **Possible values:**

   - ILU
   - ICC
   - ML
   - MLFLUID
   - MLFLUID2
   - MueLu
   - MueLu_fluid
   - MueLu_tsi
   - MueLu_contactSP
   - MueLu_BeamSolid
   - MueLu_fsi
   - AMGnxn
   - BGS2x2
   - CheapSIMPLE

.. _solver9_ifpackoverlap:

**IFPACKOVERLAP** | *default:* 0 |break| The amount of overlap used for the ifpack "ilu" preconditioner.

.. _solver9_ifpackgfill:

**IFPACKGFILL** | *default:* 0 |break| The amount of fill allowed for an internal "ilu" preconditioner.

.. _solver9_ifpackcombine:

**IFPACKCOMBINE** | *default:* Add |break| Combine mode for Ifpack Additive Schwarz

   **Possible values:**

   - Add
   - Insert
   - Zero

.. _solver9_aziter:

**AZITER** | *default:* 1000 |break| The maximum number of iterations the underlying iterative solver is allowed to perform

.. _solver9_aztol:

**AZTOL** | *default:* 1e-08 |break| The level the residual norms must reach to decide about successful convergence

.. _solver9_azconv:

**AZCONV** | *default:* AZ_r0 |break| The implicit residual norm scaling type to use for terminating the iterative solver.

   **Possible values:**

   - AZ_r0
   - AZ_noscaled

.. _solver9_azoutput:

**AZOUTPUT** | *default:* 0 |break| The number of iterations between each output of the solver's progress is written to screen

.. _solver9_azreuse:

**AZREUSE** | *default:* 0 |break| The number specifying how often to recompute some preconditioners

.. _solver9_azsub:

**AZSUB** | *default:* 50 |break| The maximum size of the Krylov subspace used with "GMRES" before
a restart is performed.

.. _solver9_azgraph:

**AZGRAPH** | *default:* 0 |break| unused

.. _solver9_azbdiag:

**AZBDIAG** | *default:* 0 |break| unused

.. _solver9_azomega:

**AZOMEGA** | *default:* 0 |break| unused

.. _solver9_ml_print:

**ML_PRINT** | *default:* 0 |break| ML print-out level (0-10)

.. _solver9_ml_maxcoarsesize:

**ML_MAXCOARSESIZE** | *default:* 5000 |break| ML stop coarsening when coarse ndof smaller then this

.. _solver9_ml_maxlevel:

**ML_MAXLEVEL** | *default:* 5 |break| ML max number of levels

.. _solver9_ml_agg_size:

**ML_AGG_SIZE** | *default:* 27 |break| objective size of an aggregate with METIS/VBMETIS, 2D: 9, 3D: 27

.. _solver9_ml_dampfine:

**ML_DAMPFINE** | *default:* 1 |break| damping fine grid

.. _solver9_ml_dampmed:

**ML_DAMPMED** | *default:* 1 |break| damping med grids

.. _solver9_ml_dampcoarse:

**ML_DAMPCOARSE** | *default:* 1 |break| damping coarse grid

.. _solver9_ml_prolong_smo:

**ML_PROLONG_SMO** | *default:* 0 |break| damping factor for prolongator smoother (usually 1.33 or 0.0)

.. _solver9_ml_prolong_thres:

**ML_PROLONG_THRES** | *default:* 0 |break| threshold for prolongator smoother/aggregation

.. _solver9_ml_smotimes:

**ML_SMOTIMES** | *default:* 1 1 1 1 1 |break| no. smoothing steps or polynomial order on each level (at least ML_MAXLEVEL numbers)

.. _solver9_ml_coarsen:

**ML_COARSEN** | *default:* UC |break| no description yet

   **Possible values:**

   - UC
   - METIS
   - VBMETIS
   - MIS

.. _solver9_ml_rebalance:

**ML_REBALANCE** | *default:* Yes |break| Performe ML-internal rebalancing of coarse level operators.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _solver9_ml_smootherfine:

**ML_SMOOTHERFINE** | *default:* ILU |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver9_ml_smoothermed:

**ML_SMOOTHERMED** | *default:* ILU |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver9_ml_smoothercoarse:

**ML_SMOOTHERCOARSE** | *default:* Umfpack |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _solver9_sub_solver1:

**SUB_SOLVER1** | *default:* -1 |break| sub solver/smoother block number (SIMPLE/C: used for prediction of primary variable on all levels, BS: used for fine and intermedium BraessSarazin (BS) level smoother)

.. _solver9_sub_solver2:

**SUB_SOLVER2** | *default:* -1 |break| sub solver/smoother block number (SIMPLE/C: used for SchurComplement eq. on all levels, BS: used for coarse BraessSarazin (BS) level smoother)

.. _solver9_muelu_xml_file:

**MUELU_XML_FILE** | *default:* none |break| xml file defining any MueLu preconditioner

.. _solver9_muelu_xml_enforce:

**MUELU_XML_ENFORCE** | *default:* Yes |break| option defining xml file usage

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _solver9_bgs2x2_fliporder:

**BGS2X2_FLIPORDER** | *default:* block0_block1_order |break| BGS2x2 flip order parameter

   **Possible values:**

   - block0_block1_order
   - block1_block0_order

.. _solver9_bgs2x2_global_damping:

**BGS2X2_GLOBAL_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner

.. _solver9_bgs2x2_block1_damping:

**BGS2X2_BLOCK1_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner block1

.. _solver9_bgs2x2_block2_damping:

**BGS2X2_BLOCK2_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner block2

.. _solver9_azscal:

**AZSCAL** | *default:* none |break| scaling of the linear system to improve properties

   **Possible values:**

   - none
   - sym
   - infnorm

.. _solver9_permute_system:

**PERMUTE_SYSTEM** | *default:* none |break| allow linear solver to permute linear system to improve properties

   **Possible values:**

   - none
   - algebraic
   - local

.. _solver9_non_diagdominance_ratio:

**NON_DIAGDOMINANCE_RATIO** | *default:* 1 |break| matrix rows with diagEntry/maxEntry<nonDiagDominanceRatio are marked to be significantly non-diagonal dominant (default: 1.0 = mark all non-diagonal dominant rows)

.. _solver9_name:

**NAME** | *default:* No_name |break| User specified name for solver block

.. _solver9_simple_damping:

**SIMPLE_DAMPING** | *default:* 1 |break| damping parameter for SIMPLE preconditioner

.. _solver9_amgnxn_type:

**AMGNXN_TYPE** | *default:* AMG(BGS) |break| Name of the pre-built preconditioner to be used. If set to"XML" the preconditioner is defined using a xml file

.. _solver9_amgnxn_xml_file:

**AMGNXN_XML_FILE** | *default:* none |break| xml file defining the AMGnxn preconditioner

.. _SECumfpacksolver:

UMFPACK SOLVER
--------------

solver parameters for UMFPACK

::

   -----------------------------------------------------UMFPACK SOLVER

.. _umfpacksolver_solver:

**SOLVER** | *default:* undefined |break| The solver to attack the system of linear equations arising of FE approach with.

   **Possible values:**

   - UMFPACK
   - Superlu
   - Belos
   - undefined

.. _umfpacksolver_azsolve:

**AZSOLVE** | *default:* GMRES |break| Type of linear solver algorithm to use.

   **Possible values:**

   - CG
   - GMRES
   - BiCGSTAB

.. _umfpacksolver_azprec:

**AZPREC** | *default:* ILU |break| Type of internal preconditioner to use.
Note! this preconditioner will only be used if the input operator
supports the Epetra_RowMatrix interface and the client does not pass
in an external preconditioner!

   **Possible values:**

   - ILU
   - ICC
   - ML
   - MLFLUID
   - MLFLUID2
   - MueLu
   - MueLu_fluid
   - MueLu_tsi
   - MueLu_contactSP
   - MueLu_BeamSolid
   - MueLu_fsi
   - AMGnxn
   - BGS2x2
   - CheapSIMPLE

.. _umfpacksolver_ifpackoverlap:

**IFPACKOVERLAP** | *default:* 0 |break| The amount of overlap used for the ifpack "ilu" preconditioner.

.. _umfpacksolver_ifpackgfill:

**IFPACKGFILL** | *default:* 0 |break| The amount of fill allowed for an internal "ilu" preconditioner.

.. _umfpacksolver_ifpackcombine:

**IFPACKCOMBINE** | *default:* Add |break| Combine mode for Ifpack Additive Schwarz

   **Possible values:**

   - Add
   - Insert
   - Zero

.. _umfpacksolver_aziter:

**AZITER** | *default:* 1000 |break| The maximum number of iterations the underlying iterative solver is allowed to perform

.. _umfpacksolver_aztol:

**AZTOL** | *default:* 1e-08 |break| The level the residual norms must reach to decide about successful convergence

.. _umfpacksolver_azconv:

**AZCONV** | *default:* AZ_r0 |break| The implicit residual norm scaling type to use for terminating the iterative solver.

   **Possible values:**

   - AZ_r0
   - AZ_noscaled

.. _umfpacksolver_azoutput:

**AZOUTPUT** | *default:* 0 |break| The number of iterations between each output of the solver's progress is written to screen

.. _umfpacksolver_azreuse:

**AZREUSE** | *default:* 0 |break| The number specifying how often to recompute some preconditioners

.. _umfpacksolver_azsub:

**AZSUB** | *default:* 50 |break| The maximum size of the Krylov subspace used with "GMRES" before
a restart is performed.

.. _umfpacksolver_azgraph:

**AZGRAPH** | *default:* 0 |break| unused

.. _umfpacksolver_azbdiag:

**AZBDIAG** | *default:* 0 |break| unused

.. _umfpacksolver_azomega:

**AZOMEGA** | *default:* 0 |break| unused

.. _umfpacksolver_ml_print:

**ML_PRINT** | *default:* 0 |break| ML print-out level (0-10)

.. _umfpacksolver_ml_maxcoarsesize:

**ML_MAXCOARSESIZE** | *default:* 5000 |break| ML stop coarsening when coarse ndof smaller then this

.. _umfpacksolver_ml_maxlevel:

**ML_MAXLEVEL** | *default:* 5 |break| ML max number of levels

.. _umfpacksolver_ml_agg_size:

**ML_AGG_SIZE** | *default:* 27 |break| objective size of an aggregate with METIS/VBMETIS, 2D: 9, 3D: 27

.. _umfpacksolver_ml_dampfine:

**ML_DAMPFINE** | *default:* 1 |break| damping fine grid

.. _umfpacksolver_ml_dampmed:

**ML_DAMPMED** | *default:* 1 |break| damping med grids

.. _umfpacksolver_ml_dampcoarse:

**ML_DAMPCOARSE** | *default:* 1 |break| damping coarse grid

.. _umfpacksolver_ml_prolong_smo:

**ML_PROLONG_SMO** | *default:* 0 |break| damping factor for prolongator smoother (usually 1.33 or 0.0)

.. _umfpacksolver_ml_prolong_thres:

**ML_PROLONG_THRES** | *default:* 0 |break| threshold for prolongator smoother/aggregation

.. _umfpacksolver_ml_smotimes:

**ML_SMOTIMES** | *default:* 1 1 1 1 1 |break| no. smoothing steps or polynomial order on each level (at least ML_MAXLEVEL numbers)

.. _umfpacksolver_ml_coarsen:

**ML_COARSEN** | *default:* UC |break| no description yet

   **Possible values:**

   - UC
   - METIS
   - VBMETIS
   - MIS

.. _umfpacksolver_ml_rebalance:

**ML_REBALANCE** | *default:* Yes |break| Performe ML-internal rebalancing of coarse level operators.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _umfpacksolver_ml_smootherfine:

**ML_SMOOTHERFINE** | *default:* ILU |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _umfpacksolver_ml_smoothermed:

**ML_SMOOTHERMED** | *default:* ILU |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _umfpacksolver_ml_smoothercoarse:

**ML_SMOOTHERCOARSE** | *default:* Umfpack |break| no description yet

   **Possible values:**

   - SGS
   - Jacobi
   - Chebychev
   - MLS
   - ILU
   - KLU
   - Superlu
   - GS
   - DGS
   - Umfpack
   - BS
   - SIMPLE
   - SIMPLEC
   - IBD
   - Uzawa

.. _umfpacksolver_sub_solver1:

**SUB_SOLVER1** | *default:* -1 |break| sub solver/smoother block number (SIMPLE/C: used for prediction of primary variable on all levels, BS: used for fine and intermedium BraessSarazin (BS) level smoother)

.. _umfpacksolver_sub_solver2:

**SUB_SOLVER2** | *default:* -1 |break| sub solver/smoother block number (SIMPLE/C: used for SchurComplement eq. on all levels, BS: used for coarse BraessSarazin (BS) level smoother)

.. _umfpacksolver_muelu_xml_file:

**MUELU_XML_FILE** | *default:* none |break| xml file defining any MueLu preconditioner

.. _umfpacksolver_muelu_xml_enforce:

**MUELU_XML_ENFORCE** | *default:* Yes |break| option defining xml file usage

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _umfpacksolver_bgs2x2_fliporder:

**BGS2X2_FLIPORDER** | *default:* block0_block1_order |break| BGS2x2 flip order parameter

   **Possible values:**

   - block0_block1_order
   - block1_block0_order

.. _umfpacksolver_bgs2x2_global_damping:

**BGS2X2_GLOBAL_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner

.. _umfpacksolver_bgs2x2_block1_damping:

**BGS2X2_BLOCK1_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner block1

.. _umfpacksolver_bgs2x2_block2_damping:

**BGS2X2_BLOCK2_DAMPING** | *default:* 1 |break| damping parameter for BGS2X2 preconditioner block2

.. _umfpacksolver_azscal:

**AZSCAL** | *default:* none |break| scaling of the linear system to improve properties

   **Possible values:**

   - none
   - sym
   - infnorm

.. _umfpacksolver_permute_system:

**PERMUTE_SYSTEM** | *default:* none |break| allow linear solver to permute linear system to improve properties

   **Possible values:**

   - none
   - algebraic
   - local

.. _umfpacksolver_non_diagdominance_ratio:

**NON_DIAGDOMINANCE_RATIO** | *default:* 1 |break| matrix rows with diagEntry/maxEntry<nonDiagDominanceRatio are marked to be significantly non-diagonal dominant (default: 1.0 = mark all non-diagonal dominant rows)

.. _umfpacksolver_name:

**NAME** | *default:* No_name |break| User specified name for solver block

.. _umfpacksolver_simple_damping:

**SIMPLE_DAMPING** | *default:* 1 |break| damping parameter for SIMPLE preconditioner

.. _umfpacksolver_amgnxn_type:

**AMGNXN_TYPE** | *default:* AMG(BGS) |break| Name of the pre-built preconditioner to be used. If set to"XML" the preconditioner is defined using a xml file

.. _umfpacksolver_amgnxn_xml_file:

**AMGNXN_XML_FILE** | *default:* none |break| xml file defining the AMGnxn preconditioner

.. _SECstructnox:

STRUCT NOX
----------

no description yet

::

   ---------------------------------------------------------STRUCT NOX

.. _structnox_nonlinearsolver:

**Nonlinear Solver** | *default:* Line Search Based |break| no description yet

   **Possible values:**

   - Line Search Based
   - Pseudo Transient
   - Trust Region Based
   - Inexact Trust Region Based
   - Tensor Based

.. _SECstructnox_direction:

STRUCT NOX/Direction
~~~~~~~~~~~~~~~~~~~~

no description yet

::

   -----------------------------------------------STRUCT NOX/Direction

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _structnox_direction_method:

**Method** | *default:* Newton |break| Choose a direction method for the nonlinear solver.

   **Possible values:**

   - Newton
   - Steepest Descent
   - NonlinearCG
   - Broyden
   - User Defined

.. _structnox_direction_userdefinedmethod:

**User Defined Method** | *default:* Modified Newton |break| Choose a user-defined direction method.

   **Possible values:**

   - Newton
   - Modified Newton

.. _SECstructnox_direction_newton:

STRUCT NOX/Direction/Newton
~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ----------------------------------------STRUCT NOX/Direction/Newton

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _structnox_direction_newton_forcingtermmethod:

**Forcing Term Method** | *default:* Constant |break| no description yet

   **Possible values:**

   - Constant
   - Type 1
   - Type 2

.. _structnox_direction_newton_forcingterminitialtolerance:

**Forcing Term Initial Tolerance** | *default:* 0.1 |break| initial linear solver tolerance

.. _structnox_direction_newton_forcingtermminimumtolerance:

**Forcing Term Minimum Tolerance** | *default:* 1e-06 |break| no description yet

.. _structnox_direction_newton_forcingtermmaximumtolerance:

**Forcing Term Maximum Tolerance** | *default:* 0.01 |break| no description yet

.. _structnox_direction_newton_forcingtermalpha:

**Forcing Term Alpha** | *default:* 1.5 |break| used only by "Type 2"

.. _structnox_direction_newton_forcingtermgamma:

**Forcing Term Gamma** | *default:* 0.9 |break| used only by "Type 2"

.. _structnox_direction_newton_rescuebadnewtonsolve:

**Rescue Bad Newton Solve** | *default:* Yes |break| If set to true, we will use the computed direction even if the linear solve does not achieve the tolerance specified by the forcing term

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECstructnox_direction_newton_modified:

STRUCT NOX/Direction/Newton/Modified
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   -------------------------------STRUCT NOX/Direction/Newton/Modified

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _structnox_direction_newton_modified_initialprimaldiagonalcorrection:

**Initial Primal Diagonal Correction** | *default:* 0.0001 |break| Initial correction factor for the diagonal of the primal block.

.. _structnox_direction_newton_modified_minimalprimaldiagonalcorrection:

**Minimal Primal Diagonal Correction** | *default:* 1e-20 |break| Minimal correction factor for the diagonal of the primal block.

.. _structnox_direction_newton_modified_maximalprimaldiagonalcorrection:

**Maximal Primal Diagonal Correction** | *default:* 1e+40 |break| Maximal correction factor for the diagonal of the primal block.

.. _structnox_direction_newton_modified_primalreductionfactor:

**Primal Reduction Factor** | *default:* 0.333333 |break| Reduction factor for the adaption of the primal diagonal correction.

.. _structnox_direction_newton_modified_primalaccretionfactor:

**Primal Accretion Factor** | *default:* 8 |break| Accretion factor for the adaption of the primal diagonal correction.

.. _structnox_direction_newton_modified_primalhighaccretionfactor:

**Primal High Accretion Factor** | *default:* 100 |break| High accretion factor for the adaption of the primal diagonal correction.

.. _structnox_direction_newton_modified_defaultsteptest:

**Default Step Test** | *default:* none |break| Choose a proper default step test strategy.

   **Possible values:**

   - none
   - Volume Change Control

.. _structnox_direction_newton_modified_catchfloatingpointexceptions:

**Catch Floating Point Exceptions** | *default:* No |break| Set to true, iffloating point exceptions during the linear solver call should be caught by the algorithm.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECstructnox_direction_newton_linearsolver:

STRUCT NOX/Direction/Newton/Linear Solver
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   --------------------------STRUCT NOX/Direction/Newton/Linear Solver

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _structnox_direction_newton_linearsolver_adaptivecontrol:

**Adaptive Control** | *default:* No |break| Switch on adaptive control of linear solver tolerance for nonlinear solution

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _structnox_direction_newton_linearsolver_adaptivecontrolobjective:

**Adaptive Control Objective** | *default:* 0.1 |break| The linear solver shall be this much better than the current nonlinear residual in the nonlinear convergence limit

.. _structnox_direction_newton_linearsolver_zeroinitialguess:

**Zero Initial Guess** | *default:* Yes |break| Zero out the delta X vector if requested.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _structnox_direction_newton_linearsolver_computingscalingmanually:

**Computing Scaling Manually** | *default:* No |break| Allows the manually scaling of your linear system (not supported at the moment).

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _structnox_direction_newton_linearsolver_outputsolverdetails:

**Output Solver Details** | *default:* Yes |break| Switch the linear solver output on and off.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECstructnox_direction_steepestdescent:

STRUCT NOX/Direction/Steepest Descent
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ------------------------------STRUCT NOX/Direction/Steepest Descent

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _structnox_direction_steepestdescent_scalingtype:

**Scaling Type** | *default:* None |break| no description yet

   **Possible values:**

   - 2-Norm
   - Quadratic Model Min
   - F 2-Norm
   - None

.. _SECstructnox_pseudotransient:

STRUCT NOX/Pseudo Transient
~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ----------------------------------------STRUCT NOX/Pseudo Transient

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _structnox_pseudotransient_deltainit:

**deltaInit** | *default:* -1 |break| Initial time step size. If its negative, the initial time step is calculated automatically.

.. _structnox_pseudotransient_deltamax:

**deltaMax** | *default:* 1.79769e+308 |break| Maximum time step size. If the new step size is greater than this value, the transient terms will be eliminated from the Newton iteration resulting in a full Newton solve.

.. _structnox_pseudotransient_deltamin:

**deltaMin** | *default:* 1e-05 |break| Minimum step size.

.. _structnox_pseudotransient_maxnumberofptciterations:

**Max Number of PTC Iterations** | *default:* 2147483647 |break| no description yet

.. _structnox_pseudotransient_ser_alpha:

**SER_alpha** | *default:* 1 |break| Exponent of SER.

.. _structnox_pseudotransient_scalingfactor:

**ScalingFactor** | *default:* 1 |break| Scaling Factor for ptc matrix.

.. _structnox_pseudotransient_timestepcontrol:

**Time Step Control** | *default:* SER |break| no description yet

   **Possible values:**

   - SER
   - Switched Evolution Relaxation
   - TTE
   - Temporal Truncation Error
   - MRR
   - Model Reduction Ratio

.. _structnox_pseudotransient_normtypefortsc:

**Norm Type for TSC** | *default:* Max Norm |break| Norm Type for the time step control

   **Possible values:**

   - Two Norm
   - One Norm
   - Max Norm

.. _structnox_pseudotransient_scalingtype:

**Scaling Type** | *default:* Identity |break| Type of the scaling matrix for the PTC method.

   **Possible values:**

   - Identity
   - CFL Diagonal
   - Lumped Mass
   - Element based

.. _structnox_pseudotransient_buildscalingoperator:

**Build scaling operator** | *default:* every timestep |break| Build scaling operator in every iteration or timestep

   **Possible values:**

   - every iter
   - every timestep

.. _SECstructnox_linesearch:

STRUCT NOX/Line Search
~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ---------------------------------------------STRUCT NOX/Line Search

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _structnox_linesearch_method:

**Method** | *default:* Full Step |break| no description yet

   **Possible values:**

   - Full Step
   - Backtrack
   - Polynomial
   - More'-Thuente
   - User Defined

.. _structnox_linesearch_innerstatustestchecktype:

**Inner Status Test Check Type** | *default:* Minimal |break| Specify the check type for the inner status tests.

   **Possible values:**

   - Complete
   - Minimal
   - None

.. _SECstructnox_linesearch_fullstep:

STRUCT NOX/Line Search/Full Step
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   -----------------------------------STRUCT NOX/Line Search/Full Step

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _structnox_linesearch_fullstep_fullstep:

**Full Step** | *default:* 1 |break| length of a full step

.. _SECstructnox_linesearch_backtrack:

STRUCT NOX/Line Search/Backtrack
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   -----------------------------------STRUCT NOX/Line Search/Backtrack

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _structnox_linesearch_backtrack_defaultstep:

**Default Step** | *default:* 1 |break| starting step length

.. _structnox_linesearch_backtrack_minimumstep:

**Minimum Step** | *default:* 1e-12 |break| minimum acceptable step length

.. _structnox_linesearch_backtrack_recoverystep:

**Recovery Step** | *default:* 1 |break| step to take when the line search fails (defaults to value for "Default Step")

.. _structnox_linesearch_backtrack_maxiters:

**Max Iters** | *default:* 50 |break| maximum number of iterations (i.e., RHS computations)

.. _structnox_linesearch_backtrack_reductionfactor:

**Reduction Factor** | *default:* 0.5 |break| A multiplier between zero and one that reduces the step size between line search iterations

.. _structnox_linesearch_backtrack_allowexceptions:

**Allow Exceptions** | *default:* No |break| Set to true, if exceptions during the force evaluation and backtracking routine should be allowed.

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECstructnox_linesearch_polynomial:

STRUCT NOX/Line Search/Polynomial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ----------------------------------STRUCT NOX/Line Search/Polynomial

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _structnox_linesearch_polynomial_defaultstep:

**Default Step** | *default:* 1 |break| Starting step length

.. _structnox_linesearch_polynomial_maxiters:

**Max Iters** | *default:* 100 |break| Maximum number of line search iterations. The search fails if the number of iterations exceeds this value

.. _structnox_linesearch_polynomial_minimumstep:

**Minimum Step** | *default:* 1e-12 |break| Minimum acceptable step length. The search fails if the computed :math:`\lambda_k` is less than this value

.. _structnox_linesearch_polynomial_recoverysteptype:

**Recovery Step Type** | *default:* Constant |break| Determines the step size to take when the line search fails

   **Possible values:**

   - Constant
   - Last Computed Step

.. _structnox_linesearch_polynomial_recoverystep:

**Recovery Step** | *default:* 1 |break| The value of the step to take when the line search fails. Only used if the "Recovery Step Type" is set to "Constant"

.. _structnox_linesearch_polynomial_interpolationtype:

**Interpolation Type** | *default:* Cubic |break| Type of interpolation that should be used

   **Possible values:**

   - Quadratic
   - Quadratic3
   - Cubic

.. _structnox_linesearch_polynomial_minboundsfactor:

**Min Bounds Factor** | *default:* 0.1 |break| Choice for :math:`\gamma_{\min}`, i.e., the factor that limits the minimum size of the new step based on the previous step

.. _structnox_linesearch_polynomial_maxboundsfactor:

**Max Bounds Factor** | *default:* 0.5 |break| Choice for :math:`\gamma_{\max}`, i.e., the factor that limits the maximum size of the new step based on the previous step

.. _structnox_linesearch_polynomial_sufficientdecreasecondition:

**Sufficient Decrease Condition** | *default:* Armijo-Goldstein |break| Choice to use for the sufficient decrease condition

   **Possible values:**

   - Armijo-Goldstein
   - Ared/Pred
   - None

.. _structnox_linesearch_polynomial_alphafactor:

**Alpha Factor** | *default:* 0.0001 |break| Parameter choice for sufficient decrease condition

.. _structnox_linesearch_polynomial_forceinterpolation:

**Force Interpolation** | *default:* No |break| Set to true if at least one interpolation step should be used. The default is false which means that the line search will stop if the default step length satisfies the convergence criteria

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _structnox_linesearch_polynomial_usecounters:

**Use Counters** | *default:* Yes |break| Set to true if we should use counters and then output the result to the paramter list as described in Output Parameters

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _structnox_linesearch_polynomial_maximumiterationforincrease:

**Maximum Iteration for Increase** | *default:* 0 |break| Maximum index of the nonlinear iteration for which we allow a relative increase

.. _structnox_linesearch_polynomial_allowedrelativeincrease:

**Allowed Relative Increase** | *default:* 100 |break| no description yet

.. _SECstructnox_linesearch_more'-thuente:

STRUCT NOX/Line Search/More'-Thuente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   -------------------------------STRUCT NOX/Line Search/More'-Thuente

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _structnox_linesearch_more'-thuente_sufficientdecrease:

**Sufficient Decrease** | *default:* 0.0001 |break| The ftol in the sufficient decrease condition

.. _structnox_linesearch_more'-thuente_curvaturecondition:

**Curvature Condition** | *default:* 0.9999 |break| The gtol in the curvature condition

.. _structnox_linesearch_more'-thuente_intervalwidth:

**Interval Width** | *default:* 1e-15 |break| The maximum width of the interval containing the minimum of the modified function

.. _structnox_linesearch_more'-thuente_maximumstep:

**Maximum Step** | *default:* 1e+06 |break| maximum allowable step length

.. _structnox_linesearch_more'-thuente_minimumstep:

**Minimum Step** | *default:* 1e-12 |break| minimum allowable step length

.. _structnox_linesearch_more'-thuente_maxiters:

**Max Iters** | *default:* 20 |break| maximum number of right-hand-side and corresponding Jacobian evaluations

.. _structnox_linesearch_more'-thuente_defaultstep:

**Default Step** | *default:* 1 |break| starting step length

.. _structnox_linesearch_more'-thuente_recoverysteptype:

**Recovery Step Type** | *default:* Constant |break| Determines the step size to take when the line search fails

   **Possible values:**

   - Constant
   - Last Computed Step

.. _structnox_linesearch_more'-thuente_recoverystep:

**Recovery Step** | *default:* 1 |break| The value of the step to take when the line search fails. Only used if the "Recovery Step Type" is set to "Constant"

.. _structnox_linesearch_more'-thuente_sufficientdecreasecondition:

**Sufficient Decrease Condition** | *default:* Armijo-Goldstein |break| Choice to use for the sufficient decrease condition

   **Possible values:**

   - Armijo-Goldstein
   - Ared/Pred
   - None

.. _structnox_linesearch_more'-thuente_optimizeslopecalculation:

**Optimize Slope Calculation** | *default:* No |break| Boolean value. If set to true the value of :math:`s^T J^T F` is estimated using a directional derivative in a call to NOX::LineSearch::Common::computeSlopeWithOutJac. If false the slope computation is computed with the NOX::LineSearch::Common::computeSlope method. Setting this to true eliminates having to compute the Jacobian at each inner iteration of the More'-Thuente line search

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECstructnox_trustregion:

STRUCT NOX/Trust Region
~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   --------------------------------------------STRUCT NOX/Trust Region

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _structnox_trustregion_minimumtrustregionradius:

**Minimum Trust Region Radius** | *default:* 1e-06 |break| Minimum allowable trust region radius

.. _structnox_trustregion_maximumtrustregionradius:

**Maximum Trust Region Radius** | *default:* 1e+09 |break| Maximum allowable trust region radius

.. _structnox_trustregion_minimumimprovementratio:

**Minimum Improvement Ratio** | *default:* 0.0001 |break| Minimum improvement ratio to accept the step

.. _structnox_trustregion_contractiontriggerratio:

**Contraction Trigger Ratio** | *default:* 0.1 |break| If the improvement ratio is less than this value, then the trust region is contracted by the amount specified by the "Contraction Factor". Must be larger than "Minimum Improvement Ratio"

.. _structnox_trustregion_contractionfactor:

**Contraction Factor** | *default:* 0.25 |break| no description yet

.. _structnox_trustregion_expansiontriggerratio:

**Expansion Trigger Ratio** | *default:* 0.75 |break| If the improvement ratio is greater than this value, then the trust region is contracted by the amount specified by the "Expansion Factor"

.. _structnox_trustregion_expansionfactor:

**Expansion Factor** | *default:* 4 |break| no description yet

.. _structnox_trustregion_recoverystep:

**Recovery Step** | *default:* 1 |break| no description yet

.. _SECstructnox_printing:

STRUCT NOX/Printing
~~~~~~~~~~~~~~~~~~~

no description yet

::

   ------------------------------------------------STRUCT NOX/Printing

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _structnox_printing_error:

**Error** | *default:* No |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _structnox_printing_warning:

**Warning** | *default:* Yes |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _structnox_printing_outeriteration:

**Outer Iteration** | *default:* Yes |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _structnox_printing_inneriteration:

**Inner Iteration** | *default:* Yes |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _structnox_printing_parameters:

**Parameters** | *default:* No |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _structnox_printing_details:

**Details** | *default:* No |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _structnox_printing_outeriterationstatustest:

**Outer Iteration StatusTest** | *default:* Yes |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _structnox_printing_linearsolverdetails:

**Linear Solver Details** | *default:* No |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _structnox_printing_testdetails:

**Test Details** | *default:* No |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _structnox_printing_debug:

**Debug** | *default:* No |break| no description yet

   **Possible values:**

   - Yes
   - No
   - yes
   - no
   - YES
   - NO

.. _SECstructnox_statustest:

STRUCT NOX/Status Test
~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ---------------------------------------------STRUCT NOX/Status Test

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _structnox_statustest_xmlfile:

**XML File** | *default:* none |break| Filename of XML file with configuration of nox status test

.. _SECstructnox_solveroptions:

STRUCT NOX/Solver Options
~~~~~~~~~~~~~~~~~~~~~~~~~

no description yet

::

   ------------------------------------------STRUCT NOX/Solver Options

.. note::

      The parameters in this section need an equal sign (=) between the parameter name and its value!

.. _structnox_solveroptions_meritfunction:

**Merit Function** | *default:* Sum of Squares |break| no description yet

   **Possible values:**

   - Sum of Squares
   - Lagrangian
   - Lagrangian Active

.. _structnox_solveroptions_statustestchecktype:

**Status Test Check Type** | *default:* Complete |break| no description yet

   **Possible values:**

   - Complete
   - Minimal
   - None

.. _SECtutorialdynamic:

TUTORIAL DYNAMIC
----------------

General parameters for any tutorial

::

   ---------------------------------------------------TUTORIAL DYNAMIC

.. _tutorialdynamic_type:

**TYPE** | *default:* NonlinearTruss |break| Choose type of tutorial.

   **Possible values:**

   - NonlinearTruss
   - FixedPointPartitioned
   - StructAleCoupling

.. _tutorialdynamic_timestep:

**TIMESTEP** | *default:* 0.1 |break| Size of time step (n -> n+1)

.. _tutorialdynamic_maxtime:

**MAXTIME** | *default:* 1 |break| Maximum Time of Simulation

.. _SECtutorialdynamic_nonlineartruss:

TUTORIAL DYNAMIC/NONLINEAR TRUSS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Control the nonlinear Truss Tutorial

::

   -----------------------------------TUTORIAL DYNAMIC/NONLINEAR TRUSS

.. _tutorialdynamic_nonlineartruss_convtol_res:

**CONVTOL_RES** | *default:* 1e-07 |break| Convergence Criterion for Residual

.. _tutorialdynamic_nonlineartruss_convtol_inc:

**CONVTOL_INC** | *default:* 1e-07 |break| Convergence Criterion for Increment

.. _SECtutorialdynamic_fixedpointscheme:

TUTORIAL DYNAMIC/FIXED POINT SCHEME
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Control the Fixed-Point Tutorial

::

   --------------------------------TUTORIAL DYNAMIC/FIXED POINT SCHEME

.. _tutorialdynamic_fixedpointscheme_relax_parameter:

**RELAX_PARAMETER** | *default:* 1 |break| Relaxation Parameter omega

.. _tutorialdynamic_fixedpointscheme_convtol:

**CONVTOL** | *default:* 1e-07 |break| Convergence Criterion for Increment of Partitioned Scheme

