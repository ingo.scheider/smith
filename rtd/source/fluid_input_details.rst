old_Fluid input details
=======================

Which stabilization is available for which element?
---------------------------------------------------

::

          +--------+--------+--------+--------+--------+--------+
          |  GLS   | USFEM  |  TDS   | FAST*) | INFSUP |  PRO   |
   -------+--------+--------+--------+--------+--------+--------+
   TRI3   |    X   |  %(X)  |    X   |        |        |        |
   -------+--------+--------+--------+--------+--------+--------+
   TRI6   |    X   |  %(X)  |    X   |        |        |        |
   -------+--------+--------+--------+--------+--------+--------+
   QUAD4  |    X   |    X   |    X   |        |        |    X   |
   -------+--------+--------+--------+--------+--------+--------+
   QUAD8  |    X   |    X   |    X   |        |    X   |    X   |
   -------+--------+--------+--------+--------+--------+--------+
   QUAD9  |    X   |    X   |    X   |        |    X   |    X   |
   -------+--------+--------+--------+--------+--------+--------+
   TET4   |    X   |    X   |        |    X   |        |        |
   -------+--------+--------+--------+--------+--------+--------+
   TET10  |        |        |        |        |        |        |
   -------+--------+--------+--------+--------+--------+--------+
   HEX8   |    X   |    X   |        |    X   |        |    X   |
   -------+--------+--------+--------+--------+--------+--------+
   HEX20  |        |        |        |    X   |    X   |    X   |
   -------+--------+--------+--------+--------+--------+--------+
   HEX27  |    X   |    X   |        |    X   |    X   |    X   |
   -------+--------+--------+--------+--------+--------+--------+



   *) for FAST elements USFEM and GLS is implemented

   %  for USFEM, the calculation of the stability parameter doesn't
      work for TRI3 and TRI6 --- but only small changes are 
      necessary.

Remarks:

-  The ``FAST`` Fortran elements are slower on non-vector machines.

-  InfSup stable elements exist.

Input for fluid problems on deforming domains
---------------------------------------------

Remarks:

-  We need both, a fluid and an ALE discretisation

-  Fluid and ALE meshes must match!

-  The ``ALE`` flag is required for the fluid elements
