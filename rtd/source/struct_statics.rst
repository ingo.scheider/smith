old_Static solution techniques for structures
=============================================

Linear statics
--------------

Works linearly.

Geometrically non-linear statics
--------------------------------

**Gid**

Data : Problem Data : Static

**Overal Specs**

   | ``NUMSTEP`` :math:`int` number of load steps (pseudo time steps)
   | ``MAXITER`` :math:`int` numer of equlibrium iterations
   | ``TOLRESID`` :math:`real` iteration tolerance for residuum
   | ``TOLDISP`` :math:`real` iteration tolerance for incremental
     displacements
   | ``STEPSIZE`` :math:`int`
   | ``RESEVRYDISP`` :math:`int` displacement results every :math:`int`
     step
   | ``RESEVRYSTRS`` :math:`int` stress results every :math:`int` step
   | ``RESTARTEVRY`` :math:`int` restart data every :math:`int` step

**Remember**

Nice for plotting Load-Displacement curves: The load factor and the
result for the controlled dof are written to xxx.cur

Load control
^^^^^^^^^^^^

Is implemented.

Displacement control
^^^^^^^^^^^^^^^^^^^^

**Control Flag**

   | ``NEWTONRAPHSO`` ``Displacement_Control``

**Node and DOF to be controlled**

   | ``————————————————CONTROL NODE``
   | ``NODE`` :math:`int_1` ``DOF`` :math:`int_2`

**Gid** Single Layer : Point : Control Node

**Remarks**

-  *Not possible to have more than one node/dof controlled.*

Arc-length control
^^^^^^^^^^^^^^^^^^

**Control Flag**

   | ``NEWTONRAPHSO`` ``Arc_Control``

**Variants**

   | ``IARC`` ``Yes`` :math:`|` ``No``
   | ``ARCSCL`` ``0`` :math:`|` ``1``
   | By default cylindrical method is used, only if ``IARC`` ``Yes`` AND
     ``ARCSCL`` ``1`` spherical method is applied (cf. Crisfield)

**Options**

   ``SIGNCHCSP`` ``Yes`` :math:`|` ``No``

   “flags on a better control of sign for the predictor depending on the
   sign of the current stiffness, i.e. to improve handling of limit
   points”

Remarks and To Do
^^^^^^^^^^^^^^^^^

**W1-Eulerstab**

Results with all solution algorithms seem to be ok.

**Scordelis Loo Example**

*With Shell8 I could not get to serious results at all, what happens?
With Brick1 problems to get over the limit point*
