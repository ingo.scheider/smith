..
   Created using baci version (git SHA1):
   eeea9d37d6cc009fe38bd81cbc4579befe0c2b2e

.. _prescribedconditionreference:

Prescribed Condition Reference
==============================

.. _designpointneumannconditions:

DESIGN POINT NEUMANN CONDITIONS
-------------------------------

Point Neumann

::

   ------------------------------------DESIGN POINT NEUMANN CONDITIONS
   DPOINT  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designpointmomentebconditions:

DESIGN POINT MOMENT EB CONDITIONS
---------------------------------

Point Neumann Moment for an Euler-Bernoulli beam

::

   ----------------------------------DESIGN POINT MOMENT EB CONDITIONS
   DPOINT  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designlineneumannconditions:

DESIGN LINE NEUMANN CONDITIONS
------------------------------

Line Neumann

::

   -------------------------------------DESIGN LINE NEUMANN CONDITIONS
   DLINE  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designsurfneumannconditions:

DESIGN SURF NEUMANN CONDITIONS
------------------------------

Surface Neumann

::

   -------------------------------------DESIGN SURF NEUMANN CONDITIONS
   DSURF  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designvolneumannconditions:

DESIGN VOL NEUMANN CONDITIONS
-----------------------------

Volume Neumann

::

   --------------------------------------DESIGN VOL NEUMANN CONDITIONS
   DVOL  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designpointtransportneumannconditions:

DESIGN POINT TRANSPORT NEUMANN CONDITIONS
-----------------------------------------

Point Neumann

::

   --------------------------DESIGN POINT TRANSPORT NEUMANN CONDITIONS
   DPOINT  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designlinetransportneumannconditions:

DESIGN LINE TRANSPORT NEUMANN CONDITIONS
----------------------------------------

Line Neumann

::

   ---------------------------DESIGN LINE TRANSPORT NEUMANN CONDITIONS
   DLINE  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designsurftransportneumannconditions:

DESIGN SURF TRANSPORT NEUMANN CONDITIONS
----------------------------------------

Surface Neumann

::

   ---------------------------DESIGN SURF TRANSPORT NEUMANN CONDITIONS
   DSURF  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designvoltransportneumannconditions:

DESIGN VOL TRANSPORT NEUMANN CONDITIONS
---------------------------------------

Volume Neumann

::

   ----------------------------DESIGN VOL TRANSPORT NEUMANN CONDITIONS
   DVOL  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designpointthermoneumannconditions:

DESIGN POINT THERMO NEUMANN CONDITIONS
--------------------------------------

Point Neumann

::

   -----------------------------DESIGN POINT THERMO NEUMANN CONDITIONS
   DPOINT  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designlinethermoneumannconditions:

DESIGN LINE THERMO NEUMANN CONDITIONS
-------------------------------------

Line Neumann

::

   ------------------------------DESIGN LINE THERMO NEUMANN CONDITIONS
   DLINE  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designsurfthermoneumannconditions:

DESIGN SURF THERMO NEUMANN CONDITIONS
-------------------------------------

Surface Neumann

::

   ------------------------------DESIGN SURF THERMO NEUMANN CONDITIONS
   DSURF  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designvolthermoneumannconditions:

DESIGN VOL THERMO NEUMANN CONDITIONS
------------------------------------

Volume Neumann

::

   -------------------------------DESIGN VOL THERMO NEUMANN CONDITIONS
   DVOL  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designpointporoneumannconditions:

DESIGN POINT PORO NEUMANN CONDITIONS
------------------------------------

Point Neumann

::

   -------------------------------DESIGN POINT PORO NEUMANN CONDITIONS
   DPOINT  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designlineporoneumannconditions:

DESIGN LINE PORO NEUMANN CONDITIONS
-----------------------------------

Line Neumann

::

   --------------------------------DESIGN LINE PORO NEUMANN CONDITIONS
   DLINE  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designsurfporoneumannconditions:

DESIGN SURF PORO NEUMANN CONDITIONS
-----------------------------------

Surface Neumann

::

   --------------------------------DESIGN SURF PORO NEUMANN CONDITIONS
   DSURF  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designvolporoneumannconditions:

DESIGN VOL PORO NEUMANN CONDITIONS
----------------------------------

Volume Neumann

::

   ---------------------------------DESIGN VOL PORO NEUMANN CONDITIONS
   DVOL  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designpointdirichconditions:

DESIGN POINT DIRICH CONDITIONS
------------------------------

Point Dirichlet

::

   -------------------------------------DESIGN POINT DIRICH CONDITIONS
   DPOINT  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designlinedirichconditions:

DESIGN LINE DIRICH CONDITIONS
-----------------------------

Line Dirichlet

::

   --------------------------------------DESIGN LINE DIRICH CONDITIONS
   DLINE  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designsurfdirichconditions:

DESIGN SURF DIRICH CONDITIONS
-----------------------------

Surface Dirichlet

::

   --------------------------------------DESIGN SURF DIRICH CONDITIONS
   DSURF  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designvoldirichconditions:

DESIGN VOL DIRICH CONDITIONS
----------------------------

Volume Dirichlet

::

   ---------------------------------------DESIGN VOL DIRICH CONDITIONS
   DVOL  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designpointaledirichconditions:

DESIGN POINT ALE DIRICH CONDITIONS
----------------------------------

Point Dirichlet

::

   ---------------------------------DESIGN POINT ALE DIRICH CONDITIONS
   DPOINT  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designlinealedirichconditions:

DESIGN LINE ALE DIRICH CONDITIONS
---------------------------------

Line Dirichlet

::

   ----------------------------------DESIGN LINE ALE DIRICH CONDITIONS
   DLINE  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designsurfaledirichconditions:

DESIGN SURF ALE DIRICH CONDITIONS
---------------------------------

Surface Dirichlet

::

   ----------------------------------DESIGN SURF ALE DIRICH CONDITIONS
   DSURF  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designvolaledirichconditions:

DESIGN VOL ALE DIRICH CONDITIONS
--------------------------------

Volume Dirichlet

::

   -----------------------------------DESIGN VOL ALE DIRICH CONDITIONS
   DVOL  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designpointtransportdirichconditions:

DESIGN POINT TRANSPORT DIRICH CONDITIONS
----------------------------------------

Point Dirichlet

::

   ---------------------------DESIGN POINT TRANSPORT DIRICH CONDITIONS
   DPOINT  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designlinetransportdirichconditions:

DESIGN LINE TRANSPORT DIRICH CONDITIONS
---------------------------------------

Line Dirichlet

::

   ----------------------------DESIGN LINE TRANSPORT DIRICH CONDITIONS
   DLINE  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designsurftransportdirichconditions:

DESIGN SURF TRANSPORT DIRICH CONDITIONS
---------------------------------------

Surface Dirichlet

::

   ----------------------------DESIGN SURF TRANSPORT DIRICH CONDITIONS
   DSURF  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designvoltransportdirichconditions:

DESIGN VOL TRANSPORT DIRICH CONDITIONS
--------------------------------------

Volume Dirichlet

::

   -----------------------------DESIGN VOL TRANSPORT DIRICH CONDITIONS
   DVOL  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designpointthermodirichconditions:

DESIGN POINT THERMO DIRICH CONDITIONS
-------------------------------------

Point Dirichlet

::

   ------------------------------DESIGN POINT THERMO DIRICH CONDITIONS
   DPOINT  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designlinethermodirichconditions:

DESIGN LINE THERMO DIRICH CONDITIONS
------------------------------------

Line Dirichlet

::

   -------------------------------DESIGN LINE THERMO DIRICH CONDITIONS
   DLINE  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designsurfthermodirichconditions:

DESIGN SURF THERMO DIRICH CONDITIONS
------------------------------------

Surface Dirichlet

::

   -------------------------------DESIGN SURF THERMO DIRICH CONDITIONS
   DSURF  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designvolthermodirichconditions:

DESIGN VOL THERMO DIRICH CONDITIONS
-----------------------------------

Volume Dirichlet

::

   --------------------------------DESIGN VOL THERMO DIRICH CONDITIONS
   DVOL  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designpointporodirichconditions:

DESIGN POINT PORO DIRICH CONDITIONS
-----------------------------------

Point Dirichlet

::

   --------------------------------DESIGN POINT PORO DIRICH CONDITIONS
   DPOINT  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designlineporodirichconditions:

DESIGN LINE PORO DIRICH CONDITIONS
----------------------------------

Line Dirichlet

::

   ---------------------------------DESIGN LINE PORO DIRICH CONDITIONS
   DLINE  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designsurfporodirichconditions:

DESIGN SURF PORO DIRICH CONDITIONS
----------------------------------

Surface Dirichlet

::

   ---------------------------------DESIGN SURF PORO DIRICH CONDITIONS
   DSURF  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designvolporodirichconditions:

DESIGN VOL PORO DIRICH CONDITIONS
---------------------------------

Volume Dirichlet

::

   ----------------------------------DESIGN VOL PORO DIRICH CONDITIONS
   DVOL  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designpointnurbslsdirichconditions:

DESIGN POINT NURBS LS DIRICH CONDITIONS
---------------------------------------

Point Dirichlet

::

   ----------------------------DESIGN POINT NURBS LS DIRICH CONDITIONS
   DPOINT  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designlinenurbslsdirichconditions:

DESIGN LINE NURBS LS DIRICH CONDITIONS
--------------------------------------

Line Dirichlet

::

   -----------------------------DESIGN LINE NURBS LS DIRICH CONDITIONS
   DLINE  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designsurfnurbslsdirichconditions:

DESIGN SURF NURBS LS DIRICH CONDITIONS
--------------------------------------

Surface Dirichlet

::

   -----------------------------DESIGN SURF NURBS LS DIRICH CONDITIONS
   DSURF  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designvolnurbslsdirichconditions:

DESIGN VOL NURBS LS DIRICH CONDITIONS
-------------------------------------

Volume Dirichlet

::

   ------------------------------DESIGN VOL NURBS LS DIRICH CONDITIONS
   DVOL  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designpointcouplingconditions:

DESIGN POINT COUPLING CONDITIONS
--------------------------------

Point Coupling

::

   -----------------------------------DESIGN POINT COUPLING CONDITIONS
   DPOINT  0
   E <setnumber> - NUMDOF 0 ONOFF 0 

.. _designpointthermocouplingconditions:

DESIGN POINT THERMO COUPLING CONDITIONS
---------------------------------------

Point Coupling

::

   ----------------------------DESIGN POINT THERMO COUPLING CONDITIONS
   DPOINT  0
   E <setnumber> - NUMDOF 0 ONOFF 0 

.. _designpointinitialfieldconditions:

DESIGN POINT INITIAL FIELD CONDITIONS
-------------------------------------

Point Initfield

::

   ------------------------------DESIGN POINT INITIAL FIELD CONDITIONS
   DPOINT  0
   E <setnumber> - <Field> <int vec:funct> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Field
     - Undefined
     - Undefined, Velocity, Pressure, Temperature, |break| 
       ScaTra, Porosity, PoroMultiFluid, Artery

.. _designlineinitialfieldconditions:

DESIGN LINE INITIAL FIELD CONDITIONS
------------------------------------

Line Initfield

::

   -------------------------------DESIGN LINE INITIAL FIELD CONDITIONS
   DLINE  0
   E <setnumber> - <Field> <int vec:funct> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Field
     - Undefined
     - Undefined, Velocity, Pressure, Temperature, |break| 
       ScaTra, Porosity, PoroMultiFluid, Artery

.. _designsurfinitialfieldconditions:

DESIGN SURF INITIAL FIELD CONDITIONS
------------------------------------

Surface Initfield

::

   -------------------------------DESIGN SURF INITIAL FIELD CONDITIONS
   DSURF  0
   E <setnumber> - <Field> <int vec:funct> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Field
     - Undefined
     - Undefined, Velocity, Pressure, Temperature, |break| 
       ScaTra, Porosity, PoroMultiFluid, Artery

.. _designvolinitialfieldconditions:

DESIGN VOL INITIAL FIELD CONDITIONS
-----------------------------------

Volume Initfield

::

   --------------------------------DESIGN VOL INITIAL FIELD CONDITIONS
   DVOL  0
   E <setnumber> - <Field> <int vec:funct> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Field
     - Undefined
     - Undefined, Velocity, Pressure, Temperature, |break| 
       ScaTra, Porosity, PoroMultiFluid, Artery

.. _designpointthermoinitialfieldconditions:

DESIGN POINT THERMO INITIAL FIELD CONDITIONS
--------------------------------------------

Set the initial temperature field if the thermo field is solved using a ScaTra discretization (e.g. STI, SSTI) on points

::

   -----------------------DESIGN POINT THERMO INITIAL FIELD CONDITIONS
   DPOINT  0
   E <setnumber> - <Field> <int vec:funct> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Field
     - Undefined
     - Undefined, ScaTra

.. _designlinethermoinitialfieldconditions:

DESIGN LINE THERMO INITIAL FIELD CONDITIONS
-------------------------------------------

Set the initial temperature field if the thermo field is solved using a ScaTra discretization (e.g. STI, SSTI) on lines

::

   ------------------------DESIGN LINE THERMO INITIAL FIELD CONDITIONS
   DLINE  0
   E <setnumber> - <Field> <int vec:funct> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Field
     - Undefined
     - Undefined, ScaTra

.. _designsurfthermoinitialfieldconditions:

DESIGN SURF THERMO INITIAL FIELD CONDITIONS
-------------------------------------------

Set the initial temperature field if the thermo field is solved using a ScaTra discretization (e.g. STI, SSTI) on surfaces

::

   ------------------------DESIGN SURF THERMO INITIAL FIELD CONDITIONS
   DSURF  0
   E <setnumber> - <Field> <int vec:funct> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Field
     - Undefined
     - Undefined, ScaTra

.. _designvolthermoinitialfieldconditions:

DESIGN VOL THERMO INITIAL FIELD CONDITIONS
------------------------------------------

Set the initial temperature field if the thermo field is solved using a ScaTra discretization (e.g. STI, SSTI) on volumes

::

   -------------------------DESIGN VOL THERMO INITIAL FIELD CONDITIONS
   DVOL  0
   E <setnumber> - <Field> <int vec:funct> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Field
     - Undefined
     - Undefined, ScaTra

.. _designdomainintegralsurfconditions:

DESIGN DOMAIN INTEGRAL SURF CONDITIONS
--------------------------------------

compute cumulative surface areas of 2D domain elements

::

   -----------------------------DESIGN DOMAIN INTEGRAL SURF CONDITIONS
   DSURF  0
   E <setnumber> - ID 0

.. _designdomainintegralvolconditions:

DESIGN DOMAIN INTEGRAL VOL CONDITIONS
-------------------------------------

compute cumulative volumes of 3D domain elements

::

   ------------------------------DESIGN DOMAIN INTEGRAL VOL CONDITIONS
   DVOL  0
   E <setnumber> - ID 0

.. _designboundaryintegralsurfconditions:

DESIGN BOUNDARY INTEGRAL SURF CONDITIONS
----------------------------------------

compute cumulative surface areas of 2D boundary elements

::

   ---------------------------DESIGN BOUNDARY INTEGRAL SURF CONDITIONS
   DSURF  0
   E <setnumber> - ID 0

.. _designlinealewearconditions2d:

DESIGN LINE ALE WEAR CONDITIONS 2D
----------------------------------

Line Ale Wear

::

   ---------------------------------DESIGN LINE ALE WEAR CONDITIONS 2D
   DLINE  0
   E <setnumber> -

.. _designsurfacewearconditions3d:

DESIGN SURFACE WEAR CONDITIONS 3D
---------------------------------

Surface Ale Wear

::

   ----------------------------------DESIGN SURFACE WEAR CONDITIONS 3D
   DSURF  0
   E <setnumber> -

.. _designpointlocsysconditions:

DESIGN POINT LOCSYS CONDITIONS
------------------------------

Point local coordinate system

::

   -------------------------------------DESIGN POINT LOCSYS CONDITIONS
   DPOINT  0
   E <setnumber> - ROTANGLE <real vec:rotangle>  FUNCT <int vec:funct>  \ 
       USEUPDATEDNODEPOS <int vec:useupdatednodepos> 

.. _designlinelocsysconditions:

DESIGN LINE LOCSYS CONDITIONS
-----------------------------

Line local coordinate system

::

   --------------------------------------DESIGN LINE LOCSYS CONDITIONS
   DLINE  0
   E <setnumber> - ROTANGLE <real vec:rotangle>  FUNCT <int vec:funct>  \ 
       USEUPDATEDNODEPOS <int vec:useupdatednodepos>  USECONSISTENTNODENORMAL <int vec:useconsistentnodenormal> 

.. _designsurflocsysconditions:

DESIGN SURF LOCSYS CONDITIONS
-----------------------------

Surface local coordinate system

::

   --------------------------------------DESIGN SURF LOCSYS CONDITIONS
   DSURF  0
   E <setnumber> - ROTANGLE <real vec:rotangle>  FUNCT <int vec:funct>  \ 
       USEUPDATEDNODEPOS <int vec:useupdatednodepos>  USECONSISTENTNODENORMAL <int vec:useconsistentnodenormal> 

.. _designvollocsysconditions:

DESIGN VOL LOCSYS CONDITIONS
----------------------------

Volume local coordinate system

::

   ---------------------------------------DESIGN VOL LOCSYS CONDITIONS
   DVOL  0
   E <setnumber> - ROTANGLE <real vec:rotangle>  FUNCT <int vec:funct>  \ 
       USEUPDATEDNODEPOS <int vec:useupdatednodepos> 

.. _designpointalelocsysconditions:

DESIGN POINT ALE LOCSYS CONDITIONS
----------------------------------

Point local coordinate system

::

   ---------------------------------DESIGN POINT ALE LOCSYS CONDITIONS
   DPOINT  0
   E <setnumber> - ROTANGLE <real vec:rotangle>  FUNCT <int vec:funct>  \ 
       USEUPDATEDNODEPOS <int vec:useupdatednodepos> 

.. _designlinealelocsysconditions:

DESIGN LINE ALE LOCSYS CONDITIONS
---------------------------------

Line local coordinate system

::

   ----------------------------------DESIGN LINE ALE LOCSYS CONDITIONS
   DLINE  0
   E <setnumber> - ROTANGLE <real vec:rotangle>  FUNCT <int vec:funct>  \ 
       USEUPDATEDNODEPOS <int vec:useupdatednodepos>  USECONSISTENTNODENORMAL <int vec:useconsistentnodenormal> 

.. _designsurfalelocsysconditions:

DESIGN SURF ALE LOCSYS CONDITIONS
---------------------------------

Surface local coordinate system

::

   ----------------------------------DESIGN SURF ALE LOCSYS CONDITIONS
   DSURF  0
   E <setnumber> - ROTANGLE <real vec:rotangle>  FUNCT <int vec:funct>  \ 
       USEUPDATEDNODEPOS <int vec:useupdatednodepos>  USECONSISTENTNODENORMAL <int vec:useconsistentnodenormal> 

.. _designvolalelocsysconditions:

DESIGN VOL ALE LOCSYS CONDITIONS
--------------------------------

Volume local coordinate system

::

   -----------------------------------DESIGN VOL ALE LOCSYS CONDITIONS
   DVOL  0
   E <setnumber> - ROTANGLE <real vec:rotangle>  FUNCT <int vec:funct>  \ 
       USEUPDATEDNODEPOS <int vec:useupdatednodepos> 

.. _designlineperiodicboundaryconditions:

DESIGN LINE PERIODIC BOUNDARY CONDITIONS
----------------------------------------

Line Periodic

::

   ---------------------------DESIGN LINE PERIODIC BOUNDARY CONDITIONS
   DLINE  0
   E <setnumber> - 0 <Is slave periodic boundary condition> PLANE <degrees of freedom for the pbc plane> \ 
       LAYER 0 ANGLE 0.0 ABSTREETOL 0.0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Is slave periodic boundary condition
     - Master
     - Master, Slave
   * - degrees of freedom for the pbc plane
     - xy
     - xy, yx, yz, zy, xz, zx, xyz

.. _designsurfperiodicboundaryconditions:

DESIGN SURF PERIODIC BOUNDARY CONDITIONS
----------------------------------------

Surface Periodic

::

   ---------------------------DESIGN SURF PERIODIC BOUNDARY CONDITIONS
   DSURF  0
   E <setnumber> - 0 <Is slave periodic boundary condition> PLANE <degrees of freedom for the pbc plane> \ 
       LAYER 0 ANGLE 0.0 ABSTREETOL 0.0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Is slave periodic boundary condition
     - Master
     - Master, Slave
   * - degrees of freedom for the pbc plane
     - xy
     - xy, yx, yz, zy, xz, zx, xyz

.. _designlineweakdirichletconditions:

DESIGN LINE WEAK DIRICHLET CONDITIONS
-------------------------------------

LineWeakDirichlet

::

   ------------------------------DESIGN LINE WEAK DIRICHLET CONDITIONS
   DLINE  0
   E <setnumber> - <Choice of gamma parameter> <Directions to apply weak dbc> \ 
       <Definition of penalty parameter> 0.0 <Linearisation> <real vec:val>  \ 
       <int vec:funct> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Choice of gamma parameter
     - adjoint-consistent
     - adjoint-consistent, diffusive-optimal
   * - Directions to apply weak dbc
     - all_directions
     - all_directions, only_in_normal_direction
   * - Definition of penalty parameter
     - constant
     - constant, Spalding
   * - Linearisation
     - lin_all
     - lin_all, no_lin_conv_inflow

.. _designsurfaceweakdirichletconditions:

DESIGN SURFACE WEAK DIRICHLET CONDITIONS
----------------------------------------

SurfaceWeakDirichlet

::

   ---------------------------DESIGN SURFACE WEAK DIRICHLET CONDITIONS
   DSURF  0
   E <setnumber> - <Choice of gamma parameter> <Directions to apply weak dbc> \ 
       <Definition of penalty parameter> 0.0 <Linearisation> <real vec:val>  \ 
       <int vec:funct> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Choice of gamma parameter
     - adjoint-consistent
     - adjoint-consistent, diffusive-optimal
   * - Directions to apply weak dbc
     - all_directions
     - all_directions, only_in_normal_direction
   * - Definition of penalty parameter
     - constant
     - constant, Spalding
   * - Linearisation
     - lin_all
     - lin_all, no_lin_conv_inflow

.. _designpatchrecoveryboundarylineconditions:

DESIGN PATCH RECOVERY BOUNDARY LINE CONDITIONS
----------------------------------------------

Boundary for SPR

::

   ---------------------DESIGN PATCH RECOVERY BOUNDARY LINE CONDITIONS
   DLINE  0
   E <setnumber> -

.. _designpatchrecoveryboundarysurfconditions:

DESIGN PATCH RECOVERY BOUNDARY SURF CONDITIONS
----------------------------------------------

Boundary for SPR

::

   ---------------------DESIGN PATCH RECOVERY BOUNDARY SURF CONDITIONS
   DSURF  0
   E <setnumber> -

.. _designsurfacevolumeconstraint3d:

DESIGN SURFACE VOLUME CONSTRAINT 3D
-----------------------------------

Surface Volume Constraint

::

   --------------------------------DESIGN SURFACE VOLUME CONSTRAINT 3D
   DSURF  0
   E <setnumber> - 0 none 0.0 <projection>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - projection
     - none
     - none, xy, yz, xz

.. _designsurfacevolumeconstraint3dpen:

DESIGN SURFACE VOLUME CONSTRAINT 3D PEN
---------------------------------------

Surface Volume Constraint Penalty

::

   ----------------------------DESIGN SURFACE VOLUME CONSTRAINT 3D PEN
   DSURF  0
   E <setnumber> - 0 none 0.0 0.0 0.0 <projection>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - projection
     - none
     - none, xy, yz, xz

.. _designsurfaceareaconstraint3d:

DESIGN SURFACE AREA CONSTRAINT 3D
---------------------------------

Surface Area Constraint

::

   ----------------------------------DESIGN SURFACE AREA CONSTRAINT 3D
   DSURF  0
   E <setnumber> - 0 none 0.0

.. _designsurfaceareaconstraint3dpen:

DESIGN SURFACE AREA CONSTRAINT 3D PEN
-------------------------------------

Surface Area Constraint Penalty

::

   ------------------------------DESIGN SURFACE AREA CONSTRAINT 3D PEN
   DSURF  0
   E <setnumber> - 0 none 0.0 0.0 0.0 <projection>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - projection
     - none
     - none, xy, yz, xz

.. _designsurfacevolumemonitor3d:

DESIGN SURFACE VOLUME MONITOR 3D
--------------------------------

Surface Volume Monitor

::

   -----------------------------------DESIGN SURFACE VOLUME MONITOR 3D
   DSURF  0
   E <setnumber> - 0

.. _designsurfaceareamonitor3d:

DESIGN SURFACE AREA MONITOR 3D
------------------------------

Surface Area Monitor

::

   -------------------------------------DESIGN SURFACE AREA MONITOR 3D
   DSURF  0
   E <setnumber> - 0 <projection>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - projection
     - none
     - none, xy, yz, xz

.. _designlineareaconstraint2d:

DESIGN LINE AREA CONSTRAINT 2D
------------------------------

Line Area Constraint

::

   -------------------------------------DESIGN LINE AREA CONSTRAINT 2D
   DLINE  0
   E <setnumber> - 0 none 0.0

.. _designlineareamonitor2d:

DESIGN LINE AREA MONITOR 2D
---------------------------

Line Area Monitor

::

   ----------------------------------------DESIGN LINE AREA MONITOR 2D
   DLINE  0
   E <setnumber> - 0

.. _designsurfacemultipntconstraint3d:

DESIGN SURFACE MULTIPNT CONSTRAINT 3D
-------------------------------------

Node on Plane Constraint

::

   ------------------------------DESIGN SURFACE MULTIPNT CONSTRAINT 3D
   DSURF  0
   E <setnumber> - 0 0.0 none 0.0 <int vec:planeNodes>  <control>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - control
     - rel
     - rel, abs

.. _designsurfacenormaldirmultipntconstraint3d:

DESIGN SURFACE NORMALDIR MULTIPNT CONSTRAINT 3D
-----------------------------------------------

Node on Plane Constraint

::

   --------------------DESIGN SURFACE NORMALDIR MULTIPNT CONSTRAINT 3D
   DSURF  0
   E <setnumber> - 0 0.0 none 0.0 0 <real vec:direction>  <value> \ 
       <control>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - value
     - disp
     - disp, x
   * - control
     - rel
     - rel, abs

.. _designsurfacenormaldirmultipntconstraint3dpen:

DESIGN SURFACE NORMALDIR MULTIPNT CONSTRAINT 3D PEN
---------------------------------------------------

Node on Plane Constraint Penalty

::

   ----------------DESIGN SURFACE NORMALDIR MULTIPNT CONSTRAINT 3D PEN
   DSURF  0
   E <setnumber> - 0 0.0 none 0.0 0.0 0 <real vec:direction>  <value> \ 
       <control>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - value
     - disp
     - disp, x
   * - control
     - rel
     - rel, abs

.. _designlinemultipntconstraint2d:

DESIGN LINE MULTIPNT CONSTRAINT 2D
----------------------------------

Node on Line Constraint

::

   ---------------------------------DESIGN LINE MULTIPNT CONSTRAINT 2D
   DLINE  0
   E <setnumber> - 0 0.0 none 0 0 0 <control value> 0.0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - control value
     - dist
     - dist, angle

.. _designsurfmodeforkrylovspaceprojection:

DESIGN SURF MODE FOR KRYLOV SPACE PROJECTION
--------------------------------------------

Surface mode for Krylov space projection

::

   -----------------------DESIGN SURF MODE FOR KRYLOV SPACE PROJECTION
   DSURF  0
   E <setnumber> - <discretization> NUMMODES 0 ONOFF 0  <weight vector definition>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - discretization
     - fluid
     - fluid, scatra, solid
   * - weight vector definition
     - integration
     - integration, pointvalues

.. _designvolmodeforkrylovspaceprojection:

DESIGN VOL MODE FOR KRYLOV SPACE PROJECTION
-------------------------------------------

Volume mode for Krylov space projection

::

   ------------------------DESIGN VOL MODE FOR KRYLOV SPACE PROJECTION
   DVOL  0
   E <setnumber> - <discretization> NUMMODES 0 ONOFF 0  <weight vector definition>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - discretization
     - fluid
     - fluid, scatra, solid
   * - weight vector definition
     - integration
     - integration, pointvalues

.. _designlinemortarcontactconditions2d:

DESIGN LINE MORTAR CONTACT CONDITIONS 2D
----------------------------------------

Line Contact Coupling

::

   ---------------------------DESIGN LINE MORTAR CONTACT CONDITIONS 2D
   DLINE  0
   E <setnumber> - 0 <Side> <Initialization> FrCoeffOrBound 0.0 AdhesionBound 0.0 \ 
       <Application> <dbc_handling> TwoHalfPass 0.0 RefConfCheckNonSmoothSelfContactSurface 0.0 \ 
       ConstitutiveLawID none

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Side
     - Master
     - Master, Slave, Selfcontact
   * - Initialization
     - Inactive
     - Inactive, Active
   * - Application
     - Solidcontact
     - Solidcontact, Beamtosolidcontact, |break| 
       Beamtosolidmeshtying
   * - dbc_handling
     - DoNothing
     - DoNothing, RemoveDBCSlaveNodes

.. _designsurfmortarcontactconditions3d:

DESIGN SURF MORTAR CONTACT CONDITIONS 3D
----------------------------------------

Surface Contact Coupling

::

   ---------------------------DESIGN SURF MORTAR CONTACT CONDITIONS 3D
   DSURF  0
   E <setnumber> - 0 <Side> <Initialization> FrCoeffOrBound 0.0 AdhesionBound 0.0 \ 
       <Application> <dbc_handling> TwoHalfPass 0.0 RefConfCheckNonSmoothSelfContactSurface 0.0 \ 
       ConstitutiveLawID none

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Side
     - Master
     - Master, Slave, Selfcontact
   * - Initialization
     - Inactive
     - Inactive, Active
   * - Application
     - Solidcontact
     - Solidcontact, Beamtosolidcontact, |break| 
       Beamtosolidmeshtying
   * - dbc_handling
     - DoNothing
     - DoNothing, RemoveDBCSlaveNodes

.. _designlinemortarcouplingconditions2d:

DESIGN LINE MORTAR COUPLING CONDITIONS 2D
-----------------------------------------

Line Mortar Coupling

::

   --------------------------DESIGN LINE MORTAR COUPLING CONDITIONS 2D
   DLINE  0
   E <setnumber> - 0 <Side> <Initialization>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Side
     - Master
     - Master, Slave
   * - Initialization
     - Inactive
     - Inactive, Active

.. _designsurfmortarcouplingconditions3d:

DESIGN SURF MORTAR COUPLING CONDITIONS 3D
-----------------------------------------

Surface Mortar Coupling

::

   --------------------------DESIGN SURF MORTAR COUPLING CONDITIONS 3D
   DSURF  0
   E <setnumber> - 0 <Side> <Initialization>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Side
     - Master
     - Master, Slave
   * - Initialization
     - Inactive
     - Inactive, Active

.. _designlinemortarsymmetryconditions3d:

DESIGN LINE MORTAR SYMMETRY CONDITIONS 3D
-----------------------------------------

Symmetry plane normal for 3D contact

::

   --------------------------DESIGN LINE MORTAR SYMMETRY CONDITIONS 3D
   DLINE  0
   E <setnumber> - ONOFF <int vec:onoff> 

.. _designpointmortarsymmetryconditions2d/3d:

DESIGN POINT MORTAR SYMMETRY CONDITIONS 2D/3D
---------------------------------------------

Symmetry plane normal for 2D/3D contact

::

   ----------------------DESIGN POINT MORTAR SYMMETRY CONDITIONS 2D/3D
   DPOINT  0
   E <setnumber> - ONOFF <int vec:onoff> 

.. _designlinemortaredgeconditions3d:

DESIGN LINE MORTAR EDGE CONDITIONS 3D
-------------------------------------

Geometrical edge for 3D contact

::

   ------------------------------DESIGN LINE MORTAR EDGE CONDITIONS 3D
   DLINE  0
   E <setnumber> -

.. _designpointmortarcornerconditions2d/3d:

DESIGN POINT MORTAR CORNER CONDITIONS 2D/3D
-------------------------------------------

Geometrical corner for 2D/3D contact

::

   ------------------------DESIGN POINT MORTAR CORNER CONDITIONS 2D/3D
   DPOINT  0
   E <setnumber> -

.. _designlinemortarmulti-couplingconditions2d:

DESIGN LINE MORTAR MULTI-COUPLING CONDITIONS 2D
-----------------------------------------------

Line Mortar Multi-Coupling

::

   --------------------DESIGN LINE MORTAR MULTI-COUPLING CONDITIONS 2D
   DLINE  0
   E <setnumber> - 0 <Side> <Initialization>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Side
     - Master
     - Master, Slave
   * - Initialization
     - Inactive
     - Inactive, Active

.. _designsurfmortarmulti-couplingconditions3d:

DESIGN SURF MORTAR MULTI-COUPLING CONDITIONS 3D
-----------------------------------------------

Surface Mortar Multi-Coupling

::

   --------------------DESIGN SURF MORTAR MULTI-COUPLING CONDITIONS 3D
   DSURF  0
   E <setnumber> - 0 <Side> <Initialization>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Side
     - Master
     - Master, Slave
   * - Initialization
     - Inactive
     - Inactive, Active

.. _designs2imeshtyinglineconditions:

DESIGN S2I MESHTYING LINE CONDITIONS
------------------------------------

Scatra-scatra line interface mesh tying

::

   -------------------------------DESIGN S2I MESHTYING LINE CONDITIONS
   DLINE  0
   E <setnumber> - 0 <interface side> S2I_KINETICS_ID 0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - interface side
     - Undefined
     - Undefined, Slave, Master

.. _designs2imeshtyingsurfconditions:

DESIGN S2I MESHTYING SURF CONDITIONS
------------------------------------

Scatra-scatra surface interface mesh tying

::

   -------------------------------DESIGN S2I MESHTYING SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0 <interface side> S2I_KINETICS_ID 0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - interface side
     - Undefined
     - Undefined, Slave, Master

.. _designs2inoevaluationlineconditions:

DESIGN S2I NO EVALUATION LINE CONDITIONS
----------------------------------------

Scatra-scatra interface no evaluation line condition. This condition can be used to deactivate the evaluation of the corresponding `S2IKinetics` condition. Another usage is in coupled algorithms, where a specific `S2IKinetics` condition should not be evaluated within the scalar transport framework, as it already evaluated elsewhere. One example is the SSI contact.

::

   ---------------------------DESIGN S2I NO EVALUATION LINE CONDITIONS
   DLINE  0
   E <setnumber> - 0 <interface side> S2I_KINETICS_ID 0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - interface side
     - Undefined
     - Undefined, Slave, Master

.. _designs2inoevaluationsurfconditions:

DESIGN S2I NO EVALUATION SURF CONDITIONS
----------------------------------------

Scatra-scatra interface no evaluation surface condition. This condition can be used to deactivate the evaluation of the corresponding `S2IKinetics` condition. Another usage is in coupled algorithms, where a specific `S2IKinetics` condition should not be evaluated within the scalar transport framework, as it already evaluated elsewhere. One example is the SSI contact.

::

   ---------------------------DESIGN S2I NO EVALUATION SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0 <interface side> S2I_KINETICS_ID 0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - interface side
     - Undefined
     - Undefined, Slave, Master

.. _designs2ikineticspointconditions:

DESIGN S2I KINETICS POINT CONDITIONS
------------------------------------

Scatra-scatra line interface kinetics

::

   -------------------------------DESIGN S2I KINETICS POINT CONDITIONS
   DPOINT  0
   E <setnumber> - 0 <interface side> [further parameters]

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - interface side
     - Undefined
     - Undefined, Slave, Master

The following parameter sets are possible for `<interface side>`:

::

   Undefined 
   Slave KINETIC_MODEL <kinetic model> [further parameters] 
   Master 

.. _designs2ikineticslineconditions:

DESIGN S2I KINETICS LINE CONDITIONS
-----------------------------------

Scatra-scatra line interface kinetics

::

   --------------------------------DESIGN S2I KINETICS LINE CONDITIONS
   DLINE  0
   E <setnumber> - 0 <interface side> [further parameters]

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - interface side
     - Undefined
     - Undefined, Slave, Master

The following parameter sets are possible for `<interface side>`:

::

   Undefined 
   Slave KINETIC_MODEL <kinetic model> [further parameters] 
   Master 

.. _designs2ikineticssurfconditions:

DESIGN S2I KINETICS SURF CONDITIONS
-----------------------------------

Scatra-scatra surface interface kinetics

::

   --------------------------------DESIGN S2I KINETICS SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0 <interface side> [further parameters]

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - interface side
     - Undefined
     - Undefined, Slave, Master

The following parameter sets are possible for `<interface side>`:

::

   Undefined 
   Slave KINETIC_MODEL <kinetic model> [further parameters] 
   Master 

.. _designscatramulti-scalecouplingpointconditions:

DESIGN SCATRA MULTI-SCALE COUPLING POINT CONDITIONS
---------------------------------------------------

Scalar transport multi-scale coupling condition

::

   ----------------DESIGN SCATRA MULTI-SCALE COUPLING POINT CONDITIONS
   DPOINT  0
   E <setnumber> - KINETIC_MODEL <kinetic model> [further parameters]

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - kinetic model
     - ConstantPermeability
     - ConstantPermeability, Butler-Volmer, |break| 
       Butler-Volmer_Linearized, Butler-VolmerReduced, |break| 
       Butler-VolmerReduced_Linearized, |break| 
       Butler-Volmer-Peltier, |break| 
       Butler-VolmerReduced_Capacitance, |break| 
       Butler-Volmer_Resistance, |break| 
       Butler-VolmerReduced_Resistance, |break| 
       Butler-VolmerReduced_ThermoResistance, |break| 
       ConstantInterfaceResistance, NoInterfaceFlux

The following parameter sets are possible for `<kinetic model>`:

::

   ConstantPermeability NUMSCAL 0 PERMEABILITIES  IS_PSEUDO_CONTACT 0 
   Butler-Volmer NUMSCAL 0 STOICHIOMETRIES  E- 0 K_R 0.0 ALPHA_A 0.0 ALPHA_C 0.0 IS_PSEUDO_CONTACT 0 
   Butler-Volmer_Linearized NUMSCAL 0 STOICHIOMETRIES  E- 0 K_R 0.0 ALPHA_A 0.0 ALPHA_C 0.0 IS_PSEUDO_CONTACT 0 
   Butler-Volmer-Peltier NUMSCAL 0 STOICHIOMETRIES  E- 0 K_R 0.0 ALPHA_A 0.0 ALPHA_C 0.0 IS_PSEUDO_CONTACT 0 PELTIER 0.0 
   Butler-VolmerReduced NUMSCAL 0 STOICHIOMETRIES  E- 0 K_R 0.0 ALPHA_A 0.0 ALPHA_C 0.0 IS_PSEUDO_CONTACT 0 
   Butler-VolmerReduced_Capacitance NUMSCAL 0 STOICHIOMETRIES  E- 0 K_R 0.0 CAPACITANCE 0.0 ALPHA_A 0.0 ALPHA_C 0.0 IS_PSEUDO_CONTACT 0 
   Butler-VolmerReduced_Linearized NUMSCAL 0 STOICHIOMETRIES  E- 0 K_R 0.0 ALPHA_A 0.0 ALPHA_C 0.0 IS_PSEUDO_CONTACT 0 
   Butler-Volmer_Resistance NUMSCAL 0 STOICHIOMETRIES  E- 0 K_R 0.0 ALPHA_A 0.0 ALPHA_C 0.0 IS_PSEUDO_CONTACT 0 RESISTANCE 0.0 CONVTOL_IMPLBUTLERVOLMER 0.0 ITEMAX_IMPLBUTLERVOLMER 0 
   Butler-VolmerReduced_Resistance NUMSCAL 0 STOICHIOMETRIES  E- 0 K_R 0.0 ALPHA_A 0.0 ALPHA_C 0.0 IS_PSEUDO_CONTACT 0 RESISTANCE 0.0 CONVTOL_IMPLBUTLERVOLMER 0.0 ITEMAX_IMPLBUTLERVOLMER 0 
   Butler-VolmerReduced_ThermoResistance NUMSCAL 0 STOICHIOMETRIES  E- 0 K_R 0.0 ALPHA_A 0.0 ALPHA_C 0.0 IS_PSEUDO_CONTACT 0 THERMOPERM 0.0 MOLAR_HEAT_CAPACITY 0.0 
   ConstantInterfaceResistance ONOFF <int vec:onoff>  RESISTANCE 0.0 E- 0 IS_PSEUDO_CONTACT 0 
   NoInterfaceFlux 

.. _designs2icouplinggrowthlineconditions:

DESIGN S2I COUPLING GROWTH LINE CONDITIONS
------------------------------------------

Scatra-scatra line interface coupling involving interface layer growth

::

   -------------------------DESIGN S2I COUPLING GROWTH LINE CONDITIONS
   DLINE  0
   E <setnumber> - 0 KINETIC_MODEL <kinetic model> [further parameters]

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - kinetic model
     - Butler-Volmer
     - Butler-Volmer

The following parameter sets are possible for `<kinetic model>`:

::

   Butler-Volmer NUMSCAL 0 STOICHIOMETRIES  E- 0 K_R 0.0 ALPHA_A 0.0 ALPHA_C 0.0 MOLMASS 0.0 DENSITY 0.0 CONDUCTIVITY 0.0 REGTYPE <regularization type> REGPAR 0.0 INITTHICKNESS 0.0 

.. _designs2icouplinggrowthsurfconditions:

DESIGN S2I COUPLING GROWTH SURF CONDITIONS
------------------------------------------

Scatra-scatra surface interface coupling involving interface layer growth

::

   -------------------------DESIGN S2I COUPLING GROWTH SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0 KINETIC_MODEL <kinetic model> [further parameters]

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - kinetic model
     - Butler-Volmer
     - Butler-Volmer

The following parameter sets are possible for `<kinetic model>`:

::

   Butler-Volmer NUMSCAL 0 STOICHIOMETRIES  E- 0 K_R 0.0 ALPHA_A 0.0 ALPHA_C 0.0 MOLMASS 0.0 DENSITY 0.0 CONDUCTIVITY 0.0 REGTYPE <regularization type> REGPAR 0.0 INITTHICKNESS 0.0 

.. _designs2isclcouplingsurfconditions:

DESIGN S2I SCL COUPLING SURF CONDITIONS
---------------------------------------

Scatra-scatra surface with SCL micro-macro coupling between

::

   ----------------------------DESIGN S2I SCL COUPLING SURF CONDITIONS
   DSURF  0
   E <setnumber> - <interface side>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - interface side
     - Undefined
     - Undefined, Slave, Master

.. _scatrafluxcalclineconditions:

SCATRA FLUX CALC LINE CONDITIONS
--------------------------------

Scalar Transport Boundary Flux Calculation

::

   -----------------------------------SCATRA FLUX CALC LINE CONDITIONS
   DLINE  0
   E <setnumber> -

.. _scatrafluxcalcsurfconditions:

SCATRA FLUX CALC SURF CONDITIONS
--------------------------------

Scalar Transport Boundary Flux Calculation

::

   -----------------------------------SCATRA FLUX CALC SURF CONDITIONS
   DSURF  0
   E <setnumber> -

.. _designtotalandmeanscalarlineconditions:

DESIGN TOTAL AND MEAN SCALAR LINE CONDITIONS
--------------------------------------------

calculation of total and mean values of transported scalars

::

   -----------------------DESIGN TOTAL AND MEAN SCALAR LINE CONDITIONS
   DLINE  0
   E <setnumber> - ID 0

.. _designtotalandmeanscalarsurfconditions:

DESIGN TOTAL AND MEAN SCALAR SURF CONDITIONS
--------------------------------------------

calculation of total and mean values of transported scalars

::

   -----------------------DESIGN TOTAL AND MEAN SCALAR SURF CONDITIONS
   DSURF  0
   E <setnumber> - ID 0

.. _designtotalandmeanscalarvolconditions:

DESIGN TOTAL AND MEAN SCALAR VOL CONDITIONS
-------------------------------------------

calculation of total and mean values of transported scalars

::

   ------------------------DESIGN TOTAL AND MEAN SCALAR VOL CONDITIONS
   DVOL  0
   E <setnumber> - ID 0

.. _designscatrarelativeerrorlineconditions:

DESIGN SCATRA RELATIVE ERROR LINE CONDITIONS
--------------------------------------------

calculation of relative error with reference to analytical solution

::

   -----------------------DESIGN SCATRA RELATIVE ERROR LINE CONDITIONS
   DLINE  0
   E <setnumber> - ID 0 Function 0

.. _designscatrarelativeerrorsurfconditions:

DESIGN SCATRA RELATIVE ERROR SURF CONDITIONS
--------------------------------------------

calculation of relative error with reference to analytical solution

::

   -----------------------DESIGN SCATRA RELATIVE ERROR SURF CONDITIONS
   DSURF  0
   E <setnumber> - ID 0 Function 0

.. _designscatrarelativeerrorvolconditions:

DESIGN SCATRA RELATIVE ERROR VOL CONDITIONS
-------------------------------------------

calculation of relative error with reference to analytical solution

::

   ------------------------DESIGN SCATRA RELATIVE ERROR VOL CONDITIONS
   DVOL  0
   E <setnumber> - ID 0 Function 0

.. _designscatracouplingsurfconditions:

DESIGN SCATRA COUPLING SURF CONDITIONS
--------------------------------------

ScaTra Coupling

::

   -----------------------------DESIGN SCATRA COUPLING SURF CONDITIONS
   DSURF  0
   E <setnumber> - NUMSCAL 0 ONOFF 0 0  COUPID 0 PERMCOEF 0.0 CONDUCT 0.0 \ 
       FILTR 0.0 WSSONOFF 0 WSSCOEFFS <real vec:wss coeffs> 

.. _designtransportrobinlineconditions:

DESIGN TRANSPORT ROBIN LINE CONDITIONS
--------------------------------------

Scalar Transport Robin Boundary Condition

::

   -----------------------------DESIGN TRANSPORT ROBIN LINE CONDITIONS
   DLINE  0
   E <setnumber> - NUMSCAL 0 ONOFF 0 0  PREFACTOR 0.0 REFVALUE 0.0

.. _designtransportrobinsurfconditions:

DESIGN TRANSPORT ROBIN SURF CONDITIONS
--------------------------------------

Scalar Transport Robin Boundary Condition

::

   -----------------------------DESIGN TRANSPORT ROBIN SURF CONDITIONS
   DSURF  0
   E <setnumber> - NUMSCAL 0 ONOFF 0 0  PREFACTOR 0.0 REFVALUE 0.0

.. _transportneumanninflowlineconditions:

TRANSPORT NEUMANN INFLOW LINE CONDITIONS
----------------------------------------

Line Transport Neumann Inflow

::

   ---------------------------TRANSPORT NEUMANN INFLOW LINE CONDITIONS
   DLINE  0
   E <setnumber> -

.. _transportneumanninflowsurfconditions:

TRANSPORT NEUMANN INFLOW SURF CONDITIONS
----------------------------------------

Surface Transport Neumann Inflow

::

   ---------------------------TRANSPORT NEUMANN INFLOW SURF CONDITIONS
   DSURF  0
   E <setnumber> -

.. _transportthermoconvectionlineconditions:

TRANSPORT THERMO CONVECTION LINE CONDITIONS
-------------------------------------------

Line Transport Thermo Convections

::

   ------------------------TRANSPORT THERMO CONVECTION LINE CONDITIONS
   DLINE  0
   E <setnumber> - <temperature state> coeff 0.0 surtemp 0.0 surtempfunct none \ 
       funct none

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - temperature state
     - Tempnp
     - Tempnp, Tempn

.. _transportthermoconvectionsurfconditions:

TRANSPORT THERMO CONVECTION SURF CONDITIONS
-------------------------------------------

Surface Transport Thermo Convections

::

   ------------------------TRANSPORT THERMO CONVECTION SURF CONDITIONS
   DSURF  0
   E <setnumber> - <temperature state> coeff 0.0 surtemp 0.0 surtempfunct none \ 
       funct none

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - temperature state
     - Tempnp
     - Tempnp, Tempn

.. _designscatraheterogeneousreactionlineconditions/master:

DESIGN SCATRA HETEROGENEOUS REACTION LINE CONDITIONS / MASTER
-------------------------------------------------------------

calculation of heterogeneous reactions

::

   ------DESIGN SCATRA HETEROGENEOUS REACTION LINE CONDITIONS / MASTER
   DLINE  0
   E <setnumber> -

.. _designscatraheterogeneousreactionsurfconditions/master:

DESIGN SCATRA HETEROGENEOUS REACTION SURF CONDITIONS / MASTER
-------------------------------------------------------------

calculation of heterogeneous reactions

::

   ------DESIGN SCATRA HETEROGENEOUS REACTION SURF CONDITIONS / MASTER
   DSURF  0
   E <setnumber> -

.. _designscatraheterogeneousreactionlineconditions/slave:

DESIGN SCATRA HETEROGENEOUS REACTION LINE CONDITIONS / SLAVE
------------------------------------------------------------

calculation of heterogeneous reactions

::

   -------DESIGN SCATRA HETEROGENEOUS REACTION LINE CONDITIONS / SLAVE
   DLINE  0
   E <setnumber> -

.. _designscatraheterogeneousreactionsurfconditions/slave:

DESIGN SCATRA HETEROGENEOUS REACTION SURF CONDITIONS / SLAVE
------------------------------------------------------------

calculation of heterogeneous reactions

::

   -------DESIGN SCATRA HETEROGENEOUS REACTION SURF CONDITIONS / SLAVE
   DSURF  0
   E <setnumber> -

.. _designscatrasurfconditions/partitioning:

DESIGN SCATRA SURF CONDITIONS / PARTITIONING
--------------------------------------------

Domain partitioning of scatra field

::

   -----------------------DESIGN SCATRA SURF CONDITIONS / PARTITIONING
   DSURF  0
   E <setnumber> -

.. _designscatravolconditions/partitioning:

DESIGN SCATRA VOL CONDITIONS / PARTITIONING
-------------------------------------------

Domain partitioning of scatra field

::

   ------------------------DESIGN SCATRA VOL CONDITIONS / PARTITIONING
   DVOL  0
   E <setnumber> -

.. _designelectrodestateofchargelineconditions:

DESIGN ELECTRODE STATE OF CHARGE LINE CONDITIONS
------------------------------------------------

electrode state of charge line condition

::

   -------------------DESIGN ELECTRODE STATE OF CHARGE LINE CONDITIONS
   DLINE  0
   E <setnumber> - ID 0 C_0% 0.0 C_100% 0.0 ONE_HOUR 0.0

.. _designelectrodestateofchargesurfconditions:

DESIGN ELECTRODE STATE OF CHARGE SURF CONDITIONS
------------------------------------------------

electrode state of charge surface condition

::

   -------------------DESIGN ELECTRODE STATE OF CHARGE SURF CONDITIONS
   DSURF  0
   E <setnumber> - ID 0 C_0% 0.0 C_100% 0.0 ONE_HOUR 0.0

.. _designelectrodestateofchargevolconditions:

DESIGN ELECTRODE STATE OF CHARGE VOL CONDITIONS
-----------------------------------------------

electrode state of charge volume condition

::

   --------------------DESIGN ELECTRODE STATE OF CHARGE VOL CONDITIONS
   DVOL  0
   E <setnumber> - ID 0 C_0% 0.0 C_100% 0.0 ONE_HOUR 0.0

.. _designcellvoltagepointconditions:

DESIGN CELL VOLTAGE POINT CONDITIONS
------------------------------------

cell voltage point condition

::

   -------------------------------DESIGN CELL VOLTAGE POINT CONDITIONS
   DPOINT  0
   E <setnumber> - ID 0

.. _designcellvoltagelineconditions:

DESIGN CELL VOLTAGE LINE CONDITIONS
-----------------------------------

cell voltage line condition

::

   --------------------------------DESIGN CELL VOLTAGE LINE CONDITIONS
   DLINE  0
   E <setnumber> - ID 0

.. _designcellvoltagesurfconditions:

DESIGN CELL VOLTAGE SURF CONDITIONS
-----------------------------------

cell voltage surface condition

::

   --------------------------------DESIGN CELL VOLTAGE SURF CONDITIONS
   DSURF  0
   E <setnumber> - ID 0

.. _electrodeboundarykineticspointconditions:

ELECTRODE BOUNDARY KINETICS POINT CONDITIONS
--------------------------------------------

point electrode boundary kinetics

::

   -----------------------ELECTRODE BOUNDARY KINETICS POINT CONDITIONS
   DPOINT  0
   E <setnumber> - ID 0 POT 0.0 FUNCT none NUMSCAL 0 STOICH 0 0  \ 
       E- 0 EPSILON 0.0 ZERO_CUR 0 <kinetic model> [further parameters]

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - kinetic model
     - Butler-Volmer
     - Butler-Volmer, Butler-Volmer-Yang1997, Tafel, |break| 
       linear, Butler-Volmer-Newman, Butler-Volmer-Bard, |break| 
       Nernst

The following parameter sets are possible for `<kinetic model>`:

::

   Butler-Volmer ALPHA_A 0.0 ALPHA_C 0.0 I0 0.0 GAMMA 0.0 REFCON 0.0 DL_SPEC_CAP 0.0 
   Butler-Volmer-Yang1997 ALPHA_A 0.0 ALPHA_C 0.0 I0 0.0 GAMMA 0.0 REFCON 0.0 DL_SPEC_CAP 0.0 
   Tafel ALPHA 0.0 I0 0.0 GAMMA 0.0 REFCON 0.0 DL_SPEC_CAP 0.0 
   linear ALPHA 0.0 I0 0.0 GAMMA 0.0 REFCON 0.0 DL_SPEC_CAP 0.0 
   Butler-Volmer-Newman K_A 0.0 K_C 0.0 BETA 0.0 DL_SPEC_CAP 0.0 
   Butler-Volmer-Bard E0 0.0 K0 0.0 BETA 0.0 C_C0 0.0 C_A0 0.0 DL_SPEC_CAP 0.0 
   Nernst E0 0.0 C0 0.0 DL_SPEC_CAP 0.0 

.. _electrodeboundarykineticslineconditions:

ELECTRODE BOUNDARY KINETICS LINE CONDITIONS
-------------------------------------------

line electrode boundary kinetics

::

   ------------------------ELECTRODE BOUNDARY KINETICS LINE CONDITIONS
   DLINE  0
   E <setnumber> - ID 0 POT 0.0 FUNCT none NUMSCAL 0 STOICH 0 0  \ 
       E- 0 EPSILON 0.0 ZERO_CUR 0 <kinetic model> [further parameters]

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - kinetic model
     - Butler-Volmer
     - Butler-Volmer, Butler-Volmer-Yang1997, Tafel, |break| 
       linear, Butler-Volmer-Newman, Butler-Volmer-Bard, |break| 
       Nernst

The following parameter sets are possible for `<kinetic model>`:

::

   Butler-Volmer ALPHA_A 0.0 ALPHA_C 0.0 I0 0.0 GAMMA 0.0 REFCON 0.0 DL_SPEC_CAP 0.0 
   Butler-Volmer-Yang1997 ALPHA_A 0.0 ALPHA_C 0.0 I0 0.0 GAMMA 0.0 REFCON 0.0 DL_SPEC_CAP 0.0 
   Tafel ALPHA 0.0 I0 0.0 GAMMA 0.0 REFCON 0.0 DL_SPEC_CAP 0.0 
   linear ALPHA 0.0 I0 0.0 GAMMA 0.0 REFCON 0.0 DL_SPEC_CAP 0.0 
   Butler-Volmer-Newman K_A 0.0 K_C 0.0 BETA 0.0 DL_SPEC_CAP 0.0 
   Butler-Volmer-Bard E0 0.0 K0 0.0 BETA 0.0 C_C0 0.0 C_A0 0.0 DL_SPEC_CAP 0.0 
   Nernst E0 0.0 C0 0.0 DL_SPEC_CAP 0.0 

.. _electrodeboundarykineticssurfconditions:

ELECTRODE BOUNDARY KINETICS SURF CONDITIONS
-------------------------------------------

surface electrode boundary kinetics

::

   ------------------------ELECTRODE BOUNDARY KINETICS SURF CONDITIONS
   DSURF  0
   E <setnumber> - ID 0 POT 0.0 FUNCT none NUMSCAL 0 STOICH 0 0  \ 
       E- 0 EPSILON 0.0 ZERO_CUR 0 <kinetic model> [further parameters]

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - kinetic model
     - Butler-Volmer
     - Butler-Volmer, Butler-Volmer-Yang1997, Tafel, |break| 
       linear, Butler-Volmer-Newman, Butler-Volmer-Bard, |break| 
       Nernst

The following parameter sets are possible for `<kinetic model>`:

::

   Butler-Volmer ALPHA_A 0.0 ALPHA_C 0.0 I0 0.0 GAMMA 0.0 REFCON 0.0 DL_SPEC_CAP 0.0 
   Butler-Volmer-Yang1997 ALPHA_A 0.0 ALPHA_C 0.0 I0 0.0 GAMMA 0.0 REFCON 0.0 DL_SPEC_CAP 0.0 
   Tafel ALPHA 0.0 I0 0.0 GAMMA 0.0 REFCON 0.0 DL_SPEC_CAP 0.0 
   linear ALPHA 0.0 I0 0.0 GAMMA 0.0 REFCON 0.0 DL_SPEC_CAP 0.0 
   Butler-Volmer-Newman K_A 0.0 K_C 0.0 BETA 0.0 DL_SPEC_CAP 0.0 
   Butler-Volmer-Bard E0 0.0 K0 0.0 BETA 0.0 C_C0 0.0 C_A0 0.0 DL_SPEC_CAP 0.0 
   Nernst E0 0.0 C0 0.0 DL_SPEC_CAP 0.0 

.. _electrodedomainkineticslineconditions:

ELECTRODE DOMAIN KINETICS LINE CONDITIONS
-----------------------------------------

line electrode domain kinetics

::

   --------------------------ELECTRODE DOMAIN KINETICS LINE CONDITIONS
   DLINE  0
   E <setnumber> - ID 0 POT 0.0 FUNCT none NUMSCAL 0 STOICH 0 0  \ 
       E- 0 ZERO_CUR 0 <kinetic model> [further parameters]

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - kinetic model
     - Butler-Volmer
     - Butler-Volmer

The following parameter sets are possible for `<kinetic model>`:

::

   Butler-Volmer A_S 0.0 ALPHA_A 0.0 ALPHA_C 0.0 I0 0.0 GAMMA 0.0 REFCON 0.0 DL_SPEC_CAP 0.0 END 

.. _electrodedomainkineticssurfconditions:

ELECTRODE DOMAIN KINETICS SURF CONDITIONS
-----------------------------------------

surface electrode domain kinetics

::

   --------------------------ELECTRODE DOMAIN KINETICS SURF CONDITIONS
   DSURF  0
   E <setnumber> - ID 0 POT 0.0 FUNCT none NUMSCAL 0 STOICH 0 0  \ 
       E- 0 ZERO_CUR 0 <kinetic model> [further parameters]

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - kinetic model
     - Butler-Volmer
     - Butler-Volmer

The following parameter sets are possible for `<kinetic model>`:

::

   Butler-Volmer A_S 0.0 ALPHA_A 0.0 ALPHA_C 0.0 I0 0.0 GAMMA 0.0 REFCON 0.0 DL_SPEC_CAP 0.0 END 

.. _electrodedomainkineticsvolconditions:

ELECTRODE DOMAIN KINETICS VOL CONDITIONS
----------------------------------------

volume electrode domain kinetics

::

   ---------------------------ELECTRODE DOMAIN KINETICS VOL CONDITIONS
   DVOL  0
   E <setnumber> - ID 0 POT 0.0 FUNCT none NUMSCAL 0 STOICH 0 0  \ 
       E- 0 ZERO_CUR 0 <kinetic model> [further parameters]

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - kinetic model
     - Butler-Volmer
     - Butler-Volmer

The following parameter sets are possible for `<kinetic model>`:

::

   Butler-Volmer A_S 0.0 ALPHA_A 0.0 ALPHA_C 0.0 I0 0.0 GAMMA 0.0 REFCON 0.0 DL_SPEC_CAP 0.0 END 

.. _designcccvcellcyclingpointconditions:

DESIGN CCCV CELL CYCLING POINT CONDITIONS
-----------------------------------------

line boundary condition for constant-current constant-voltage (CCCV) cell cycling

::

   --------------------------DESIGN CCCV CELL CYCLING POINT CONDITIONS
   DPOINT  0
   E <setnumber> - NUMBER_OF_HALF_CYCLES 0 BEGIN_WITH_CHARGING 0 \ 
       CONDITION_ID_FOR_CHARGE none CONDITION_ID_FOR_DISCHARGE none \ 
       INIT_RELAX_TIME 0.0 ADAPTIVE_TIME_STEPPING_INIT_RELAX 0 NUM_ADD_ADAPT_TIME_STEPS none \ 
       MIN_TIME_STEPS_DURING_INIT_RELAX none

.. _designcccvcellcyclinglineconditions:

DESIGN CCCV CELL CYCLING LINE CONDITIONS
----------------------------------------

line boundary condition for constant-current constant-voltage (CCCV) cell cycling

::

   ---------------------------DESIGN CCCV CELL CYCLING LINE CONDITIONS
   DLINE  0
   E <setnumber> - NUMBER_OF_HALF_CYCLES 0 BEGIN_WITH_CHARGING 0 \ 
       CONDITION_ID_FOR_CHARGE none CONDITION_ID_FOR_DISCHARGE none \ 
       INIT_RELAX_TIME 0.0 ADAPTIVE_TIME_STEPPING_INIT_RELAX 0 NUM_ADD_ADAPT_TIME_STEPS none \ 
       MIN_TIME_STEPS_DURING_INIT_RELAX none

.. _designcccvcellcyclingsurfconditions:

DESIGN CCCV CELL CYCLING SURF CONDITIONS
----------------------------------------

surface boundary condition for constant-current constant-voltage (CCCV) cell cycling

::

   ---------------------------DESIGN CCCV CELL CYCLING SURF CONDITIONS
   DSURF  0
   E <setnumber> - NUMBER_OF_HALF_CYCLES 0 BEGIN_WITH_CHARGING 0 \ 
       CONDITION_ID_FOR_CHARGE none CONDITION_ID_FOR_DISCHARGE none \ 
       INIT_RELAX_TIME 0.0 ADAPTIVE_TIME_STEPPING_INIT_RELAX 0 NUM_ADD_ADAPT_TIME_STEPS none \ 
       MIN_TIME_STEPS_DURING_INIT_RELAX none

.. _designcccvhalf-cyclepointconditions:

DESIGN CCCV HALF-CYCLE POINT CONDITIONS
---------------------------------------

line boundary condition for constant-current constant-voltage (CCCV) half-cycle

::

   ----------------------------DESIGN CCCV HALF-CYCLE POINT CONDITIONS
   DPOINT  0
   E <setnumber> - ID 0 CURRENT 0.0 CUT_OFF_VOLTAGE 0.0 CUT_OFF_C_RATE 0.0 \ 
       RELAX_TIME 0.0 ADAPTIVE_TIME_STEPPING_PHASE_ON_OFF <int vec:AdaptiveTimeSteppingPhaseOnOff> 

.. _designcccvhalf-cyclelineconditions:

DESIGN CCCV HALF-CYCLE LINE CONDITIONS
--------------------------------------

line boundary condition for constant-current constant-voltage (CCCV) half-cycle

::

   -----------------------------DESIGN CCCV HALF-CYCLE LINE CONDITIONS
   DLINE  0
   E <setnumber> - ID 0 CURRENT 0.0 CUT_OFF_VOLTAGE 0.0 CUT_OFF_C_RATE 0.0 \ 
       RELAX_TIME 0.0 ADAPTIVE_TIME_STEPPING_PHASE_ON_OFF <int vec:AdaptiveTimeSteppingPhaseOnOff> 

.. _designcccvhalf-cyclesurfconditions:

DESIGN CCCV HALF-CYCLE SURF CONDITIONS
--------------------------------------

surface boundary condition for constant-current constant-voltage (CCCV) half-cycle

::

   -----------------------------DESIGN CCCV HALF-CYCLE SURF CONDITIONS
   DSURF  0
   E <setnumber> - ID 0 CURRENT 0.0 CUT_OFF_VOLTAGE 0.0 CUT_OFF_C_RATE 0.0 \ 
       RELAX_TIME 0.0 ADAPTIVE_TIME_STEPPING_PHASE_ON_OFF <int vec:AdaptiveTimeSteppingPhaseOnOff> 

.. _designsurfturbulentinflowtransfer:

DESIGN SURF TURBULENT INFLOW TRANSFER
-------------------------------------

TransferTurbulentInflow

::

   ------------------------------DESIGN SURF TURBULENT INFLOW TRANSFER
   DSURF  0
   E <setnumber> - ID 0 <toggle> DIRECTION <transfer direction> <int vec [incl none]:curve> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - toggle
     - master
     - master, slave
   * - transfer direction
     - x
     - x, y, z

.. _fluidturbulentinflowvolume:

FLUID TURBULENT INFLOW VOLUME
-----------------------------

TurbulentInflowSection

::

   --------------------------------------FLUID TURBULENT INFLOW VOLUME
   DVOL  0
   E <setnumber> -

.. _designlineflow-dependentpressureconditions:

DESIGN LINE FLOW-DEPENDENT PRESSURE CONDITIONS
----------------------------------------------

LineFlowDepPressure

::

   ---------------------DESIGN LINE FLOW-DEPENDENT PRESSURE CONDITIONS
   DLINE  0
   E <setnumber> - <type of flow dependence> 0.0 0.0 0.0 0.0 0.0 \ 
       none

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type of flow dependence
     - flow_rate
     - flow_rate, flow_volume, fixed_pressure

.. _designsurfaceflow-dependentpressureconditions:

DESIGN SURFACE FLOW-DEPENDENT PRESSURE CONDITIONS
-------------------------------------------------

SurfaceFlowDepPressure

::

   ------------------DESIGN SURFACE FLOW-DEPENDENT PRESSURE CONDITIONS
   DSURF  0
   E <setnumber> - <type of flow dependence> 0.0 0.0 0.0 0.0 0.0 \ 
       none

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type of flow dependence
     - flow_rate
     - flow_rate, flow_volume, fixed_pressure

.. _designlineslipsupplementalcurvedboundaryconditions:

DESIGN LINE SLIP SUPPLEMENTAL CURVED BOUNDARY CONDITIONS
--------------------------------------------------------

LineSlipSupp

::

   -----------DESIGN LINE SLIP SUPPLEMENTAL CURVED BOUNDARY CONDITIONS
   DLINE  0
   E <setnumber> - USEUPDATEDNODEPOS <int vec:useupdatednodepos> 

.. _designsurfaceslipsupplementalcurvedboundaryconditions:

DESIGN SURFACE SLIP SUPPLEMENTAL CURVED BOUNDARY CONDITIONS
-----------------------------------------------------------

SurfaceSlipSupp

::

   --------DESIGN SURFACE SLIP SUPPLEMENTAL CURVED BOUNDARY CONDITIONS
   DSURF  0
   E <setnumber> - USEUPDATEDNODEPOS <int vec:useupdatednodepos> 

.. _designlinenavier-slipboundaryconditions:

DESIGN LINE NAVIER-SLIP BOUNDARY CONDITIONS
-------------------------------------------

LineNavierSlip

::

   ------------------------DESIGN LINE NAVIER-SLIP BOUNDARY CONDITIONS
   DLINE  0
   E <setnumber> - SLIPCOEFFICIENT 0.0

.. _designsurfnavier-slipboundaryconditions:

DESIGN SURF NAVIER-SLIP BOUNDARY CONDITIONS
-------------------------------------------

SurfNavierSlip

::

   ------------------------DESIGN SURF NAVIER-SLIP BOUNDARY CONDITIONS
   DSURF  0
   E <setnumber> - SLIPCOEFFICIENT 0.0

.. _designsurfaceconservativeoutflowconsistency:

DESIGN SURFACE CONSERVATIVE OUTFLOW CONSISTENCY
-----------------------------------------------

SurfaceConservativeOutflowConsistency

::

   --------------------DESIGN SURFACE CONSERVATIVE OUTFLOW CONSISTENCY
   DSURF  0
   E <setnumber> -

.. _fluidneumanninflowlineconditions:

FLUID NEUMANN INFLOW LINE CONDITIONS
------------------------------------

Line Fluid Neumann Inflow

::

   -------------------------------FLUID NEUMANN INFLOW LINE CONDITIONS
   DLINE  0
   E <setnumber> -

.. _fluidneumanninflowsurfconditions:

FLUID NEUMANN INFLOW SURF CONDITIONS
------------------------------------

Surface Fluid Neumann Inflow

::

   -------------------------------FLUID NEUMANN INFLOW SURF CONDITIONS
   DSURF  0
   E <setnumber> -

.. _designlinemixed/hybriddirichletconditions:

DESIGN LINE MIXED/HYBRID DIRICHLET CONDITIONS
---------------------------------------------

LineMixHybDirichlet

::

   ----------------------DESIGN LINE MIXED/HYBRID DIRICHLET CONDITIONS
   DLINE  0
   E <setnumber> - <real vec:val>  <int vec:funct>  0.0 <Definition of penalty parameter> \ 
       0.0 <utau_computation>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Definition of penalty parameter
     - constant
     - constant, Spalding
   * - utau_computation
     - at_wall
     - at_wall, viscous_tangent

.. _designsurfacemixed/hybriddirichletconditions:

DESIGN SURFACE MIXED/HYBRID DIRICHLET CONDITIONS
------------------------------------------------

SurfaceMixHybDirichlet

::

   -------------------DESIGN SURFACE MIXED/HYBRID DIRICHLET CONDITIONS
   DSURF  0
   E <setnumber> - <real vec:val>  <int vec:funct>  0.0 <Definition of penalty parameter> \ 
       0.0 <utau_computation>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Definition of penalty parameter
     - constant
     - constant, Spalding
   * - utau_computation
     - at_wall
     - at_wall, viscous_tangent

.. _surfacetensionconditions:

SURFACE TENSION CONDITIONS
--------------------------

Surface Stress (ideal water)

::

   -----------------------------------------SURFACE TENSION CONDITIONS
   DSURF  0
   E <setnumber> - none gamma 0.0

.. _designfluidfreesurfacelineconditions:

DESIGN FLUID FREE SURFACE LINE CONDITIONS
-----------------------------------------

FREESURF Coupling

::

   --------------------------DESIGN FLUID FREE SURFACE LINE CONDITIONS
   DLINE  0
   E <setnumber> - FIELD <field> COUPLING <coupling> VAL <real vec:val>  \ 
       NODENORMALFUNCT 0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - field
     - fluid
     - fluid, ale
   * - coupling
     - lagrange
     - lagrange, heightfunction, sphereHeightFunction, |break| 
       meantangentialvelocity, |break| 
       meantangentialvelocityscaled

.. _designfluidfreesurfacesurfconditions:

DESIGN FLUID FREE SURFACE SURF CONDITIONS
-----------------------------------------

FREESURF Coupling

::

   --------------------------DESIGN FLUID FREE SURFACE SURF CONDITIONS
   DSURF  0
   E <setnumber> - FIELD <field> COUPLING <coupling> VAL <real vec:val>  \ 
       NODENORMALFUNCT 0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - field
     - fluid
     - fluid, ale
   * - coupling
     - lagrange
     - lagrange, heightfunction, sphereHeightFunction, |break| 
       meantangentialvelocity, |break| 
       meantangentialvelocityscaled

.. _designfluidstresscalclineconditions:

DESIGN FLUID STRESS CALC LINE CONDITIONS
----------------------------------------

Line Fluid Stress Calculation

::

   ---------------------------DESIGN FLUID STRESS CALC LINE CONDITIONS
   DLINE  0
   E <setnumber> -

.. _designfluidstresscalcsurfconditions:

DESIGN FLUID STRESS CALC SURF CONDITIONS
----------------------------------------

Surf Fluid Stress Calculation

::

   ---------------------------DESIGN FLUID STRESS CALC SURF CONDITIONS
   DSURF  0
   E <setnumber> -

.. _designfluidlinelift&drag:

DESIGN FLUID LINE LIFT&DRAG
---------------------------

Line LIFTDRAG

::

   ----------------------------------------DESIGN FLUID LINE LIFT&DRAG
   DLINE  0
   E <setnumber> - 0 CENTER <real vec:centerCoord>  AXIS <real vec:axis> 

.. _designfluidsurflift&drag:

DESIGN FLUID SURF LIFT&DRAG
---------------------------

Surface LIFTDRAG

::

   ----------------------------------------DESIGN FLUID SURF LIFT&DRAG
   DSURF  0
   E <setnumber> - 0 CENTER <real vec:centerCoord>  AXIS <real vec:axis> 

.. _designflowratelineconditions:

DESIGN FLOW RATE LINE CONDITIONS
--------------------------------

Line Flow Rate

::

   -----------------------------------DESIGN FLOW RATE LINE CONDITIONS
   DLINE  0
   E <setnumber> - 0

.. _designflowratesurfconditions:

DESIGN FLOW RATE SURF CONDITIONS
--------------------------------

Surface Flow Rate

::

   -----------------------------------DESIGN FLOW RATE SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0

.. _designimpulsratesurfconditions:

DESIGN IMPULS RATE SURF CONDITIONS
----------------------------------

Surface Impuls Rate

::

   ---------------------------------DESIGN IMPULS RATE SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0

.. _designsurfvolumetricflowconditions:

DESIGN SURF VOLUMETRIC FLOW CONDITIONS
--------------------------------------

volumetric surface flow condition

::

   -----------------------------DESIGN SURF VOLUMETRIC FLOW CONDITIONS
   DSURF  0
   E <setnumber> - 0 <ConditionType> <prebiased> <FlowType> <CorrectionFlag> \ 
       Period 0.0 Order 0 Harmonics 0 Val 0.0 Funct 0 <NORMAL> 0.0 \ 
       0.0 0.0 <CenterOfMass> 0.0 0.0 0.0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - ConditionType
     - WOMERSLEY
     - WOMERSLEY, POLYNOMIAL
   * - prebiased
     - NOTPREBIASED
     - NOTPREBIASED, PREBIASED, FORCED
   * - FlowType
     - InFlow
     - InFlow, OutFlow
   * - CorrectionFlag
     - WithOutCorrection
     - WithOutCorrection, WithCorrection
   * - NORMAL
     - SelfEvaluateNormal
     - SelfEvaluateNormal, UsePrescribedNormal
   * - CenterOfMass
     - SelfEvaluateCenterOfMass
     - SelfEvaluateCenterOfMass, |break| 
       UsePrescribedCenterOfMass

.. _designlinevolumetricflowbordernodes:

DESIGN LINE VOLUMETRIC FLOW BORDER NODES
----------------------------------------

volumetric flow border nodes condition

::

   ---------------------------DESIGN LINE VOLUMETRIC FLOW BORDER NODES
   DLINE  0
   E <setnumber> - 0

.. _designsurftotaltractioncorrectionconditions:

DESIGN SURF TOTAL TRACTION CORRECTION CONDITIONS
------------------------------------------------

total traction correction condition

::

   -------------------DESIGN SURF TOTAL TRACTION CORRECTION CONDITIONS
   DSURF  0
   E <setnumber> - 0 <ConditionType> <prebiased> <FlowType> <CorrectionFlag> \ 
       Period 0.0 Order 0 Harmonics 0 Val 0.0 Funct 0 <NORMAL> 0.0 \ 
       0.0 0.0 <CenterOfMass> 0.0 0.0 0.0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - ConditionType
     - POLYNOMIAL
     - POLYNOMIAL, WOMERSLEY
   * - prebiased
     - NOTPREBIASED
     - NOTPREBIASED, PREBIASED, FORCED
   * - FlowType
     - InFlow
     - InFlow, OutFlow
   * - CorrectionFlag
     - WithOutCorrection
     - WithOutCorrection, WithCorrection
   * - NORMAL
     - SelfEvaluateNormal
     - SelfEvaluateNormal, UsePrescribedNormal
   * - CenterOfMass
     - SelfEvaluateCenterOfMass
     - SelfEvaluateCenterOfMass, |break| 
       UsePrescribedCenterOfMass

.. _designlinetotaltractioncorrectionbordernodes:

DESIGN LINE TOTAL TRACTION CORRECTION BORDER NODES
--------------------------------------------------

total traction correction border nodes condition

::

   -----------------DESIGN LINE TOTAL TRACTION CORRECTION BORDER NODES
   DLINE  0
   E <setnumber> - 0

.. _designsurfacenormalnopenetrationcondition:

DESIGN SURFACE NORMAL NO PENETRATION CONDITION
----------------------------------------------

No Penetration

::

   ---------------------DESIGN SURFACE NORMAL NO PENETRATION CONDITION
   DSURF  0
   E <setnumber> -

.. _designlinenormalnopenetrationcondition:

DESIGN LINE NORMAL NO PENETRATION CONDITION
-------------------------------------------

No Penetration

::

   ------------------------DESIGN LINE NORMAL NO PENETRATION CONDITION
   DLINE  0
   E <setnumber> -

.. _designvolumeporocouplingcondition:

DESIGN VOLUME POROCOUPLING CONDITION
------------------------------------

Poro Coupling

::

   -------------------------------DESIGN VOLUME POROCOUPLING CONDITION
   DVOL  0
   E <setnumber> -

.. _designsurfaceporocouplingcondition:

DESIGN SURFACE POROCOUPLING CONDITION
-------------------------------------

Poro Coupling

::

   ------------------------------DESIGN SURFACE POROCOUPLING CONDITION
   DSURF  0
   E <setnumber> -

.. _designsurfaceporopartialintegration:

DESIGN SURFACE PORO PARTIAL INTEGRATION
---------------------------------------

Poro Partial Integration

::

   ----------------------------DESIGN SURFACE PORO PARTIAL INTEGRATION
   DSURF  0
   E <setnumber> -

.. _designlineporopartialintegration:

DESIGN LINE PORO PARTIAL INTEGRATION
------------------------------------

Poro Partial Integration

::

   -------------------------------DESIGN LINE PORO PARTIAL INTEGRATION
   DLINE  0
   E <setnumber> -

.. _designsurfaceporopressureintegration:

DESIGN SURFACE PORO PRESSURE INTEGRATION
----------------------------------------

Poro Pressure Integration

::

   ---------------------------DESIGN SURFACE PORO PRESSURE INTEGRATION
   DSURF  0
   E <setnumber> -

.. _designlineporopressureintegration:

DESIGN LINE PORO PRESSURE INTEGRATION
-------------------------------------

Poro Pressure Integration

::

   ------------------------------DESIGN LINE PORO PRESSURE INTEGRATION
   DLINE  0
   E <setnumber> -

.. _designflucthydrostatisticssurfconditions:

DESIGN FLUCTHYDRO STATISTICS SURF CONDITIONS
--------------------------------------------

FluctHydro_StatisticsSurf

::

   -----------------------DESIGN FLUCTHYDRO STATISTICS SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0 <evaluation type>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - evaluation type
     - nodalbased
     - elebased, nodalbased, ele_and_nodalbased

.. _designflucthydrostatisticslineconditions:

DESIGN FLUCTHYDRO STATISTICS LINE CONDITIONS
--------------------------------------------

FluctHydro_StatisticsLine

::

   -----------------------DESIGN FLUCTHYDRO STATISTICS LINE CONDITIONS
   DLINE  0
   E <setnumber> - 0 <evaluation type>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - evaluation type
     - nodalbased
     - elebased, nodalbased, ele_and_nodalbased

.. _designaleupdatelineconditions:

DESIGN ALE UPDATE LINE CONDITIONS
---------------------------------

ALEUPDATE Coupling

::

   ----------------------------------DESIGN ALE UPDATE LINE CONDITIONS
   DLINE  0
   E <setnumber> - COUPLING <coupling> VAL <real vec:val>  NODENORMALFUNCT 0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - coupling
     - lagrange
     - lagrange, heightfunction, sphereHeightFunction, |break| 
       meantangentialvelocity, |break| 
       meantangentialvelocityscaled

.. _designaleupdatesurfconditions:

DESIGN ALE UPDATE SURF CONDITIONS
---------------------------------

ALEUPDATE Coupling

::

   ----------------------------------DESIGN ALE UPDATE SURF CONDITIONS
   DSURF  0
   E <setnumber> - COUPLING <coupling> VAL <real vec:val>  NODENORMALFUNCT 0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - coupling
     - lagrange
     - lagrange, heightfunction, sphereHeightFunction, |break| 
       meantangentialvelocity, |break| 
       meantangentialvelocityscaled

.. _designfsicouplinglineconditions:

DESIGN FSI COUPLING LINE CONDITIONS
-----------------------------------

FSI Coupling

::

   --------------------------------DESIGN FSI COUPLING LINE CONDITIONS
   DLINE  0
   E <setnumber> - 0

.. _designfsicouplingsurfconditions:

DESIGN FSI COUPLING SURF CONDITIONS
-----------------------------------

FSI Coupling

::

   --------------------------------DESIGN FSI COUPLING SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0

.. _designfsicouplingnoslidelineconditions:

DESIGN FSI COUPLING NO SLIDE LINE CONDITIONS
--------------------------------------------

FSI Coupling No Slide

::

   -----------------------DESIGN FSI COUPLING NO SLIDE LINE CONDITIONS
   DLINE  0
   E <setnumber> -

.. _designfsicouplingnoslidesurfconditions:

DESIGN FSI COUPLING NO SLIDE SURF CONDITIONS
--------------------------------------------

FSI Coupling No Slide

::

   -----------------------DESIGN FSI COUPLING NO SLIDE SURF CONDITIONS
   DSURF  0
   E <setnumber> -

.. _designfsicouplingcenterdisplineconditions:

DESIGN FSI COUPLING CENTER DISP LINE CONDITIONS
-----------------------------------------------

FSI Coupling Center Disp

::

   --------------------DESIGN FSI COUPLING CENTER DISP LINE CONDITIONS
   DLINE  0
   E <setnumber> -

.. _designfsicouplingcenterdispsurfconditions:

DESIGN FSI COUPLING CENTER DISP SURF CONDITIONS
-----------------------------------------------

FSI Coupling Center Disp

::

   --------------------DESIGN FSI COUPLING CENTER DISP SURF CONDITIONS
   DSURF  0
   E <setnumber> -

.. _designstructurealecouplingsurfconditions:

DESIGN STRUCTURE ALE COUPLING SURF CONDITIONS
---------------------------------------------

StructAleCoupling

::

   ----------------------DESIGN STRUCTURE ALE COUPLING SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0 <field>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - field
     - structure
     - structure, fluid

.. _designstructurefluidvolumecouplingsurfconditions:

DESIGN STRUCTURE FLUID VOLUME COUPLING SURF CONDITIONS
------------------------------------------------------

StructFluidSurfCoupling

::

   -------------DESIGN STRUCTURE FLUID VOLUME COUPLING SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0 <field>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - field
     - structure
     - structure, fluid

.. _designstructurefluidvolumecouplingvolconditions:

DESIGN STRUCTURE FLUID VOLUME COUPLING VOL CONDITIONS
-----------------------------------------------------

StructFluidVolCoupling

::

   --------------DESIGN STRUCTURE FLUID VOLUME COUPLING VOL CONDITIONS
   DVOL  0
   E <setnumber> - 0 <field>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - field
     - structure
     - structure, fluid

.. _designfpsicouplinglineconditions:

DESIGN FPSI COUPLING LINE CONDITIONS
------------------------------------

FPSI Coupling

::

   -------------------------------DESIGN FPSI COUPLING LINE CONDITIONS
   DLINE  0
   E <setnumber> - 0

.. _designfpsicouplingsurfconditions:

DESIGN FPSI COUPLING SURF CONDITIONS
------------------------------------

FPSI Coupling

::

   -------------------------------DESIGN FPSI COUPLING SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0

.. _designsurfaceneumannintegration:

DESIGN SURFACE NEUMANN INTEGRATION
----------------------------------

Neumann Integration

::

   ---------------------------------DESIGN SURFACE NEUMANN INTEGRATION
   DSURF  0
   E <setnumber> -

.. _designlineneumannintegration:

DESIGN LINE NEUMANN INTEGRATION
-------------------------------

Neumann Integration

::

   ------------------------------------DESIGN LINE NEUMANN INTEGRATION
   DLINE  0
   E <setnumber> -

.. _designvolumeimmersedsearchbox:

DESIGN VOLUME IMMERSED SEARCHBOX
--------------------------------

Immersed Searchbox

::

   -----------------------------------DESIGN VOLUME IMMERSED SEARCHBOX
   DVOL  0
   E <setnumber> -

.. _designimmersedcouplinglineconditions:

DESIGN IMMERSED COUPLING LINE CONDITIONS
----------------------------------------

IMMERSED Coupling

::

   ---------------------------DESIGN IMMERSED COUPLING LINE CONDITIONS
   DLINE  0
   E <setnumber> - 0

.. _designimmersedcouplingsurfconditions:

DESIGN IMMERSED COUPLING SURF CONDITIONS
----------------------------------------

IMMERSED Coupling

::

   ---------------------------DESIGN IMMERSED COUPLING SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0

.. _designfluidfluidcouplingsurfconditions:

DESIGN FLUID FLUID COUPLING SURF CONDITIONS
-------------------------------------------

FLUID FLUID Coupling

::

   ------------------------DESIGN FLUID FLUID COUPLING SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0

.. _designfluidmeshvolconditions:

DESIGN FLUID MESH VOL CONDITIONS
--------------------------------

Fluid Mesh

::

   -----------------------------------DESIGN FLUID MESH VOL CONDITIONS
   DVOL  0
   E <setnumber> - 0

.. _designalefluidcouplingsurfconditions:

DESIGN ALE FLUID COUPLING SURF CONDITIONS
-----------------------------------------

ALE FLUID Coupling

::

   --------------------------DESIGN ALE FLUID COUPLING SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0

.. _designxfemdisplacementsurfconditions:

DESIGN XFEM DISPLACEMENT SURF CONDITIONS
----------------------------------------

XFEM Surf Displacement

::

   ---------------------------DESIGN XFEM DISPLACEMENT SURF CONDITIONS
   DSURF  0
   E <setnumber> - COUPLINGID <int vec:label>  EVALTYPE <evaltype> \ 
       NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - evaltype
     - funct
     - zero, funct, implementation
   * - tag
     - none
     - none, monitor_reaction

.. _designxfemlevelsetweakdirichletvolconditions:

DESIGN XFEM LEVELSET WEAK DIRICHLET VOL CONDITIONS
--------------------------------------------------

XFEM Levelset Weak Dirichlet

::

   -----------------DESIGN XFEM LEVELSET WEAK DIRICHLET VOL CONDITIONS
   DVOL  0
   E <setnumber> - COUPLINGID <int vec:label>  LEVELSETFIELDNO <int vec:levelsetfieldno>  \ 
       BOOLEANTYPE <booleantype> COMPLEMENTARY <int vec:complementary>  \ 
       NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag> RANDNOISE <real vec:randnoise> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - booleantype
     - none
     - none, cut, union, difference, sym_difference
   * - tag
     - none
     - none, monitor_reaction

.. _designxfemlevelsetneumannvolconditions:

DESIGN XFEM LEVELSET NEUMANN VOL CONDITIONS
-------------------------------------------

XFEM Levelset Neumann

::

   ------------------------DESIGN XFEM LEVELSET NEUMANN VOL CONDITIONS
   DVOL  0
   E <setnumber> - COUPLINGID <int vec:label>  LEVELSETFIELDNO <int vec:levelsetfieldno>  \ 
       BOOLEANTYPE <booleantype> COMPLEMENTARY <int vec:complementary>  \ 
       NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface> INFLOW_STAB <int vec [incl none]:InflowStab> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - booleantype
     - none
     - none, cut, union, difference, sym_difference
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designxfemlevelsetnavierslipvolconditions:

DESIGN XFEM LEVELSET NAVIER SLIP VOL CONDITIONS
-----------------------------------------------

XFEM Levelset Navier Slip

::

   --------------------DESIGN XFEM LEVELSET NAVIER SLIP VOL CONDITIONS
   DVOL  0
   E <setnumber> - COUPLINGID <int vec:label>  LEVELSETFIELDNO <int vec:levelsetfieldno>  \ 
       BOOLEANTYPE <booleantype> COMPLEMENTARY <int vec:complementary>  \ 
       SURFACE_PROJECTION <SURFACE_PROJECTION> L2_PROJECTION_SOLVER <int vec [incl none]:l2projsolv>  \ 
       ROBIN_DIRICHLET_ID <int vec [incl none]:robin_id_dirch>  ROBIN_NEUMANN_ID <int vec [incl none]:robin_id_neumann>  \ 
       SLIPCOEFFICIENT 0.0 SLIP_FUNCT <int vec:funct>  FORCE_ONLY_TANG_VEL <int vec:force_tang_vel> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - booleantype
     - none
     - none, cut, union, difference, sym_difference
   * - SURFACE_PROJECTION
     - proj_normal
     - proj_normal, proj_smoothed, |break| 
       proj_normal_smoothed_comb, proj_normal_phi

.. _designxfemrobindirichletvolconditions:

DESIGN XFEM ROBIN DIRICHLET VOL CONDITIONS
------------------------------------------

XFEM Robin Dirichlet Volume

::

   -------------------------DESIGN XFEM ROBIN DIRICHLET VOL CONDITIONS
   DVOL  0
   E <setnumber> - ROBIN_DIRICHLET_ID <int vec [incl none]:robin_id>  \ 
       NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - tag
     - none
     - none, monitor_reaction

.. _designxfemrobinneumannvolconditions:

DESIGN XFEM ROBIN NEUMANN VOL CONDITIONS
----------------------------------------

XFEM Robin Neumann Volume

::

   ---------------------------DESIGN XFEM ROBIN NEUMANN VOL CONDITIONS
   DVOL  0
   E <setnumber> - ROBIN_NEUMANN_ID <int vec [incl none]:robin_id>  \ 
       NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designxfemlevelsettwophasevolconditions:

DESIGN XFEM LEVELSET TWOPHASE VOL CONDITIONS
--------------------------------------------

XFEM Levelset Twophase

::

   -----------------------DESIGN XFEM LEVELSET TWOPHASE VOL CONDITIONS
   DVOL  0
   E <setnumber> - COUPLINGID <int vec:label>  LEVELSETFIELDNO <int vec:levelsetfieldno>  \ 
       BOOLEANTYPE <booleantype> COMPLEMENTARY <int vec:complementary> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - booleantype
     - none
     - none, cut, union, difference, sym_difference

.. _designxfemlevelsetcombustionvolconditions:

DESIGN XFEM LEVELSET COMBUSTION VOL CONDITIONS
----------------------------------------------

XFEM Levelset Combustion

::

   ---------------------DESIGN XFEM LEVELSET COMBUSTION VOL CONDITIONS
   DVOL  0
   E <setnumber> - COUPLINGID <int vec:label>  LEVELSETFIELDNO <int vec:levelsetfieldno>  \ 
       BOOLEANTYPE <booleantype> COMPLEMENTARY <int vec:complementary>  \ 
       LAMINAR_FLAMESPEED <real vec:laminar_flamespeed>  MOL_DIFFUSIVITY <real vec:mol_diffusivity>  \ 
       MARKSTEIN_LENGTH <real vec:markstein_length>  TRANSPORT_DIRECTIONS <TRANSPORT_DIRECTIONS> \ 
       TRANSPORT_CURVATURE <int vec:transport_curvature> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - booleantype
     - none
     - none, cut, union, difference, sym_difference
   * - TRANSPORT_DIRECTIONS
     - all
     - all, normal

.. _designxfemfluidfluidsurfconditions:

DESIGN XFEM FLUIDFLUID SURF CONDITIONS
--------------------------------------

XFEM Surf FluidFluid

::

   -----------------------------DESIGN XFEM FLUIDFLUID SURF CONDITIONS
   DSURF  0
   E <setnumber> - COUPLINGID <int vec:label>  <COUPSTRATEGY>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - COUPSTRATEGY
     - xfluid
     - xfluid, embedded, mean

.. _designxfemfsipartitionedsurfconditions:

DESIGN XFEM FSI PARTITIONED SURF CONDITIONS
-------------------------------------------

XFEM Surf FSI Part

::

   ------------------------DESIGN XFEM FSI PARTITIONED SURF CONDITIONS
   DSURF  0
   E <setnumber> - COUPLINGID <int vec:label>  INTLAW <INTLAW> SLIPCOEFFICIENT <real vec:slipcoeff>  \ 
       SLIP_FUNCT <int vec:funct> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - INTLAW
     - noslip
     - noslip, noslip_splitpen, slip, navslip

.. _designxfemfsimonolithicsurfconditions:

DESIGN XFEM FSI MONOLITHIC SURF CONDITIONS
------------------------------------------

XFEM Surf FSI Mono

::

   -------------------------DESIGN XFEM FSI MONOLITHIC SURF CONDITIONS
   DSURF  0
   E <setnumber> - COUPLINGID <int vec:label>  COUPSTRATEGY <COUPSTRATEGY> \ 
       INTLAW <INTLAW> SLIPCOEFFICIENT <real vec:slipcoeff>  SLIP_FUNCT <int vec:funct> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - COUPSTRATEGY
     - xfluid
     - xfluid, solid, mean, harmonic
   * - INTLAW
     - noslip
     - noslip, noslip_splitpen, slip, navslip, |break| 
       navslip_contact

.. _designxfemfpimonolithicsurfconditions:

DESIGN XFEM FPI MONOLITHIC SURF CONDITIONS
------------------------------------------

XFEM Surf FPI Mono

::

   -------------------------DESIGN XFEM FPI MONOLITHIC SURF CONDITIONS
   DSURF  0
   E <setnumber> - COUPLINGID <int vec:label>  BJ_COEFF <real vec:bj_coeff>  \ 
       <Variant> <Method> <Contact>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Variant
     - BJ
     - BJ, BJS
   * - Method
     - NIT
     - NIT, SUB
   * - Contact
     - contact_no
     - contact_no, contact_yes

.. _designxfemweakdirichletsurfconditions:

DESIGN XFEM WEAK DIRICHLET SURF CONDITIONS
------------------------------------------

XFEM Surf Weak Dirichlet

::

   -------------------------DESIGN XFEM WEAK DIRICHLET SURF CONDITIONS
   DSURF  0
   E <setnumber> - COUPLINGID <int vec:label>  EVALTYPE <evaltype> \ 
       NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag> RANDNOISE <real vec:randnoise> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - evaltype
     - funct_interpolated
     - zero, funct_interpolated, funct_gausspoint, |break| 
       displacement_1storder_wo_initfunct, |break| 
       displacement_2ndorder_wo_initfunct, |break| 
       displacement_1storder_with_initfunct, |break| 
       displacement_2ndorder_with_initfunct
   * - tag
     - none
     - none, monitor_reaction

.. _designxfemneumannsurfconditions:

DESIGN XFEM NEUMANN SURF CONDITIONS
-----------------------------------

XFEM Surf Neumann

::

   --------------------------------DESIGN XFEM NEUMANN SURF CONDITIONS
   DSURF  0
   E <setnumber> - COUPLINGID <int vec:label>  NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface> INFLOW_STAB <int vec [incl none]:InflowStab> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designxfemnavierslipsurfconditions:

DESIGN XFEM NAVIER SLIP SURF CONDITIONS
---------------------------------------

XFEM Surf Navier Slip

::

   ----------------------------DESIGN XFEM NAVIER SLIP SURF CONDITIONS
   DSURF  0
   E <setnumber> - COUPLINGID <int vec:label>  EVALTYPE <evaltype> \ 
       ROBIN_DIRICHLET_ID <int vec [incl none]:robin_id_dirch>  ROBIN_NEUMANN_ID <int vec [incl none]:robin_id_neumann>  \ 
       SLIPCOEFFICIENT 0.0 SLIP_FUNCT <int vec:funct>  FORCE_ONLY_TANG_VEL <int vec:force_tang_vel> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - evaltype
     - funct_interpolated
     - zero, funct_interpolated, funct_gausspoint, |break| 
       displacement_1storder_wo_initfunct, |break| 
       displacement_2ndorder_wo_initfunct, |break| 
       displacement_1storder_with_initfunct, |break| 
       displacement_2ndorder_with_initfunct

.. _designxfemnaviersliptwophasesurfconditions:

DESIGN XFEM NAVIER SLIP TWO PHASE SURF CONDITIONS
-------------------------------------------------

XFEM Surf Navier Slip

::

   ------------------DESIGN XFEM NAVIER SLIP TWO PHASE SURF CONDITIONS
   DSURF  0
   E <setnumber> - COUPLINGID <int vec:label>  EVALTYPE <evaltype> \ 
       ROBIN_DIRICHLET_ID <int vec [incl none]:robin_id_dirch>  ROBIN_NEUMANN_ID <int vec [incl none]:robin_id_neumann>  \ 
       SLIP_SMEAR 0.0 NORMAL_PENALTY_SCALING 0.0 SLIPCOEFFICIENT 0.0 \ 
       SLIP_FUNCT <int vec:funct>  FORCE_ONLY_TANG_VEL <int vec:force_tang_vel> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - evaltype
     - funct_interpolated
     - zero, funct_interpolated, funct_gausspoint, |break| 
       displacement_1storder_wo_initfunct, |break| 
       displacement_2ndorder_wo_initfunct, |break| 
       displacement_1storder_with_initfunct, |break| 
       displacement_2ndorder_with_initfunct

.. _designxfemrobindirichletsurfconditions:

DESIGN XFEM ROBIN DIRICHLET SURF CONDITIONS
-------------------------------------------

XFEM Robin Dirichlet Volume

::

   ------------------------DESIGN XFEM ROBIN DIRICHLET SURF CONDITIONS
   DSURF  0
   E <setnumber> - COUPLINGID <int vec:label>  ROBIN_DIRICHLET_ID <int vec [incl none]:robin_id>  \ 
       EVALTYPE <evaltype> NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       TAG <tag>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - evaltype
     - funct_interpolated
     - zero, funct_interpolated, funct_gausspoint, |break| 
       displacement_1storder_wo_initfunct, |break| 
       displacement_2ndorder_wo_initfunct, |break| 
       displacement_1storder_with_initfunct, |break| 
       displacement_2ndorder_with_initfunct
   * - tag
     - none
     - none, monitor_reaction

.. _designxfemrobinneumannsurfconditions:

DESIGN XFEM ROBIN NEUMANN SURF CONDITIONS
-----------------------------------------

XFEM Robin Neumann Volume

::

   --------------------------DESIGN XFEM ROBIN NEUMANN SURF CONDITIONS
   DSURF  0
   E <setnumber> - COUPLINGID <int vec:label>  ROBIN_NEUMANN_ID <int vec [incl none]:robin_id>  \ 
       NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>   \ 
       <type> <surface>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - Live
     - Live, Dead, PrescribedDomainLoad, constHydro_z, |break| 
       increaseHydro_z, pseudo_orthopressure, |break| 
       orthopressure, LAS, PressureGrad, Torque
   * - surface
     - Mid
     - Mid, Top, Bot

.. _designsurfaceinvanalysis:

DESIGN SURFACE INV ANALYSIS
---------------------------

Inverse Analysis Surface

::

   ----------------------------------------DESIGN SURFACE INV ANALYSIS
   DSURF  0
   E <setnumber> - 0

.. _designsurfacecurrentevaluationcondition:

DESIGN SURFACE CURRENT EVALUATION CONDITION
-------------------------------------------

Surface current

::

   ------------------------DESIGN SURFACE CURRENT EVALUATION CONDITION
   DSURF  0
   E <setnumber> - 0

.. _designuncertainsurfacecondition:

DESIGN UNCERTAIN SURFACE CONDITION
----------------------------------

Uncertain surface

::

   ---------------------------------DESIGN UNCERTAIN SURFACE CONDITION
   DSURF  0
   E <setnumber> - 0

.. _designbiofilmgrowthcouplinglineconditions:

DESIGN BIOFILM GROWTH COUPLING LINE CONDITIONS
----------------------------------------------

BioGrCoupling

::

   ---------------------DESIGN BIOFILM GROWTH COUPLING LINE CONDITIONS
   DLINE  0
   E <setnumber> - 0

.. _designbiofilmgrowthcouplingsurfconditions:

DESIGN BIOFILM GROWTH COUPLING SURF CONDITIONS
----------------------------------------------

BioGrCoupling

::

   ---------------------DESIGN BIOFILM GROWTH COUPLING SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0

.. _designnode1darteryjunctionconditions:

DESIGN NODE 1D ARTERY JUNCTION CONDITIONS
-----------------------------------------

Artery junction boundary condition

::

   --------------------------DESIGN NODE 1D ARTERY JUNCTION CONDITIONS
   DPOINT  0
   E <setnumber> - 0 0.0

.. _designlineexport1d-arterialnetworkgnuplotformat:

DESIGN LINE EXPORT 1D-ARTERIAL NETWORK GNUPLOT FORMAT
-----------------------------------------------------

Artery write gnuplot format condition

::

   --------------DESIGN LINE EXPORT 1D-ARTERIAL NETWORK GNUPLOT FORMAT
   DLINE  0
   E <setnumber> - 0

.. _designnode1darteryprescribedconditions:

DESIGN NODE 1D ARTERY PRESCRIBED CONDITIONS
-------------------------------------------

Artery prescribed boundary condition

::

   ------------------------DESIGN NODE 1D ARTERY PRESCRIBED CONDITIONS
   DPOINT  0
   E <setnumber> - <boundarycond> <type> <real vec:val>  <int vec [incl none]:curve> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - boundarycond
     - flow
     - flow, pressure, velocity, area, characteristicWave
   * - type
     - forced
     - forced, absorbing

.. _designnode1darteryreflectiveconditions:

DESIGN NODE 1D ARTERY REFLECTIVE CONDITIONS
-------------------------------------------

Artery reflection condition

::

   ------------------------DESIGN NODE 1D ARTERY REFLECTIVE CONDITIONS
   DPOINT  0
   E <setnumber> - <real vec:val>  <int vec [incl none]:curve> 

.. _designnode1darterywindkesselconditions:

DESIGN NODE 1D ARTERY WINDKESSEL CONDITIONS
-------------------------------------------

Artery windkessel condition

::

   ------------------------DESIGN NODE 1D ARTERY WINDKESSEL CONDITIONS
   DPOINT  0
   E <setnumber> - <intigrationType> <windkesselType> <real vec:val>  \ 
       <int vec [incl none]:curve> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - intigrationType
     - ExplicitWindkessel
     - ExplicitWindkessel, ImpedaceWindkessel
   * - windkesselType
     - RCR
     - R, RC, RCR, RCRL, none

.. _designnode1darteryin_outletconditions:

DESIGN NODE 1D ARTERY IN_OUTLET CONDITIONS
------------------------------------------

Artery terminal in_outlet condition

::

   -------------------------DESIGN NODE 1D ARTERY IN_OUTLET CONDITIONS
   DPOINT  0
   E <setnumber> - <terminaltype>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - terminaltype
     - inlet
     - inlet, outlet

.. _designnode1darteryscatraprescribedconditions:

DESIGN NODE 1D ARTERY SCATRA PRESCRIBED CONDITIONS
--------------------------------------------------

Artery prescribed scatra boundary condition

::

   -----------------DESIGN NODE 1D ARTERY SCATRA PRESCRIBED CONDITIONS
   DPOINT  0
   E <setnumber> - <real vec:val>  <int vec [incl none]:curve> 

.. _designnode1darterytoporofluidcouplingconditions:

DESIGN NODE 1D ARTERY TO POROFLUID COUPLING CONDITIONS
------------------------------------------------------

Artery coupling with porofluid

::

   -------------DESIGN NODE 1D ARTERY TO POROFLUID COUPLING CONDITIONS
   DPOINT  0
   E <setnumber> - COUPID 0

.. _designnode1darterytoscatracouplingconditions:

DESIGN NODE 1D ARTERY TO SCATRA COUPLING CONDITIONS
---------------------------------------------------

Artery coupling with porofluid

::

   ----------------DESIGN NODE 1D ARTERY TO SCATRA COUPLING CONDITIONS
   DPOINT  0
   E <setnumber> - COUPID 0

.. _design1dartery/airwaytoporofluidnonconfcouplingconditions:

DESIGN 1D ARTERY/AIRWAY TO POROFLUID NONCONF COUPLING CONDITIONS
----------------------------------------------------------------

Artery coupling with porofluid nonconf

::

   ---DESIGN 1D ARTERY/AIRWAY TO POROFLUID NONCONF COUPLING CONDITIONS
   DPOINT  0
   E <setnumber> - <coupling_type> COUPLEDDOF_REDUCED 0 COUPLEDDOF_PORO 0 \ 
       PENALTY 0.0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - coupling_type
     - ARTERY
     - ARTERY, AIRWAY

.. _design1dartery/airwaytoscatranonconfcouplingconditions:

DESIGN 1D ARTERY/AIRWAY TO SCATRA NONCONF COUPLING CONDITIONS
-------------------------------------------------------------

Artery coupling with scatra nonconf

::

   ------DESIGN 1D ARTERY/AIRWAY TO SCATRA NONCONF COUPLING CONDITIONS
   DPOINT  0
   E <setnumber> - <coupling_type> COUPLEDDOF_REDUCED 0 COUPLEDDOF_PORO 0 \ 
       PENALTY 0.0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - coupling_type
     - ARTERY
     - ARTERY, AIRWAY

.. _designnodereduceddto3dflowcouplingconditions:

DESIGN NODE REDUCED D To 3D FLOW COUPLING CONDITIONS
----------------------------------------------------

Artery reduced D 3D coupling condition

::

   ---------------DESIGN NODE REDUCED D To 3D FLOW COUPLING CONDITIONS
   DPOINT  0
   E <setnumber> - 0 <CouplingType> <ReturnedVariable> Tolerance 0.0 \ 
       MaximumIterations 0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - CouplingType
     - forced
     - forced, absorbing
   * - ReturnedVariable
     - pressure
     - pressure, flow

.. _designsurf3dtoreduceddflowcouplingconditions:

DESIGN SURF 3D To REDUCED D FLOW COUPLING CONDITIONS
----------------------------------------------------

Artery 3D reduced D coupling condition

::

   ---------------DESIGN SURF 3D To REDUCED D FLOW COUPLING CONDITIONS
   DSURF  0
   E <setnumber> - 0 <ReturnedVariable> Tolerance 0.0 MaximumIterations 0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - ReturnedVariable
     - flow
     - pressure, flow

.. _designsurftissueredairwayconditions:

DESIGN SURF TISSUE REDAIRWAY CONDITIONS
---------------------------------------

tissue RedAirway coupling surface condition

::

   ----------------------------DESIGN SURF TISSUE REDAIRWAY CONDITIONS
   DSURF  0
   E <setnumber> - 0

.. _designnodetissueredairwayconditions:

DESIGN NODE TISSUE REDAIRWAY CONDITIONS
---------------------------------------

tissue RedAirway coupling node condition

::

   ----------------------------DESIGN NODE TISSUE REDAIRWAY CONDITIONS
   DPOINT  0
   E <setnumber> - 0

.. _designnodereduceddairwaysprescribedconditions:

DESIGN NODE Reduced D AIRWAYS PRESCRIBED CONDITIONS
---------------------------------------------------

Reduced d airway prescribed boundary condition

::

   ----------------DESIGN NODE Reduced D AIRWAYS PRESCRIBED CONDITIONS
   DPOINT  0
   E <setnumber> - <boundarycond> <real vec:val>  <int vec [incl none]:curve>  \ 
       <int vec [incl none]:funct> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - boundarycond
     - flow
     - flow, pressure, switchFlowPressure, |break| 
       VolumeDependentPleuralPressure

.. _designnodereduceddairwaysswitchflowpressureconditions:

DESIGN NODE Reduced D AIRWAYS SWITCH FLOW PRESSURE CONDITIONS
-------------------------------------------------------------

Reduced d airway switch flow pressure boundary condition

::

   ------DESIGN NODE Reduced D AIRWAYS SWITCH FLOW PRESSURE CONDITIONS
   DPOINT  0
   E <setnumber> - FUNCT_ID_FLOW 0 FUNCT_ID_PRESSURE 0 FUNCT_ID_PRESSURE_ACTIVE 0

.. _designlinereduceddairwaysprescribedexternalpressureconditions:

DESIGN LINE Reduced D AIRWAYS PRESCRIBED EXTERNAL PRESSURE CONDITIONS
---------------------------------------------------------------------

Reduced d airway prescribed external pressure boundary condition

::

   --DESIGN LINE Reduced D AIRWAYS PRESCRIBED EXTERNAL PRESSURE CONDITIONS
   DLINE  0
   E <setnumber> - <boundarycond> <real vec:val>  <int vec [incl none]:curve>  \ 
       <int vec:funct> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - boundarycond
     - ExternalPressure
     - ExternalPressure

.. _designnodereduceddairwaysprescribedscatraconditions:

DESIGN NODE Reduced D AIRWAYS PRESCRIBED SCATRA CONDITIONS
----------------------------------------------------------

Reduced d airway prescribed scatra boundary condition

::

   ---------DESIGN NODE Reduced D AIRWAYS PRESCRIBED SCATRA CONDITIONS
   DPOINT  0
   E <setnumber> - <real vec:val>  <int vec [incl none]:curve>  <int vec:funct> 

.. _designlinereduceddairwaysinitialscatraconditions:

DESIGN LINE Reduced D AIRWAYS INITIAL SCATRA CONDITIONS
-------------------------------------------------------

Reduced d airway initial scatra boundary condition

::

   ------------DESIGN LINE Reduced D AIRWAYS INITIAL SCATRA CONDITIONS
   DLINE  0
   E <setnumber> - <scalar> CONCENTRATION 0.0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - scalar
     - O2
     - O2, CO2

.. _designlinereduceddairwaysscatraexchangeconditions:

DESIGN LINE Reduced D AIRWAYS SCATRA EXCHANGE CONDITIONS
--------------------------------------------------------

scatra exchange condition

::

   -----------DESIGN LINE Reduced D AIRWAYS SCATRA EXCHANGE CONDITIONS
   DLINE  0
   E <setnumber> - 0

.. _designlinereduceddairwayshemoglobinconditions:

DESIGN LINE Reduced D AIRWAYS HEMOGLOBIN CONDITIONS
---------------------------------------------------

scatra hemoglobin condition

::

   ----------------DESIGN LINE Reduced D AIRWAYS HEMOGLOBIN CONDITIONS
   DLINE  0
   E <setnumber> - INITIAL_CONCENTRATION 0.0

.. _designlinereduceddairwaysairconditions:

DESIGN LINE Reduced D AIRWAYS AIR CONDITIONS
--------------------------------------------

scatra air condition

::

   -----------------------DESIGN LINE Reduced D AIRWAYS AIR CONDITIONS
   DLINE  0
   E <setnumber> - INITIAL_CONCENTRATION 0.0

.. _designlinereduceddairwayscapillaryconditions:

DESIGN LINE Reduced D AIRWAYS CAPILLARY CONDITIONS
--------------------------------------------------

scatra capillary condition

::

   -----------------DESIGN LINE Reduced D AIRWAYS CAPILLARY CONDITIONS
   DLINE  0
   E <setnumber> -

.. _designnodereduceddairwaysventilatorconditions:

DESIGN NODE Reduced D AIRWAYS VENTILATOR CONDITIONS
---------------------------------------------------

Reduced d airway prescribed ventilator condition

::

   ----------------DESIGN NODE Reduced D AIRWAYS VENTILATOR CONDITIONS
   DPOINT  0
   E <setnumber> - <phase1> <Phase1Smoothness> <phase2> <Phase2Smoothness> \ 
       period 0.0 phase1_period 0.0 smoothness_period1 0.0 smoothness_period2 0.0 \ 
       <real vec:val>  <int vec [incl none]:curve> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - phase1
     - flow
     - flow, volume, pressure
   * - Phase1Smoothness
     - smooth
     - smooth, discontinous
   * - phase2
     - pressure
     - pressure, flow, volume
   * - Phase2Smoothness
     - smooth
     - smooth, discontinous

.. _designlinereduceddairwaysvoldependentpleuralpressureconditions:

DESIGN LINE REDUCED D AIRWAYS VOL DEPENDENT PLEURAL PRESSURE CONDITIONS
-----------------------------------------------------------------------

Reduced D airways volume-dependent peural pressure condition

::

   --DESIGN LINE REDUCED D AIRWAYS VOL DEPENDENT PLEURAL PRESSURE CONDITIONS
   DLINE  0
   E <setnumber> - <TYPE> TLC 0.0 RV 0.0 P_PLEURAL_0 0.0 P_PLEURAL_LIN 0.0 \ 
       P_PLEURAL_NONLIN 0.0 TAU 0.0 <real vec:val>  <int vec [incl none]:curve> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - TYPE
     - Linear_Exponential
     - Linear_Polynomial, Linear_Exponential, |break| 
       Linear_Ogden, Nonlinear_Polynomial, |break| 
       Nonlinear_Exponential, Nonlinear_Ogden

.. _designlinereduceddairwaysevaluatelungvolumeconditions:

DESIGN LINE REDUCED D AIRWAYS EVALUATE LUNG VOLUME CONDITIONS
-------------------------------------------------------------

Reduced D airways evaluate lung volume condition

::

   ------DESIGN LINE REDUCED D AIRWAYS EVALUATE LUNG VOLUME CONDITIONS
   DLINE  0
   E <setnumber> -

.. _designsurfimpedanceconditions:

DESIGN SURF IMPEDANCE CONDITIONS
--------------------------------

Impedance boundary condition

::

   -----------------------------------DESIGN SURF IMPEDANCE CONDITIONS
   DSURF  0
   E <setnumber> - 0 <TYPE> R1 0.0 R2 0.0 C 0.0 TIMEPERIOD 0.0 FUNCT 0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - TYPE
     - windkessel
     - windkessel, resistive, pressure_by_funct

.. _designsurfcardiovascular0d4-elementwindkesselconditions:

DESIGN SURF CARDIOVASCULAR 0D 4-ELEMENT WINDKESSEL CONDITIONS
-------------------------------------------------------------

Surface Cardiovascular0D

::

   ------DESIGN SURF CARDIOVASCULAR 0D 4-ELEMENT WINDKESSEL CONDITIONS
   DSURF  0
   E <setnumber> - id 0 C 0.0 R_p 0.0 Z_c 0.0 L 0.0 p_ref 0.0 p_0 0.0

.. _designsurfcardiovascular0darterialproxdistconditions:

DESIGN SURF CARDIOVASCULAR 0D ARTERIAL PROX DIST CONDITIONS
-----------------------------------------------------------

Surface 0D cardiovascular arterial proximal and distal

::

   --------DESIGN SURF CARDIOVASCULAR 0D ARTERIAL PROX DIST CONDITIONS
   DSURF  0
   E <setnumber> - id 0 R_arvalve_max 0.0 R_arvalve_min 0.0 R_atvalve_max 0.0 \ 
       R_atvalve_min 0.0 k_p 0.0 L_arp 0.0 C_arp 0.0 R_arp 0.0 C_ard 0.0 \ 
       R_ard 0.0 p_ref 0.0 p_v_0 0.0 p_arp_0 0.0 y_arp_0 0.0 p_ard_0 0.0 \ 
       P_AT fac 0.0 crv <int vec [incl none]:curve> 

.. _designsurfcardiovascular0dsys-pulcirculationconditions:

DESIGN SURF CARDIOVASCULAR 0D SYS-PUL CIRCULATION CONDITIONS
------------------------------------------------------------

Surface cardiovascular 0D sys pul circulation condition

::

   -------DESIGN SURF CARDIOVASCULAR 0D SYS-PUL CIRCULATION CONDITIONS
   DSURF  0
   E <setnumber> - id 0 TYPE <type>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - ventricle_left
     - ventricle_left, ventricle_right, atrium_left, |break| 
       atrium_right, dummy

.. _designsurfcardiovascularrespiratory0dsys-pulperiphcirculationconditions:

DESIGN SURF CARDIOVASCULAR RESPIRATORY 0D SYS-PUL PERIPH CIRCULATION CONDITIONS
-------------------------------------------------------------------------------

Surface 0D cardiovascular respiratory sys-pul periph circulation condition

::

   --DESIGN SURF CARDIOVASCULAR RESPIRATORY 0D SYS-PUL PERIPH CIRCULATION CONDITIONS
   DSURF  0
   E <setnumber> - id 0 TYPE <type>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - type
     - ventricle_left
     - ventricle_left, ventricle_right, atrium_left, |break| 
       atrium_right, dummy

.. _designsurfcardiovascular0d-structurecouplingconditions:

DESIGN SURF CARDIOVASCULAR 0D-STRUCTURE COUPLING CONDITIONS
-----------------------------------------------------------

structure 0d cardiovascular coupling surface condition

::

   --------DESIGN SURF CARDIOVASCULAR 0D-STRUCTURE COUPLING CONDITIONS
   DSURF  0
   E <setnumber> - coupling_id 0

.. _designsurfrobinspringdashpotconditions:

DESIGN SURF ROBIN SPRING DASHPOT CONDITIONS
-------------------------------------------

Robin Spring Dashpot

::

   ------------------------DESIGN SURF ROBIN SPRING DASHPOT CONDITIONS
   DSURF  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  STIFF <real vec:stiff>  \ 
       TIMEFUNCTSTIFF <int vec:funct_stiff>  VISCO <real vec:visco>  \ 
       TIMEFUNCTVISCO <int vec:funct_visco>  DISPLOFFSET <real vec:disploffset>  \ 
       TIMEFUNCTDISPLOFFSET <int vec:funct_disploffset>  FUNCTNONLINSTIFF <int vec:funct_nonlinstiff>  \ 
       DIRECTION <direction> COUPLING <int vec [incl none]:coupling id> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - direction
     - xyz
     - xyz, refsurfnormal, cursurfnormal

.. _designpointrobinspringdashpotconditions:

DESIGN POINT ROBIN SPRING DASHPOT CONDITIONS
--------------------------------------------

Robin Spring Dashpot

::

   -----------------------DESIGN POINT ROBIN SPRING DASHPOT CONDITIONS
   DPOINT  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  STIFF <real vec:stiff>  \ 
       TIMEFUNCTSTIFF <int vec:funct_stiff>  VISCO <real vec:visco>  \ 
       TIMEFUNCTVISCO <int vec:funct_visco>  DISPLOFFSET <real vec:disploffset>  \ 
       TIMEFUNCTDISPLOFFSET <int vec:funct_disploffset>  FUNCTNONLINSTIFF <int vec:funct_nonlinstiff>  \ 
       DIRECTION <direction> COUPLING <int vec [incl none]:coupling id> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - direction
     - xyz
     - xyz, refsurfnormal, cursurfnormal

.. _designsurfrobinspringdashpotcouplingconditions:

DESIGN SURF ROBIN SPRING DASHPOT COUPLING CONDITIONS
----------------------------------------------------

RobinSpring Dashpot Coupling

::

   ---------------DESIGN SURF ROBIN SPRING DASHPOT COUPLING CONDITIONS
   DSURF  0
   E <setnumber> - 0

.. _surfactantconditions:

SURFACTANT CONDITIONS
---------------------

Surface Stress (surfactant)

::

   ----------------------------------------------SURFACTANT CONDITIONS
   DSURF  0
   E <setnumber> - none k1xCbulk 0.0 k2 0.0 m1 0.0 m2 0.0 gamma_0 0.0 \ 
       gamma_min 0.0

.. _designthermoconvectionlineconditions:

DESIGN THERMO CONVECTION LINE CONDITIONS
----------------------------------------

Line Thermo Convections

::

   ---------------------------DESIGN THERMO CONVECTION LINE CONDITIONS
   DLINE  0
   E <setnumber> - <temperature state> coeff 0.0 surtemp 0.0 surtempfunct none \ 
       funct none

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - temperature state
     - Tempnp
     - Tempnp, Tempn

.. _designthermoconvectionsurfconditions:

DESIGN THERMO CONVECTION SURF CONDITIONS
----------------------------------------

Surface Thermo Convections

::

   ---------------------------DESIGN THERMO CONVECTION SURF CONDITIONS
   DSURF  0
   E <setnumber> - <temperature state> coeff 0.0 surtemp 0.0 surtempfunct none \ 
       funct none

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - temperature state
     - Tempnp
     - Tempnp, Tempn

.. _designthermorobinlineconditions:

DESIGN THERMO ROBIN LINE CONDITIONS
-----------------------------------

Thermo Robin boundary condition

::

   --------------------------------DESIGN THERMO ROBIN LINE CONDITIONS
   DLINE  0
   E <setnumber> - NUMSCAL 0 ONOFF 0 0  PREFACTOR 0.0 REFVALUE 0.0

.. _designthermorobinsurfconditions:

DESIGN THERMO ROBIN SURF CONDITIONS
-----------------------------------

Thermo Robin boundary condition

::

   --------------------------------DESIGN THERMO ROBIN SURF CONDITIONS
   DSURF  0
   E <setnumber> - NUMSCAL 0 ONOFF 0 0  PREFACTOR 0.0 REFVALUE 0.0

.. _designssicouplinglineconditions:

DESIGN SSI COUPLING LINE CONDITIONS
-----------------------------------

SSI Coupling

::

   --------------------------------DESIGN SSI COUPLING LINE CONDITIONS
   DLINE  0
   E <setnumber> - 0

.. _designssicouplingsurfconditions:

DESIGN SSI COUPLING SURF CONDITIONS
-----------------------------------

SSI Coupling

::

   --------------------------------DESIGN SSI COUPLING SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0

.. _designssicouplingvolconditions:

DESIGN SSI COUPLING VOL CONDITIONS
----------------------------------

SSI Coupling

::

   ---------------------------------DESIGN SSI COUPLING VOL CONDITIONS
   DVOL  0
   E <setnumber> - 0

.. _designssicouplingsolidtoscatralineconditions:

DESIGN SSI COUPLING SOLIDTOSCATRA LINE CONDITIONS
-------------------------------------------------

SSI Coupling SolidToScatra

::

   ------------------DESIGN SSI COUPLING SOLIDTOSCATRA LINE CONDITIONS
   DLINE  0
   E <setnumber> - 0

.. _designssicouplingsolidtoscatrasurfconditions:

DESIGN SSI COUPLING SOLIDTOSCATRA SURF CONDITIONS
-------------------------------------------------

SSI Coupling SolidToScatra

::

   ------------------DESIGN SSI COUPLING SOLIDTOSCATRA SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0

.. _designssicouplingsolidtoscatravolconditions:

DESIGN SSI COUPLING SOLIDTOSCATRA VOL CONDITIONS
------------------------------------------------

SSI Coupling SolidToScatra

::

   -------------------DESIGN SSI COUPLING SOLIDTOSCATRA VOL CONDITIONS
   DVOL  0
   E <setnumber> - 0

.. _designssicouplingscatratosolidlineconditions:

DESIGN SSI COUPLING SCATRATOSOLID LINE CONDITIONS
-------------------------------------------------

SSI Coupling ScatraToSolid

::

   ------------------DESIGN SSI COUPLING SCATRATOSOLID LINE CONDITIONS
   DLINE  0
   E <setnumber> - 0

.. _designssicouplingscatratosolidsurfconditions:

DESIGN SSI COUPLING SCATRATOSOLID SURF CONDITIONS
-------------------------------------------------

SSI Coupling ScatraToSolid

::

   ------------------DESIGN SSI COUPLING SCATRATOSOLID SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0

.. _designssicouplingscatratosolidvolconditions:

DESIGN SSI COUPLING SCATRATOSOLID VOL CONDITIONS
------------------------------------------------

SSI Coupling ScatraToSolid

::

   -------------------DESIGN SSI COUPLING SCATRATOSOLID VOL CONDITIONS
   DVOL  0
   E <setnumber> - 0

.. _designssiinterfacemeshtyingpointconditions:

DESIGN SSI INTERFACE MESHTYING POINT CONDITIONS
-----------------------------------------------

SSI Interface Meshtying

::

   --------------------DESIGN SSI INTERFACE MESHTYING POINT CONDITIONS
   DPOINT  0
   E <setnumber> - 0 <interface side> S2I_KINETICS_ID 0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - interface side
     - Undefined
     - Undefined, Slave, Master

.. _designssiinterfacemeshtyinglineconditions:

DESIGN SSI INTERFACE MESHTYING LINE CONDITIONS
----------------------------------------------

SSI Interface Meshtying

::

   ---------------------DESIGN SSI INTERFACE MESHTYING LINE CONDITIONS
   DLINE  0
   E <setnumber> - 0 <interface side> S2I_KINETICS_ID 0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - interface side
     - Undefined
     - Undefined, Slave, Master

.. _designssiinterfacemeshtyingsurfconditions:

DESIGN SSI INTERFACE MESHTYING SURF CONDITIONS
----------------------------------------------

SSI Interface Meshtying

::

   ---------------------DESIGN SSI INTERFACE MESHTYING SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0 <interface side> S2I_KINETICS_ID 0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - interface side
     - Undefined
     - Undefined, Slave, Master

.. _designssimanifoldsurfconditions:

DESIGN SSI MANIFOLD SURF CONDITIONS
-----------------------------------

scalar transport on manifold

::

   --------------------------------DESIGN SSI MANIFOLD SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0 ImplType <ImplType> thickness 0.0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - ImplType
     - Undefined
     - Undefined, Standard, ElchElectrode, ElchDiffCond

.. _designsurfscatramanifoldinitialfieldconditions:

DESIGN SURF SCATRA MANIFOLD INITIAL FIELD CONDITIONS
----------------------------------------------------

Surface ScaTra Manifold Initfield

::

   ---------------DESIGN SURF SCATRA MANIFOLD INITIAL FIELD CONDITIONS
   DSURF  0
   E <setnumber> - <Field> <int vec:funct> 

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Field
     - ScaTra
     - ScaTra

.. _designssimanifoldkineticssurfconditions:

DESIGN SSI MANIFOLD KINETICS SURF CONDITIONS
--------------------------------------------

kinetics model for coupling scatra <-> scatra on manifold

::

   -----------------------DESIGN SSI MANIFOLD KINETICS SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0 ManifoldConditionID 0 KINETIC_MODEL <kinetic model> [further parameters]

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - kinetic model
     - ConstantInterfaceResistance
     - ConstantInterfaceResistance, Butler-VolmerReduced, |break| 
       NoInterfaceFlux

The following parameter sets are possible for `<kinetic model>`:

::

   Butler-VolmerReduced NUMSCAL 0 STOICHIOMETRIES  E- 0 K_R 0.0 ALPHA_A 0.0 ALPHA_C 0.0 
   ConstantInterfaceResistance ONOFF <int vec:onoff>  RESISTANCE 0.0 E- 0 
   NoInterfaceFlux 

.. _designpointmanifolddirichconditions:

DESIGN POINT MANIFOLD DIRICH CONDITIONS
---------------------------------------

Point Dirichlet

::

   ----------------------------DESIGN POINT MANIFOLD DIRICH CONDITIONS
   DPOINT  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>  

.. _designlinemanifolddirichconditions:

DESIGN LINE MANIFOLD DIRICH CONDITIONS
--------------------------------------

Line Dirichlet

::

   -----------------------------DESIGN LINE MANIFOLD DIRICH CONDITIONS
   DLINE  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>  

.. _designsurfmanifolddirichconditions:

DESIGN SURF MANIFOLD DIRICH CONDITIONS
--------------------------------------

Surface Dirichlet

::

   -----------------------------DESIGN SURF MANIFOLD DIRICH CONDITIONS
   DSURF  0
   E <setnumber> - NUMDOF 0 ONOFF <int vec:onoff>  VAL <real vec:val>  FUNCT <int vec [incl none]:funct>  

.. _designssiinterfacecontactlineconditions:

DESIGN SSI INTERFACE CONTACT LINE CONDITIONS
--------------------------------------------

SSI Interface Contact

::

   -----------------------DESIGN SSI INTERFACE CONTACT LINE CONDITIONS
   DLINE  0
   E <setnumber> - 0 <interface side> S2I_KINETICS_ID 0 CONTACT_CONDITION_ID 0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - interface side
     - Undefined
     - Undefined, Slave, Master

.. _designssiinterfacecontactsurfconditions:

DESIGN SSI INTERFACE CONTACT SURF CONDITIONS
--------------------------------------------

SSI Interface Contact

::

   -----------------------DESIGN SSI INTERFACE CONTACT SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0 <interface side> S2I_KINETICS_ID 0 CONTACT_CONDITION_ID 0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - interface side
     - Undefined
     - Undefined, Slave, Master

.. _designsstiinterfacemeshtyinglineconditions:

DESIGN SSTI INTERFACE MESHTYING LINE CONDITIONS
-----------------------------------------------

SSTI Interface Meshtying

::

   --------------------DESIGN SSTI INTERFACE MESHTYING LINE CONDITIONS
   DLINE  0
   E <setnumber> - 0 <interface side> S2I_KINETICS_ID 0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - interface side
     - Undefined
     - Undefined, Slave, Master

.. _designsstiinterfacemeshtyingsurfconditions:

DESIGN SSTI INTERFACE MESHTYING SURF CONDITIONS
-----------------------------------------------

SSTI Interface Meshtying

::

   --------------------DESIGN SSTI INTERFACE MESHTYING SURF CONDITIONS
   DSURF  0
   E <setnumber> - 0 <interface side> S2I_KINETICS_ID 0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - interface side
     - Undefined
     - Undefined, Slave, Master

.. _designsurfaceparticlewall:

DESIGN SURFACE PARTICLE WALL
----------------------------

Wall for particle interaction with (optional) material definition

::

   ---------------------------------------DESIGN SURFACE PARTICLE WALL
   DSURF  0
   E <setnumber> - MAT 0

.. _designaaasurfacecondition:

DESIGN AAA SURFACE CONDITION
----------------------------

AAA surface

::

   ---------------------------------------DESIGN AAA SURFACE CONDITION
   DSURF  0
   E <setnumber> - 0

.. _taylorgalerkinoutflowsurfconditions:

TAYLOR GALERKIN OUTFLOW SURF CONDITIONS
---------------------------------------

Surface Taylor Galerkin Outflow

::

   ----------------------------TAYLOR GALERKIN OUTFLOW SURF CONDITIONS
   DSURF  0
   E <setnumber> -

.. _taylorgalerkinneumanninflowsurfconditions:

TAYLOR GALERKIN NEUMANN INFLOW SURF CONDITIONS
----------------------------------------------

Surface Taylor Galerkin Neumann Inflow

::

   ---------------------TAYLOR GALERKIN NEUMANN INFLOW SURF CONDITIONS
   DSURF  0
   E <setnumber> -

.. _reinitializationtaylorgalerkinsurfconditions:

REINITIALIZATION TAYLOR GALERKIN SURF CONDITIONS
------------------------------------------------

Surface Reinitialization Taylor Galerkin

::

   -------------------REINITIALIZATION TAYLOR GALERKIN SURF CONDITIONS
   DSURF  0
   E <setnumber> -

.. _designlinelevelsetcontactcondition:

DESIGN LINE LEVEL SET CONTACT CONDITION
---------------------------------------

level-set condition for contact points

::

   ----------------------------DESIGN LINE LEVEL SET CONTACT CONDITION
   DLINE  0
   E <setnumber> -

.. _designpointlevelsetcontactcondition:

DESIGN POINT LEVEL SET CONTACT CONDITION
----------------------------------------

level-set condition for contact points

::

   ---------------------------DESIGN POINT LEVEL SET CONTACT CONDITION
   DPOINT  0
   E <setnumber> -

.. _designlinesilver-muellerconditions:

DESIGN LINE SILVER-MUELLER CONDITIONS
-------------------------------------

Absorbing-emitting line for electromagnetics

::

   ------------------------------DESIGN LINE SILVER-MUELLER CONDITIONS
   DLINE  0
   E <setnumber> - NUMDOF 0 ONOFF 0 FUNCT none VAL 0.0 

.. _designsurfsilver-muellerconditions:

DESIGN SURF SILVER-MUELLER CONDITIONS
-------------------------------------

Absorbing-emitting surface for electromagnetics

::

   ------------------------------DESIGN SURF SILVER-MUELLER CONDITIONS
   DSURF  0
   E <setnumber> - NUMDOF 0 ONOFF 0 FUNCT none VAL 0.0 

.. _designpointrigidspherepotentialchargeconditions:

DESIGN POINT RIGIDSPHERE POTENTIAL CHARGE CONDITIONS
----------------------------------------------------

Rigidsphere_Potential_Point_Charge

::

   ---------------DESIGN POINT RIGIDSPHERE POTENTIAL CHARGE CONDITIONS
   DPOINT  0
   E <setnumber> - POTLAW 0 VAL <real vec:val>  FUNCT <int vec [incl none]:funct> 

.. _designlinebeampotentialchargeconditions:

DESIGN LINE BEAM POTENTIAL CHARGE CONDITIONS
--------------------------------------------

Beam_Potential_Line_Charge_Density

::

   -----------------------DESIGN LINE BEAM POTENTIAL CHARGE CONDITIONS
   DLINE  0
   E <setnumber> - POTLAW 0 VAL <real vec:val>  FUNCT <int vec [incl none]:funct> 

.. _designlinebeamfilamentconditions:

DESIGN LINE BEAM FILAMENT CONDITIONS
------------------------------------

Beam_Line_Filament_Condition

::

   -------------------------------DESIGN LINE BEAM FILAMENT CONDITIONS
   DLINE  0
   E <setnumber> - ID 0 TYPE <Type>

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Type
     - Arbitrary
     - Arbitrary, arbitrary, Actin, actin, Collagen, |break| 
       collagen

.. _designpointpenaltycouplingconditions:

DESIGN POINT PENALTY COUPLING CONDITIONS
----------------------------------------

Couples beam nodes that lie on the same position

::

   ---------------------------DESIGN POINT PENALTY COUPLING CONDITIONS
   DPOINT  0
   E <setnumber> - POSITIONAL_PENALTY_PARAMETER 0.0 ROTATIONAL_PENALTY_PARAMETER 0.0

.. _beaminteraction/beamtobeamcontactconditions:

BEAM INTERACTION/BEAM TO BEAM CONTACT CONDITIONS
------------------------------------------------

Beam-to-beam contact conditions

::

   -------------------BEAM INTERACTION/BEAM TO BEAM CONTACT CONDITIONS
   DLINE  0
   E <setnumber> - COUPLING_ID 0

.. _beaminteraction/beamtosolidvolumemeshtyingvolume:

BEAM INTERACTION/BEAM TO SOLID VOLUME MESHTYING VOLUME
------------------------------------------------------

Beam-to-volume mesh tying conditions - volume definition

::

   -------------BEAM INTERACTION/BEAM TO SOLID VOLUME MESHTYING VOLUME
   DVOL  0
   E <setnumber> - COUPLING_ID 0

.. _beaminteraction/beamtosolidvolumemeshtyingline:

BEAM INTERACTION/BEAM TO SOLID VOLUME MESHTYING LINE
----------------------------------------------------

Beam-to-volume mesh tying conditions - line definition

::

   ---------------BEAM INTERACTION/BEAM TO SOLID VOLUME MESHTYING LINE
   DLINE  0
   E <setnumber> - COUPLING_ID 0

.. _beaminteraction/beamtosolidsurfacemeshtyingsurface:

BEAM INTERACTION/BEAM TO SOLID SURFACE MESHTYING SURFACE
--------------------------------------------------------

Beam-to-surface mesh tying conditions - surface definition

::

   -----------BEAM INTERACTION/BEAM TO SOLID SURFACE MESHTYING SURFACE
   DSURF  0
   E <setnumber> - COUPLING_ID 0

.. _beaminteraction/beamtosolidsurfacemeshtyingline:

BEAM INTERACTION/BEAM TO SOLID SURFACE MESHTYING LINE
-----------------------------------------------------

Beam-to-surface mesh tying conditions - line definition

::

   --------------BEAM INTERACTION/BEAM TO SOLID SURFACE MESHTYING LINE
   DLINE  0
   E <setnumber> - COUPLING_ID 0

.. _beaminteraction/beamtosolidsurfacecontactsurface:

BEAM INTERACTION/BEAM TO SOLID SURFACE CONTACT SURFACE
------------------------------------------------------

Beam-to-surface contact conditions - surface definition

::

   -------------BEAM INTERACTION/BEAM TO SOLID SURFACE CONTACT SURFACE
   DSURF  0
   E <setnumber> - COUPLING_ID 0

.. _beaminteraction/beamtosolidsurfacecontactline:

BEAM INTERACTION/BEAM TO SOLID SURFACE CONTACT LINE
---------------------------------------------------

Beam-to-surface contact conditions - line definition

::

   ----------------BEAM INTERACTION/BEAM TO SOLID SURFACE CONTACT LINE
   DLINE  0
   E <setnumber> - COUPLING_ID 0

.. _designlineehlmortarcouplingconditions2d:

DESIGN LINE EHL MORTAR COUPLING CONDITIONS 2D
---------------------------------------------

Line EHL Coupling

::

   ----------------------DESIGN LINE EHL MORTAR COUPLING CONDITIONS 2D
   DLINE  0
   E <setnumber> - 0 <Side> <Initialization> FrCoeffOrBound 0.0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Side
     - Master
     - Master, Slave
   * - Initialization
     - Active
     - Inactive, Active

.. _designsurfehlmortarcouplingconditions3d:

DESIGN SURF EHL MORTAR COUPLING CONDITIONS 3D
---------------------------------------------

Surface EHL Coupling

::

   ----------------------DESIGN SURF EHL MORTAR COUPLING CONDITIONS 3D
   DSURF  0
   E <setnumber> - 0 <Side> <Initialization> FrCoeffOrBound 0.0

**String options:**

.. list-table::
   :header-rows: 1
   :widths: 33,33,50

   * - Parameter
     - Default
     - Admissible values
   * - Side
     - Master
     - Master, Slave
   * - Initialization
     - Active
     - Inactive, Active

.. _designoxygenpartialpressurecalculationlineconditions:

DESIGN OXYGEN PARTIAL PRESSURE CALCULATION LINE CONDITIONS
----------------------------------------------------------

PoroMultiphaseScatra Oxygen Partial Pressure Calculation line condition

::

   ---------DESIGN OXYGEN PARTIAL PRESSURE CALCULATION LINE CONDITIONS
   DLINE  0
   E <setnumber> - SCALARID 0 n 0.0 Pb50 0.0 CaO2_max 0.0 alpha_bl_eff 0.0 \ 
       rho_oxy 0.0 rho_bl 0.0

.. _designoxygenpartialpressurecalculationsurfconditions:

DESIGN OXYGEN PARTIAL PRESSURE CALCULATION SURF CONDITIONS
----------------------------------------------------------

PoroMultiphaseScatra Oxygen Partial Pressure Calculation surface condition

::

   ---------DESIGN OXYGEN PARTIAL PRESSURE CALCULATION SURF CONDITIONS
   DSURF  0
   E <setnumber> - SCALARID 0 n 0.0 Pb50 0.0 CaO2_max 0.0 alpha_bl_eff 0.0 \ 
       rho_oxy 0.0 rho_bl 0.0

.. _designoxygenpartialpressurecalculationvolconditions:

DESIGN OXYGEN PARTIAL PRESSURE CALCULATION VOL CONDITIONS
---------------------------------------------------------

PoroMultiphaseScatra Oxygen Partial Pressure Calculation volume condition

::

   ----------DESIGN OXYGEN PARTIAL PRESSURE CALCULATION VOL CONDITIONS
   DVOL  0
   E <setnumber> - SCALARID 0 n 0.0 Pb50 0.0 CaO2_max 0.0 alpha_bl_eff 0.0 \ 
       rho_oxy 0.0 rho_bl 0.0

.. _microscaleconditions:

MICROSCALE CONDITIONS
---------------------

Microscale Boundary

::

   ----------------------------------------------MICROSCALE CONDITIONS
   DSURF  0
   E <setnumber> -

.. _designvolstclayer:

DESIGN VOL STC LAYER
--------------------

Layer for Multilayered STC

::

   -----------------------------------------------DESIGN VOL STC LAYER
   DVOL  0
   E <setnumber> - 0

