.. _preprocessing:

Preprocessing
---------------

BACI reads the mesh, boundary conditions, materials and simulation parameters from an ASCII file in a proprietary format, which usually has the suffix ``.dat``, but this suffix is not necessary, it can be anything. 

There are not so many means to create a valid input file. At this point, we know of the following
different ways to create input file. In general, you'll have two options:

#. Either you create the input file in ``BACI``'s native format directly,

#. or you create an input file in a general binary format for finite element information, ``Exodus II``, develeloped at `Sandia national lab <https://www.sandia.gov/files/cubit/15.8/help_manual/WebHelp/finite_element_model/exodus/exodus2_file_specification.htm>`_.
This can be converted into ``BACI`` s native format by an internal converter, :ref:`pre_exodus <pre_exodus>`.

Since the conversion from the Exodus II format is the most versatile way to generate a BACI input file, this method is explained first.

.. _pre_exodus:

Exodus II to BACI file conversion
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The main procedure to generate a valid BACI input file is from a binary mesh file in Exodus II format, together with two additional files:

#. for the global system parameters (solver, material, step information, etc.), called the *headerfile*, and 

#. a file for the element types and boundary conditions, called the *bcfile*. 

With these three files at hand, simply call the program ``pre_exodus``, which is created together with baci executable, if ``make full`` has been invoked. The program can be compiled solely by ``make pre_exodus``.

::

   $> pre_exodus --exo=<exodusfile> --bc=<bcfile> --head=<headerfile> --dat=<baciinput> \
               [ --gensosh=<thickness> [ --numlayer=<nlayer> ] ]   \
               [ --d2 | --d3  ]           \
               [ --quadtri  ]             \
               [ --gmesh=<startelement> ]

*Optional parameters*

The optional parameter ``--quadtri`` reads the exodus file and converts all quad elements in two triangular elements. It does not write a dat file, but writes a new exodus file instead named ``tri_<problemname>.e``. Remark: It does not modify 3D elements.

The option to generate a 3D mesh from a shell surface, ``pre_exodus --exo=<exodusfile> --gensosh=<thickness> --numlayer=<nlayer>``, does not create a BACI input file either, but it creates an exodus file containing the solid model named ``extr_<exodusfile>``.

.. warning::

   It seems that the gensosh feature does not work properly. Neither Cubit nor Paraview can read a file created this way.

.. _createbaciinput:

Other ways to create a BACI input directly
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _abaqus:

**ABAQUS**

There is an in-house Python module ``abaqus_meshio`` for the conversion from inp to dat file. Since the inp can be generated using Cubit or Abaqus, this submodule can be used in conjunction with both software. The usage of this submodule starts firstly by importing it providing the path where it is located.

.. code-block:: python

   import sys

   abaqus_meshio_path = "path_to_abaqus_meshio"
   sys.path.append(abaqus_meshio_path)

Subsequently, the inp shall be read using the command

.. code-block:: python

   model = abaqus_meshio.read("path_to_inp.dat")

Unlike ``meshio.read``, the command ``abaqus_meshio.read`` will return a model, which is instance of ``BModel``, where:

- ``model.rootAssembly.instances[instance_name].mesh`` is a ``BMesh. ``BMesh`` is a subclass of ``meshio.Mesh`` with additional attributes sections (for material assignment) and surfaces (for distributed load).
- ``model`` has attributes materials (from MATERIAL), parts (from PART/END PART) and steps (from STEP)
- ``model.parts[part_name].mesh`` is again a ``BMesh``, ``model.rootAssembly.instances[instance_name].mesh`` is a transformation of this mesh.

``BModel`` is designed to mimick the way Abaqus systematically stores its data. To access the original ``meshio.Mesh`` one has to use ``model.parts[part_name].mesh``.

Proving that the information from inp is properly stored, the transformation to dat file is done by a simple command

.. code-block:: python

   baciio = abaqus_meshio.Inp2Baci(model, [params_step_1])
   baciio.write("prefix")

If the inp has many steps defined by STEP/END STEP keywords, the list of parameters for each step has to be provided, e.g. ``[params_step_1, params_step_2, ...]``. Default parameters for a structural analysis can be obtained using

.. code-block:: python

   params_step_1 = abaqus_meshio.GenerateDefaultParams()

Alternatively, one may run a python script called ``CAEabq2baci.py`` to convert an ABAQUS input file to a BACI input file (available on request). This script uses ABAQUS/CAE commands, that is, an abaqus license is necessary to run this script.



.. _gid:

**GiD**

A BACI input file can be generated using the GiD problemtype baci.gid.

Generating Exodus II files
~~~~~~~~~~~~~~~~~~~~~~~~~~

Even though the generation of Exodus II files might be out of scope of a BACI manual, 
users are informed on how to generate these files conveniently, so options are given in the following:

.. _cubit:

**CUBIT**


CUBIT `<http://cubit.sandia.gov/>`_ is a powerful pre- postprocessing
tool. (The commercial version of the software was called *Trelis*, 
but has been renamed into CUBIT now as well, so we may stick to the name CUBIT).

CUBIT can create EXODUS-II files which can be converted into a
valid BACI inpufile using the pre_exodus filter, so the preprocessing is a two step process:

#. Cubit
   - create geometry, mesh, and necessary node sets
   - export to exodus file format (\*.e)
#. :ref:`pre_exodus <pre_exodus>`
   - define appropriate boundary conditions and element types
   - convert into a baci \*.dat file.

Note that

- it is not necessary to define boundary conditions in Cubit, since they are not converted
  to the dat file later on.

- you should only define node sets, but not sidesets (surface sets). The node sets are
  converted into surface sets if the surface definition is given in the boundary condition
  control file (<problem>.bc) anyway.

.. ifconfig:: institution in ("lnm", )

    CUBIT is installed on Gauss. Its folder is ``/lnm/programs/cubit12/``
    You can start CUBIT by typing ``/lnm/programs/cubit12/cubit`` on any LNM
    machine. (It may be convenient to place a link to this executable in
    your ``/̃bin`` directory by doing
    ``ln -s /lnm/cubit12/cubit /̃bin/cubit``, then typing ``gid`` will do the
    same. Another optin is to create an alias in your ``/̃.bashrc`` file)
    Once started go to *Help* :math:`\rightarrow` *Cubit Tutorials* for an
    introduction or go to the :ref:`Fluid tutorial <fluidtutorial>`.

.. ifconfig:: institution in ("hereon", )

    CUBIT will be available from the Software Kiosk


.. ifconfig:: institution in ("imcs", )

    Don't know

.. _abaquscae:


**Other Software**

Geometry as well as element and node sets can be created in any finite element preprocessor.
However, the preprocessor should be capable of exporting a file format, which can be converted
by the python toolset meshio (see <https://pypi.org/project/meshio/>) into an exodus file, with
which the input can be converted into a BACI .dat file.

Also, the exported input file can probably be imported in Cubit, then further edited and
eventually exported as an exodus (.e) file.

So the steps are

#. Create finite element model and sets in your favorite preprocessor

#. Export to some format, like Exodus II or the Gmesh format ``.msh`` file.

#. **Optional:** Read in the model to Cubit for further editing

#. **Optional:** If you are not able to write in Exodus II format, 
   use the python module meshio (packed in pip) to convert the mesh to an exodus (.e) file
   (<https://pypi.org/project/meshio/>)

#. Run ``pre_exodus`` from your ``BACI`` build to convert the data (see above).


Modify ``BACI`` input files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``BACI`` input files are text files so you can modify them using your
favorite text editor. You can see all possible parameters and keywords in the 
:ref:`reference part <inputparameterreference>`.

However, sometimes you might want some more
modifications (e.g. modifying many nodes coordinates) that might be better
done by a script. And indeed there is a python script that can help you
edit input files.


