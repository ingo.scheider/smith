old_Structural dynamics
=======================

Input parameters
----------------

::

   ------------------------------------------------STRUCTURAL DYNAMIC

The most important parameter is :ref:`DYNAMICTYP <structuraldynamic_dynamictyp>`, 
which controls the type of time integration by one of the following keywords and their explanations:

+----------------------+----------------------+
| :math:`string`       | meaning              |
+======================+======================+
| ``Static``           | static analysis      |
|                      |                      |
+----------------------+----------------------+
| ``GenAlpha``         | generalised          |
|                      | energy-momentum      |
|                      | method               |
+----------------------+----------------------+
| ``OneStepTheta``     | one-step-theta       |
|                      | method               |
+----------------------+----------------------+
| ``GEMM``             | generalised          |
|                      | energy-momentum      |
|                      | method               |
+----------------------+----------------------+
| ``AdamsBashforth2``  | Adams-Bashforthn 2nd |
|                      | order                |
+----------------------+----------------------+
| ``CentrDiff``        |                      |
+----------------------+----------------------+
| ``EulerMaruyama``    |                      |
+----------------------+----------------------+
| ``EulerImpStoch``    |                      |
+----------------------+----------------------+

| :ref:`RESULTSEVRY<structuraldynamic_resultsevry>` :math:`int`
| save displacements and contact forces every :math:`int` steps

| :ref:`RESTARTEVRY<structuraldynamic_restartevry>` :math:`int`
| write restart information every :math:`int` steps

The timing is given by three main parameters: 
``TIMESTEP`` (time step size),
``NUMSTEP`` (maximum number of steps), and
``MAXTIME`` the end time. The simulation ends whatever is reached first: 
the maximum time, or the maximum number of steps.

Convergence difficulties:

If the solution does not converge within ``MAXITER`` iterations, 
one can define the behavior for the step with the parameter ``DIVERCONT``:

- ``stop``: stop the simulation 
- ``continue``: Simply ignore the missing convergence and continue 
- ``repeat_step``: Repeat the step (I don't know the benefit of doing a failed simulation again)
- ``halve_step``: Reduce the step size of the current increment by 50%.
- ``adapt_step``: As halve_step reducing the step size by 50%, but checking against the maximum number of cutbacks, 
  which can be given in ``MAXDIVCONREFINEMENTLEVEL`` (default: 10).
  Also the step size may increase by 2 if convergence is achieved four times successively.
- ``rand_adapt_step``: Changing the time step size by a factor between 0.51 and 1.99, chosen by random number generator.
  Step size change is alternating between values larger and smaller than 1.0 in consecutive time steps.
- ``repeat_simulation``: (what the name says - I don't know what it's good for...)
- ``adapat_penaltycontact``: (is not yet implemented for the new time integration scheme)
- ``adapt_3D0Dptc_ele_err``: (not implemented in the new time integration scheme)



| ``DAMPING`` :math:`string`

+--------------+---------------------------------------+
|:math:`string`|meaning                                |
+==============+=======================================+
|``No``        |no global damping                      |
|``Yes``       |Rayleigh-damping                       |
|``Rayleigh``  |Rayleigh-damping                       |
|``Material``  |material-based damping on element-level|
+--------------+---------------------------------------+

| ``M_DAMP`` :math:`real`
| Rayleigh-coefficient for Rayleigh damping proportional to mass matrix
  :math:`\text{M\_DAMP} \times M`

| ``K_DAMP`` :math:`real`
| Rayleigh-coefficient for Rayleigh damping proportional to
  initial/reference stiffness matrix :math:`\text{K\_DAMP} \times K`

| ``NLNSOL`` :math:`string`
| type of non-linear solution technique chosen, i.e.

+----------------------+----------------------+----------------------+
| :math:`string`       | meaning              | valid with           |
+======================+======================+======================+
| ``fullnewton``       | full Newton-Raphson  | BACI                 |
|                      | iteration            |                      |
|                      | (tangential)         |                      |
+----------------------+----------------------+----------------------+
| ``modnewton``        | modified             | BACI                 |
|                      | Newton-Raphson       |                      |
|                      | iteration            |                      |
|                      | (secantial)          |                      |
+----------------------+----------------------+----------------------+
| ``matfreenewton``    | matrix-free          | BACI                 |
|                      | Newton-Raphson       |                      |
|                      | iteration            |                      |
+----------------------+----------------------+----------------------+
| ``nlncg``            | non-linear           | BACI                 |
|                      | conjugate-gradient   |                      |
|                      | method               |                      |
+----------------------+----------------------+----------------------+
| ``ptc``              | ???                  | BACI                 |
|                      |                      | ``StruGenAlpha``     |
+----------------------+----------------------+----------------------+
| ``lsnewton``         | line-search Newton   | BACI                 |
|                      |                      | ``StruGenAlpha``     |
+----------------------+----------------------+----------------------+
| ``newtonlinuzawa``   | linearised           | BACI                 |
|                      | Newton-Uzawa         |                      |
+----------------------+----------------------+----------------------+
| ``augmentedlagrange``| non-linear           | BACI                 |
|                      | Newton-Uzawa         |                      |
+----------------------+----------------------+----------------------+

| ``TOLDISP`` :math:`real`
| tolerance in the displacement norm for the Newton iteration

| ``MAXITER`` :math:`int`
| maximum number of iterations allowed for newton iteration before
  failure


BACI common parameters
~~~~~~~~~~~~~~~~~~~~~~

| ``RESEVRYERGY`` :math:`int`
| write system energies every requested :math:`int`\ th step

| ``TOLRES`` :math:`real`
| tolerance in the residual forces norm for the Newton iteration
  (generalised-alpha)

| ``TOLCONSTR`` :math:`real`
| tolerance in the constraint error norm for the Newton iteration

| ``CONV_CHECK`` :math:`string`
| type of convergence check in non-linear solution technique, i.e. with
  respect to ``TOLDISP`` and ``TOLRES`` we have the following
  control possibilities

+-------------------------+-------------------------------+---+
| :math:`string`          | meaning                       |   |
+=========================+===============================+===+
| ``AbsRes_And_AbsDis``   | absolute norm of residual     |   |
|                         | forces AND absolute norm of   |   |
|                         | iterative displacements       |   |
|                         | increments                    |   |
+-------------------------+-------------------------------+---+
|                         |                               |   |
+-------------------------+-------------------------------+---+
| ``AbsRes_Or_AbsDis``    | absolute norm of residual     |   |
|                         | forces OR absolute norm of    |   |
|                         | iterative displacements       |   |
|                         | increments                    |   |
+-------------------------+-------------------------------+---+
| ``RelRes_And_AbsDis``   | relative norm of residual     |   |
|                         | forces AND absolute norm of   |   |
|                         | iterative displacements       |   |
|                         | increments                    |   |
+-------------------------+-------------------------------+---+
| ``RelRes_Or_AbsDis``    | relative norm of residual     |   |
|                         | forces OR absolute norm of    |   |
|                         | iterative displacements       |   |
|                         | increments                    |   |
+-------------------------+-------------------------------+---+
| ``RelRes_And_RelDis``   | relative norm of residual     |   |
|                         | forces AND relative norm of   |   |
|                         | iterative displacements       |   |
|                         | increments                    |   |
+-------------------------+-------------------------------+---+
| ``RelRes_Or_RelDis``    | relative norm of residual     |   |
|                         | forces OR relative norm of    |   |
|                         | iterative displacements       |   |
|                         | increments                    |   |
+-------------------------+-------------------------------+---+

The relative norms are calculated by dividing the absolute norm by the
norm of the overall displacements and the maximum norm of internal,
external, inertial (and possibly viscous) forces of the last time step,
respectively.

| ``ADAPTCONV`` ( ``No`` :math:`|` ``Yes`` )
| Switch on adaptive control of linear solver tolerance for nonlinear
  solution

| ``ADAPTCONV_BETTER`` :math:`float`
| The linear solver shall be this much better than the current nonlinear
  residual in the nonlinear convergence limit

| ``PREDICT`` :math:`string`
| set predictor for non-linear solution technique

+----------------------+----------------------------------------------+
| ``Vague``            | undetermined                                 |
+======================+==============================================+
| ``ConstDis``         | constant displacements, consistent           |
|                      | velocities and accelerations                 |
+----------------------+----------------------------------------------+
| ``ConstDisVelAcc``   | consta displacements, velocties and          |
|                      | accelerations                                |
+----------------------+----------------------------------------------+

| ``UZAWAPARAM``   :math:`float`
| Parameter for Uzawa algorithm dealing with lagrange multipliers

| ``UZAWATOL``   :math:`float`
| Tolerance for iterative solve with Uzawa algorithm

| ``UZAWAMAXITER``   :math:`int`
| maximum number of iterations allowed for uzawa algorithm before
  failure going to next newton step

| ``UZAWAALGO``   ( ``iterative``:math:`|` ``direct``   )
| Uzawa algorithm

BACI ``StruGenAlpha`` parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| ``BETA`` :math:`real`
| :math:`\beta` factor of generalized-:math:`\alpha`

| ``GAMMA`` :math:`real`
| :math:`\gamma` factor of generalized-:math:`\alpha`

| ``ALPHA_M`` :math:`real`
| :math:`\alpha_m` factor of generalized-:math:`\alpha`

| ``ALPHA_F`` :math:`real`
| :math:`\alpha_f` factor of generalized-:math:`\alpha`

BACI ``STR::TimInt`` parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

   ---------------------------------------STRUCTURAL DYNAMIC/GENALPHA

| ``GENAVG`` ( ``ImrLike`` :math:`|` ``TrLike`` )
| mid-average type of internal forces

| ``BETA`` :math:`real`
| Generalised-alpha coefficient in :math:`(0,1/2]`

| ``GAMMA`` :math:`real`
| Generalised-alpha factor in :math:`(0,1]`

| ``ALPHA_M`` :math:`real`
| Generalised-alpha factor in :math:`[0,1)`

| ``ALPHA_F`` :math:`real`
| Generalised-alpha factor in :math:`[0,1)`

::

   -----------------------------------STRUCTURAL DYNAMIC/ONESTEPTHETA

``THETA``

One-step-theta coefficient :math:`\theta` in :math:`(0,1]`

::

   -------------------------------------------STRUCTURAL DYNAMIC/GEMM

| ``ALPHA_M`` :math:`real`
| Generalised-alpha coefficient :math:`\alpha_m` in :math:`[0,1)`

| ``ALPHA_F`` :math:`real`
| Generalised-alpha coefficient :math:`\alpha_f` in :math:`[0,1)`

| ``XI`` :math:`real`
| Generalisation damping coefficient :math:`\xi` in :math:`[0,1)`

BACI ``STR::TimAda`` parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

   ---------------------------------STRUCTURAL DYNAMIC/TIMEADAPTIVITY

| ``KIND`` ( ``None`` :math:`|` ``ZienkiewiczXie`` :math:`|` ``AdamsBashforth2`` )
| Method for time step size adapivity; ``None`` means no adapivity in time.

| ``OUTSYSPERIOD`` :math:`real`
| Write system vectors (displacements, velocities, etc) every given
  period of time; if :math:`0` writing is off

| ``OUTSTRPERIOD`` :math:`real`
| Write stress/strain every given period of time; if :math:`0` writing
  is off

| ``OUTENEPERIOD`` :math:`real`
| Write energy every given period of time; if :math:`0` writing is off

| ``OUTRESTPERIOD`` :math:`real`
| Write restart data every given period of time; if :math:`0` writing is
  off

| ``OUTSIZEEVERY`` :math:`int`
| Write step size every given time step

| ``STEPSIZEMAX`` :math:`real`
| Limit maximally permitted time step size (:math:`>0`)

| ``STEPSIZEMIN`` :math:`real`
| Limit minimally allowed time step size (:math:`>0`)

| ``SIZERATIOMAX`` :math:`real`
| Limit maximally permitted change of time step size compared to
  previous size, important for multi-step schemes (:math:`>0`)

| ``SIZERATIOMIN`` :math:`real`
| Limit minimally permitted change of time step size compared to
  previous size, important for multi-step schemes (:math:`>0`)

| ``SIZERATIOSCALE`` :math:`real`
| This is a safety factor to scale theretical optimal step size, should
  be lower than 1 and must be larger than :math:`0`

| ``LOCERRNORM`` ( ``L1`` :math:`|` ``L2`` :math:`|` ``Rms``
  :math:`|` ``Inf`` )
| Vector norm to treat error vector with

| ``LOCERRTOL`` :math:`real`
| Target local error tolerance (:math:`>0`)

| ``ADAPTSTEPMAX`` :math:`int`
| Limit maximally allowed step size reduction attempts (:math:`>0`)

A few preliminaries
-------------------

.. _`strdyn:sec:accuracy`:

Order of accuracy
~~~~~~~~~~~~~~~~~

The difference of an exact solution compared to an approximate solution
is called *global error* :math:`\boldsymbol{g}` (Hairer et al.
[strdyn:hairer87]_, Deuflhard et al. 
[strdyn:deuflhard94]_). The global error at time
:math:`t_{n+1}` can be written as

.. math::

   \label{strdyn:eq:global-error}
     \boldsymbol{g}_{n+1} = \boldsymbol{y}(t_{n+1}) - \boldsymbol{y}_{n+1}
     \text{.}

Although techniques exist to estimate the global error (Hairer et al.
[strdyn:hairer87]_, [strdyn:hairer91]_), it is easier to
deal with the *local discretisation error* (cf. Hairer et al.
[strdyn:hairer87]_, Deuflhard et al.
[strdyn:deuflhard94]_; it is abbreviated with LDE). The
global error can be split in a local part and a propagation part. The
local part or the local discretisation error, :math:`\boldsymbol{l}`,
contains the error produced in the last integration step
:math:`t_n \to t_{n+1}`. The propagation part,
:math:`\bar{\boldsymbol{g}}`, describes the errors accumulated previous
steps. As a consequence, the global error coincides with the local
discretisation error for the very first integration step
:math:`\boldsymbol{g}_1 = \boldsymbol{l}_1`, because of the shared
initial conditions :math:`\boldsymbol{y}(t_0) = \boldsymbol{y}_0`

.. math::

   \label{strdyn:eq:adap:ge-lde-prop}
   \begin{split}
     \boldsymbol{g}_{n+1}
     & = \boldsymbol{y}(t_{n+1}) - \boldsymbol{y}_{n+1}
   \\
     & = \boldsymbol{\Phi}_{n+1,n} \boldsymbol{y}(t_n) - \boldsymbol{\Psi}_{n+1,n} \boldsymbol{y}_n
   \\
     & = \underbrace{\boldsymbol{\Phi}_{n+1,n} \boldsymbol{y}(t_n) -
     \boldsymbol{\Psi}_{n+1,n} \boldsymbol{y}(t_n)}_{\text{LDE}}
     \,+\, \underbrace{\boldsymbol{\Psi}_{n+1,n} \boldsymbol{y}(t_n) -
     \boldsymbol{\Psi}_{n+1,n} \boldsymbol{y}_n}_{\text{propagation}}
   \\
     & = \boldsymbol{l}_{n+1} \,+\, \bar{\boldsymbol{g}}_{n+1,0}
     \text{.}
   \end{split}

The term :math:`\boldsymbol{\Psi}_{n+1,n} \boldsymbol{y}(t_n)` can be
understood as the time integration method but applied to exact rather
than approximate values. A Taylor expansion of the local discretisation
error leads to an expression in :math:`\Delta t_n^{p+1}`, where
:math:`p` is the order of accuracy of the scheme:

.. math::

   \label{strdyn:eq:lde}
     \boldsymbol{l}_{n+1} 
     = \mathcal{O}(\Delta t_n^{p+1})
     = \boldsymbol{c}(t_n)\, \Delta t_n^{p+1} + \mathcal{O}(\Delta t_n^{p+2})
     \quad\text{with}\quad
     \boldsymbol{c}(t_n) \neq \boldsymbol{0}
     \text{.}

The vector function :math:`\boldsymbol{c}(t_n)` depends on the time integration scheme. 
In the case of Runge-Kutta methods
:math:`\boldsymbol{c}` has the form
:math:`\boldsymbol{c}(t_n) = K \frac{\mathrm{d}^{p+1} \boldsymbol{y}(t_n)}{\mathrm{d}
t^{p+1}}` with a constant :math:`K \neq 0`.

If the approximate solution converges to the exact solution for
:math:`\Delta t_n\to0`, the scheme is called *consistent*. Consistency
requires :math:`p\geq1`. This is because the global error is
:math:`\mathcal{O}(\Delta t_n^p)`. The reduced power of the global error
stems from the propagation of the local errors in time.

Generalised-alpha
-----------------

Works generally.

Theory
~~~~~~

Newmark’s method
^^^^^^^^^^^^^^^^

Newmark’s method [strdyn:newmark59]_ is a family of
schemes with two parameters :math:`\gamma\in[0,1]` and
:math:`\beta\in[0,\frac{1}{2}]`. The basic assumption of Newmark’s
method is a linear approximation of the acceleration
:math:`\boldsymbol{A}` as shown in
Figure `[strdyn:fig:lindyn-newmark] <#strdyn:fig:lindyn-newmark>`__

.. math:: \boldsymbol{A}(\tau) = \boldsymbol{A}_n + \frac{\boldsymbol{A}_{n+1} - \boldsymbol{A}_n}{\Delta t}\tau

The integration parameter :math:`\tau` is defined on the interval
:math:`[t_n,t_n+1]` as :math:`\tau \in[0,\Delta t]`. Now, two parameters
are introduced to control the behavior of this approximation

.. math::

   \begin{aligned}
     \boldsymbol{A}^\gamma(\tau) &=& \boldsymbol{A}_n + 2\gamma \frac{\boldsymbol{A}_{n+1} - \boldsymbol{A}_n}{\Delta t}\tau\\
     \boldsymbol{A}^\beta(\tau) &=& \boldsymbol{A}_n + 6\beta  \frac{\boldsymbol{A}_{n+1} - \boldsymbol{A}_n}{\Delta t}\tau
   \end{aligned}

If :math:`\gamma=\frac{1}{2}` and :math:`\beta=\frac{1}{6}` are choosen,
a linear acceleration scheme is obtaind. The
:math:`\gamma`-parameterized acceleration
:math:`\boldsymbol{A}^\gamma(\tau)` is integrated once over
:math:`\tau`, which yields

.. math:: \boldsymbol{V}(\tau) = \boldsymbol{A}_n \tau + \frac{2\gamma}{2}\frac{\boldsymbol{A}_{n+1} - \boldsymbol{A}_n}{\Delta t}\tau^2 + c

The integration constant :math:`c` is defined by inserting the known
boundary condition of the integral
:math:`\boldsymbol{V}(\tau=0) = \boldsymbol{V}_n`, which gives

.. math:: \boldsymbol{V}(\tau) = \boldsymbol{V}_n + \boldsymbol{A}_n \tau + \gamma\frac{\boldsymbol{A}_{n+1} - \boldsymbol{A}_n}{\Delta t}\tau^2\text{.}

The new timesteps velocity :math:`\boldsymbol{V}_{n+1}` is therefore
obtained at :math:`\boldsymbol{V}(\tau = \Delta t)`

.. math:: \boldsymbol{V}_{n+1} = \boldsymbol{V}_n + (1-\gamma)\Delta t\boldsymbol{A}_n  + \gamma\Delta t\boldsymbol{A}_{n+1}\text{.}

Likewise, the :math:`\beta`-parameterized acceleration
:math:`\boldsymbol{A}^\beta(\tau)` is integrated to obtain the velocity

.. math:: \boldsymbol{V}(\tau) = \boldsymbol{V}_n + \boldsymbol{A}_n \tau + \frac{6\beta}{2}\frac{\boldsymbol{A}_{n+1} - \boldsymbol{A}_n}{\Delta t}\tau^2

To obtain the displacement approximation, we integrate again over
:math:`\tau` and yield

.. math:: \boldsymbol{D}(\tau) = \boldsymbol{V}_n \tau + \frac{1}{2}\boldsymbol{A}_n \tau^2 + \frac{6\beta}{6}\frac{\boldsymbol{A}_{n+1} - \boldsymbol{A}_n}{\Delta t}\tau^3 + C

Inserting the boundary condition
:math:`\boldsymbol{D}(\tau=0) = \boldsymbol{D}_n`, we get the
displacement
:math:`\boldsymbol{D}_{n+1} = \boldsymbol{D}(\tau = \Delta t)` at the
end of the time interval:

.. math:: \boldsymbol{D}_{n+1} = \boldsymbol{D}_n + \Delta t\boldsymbol{V}_n  + (\frac{1}{2}-\beta)\Delta t^2\boldsymbol{A}_n + \beta\Delta t^2\boldsymbol{A}_{n+1}\text{.}

Now we can express the new time steps velocity and acceleration solely
from old time steps values and the new displacement as

.. math::

   \begin{aligned}
     \boldsymbol{A}_{n+1}
     &=& \frac{1}{\beta\Delta t^2} \big( \boldsymbol{D}_{n+1} - \boldsymbol{D}_n \big)
     - \frac{1}{\beta \Delta t} \boldsymbol{V}_n
     - \frac{1-2\beta}{2\beta} \boldsymbol{A}_n\text{,}\\
       \boldsymbol{V}_{n+1}
     &=& \boldsymbol{V}_{n} + \gamma\Delta t\boldsymbol{A}_{n+1} + (1-\gamma)\Delta t\boldsymbol{A}_n\text{.}
   \end{aligned}

The final pair of equations can be rewritten such that (with
:math:`\beta\in[0,\frac{1}{2}]`):

.. math::

   \label{strdyn:eq:newmark}
   \begin{aligned}
   & \left\{\begin{array}{rcl}
     \dfrac{\boldsymbol{D}_{n+1} - \boldsymbol{D}_n}{\Delta t}
     & =
     & \boldsymbol{V}_n + \frac{\Delta t}{2} \big(
       2\beta \boldsymbol{A}_{n+1} + (1-2\beta) \boldsymbol{A}_n
       \big)
   \\
     \dfrac{\boldsymbol{V}_{n+1} - \boldsymbol{V}_n}{\Delta t}
     & =
     & \gamma \boldsymbol{A}_{n+1} + (1-\gamma)\boldsymbol{A}_n
   \end{array}\right.
   && \text{with}
   &&\begin{array}{l}
     \beta\in[0,\frac{1}{2}]
   \\
     \gamma\in[0,1]
   \end{array}
   \end{aligned}

Here, we abbreviated the unknown accelerations at :math:`t_{n+1}` with
with
:math:`\boldsymbol{A}_{n+1} = \boldsymbol{M}^{-1} \big( -\boldsymbol{C} \boldsymbol{V}_{n+1} -
\boldsymbol{F}_{\text{int};n+1} + \boldsymbol{F}_{\text{ext};n+1}) \big)`.

This temporal discretisation leads to a fully discretised set of
equations of motion:

.. math::

   \boldsymbol{M} \boldsymbol{A}_{n+1}
     + \boldsymbol{C} \boldsymbol{V}_{n+1}
     + \boldsymbol{F}_{\text{int}}(\boldsymbol{D}_{n+1})
     = \boldsymbol{F}_{\text{ext}}(t_{n+1})
     \text{.}

This completely discretised equation of motion is primarily an
:math:`\mathit{ndof}`-dimensional system of nonlinear equations in the
unknown displacements :math:`\boldsymbol{D}_{n+1}`. This statements can
be clarified by writing Newmark’s method such that the velocity and
acceleration at :math:`t_{n+1}` are given depending on the displacements
:math:`\boldsymbol{D}_{n+1}`:

.. math::

   \begin{aligned}
   \label{strdyn:eq:newmark-velnew}
      \boldsymbol{V}_{n+1}(\boldsymbol{D}_{n+1})
   &  = \frac{\gamma}{\beta\, \Delta t} \big( \boldsymbol{D}_{n+1} - \boldsymbol{D}_n \big)
      - \frac{\gamma-\beta}{\beta} \boldsymbol{V}_{n}
      - \frac{\gamma-2\beta}{2\beta}\Delta t\boldsymbol{A}_n
      \text{,}
   \\ \label{strdyn:eq:newmark-accnew}
      \boldsymbol{A}_{n+1}(\boldsymbol{D}_{n+1})
   &  = \frac{1}{\beta\, \Delta t^2} \big( \boldsymbol{D}_{n+1} - \boldsymbol{D}_n \big)
      - \frac{1}{\beta\,\Delta t} \boldsymbol{V}_{n}
      - \frac{1-2\beta}{2\beta} \boldsymbol{A}_n
      \text{.}
   \end{aligned}

Generalised-alpha method
^^^^^^^^^^^^^^^^^^^^^^^^

The key idea behind the generalised-alpha method
[strdyn:chung95]_ is a modification of the time point
at which the discretised equations of motion is evaluated. Newmark’s
method searches for equilibrium at the end of the current time step
:math:`[t_n,t_{n+1}]`, i.e.at the time :math:`t_{n+1}`. The
generalised-alpha method shifts this evaluation point to generalised
mid-points :math:`t_{n+1-\alpha_\text{f}}` and
:math:`t_{n+1-\alpha_\text{m}}`, respectively. The non-linear equation
of motion becomes at the generalised mid-point

  .. math::

     \boldsymbol{M} \boldsymbol{A}_{n+1-\alpha_\text{m}}
       + \boldsymbol{C} \boldsymbol{V}_{n+1-\alpha_\text{f}}
       + \boldsymbol{F}_{\text{int};n+1-\alpha_\text{f}} %%\vct{F}_{\Int}(\vct{D}_{n+1-\alpha_\text{f}})
       = \boldsymbol{F}_{\text{ext};n+1-\alpha_\text{f}}

The mid accelerations, velocities, displacements and external forces
are defined as linear combinations of the corresponding start and end
vector:

  .. math::

     \label{strdyn:eq:genalpha-middef}
     \begin{aligned}
     &   \boldsymbol{A}_{n+1-\alpha_\text{m}}
         := \big( 1- \alpha_\text{m} \big) \boldsymbol{A}_{n+1}
         + \alpha_\text{m} \boldsymbol{A}_n
     && \} \quad \alpha_\text{m} \in[0,1]
     \\
     &   \boldsymbol{V}_{n+1-\alpha_\text{f}}
         := \big( 1- \alpha_\text{f} \big) \boldsymbol{V}_{n+1}
         + \alpha_\text{f} \boldsymbol{V}_n
     &&  \multirow{3}{*}{\text{$\Bigg\} \quad \alpha_\text{f} \in[0,1]$}}
     \\
     &   \boldsymbol{D}_{n+1-\alpha_\text{f}}
         := \big( 1- \alpha_\text{f} \big) \boldsymbol{D}_{n+1}
         + \alpha_\text{f} \boldsymbol{D}_n
     &&
     \\
     &   \boldsymbol{F}_{\text{ext};n+1-\alpha_\text{f}}
         := \big( 1- \alpha_\text{f} \big) \boldsymbol{F}_{\text{ext};n+1}
         + \alpha_\text{f} \boldsymbol{F}_{\text{ext};n}
     &&
     \end{aligned}

  with the parameters :math:`\alpha_\text{m},\alpha_\text{f}\in[0,1]`.
  There two possibilties for the internal mid-forces
  :math:`\boldsymbol{F}_{\text{int},\text{mid}}`. Either they are
  defined as well by a linear combination (which we call ‘TR-like’) or
  by inserting mid-displacements (which we call ‘IMR-like’), i.e.

  .. math::

     \begin{aligned}
       \text{TR-like} & \quad \boldsymbol{F}_{\text{int};n+1-\alpha_\text{f}}
         := \big( 1- \alpha_\text{f} \big) \boldsymbol{F}_{\text{int}}(\boldsymbol{D}_{n+1})
         + \alpha_\text{f} \boldsymbol{F}_{\text{int}}(\boldsymbol{D}_{n})
     \\
       \text{IMR-like} & \quad \boldsymbol{F}_ {\text{int};n+1-\alpha_\text{f}} :=  \boldsymbol{F}_{\text{int}}(\boldsymbol{D}_{n+1-\alpha_\text{f}})
     \end{aligned}

The end-point accelerations and velocities, i.e.
:math:`\boldsymbol{A}_{n+1}` and :math:`\boldsymbol{V}_{n+1}`, are
related linearly to the end-point displacements
:math:`\boldsymbol{D}_{n+1}` by Newmark’s method
`[strdyn:eq:newmark-velnew] <#strdyn:eq:newmark-velnew>`__ and
`[strdyn:eq:newmark-accnew] <#strdyn:eq:newmark-accnew>`__. Therefore,
the mid-equilibrium can be still thought of a system of nonlinear
equations in :math:`\boldsymbol{D}_{n+1}`. Let us again write the
unknown mid-velocities and mid-accelerations in terms of
:math:`\boldsymbol{D}_{n+1}`:

.. math::

   \begin{aligned}
   \label{strdyn:eq:genalpha-velnew}
      \boldsymbol{V}_{n+1-\alpha_\text{f}}(\boldsymbol{D}_{n+1})
   &  = \frac{(1-\alpha_\text{f})\gamma}{\beta\, \Delta t} \big( \boldsymbol{D}_{n+1} -
        \boldsymbol{D}_n \big) 
      - \frac{(1-\alpha_\text{f})\gamma-\beta}{\beta} \boldsymbol{V}_{n}
      - \frac{(1-\alpha_\text{f})(\gamma-2\beta)}{2\beta}\Delta t\boldsymbol{A}_n
      \text{,}
   \\ \label{strdyn:eq:genalpha-accnew}
      \boldsymbol{A}_{n+1-\alpha_\text{m}}(\boldsymbol{D}_{n+1})
   &  = \frac{1-\alpha_\text{m}}{\beta\, \Delta t^2} \big( \boldsymbol{D}_{n+1} - \boldsymbol{D}_n \big)
      - \frac{1-\alpha_\text{m}}{\beta\,\Delta t} \boldsymbol{V}_{n}
      - \frac{1-\alpha_\text{m}-2\beta}{2\beta} \boldsymbol{A}_n
      \text{.}
   \end{aligned}

The mid-point internal force vector means in terms of assembled element
force vectors:

.. math::

   \begin{split}
     \text{TR-like} \quad \boldsymbol{F}_{\text{int};n+1-\alpha_\text{f}}
   &  = \big( 1- \alpha_\text{f} \big) \boldsymbol{F}_{\text{int};n+1}
       + \alpha_\text{f} \boldsymbol{F}_{\text{int};n}
   \\
   &  = \big( 1- \alpha_\text{f} \big) %  % assembly operator
   % option 'd' is useless
   \mathchoice{  % display style
   \overset{\mathit{nele}}{\underset{e}{\raisebox{-0.6ex}{\mbox{\huge $\mathsf{A}$}}}}
   }{  % text style
   \raisebox{-0.35ex}{\mbox{\Large $\mathsf{A}$}}_{e}^{\mathit{nele}}
   }{  % script style
   \raisebox{-0.35ex}{\mbox{\Large $\mathsf{A}$}}_{e}^{\mathit{nele}}
   }{  % scriptscript style
   \raisebox{-0.35ex}{\mbox{\Large $\mathsf{A}$}}_{e}^{\mathit{nele}}
   } \boldsymbol{f}_\text{int}(\boldsymbol{d}_{n+1})
      + \alpha_\text{f} %  % assembly operator
   % option 'd' is useless
   \mathchoice{  % display style
   \overset{\mathit{nele}}{\underset{e}{\raisebox{-0.6ex}{\mbox{\huge $\mathsf{A}$}}}}
   }{  % text style
   \raisebox{-0.35ex}{\mbox{\Large $\mathsf{A}$}}_{e}^{\mathit{nele}}
   }{  % script style
   \raisebox{-0.35ex}{\mbox{\Large $\mathsf{A}$}}_{e}^{\mathit{nele}}
   }{  % scriptscript style
   \raisebox{-0.35ex}{\mbox{\Large $\mathsf{A}$}}_{e}^{\mathit{nele}}
   } \boldsymbol{f}_\text{int}(\boldsymbol{d}_{n})
   \\
     \text{IMR-like} \quad \boldsymbol{F}_{\text{int};n+1-\alpha_\text{f}}
   &  = \boldsymbol{F}_{\text{int}}(\boldsymbol{D}_{n+1-\alpha_\text{f}})
   \\
   & = %  % assembly operator
   % option 'd' is useless
   \mathchoice{  % display style
   \overset{\mathit{nele}}{\underset{e}{\raisebox{-0.6ex}{\mbox{\huge $\mathsf{A}$}}}}
   }{  % text style
   \raisebox{-0.35ex}{\mbox{\Large $\mathsf{A}$}}_{e}^{\mathit{nele}}
   }{  % script style
   \raisebox{-0.35ex}{\mbox{\Large $\mathsf{A}$}}_{e}^{\mathit{nele}}
   }{  % scriptscript style
   \raisebox{-0.35ex}{\mbox{\Large $\mathsf{A}$}}_{e}^{\mathit{nele}}
   } \boldsymbol{f}_\text{int}(\boldsymbol{d}_{n+1-\alpha_\text{f}})
     = %  % assembly operator
   % option 'd' is useless
   \mathchoice{  % display style
   \overset{\mathit{nele}}{\underset{e}{\raisebox{-0.6ex}{\mbox{\huge $\mathsf{A}$}}}}
   }{  % text style
   \raisebox{-0.35ex}{\mbox{\Large $\mathsf{A}$}}_{e}^{\mathit{nele}}
   }{  % script style
   \raisebox{-0.35ex}{\mbox{\Large $\mathsf{A}$}}_{e}^{\mathit{nele}}
   }{  % scriptscript style
   \raisebox{-0.35ex}{\mbox{\Large $\mathsf{A}$}}_{e}^{\mathit{nele}}
   } \int_{\Omega^{(e)}}\limits \left.\big(
     \frac{\partial\boldsymbol{E}(\boldsymbol{d})}{\partial
     \boldsymbol{d}}\big)^\mathrm{T}\right|_{\boldsymbol{d}_{n+1-\alpha_\text{f}}}
     \hspace{-2.5em}\boldsymbol{S}(\boldsymbol{d}_{n+1-\alpha_\text{f}})
     \, \mathrm{d}V
   \end{split}

Linearisation and Newton–Raphson iteration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The generalised mid-point-discretised linear momentum balance can be
written as an residual

  .. math::

     \boldsymbol{R}_\text{effdyn}(\boldsymbol{D}_{n+1})
       = \boldsymbol{M} \boldsymbol{A}_{n+1-\alpha_\text{m}}
       + \boldsymbol{C} \boldsymbol{V}_{n+1-\alpha_\text{f}}
       + \boldsymbol{F}_{\text{int};n+1-\alpha_\text{f}}
       - \boldsymbol{F}_{\text{ext};n+1-\alpha_\text{f}}
       \stackrel{!}{=} \boldsymbol{0}

  These nonlinear equations can be linearised at the end of the time
  step at :math:`t_{n+1}` with :math:`\boldsymbol{D}_{n+1}`:

  .. math::

     Lin\boldsymbol{R}_\text{effdyn}(\boldsymbol{D}_{n+1})
       = \boldsymbol{R}_\text{effdyn}(\boldsymbol{D}_{n+1}^i) 
       + \left.\frac{\partial\boldsymbol{R}_\text{effdyn}(\boldsymbol{D}_{n+1})}
         {\partial\boldsymbol{D}_{n+1}}\right|^{i}  \Delta\boldsymbol{D}_{n+1}^{i+1}

  in which the *dynamic effective tangential stiffness matrix*
  :math:`\boldsymbol{K}_{\text{T}\,\text{effdyn}}` appears as the
  differentiation of the dynamic effective residual with respect to the
  displacements :math:`\boldsymbol{D}_{n+1}`. This stiffness matrix is
  obtained detailed in

  .. math::

     \begin{aligned}
     &  \boldsymbol{K}_{\text{T}\,\text{effdyn}}(\boldsymbol{D}_{n+1}^i)
        = \left.\frac{\partial\boldsymbol{R}(\boldsymbol{D}_{n+1})}{\partial\boldsymbol{D}_{n+1}}\right|^{i}
     \\
     & 
       \quad = \Bigg.\Bigg[\boldsymbol{M} 
               \underbrace{\frac{\partial\boldsymbol{A}_{n+1-\alpha_\text{m}}}{\partial\boldsymbol{A}_{n+1}}}_{1-\alpha_\text{m}}
               \underbrace{\frac{\partial\boldsymbol{A}_{n+1}}{\partial\boldsymbol{D}_{n+1}}}_{\frac{1}{\beta\Delta t^2}}
        +     \boldsymbol{C}
               \underbrace{\frac{\partial\boldsymbol{V}_{n+1-\alpha_\text{f}}}{\partial\boldsymbol{V}_{n+1}}}_{1-\alpha_\text{f}}
               \underbrace{\frac{\partial\boldsymbol{V}_{n+1}}{\partial\boldsymbol{D}_{n+1}}}_{\frac{\gamma}{\beta\Delta t}}
        +     \frac{\partial\boldsymbol{F}_{\text{int},n+1-\alpha_\text{f}}}{\partial\boldsymbol{D}_{n+1}}
         \Bigg]\Bigg|^i
     \\
     &  \quad = \Bigg.\Bigg[
        \frac{1-\alpha_\text{m}}{\beta \Delta t^2} \boldsymbol{M}
        + \frac{(1-\alpha_\text{f})\gamma}{\beta\Delta t} \boldsymbol{C}
        + \frac{\partial\boldsymbol{F}_{\text{int},n+1-\alpha_\text{f}}}{\partial\boldsymbol{D}_{n+1}}
        \Bigg]\Bigg|^i
     \end{aligned}

  with

  .. math::

     \begin{aligned}
        \text{TR-like} & \quad \frac{\partial\boldsymbol{F}_{\text{int},n+1-\alpha_\text{f}}}{\partial\boldsymbol{D}_{n+1}}
        = \frac{\partial\boldsymbol{F}_{\text{int}}(\boldsymbol{D}_{n+1-\alpha_\text{f}})}{\partial\boldsymbol{D}_{n+1-\alpha_\text{f}}} 
        \frac{\partial\boldsymbol{D}_{n+1-\alpha_\text{f}}}{\partial\boldsymbol{D}_{n+1}}
        = \big( 1-\alpha_\text{f} \big)  \boldsymbol{K}_\text{T}(\boldsymbol{D}_{n+1-\alpha_\text{f}})
     \\
        \text{IMR-like} & \quad \begin{array}{ll} \frac{\partial\boldsymbol{F}_{\text{int},n+1-\alpha_\text{f}}}{\partial\boldsymbol{D}_{n+1}}
        & = \frac{\partial}{\partial\boldsymbol{D}_{n+1}} \Big( \big( 1- \alpha_\text{f} \big) \boldsymbol{F}_{\text{int}}(\boldsymbol{D}_{n+1})
         + \alpha_\text{f} \boldsymbol{F}_{\text{int}}(\boldsymbol{D}_{n}) \Big) \\
        & = \big( 1-\alpha_\text{f} \big)  \frac{\partial\boldsymbol{F}_{\text{int}}(\boldsymbol{D}_{n+1})}{\partial\boldsymbol{D}_{n+1}}
        = \big( 1-\alpha_\text{f} \big) \boldsymbol{K}_\text{T}(\boldsymbol{D}_{n+1})
        \end{array}
     \end{aligned}

In a Newton–Raphson iteration the iterative displacement increment
:math:`\Delta\boldsymbol{D}_{n+1}^{i+1}` is calculated by solving

.. math::

   \begin{aligned}
   &  \boldsymbol{K}_{\text{T}\,\text{effdyn}}(\boldsymbol{D}_{n+1}^i)\, \Delta\boldsymbol{D}_{n+1}^{i+1}
     = - \boldsymbol{R}_\text{effdyn}(\boldsymbol{D}_{n+1}^i)
   \\
   & \qquad\qquad\qquad\leadsto\qquad
     \Delta\boldsymbol{D}_{n+1}^{i+1}
     = - {\boldsymbol{K}_{\text{T}\,\text{effdyn}}(\boldsymbol{D}_{n+1}^i)}^{-1} \boldsymbol{R}_\text{effdyn}(\boldsymbol{D}_{n+1}^i)
   \end{aligned}

This allows to update the unknown displacements with

.. math::

   \boldsymbol{D}_{n+1}^{i+1}
     = \boldsymbol{D}_{n+1}^{i} + \Delta\boldsymbol{D}_{n+1}^{i+1}
     \text{.}

In essence, the actual right-hand-side
:math:`\boldsymbol{R}_\text{effdyn}(\boldsymbol{D}_{n+1}^{i})` depends
as shown only on the actual end-displacements
:math:`\boldsymbol{D}_{n+1}^{i+1}`, but it is convenient to calculate
:math:`\boldsymbol{R}_\text{effdyn}(\boldsymbol{D}_{n+1}^i)` using the
current mid-displacements, -velocities and -accelerations. These current
vectors can be calculated based on the formulas given in
`[strdyn:eq:genalpha-middef] <#strdyn:eq:genalpha-middef>`__ or
`[strdyn:eq:genalpha-velnew] <#strdyn:eq:genalpha-velnew>`__ and
`[strdyn:eq:genalpha-accnew] <#strdyn:eq:genalpha-accnew>`__.
Optionally, they can be evaluated with an update mechanism with these
increments

.. math::

   \begin{aligned}
      \Delta\boldsymbol{D}_{n+1-\alpha_\text{f}}^{i+1}
   &   = \frac{\partial\boldsymbol{D}_{n+1-\alpha_\text{f}}}{\partial\boldsymbol{D}_{n+1}} 
        \, \Delta\boldsymbol{D}_{n+1}^{i+1}
      = (1-\alpha_\text{f}) \, \Delta\boldsymbol{D}_{n+1}^{i+1}
   \\
      \Delta\boldsymbol{V}_{n+1-\alpha_\text{f}}^{i+1}
   &   = \frac{\partial\boldsymbol{V}_{n+1-\alpha_\text{f}}}{\partial\boldsymbol{D}_{n+1}} 
        \, \Delta\boldsymbol{D}_{n+1}^{i+1}
      = \frac{(1-\alpha_\text{f})\gamma}{\beta \Delta t} \, \Delta\boldsymbol{D}_{n+1}^{i+1}
   \\
      \Delta\boldsymbol{A}_{n+1-\alpha_\text{m}}^{i+1}
   &   = \frac{\partial\boldsymbol{A}_{n+1-\alpha_\text{m}}}{\partial\boldsymbol{D}_{n+1}} 
        \, \Delta\boldsymbol{D}_{n+1}^{i+1}
      = \frac{1-\alpha_\text{m}}{\beta \Delta t^2} \, \Delta\boldsymbol{D}_{n+1}^{i+1}
   \end{aligned}

and the usual update procedure

.. math::

   \begin{aligned}
      \boldsymbol{D}_{n+1-\alpha_\text{f}}^{i+1}
   &  = \boldsymbol{D}_{n+1-\alpha_\text{f}}^{i} 
      + \Delta\boldsymbol{D}_{n+1-\alpha_\text{f}}^{i+1}
   \\
      \boldsymbol{V}_{n+1-\alpha_\text{f}}^{i+1}
   &  = \boldsymbol{V}_{n+1-\alpha_\text{f}}^{i} 
      + \Delta\boldsymbol{V}_{n+1-\alpha_\text{f}}^{i+1}
   \\
      \boldsymbol{A}_{n+1-\alpha_\text{m}}^{i+1}
   &  = \boldsymbol{A}_{n+1-\alpha_\text{m}}^{i} 
      + \Delta\boldsymbol{A}_{n+1-\alpha_\text{m}}^{i+1}
   \\
   \end{aligned}

The convergence of the Newton–Raphson iteration can be tested — for
instance — by checking the residual

.. math::

   \| \boldsymbol{R}_\text{effdyn}(\boldsymbol{D}_{n+1}^{i+1}) \| \leq \mathit{tol}
     \text{.}

A different relative convergence check is based on the displacement
increment:

.. math::

   \frac{\| \Delta\boldsymbol{D}_{n+1}^{i+1} \|}
     {\| \boldsymbol{D}_{n+1}^{i+1} - \boldsymbol{D}_n \|} 
     \leq \mathit{tol}_\text{D}
     \text{.}

**Algorithm Newton–Raphson iteration**

.. note:: 

   struktogramm to be added.


Here we have used a very simple predictor for the new displacements (and
in consequence for velocities and accelerations): The previously
converged time step is used. More sophisticated predictors can be
constructed introducing extrapolation techniques or explicit time
integration schemes. For instance, the forward Euler time integration
scheme could be applied as a predictor (forward Euler was introduced in
the course “Finite Elemente”); however, forward Euler is not a
recommended choice.

Order of accuracy
^^^^^^^^^^^^^^^^^

According to Section `1.2.1 <#strdyn:sec:accuracy>`__ we can deduce the
order of accuracy of the displacement approximation given by the
generalised-alpha (GA) method. We achieve

.. math::

   \begin{aligned}
      \boldsymbol{l}_{n+1}^{\text{GA}}
   &  = \boldsymbol{D}(t_{n+1}) - \boldsymbol{\Psi}_{n+1,n}^{\text{GA}} \boldsymbol{D}(t_n)
   \\
   &  = \frac{\Delta t^3}{2} \Big( \frac{1}{3} - 2\beta + \alpha_\text{f} - \alpha_\text{m}\Big) 
      \dot{\boldsymbol{A}}(t_n)
   \\
   &  + \frac{\Delta t^4}{4} \Big( \frac{1}{6} - 2\beta\big(1-\alpha_\text{f}+2\alpha_\text{m}\big) - (\alpha_\text{f}-\alpha_\text{m})(1-2\alpha_\text{m}) \Big)  
      \ddot{\boldsymbol{A}}(t_n) 
   \\
   &   + \mathcal{O}(\Delta t^5)
   \end{aligned}

This equation implies: The displacements are always at least
:math:`2`\ nd order accurate and they are even :math:`3`\ rd order
accurate if
:math:`\frac{1}{3} - 2\beta + \alpha_\text{f} - \alpha_\text{m} = 0`.

Since the governing equations are a set of second order ODEs, we provide
the LDE of the velocities as well. These are

.. math::

   \begin{aligned}
      \dot{\boldsymbol{l}}_{n+1}^{\text{GA}}
   &  = \boldsymbol{V}(t_{n+1}) - \dot{\boldsymbol{\Psi}}_{n+1,n}^{\text{GA}} \boldsymbol{V}(t_n)
   \\
   &  = \Delta t^2 \Big( \frac{1}{2} - \gamma + \alpha_\text{f} - \alpha_\text{m} \Big) 
      \dot{\boldsymbol{A}}(t_n)
   \\
   &  + \frac{\Delta t^3}{2} \Big( \frac{1}{3} - \gamma\big(1-\alpha_\text{f}+2\alpha_\text{m}\big) - (\alpha_\text{f}-\alpha_\text{m})(1-2\alpha_\text{m}) \Big)  
      \ddot{\boldsymbol{A}}(t_n) 
   \\
   &   + \mathcal{O}(\Delta t^4)
   \end{aligned}

in which :math:`\dot{\boldsymbol{l}}_{n+1}^{\text{GA}}` and
:math:`\dot{\boldsymbol{\Psi}}_{n+1,n}^{\text{GA}}` do *not* imply a
time differentation – the dot is merely a notation. It can be seen the
velocities are :math:`2`\ nd order accurate if
:math:`\frac{1}{2} - \gamma + \alpha_\text{f} - \alpha_\text{m}`
otherwise only :math:`1`\ st order.

The order of accuracy of the generalised-alpha method follows the lower
value of the order of the displacements or velocities.

As stated before, the semi-discrete equations of motion are second order
ODE, thus both LDEs have to be taken into account and the worse value
determines the overall order of accuracy according to Hairer et al.
[strdyn:hairer87]_, [strdyn:hairer91]_).



Time adaptivity
---------------

This section is an excerpt of [strdyn:bornemann03]_.

Theory of time adaptivity based on indication of the local discretisation error
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Different ways exist to adapt the time step size :math:`\Delta t`, here
we only apply an a posteriori method utilising the local discretisation
error (LDE).

.. _`strdyn:sec:adaptivity`:

Step size adaptivity
^^^^^^^^^^^^^^^^^^^^

Local error control is based on requiring the estimated local
discretisation error to stay below a user-defined tolerance
:math:`\mathit{tol}`. If the local discretisation error, resulting from
a time integration with time step size :math:`\Delta t_n`, is higher
than the tolerance, the step is repeated with a smaller step size. A
proposal for this smaller step size is the so-called ‘optimal’ step
size, denoted with :math:`\Delta t_n^*`, which is the time step size
resulting in a local discretisation error which is approximately equal
to the tolerance. The procedure is repeated until a small enough step
size is found. Then the ‘optimal’ time step size might be used as an
initial value for the next time step size
:math:`\Delta t_{n+1} = \Delta t_n^*`.

Therefore the basic requirement for the local discretisation error is

.. math::

   \label{strdyn:eq:lde-leq-tol}
     \|\boldsymbol{l}_{n+1}(\Delta t_n)\| \leq \mathit{tol}
   %%  \comma\quad \tol > 0
     \text{,}

whereby the dimensionless tolerance :math:`\mathit{tol}>0` is
user-prescribed. The above described procedure is contained in
Figure `1.1 <#strdyn:fig:adap-sch>`__.

.. container:: center

   .. figure:: figures/adap-sch.pdf
      :alt: Diagram of LDE-based step size adaptivity
      :name: strdyn:fig:adap-sch
      :width: 60.0%

      Diagram of LDE-based step size adaptivity

Different norms, such as average, root-mean-square or infinity norm, can
be used to obtain a dimensionless scalar from the local discretisation
error vector.

The ‘optimal’ step size :math:`\Delta t_n^*` is derived in the following
equations. The starting point is the usual definition of the local
discretisation error in which the local discretisation error obtained
with :math:`\Delta t_n` is assumed to be larger than
:math:`\mathit{tol}`:

.. math::

   \begin{gathered}
     \label{strdyn:eq:LDE-old-step-size-greater-tol}
     \| \boldsymbol{l}_{n+1}(\Delta t_n) \|
     \approx \boldsymbol{C}(t_n)\, \Delta t_n^{p+1} 
     \geq \mathit{tol}
     \text{,}
   \\
     \label{strdyn:eq:LDE-new-step-size-approx-tol}
     \| \boldsymbol{l}_{n+1}(\Delta t_n^*) \|
     \approx \boldsymbol{C}(t_n)\, {\Delta t_n^*}^{p+1} 
     \approx \mathit{tol}
     \text{.}
   \end{gathered}

Equation
`[strdyn:LDE-new-step-size-approx-tol] <#strdyn:LDE-new-step-size-approx-tol>`__
can be transformed to

.. math::

   \label{strdyn:eq:LDE-new-step-size-approx-tol-2}
     \boldsymbol{C}(t_n)
     \approx \frac{\| \boldsymbol{l}_{n+1}(\Delta t_n^*) \|}{{\Delta
     t_n^*}^{p+1}}
     \approx \frac{\mathit{tol}}{{\Delta t_n^*}^{p+1}}
     \text{.}

Introducing
`[strdyn:eq:LDE-new-step-size-approx-tol-2] <#strdyn:eq:LDE-new-step-size-approx-tol-2>`__
into
`[strdyn:eq:LDE-old-step-size-greater-tol] <#strdyn:eq:LDE-old-step-size-greater-tol>`__
results in

.. math::

   \label{strdyn:eq:new-step-size}
     \Delta t_n^* \leq \sqrt[{p+1}]{\frac{\mathit{tol}}{\|
     \boldsymbol{l}_{n+1}(\Delta t_n) \|}}\, \Delta t_n
     \text{.}

The ‘optimal’ step size corresponds to the lower bound in
`[strdyn:eq:new-step-size] <#strdyn:eq:new-step-size>`__.

Furthermore, the ‘optimal’ step size might be more reliable if the
tolerance is reduced by ‘safety’ scale factors

.. math::

   \label{strdyn:eq:new-step-size-scaled}
     \Delta t_n^\text{new}
     = \min\big\{ \Delta t_\text{max}, \max\{\min\big( r_\text{max}, \max(
     r_\text{min}, s r^*)\Delta t_n, \Delta t_\text{min} \} \big\}
     \text{.}

In the previous equation the ‘optimal’ ratio is abbreviated with

.. math::

   \label{strdyn:eq:new-step-size-optscal}
     r^* = \sqrt[{p+1}]{\frac{\mathit{tol}}{\|
     \boldsymbol{l}_{n+1}(\Delta t_n) \|}}
     \text{.}

The step size :math:`\Delta t_n^\text{new}`,
`[strdyn:eq:new-step-size-scaled] <#strdyn:eq:new-step-size-scaled>`__,
replaces the ‘optimal’ step size :math:`\Delta t_n^*` in the algorithm
(Figure `1.1 <#strdyn:fig:adap-sch>`__). The factor :math:`r_\text{max}`
limits the maximum size increase between two steps, :math:`r_\text{min}`
bounds the decrease. In the same spirit, a maximum and minimum step
size, :math:`\Delta t_\text{max}` and :math:`\Delta t_\text{min}`, is
imposed to achieve a more robust algorithm. Sometimes, :math:`p` instead
of :math:`(p+1)` is used in
`[strdyn:eq:new-step-size-optscal] <#strdyn:eq:new-step-size-optscal>`__
to reflect the order of the global error.

Generally speaking, estimations for the local discretisation error are
obtained by using two different time integration schemes A and B. The
comparison of results :math:`\boldsymbol{y}_{n+1}^\text{A}` to
:math:`\boldsymbol{y}_{n+1}^\text{B}` makes it possible to evaluate an
local discretisation error estimation of the lower-order method of
scheme A or B. If the results of the scheme A are kept to integrate
forward in time, then A is called *marching* time integration scheme.
The scheme B is only used to indicate the local discretisation error,
hence it is referred to as *auxiliary* scheme. The adaptive algorithm is
denoted with B/A.

The algorithms based on embedded Runge-Kutta methods are instances of
such local error control with time step adaption.

.. _`strdyn:sec:zx`:

Zienkiewicz and Xie indicator
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Zienkiewicz and Xie presented in
[strdyn:zienkiewicz91]_ a local error indicator for the
Newmark algorithm (Section `[strdyn:sec:new] <#strdyn:sec:new>`__). The
estimator advantageously uses the member which integrates the
displacements with third-order accuracy, i.e. :math:`\beta=\frac{1}{6}`,
:math:`\gamma` arbitrary, cf.
`[strdyn:eq:new-lde-1] <#strdyn:eq:new-lde-1>`__. The concept can be
straight forwardly applied to the generalised-alpha method.

In essence, the general generalised-alpha method with :math:`2`\ nd
order accurate displacements (dubbed here GA2) is considered as the
marching time integration scheme. Its :math:`3`\ rd order accurate
sibling GA3 is used as the auxiliary scheme to eventually obtain the
local discretisation error estimation/indication of the displacements.

The GA2 and GA3 methods are implicit schemes, hence a direct calculation
with GA2 and GA3 would require two iterative solutions. This can be
overcome by avoiding a direct determination of the GA3. The results of
the marching GA2 method (:math:`\boldsymbol{D}_{n+1}^\text{GA2}`,
:math:`\boldsymbol{V}_{n+1}^\text{GA2}`,
:math:`\boldsymbol{A}_{n+1}^\text{GA2}`) can be used to explicitely
construct a third-order accurate result, which is related to the NM3 and
is called ZX here.

The ZX method is defined as

.. math::

   \boldsymbol{D}_{n+1}^\text{ZX}
     = \boldsymbol{D}_n + \Delta t_n \boldsymbol{V}_n + \frac{\Delta t^2}{3}\boldsymbol{A}_n
     + \frac{\Delta t^2}{6} \boldsymbol{A}_{n+1}^\text{GA2}
     \text{.}

The dependence of this algorithm on
:math:`\boldsymbol{D}_{n+1}^\text{GA2}` is revealed by introducing
`[strdyn:eq:new-algo] <#strdyn:eq:new-algo>`__

.. math::

   \label{strdyn:eq:zx-algo}
     \boldsymbol{D}_{n+1}^\text{ZX}
     = \frac{1}{6\beta} \Big( \boldsymbol{D}_{n+1}^\text{GA2}
     + (6\beta-1)(\boldsymbol{D}_n + \Delta t\boldsymbol{V}_n +
     \frac{\Delta t^2}{2}\boldsymbol{A}_n) \Big)
     \quad\text{with}\quad
     \beta\neq\frac{1}{6}
     \text{.}

The local discretisation error of the ZX is based on
`[strdyn:eq:zx-algo] <#strdyn:eq:zx-algo>`__ by expanding it in Taylor
series:

.. math::

   \boldsymbol{l}_{n+1}^\text{ZX}
     = \boldsymbol{D}(t_{n+1}) - \boldsymbol{\Psi}_{n+1,n}^\text{ZX}\boldsymbol{D}(t_n) 
     = -\frac{\Delta t^4}{24} \bar{\boldsymbol{u}}^{(4)}(t_n) + \mathcal{O}(\Delta t^5)
     \text{,}

in which the third-order accuracy is shown.

Zienkiewicz and Xie’s indicator of the local discretisation error uses
the definitions of the local discretisation errors for the displacements
and assumes direct differences as feasible approximations

.. math::

   \begin{equation}\label{strdyn:eq:zx-nm-lde1}
     \boldsymbol{l}_{n+1}^\text{GA2}
     = \boldsymbol{D}(t_{n+1}) - \boldsymbol{\Psi}_{n+1,n}^\text{GA2}\boldsymbol{D}(t_n) 
     \approx \boldsymbol{D}(t_{n+1}) - \boldsymbol{D}_{n+1}^\text{GA2}
     = \mathcal{O}(\Delta t^3)
     \text{,}
   \end{equation}
   \begin{equation}\label{strdyn:eq:zx-nm-lde2}
     \boldsymbol{l}_{n+1}^\text{ZX}
     = \boldsymbol{D}(t_{n+1}) - \boldsymbol{\Psi}_{n+1,n}^\text{ZX}
     \approx \boldsymbol{D}(t_{n+1}) - \boldsymbol{D}_{n+1}^\text{ZX}
     = \mathcal{O}(\Delta t^4)
     \text{.}
   \end{equation}

Equations `[strdyn:eq:zx-nm-lde1] <#strdyn:eq:zx-nm-lde1>`__ and
`[strdyn:eq:zx-nm-lde2] <#strdyn:eq:zx-nm-lde2>`__ are subtracted from
each other leading to

.. math::

   \boldsymbol{l}_{n+1}^\text{GA2}
     = \boldsymbol{D}_{n+1}^\text{ZX} -\boldsymbol{D}_{n+1}^\text{GA2}
     \text{,}

as :math:`\boldsymbol{l}_{n+1}^\text{ZX}` is negligible compared to
:math:`\boldsymbol{l}_{n+1}^\text{GA2}`. The indicator is given
alternatively as

.. math::

   \boldsymbol{l}_{n+1}^\text{GA2}
     = \frac{\Delta t^2}{6} ( 1 - 6\beta )
     \big( \boldsymbol{A}_{n+1}^\text{GA2} -\boldsymbol{A}_{n} \big)
     \text{,}

which coincides with the formula of Zienkiewicz and Xie
[strdyn:zienkiewicz91]_ except the sign.

The presented local discretisation error estimator allows only to assess
the displacements, the velocities are not checked.

Inclined boundary conditions (BCs) – ``ccarat``
-----------------------------------------------

.. note::
   
   Is this information about inclined boundary conditions also valid / necessary
   for BACI?

About
~~~~~

Sometimes it is desirable to have boundary conditions which are not
oriented in the global Cartesian :math:`XYZ`-axes, but in other
directions. This occurs for instance with rotatory symmetric problems.

Method
~~~~~~

The implementation is an adaptation of Steffen Genkinger’s local systems
(locsys, for fluid) to structures. The idea is to rotate the DOFs on an
inclined boundary in the direction of a local co-ordinate system. These
locally oriented DOFs are solved on the global level. The
generalised-alpha holds locally oriented DOFs for all DOFs at Dirichlet
nodes (prescribed/supported & free DOFs there). Therefore, all assembled
quantities (stiffness, mass, internal force, velocity, etc) have at
certain nodes rotated DOFs. On the other hand, the ‘``sol``’,
‘``sol_increment``’, etc arrays at the ``NODE``\ s are always globally
oriented (except for intermediate operations at solution algo level)

Gid input
~~~~~~~~~

#. | Define & name (e.g. ‘*mysys*’) local axes
   | Data :math:`\to` Local axes :math:`\to` Define

#. | Assign local systems to design points, lines and surface to which
     inclinded BCs are wanted. E.g.
   | ``Data`` :math:`\to` ``Conditions`` :math:`\to` ``Single``
     :math:`\to` ``Layer`` :math:`\to` ``Points`` :math:`\to`
     ``Locsys Point``
   | choose ``LocsysId`` (e.g. *1*)
   | You need to apply to the full hierarchy of design element. It
     avoids conflicts on lines which belong to different Dirichlet BCs
     coming from higher entities.

#. | Bind to ``locsysId`` the named (e.g. ‘*mysys*’) local axes
   | Data :math:`\to` Problem Data :math:`\to` Locsys
   | ``Locsys_1`` : ``on``
   | ``Locsys_Typ_1`` : ``BASEVEC``
   | ``Ls1_Basevec_Input_Type`` : ``LocalAxes_Name``
   | ``Ls1_LocalAxes_Name`` : ‘*mysys*’

Compilation
~~~~~~~~~~~

``LOCALSYSTEMS_ST`` preprocessor definition required (``defines`` file)

Restrictions
~~~~~~~~~~~~

-  Available in old discretisation

-  Implemented for

   -  ``WALL1`` (untested)

   -  ``BRICK1`` (untested)

   -  ``SOLID3`` (verified)

-  Prescribed deflection of DOFs available

-  Solution technique: Generalised-alpha (``stru_dyn_nln.c``)

-  fsiloads in ``WALL1``???

-  test file ``so3_dyn_inclined_bc.dat``

.. [strdyn:hairer87] Hairer el al., 1987
.. [strdyn:deuflhard94] Deuflhard et al., 1994
.. [strdyn:hairer91] Hairer el al., 1991
.. [strdyn:newmark59] Newmark, 1959
.. [strdyn:zienkiewicz91] Zienkiewicz, 1991
.. [strdyn:chung95] Chung, 1995
.. [strdyn:bornemann03] Bornemann, 2003
