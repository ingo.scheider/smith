.. _inputparameterreference:

==========================
Input Parameter Reference
==========================

It is always hard to write and maintain an accurate reference
documentation for a moving target like BACI. The following pages present
our best effort. And please note that BACI knows its input file format
very well. You can ask it. Just type

::

        ${PATH_TO_BACI}/baci-release --parameters

and you will get a (partial) input file with all the default parameters
and documentation.


.. toctree::
   :maxdepth: 2

   referenceintroduction
   elementreference
   materialreference
   conditionreference
   headerreference

.. bcreference
