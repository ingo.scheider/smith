Elements
============

.. toctree::
   :maxdepth: 2

   conventions
   elementsstructure
   elementsfluid
   elementslubrication
   elementstransport
   elementstransport2
   elementsale
   elementsthermo
   elementsartery
   elementsreduceddairways
   elementselectromagnetic


Originally, there were some files with old elements, which are not valid anymore:

-  xfem
-  brick1
-  shell8
-  beam3

There were also the following files, but the corresponding elements do not exist:

- wall1
- shell9
- axishell
- wallge
