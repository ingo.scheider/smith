References
===========

.. [Gee07] M. Gee, C.M. Siefert, J.J. Hu, R.S. Tuminaro, M.G. Sala: ML 5.0 Smoothed Aggregation User's Guide, SAND2006-2649, Sandia Nat. Lab, 2007.
.. [Wall99] W.A. Wall, PhD thesis, 1999 (available under ``/lnm/literature/1999/``)

.. [Meyers98] Meyers, Scott (1998): Effective C++. Addison-Wesley, Reading, Massachusetts, 2. edition.

.. [Meyers01] Meyers, Scott (2001): Effective STL. Addison-Wesley, Reading, Massachusetts.

.. [Stroustrup98] Stroustrup, Bjarne (1998): Die C++ Programmiersprache. Addison-Wesley, Reading, Massachusetts, 3. Auflage

.. [Alexandrescu01] Alexandrescu, Andrei (2001): Modern C++ Design. Addison-Wesley, Reading, Massachusetts.

.. [Reiser94] Reiser et al., 1994