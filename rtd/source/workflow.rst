.. _baciworkflow:

The BACI Workflow
===================

.. toctree::
   :maxdepth: 2

   preprocessing
   bacisimulation
   postprocessing


