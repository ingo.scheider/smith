old_Source code commenting conventions
======================================

Again, this is old stuff.

The file
~~~~~~~~

Every file must begin with a comment in the doxygen style including:

-  a brief description

-  and a detailed description

-  the maintainer

::

   /*------------------------------------------------------*/
   /*!
   \brief Brief description.

   \level [0,1,2,3]

   \maintainer Firstname Lastname
    */
   /*------------------------------------------------------*/

All functions in the file must be added to a doxygen module. Use the
following syntax to open the doxygen module ’Ale’ and add all the
following functions:

::

   /*!
   \addtogroup Ale
   *//*! @{ (documentation module open)*/

At the end of the file the doxygen module needs to be closed:

::

   /*! @} (documentation module close)*/

The function
~~~~~~~~~~~~

Every function must begin with a comment in the doxygen style including:

-  a brief description (one line)

-  and a detailed description what is done in the function

-  the list of parameters

-  the author

-  the date when the function was written

::

   /*------------------------------------------------------*/
   /*!
     \brief Brief description

     Detailed description what the function is doing why.

     \param b        (o) the calculated operator matrix
     \param deriv    (i) the derivatives of the shape functions
     \param xjm      (i) the Jacobian matrix
     \param det      (i) the determinant of the Jacobian matrix
     \param iel      (i) number of nodes per element

     \author mn
     \date   10/04
    */
   /*------------------------------------------------------*/

The input file
~~~~~~~~~~~~~~

Every input file must begin with a comment including:

-  the maintainer

-  a brief description what is calculated

-  all necessary defines

-  the optimal sizes of MAXNOD, MAXELE, MAXDOFPERNODE and MAXGAUSS

The list of necessary defines must be in ONE line. In addition to the
four reauired sizes further defines can be specified if necessary.

::

   //
   // Maintainer: Firstname Lastname
   //
   // necessary defines: RESULTTEST D_FLUID D_FLUID3 AZTEC_PACKAGE CHECK_MAX FAST_ASS2
   //
   // #define MAXNOD           (8)
   // #define MAXELE           (8)
   // #define MAXDOFPERNODE    (4)
   // #define MAXGAUSS         (8)
   //
