.. _materials:

Materials
===========

General information
--------------------

The definition of materials happens in the section ``-----MATERIALS``.
A material model defines its constituive behavior, which may consist of several terms; however, there is always *one* material assigned to each element. 
This material can be a material definition on its own, 
or a collection of behaviours consisting of a number of material models
to be defined subsequently. A single material model is in general defined by a single line of the form

::

   MAT <id> <materialname> <parameters...>

One may also define a material by summation of several potentials. A collection of material behaviors, on the other hand, looks like this:

::

   MAT <id> <collectionname> NUMMAT <nmaterials> MATIDS <id_1> ... <id_nmaterials> <possibly further parameters>

Here, terms of the material behavior have to follow with their own number, which must correspond to ``<id_1> ... <id_nmaterials>``.



Structural Material Models
--------------------------


A material model in structural mechanics defines the connection between deformation (usually strain) and stress. 


Fluid Material Models
---------------------



Other Material Models
---------------------



