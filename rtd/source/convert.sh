for dir in cppforbacists
do
  echo $dir
  if [ -d $dir ] 
  then
    maintexfile=single_report_${dir}.tex
    sed -f convert.sed $maintexfile > ${dir}/${maintexfile}
    cd $dir
    for texfile in *
    do 
      if [ -f $texfile ]
      then
        sed -f ../convert.sed ${texfile} > tmp
        mv tmp ${texfile}
      fi
    done
    pandoc -f latex -t rst -o ../${dir}.rst $maintexfile
    cd ..
  fi
done

