old_Binary IO
=============

.. note::

   Actually, it is not clear to me, what this section is good for. As a user
   documentation, people rather use the capabilities which are supposed to be
   described in the postprocessing section. 
   The section is extremely outdated anyway, so I am inclined to dump the whole text.

This section was originally written to be both: An indepth presentation
of the new binary IO machinery in ``ccarat`` and an advertisement for
it. Unfortunately it does not explain how to add new elements. For that
you will have to read the extensive code comments.

Note: This discussion concerns the C style binary IO implemented in
``ccarat``. IO in BACI follows the same lines, but turns out to be much
simpler to use and extend.

The purpose of output
---------------------

To be of use ccarat needs the ability to write the calculated values.
That’s obvious. The interesting values live in various places.

Node arrays
~~~~~~~~~~~

Of course there are the node arrays. These arrays are always there, but
their sizes and the meaning of the data inside them vary a lot.
Basically each algorithm has its own idea what’s going on in there. Only
the elements constrain the algorithms’ freedom in assigning meaning to
the node arrays because the elements, too, rely on these arrays.

Most of the time the node arrays have as many columns as there are dofs
at this node and as many rows as required by the algorithm. Only a few
of them are interessting results. The others are just working data.

Element data
~~~~~~~~~~~~

The second source of values that need to be written somewhere are the
elements themselves. Stresses, for example, are calculated at the
element level therefore stored in element arrays. But element data is
highly element specific. Each element is different.

Distributed Vectors
~~~~~~~~~~~~~~~~~~~

A third, less obvious, source of data are the distributed vectors that
make up our rhs and solution vectors. These need to be stored for
restart purposes. One would not attempt to postprocess them, but the io
system must support them nevertheless.

Output targets
--------------

The output that ccarat produces is used in many different ways.

Restart
~~~~~~~

For restart it is necessary to save the current step’s working data.
There is no need to store any input, the input file will be read again
anyway. There is no need to store data that can easily be reconstructed,
too. Thus we don’t store the current system matrix. But we need to store
all node arrays and everything that cannot be reconstructed without
recalculating former steps.

The reading part of our restart facility builds on the assumption that
all data structures are set up by the algorithm. No memory allocation is
done by the io module. It only reads the data files and puts the values
to the right places in memory.

The restart system is not concerned with the meaning of the data it
reads and writes.

GiD
~~~

Talking about GiD is talking about postprocessing. We need to give all
information to GiD that’s needed to visualize the results. In particular
the node coordinates and mesh connectivity must be written as well,
because GiD never sees the ccarat input file. (Okay, that’s a lie. These
files are produced by GiD. But that’s another story.)

But looking closer one sees that ccarat’s view of its data is different
from what GiD, or any other postprocessor for that matter, expects. So a
slight transformation has to be applied.

Plain Text
~~~~~~~~~~

Often people (read: developer) are interessted in the real numbers. So
some means to output a plain text file are necessary. Transformations
are not welcome here. The plain text output has to show the real ccarat
data structures.

Monitor
~~~~~~~

Sometimes only some selected dofs are important. We want to be able to
watch these dofs, that is write their evolution to a separate file.

Visual
~~~~~~

There are the visual libraries that allow to visualize 2d and 3d fluid
results. (That’s nothing but a special postprocessor.)

State of the art
----------------

The old way of handling output (which is still the current way, but
things are going to change) is to handle every special case separately.
There are \*.pss files that are used for restart (These are binary
files, the only ones that are binary. But these files are very closely
linked to ccarat’s internal data structures.) There are those flavia
files GiD asks for. There are \*.out files that contain a nice readable
representation and so on. The monitor facility is build into ccarat and
it’s even possible to link ccarat with the visual libraries.

In general the situation is not too bad, after all output works fine and
people are free to think about the important stuff. Sure, the code
contains lots of redundancies (this is especially true for the GiD
output part) but it’s straight forward to understand and you’ll have
little difficulty to add you own element. This is what actually happend
quite some times already. It has grown a lot by copy and paste.

In a parallel setting all processes write some files. Each process
writes its own data to its binary (pss) file. The text output (GiD),
however, is done by the first process only. Thus all results must be
available there.

New Requirements
----------------

But our attempt to improve ccarat’s parallelization enforces a different
style of output. We cannot write our results to plain text files
anymore. And we don’t want to constrain our output files to the number
of processes that were used to generate them. Thus neither of the
current approaches fulfills our needs. Additionally we want to be
plattform independent (we want to use the same format on little endian
and big endian plattforms).

Finally we have to recognize that ccarat’s file format is part of its
interface, this means it must be clean and simple and must not expose
internal details. This files will be read by external programs and, even
more frightening, they will stay around for a while. When we calculate
big problems we’ll keep the results. Therefore we have to maintain
ccarat’s ability to read it’s old files.

Of course the most important old requirement does remain: Our output
system must be as simple to enhance as possible. But nowadays we accept
a certain complexity for the sake of efficiency.

The new approach
----------------

We want to unify the various output methods, we want ccarat to have one
way to output data. All data is supposed to go into the same file. Of
course, it’s going to be a binary one. This file can be postprocessed by
various filters afterwards. These filters are responsible to apply any
required transformation and to create the input needed for your
favourite postprocessing tool.

Actually just one file will not do. In ccarat the central data
structures are the discretizations. Each discretization has its own set
of nodes and elements. We’ll have to have a per discretization output.
Thus it seems wise to put each discretization in a different output
file.

Looking closer we find two kinds of data we have to write: integers and
doubles. If we manage to separate these and put them to different files,
both files will have a very simple structure. The big advantage is that
we can easily change the endianess of both of them and we can, if
everything else fails, look at these files with ``hexdump`` or similar
tools. So we are going to have two files per discretization.

This way the bulk data can be stored, but we still have to keep track of
what we write to these files. We’ll have to store a description
somewhere. To do this we’re going to write an additional control file.
No matter how many discretizations or processes there are, we just need
one control file for each ccarat run. This file is supposed to contain
very little compared to the two value files. Thus control files can be
plain text. That’s convenient because this way one can easily read it
and see that kind of data has been written.

File format
-----------

During a ccarat run there are lots of results to be stored. A nonlinear
shell problem, for example, might require to store displacements and
stresses in each interation. That is there are six displacement values
per node and six times ngauss stress values per element. Any other
example will have a different number of values with different meaning to
be written, however the two cases, nodal values or element values, are
typical. The general picture is that for every time step for every
result there’s set of items (nodes or elements) each contributing some
values to be written. Such a collection of values is called chunk. A
chunk consists of consecutive entries of constant size. Each entry
contains the contribution of one node or element. The entries inside a
chunk are ordered by their items’ ids. There are many chunks in the
output files, one after the other.

Each chunk comes with a group definition in the control file. Such a
group tells where the chunk starts (offset in bytes) in the double and
integer data files as well as the length of its entries (in double or
integer units). Additionally it contains the information whether this is
a node or element chunk, even though that’s redundant, the name of the
group already conveys the meaning of the corresponding chunk.

Take the following ``mesh`` group as an example. It describes a chunk
that contains the elements’ ids, their types and the ids of those nodes
an element is connected to. There is a ``mesh`` group in each
discretizations main group:

::

   mesh:
       size_entry_length = 7
       size_offset = 0
       type = element
       value_entry_length = 0
       value_offset = 0

As you see both ``size_offset`` and
``value_offset`` are zero. That is the chunk starts at the
beginning of both data files, where ``value`` always refers to the file
containing double values. The integer file is called ’size’ file because
the first values it ever contained were the node array’s dimensions (and
because a name was needed).
``size_entry_length`` is seven, that is there
are seven integer values per element. Accordingly
``value_entry_length = 0`` means that this chunk
contains no double values.

Please note that the number of nodes and elements per discretization are
known (there is the ``numnp`` and the ``numele`` variable in the
discretizations main group in the control file) and that the total size
of one chunk can be determined with this information.

There is another group that has to be written for each discretization.
It’s called ``coords`` and contains the node coordinates. Here is an
example:

::

   coords:
       size_entry_length = 1
       size_offset = 28672
       type = node
       value_entry_length = 2
       value_offset = 0

The size file contains one integer per node. It’s the node’s (global)
id. The value file contains the coordinate values. The above example
belongs to a 2d problem with two coordinate values per node.

Generally, the control file consists of a sequence of definitions and
have a pythonic flavor. There are simple definitions of the form:

   ``attr_name = value``

where ``attr_name`` is any name consisting of letters,
digits and underscores. ``value`` might be a string, an integer or a
double value. Additionally there are group definitions like the ones
above. Groups always start with the group’s name and a colon. The
definitions a group contains are recognized by their indention. Groups
can be nested.

A particular thing is that names (in one group) need not be unique. All
values that are assigned to one name will be remembered. This feature
can be used to have many ``result`` groups, one for each time step, for
example. For simple variable definitions (like the ``numnp`` and
``numele`` variables mentioned above) the feature is of no use. In these
cases the **last** definition counts.

The control file reader and the internal representation it generates (a
map hierarchy) are defined in the file
``src/pss_full/pss_table.c``. It might be usable
in other contexts, too.

Implementation strategy
-----------------------

For efficiency reasons we want to write in parallel, utilizing the magic
of MPI IO. Our io module has to handle problems that are too big to fit
in the memory of one processing unit. On the other hand, however, we
want to save the items of one chunk sorted by its id. To do this we
assign each processor a consecutive range of ids it is responsible for.
The first processor gets the first range, the second the second and so
on. This way we can determine very easily which processor is responsible
for a particular item.

This scheme will not coincide with the physical reasonable distribution
figured out by metis. So we need a communication layer that sends the
values to be stored from the processor where its node or element lives
to the processor that’s responsible for writing it. The same goes for
the reading part (just in the opposite direction).

The details how that’s done are discussed further down below. We just
need the general picture in order to understand the following.

How to use the io module
------------------------

Example: Binary io in fluids
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To see a real working example of the binary output lets have a look at
the fluid algorithm ``fluid_isi`` in
``fluid_imp_semimp.c``. This file includes
``io.h`` from the io module which makes the io definitions available.
When the function ``fluid_isi`` is called the general io
module’s setup has already been done, but some discretization specific
setup is needed as well. In order to have this we need an output
variable:

   ``BIN_OUT_FIELD   out_context``

It is used to call the output initialization:

::

   init_bin_out_field(&out_context,
                      &(actsolv->sysarray_typ[actsysarray]),
                      &(actsolv->sysarray[actsysarray]),
                      actfield, actpart, actintra, 0);

At this point in time the discretization’s data files are open and the
connectivity (see above) has already be written. Now there is the
calculation going on. It’s a parallel calculation. Longish and
difficult. Better have restart information written every now and then:

   ``restart_write_bin_fluiddyn(&out_context,fdyn);``

Of course some postprocessing output is needed as well:

::

   if (ioflags.fluid_sol_gid==1) {out_results(&out_context, fdyn->acttime, fdyn->step, actpos, 
                                  OUTPUT_VELOCITY | OUTPUT_PRESSURE); 
   }
   if (ioflags.fluid_stress_gid==1) {out_results(&out_context, fdyn->acttime, fdyn->step, actpos,
                                     OUTPUT_STRESS); 
   }

When everything is said and done we destroy the context:

   ``destroy_bin_out_field(&out_context);``

That’s it.

But yes, that’s just the calling. You might want to enhance the io
system. To do this a closer look is needed.

A closer look: Restart
----------------------

In the io module you can find the file ``io_restart.c`` that
provides the two functions to write and read all information needed to
restart a simple fluid calculation.

Writing
~~~~~~~

The function
``restart_write_bin_fluiddyn`` that
writes the restart data consists basically of three parts. The first one
writes the general information to the control file:

::

   if (rank == 0) {
      out_main_group_head(context, restart);
      fprintf(bin_out_main.control_file,
              "step = %d \n"
              "time = %20.20f \n" 
              "\n",
             fdyn->step, fdyn->acttime);
   }

This is done on the first processor only. A new group named ``restart``
is started, that knows its field, discretization number, and step (and
is thus unique) as well as the current time. (There is one more
variable: the field position. That’s an unique number to each field.
It’s needed because there might be problems with many fields of the same
type.)

Please note that this function relies on a proper setup of the io
module. The ``bin_out_main`` variable must be
initialized. The first parameter of the function, a pointer to a
``struct_BIN_OUT_FIELD``, is
something that needs to be specified, too. We’ll talk about this later.

The next part just writes the node arrays we care about:

::

   out_node_arrays(context, node_array_sol);
   out_node_arrays(context, node_array_sol_increment);
   if (context->max_size[node_array_sol_mf] > 0) {
      out_node_arrays(context, node_array_sol_mf); 
   }

Here the ``out_node_arrays`` function does all
the work. We just need to specify which node array is to be written. The
``sol_mf`` array is optional (it’s used in multifield
problems only) and won’t be read later if we don’t write it now.
Internally this function will gather the data, write it and add a new
subgroup to the control file to describe the chunk of data it has
written.

The third part consists of two simple function calls that write all
restart related element data:

::

   find_restart_item_length(context, &value_length, &size_length);
   out_element_chunk(context, ‘̈element_data‘̈, cc_restart_element,
                     value_length, size_length, 0);

Yet these innocent looking calls are very haunting. It’s highly element
specific what kind of data we need to write. Thus both
``find_restart_item_length``, that
has to find the length of one chunk entry, as well as
``out_element_chunk``, the function that gathers
the data and writes the chunk, have to contain element specific code and
need to be changed when the elements evolve.

The first function is comparatively simple. In essence it loops the
discretization, finds out what elements there are and sets the output
parameters accordingly. The function
``out_element_chunk``, however, is a different
beast. It does all the work and does it in a generic way (the same
functions are used for element and nodal chunks). That’s not something
every user can be forced to study closely, for that reason all element
specific code is collected in a subfunction called
``out_pack_items`` (defined in
``io_packing.c``). This function is responsible to gather
all relevant element values and pack them into an array.

The function ``out_pack_items`` knows the kind
of values is has to gather by the flag
``cc_restart_element`` (“cc” stands for “chunk
content”). Actually there is a further
``out_pack_*`` subfunction to each “cc” flag and
these are the ones one must look at when element io has to be changed.

Such a functions is called for each processor in turn and has to find
all elements that have to go to this particular processor and pack them.
Packing means to copy the relevant values to the memory provided by the
caller. This first thing to do is to remember the number of integers and
doubles each element is allowed to contribute. It might contribute less,
leaving some space unoccupied, but it must not write more that this:

::

   len = chunk->value_entry_length;
   slen = chunk->size_entry_length;

Afterwards the function loops all elements in the current partition and
finds each one that is to be written by the specific processor. The
arguments ``dst_first_id`` and
``dst_num`` give the id of the first element this processor
is interested in and the total number of elements this processor has to
write. Of course we know that these ``dst_num`` elements
will have the ids ``dst_first_id``,
``dst_first_id+1`` and so on. The loop starts as
follows:

::

   counter = 0;
   for (i=0; i<actpdis->numele; ++i) {
     ELEMENT* actele = actpdis->element[i];
     if ((actele->Id_loc >= dst_first_id) &&
         (actele->Id_loc < dst_first_id+dst_num)) {
       DOUBLE* dst_ptr;
       INT* size_dst_ptr;

Please note that we know the elements are ordered by their
``Id_loc``, but we don’t own a consecutive list. After all,
we are just one processor in a massively parallel calculation, we own
some elements but nobody said their ``Id_loc`` would be
consecutive. However we know the range of elements the receiving
processor expects and every element that falls in that range will be
packed.

The two pointers ``dst_ptr`` and
``size_dst_ptr`` are used to point to the place
in memory where the current element’s values are to be written. The
memory is provided by the caller and is just big enough to hold all
elements of this partition that have to go to that (the receiving)
processor. In the parallel case the sizes array is enhanced by one
integer per element and each entry must start with the element’s
``Id_loc``:

::

   #ifdef PARALLEL
       send_size_buf[(slen+1)*counter] = actele->Id_loc;
       size_dst_ptr = &(send_size_buf[(slen+1)*counter+1]);
   #else
       size_dst_ptr = &(send_size_buf[slen*counter]);
   #endif
   
       dst_ptr = &(send_buf[len*counter]);

The receiving processor needs this additional integer in the parallel
case to know to which element this entry belongs. However, the
``Id_loc`` here won’t be written to the output files.

After this setup the interesting data has to be identified and copied
(for each element, we are inside the loop):

::

   switch (actele->eltyp) {
   
   /* Do all elements that might appear in such a chunk. */
   
   default:
     dserror(‘̈element type %d unsupported‘̈, actele->eltyp);
   }

And finally we advance the counter that controls the placement of the
element’s data in the memory buffers:

       counter += 1;

This way all element data is collected and can be transfered to the
processor that’s supposed to write it.

That’s a rough sketch of what happens when writing restart data. The big
gap in the middle remains to be filled with advanced material. See
below.

Reading
~~~~~~~

The function
``restart_read_bin_fluiddyn`` is
very similar to its write counterpart. However, there are differences.
Each field is read just once, that’s why there’s no strange ``context``
argument here but instead we have to initialize a local context
variable. That’s done by the function call:

   init_bin_in_field(&context, sysarray_typ, sysarray,
                     actfield, actpart, actintra, disnum);

This function figures out which processor reads what part of the data
and opens the discretization’s data files named by the control file. All
the internal variables are set up properly. And because there is dynamic
memory involved we need to clean things up afterwards. Thus the last
line of ``restart_read_bin_fluiddyn`` is:

   ``destroy_bin_in_field(&context);``

Between these two calls there are again the three parts already known
from writing. At first the corresponding group from the control file is
searched:

   ``result_info = in_find_restart_group(actfield, disnum, step);``

This will search for a group called ``restart`` with matching field
name, discretization number and step count. If there is no such group
the input is invalid and ccarat comes down via ``dserror``. That’s the
general idea of ccarat’s input facilities: Stop if something went wrong.
In this call the whole control file magic is hidden.

In the second part the node arrays are read. The ``sol_mf``
array might not be there so we have to check first:

::

   in_node_arrays(&context, result_info, node_array_sol);
   in_node_arrays(&context, result_info, node_array_sol_increment);
   if (map_has_map(result_info, ‘̈sol_mf‘̈)) {
     in_node_arrays(&context, result_info, node_array_sol_mf);
   | }

And finally we need to read the element data:

   ``in_element_chunk(&context, result_info, ‘̈element_data‘̈, cc_restart_element);``

This, again, needs some more attention because it’s a element specific
read. This time the io module has means to figure out how large the data
to be read are (these numbers are in the control file) but the ability
to store the values back into the elements must be provided for each
kind of chunk.

The input function that matches
``out_pack_items`` is called
``in_unpack_items`` and lives in
``io_packing.c`` as well. It calls the corresponding unpack
function,
``in_unpack_restart_element`` in
case of a restart read.

This functions is called once for each processor that reads data. Each
time one processor’s data is handed in and the task consists of
distributing it to the elements. For this reason there is a loop over
the received items:

   ``for (el=0; el<get_recv_numele; ++el) {``

For each item the receiving array is found and the values are copied. Of
course that’s again element specific:

::

   switch (actele->eltyp) {
   
     /* Handle reading all kinds of elements */
   
   default:
     dserror("element type %d unsupported" actele->eltyp);
   | }

A slight complication araises again from the distinction between
sequential and parallel version. The sequential version knows that all
elements are read on its one and only processor. Therefore it simply
loops all its elements. The parallel version, however, handles each
source processor in turn. So the number of elements in the loop depends
on the current source processor and has to be looked up from the context
data. The same goes for the element’s position inside the local
partition. These peculiarities, however, can be hidden by the
preprocessor so that we can use the same code for both versions.

.. _distributed-vectors-1:

Distributed Vectors
~~~~~~~~~~~~~~~~~~~

The restart functions for structure algorithms need to store distributed
vectors additional to node and element arrays. However, that easy. There
are the functions ``out_distvec_chunk`` and
``in_distvec_chunk`` that do for these vectors
what ``out_node_arrays`` and
``in_node_arrays`` do for node arrays. There’s
no user supplied low level code needed.

Restart conclusion
~~~~~~~~~~~~~~~~~~

The ``restart_write_*`` and
``restart_read_*`` functions from
``io_restart.h`` are public functions of the io module, but
at the same time these are functions any developer might want to write.
But that’s easy because they build upon the io module’s “internal
interface”, a set of functions that is meant to be used within the io
module without showing too much of the implementation details.

To handle the elements one has to consider low level details. There is
no way to avoid that. But there’s a clean cut: You can circumvent the
main machinery and just look at how data is extracted from and put into
elements.

Final restart remark
~~~~~~~~~~~~~~~~~~~~

The nice thing about the new restart is that you don’t need to move your
files even when you restart many times. The restart setup will
automatically find a new name for your files that has not been used
before. Imagine a restart call like the following:

   ``./baci-release inputfile outputname restart``

Here the ``outputname`` will be changed to ``outputname-1``. If there’s
a file called ``outputname-1.control`` the name will change to
``outputname-2`` and so on. However, if you start with:

   ``./baci-release inputfile outputname-7 restart``

the first try will be ``outputname-8``, going on to ``outputname-9``.
But please note that only the existence of the control file is tested.
That’s fine as long as you don’t change the data files’ names. Also note
that it might be a bad idea to end your output names with a minus sign
and a number.

Postprocessing output
---------------------

The general idea of restart is to copy data from memory to the disc and
be able to copy it back. No transformations whatsoever are performed.
The postprocessing output is different in this respect. Here meaningful
data is to be written. In particular data that can be interpreted
outside of ccarat’s internal data structures. So it’s no longer
sufficient to simply dump all the node arrays. Instead we are interested
in a few specific entries, one row for example.

It follows that there is no use to read this data back into ccarat.
(Instead we have to write filter applications that generate input
suitable for GiD and others.) So the output part of the io module is
going to be bigger that the input part.

The topmost output function for general postprocessing output is the
``out_results`` function that has already been used in the
example above. It is again defined in ``in_packing.c``. You
tell it what kinds of output you want to get. The supported output types
are the ones known from the old text output:

-  displacement

-  velocity

-  pressure

-  stress

-  contact

-  eigenmodes

-  thickness

-  axi\_loads

It’s the task of this function to figure out what type of problem is
running and what kind of chunks are needed accordingly. In order to do
so it relies on the knowledge about the types of elements in the current
discretization that was gathered during initialization. (See below.)

The function ``out_results`` is a public one like the
restart functions and the same reasoning applies here. You might want to
enhance it, but you should be able to get by with the “internal
interface” functions of the io module apart from the element handling.
To do special things with elements you might have to introduce new
``cc_*`` constants and corresponding low level functions.
The mechanism is the same as explained for the restart case above.

Types of Elements
-----------------

The function ``init_bin_out_fluid``
has to figure out, among other things, what kind of elements there are
in this discretization. However, the simple ``ELEMENT_TYP``
enum is not sufficient for the purpose of io. Additional to the element
type already present in ccarat the number of nodes and gauss points is
important. So each triple (type, #nodes, #gauss points) counts as
different kind of element.

In order to keep things manageable I introduced a new classification
scheme that consists of major and minor version numbers. Each possible
combination of element type, node numbers and gauss point numbers is
identified by a unique major, minor pair where the major number
coincides with the common element type and the minor number steams from
a consecutive counting of the element’s variants. This way the
``ELEMENT_FLAGS`` type can be defined to be a two
dimensional array with fixed size. The function
``out_find_element_types``
initializes this array to zero and afterwards sets each entry
corresponding to an element type found in the discretization to one.

This arrangement might look like an implementation detail but it has
some important consequences. First of all it enables us to handle
different element types in a systematic manner. There’s the global
variable ``element_info`` (from
``global_element_info.c``) that contains
information about the different element variants known to ccarat by
major and minor number. Secondly we can use the major and minor numbers
to identify the elements in the output. That’s obviously needed because
the postprocessing output needs to be interpreted without the ccarat
input file’s aid. Therefore the type of each element has to be written
along with the mesh connectivity.

These numbers are written to result files and those files are going to
stay around for a while, so the meaning of these numbers must be
preserved. On the other hand are the values of these numbers
implementation details, we simply cannot guarantee that there will be no
changes. For this reason we need to distinguish internal and external
versions of these number.

It’s really very simple. The ccarat code always works with the internal
numbers. The numbers written to a file are always external. This demands
that on reading we convert the external numbers we read to internal
numbers we can work with. (On writing no explicit conversion is needed.)
To facilitate this conversion there are ``element_variant``
groups in each discretization’s main group in the control file. For
example a 2d fluid problem might contain the group:

::

   element_variant:
       type = ‘"luid2"
       major = 7
       minor = 0
       numnp = 4
       nGP0 = 2

This states that the element with the (external) major number 7 and
minor number 0 used in this discretization is a fluid2 element with four
nodes and two gauss points (in each direction).

There must be a ``element_variant`` group for every type of
element used in a discretization. On reading the filters are able to
find the internal major and minor numbers corresponding to the given
criteria (fluid2, four nodes, two gauss points in this case).

Please note that due to the flexibility of the control file it’s
possible to specify any criteria needed to distinguish an element
variant.

Mesh Connectivity
-----------------

As mentioned above we have to store the element connectivity along with
each discretization. The
``init_bin_out_field`` function does
this automatically, utilizing the normal io facilities. The relevant
lines are close to the end of this function:

   find_mesh_item_length(context, &value_length, &size_length); `` 
   out_element_chunk(context, ‘̈mesh‘̈, cc_mesh, value_length, size_length);``

Here ``find_mesh_item_length``
figures out the length of both types of entries, the entries in the
value array as well as the entries in the size array. Keep in mind that
both data files consist of individual chunks, each chunk beeing divided
into entries of constant length. This way the entries can be accessed by
their index.

The next line in
``init_bin_out_field`` stores the
node’s coordinates. To do this ``genprob.ndim`` double values are needed
and one integer (that’s known and thus no find\_\* function is needed
here):

   ``out_node_chunk(context, ‘̈coords‘̈, cc_coords, value_length, size_length, 0);``

These function calls write the ``mesh`` and ``coords`` chunks we’ve
already discussed above.

The communication layer
-----------------------

The functional core of the io module is the communication layer that
lives in ``io_singlefile.c``. The communication happens each
time a chunk is read or written. It’s the function
``in_scatter_chunk`` that takes the data read by
each processor and distributes it to the processor where the
corresponding node or element lives. On the other side it’s
``out_gather_values`` which collects the data
and distributes it to the processor where it is written. This
communication process happens in a chattering style.

Chattering processors communication pattern
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This is a particular clever way to arrange your code when each pair of
processors needs to exchange a different piece of information. That’s in
contrast to the famous round robin scheme which is used when each
processor has got some piece of information that needs to be
communicated to everyone. I learnt about this pattern from Dr. Behr, but
he claimed not to be the originator of it so I refrained from using his
name. (I was tempted, though.) Peter suggested the name chattering
processors.

The central idea of this scheme is to arrange the processor in a
circular list. Then each processor sends data to its right neighbour and
receives from its left. There is the ``MPI_Sendrecv``
function that handles sending and receiving at the same time. In the
next step data is sent to the right but one neighbour and received from
the left but one neighbour accordingly. And so on until every processor
has talked to every other.

The central point is to find in each iteration ``i`` the processor the
local one has to talk to:

::

   dst = (rank + i + 1) % nprocs;
   src = (nprocs + rank - i - 1) % nprocs;

If we know how many numbers we have to send and receive the MPI call
itself is pretty simple:

::

   err = MPI_Sendrecv(send_buf, send_count, MPI_DOUBLE, dst, tag_base+i,
                      recv_buf, recv_count, MPI_DOUBLE, src, tag_base+i,
                      context->actintra->MPI_INTRA_COMM, &status);

To figure out these counts some more communication is needed. But
luckily these counts are the same for all chunks of one type (in one
discretization). Thus we can find them in the setup phase (in
``init_bin_in_field`` and
``init_bin_out_field``) and use them
afterwards.

There are two variants to this communication pattern that differ in
whether each processor has to talk to itself. In this case that’s
needed, thus the loop starts like this:

   ``for (i=0; i<nprocs; ++i) {``

and in the last iteration ``src == dst == rank``. But this case is
handled by ``MPI_Sendrecv`` equally well (without
communication). No special attention is needed.

Module data structures
----------------------

The io module is split into an input and an output part. In each part
there is a data structure hierarchy consisting of the main structure
(``BIN_IN_MAIN``,
``BIN_OUT_MAIN``), the per discretization
structure (``BIN_IN_FIELD``,
``BIN_OUT_FIELD``) and the per chunk structure
(``BIN_IN_CHUNK``,
``BIN_OUT_CHUNK``). There is exactly one object
of the main structure.
