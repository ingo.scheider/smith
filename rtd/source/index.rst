.. BACI-documentation documentation master file, created by
   sphinx-quickstart on Wed Nov 23 14:27:39 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

BACI - the Bavarian Advanced Computation Initiative
====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   welcome
   tutorials
   analysisguide
   developmentguide
   parameterreference
   tools
   appendix



