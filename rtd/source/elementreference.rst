.. _elements:

Element reference
==================

.. toctree::

   elementtypes
   elementshapes

.. note::

   Maybe the element shapes are not modified that often, so that one can include them statically,
   as shown in the :ref:`conventions`.