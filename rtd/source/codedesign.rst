old_The BACI code design
========================

You have read so many chapters of the BACI guide already, now here we
come to the stuff you really want to know. But please be careful. This
is meant to serve as an introduction to a very detail rich topic. There
are a few simple statements here so you can get a broad picture. You
will have to dig deeper to find the exceptions.

Design goals
------------

BACI aims to be a multi-field, multi-purpose, multi-younameit parallel
FE package that supports all kinds of weird use cases. Possible types of
calculations BACI supports include

Single field
   The obvious one. One mesh with nodes, elements, dofs. One time loop.
   Examples are structural or fluid dynamics.

Multi field
   Several physical fields. FSI calculations for example. Three
   different fields with one mesh each. Accordingly three different
   kinds of elements, three distinct (but intervened) solution
   algorithms.

Multi mesh
   Calculations with several meshes in one physical field. Examples
   include fluid calculations with background mesh and moving (rotating)
   front mesh. (There is currently no implementation of this.)

Multi dof set
   Some calculation have to solve different equations on the same mesh,
   like fluid projection algorithms. This can be done with one mesh, you
   just have to exchange the dof numbers. (Projection method is
   unimplemented right now.)

Multi problem
   And if you happen to suffer from multiple problems at once, you might
   end up reading many input files at the same time. This happens with
   micro-macro couplings for example.

All these use cases come with their specific algorithmic challenges,
their build in hassle and so on. The mechanical content is outside of
this discussion. But BACI provides the data structures to handle such
things. The design of BACI was meant to make such problem types (easily)
possible.

Fundamental building blocks
---------------------------

From the above list of anticipated use cases the most fundamental
building blocks can be derived.

#. Central to each FE package is the mesh with its elements and nodes.
   These are available via the classes ``DRT::Discretization``,
   ``DRT::Element`` and ``DRT::Node`` (and the respective subclasses.)
   In BACI we have many ``DRT::Element`` subclasses. Each subclasses
   provides a physically distinct element type.

#. For flexibility the dofs are not directly attached to nodes (or
   elements), but are managed by an ``DRT::DofSet`` object. This is an
   internal detail most users do not need to know. But this way multiple
   global systems with different sizes are possible.

#. The calculation of a particular case involves a solution algorithm,
   that is a time loop, a nonlinear Newton iteration, an embedded linear
   solver and all the glorious details. This sounds pretty much like a
   large function, but in BACI algorithms are actually objects. This
   helps to combine different algorithms to obtain a larger one. For
   example the FSI algorithm class uses the structural and the fluid
   algorithm classes for the respective fields.

#. All meshes of the problem are stored by the global ``DRT::Problem``
   object. There exists one object of this class. The whole setup (that
   is dat file input) is managed here.

#. Actually there is one ``DRT::Problem`` object for each dat file you
   read. So most of the time there is one such object. Only very special
   problems require multiple dat file to be read at the same time.



Parallel calculation building blocks
------------------------------------

BACI is a parallel code. Parallelization is gained by data splitting.
That is each processor performs the same steps, everyone calculates the
same thing, only the numbers are different. The whole picture is
obtained by combining the results from all processors.

This kind of parallelization is the simplest one possible. Most of the
time you do not have to think of different processors at all.

At the heart of its parallelization there are a few classes from the
trilinos library Epetra. The most important ones are, in increasing
complexity.

Epetra_Comm
   provides basic communication mechanisms. An object of ``Epetra_Comm``
   is needed by every other Epetra class.

Epetra_Map
   describes the parallel distribution of things. Things can be anything
   as long as each one has an unique id. ``Epetra_Map`` describes the
   parallel distribution of these ids.

Epetra_Vector
   is a parallel vector that is distributed based on a ``Epetra_Map``.

Epetra_CrsMatrix
   is a parallel sparse matrix. Here one ``Epetra_Map`` object
   (non-overlapping) describes the distribution of the matrix rows.
   Another ``Epetra_Map`` object (overlapping) describes the
   distribution of the columns inside each row.

Of course the base classes of those are just as important. It is a very
good idea to became familiar with Epetra. There is doxygen documentation
on the trilinos website.

So Epetra provides basic linear algebra objects. The overall FE
algorithms in BACI are constructed using these objects. At this level
parallelization comes (almost) by itself.

Input machinery
---------------

Before the calculation starts BACI performes a rather elaborated input
step. The goal of this is to construct and setup the ``DRT::Problem``
object according to the dat file contents. The main content of the
``DRT::Problem`` object are the ``DRT::Discretization`` objects, one for
each mesh. A ``DRT::Discretization`` object knows two states: filled and
non-filled. After the input phase all ``DRT::Discretization`` objects
are in filled state, that is all elements and nodes are in their final
parallel distribution, all dofs are assigned, all maps constructed and
everything is finished to launch into the calculation.

During the input phase the ``DRT::INPUT::DatFileReader`` class and other
helper classes from ``DRT::INPUT`` are used. These objects are destroyed
once the input step is finished.

Different things need to be read from the input file.

-  General parameters. (key, value) pairs. That is a large
   ``Teuchos::ParameterList`` with lots of sublists.

-  Node ids and coordinates

-  Element connectivity and element flags

-  (boundary) conditions connected to sets of nodes

-  time curve definitions

-  spacial function definitions

-  material definitions

-  result test values (test runs only)

From these items element and nodal data are the voluminous ones. In a
parallel run we do not read all elements or nodes at once in one
processor. Instead processor 0 reads and constructs subsets of elements
and distributs them to another processor before the next subset is read.
This is performed by the ``DRT::INPUT::DatFileReader`` with the help of
``DRT::INPUT::ElementReader`` and ``DRT::INPUT::NodeReader``.

All other dat file sections are distributed to all processors inside
``DRT::INPUT::DatFileReader`` and read on each processor. This way all
processors get to know all flags and conditions.

Further documentation
---------------------

BACI itself comes with a very good doxygen documentation. The
``Doxyfile`` is in the ``doc`` directory. Use it. It gives a nice
picture of namespaces and classes in BACI. The following books might be a bit outdated, but they may still serve as a starting point for learning the basics of C++: [Meyers98]_, [Meyers01]_, [Stroustrup98]_, [Alexandrescu01]_.

