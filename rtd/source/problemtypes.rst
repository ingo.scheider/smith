.. _problemtypes:

Problem types
==============

.. ToDo:

   Here, we have to describe the different types that can be calculated with BACI.

.. _singlefieldproblems:

Single field problems:
----------------------

Ale
~~~~

**General**

This problemtype considers . 

**Elements and Degrees of freedom**

The element types used are given in :ref:`STRUCTURE ELEMENTS<structureelements>`. The number of degrees of freedom 

**Results**



ArterialNetwork
~~~~~~~~~~~~~~~

**General**

This problemtype considers . 

**Elements and Degrees of freedom**

The element types used are given in :ref:`STRUCTURE ELEMENTS<structureelements>`. The number of degrees of freedom 

**Results**


Cardiac_Monodomain
~~~~~~~~~~~~~~~~~~~~~~~~~

**General**

This problemtype considers . 

**Elements and Degrees of freedom**

The element types used are given in :ref:`STRUCTURE ELEMENTS<structureelements>`. The number of degrees of freedom 

**Results**

Internally: 
At Dirichlet boundary conditions: 


Fluid
~~~~~~~~~~~~~~~~~~~~~~~~~

**General**

This problemtype considers . 

**Elements and Degrees of freedom**

The element types used are given in :ref:`STRUCTURE ELEMENTS<structureelements>`. The number of degrees of freedom 

**Results**

Internally: 
At Dirichlet boundary conditions: 


Electrochemistry
~~~~~~~~~~~~~~~~~~~~~~~~~

**General**

This problemtype considers . 

**Elements and Degrees of freedom**

The element types used are given in :ref:`STRUCTURE ELEMENTS<structureelements>`. The number of degrees of freedom 

**Results**

Internally: 
At Dirichlet boundary conditions: 


Electromagnetics
~~~~~~~~~~~~~~~~~~~~~~~~~

**General**

This problemtype considers . 

**Elements and Degrees of freedom**

The element types used are given in :ref:`STRUCTURE ELEMENTS<structureelements>`. The number of degrees of freedom 

**Results**

Internally: 
At Dirichlet boundary conditions: 


Fluid_Top_Opt
~~~~~~~~~~~~~~~~~~~~~~~~~

**General**

This problemtype considers . 

**Elements and Degrees of freedom**

The element types used are given in :ref:`STRUCTURE ELEMENTS<structureelements>`. The number of degrees of freedom 

**Results**

Internally: 
At Dirichlet boundary conditions: 


Fluid_XFEM
~~~~~~~~~~~~~~~~~~~~~~~~~

**General**

This problemtype considers . 

**Elements and Degrees of freedom**

The element types used are given in :ref:`STRUCTURE ELEMENTS<structureelements>`. The number of degrees of freedom 

**Results**

Internally: 
At Dirichlet boundary conditions: 


Inverse_Analysis
~~~~~~~~~~~~~~~~~~~~~~~~~

**General**

This problemtype considers . 

**Elements and Degrees of freedom**

The element types used are given in :ref:`STRUCTURE ELEMENTS<structureelements>`. The number of degrees of freedom 

**Results**

Internally: 
At Dirichlet boundary conditions: 


Level_Set
~~~~~~~~~~~~~~~~~~~~~~~~~

**General**

This problemtype considers . 

**Elements and Degrees of freedom**

The element types used are given in :ref:`STRUCTURE ELEMENTS<structureelements>`. The number of degrees of freedom 

**Results**

Internally: 
At Dirichlet boundary conditions: 


Particle
~~~~~~~~~~~~~~~~~~~~~~~~~

**General**

This problemtype considers . 

**Elements and Degrees of freedom**

The element types used are given in :ref:`STRUCTURE ELEMENTS<structureelements>`. The number of degrees of freedom 

**Results**

Internally: 
At Dirichlet boundary conditions: 


Polymer_Network
~~~~~~~~~~~~~~~~~~~~~~~~~

**General**

This problemtype considers . 

**Elements and Degrees of freedom**

The element types used are given in :ref:`STRUCTURE ELEMENTS<structureelements>`. The number of degrees of freedom 

**Results**

Internally: 
At Dirichlet boundary conditions: 


ReducedDimensionalAirWays
~~~~~~~~~~~~~~~~~~~~~~~~~

**General**

This problemtype considers . 

**Elements and Degrees of freedom**

The element types used are given in :ref:`STRUCTURE ELEMENTS<structureelements>`. The number of degrees of freedom 

**Results**

Internally: 
At Dirichlet boundary conditions: 


Scalar_Transport
~~~~~~~~~~~~~~~~~~~~~~~~~

**General**

This problemtype considers . 

**Elements and Degrees of freedom**

The element types used are given in :ref:`STRUCTURE ELEMENTS<structureelements>`. The number of degrees of freedom 

**Results**

Internally: 
At Dirichlet boundary conditions: 


Structure
~~~~~~~~~~~~~~~~~~~~~~~~~

**General**

The problemtype *Structure* considers deformations and stresses in solid mechanics. 

**Elements and Degrees of freedom**

The element types used are given in :ref:`STRUCTURE ELEMENTS<structureelements>`. The number of degrees of freedom depends on the element type.
In general, all elements have displacements in spatial directions (2 or 3, depending on the dimensionality).
In the case of C1-steady elements like beams and shells, the rotations (again 2 or 3) are added to the degrees of freedom,
so there are up to 6 DoFs.

**Results**

The result variables are stresses and strains within the elements, 
and reaction forces at the nodes, where a Dirichlet boundary condition has been applied.
Other internal variables may be calculated as necessary (and desired).

Thermo
~~~~~~~~~~~~~~~~~~~~~~~~~

**General**

The problemtype *Thermo* considers heat transfer in arbitrary structures. 

**Elements and Degrees of freedom**

The element types used are given in :ref:`THERMO ELEMENTS<thermoelements>`. There is only one degree of freedom, that is the temperature.

**Results**

Internally: Heat flux per area
At Dirichlet boundary conditions: heat flux



.. _multifieldproblems:

Multi field problems:
----------------------

These problems combine a number of single field problems, and are therfore sometimes called *Coupled Problems*.

Atherosclerosis_Fluid_Structure_Interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Biofilm_Fluid_Structure_Interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Elastohydrodynamic_Lubrication
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: STRUCTURAL | LUBRICATION | ELASTO HYDRO

Fluid_Ale
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics:  FLUID | ALE | FSI

Fluid_Beam_Interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: FSI | FLUID | STRUCTURAL

Fluid_Freesurface
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: FLUID | FSI | ALE


Fluid_Poro_Structure_Interaction_XFEM
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: STRUCTURAL | POROELASTICITY | FSI | FLUID

Fluid_Porous_Structure_Interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 



Fluid_Porous_Structure_Scalar_Scalar_Interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Fluid_RedModels
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Fluid_Structure_Interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Fluid_Structure_Interaction_Lung
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Fluid_Structure_Interaction_RedModels
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Fluid_Structure_Interaction_XFEM
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Fluid_XFEM_LevelSet
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Gas_Fluid_Structure_Interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Immersed_FSI
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Low_Mach_Number_Flow
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Lubrication
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Multiphase_Poroelasticity
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Multiphase_Poroelasticity_ScaTra
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Multiphase_Porous_Flow
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

NP_Supporting_Procs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Particle_Structure_Interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Poroelastic_scalar_transport
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Poroelasticity
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

RedAirways_Tissue
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Scalar_Thermo_Interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Structure_Ale
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Structure_Scalar_Interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Structure_Scalar_Thermo_Interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Thermo_Fluid_Structure_Interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Thermo_Structure_Interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Tutorial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

Two_Phase_Flow
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One has to define solvers for the following dynamics: 

