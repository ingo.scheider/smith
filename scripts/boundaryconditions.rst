.. _boundaryconditionreference:

Boundary condition reference
=============================

1D ARTERY IN_OUTLET CONDITION
-----------------------------

::

   --DESIGN NODE 1D ARTERY IN_OUTLET CONDITION

The following geometry can be treated with this condition:

   - NODE 

1D ARTERY JUNCTION CONDITION
----------------------------

::

   --DESIGN NODE 1D ARTERY JUNCTION CONDITION

The following geometry can be treated with this condition:

   - NODE 

1D ARTERY PRESCRIBED CONDITION
------------------------------

::

   --DESIGN NODE 1D ARTERY PRESCRIBED CONDITION

The following geometry can be treated with this condition:

   - NODE 

1D ARTERY REFLECTIVE CONDITION
------------------------------

::

   --DESIGN NODE 1D ARTERY REFLECTIVE CONDITION

The following geometry can be treated with this condition:

   - NODE 

1D ARTERY SCATRA PRESCRIBED CONDITION
-------------------------------------

::

   --DESIGN NODE 1D ARTERY SCATRA PRESCRIBED CONDITION

The following geometry can be treated with this condition:

   - NODE 

1D ARTERY TO POROFLUID COUPLING CONDITION
-----------------------------------------

::

   --DESIGN NODE 1D ARTERY TO POROFLUID COUPLING CONDITION

The following geometry can be treated with this condition:

   - NODE 

1D ARTERY TO SCATRA COUPLING CONDITION
--------------------------------------

::

   --DESIGN NODE 1D ARTERY TO SCATRA COUPLING CONDITION

The following geometry can be treated with this condition:

   - NODE 

1D ARTERY WINDKESSEL CONDITION
------------------------------

::

   --DESIGN NODE 1D ARTERY WINDKESSEL CONDITION

The following geometry can be treated with this condition:

   - NODE 

3D To REDUCED D FLOW COUPLING CONDITION
---------------------------------------

::

   --DESIGN SURF 3D To REDUCED D FLOW COUPLING CONDITION

The following geometry can be treated with this condition:

   - SURF 

ADHESION LINE CONDITION
-----------------------

::

   --DESIGN FOCAL ADHESION LINE CONDITION

The following geometry can be treated with this condition:

   - FOCAL 

ADHESION SURF CONDITION
-----------------------

::

   --DESIGN FOCAL ADHESION SURF CONDITION

The following geometry can be treated with this condition:

   - FOCAL 

ALE COUPLING SURF CONDITION
---------------------------

::

   --DESIGN STRUCTURE ALE COUPLING SURF CONDITION

The following geometry can be treated with this condition:

   - STRUCTURE 

ALE DIRICH CONDITION
--------------------

::

   --DESIGN POINT ALE DIRICH CONDITION
   --DESIGN LINE ALE DIRICH CONDITION
   --DESIGN SURF ALE DIRICH CONDITION
   --DESIGN VOL ALE DIRICH CONDITION

The following geometry can be treated with this condition:

   - POINT 
   - LINE 
   - SURF 
   - VOL 

ALE LOCSYS CONDITION
--------------------

::

   --DESIGN POINT ALE LOCSYS CONDITION
   --DESIGN LINE ALE LOCSYS CONDITION
   --DESIGN SURF ALE LOCSYS CONDITION
   --DESIGN VOL ALE LOCSYS CONDITION

The following geometry can be treated with this condition:

   - POINT 
   - LINE 
   - SURF 
   - VOL 

ALE WEAR CONDITIONS 2
---------------------

::

   --DESIGN LINE ALE WEAR CONDITIONS 2

The following geometry can be treated with this condition:

   - LINE 

AND MEAN SCALAR LINE CONDITION
------------------------------

::

   --DESIGN TOTAL AND MEAN SCALAR LINE CONDITION

The following geometry can be treated with this condition:

   - TOTAL 

AND MEAN SCALAR SURF CONDITION
------------------------------

::

   --DESIGN TOTAL AND MEAN SCALAR SURF CONDITION

The following geometry can be treated with this condition:

   - TOTAL 

AND MEAN SCALAR VOL CONDITION
-----------------------------

::

   --DESIGN TOTAL AND MEAN SCALAR VOL CONDITION

The following geometry can be treated with this condition:

   - TOTAL 

AREA CONSTRAINT 2
-----------------

::

   --DESIGN LINE AREA CONSTRAINT 2

The following geometry can be treated with this condition:

   - LINE 

AREA CONSTRAINT 3
-----------------

::

   --DESIGN SURFACE AREA CONSTRAINT 3

The following geometry can be treated with this condition:

   - SURFACE 

AREA CONSTRAINT 3D PE
---------------------

::

   --DESIGN SURFACE AREA CONSTRAINT 3D PE

The following geometry can be treated with this condition:

   - SURFACE 

AREA MONITOR 2
--------------

::

   --DESIGN LINE AREA MONITOR 2

The following geometry can be treated with this condition:

   - LINE 

AREA MONITOR 3
--------------

::

   --DESIGN SURFACE AREA MONITOR 3

The following geometry can be treated with this condition:

   - SURFACE 

BEAM FILAMENT CONDITION
-----------------------

::

   --DESIGN LINE BEAM FILAMENT CONDITION

The following geometry can be treated with this condition:

   - LINE 

BEAM POTENTIAL CHARGE CONDITION
-------------------------------

::

   --DESIGN LINE BEAM POTENTIAL CHARGE CONDITION

The following geometry can be treated with this condition:

   - LINE 

CARDIOVASCULAR 0D 4-ELEMENT WINDKESSEL CONDITION
------------------------------------------------

::

   --DESIGN SURF CARDIOVASCULAR 0D 4-ELEMENT WINDKESSEL CONDITION

The following geometry can be treated with this condition:

   - SURF 

CARDIOVASCULAR 0D ARTERIAL PROX DIST CONDITION
----------------------------------------------

::

   --DESIGN SURF CARDIOVASCULAR 0D ARTERIAL PROX DIST CONDITION

The following geometry can be treated with this condition:

   - SURF 

CARDIOVASCULAR 0D SYS-PUL CIRCULATION CONDITION
-----------------------------------------------

::

   --DESIGN SURF CARDIOVASCULAR 0D SYS-PUL CIRCULATION CONDITION

The following geometry can be treated with this condition:

   - SURF 

CARDIOVASCULAR 0D-STRUCTURE COUPLING CONDITION
----------------------------------------------

::

   --DESIGN SURF CARDIOVASCULAR 0D-STRUCTURE COUPLING CONDITION

The following geometry can be treated with this condition:

   - SURF 

CARDIOVASCULAR RESPIRATORY 0D SYS-PUL PERIPH CIRCULATION CONDITION
------------------------------------------------------------------

::

   --DESIGN SURF CARDIOVASCULAR RESPIRATORY 0D SYS-PUL PERIPH CIRCULATION CONDITION

The following geometry can be treated with this condition:

   - SURF 

CELL CYCLING LINE CONDITION
---------------------------

::

   --DESIGN CCCV CELL CYCLING LINE CONDITION

The following geometry can be treated with this condition:

   - CCCV 

CELL CYCLING SURF CONDITION
---------------------------

::

   --DESIGN CCCV CELL CYCLING SURF CONDITION

The following geometry can be treated with this condition:

   - CCCV 

CONSERVATIVE OUTFLOW CONSISTENC
-------------------------------

::

   --DESIGN SURFACE CONSERVATIVE OUTFLOW CONSISTENC

The following geometry can be treated with this condition:

   - SURFACE 

CONVECTION LINE CONDITION
-------------------------

::

   --DESIGN THERMO CONVECTION LINE CONDITION

The following geometry can be treated with this condition:

   - THERMO 

CONVECTION SURF CONDITION
-------------------------

::

   --DESIGN THERMO CONVECTION SURF CONDITION

The following geometry can be treated with this condition:

   - THERMO 

COUPLING CENTER DISP LINE CONDITION
-----------------------------------

::

   --DESIGN FSI COUPLING CENTER DISP LINE CONDITION

The following geometry can be treated with this condition:

   - FSI 

COUPLING CENTER DISP SURF CONDITION
-----------------------------------

::

   --DESIGN FSI COUPLING CENTER DISP SURF CONDITION

The following geometry can be treated with this condition:

   - FSI 

COUPLING CONDITION
------------------

::

   --DESIGN POINT COUPLING CONDITION

The following geometry can be treated with this condition:

   - POINT 

COUPLING GROWTH LINE CONDITION
------------------------------

::

   --DESIGN S2I COUPLING GROWTH LINE CONDITION

The following geometry can be treated with this condition:

   - S2I 

COUPLING GROWTH SURF CONDITION
------------------------------

::

   --DESIGN S2I COUPLING GROWTH SURF CONDITION

The following geometry can be treated with this condition:

   - S2I 

COUPLING LINE CONDITION
-----------------------

::

   --DESIGN S2I COUPLING LINE CONDITION
   --DESIGN FSI COUPLING LINE CONDITION
   --DESIGN FPSI COUPLING LINE CONDITION
   --DESIGN IMMERSED COUPLING LINE CONDITION
   --DESIGN SSI COUPLING LINE CONDITION

The following geometry can be treated with this condition:

   - S2I 
   - FSI 
   - FPSI 
   - IMMERSED 
   - SSI 

COUPLING NO SLIDE LINE CONDITION
--------------------------------

::

   --DESIGN FSI COUPLING NO SLIDE LINE CONDITION

The following geometry can be treated with this condition:

   - FSI 

COUPLING NO SLIDE SURF CONDITION
--------------------------------

::

   --DESIGN FSI COUPLING NO SLIDE SURF CONDITION

The following geometry can be treated with this condition:

   - FSI 

COUPLING SCATRATOSOLID LINE CONDITION
-------------------------------------

::

   --DESIGN SSI COUPLING SCATRATOSOLID LINE CONDITION

The following geometry can be treated with this condition:

   - SSI 

COUPLING SCATRATOSOLID SURF CONDITION
-------------------------------------

::

   --DESIGN SSI COUPLING SCATRATOSOLID SURF CONDITION

The following geometry can be treated with this condition:

   - SSI 

COUPLING SCATRATOSOLID VOL CONDITION
------------------------------------

::

   --DESIGN SSI COUPLING SCATRATOSOLID VOL CONDITION

The following geometry can be treated with this condition:

   - SSI 

COUPLING SOLIDTOSCATRA LINE CONDITION
-------------------------------------

::

   --DESIGN SSI COUPLING SOLIDTOSCATRA LINE CONDITION

The following geometry can be treated with this condition:

   - SSI 

COUPLING SOLIDTOSCATRA SURF CONDITION
-------------------------------------

::

   --DESIGN SSI COUPLING SOLIDTOSCATRA SURF CONDITION

The following geometry can be treated with this condition:

   - SSI 

COUPLING SOLIDTOSCATRA VOL CONDITION
------------------------------------

::

   --DESIGN SSI COUPLING SOLIDTOSCATRA VOL CONDITION

The following geometry can be treated with this condition:

   - SSI 

COUPLING SURF CONDITION
-----------------------

::

   --DESIGN S2I COUPLING SURF CONDITION
   --DESIGN SCATRA COUPLING SURF CONDITION
   --DESIGN FSI COUPLING SURF CONDITION
   --DESIGN FPSI COUPLING SURF CONDITION
   --DESIGN IMMERSED COUPLING SURF CONDITION
   --DESIGN SSI COUPLING SURF CONDITION

The following geometry can be treated with this condition:

   - S2I 
   - SCATRA 
   - FSI 
   - FPSI 
   - IMMERSED 
   - SSI 

COUPLING SURF CONDITIONS / PARTITIONIN
--------------------------------------

::

   --DESIGN S2I COUPLING SURF CONDITIONS / PARTITIONIN

The following geometry can be treated with this condition:

   - S2I 

COUPLING VOL CONDITION
----------------------

::

   --DESIGN SSI COUPLING VOL CONDITION

The following geometry can be treated with this condition:

   - SSI 

COUPLING VOL CONDITIONS / PARTITIONIN
-------------------------------------

::

   --DESIGN S2I COUPLING VOL CONDITIONS / PARTITIONIN

The following geometry can be treated with this condition:

   - S2I 

CURRENT EVALUATION CONDITIO
---------------------------

::

   --DESIGN SURFACE CURRENT EVALUATION CONDITIO

The following geometry can be treated with this condition:

   - SURFACE 

DIRICH CONDITION
----------------

::

   --DESIGN POINT DIRICH CONDITION
   --DESIGN LINE DIRICH CONDITION
   --DESIGN SURF DIRICH CONDITION
   --DESIGN VOL DIRICH CONDITION

The following geometry can be treated with this condition:

   - POINT 
   - LINE 
   - SURF 
   - VOL 

DISPLACEMENT SURF CONDITION
---------------------------

::

   --DESIGN XFEM DISPLACEMENT SURF CONDITION

The following geometry can be treated with this condition:

   - XFEM 

EHL MORTAR COUPLING CONDITIONS 2
--------------------------------

::

   --DESIGN LINE EHL MORTAR COUPLING CONDITIONS 2

The following geometry can be treated with this condition:

   - LINE 

EHL MORTAR COUPLING CONDITIONS 3
--------------------------------

::

   --DESIGN SURF EHL MORTAR COUPLING CONDITIONS 3

The following geometry can be treated with this condition:

   - SURF 

EXPORT 1D-ARTERIAL NETWORK GNUPLOT FORMA
----------------------------------------

::

   --DESIGN LINE EXPORT 1D-ARTERIAL NETWORK GNUPLOT FORMA

The following geometry can be treated with this condition:

   - LINE 

FLOW-DEPENDENT PRESSURE CONDITION
---------------------------------

::

   --DESIGN LINE FLOW-DEPENDENT PRESSURE CONDITION
   --DESIGN SURFACE FLOW-DEPENDENT PRESSURE CONDITION

The following geometry can be treated with this condition:

   - LINE 
   - SURFACE 

FLUID COUPLING SURF CONDITION
-----------------------------

::

   --DESIGN FLUID FLUID COUPLING SURF CONDITION
   --DESIGN ALE FLUID COUPLING SURF CONDITION

The following geometry can be treated with this condition:

   - FLUID 
   - ALE 

FLUID VOLUME COUPLING SURF CONDITION
------------------------------------

::

   --DESIGN STRUCTURE FLUID VOLUME COUPLING SURF CONDITION

The following geometry can be treated with this condition:

   - STRUCTURE 

FLUID VOLUME COUPLING VOL CONDITION
-----------------------------------

::

   --DESIGN STRUCTURE FLUID VOLUME COUPLING VOL CONDITION

The following geometry can be treated with this condition:

   - STRUCTURE 

FLUIDFLUID SURF CONDITION
-------------------------

::

   --DESIGN XFEM FLUIDFLUID SURF CONDITION

The following geometry can be treated with this condition:

   - XFEM 

FPI MONOLITHIC SURF CONDITION
-----------------------------

::

   --DESIGN XFEM FPI MONOLITHIC SURF CONDITION

The following geometry can be treated with this condition:

   - XFEM 

FREE SURFACE LINE CONDITION
---------------------------

::

   --DESIGN FLUID FREE SURFACE LINE CONDITION

The following geometry can be treated with this condition:

   - FLUID 

FREE SURFACE SURF CONDITION
---------------------------

::

   --DESIGN FLUID FREE SURFACE SURF CONDITION

The following geometry can be treated with this condition:

   - FLUID 

FSI MONOLITHIC SURF CONDITION
-----------------------------

::

   --DESIGN XFEM FSI MONOLITHIC SURF CONDITION

The following geometry can be treated with this condition:

   - XFEM 

FSI PARTITIONED SURF CONDITION
------------------------------

::

   --DESIGN XFEM FSI PARTITIONED SURF CONDITION

The following geometry can be treated with this condition:

   - XFEM 

GROWTH COUPLING LINE CONDITION
------------------------------

::

   --DESIGN BIOFILM GROWTH COUPLING LINE CONDITION

The following geometry can be treated with this condition:

   - BIOFILM 

GROWTH COUPLING SURF CONDITION
------------------------------

::

   --DESIGN BIOFILM GROWTH COUPLING SURF CONDITION

The following geometry can be treated with this condition:

   - BIOFILM 

HALF-CYCLE LINE CONDITION
-------------------------

::

   --DESIGN CCCV HALF-CYCLE LINE CONDITION

The following geometry can be treated with this condition:

   - CCCV 

HALF-CYCLE SURF CONDITION
-------------------------

::

   --DESIGN CCCV HALF-CYCLE SURF CONDITION

The following geometry can be treated with this condition:

   - CCCV 

HETEROGENEOUS REACTION LINE CONDITIONS / MASTE
----------------------------------------------

::

   --DESIGN SCATRA HETEROGENEOUS REACTION LINE CONDITIONS / MASTE

The following geometry can be treated with this condition:

   - SCATRA 

HETEROGENEOUS REACTION LINE CONDITIONS / SLAV
---------------------------------------------

::

   --DESIGN SCATRA HETEROGENEOUS REACTION LINE CONDITIONS / SLAV

The following geometry can be treated with this condition:

   - SCATRA 

HETEROGENEOUS REACTION SURF CONDITIONS / MASTE
----------------------------------------------

::

   --DESIGN SCATRA HETEROGENEOUS REACTION SURF CONDITIONS / MASTE

The following geometry can be treated with this condition:

   - SCATRA 

HETEROGENEOUS REACTION SURF CONDITIONS / SLAV
---------------------------------------------

::

   --DESIGN SCATRA HETEROGENEOUS REACTION SURF CONDITIONS / SLAV

The following geometry can be treated with this condition:

   - SCATRA 

IMMERSED SEARCHBO
-----------------

::

   --DESIGN VOLUME IMMERSED SEARCHBO

The following geometry can be treated with this condition:

   - VOLUME 

IMPEDANCE CONDITION
-------------------

::

   --DESIGN SURF IMPEDANCE CONDITION

The following geometry can be treated with this condition:

   - SURF 

INITIAL FIELD CONDITION
-----------------------

::

   --DESIGN POINT INITIAL FIELD CONDITION
   --DESIGN LINE INITIAL FIELD CONDITION
   --DESIGN SURF INITIAL FIELD CONDITION
   --DESIGN VOL INITIAL FIELD CONDITION

The following geometry can be treated with this condition:

   - POINT 
   - LINE 
   - SURF 
   - VOL 

INTEGRAL SURF CONDITION
-----------------------

::

   --DESIGN DOMAIN INTEGRAL SURF CONDITION
   --DESIGN BOUNDARY INTEGRAL SURF CONDITION

The following geometry can be treated with this condition:

   - DOMAIN 
   - BOUNDARY 

INTEGRAL VOL CONDITION
----------------------

::

   --DESIGN DOMAIN INTEGRAL VOL CONDITION

The following geometry can be treated with this condition:

   - DOMAIN 

INTERFACE MESHTYING LINE CONDITION
----------------------------------

::

   --DESIGN SSI INTERFACE MESHTYING LINE CONDITION

The following geometry can be treated with this condition:

   - SSI 

INTERFACE MESHTYING SURF CONDITION
----------------------------------

::

   --DESIGN SSI INTERFACE MESHTYING SURF CONDITION

The following geometry can be treated with this condition:

   - SSI 

INV ANALYSI
-----------

::

   --DESIGN SURFACE INV ANALYSI

The following geometry can be treated with this condition:

   - SURFACE 

LEVEL SET CONTACT CONDITIO
--------------------------

::

   --DESIGN LINE LEVEL SET CONTACT CONDITIO
   --DESIGN POINT LEVEL SET CONTACT CONDITIO

The following geometry can be treated with this condition:

   - LINE 
   - POINT 

LEVELSET COMBUSTION VOL CONDITION
---------------------------------

::

   --DESIGN XFEM LEVELSET COMBUSTION VOL CONDITION

The following geometry can be treated with this condition:

   - XFEM 

LEVELSET NAVIER SLIP VOL CONDITION
----------------------------------

::

   --DESIGN XFEM LEVELSET NAVIER SLIP VOL CONDITION

The following geometry can be treated with this condition:

   - XFEM 

LEVELSET NEUMANN VOL CONDITION
------------------------------

::

   --DESIGN XFEM LEVELSET NEUMANN VOL CONDITION

The following geometry can be treated with this condition:

   - XFEM 

LEVELSET TWOPHASE VOL CONDITION
-------------------------------

::

   --DESIGN XFEM LEVELSET TWOPHASE VOL CONDITION

The following geometry can be treated with this condition:

   - XFEM 

LEVELSET WEAK DIRICHLET VOL CONDITION
-------------------------------------

::

   --DESIGN XFEM LEVELSET WEAK DIRICHLET VOL CONDITION

The following geometry can be treated with this condition:

   - XFEM 

LINE CONDITION
--------------

::

   --DESIGN ABSORBING LINE CONDITION

The following geometry can be treated with this condition:

   - ABSORBING 

LINE LIFT&DRA
-------------

::

   --DESIGN FLUID LINE LIFT&DRA

The following geometry can be treated with this condition:

   - FLUID 

LOCSYS CONDITION
----------------

::

   --DESIGN POINT LOCSYS CONDITION
   --DESIGN LINE LOCSYS CONDITION
   --DESIGN SURF LOCSYS CONDITION
   --DESIGN VOL LOCSYS CONDITION

The following geometry can be treated with this condition:

   - POINT 
   - LINE 
   - SURF 
   - VOL 

MESH VOL CONDITION
------------------

::

   --DESIGN FLUID MESH VOL CONDITION

The following geometry can be treated with this condition:

   - FLUID 

MIXED/HYBRID DIRICHLET CONDITION
--------------------------------

::

   --DESIGN LINE MIXED/HYBRID DIRICHLET CONDITION
   --DESIGN SURFACE MIXED/HYBRID DIRICHLET CONDITION

The following geometry can be treated with this condition:

   - LINE 
   - SURFACE 

MODE FOR KRYLOV SPACE PROJECTIO
-------------------------------

::

   --DESIGN SURF MODE FOR KRYLOV SPACE PROJECTIO
   --DESIGN VOL MODE FOR KRYLOV SPACE PROJECTIO

The following geometry can be treated with this condition:

   - SURF 
   - VOL 

MOMENT EB CONDITION
-------------------

::

   --DESIGN POINT MOMENT EB CONDITION

The following geometry can be treated with this condition:

   - POINT 

MONITOR LINE CONDITION
----------------------

::

   --DESIGN PRESSURE MONITOR LINE CONDITION

The following geometry can be treated with this condition:

   - PRESSURE 

MONITOR SURF CONDITION
----------------------

::

   --DESIGN PRESSURE MONITOR SURF CONDITION

The following geometry can be treated with this condition:

   - PRESSURE 

MORTAR CONTACT CONDITIONS 2
---------------------------

::

   --DESIGN LINE MORTAR CONTACT CONDITIONS 2

The following geometry can be treated with this condition:

   - LINE 

MORTAR CONTACT CONDITIONS 3
---------------------------

::

   --DESIGN SURF MORTAR CONTACT CONDITIONS 3

The following geometry can be treated with this condition:

   - SURF 

MORTAR CORNER CONDITIONS 2D/3
-----------------------------

::

   --DESIGN POINT MORTAR CORNER CONDITIONS 2D/3

The following geometry can be treated with this condition:

   - POINT 

MORTAR COUPLING CONDITIONS 2
----------------------------

::

   --DESIGN LINE MORTAR COUPLING CONDITIONS 2

The following geometry can be treated with this condition:

   - LINE 

MORTAR COUPLING CONDITIONS 3
----------------------------

::

   --DESIGN SURF MORTAR COUPLING CONDITIONS 3

The following geometry can be treated with this condition:

   - SURF 

MORTAR EDGE CONDITIONS 3
------------------------

::

   --DESIGN LINE MORTAR EDGE CONDITIONS 3

The following geometry can be treated with this condition:

   - LINE 

MORTAR MULTI-COUPLING CONDITIONS 2
----------------------------------

::

   --DESIGN LINE MORTAR MULTI-COUPLING CONDITIONS 2

The following geometry can be treated with this condition:

   - LINE 

MORTAR MULTI-COUPLING CONDITIONS 3
----------------------------------

::

   --DESIGN SURF MORTAR MULTI-COUPLING CONDITIONS 3

The following geometry can be treated with this condition:

   - SURF 

MORTAR SYMMETRY CONDITIONS 2D/3
-------------------------------

::

   --DESIGN POINT MORTAR SYMMETRY CONDITIONS 2D/3

The following geometry can be treated with this condition:

   - POINT 

MORTAR SYMMETRY CONDITIONS 3
----------------------------

::

   --DESIGN LINE MORTAR SYMMETRY CONDITIONS 3

The following geometry can be treated with this condition:

   - LINE 

MULTI-SCALE COUPLING POINT CONDITION
------------------------------------

::

   --DESIGN SCATRA MULTI-SCALE COUPLING POINT CONDITION

The following geometry can be treated with this condition:

   - SCATRA 

MULTIPNT CONSTRAINT 2
---------------------

::

   --DESIGN LINE MULTIPNT CONSTRAINT 2

The following geometry can be treated with this condition:

   - LINE 

MULTIPNT CONSTRAINT 3
---------------------

::

   --DESIGN SURFACE MULTIPNT CONSTRAINT 3

The following geometry can be treated with this condition:

   - SURFACE 

NAVIER SLIP SURF CONDITION
--------------------------

::

   --DESIGN XFEM NAVIER SLIP SURF CONDITION

The following geometry can be treated with this condition:

   - XFEM 

NAVIER SLIP TWO PHASE SURF CONDITION
------------------------------------

::

   --DESIGN XFEM NAVIER SLIP TWO PHASE SURF CONDITION

The following geometry can be treated with this condition:

   - XFEM 

NAVIER-SLIP BOUNDARY CONDITION
------------------------------

::

   --DESIGN LINE NAVIER-SLIP BOUNDARY CONDITION
   --DESIGN SURF NAVIER-SLIP BOUNDARY CONDITION

The following geometry can be treated with this condition:

   - LINE 
   - SURF 

NEUMANN CONDITION
-----------------

::

   --DESIGN POINT NEUMANN CONDITION
   --DESIGN LINE NEUMANN CONDITION
   --DESIGN SURF NEUMANN CONDITION
   --DESIGN VOL NEUMANN CONDITION

The following geometry can be treated with this condition:

   - POINT 
   - LINE 
   - SURF 
   - VOL 

NEUMANN INTEGRATIO
------------------

::

   --DESIGN SURFACE NEUMANN INTEGRATIO
   --DESIGN LINE NEUMANN INTEGRATIO

The following geometry can be treated with this condition:

   - SURFACE 
   - LINE 

NEUMANN SURF CONDITION
----------------------

::

   --DESIGN XFEM NEUMANN SURF CONDITION

The following geometry can be treated with this condition:

   - XFEM 

NORMAL NO PENETRATION CONDITIO
------------------------------

::

   --DESIGN SURFACE NORMAL NO PENETRATION CONDITIO
   --DESIGN LINE NORMAL NO PENETRATION CONDITIO

The following geometry can be treated with this condition:

   - SURFACE 
   - LINE 

NORMALDIR MULTIPNT CONSTRAINT 3
-------------------------------

::

   --DESIGN SURFACE NORMALDIR MULTIPNT CONSTRAINT 3

The following geometry can be treated with this condition:

   - SURFACE 

NORMALDIR MULTIPNT CONSTRAINT 3D PE
-----------------------------------

::

   --DESIGN SURFACE NORMALDIR MULTIPNT CONSTRAINT 3D PE

The following geometry can be treated with this condition:

   - SURFACE 

NURBS LS DIRICH CONDITION
-------------------------

::

   --DESIGN POINT NURBS LS DIRICH CONDITION
   --DESIGN LINE NURBS LS DIRICH CONDITION
   --DESIGN SURF NURBS LS DIRICH CONDITION
   --DESIGN VOL NURBS LS DIRICH CONDITION

The following geometry can be treated with this condition:

   - POINT 
   - LINE 
   - SURF 
   - VOL 

PARTIAL PRESSURE CALCULATION LINE CONDITION
-------------------------------------------

::

   --DESIGN OXYGEN PARTIAL PRESSURE CALCULATION LINE CONDITION

The following geometry can be treated with this condition:

   - OXYGEN 

PARTIAL PRESSURE CALCULATION SURF CONDITION
-------------------------------------------

::

   --DESIGN OXYGEN PARTIAL PRESSURE CALCULATION SURF CONDITION

The following geometry can be treated with this condition:

   - OXYGEN 

PARTIAL PRESSURE CALCULATION VOL CONDITION
------------------------------------------

::

   --DESIGN OXYGEN PARTIAL PRESSURE CALCULATION VOL CONDITION

The following geometry can be treated with this condition:

   - OXYGEN 

PARTICLE WAL
------------

::

   --DESIGN SURFACE PARTICLE WAL

The following geometry can be treated with this condition:

   - SURFACE 

PERIODIC BOUNDARY CONDITION
---------------------------

::

   --DESIGN LINE PERIODIC BOUNDARY CONDITION
   --DESIGN SURF PERIODIC BOUNDARY CONDITION

The following geometry can be treated with this condition:

   - LINE 
   - SURF 

PORO DIRICH CONDITION
---------------------

::

   --DESIGN POINT PORO DIRICH CONDITION
   --DESIGN LINE PORO DIRICH CONDITION
   --DESIGN SURF PORO DIRICH CONDITION
   --DESIGN VOL PORO DIRICH CONDITION

The following geometry can be treated with this condition:

   - POINT 
   - LINE 
   - SURF 
   - VOL 

PORO NEUMANN CONDITION
----------------------

::

   --DESIGN POINT PORO NEUMANN CONDITION
   --DESIGN LINE PORO NEUMANN CONDITION
   --DESIGN SURF PORO NEUMANN CONDITION
   --DESIGN VOL PORO NEUMANN CONDITION

The following geometry can be treated with this condition:

   - POINT 
   - LINE 
   - SURF 
   - VOL 

PORO PARTIAL INTEGRATIO
-----------------------

::

   --DESIGN SURFACE PORO PARTIAL INTEGRATIO
   --DESIGN LINE PORO PARTIAL INTEGRATIO

The following geometry can be treated with this condition:

   - SURFACE 
   - LINE 

PORO PRESSURE INTEGRATIO
------------------------

::

   --DESIGN SURFACE PORO PRESSURE INTEGRATIO
   --DESIGN LINE PORO PRESSURE INTEGRATIO

The following geometry can be treated with this condition:

   - SURFACE 
   - LINE 

POROCOUPLING CONDITIO
---------------------

::

   --DESIGN VOLUME POROCOUPLING CONDITIO
   --DESIGN SURFACE POROCOUPLING CONDITIO

The following geometry can be treated with this condition:

   - VOLUME 
   - SURFACE 

RATE LINE CONDITION
-------------------

::

   --DESIGN FLOW RATE LINE CONDITION

The following geometry can be treated with this condition:

   - FLOW 

RATE SURF CONDITION
-------------------

::

   --DESIGN FLOW RATE SURF CONDITION
   --DESIGN IMPULS RATE SURF CONDITION

The following geometry can be treated with this condition:

   - FLOW 
   - IMPULS 

RECOVERY BOUNDARY LINE CONDITION
--------------------------------

::

   --DESIGN PATCH RECOVERY BOUNDARY LINE CONDITION

The following geometry can be treated with this condition:

   - PATCH 

RECOVERY BOUNDARY SURF CONDITION
--------------------------------

::

   --DESIGN PATCH RECOVERY BOUNDARY SURF CONDITION

The following geometry can be treated with this condition:

   - PATCH 

REDUCED D AIRWAYS EVALUATE LUNG VOLUME CONDITION
------------------------------------------------

::

   --DESIGN LINE REDUCED D AIRWAYS EVALUATE LUNG VOLUME CONDITION

The following geometry can be treated with this condition:

   - LINE 

REDUCED D AIRWAYS VOL DEPENDENT PLEURAL PRESSURE CONDITION
----------------------------------------------------------

::

   --DESIGN LINE REDUCED D AIRWAYS VOL DEPENDENT PLEURAL PRESSURE CONDITION

The following geometry can be treated with this condition:

   - LINE 

REDUCED D To 3D FLOW COUPLING CONDITION
---------------------------------------

::

   --DESIGN NODE REDUCED D To 3D FLOW COUPLING CONDITION

The following geometry can be treated with this condition:

   - NODE 

RELATIVE ERROR LINE CONDITION
-----------------------------

::

   --DESIGN SCATRA RELATIVE ERROR LINE CONDITION

The following geometry can be treated with this condition:

   - SCATRA 

RELATIVE ERROR SURF CONDITION
-----------------------------

::

   --DESIGN SCATRA RELATIVE ERROR SURF CONDITION

The following geometry can be treated with this condition:

   - SCATRA 

RELATIVE ERROR VOL CONDITION
----------------------------

::

   --DESIGN SCATRA RELATIVE ERROR VOL CONDITION

The following geometry can be treated with this condition:

   - SCATRA 

RIGIDSPHERE POTENTIAL CHARGE CONDITION
--------------------------------------

::

   --DESIGN POINT RIGIDSPHERE POTENTIAL CHARGE CONDITION

The following geometry can be treated with this condition:

   - POINT 

ROBIN DIRICHLET SURF CONDITION
------------------------------

::

   --DESIGN XFEM ROBIN DIRICHLET SURF CONDITION

The following geometry can be treated with this condition:

   - XFEM 

ROBIN DIRICHLET VOL CONDITION
-----------------------------

::

   --DESIGN XFEM ROBIN DIRICHLET VOL CONDITION

The following geometry can be treated with this condition:

   - XFEM 

ROBIN LINE CONDITION
--------------------

::

   --DESIGN TRANSPORT ROBIN LINE CONDITION
   --DESIGN THERMO ROBIN LINE CONDITION

The following geometry can be treated with this condition:

   - TRANSPORT 
   - THERMO 

ROBIN NEUMANN SURF CONDITION
----------------------------

::

   --DESIGN XFEM ROBIN NEUMANN SURF CONDITION

The following geometry can be treated with this condition:

   - XFEM 

ROBIN NEUMANN VOL CONDITION
---------------------------

::

   --DESIGN XFEM ROBIN NEUMANN VOL CONDITION

The following geometry can be treated with this condition:

   - XFEM 

ROBIN SPRING DASHPOT CONDITION
------------------------------

::

   --DESIGN SURF ROBIN SPRING DASHPOT CONDITION

The following geometry can be treated with this condition:

   - SURF 

ROBIN SPRING DASHPOT COUPLING CONDITION
---------------------------------------

::

   --DESIGN SURF ROBIN SPRING DASHPOT COUPLING CONDITION

The following geometry can be treated with this condition:

   - SURF 

ROBIN SURF CONDITION
--------------------

::

   --DESIGN TRANSPORT ROBIN SURF CONDITION
   --DESIGN THERMO ROBIN SURF CONDITION

The following geometry can be treated with this condition:

   - TRANSPORT 
   - THERMO 

Reduced D AIRWAYS AIR CONDITION
-------------------------------

::

   --DESIGN LINE Reduced D AIRWAYS AIR CONDITION

The following geometry can be treated with this condition:

   - LINE 

Reduced D AIRWAYS CAPILLARY CONDITION
-------------------------------------

::

   --DESIGN LINE Reduced D AIRWAYS CAPILLARY CONDITION

The following geometry can be treated with this condition:

   - LINE 

Reduced D AIRWAYS HEMOGLOBIN CONDITION
--------------------------------------

::

   --DESIGN LINE Reduced D AIRWAYS HEMOGLOBIN CONDITION

The following geometry can be treated with this condition:

   - LINE 

Reduced D AIRWAYS INITIAL SCATRA CONDITION
------------------------------------------

::

   --DESIGN LINE Reduced D AIRWAYS INITIAL SCATRA CONDITION

The following geometry can be treated with this condition:

   - LINE 

Reduced D AIRWAYS PRESCRIBED CONDITION
--------------------------------------

::

   --DESIGN NODE Reduced D AIRWAYS PRESCRIBED CONDITION

The following geometry can be treated with this condition:

   - NODE 

Reduced D AIRWAYS PRESCRIBED EXTERNAL PRESSURE CONDITION
--------------------------------------------------------

::

   --DESIGN LINE Reduced D AIRWAYS PRESCRIBED EXTERNAL PRESSURE CONDITION

The following geometry can be treated with this condition:

   - LINE 

Reduced D AIRWAYS PRESCRIBED SCATRA CONDITION
---------------------------------------------

::

   --DESIGN NODE Reduced D AIRWAYS PRESCRIBED SCATRA CONDITION

The following geometry can be treated with this condition:

   - NODE 

Reduced D AIRWAYS SCATRA EXCHANGE CONDITION
-------------------------------------------

::

   --DESIGN LINE Reduced D AIRWAYS SCATRA EXCHANGE CONDITION

The following geometry can be treated with this condition:

   - LINE 

Reduced D AIRWAYS VENTILATOR CONDITION
--------------------------------------

::

   --DESIGN NODE Reduced D AIRWAYS VENTILATOR CONDITION

The following geometry can be treated with this condition:

   - NODE 

SILVER-MUELLER CONDITION
------------------------

::

   --DESIGN LINE SILVER-MUELLER CONDITION
   --DESIGN SURF SILVER-MUELLER CONDITION

The following geometry can be treated with this condition:

   - LINE 
   - SURF 

SLIP SUPPLEMENTAL CURVED BOUNDARY CONDITION
-------------------------------------------

::

   --DESIGN LINE SLIP SUPPLEMENTAL CURVED BOUNDARY CONDITION
   --DESIGN SURFACE SLIP SUPPLEMENTAL CURVED BOUNDARY CONDITION

The following geometry can be treated with this condition:

   - LINE 
   - SURFACE 

STATE OF CHARGE LINE CONDITION
------------------------------

::

   --DESIGN ELECTRODE STATE OF CHARGE LINE CONDITION

The following geometry can be treated with this condition:

   - ELECTRODE 

STATE OF CHARGE SURF CONDITION
------------------------------

::

   --DESIGN ELECTRODE STATE OF CHARGE SURF CONDITION

The following geometry can be treated with this condition:

   - ELECTRODE 

STATE OF CHARGE VOL CONDITION
-----------------------------

::

   --DESIGN ELECTRODE STATE OF CHARGE VOL CONDITION

The following geometry can be treated with this condition:

   - ELECTRODE 

STATISTICS LINE CONDITION
-------------------------

::

   --DESIGN FLUCTHYDRO STATISTICS LINE CONDITION

The following geometry can be treated with this condition:

   - FLUCTHYDRO 

STATISTICS SURF CONDITION
-------------------------

::

   --DESIGN FLUCTHYDRO STATISTICS SURF CONDITION

The following geometry can be treated with this condition:

   - FLUCTHYDRO 

STC LAYE
--------

::

   --DESIGN VOL STC LAYE

The following geometry can be treated with this condition:

   - VOL 

STRESS CALC LINE CONDITION
--------------------------

::

   --DESIGN FLUID STRESS CALC LINE CONDITION

The following geometry can be treated with this condition:

   - FLUID 

STRESS CALC SURF CONDITION
--------------------------

::

   --DESIGN FLUID STRESS CALC SURF CONDITION

The following geometry can be treated with this condition:

   - FLUID 

SURF CONDITION
--------------

::

   --DESIGN ABSORBING SURF CONDITION

The following geometry can be treated with this condition:

   - ABSORBING 

SURF LIFT&DRA
-------------

::

   --DESIGN FLUID SURF LIFT&DRA

The following geometry can be treated with this condition:

   - FLUID 

SURFACE CONDITIO
----------------

::

   --DESIGN UNCERTAIN SURFACE CONDITIO
   --DESIGN AAA SURFACE CONDITIO

The following geometry can be treated with this condition:

   - UNCERTAIN 
   - AAA 

THERMO COUPLING CONDITION
-------------------------

::

   --DESIGN POINT THERMO COUPLING CONDITION

The following geometry can be treated with this condition:

   - POINT 

THERMO DIRICH CONDITION
-----------------------

::

   --DESIGN POINT THERMO DIRICH CONDITION
   --DESIGN LINE THERMO DIRICH CONDITION
   --DESIGN SURF THERMO DIRICH CONDITION
   --DESIGN VOL THERMO DIRICH CONDITION

The following geometry can be treated with this condition:

   - POINT 
   - LINE 
   - SURF 
   - VOL 

THERMO INITIAL FIELD CONDITION
------------------------------

::

   --DESIGN POINT THERMO INITIAL FIELD CONDITION
   --DESIGN LINE THERMO INITIAL FIELD CONDITION
   --DESIGN SURF THERMO INITIAL FIELD CONDITION
   --DESIGN VOL THERMO INITIAL FIELD CONDITION

The following geometry can be treated with this condition:

   - POINT 
   - LINE 
   - SURF 
   - VOL 

THERMO NEUMANN CONDITION
------------------------

::

   --DESIGN POINT THERMO NEUMANN CONDITION
   --DESIGN LINE THERMO NEUMANN CONDITION
   --DESIGN SURF THERMO NEUMANN CONDITION
   --DESIGN VOL THERMO NEUMANN CONDITION

The following geometry can be treated with this condition:

   - POINT 
   - LINE 
   - SURF 
   - VOL 

TISSUE REDAIRWAY CONDITION
--------------------------

::

   --DESIGN SURF TISSUE REDAIRWAY CONDITION
   --DESIGN NODE TISSUE REDAIRWAY CONDITION

The following geometry can be treated with this condition:

   - SURF 
   - NODE 

TOTAL TRACTION CORRECTION BORDER NODE
-------------------------------------

::

   --DESIGN LINE TOTAL TRACTION CORRECTION BORDER NODE

The following geometry can be treated with this condition:

   - LINE 

TOTAL TRACTION CORRECTION CONDITION
-----------------------------------

::

   --DESIGN SURF TOTAL TRACTION CORRECTION CONDITION

The following geometry can be treated with this condition:

   - SURF 

TRANSPORT DIRICH CONDITION
--------------------------

::

   --DESIGN POINT TRANSPORT DIRICH CONDITION
   --DESIGN LINE TRANSPORT DIRICH CONDITION
   --DESIGN SURF TRANSPORT DIRICH CONDITION
   --DESIGN VOL TRANSPORT DIRICH CONDITION

The following geometry can be treated with this condition:

   - POINT 
   - LINE 
   - SURF 
   - VOL 

TRANSPORT NEUMANN CONDITION
---------------------------

::

   --DESIGN POINT TRANSPORT NEUMANN CONDITION
   --DESIGN LINE TRANSPORT NEUMANN CONDITION
   --DESIGN SURF TRANSPORT NEUMANN CONDITION

The following geometry can be treated with this condition:

   - POINT 
   - LINE 
   - SURF 

TURBULENT INFLOW TRANSFE
------------------------

::

   --DESIGN SURF TURBULENT INFLOW TRANSFE

The following geometry can be treated with this condition:

   - SURF 

UPDATE LINE CONDITION
---------------------

::

   --DESIGN ALE UPDATE LINE CONDITION

The following geometry can be treated with this condition:

   - ALE 

UPDATE SURF CONDITION
---------------------

::

   --DESIGN ALE UPDATE SURF CONDITION

The following geometry can be treated with this condition:

   - ALE 

VOLTAGE LINE CONDITION
----------------------

::

   --DESIGN CELL VOLTAGE LINE CONDITION

The following geometry can be treated with this condition:

   - CELL 

VOLTAGE POINT CONDITION
-----------------------

::

   --DESIGN CELL VOLTAGE POINT CONDITION

The following geometry can be treated with this condition:

   - CELL 

VOLTAGE SURF CONDITION
----------------------

::

   --DESIGN CELL VOLTAGE SURF CONDITION

The following geometry can be treated with this condition:

   - CELL 

VOLUME CONSTRAINT 3
-------------------

::

   --DESIGN SURFACE VOLUME CONSTRAINT 3

The following geometry can be treated with this condition:

   - SURFACE 

VOLUME CONSTRAINT 3D PE
-----------------------

::

   --DESIGN SURFACE VOLUME CONSTRAINT 3D PE

The following geometry can be treated with this condition:

   - SURFACE 

VOLUME MONITOR 3
----------------

::

   --DESIGN SURFACE VOLUME MONITOR 3

The following geometry can be treated with this condition:

   - SURFACE 

VOLUMETRIC FLOW BORDER NODE
---------------------------

::

   --DESIGN LINE VOLUMETRIC FLOW BORDER NODE

The following geometry can be treated with this condition:

   - LINE 

VOLUMETRIC FLOW CONDITION
-------------------------

::

   --DESIGN SURF VOLUMETRIC FLOW CONDITION

The following geometry can be treated with this condition:

   - SURF 

WEAK DIRICHLET CONDITION
------------------------

::

   --DESIGN LINE WEAK DIRICHLET CONDITION
   --DESIGN SURFACE WEAK DIRICHLET CONDITION

The following geometry can be treated with this condition:

   - LINE 
   - SURFACE 

WEAK DIRICHLET SURF CONDITION
-----------------------------

::

   --DESIGN XFEM WEAK DIRICHLET SURF CONDITION

The following geometry can be treated with this condition:

   - XFEM 

WEAR CONDITIONS 3
-----------------

::

   --DESIGN SURFACE WEAR CONDITIONS 3

The following geometry can be treated with this condition:

   - SURFACE 

Other boundary conditions:
-----------------------------

- SCATRA FLUX CALC LINE CONDITIONS
- SCATRA FLUX CALC SURF CONDITIONS
- TRANSPORT NEUMANN INFLOW LINE CONDITIONS
- TRANSPORT NEUMANN INFLOW SURF CONDITIONS
- TRANSPORT THERMO CONVECTION LINE CONDITIONS
- TRANSPORT THERMO CONVECTION SURF CONDITIONS
- ELECTRODE BOUNDARY KINETICS POINT CONDITIONS
- ELECTRODE BOUNDARY KINETICS LINE CONDITIONS
- ELECTRODE BOUNDARY KINETICS SURF CONDITIONS
- ELECTRODE DOMAIN KINETICS LINE CONDITIONS
- ELECTRODE DOMAIN KINETICS SURF CONDITIONS
- ELECTRODE DOMAIN KINETICS VOL CONDITIONS
- FLUID TURBULENT INFLOW VOLUME
- FLUID NEUMANN INFLOW LINE CONDITIONS
- FLUID NEUMANN INFLOW SURF CONDITIONS
- SURFACE TENSION CONDITIONS
- SCATRA CELL INTERNALIZATION LINE CONDITIONS
- SCATRA CELL INTERNALIZATION SURF CONDITIONS
- SCATRA CELL EXTERNALIZATION LINE CONDITIONS
- SCATRA CELL EXTERNALIZATION SURF CONDITIONS
- SCATRA CELL EXTERNALIZATION VOL CONDITIONS
- SURFACTANT CONDITIONS
- TAYLOR GALERKIN OUTFLOW SURF CONDITIONS
- TAYLOR GALERKIN NEUMANN INFLOW SURF CONDITIONS
- REINITIALIZATION TAYLOR GALERKIN SURF CONDITIONS
- BEAM INTERACTION/BEAM TO SOLID VOLUME MESHTYING VOLUME
- BEAM INTERACTION/BEAM TO SOLID VOLUME MESHTYING LINE
- BEAM INTERACTION/BEAM TO SOLID SURFACE MESHTYING SURFACE
- BEAM INTERACTION/BEAM TO SOLID SURFACE MESHTYING LINE
- MICROSCALE CONDITIONS
