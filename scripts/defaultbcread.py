#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Reading boundary condition types from text file (extract from default.bc)
@author: scheider
"""
from sys import exit


class Boundarycondition():
  def __init__(self, bctype1, bctype2, position):
    self.bctype1 = bctype1
    self.bctype2 = bctype2
    bctype = bctype1 + " " + bctype2
    self.bcname = bctype.strip()
    self.geometries = []
    self.position = position
    self.fullnames = []
    
  def addGeometry(self, geometry):
    self.geometries.append(geometry)
    fullname = self.bctype1 
    fullname += " " if self.bctype1 != "" else ""
    fullname += geometry + " " + self.bctype2
    self.fullnames.append(fullname)
    
  def write(self, outfile):
    linktarget = self.bcname.replace(" ","")
    outfile.write(".. _" + linktarget.lower() + ":\n\n")
    outfile.write(self.bcname + "\n")
    outfile.write(underscore("-", len(self.bcname)))
    outfile.write("\n\n")
    outfile.write("::\n\n")
    for geo in self.geometries:
      outfile.write("   --%s %s %s\n" % (self.bctype1, geo, self.bctype2))
    outfile.write("\nThe following geometry can be treated with this condition:\n\n")
    for geo in self.geometries:
      outfile.write("   - %s \n" % (geo))
    outfile.write("\n")
    
  
class Element():
  shapes  = {}

  def __init__(self, name, shape, family, nodes):
    self.name = name
    self.family = family
    self.shape = [shape]
    if shape not in Element.shapes:
      Element.shapes[shape] = nodes
    else:
      if nodes != Element.shapes[shape]:
        print("Element shape %s should have %d nodes" % (shape, Element.shapes[shape]))
        print("But it has %d nodes for element type %s" % (nodes, name))
        exit()
    self.initialparameter = "no parameter"
    self.parameters = {self.initialparameter:{"number": 0, "string": 0, "optional": False}}


  def addshape(self, newshape, nodes):
    self.shape.append(newshape)
    if newshape not in Element.shapes:
      Element.shapes[newshape] = nodes
      
      
  def write(self, file):
    linktarget = self.family + self.name
    file.write(".. _" + linktarget.lower() + ":\n\n")
    file.write(self.name + "\n")
    file.write(underscore("^", len(self.name)))
    file.write("\n\n")
    file.write("*Shapes:*\n\n")
    for shp in self.shape:
      file.write("- :ref:`%s <%s>` (%d nodes)\n" % (
      shp, shp.lower(), Element.shapes[shp]))
    file.write("\n")
    #
    # print parameter table
    if len(self.parameters)>1:
      file.write("**Parameters**\n\n")
      lenparmax = len("Parameter")
      for p,v in self.parameters.items():
        lenpar = 7 if v["optional"] else 0
        lenpar += len(p)
        if lenpar>lenparmax:
          lenparmax = lenpar
      horline = "+" + underscore("-", lenparmax+2) + "+" + underscore("-", 13) + "+\n"
      file.write(horline)
      file.write("| {0:<{1}} | {2:11} |\n".format("Parameter", lenparmax, "Values"))
      file.write(horline.replace("-", "="))
      for p,v in self.parameters.items():      
        parname = p
        if parname == self.initialparameter:
          continue
        if v["optional"]:
          parname += " (opt.)"
        if v["number"]>0:
          file.write("| {0:<{1}} | {2:2d} x number |\n".format(parname, lenparmax, v["number"]))
        elif v["string"]>0:
          file.write("| {0:<{1}} | {2:2d} x string |\n".format(parname, lenparmax, v["string"]))
        else:
          file.write("| {0:<{1}} | {2:<11} |\n".format(parname, lenparmax, "None"))
        file.write(horline)
      file.write("\n")

    

def underscore(charac, n):
  fmtstring = ""
  for _ in range(n):
    fmtstring += charac
  return fmtstring


typedict = {}
geometries = ["POINT", "LINE", "SURF", "SURFACE", "VOL", "VOLUME"]
geometrydict = {}
otherbc = []
elementclass = "No class"
elementdict = {elementclass: {}}
readbc = False
readelement = False
with open("default.bc", "r") as bcinput:
  for line in bcinput.readlines():
    line = line.strip()
    begindash = line.startswith("--")
    if begindash and line.find("VALIDCONDITIONS")>=0:
        readbc = True
        readelement = False
    elif begindash and line.endswith("ELEMENTS"):
      dashposition = line.rfind("-")
      spaceposition = line.rfind(" ")
      elementclass = line[dashposition+1: spaceposition]
      elementdict[elementclass] = {}
      readelement = True
      readbc = False
    elif readbc:
      #
      # read boundary conditions
      #
      if begindash:
        dashposition = line.rfind("--")
        if dashposition<0: 
          continue
        bcfullname = line[dashposition+2:]
        splitline = bcfullname.split(" ")
        if len(splitline)<2:
          raise KeyError(splitline+" does not seem to be a valid boundary condition")
        for pos, word in enumerate(splitline):
          if word in geometrydict.keys():
            break
        else:
          pos = 1
        try:
          geometry = splitline[pos]
        except:
          print(pos, bcfullname)
          raise Exception()
        bctype1 = " ".join(splitline[:pos])
        bctype2 = " ".join(splitline[pos+1:])
        bcname = bctype1 + " " + bctype2
        bcname = bcname.strip()
        if geometry in geometrydict:
          geometrydict[geometry].append(bcname)
        else:
          geometrydict[geometry] = [bcname]
        if bcname not in typedict:
          typedict[bcname] = Boundarycondition(bctype1, bctype2, pos)
        typedict[bcname].addGeometry(geometry)
    elif readelement:
      #
      # read elements
      #
      if line.startswith("//"):
        linelist = line.split(" ")
        ipos = 4
        while ipos<len(linelist) and linelist[ipos]=="0":
          ipos += 1
        nodes = ipos - 4
        elementtype = linelist[2]
        elementshape = linelist[3]
        if elementtype in elementdict[elementclass]:
          newelement = elementdict[elementclass][elementtype]
          newelement.addshape(elementshape, nodes)
        else:
          newelement = Element(elementtype, elementshape, elementclass, nodes)
          optional = False
          parameter = "nopar"
          while ipos<len(linelist):
            if linelist[ipos] == "0":
              newelement.parameters[parameter]["number"] += 1
            elif linelist[ipos] == "''":
              newelement.parameters[parameter]["string"] += 1
            elif linelist[ipos] == "[":
              optional = True
            elif linelist[ipos] == "]":
              optional = False
            elif linelist[ipos] != "":
              parameter = linelist[ipos]
              newelement.parameters[parameter] = {"number": 0, "string": 0, "optional": optional}
            ipos += 1
          elementdict[elementclass][elementtype] = newelement
          
#
# generate boundaryconditions.rst
#
bcoutput = open("bcreference.rst", "w")
bcoutput.write(".. _boundaryconditionreference:\n\n")
bcoutput.write("Boundary condition reference\n")
bcoutput.write("=============================\n\n")

bclist = list(typedict.keys())
bclist.sort()
print("%d DESIGN boundaries have been read from the boundary section.\n" % len(bclist))
for bctype in bclist:
  typedict[bctype].write(bcoutput)
bcoutput.close()
#
# generate elementtypes.rst
#
eleoutput = open("elementtypes.rst", "w")
eleoutput.write(".. _elementtypes:\n\n")
eleoutput.write("Element types\n")
eleoutput.write("-------------\n\n")
for eclass,elementtypes in elementdict.items():
  print("%d %s elements have been read  from the element section" % (len(elementtypes), eclass))
  if len(elementtypes)>0:
    eleoutput.write(".. _%selements:\n\n" % eclass.lower())
    eleoutput.write("%s ELEMENTS\n" % eclass)
    eleoutput.write("%s\n\n" % (underscore("~", len(eclass)+9)))
  for etype in elementtypes.values():
    etype.write(eleoutput)
eleoutput.close()
#
# generate elementshapes.rst
#
shapeoutput = open("elementshapes.rst", "w")
shapeoutput.write(".. _elementshapes:\n\n")
shapeoutput.write("Element shapes\n")
shapeoutput.write("--------------\n\n")
for shape, nodes in Element.shapes.items():
  shapeoutput.write(".. _%s:\n\n" % shape.lower())
  shapeoutput.write(shape + "\n")
  shapeoutput.write(underscore("~", len(shape)) + "\n\n")
  shapeoutput.write("- Dimension: %s\n" % "xD")
  shapeoutput.write("- Nodes: %d\n" % nodes)
  shapeoutput.write("- Integration points: %d\n\n" % 0)
shapeoutput.close()
