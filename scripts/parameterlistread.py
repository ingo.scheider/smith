# -*- coding: utf-8 -*-
"""
This script reads the valid parameters from the output of
baci-release --parameters
in order to generate a restructuredText file for use with ReadTheDocs.
Input:
  A file denoted as parameter.txt must exist in the current directory.
Output:
  A file denoted as parameter.rst is generated

"""
import json
from sys import exit
from os import path


class Parameteritem():
  def __init__(self, name, value, section):
    self._name = name
    self._value = value
    self._docstring = ""
    self._section = section
    self.validtypes = []
    self.validvalues = []
  
  def __repr__(self):
    return self._name


  @property
  def name(self):
    return self._name
  
  @name.setter
  def name(self, name):
    self._name = name
  

  @property
  def value(self):
    return self._value
  
  @value.setter
  def value(self, value):
    self._value = value
  

  @property
  def section(self):
    return self._section
  
  @section.setter
  def section(self, section):
    self._section = section
  
  
  @property
  def docstring(self):
    return self._docstring
  
  @docstring.setter
  def docstring(self, docstr):
    dollar1 = docstr.find("\f$")
    while dollar1>=0:
      dollar2 = docstr[dollar1+2:].find("\f$")
      if dollar2>=0:
        mathstring = ":math:`" + docstr[dollar1+2:dollar1+dollar2+2] + "`"
        if "*" in mathstring:
          mathstring = mathstring.replace("*", "\cdot")
        docstr = docstr[:dollar1] + mathstring + docstr[dollar1+dollar2+4:]
        dollar1 = docstr.find("\f$")
      else:
        dollar1 = -1
        print(docstr, "seems to be erronous")
    if "*" in docstr:
      docstr = docstr.replace("*", "\*")
    self._docstring = docstr


  def getfullname(self, separator="/", deletespaces = False):
    fullname = self._name
    if deletespaces:
      fullname = fullname.replace("_", "")
    tmpsec = self._section
    fullname = tmpsec.name + separator + fullname
    for i in range(tmpsec.level):
      try:
        fullname = tmpsec._parentsection._name + separator + fullname
      except:
        print("WARNING!", self._name, self._section.getfullname())
        break
      tmpsec = tmpsec._parentsection
    if deletespaces:
      fullname = fullname.replace(" ", "")
    return fullname


  def print(self, outfile):
    targetstring = self.getfullname("_", True)
    # print link tartet
    outfile.write(".. _%s:\n\n" % targetstring.lower())
    # print parameter itself
    partypes = ",".join(self.validtypes)
    outfile.write("``{}``  {} (valid types: {})\n\n".format(
                  self.name, self.value, partypes))
    if self._docstring!="":
      docstring = self._docstring
      p = docstring.find("\n")
      for outline in docstring.split("\n"):
        outfile.write(outline + "\n")
      outfile.write("\n")
    if targetstring in parameterdict:
      additionaldoc = parameterdict[targetstring]
      if additionaldoc!="":
        outfile.write(additionaldoc + "\n\n")
    if len(self.validvalues)>0:
      outfile.write("\nThe following values are valid:\n\n")
      vUpper = set()
      for v in self.validvalues:
        if v.upper() in vUpper:
          continue
        vUpper.add(v.upper())
        outfile.write("- {}\n".format(v))
      outfile.write("\n")
  

class Parametersection():
  maxlevel = 4
  def __init__(self, name, level, docstring = "", parent = None):
    self._name = name
    self._docstring = docstring
    self._level = level
    self.parameters = []
    self.subsections = []
    self._parentsection = parent

  def __repr__(self):
    return self._name
  

  @property
  def name(self):
    return self._name
  
  @name.setter
  def name(self, name):
    self._name = name
  
  
  @property
  def level(self):
    return self._level
  
  @level.setter
  def level(self, level):
    self._level = level
  
  
  @property
  def docstring(self):
    return self._docstring
  
  @docstring.setter
  def docstring(self, docstr):
    self._docstring = docstr


  @property
  def parentsection(self):
    return self._parentsection
  
  @parentsection.setter
  def parentsection(self, parentsection):
    self._parentsection = parentsection
  

  def getfullname(self, separator="/", deletespaces = False):
    fullname = self._name
    tmpsec = self
    for i in range(self._level):
      try:
        fullname = tmpsec._parentsection._name + separator + fullname
      except:
        print("WARNING!", self._name, self._level, tmpsec)
        break
      tmpsec = tmpsec._parentsection
    if deletespaces:
      fullname = fullname.replace(" ", "")
    return fullname

    
  def print(self, outfile, includeSubsection = False):
    sectionstring = self.getfullname()
    targetstring = self.getfullname("_", True)
    # print link target
    outfile.write("\n.. _SEC%s:\n" % targetstring.lower())
    # print section title
    outfile.write("\n{0}\n".format(sectionstring))
    if self._level==0:
      fmtstring = "{:->%d}\n" % len(sectionstring)
    else:
      fmtstring = "{:~>%d}\n" % len(sectionstring)      
    outfile.write(fmtstring.format(""))
    # print index entry
    outfile.write("\n.. index::\n")
    outfile.write("   single: {0}\n   section: {0}\n\n".format(self.name))
    # print code snippet
    outfile.write("::\n\n   {:->75}\n\n".format(sectionstring))
    # write docstring
    if self.docstring!="":
      outfile.write("\n{}\n\n".format(self._docstring))
    if targetstring in parameterdict:
      additionaldoc = parameterdict[targetstring]
      if additionaldoc!="":
        outfile.write(additionaldoc + "\n")
    # write parameters
    outfile.write("\n**Parameters**\n\n")
    for parameteritem in self.parameters:
      parameteritem.print(outfile)

#--------------------------------------------
# Main program
#--------------------------------------------

# Dictionary for additional docstring input.
if path.isfile("parameter.json"):
  with open("parameter.json", "r") as paramjson:
    parameterdict = json.load(paramjson)
    print("There are %d additional parameter documentation entries in parameter.json" %
          len(parameterdict))
else:
  parameterdict = {}
originalparamdict = {}

paramfile = open("parameter.txt", "r")
rstfile = open("parameter.rst", "w")
rstfile.write(".. _furtherinputparameters:\n\n")
rstfile.write("Further input parameters\n")
rstfile.write("===========================\n\n")
#
# initialization
startreading = False
firstparameter = False
currentsec = Parametersection("", -1)
currentparent = None
#
# Main loop over the lines in parameter.txt file
line = paramfile.readline()
linestrip = line.strip()
while line!="" and not line.startswith("Memory"):
  if linestrip=="":
    line = paramfile.readline()
    linestrip = line.strip()
    continue
  elif linestrip.endswith("->"):
    # found a section header
    startreading = True
    firstparameter = True
    section = line[:-4].strip()
    level = line.find(section) - 4
    newsection = Parametersection(section, level)
    # write section stuff
    # now check level and add to the tree:
    if currentsec.level==level:
      currentparent.subsections.append(newsection)
      newsection.parentsection = currentparent
    elif level == currentsec.level + 1:
      currentsec.subsections.append(newsection)
      newsection.parentsection = currentsec
      currentparent = currentsec
    elif level<currentsec.level:
      for i in range(currentsec.level - level):
        currentsec = currentsec.parentsection
      currentparent = currentsec.parentsection
      newsection.parentsection = currentparent
      currentparent.subsections.append(newsection)
    else:
      print("Got level %d after %d, which is wrong. Exiting..." % (level, currentsec.level))
      exit(1)
    if level==0:
      originalparamdict[newsection.getfullname("_", True)] = ""
    currentsec = newsection
    line = paramfile.readline()
    linestrip = line.strip()

  elif not startreading:
    # first lines are ignored
    line = paramfile.readline()
    linestrip = line.strip()
  elif linestrip.startswith("#"):
    # This can only be a docstring of a section
    currentsec.docstring = linestrip[2:]
    line = paramfile.readline()
    linestrip = line.strip()
  else:
    maxvaluelen = 8
    maxparamlen = 0
    newparameter = None
    
    while (not linestrip.endswith("->")) and len(line)>0:
      if linestrip=="" or "PrintEqualSign" in linestrip:
        line = paramfile.readline()
        linestrip = line.strip()
        continue
      if line.startswith("Memory"):
        print("Reading finished")
        break
      if not linestrip[0]=="#":
        # It's a parameter
        if firstparameter:
          firstparameter = False
        else:
          currentsec.parameters.append(newparameter)
        # create a new Parameter instance
        try:
          param, default = linestrip.split(" = ")
        except:
          print("Could not evaluate parameter from")
          print(line)
          exit()
        newparameter = Parameteritem(param.strip(), default.strip(), currentsec)
        if len(param)>maxparamlen:
          maxparamlen = len(param)
      else:
        # it's some additional parameter information
        linestring = linestrip[2:].strip()
        if linestring.startswith("Accepted"):
          # it's a numerical type
          paramtypes = linestring.split(":")[1]
          newparameter.validtypes += [s.replace('"', '') for s in paramtypes[:-1].split(",")]
        elif linestring.startswith("Validator"):
          # Here a particular validator is used
          newparameter.validtypes.append("string")
          linestrip = paramfile.readline().strip()[2:]
          newparameter.validvalues.append(linestrip.strip().split(" ")[0])
        elif linestring.startswith("Valid "):
          # it's a string with some valid values
          newparameter.validtypes.append("string")
          paramfile.readline() # read the opening bracket
          linestrip = paramfile.readline().strip()
          while not linestrip.endswith("}"):
            v = linestrip[2:].replace('"', '').strip()
            if len(v)>maxvaluelen:
              maxvaluelen = len(v)
            newparameter.validvalues.append(v)
            linestrip = paramfile.readline().strip()
        else:
          # it's a docstring
          newparameter.docstring += linestring
        # endif
      # endif
      line = paramfile.readline()
      linestrip = line.strip()
      originalparamdict[newparameter.getfullname("_", True)] = ""
    # end while - parameters inside sections
    if newparameter:
      currentsec.parameters.append(newparameter)
    currentsec.print(rstfile)
  # end if
# end while - parameter file
rstfile.close()
            
with open("originalparam.json", "w") as jsonfile:
  json.dump(originalparamdict, jsonfile, indent=4)
